Ext.define('MAM.view.window.ActionWindow', {
    extend: 'Ext.window.Window',
    cls: 'actionWindow',
    alias: 'widget.actionWindow',
    height: 450,
    width: 800,
    layout: 'fit',
    modal: true,

    initComponent: function () {
        this.callParent(arguments);
    }
});