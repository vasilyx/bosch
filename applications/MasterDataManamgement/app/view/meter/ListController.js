Ext.define('MAM.view.meter.ListController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.meterListController',
    control: {
        '#': {
            boxready: 'loadMeters'
        }
    },
   
    loadMeters: function( ){
        var store = this.getViewModel().getStore('meters');
        this.fireEvent('toggleLoadingMask', true , _tl.get('mam.label.ui.loading.meter'));
        this.loadStore( store );
    },
     onCustomIdSearch: function( textfield ){
        var store = this.getView().getStore();

         if(!Ext.isEmpty(textfield.getValue())){
             Ext.merge(store.getProxy().extraParams, {customId: textfield.getValue()});
             this.loadStore( store );
         }
         else{
            delete store.getProxy().extraParams.customId;
            this.loadStore( store );
        }

    },
    loadStore: function( store ){
        store.load({
            scope: this,
            callback: function(){
                this.fireEvent('toggleLoadingMask', false );
            }
        });
    }
});