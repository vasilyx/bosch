Ext.define('MAM.store.MarketParticipants', {
    extend: 'Ext.data.Store',
    alias: 'store.marketParticipants',
    storeId: 'marketParticipants',
    model: 'MAM.model.MarketParticipant',
    autoLoad: false,
    pageSize: MAM.Config.List.limit,
    proxy: {
        type: 'ajax',
        url: '/MAM/' + MAM.Config.Backend.Versions + '/marketParticipants',
        startParam: 'skip',
        limitParam: 'top',
        extraParams: {
            expand: 'properties',
            totalNumber: true
        },
        headers: {
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty: 'marketParticipants',
            totalProperty: 'totalNumber'
        }
    }
});