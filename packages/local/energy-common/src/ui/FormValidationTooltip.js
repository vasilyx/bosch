/**
 * Displays a tooltip with an info icon and a text when hovering the icon
 * Usage example:
 * {
 *  xtype: 'Tooltip'
 *  quicktipHeader: 'header of the quicktip',
 *  quicktip: 'Explain why this field is neccesary or what happens here',
 *  type: 'error' //information is default, warning
 *  }
 */

Ext.define('Energy.common.ui.FormValidationTooltip', {
    extend: 'Energy.common.ui.FormTooltip',

    alias: 'widget.commonFormValidationTooltip',

    tipData: undefined,
    showFormat: false,
    showRemainingChars: false,
    showLength: false,

    onAfterRender: function (me) {
        var tip = new Ext.tip.ToolTip({
            target: me.getId(),
            tpl: me._buildTemplate(),
            data: me.tipData
        });

        me._applyFieldSpecificData(tip);
    },

    _buildTemplate: function () {
        var tpl = [];

        tpl.push('<tpl for=".">', '<div class="commonFormValidationTooltip"><h1>{title}</h1>');

        if (this.tipData.example) {
            tpl.push('<div class="example">{example}</div>');
        }

        tpl.push('<hr />');

        if (this.tipData.format || this.showRemainingChars) {
            tpl.push('<table>');

            if (this.showFormat && this.tipData.format) {
                if (Ext.isArray(this.tipData.format)) {
                    tpl.push(
                        '<tpl for="format">',
                        '<tr>',
                        '<tpl if="xindex === 1">',
                        Ext.String.format('<td>{0}:</td>', this.showFormat),
                        '</tpl>',
                        '<tpl if="xindex !== 1">',
                        '<td></td>',
                        '</tpl>',
                        '<td>{.}</td>',
                        '</tr>',
                        '</tpl>'
                    );
                } else {
                    tpl.push(
                        '<tr>',
                        Ext.String.format('<td>{0}:</td>', this.showFormat),
                        '<td>{format}</td>',
                        '</tr>'
                    );
                }
            }

            if (this.showLength) {
                tpl.push(
                    '<tr>',
                    Ext.String.format('<td>{0}:</td>', this.showLength),
                    '<td>{length}</td>',
                    '</tr>'
                );
            }

            if (this.showRemainingChars) {
                tpl.push(
                    '<tr>',
                    Ext.String.format('<td>{0}:</td>', this.showRemainingChars),
                    '<td>{remainingChars}</td>',
                    '</tr>'
                );
            }

            tpl.push('</table>');
        }

        tpl.push('</div></tpl>');

        return tpl;
    },

    _applyFieldSpecificData: function (tooltip) {
        var previousNode = this.previousNode('textfield');

        if (previousNode) {
            if (previousNode.minLength === previousNode.maxLength) {
                this.tipData.length = previousNode.minLength;
            } else {
                var minLength = 0 !== previousNode.minLength ? previousNode.minLength : (previousNode.allowBlank ? 0 : 1),
                    maxLength = (Number.MAX_VALUE === previousNode.maxLength ? '&infin;' : previousNode.maxLength);

                this.tipData.length = minLength + ' - ' + maxLength;
            }

            previousNode.on('change', function (field) {
                var maxLength = field.maxLength,
                    currLength = field.getValue().length;

                this.tipData.remainingChars = (Number.MAX_VALUE === maxLength) ? '&infin;' : Math.max(0, maxLength - currLength);
            }, this);
        }


        tooltip.on('beforeshow', function () {
            tooltip.update(this.tipData);
        }, this);
    }
});
