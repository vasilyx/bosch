Ext.define('MAM.utils.Configuration', {

    singleton: true,
    requires: [ 'MAM.utils.Constants'],
    config: {
        mam: undefined
    },

    getNavConfig: function(){
        return this.getMam().Navigation;
    },

    getGatewayTabConfig: function(){
      return this.getTabConfig( MAM.utils.Constants.GATEWAY );
    },

    getFirstTabConfig: function(){
        return this.getMam().Navigation[0];
    },

    getTabConfig: function( entityType ){
        var tabConfig;
        var navigation = this.getMam().Navigation;
        Ext.each(navigation, function( config ){
            if(config.type === entityType ){
                tabConfig = config.subDetail;
            }
        });
        return tabConfig;
    },
    getActionPanelSubMenu: function(menu, entity) {
        if (this.getMam().ActionPanel.subMenu.hasOwnProperty(menu)) {

            var menu = this.getMam().ActionPanel.subMenu[menu];
            var items=[];

            Ext.each(menu, function( val ){

                if (val.entity.indexOf(entity)> -1) {
                    val.text = _tl.get(val.text);
                    items.push(val);
                }

            });

            return {items: items};
        } else {
            return null;
        }
    },
    getActionPanelRules: function(menu){
        if (this.getMam().ActionPanel.rules.hasOwnProperty(menu)) {
            return this.getMam().ActionPanel.rules[menu];
        } else {
            return [];
        }
    },
    getHistory: function(){
        return this.getMam().History;
    },

    /**
     * Get url for entities property list of allowed values
     * @param entity string
     * @param property string
     * @returns string
     */
    getUrlEnumProperty: function(entity, property) {
        if (!this.getMam().Backend.UrlEnumProperties.hasOwnProperty(entity)) {
            console.log('Exception: Check config for Backend.UrlEnumProperties.' + entity);
            return '';
        }

        if (!this.getMam().Backend.UrlEnumProperties[entity].hasOwnProperty(property)) {
            console.log('Exception: Check config for Backend.UrlEnumProperties.' + entity + '.' + property);
            return '';
        }

        return this.getMam().Backend.UrlEnumProperties[entity][property];
    },

    /**
     * Get list of allowed custom attributes
     * @param entityType
     */
    getCustomAttributesList: function(entityType) {
        var keys= '',
            result=[];

        var tab = this.getTabConfig(entityType);
        Ext.each(tab, function( arr ){
            if(arr.type === 'customAttributes' && arr.hasOwnProperty('keys')){
                keys = arr.keys;
            }
        });

        if (keys) {
            Ext.each(keys, function( key ){
               result.push({key: key, label: MAM.utils.model.Converts.convertCustomAttribute(key) });
            });
        }

        return result;
    },

    /**
     * @returns {Array}
     */
    getDefinedRelationList: function() {
        return this.getMam().Relations.definedEntityList;
    },

    /**
     * @param entityType
     * @returns {Array}
     */
    getRelationEditList: function(entityType) {
        var tab = this.getTabConfig(entityType);
        var result = [];

        Ext.each(tab, function( arr ){
            if(arr.type === 'relations' && arr.hasOwnProperty('editAllowed')){
                result = arr.editAllowed;
            }
        });

        return result;
    }
});