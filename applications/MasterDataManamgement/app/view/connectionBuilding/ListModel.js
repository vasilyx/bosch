Ext.define('MAM.view.connectionBuilding.ListModel', {
    extend: 'Ext.app.ViewModel',
    requires: ['MAM.model.Meter', 'MAM.store.Meters'],
    
    alias: 'viewmodel.connectionBuildingListModel',
    
    data: {
        title: 'Meter Liste'
    },
    stores: {
        connectionBuildings: {
           type: 'connectionBuildings'
        }
    }
});