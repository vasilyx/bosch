Ext.define('MAM.view.customAttributes.InitialModel', {
    extend: 'Ext.app.ViewModel',
    requires: [
       'MAM.model.Attribute'
    ],
    alias: 'viewmodel.customAttributesInitialModel',

    data: {
        entity: null
    },

    stores: {
        customAttributes: {
            model: 'MAM.model.Attribute',
            autoLoad: false,
            proxy: {
                type: 'memory'
            }
        }
    }


});