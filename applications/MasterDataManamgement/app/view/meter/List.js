Ext.define('MAM.view.meter.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.meterList',
    requires: [
        'MAM.view.meter.ListModel',
        'MAM.view.meter.ListController'
    ],
    viewModel: 'meterListModel',
    controller: 'meterListController',
    border: true,
    bind: {
        store: '{meters}'
    },
    viewConfig: {
        loadMask: false
    },
    dockedItems: [{
        xtype: 'pagingtoolbar',
        reference: 'pagingToolbar',
        bind: {
            store: '{meters}'
        },
        overflowHandler: 'menu',
        dock: 'bottom',
        displayInfo: true
    }],
    initComponent: function () {
        var columns = [
            {
                text: _tl.get('mam.label.meter.customId'),
                dataIndex: 'customId',
                flex: 1,
                items: [{
                    xtype: 'textfield',
                    margin: 3,
                    flex: 1,
                    width: 270, //theme error
                    enableKeyEvents: true,
                    listeners: {
                        keyup: 'onCustomIdSearch',
                        buffer: 1000
                    }
                }]
            }
        ];
        Ext.apply(this, {columns: columns});
        this.callParent(arguments);
    }

});