/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
!function () {
/* header */
    
var fs      = require('fs')
    
Role('Siesta.Launcher.FileSystem.PhantomJS', {
    
    does        : Siesta.Launcher.FileSystem.Base,
    
    has : {
    },
    
    
    methods : {
        
        fileExists : function (fileName) {
            fileName        = this.normalizePath(fileName)
            
            return fs.exists(fileName)
        },
        
        
        readFile : function (fileName, mode) {
            fileName        = this.normalizePath(fileName)
            
            if (mode == 'rb') {
                return fs.open(fileName, 'rb').read() 
            } else
                return fs.read(fileName)
        },
        
        
        saveFile : function (fileName, content, mode) {
            // has to be called with linux-style filename, before the `normalizePath` is called
            // for Slimer
            this.ensurePathExists(fileName)
            
            fileName        = this.normalizePath(fileName)
            
            fs.write(fileName, content, mode || 'w')
        },
        
        
        copyFile : function (source, dest) {
            this.saveFile(dest, this.readFile(source, 'rb'), 'wb')            
        },
        
        
        copyTree : function (source, dest) {
            try {
                fs.removeTree(dest)
            } catch (e) {
            }
            
            fs.copyTree(source, dest)
        },

        
        ensurePathExists : function (fileName) {
        },
        
        
        normalizePath : function (path) {
            return fs.absolute(path)
        }
    }
})

/* footer */
}()