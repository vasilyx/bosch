/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Siesta.Launcher.Runner.BaseRunner', {
    
    does : [
        JooseX.Observable,
        Siesta.Launcher.Role.CanPrintWithLauncher
    ],
    
    
    has : {
        launcher                    : { required : true },
        dispatcher                  : null,
        
        // indicates the max number of pages (but not guarantees that number of pages can be created)
        // runner always should be able to create at least one page!
        maxWorkers                  : 1,
        
        pageCount                   : 0,
        reservedPageCount           : 0,
        pages                       : Joose.I.Object,
        
        pagesPerChunk               : 20,
        
        pageCreationFailuresCount   : 0,
        
        isPageCreationPaused        : false,
        pauseTimeout                : null
    },
    
    
    methods : {
        
        pausePageCreation : function (timeout, callback) {
            var me                      = this
            
            this.isPageCreationPaused   = true
            
            clearTimeout(this.pauseTimeout)
            
            this.pauseTimeout           = setTimeout(function () {
                me.isPageCreationPaused = false
                
                callback && callback()
            }, timeout)
        },
        
        
        getPageIdList : function () {
            var ids     = []
            
            Joose.O.each(this.pages, function (page, id) { ids.push(id) })
            
            return ids
        },
        
        
        reserveOnePage : function () {
            if (!this.canCreatePage()) return false
            
            this.reservedPageCount++
            
            return true
        },
        
        
        releaseReservedPage : function () {
            this.reservedPageCount--
            if (this.reservedPageCount < 0) this.reservedPageCount = 0
        },
        
        
        createPage : function (callback) {
            throw new Error("Abstract method call: `createPage`")
        },
        
        
        canCreatePage : function () {
            return this.pageCount + this.reservedPageCount < this.maxWorkers
        },
        
        
        requestPage : function (callback, arg) {
            if (!this.reserveOnePage()) return false
            
            var me      = this
            
            // `createPage` should not throw and instead return null
            this.createPage(function (e, page) {
                if (page) {
                    me.pageCreationFailuresCount    = 0
                    
                    me.pages[ page.id ]   = page
                    me.pageCount++
                    
                    me.releaseReservedPage()
                    
                    page.on('close', me.onPageClosed, me, { single : true })
                    
                    callback(page, arg)
                } else {
                    me.pageCreationFailuresCount++
                    
                    me.releaseReservedPage()
                    
                    callback(null, arg, e)
                }
            })
            
            return true
        },
        
        
        onPageClosed : function (event) {
            var page        = event.source
            
            delete this.pages[ page.id ]
            this.pageCount--
        },

        
        // promised method
        destroy : function () {
            clearTimeout(this.pauseTimeout)
            
            var promises    = []
            
            Joose.O.each(this.pages, function (page) {
                promises.push(page.close())
            })
            
            return Promise.all(promises)
        },
        
        
        // promised method, should never reject, but instead resolve to the instance of Error
        setup : function () {
            this.debug("Runner setup: " + this)
            
            return Promise.resolve(this)
        }
    }
})
