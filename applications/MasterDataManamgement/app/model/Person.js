Ext.define('MAM.model.Person', {
    extend: 'Ext.data.Model',
    requires: [
        'MAM.model.Relation'
    ],
    idProperty: 'id',
    fields: [
        { name: 'id', type: 'int', mapping: 'personId'},
        { name: 'customId', type: 'string'},
        { name: 'salutation', type: 'string'},
        { name: 'title', type: 'string'},

        { name: 'firstName', type: 'string'},
        { name: 'name', type: 'string'},
        { name: 'nameAffix', type: 'string'},


        { name: 'street', type: 'string'},
        { name: 'houseNumber', type: 'string'},
        { name: 'houseNumberAppendix', type: 'string'},

        { name: 'postBoxCode', type: 'string'},
        { name: 'postalCode', type: 'string'},
        { name: 'city', type: 'string'},
        { name: 'country', type: 'string'},

        { name: 'phoneNumber', type: 'string'},
        { name: 'mobileNumber', type: 'string'},
        { name: 'emailAddress', type: 'string'},

        { name: 'createdTimeStamp', convert: MAM.utils.model.Converts.convertDate },

        { name: 'customAttributes', reference: 'MAM.model.CustomAttribute'}

    ]
});