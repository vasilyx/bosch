StartTest( function(t) {

    var list = t.getExt().create('TestApplication.application.AbstractList', {t: t, name: 'person'});

    MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/l/details_1477904.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/l/details_1477907.json');
    //dummy only to mock data and don't get error from browser
    MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/l/customAttributes_dummy.json', 1477904);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/l/relations_dummy.json', 1477904);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/l/customAttributes_dummy.json', 1477907);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/l/relations_dummy.json', 1477907);

    list.testListViewArea();
    list.testFilledDataInList(44);
    list.testPageCorrect();
    list.testDetailArea();
    list.testFilterById('70466612');
    list.testFilterEmpty();
    list.testSelectRow(1); // select second row
    list.testSelectRow(4, 8);
});