// also supports: startTest(function(t) {
StartTest(function(t) {
    t.diag("Initial Config");

    //this test is checking, if all base preloads are there

    t.ok(Ext, 'ExtJS is here');

    t.ok(MAM.Config, 'MAM Application Config loaded');

    t.ok('MAM.Application', 'MAM Application loaded');

    t.ok(Energy.common, 'Energy is here');

    t.ok(TestApplication.data.MockData, 'TestApplication.data.MockData object available');

    t.ok(TestApplication.application.AbstractList, 'TestApplication.application.AbstractList object available');

    t.done();   // Optional, marks the correct exit point from the test
})