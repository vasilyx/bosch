/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Launcher.Options.Proxy', {
    
    override : {

        validate : function () {
            if (this.SUPER() === false) return false
            
            var options     = this.options
            var proxy       = options.proxy
            
            proxy && Joose.O.extend(options, this.parseProxyShortcut(proxy))
            
            return true
        }
    },
            
    methods : {

        parseProxyShortcut : function (str) {
            var res         = {}
            var match       = /(?:(.+?)(?:\:(.+))?@)?(.*)/.exec(str)
            
            if (match) {
                res[ 'proxy-user' ]     = match[ 1 ]
                res[ 'proxy-password' ] = match[ 2 ] || null
                str                     = match[ 3 ]
            }
            
            var match       = /(.+):(\d+)/.exec(str)
            
            if (match) {
                res[ 'proxy-host' ] = match[ 1 ]
                res[ 'proxy-port' ] = match[ 2 ]
            } else
                res[ 'proxy-host' ] = str
                
            return res
        },
        
        
        getProxyHostPort : function () {
            var options     = this.options
            
            return options[ 'proxy-host' ] + (options[ 'proxy-port' ] ? ':' + options[ 'proxy-port' ] : '')
        }
    }
})
