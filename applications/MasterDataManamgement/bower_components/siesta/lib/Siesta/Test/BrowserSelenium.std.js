/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Siesta.Test.Browser.meta.extend({
    
    has : {
        /**
         * @cfg {Object} screenshotCompareConfig Config object with the detailed comparison options, which 
         * corresponds to the command line options of the [`compare` command](http://www.imagemagick.org/script/compare.php) 
         * from the [ImageMagick toolkit](http://www.imagemagick.org). You can override/add any of it globally (using {@link #configure} method, or at the individual screenshot level, 
         * please refer to the {@link #screenshot} method.
         * 
         * @member Siesta.Test.Browser 
         */
        screenshotCompareConfig     : null 
        // the default value for this option is actually defined in the `compareImages` 
        // method of the Siesta.Launcher.Page.WebDriverRhino
    },
    
    override : {
        
        queueCommand : function (command, callback) {
            if (!this.harness.isAutomated) {
                this.diag("Command: `" + command.name + "` skipped - not running in automation")
                
                callback && callback('skipped')
                
                return
            }
            
            var me          = this
            var async       = me.beginAsync(command.timeout)
            
            this.harness.queueCommand(command, function (result) {
                callback && callback(result)
                
                me.endAsync(async)
            })
        },
        
        /**
         * **This feature is available only in Standard package**.
         * 
         * This method will take a screenshot of the currently visible state of the entire page.
         * 
         * Screenshot will not be taken immediately, but after slight delay (while the command will reach the WebDriver). 
         * This method is asynchronous, and to continue test execution after screenshot, you need to provide a callback to it.
         * 
         * You can use this method in the chain step, using the method invocation form:
         * 

    t.chain(
        { screenshot : 'mydir/fileName.png' },
        // or
        { screenshot : { fileName : 'mydir/fileName.png'} }
    )

         * 
         * One can use the `--screenshot-compare-with-previous` and `--screenshot-compare-with-base` command line options
         * to compare the screenshots with the corresponding existing image. If the screenshots will be found to be different
         * a failing assertion will be added to the test.
         * 
         * **Note**, that this feature only works when running in the WebDriver launcher. When running tests in the browser
         * this command will be ignored with diagnostic message.
         * 
         * @param {String/Object} options Either a string or a config object with the following properties. String corresponds
         * to the following object: `{ fileName : "string" }`
         * 
         * @param {String} options.fileName Required in the object form. The name of the file to 
         * save the screenshot to, relative to the current working directory
         * of the launcher script or `--screenshot-dir` directory. File will be saved in the PNG format, 
         * the ".png" extension will be appended if missing. You can provide the directory in the file name, 
         * it will be created if missing, for example: `screenshots/grids/checkpoint1.png` - the `screenshots` 
         * and `grids` directories will be auto-created.
         * 
         * @param {Siesta.Test.ActionTarget} options.target If this option is provided, instead of taking a screenshot of the whole page, 
         * only the individual DOM element will be captured. Should contain one of the {@link Siesta.Test.ActionTarget} values to 
         * convert to DOM element.
         * 
         * @param {Boolean} options.skipCompare If set to `true`, this screenshot command will simply take a screenshot
         * and ignore the `--screenshot-compare-with-previous` and `--screenshot-compare-with-base` command line options.
         * 
         * @param {Object} options.cfg Config object with the detailed comparison options, if provided, this value will override
         * a global setting {@link #screenshotCompareConfig}
         * 
         * @param {Function} callback The callback to call, after the screenshot has been saved to the file.
         * 
         * @method
         * @member Siesta.Test.Browser 
         */
        screenshot : function (options, callback) {
            var me          = this
            
            if (me.typeOf(options) == 'String') options = { fileName : options }
            
            if (!options.fileName) {
                me.fail("No filename provided for the `screenshot` method")
                callback && me.processCallbackFromTest(callback)
                return
            }
            
            var target      = options.target
            
            if (target)
                me.waitForTargetAndSyncMousePosition(target, null, takeScreenshot, [], false, false)
            else
                takeScreenshot()
            
            function takeScreenshot() {
                var el          = target ? me.normalizeElement(target) : me.global
                
                me.fireEvent('beforescreenshot')
                
                var $el         = me.$(el)
                var offset      = $el.offset()
                
                var width       = target ? $el.outerWidth() : $el.width()
                var height      = target ? $el.outerHeight() : $el.height()
                
                me.queueCommand({
                    // reserverd props: "name", "id"
                    name            : 'screenshot',
                    
                    fileName        : options.fileName,
                    skipCompare     : options.skipCompare,
                    cfg             : options.cfg,
                    
                    // total width/height of the screenshot, will be used to detect the Retina resolution
                    totalWidth      : me.$(window).width(),
                    totalHeight     : me.$(window).height(),
                    
                    x               : target ? offset.left : 0,
                    y               : target ? offset.top : 0,
                    width           : width,
                    height          : height
                }, function (result) {
                    me.fireEvent('screenshot')
                    
                    if (result != 'skipped')
                        if (!result.success) 
                            me.fail("Taking screenshot has failed with error: " + result.error)
                        else {
                            var compare     = result.compare
                            
                            if (compare) 
                                if (result.similar)
                                    me.pass("Screenshots are equal", {
                                        got         : result.newScreenshot,
                                        need        : result.oldScreenshot,
                                        gotDesc     : 'New screenshot',
                                        needDesc    : compare == 'base' ? 'Base screenshot' : 'Previous screenshot',
                                        annotation  : 'Similarity: ' + result.similarity
                                    })
                                else
                                    me.fail("Screenshots are different", {
                                        got         : result.newScreenshot,
                                        need        : result.oldScreenshot,
                                        gotDesc     : 'New screenshot',
                                        needDesc    : compare == 'base' ? 'Base screenshot' : 'Previous screenshot',
                                        annotation  : 'Similarity: ' + result.similarity
                                    })
                            else
                                me.pass("Successfully taken screenshot: " + options.fileName)
                        }
                    
                    callback && me.processCallbackFromTest(callback, [ result ])
                })
                
            }
        },
        

        /**
         * **This feature is available only in Standard package**.
         * 
         * Convenience wrapper for the {@link #screenshot}, takes the screenshot of individual DOM element.
         * 
         * @param {Siesta.Test.ActionTarget} target One of the {@link Siesta.Test.ActionTarget} values to convert to the DOM
         * element.
         * 
         * @param {String} fileName The name of the file to save the screenshot to. See the {@link #screenshot} method
         * for additional details.
         * 
         * @param {Function} callback The callback to call, after the screenshot has been saved to the file.
         * 
         * @method
         * @member Siesta.Test.Browser 
         */ 
        screenshotElement : function (target, fileName, callback) {
            this.screenshot({ target : target, fileName : fileName }, callback)
        }
    }
});
