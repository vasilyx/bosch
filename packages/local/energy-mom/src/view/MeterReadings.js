Ext.define('Energy.mom.view.MeterReadings', {
    extend: 'Ext.panel.Panel',
    requires: ['Energy.mom.view.MeterReadingsController', 'Energy.mom.view.MeterReadingsModel'],
    alias: 'widget.actionMeterReadings',

    controller: 'actionMeterReadingsController',
    viewModel: {
        type: 'meterReadingsModel'
    },

    layout: {
        type: 'vbox',
        align: 'stretch'
    },


    initComponent: function () {
        var groupingFeature = Ext.create('Ext.grid.feature.Grouping',{
            id: 'group',
            groupHeaderTpl: LangMeterReadings.get('meter') + ' {name} {[values.rows[0].data.meterPointTimePeriod]}',
            hideGroupedHeader: true
        });

        var items = [
            {
                xtype: 'container',
                html: LangMeterReadings.get('meterPoint') + ': ' + this.getViewModel().get('record').get('customId'),
                cls: 'marketParticipantHistoryHeader',
                margin: 10
            },
            {
                xtype: 'grid',
                itemId: 'meterReadingsGrid',
                bind: {
                    store: '{meterReadings}'
                },
                emptyText: LangMeterReadings.get('noValues'),
                flex: 1,
                features: groupingFeature,
                columns: [
                    {
                        text: LangMeterReadings.get('registerName'),
                        dataIndex: 'registerName',
                        minWidth: 150,
                        flex: 2,
                        renderer: function( value ){
                            if(!value){
                                return LangMeterReadings.get('noValues');
                            }
                            else{
                                return value;
                            }
                        }
                    },
                    {
                        text: LangMeterReadings.get('reason'),
                        dataIndex: 'reason',
                        flex: 1,
                        renderer: function(value){
                            if (LangMeterReadings.isset('reasonType_' + value)) {
                                return LangMeterReadings.get('reasonType_' + value);
                            } else {
                                return value;
                            }
                        }
                    },
                    {
                        xtype: 'numbercolumn',
                        text: LangMeterReadings.get('value'),
                        dataIndex: 'value',
                        flex: 1,
                        align: 'right'
                    },
                    {
                        text: LangMeterReadings.get('unit'),
                        dataIndex: 'unit',
                        width: 100
                    },
                    {
                        xtype: 'datecolumn',
                        text: LangMeterReadings.get('readingDate'),
                        dataIndex: 'readingDate',
                        width: 150,
                        format: 'd.m.Y H:i:s'}
                ]
            }
        ];

        var buttons =  [{
            text: LangMeterReadings.get('close'),
            itemId: 'closeButton'
        }];

        Ext.apply(this, {items: items, buttons: buttons});
        this.callParent(arguments);
    }
});