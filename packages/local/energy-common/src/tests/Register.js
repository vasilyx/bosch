Ext.define('Energy.common.tests.Register', {
    requires: [
		'Energy.common.ux.ajax.PlainJsonSimlet',
        'Ext.ux.ajax.SimManager',
        'Ext.Ajax'
    ],
    //singleton: true,
    constructor: function() {

        var simMgr = Ext.ux.ajax.SimManager.init({
            delay: 150,
            defaultSimlet: null
        });

        this.simMgr = simMgr;

    },

    setMockTodayDate: function(year, month, day){
        var testDate = Date;
        //override Date object to have alwas the
        //same date for new Date()
        Date = function (Date) {
            FakeDate.prototype = Date.prototype;
            FakeDate.parse = Date.parse;
            return FakeDate;

            function FakeDate() {
                //Fake date is 07.10.2016
                return new testDate(year, month-1, day);

            }
        }(Date);
    },

    getMockedData: function (dataLocation) {
        var me = this;
        Ext.Ajax.request({
            url: dataLocation,
            success: function (response) {
                if(dataLocation === 'tests/data/gateways.json'){
                    me.registerGatewaysUrl(Ext.JSON.decode(response.responseText));
                }
                /** add there other url types
                 else if(dataLocation.startsWith('tests/data/meterPoints.json')){
                    me.registerMeterPointsUrl(Ext.JSON.decode(response.responseText));
                }*/
            }
        });
    },

    registerGatewaysUrl: function(responseData ){
        this.simMgr.register(
            {
                '/MAM/v1/gateways': {//'/ibis/rest/rc/mdm/asset': {
                    stype: 'plainjson',  // use JsonSimlet (stype is like xtype for components)
                    data: responseData
                }
            }
        );
    },

    registerAssetDetailUrl: function(responseData){
        this.simMgr.register(
            {
                '/MAM/v1/meterPoints': {
                    stype: 'plainjson',
                    data: responseData
                }
            }
        );
    }
});