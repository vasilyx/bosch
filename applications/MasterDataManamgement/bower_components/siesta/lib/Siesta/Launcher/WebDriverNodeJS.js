/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
!function () {
/* header */

SHUTDOWN        = false
    
// global var
selenium        = require('selenium-webdriver')

require('http').globalAgent.keepAlive   = true
require('https').globalAgent.keepAlive  = true

Class('Siesta.Launcher.WebDriverNodeJS', {
    
    isa         : Siesta.Launcher.BaseLauncher,
    
    does        : [
        Siesta.Launcher.CommandLineTool.NodeJSTool,
        Siesta.Launcher.CommandLineTool.WebDriverNodeJSTool
    ],
    
    has : {
        executableName      : 'webdriver',
        serverJarPath       : 'binary/selenium/selenium-server-standalone-2.52.0.jar',
        
        capabilities        : Joose.I.Array,
        
        knownOptionGroups   : {
            init : {
                '11-proxy' : {
                    name        : 'Proxy options'
                },
                '15-screenshot' : {
                    name        : 'Screenshots options'
                },
                '40-remote' : {
                    name        : 'Remote host options'
                },
                '50-browserstack' : {
                    name        : 'BrowserStack options'
                },
                '60-saucelabs' : {
                    name    : 'SauceLabs options'
                }
            }
        },
        
        knownOptions        : {
            init : [
                {
                    name    : 'browser',
                    desc    : [
                        'Should be exactly one of the "firefox / chrome / ie / safari". Please note, that',
                        '--cap browserName=value capability takes precendece over this option.'
                    ],
                    // put at the top
                    group   : '00-system'
                },
                {
                    name    : 'xvfb',
                    desc    : [
                        'This is experimental option, supported only on Linux and only for local testing. ',
                        'Expects `google-chrome-stable` and `firefox` to be available in the PATH. When enabled, it will wrap every',
                        'launched browser process in isolated virtual desktop, using Xvfb command. This allows you to ',
                        'safely specify the value for --max-workers bigger than 1, significantly speeding up test suite execution'
                    ],
                    group   : '10-basic'
                },
                {
                    name    : 'cap',
                    desc    : [
                        'This option can be repeated several times, and specifies WebDriver capability',
                        'in the form --cap name=value, for example:\n',
                        '    --cap browserName=firefox --cap platform=XP'
                    ],
                    group   : '10-basic'
                },
                {
                    name    : 'proxy',
                    desc    : [
                        'This is a combined shortcut for all proxy options, with the following format:\n',
                        '   --proxy=name=USER:PASSWORD@HOST:PORT\n',
                        'The USER/PASSWORD/PORT parts are optional.',
                        'The USER/PASSWORD authentication is currently supported only for SOCKS proxy\n',
                        'When both shortcut and individual options are provided, the latters override the shortcut information'
                    ],
                    group   : '11-proxy'
                },
                {
                    name    : 'proxy-host',
                    desc    : [
                        'This option set the proxy host for this session. Example: localhost'
                    ],
                    group   : '11-proxy'
                },
                {
                    name    : 'proxy-port',
                    desc    : [
                        'This option set the proxy port for this session. Default value is 80'
                    ],
                    group   : '11-proxy'
                },
                {
                    name    : 'proxy-user',
                    desc    : [
                        'This option set the user name for the proxy server.',
                        'Currently supported only for SOCKS proxy'
                    ],
                    group   : '11-proxy'
                },
                {
                    name    : 'proxy-password',
                    desc    : [
                        'This option set the proxy password for this session.',
                        'Currently supported only for SOCKS proxy'
                    ],
                    group   : '11-proxy'
                },
                {
                    name    : 'touch-events',
                    desc    : [
                        'Enables the touch events support in the browser. Currently only recognized in Chrome.',
                        'With this option you can test touch-based applications in desktop browser'
                    ],
                    group   : '20-misc'
                },
                {
                    name    : 'firefox-profile',
                    desc    : [
                        'This option specifies a path to the local FireFox profile, to be used during tests.',
                        '   --firefox-profile=/home/user/test.profile',
                        'Note, that when used with remote testing, profile will be transfered to the server for every',
                        'opened browser window (expected certain performance impact).'
                    ],
                    group   : '20-misc'
                },
                {
                    name    : 'browser-arg',
                    desc    : [
                        'This option can be repeated several times, and specifies command line arguments for the browser process',
                        'in the form --browser-arg name=value, for example:\n',
                        '    --browser-arg multiple-automatic-downloads=1 --browser-arg disable-gpu',
                        'This option is supported for Chrome (local/remote), Firefox (local) and IE (local)'
                    ],
                    group   : '20-misc'
                },
                {
                    name    : 'screenshot-dir',
                    desc    : [
                        'Specifies the base directory for the screenshots. Screenshot paths will be resolved',
                        'relatively this directory. Current working directory by default.'
                    ],
                    group   : '15-screenshot'
                },
                {
                    name    : 'screenshot-compare-with-previous',
                    desc    : [
                        'If specified, screenshots will be compared with the currently existing files.',
                        'If screenshots will be found to be different, a failing assertion will be added to the test and',
                        'the diff image file will be created, having a name of the screenshot image, suffixed with the `-diff`',
                        'The previous screenshot file will be kept with the `-prev` suffix in the file name.', 
                        'See also the harness option `screenshotCompareConfig`'
                    ],
                    group   : '15-screenshot'
                },
                {
                    name    : 'screenshot-compare-with-base',
                    desc    : [
                        'A base directory of the other set of screenshots. Screenshots will be compared',
                        'with the correspoding screenshot from that base. For example, if `--screenshot-dir`',
                        'option is set to `chrome`, `--screenshot-compare-with-base` to `firefox`',
                        'and screenshot`s file name is `grids/users.png`, then the following screenshots',
                        'will be compared: `chrome/grids/users.png` (new version)',
                        'and `firefox/grids/users.png` (base version).',
                        'If screenshots will be found to be different, a failing assertion will be added to the test and',
                        'the diff image file will be created, having a name of the screenshot image, suffixed with the `-diff`:',
                        '`chrome/grids/users-diff.png`.',
                        'See also the harness option `screenshotCompareConfig`'
                    ],
                    group   : '15-screenshot'
                },
                {
                    name    : 'host',
                    desc    : [
                        'Host, running the RemoteWebDriver server. When provided, test suite will be launched',
                        'on that host. Note, that harness url will be accessed from remote host.'
                    ],
                    group   : '40-remote'
                },
                {
                    name    : 'port',
                    desc    : [
                        'Port number on the host with RemoteWebDriver server, default value is 4444'
                    ],
                    group   : '40-remote'
                },
                {
                    name    : 'browserstack-user',
                    desc    : [
                        'The username in the BrowserStack cloud testing infrastructure. When provided, will require a ',
                        '--browserstack-key option and automatically set the value of the "--host" option, pointing to',
                        'the BrowserStack server, also will set the capability --cap browserstack.local=true'
                    ],
                    group   : '50-browserstack'
                },
                {
                    name    : 'browserstack-key',
                    desc    : [
                        'The automate key in the BrowserStack cloud testing infrastructure'
                    ],
                    group   : '50-browserstack'
                },
                {
                    name    : 'browserstack',
                    desc    : [
                        'A shortcut option, allowing you to run your tests in the BrowserStack cloud testing infrastructure,',
                        'choosing from various browser/OS combinations. Usage format:\n',
                        '    --browserstack=<BS_USER>,<BS_KEY>\n',
                        'If any of the <BS_USER>, <BS_KEY> is missing, the following environment variables',
                        'will be read: BS_USER, BS_KEY.',
                        'This option will:\n',
                        '1) Establish a tunnel to the BrowserStack\n',
                        '2) Set the value of the "--host" option, pointing to the BrowserStack servers\n',
                        '3) Set the value of the "browserstack.local" webdriver capability to "true"\n',
                        'Use the --cap options to specify the desired browser and OS.', 
                        'More info at: https://www.browserstack.com/automate/capabilities (BrowserStack specific) and',
                        'https://github.com/SeleniumHQ/selenium/wiki/DesiredCapabilities (generic WebDriver)',
                        'You can do the 1) manually, using a provided wrapper for BrowserStack binaries:\n',
                        '    /bin/browserstacklocal\n',
                        'In such case just use the "--browserstack-user", "--browserstack-key" options'
                    ],
                    group   : '50-browserstack'
                },
                {
                    name    : 'browserstack_',
                    desc    : [
                        'Same as --browserstack but does not start tunnel.'
                    ],
                    group   : '50-browserstack'
                },
                {
                    name    : 'browserstack-enable-screenshots',
                    desc    : [
                        'Enable screenshots after every command, sent to the test page. Note, that enabling it',
                        'will increase the workload of the VM with the browser, which, in our experience,',
                        'may cause sporadic network failures.'
                    ],
                    group   : '50-browserstack'
                },
                {
                    name    : 'browserstack-tunnel-identifier',
                    desc    : [
                        "The id of the Browser Stack tunnel to use. Ignored if you've used the --browserstack shortcut"
                    ],
                    group   : '50-browserstack'
                },
                {
                    name    : 'saucelabs-user',
                    desc    : [
                        'The username in the SauceLabs cloud testing infrastructure. When provided, will require a',
                        '"--saucelabs-key" option and set the value of the "--host" option, pointing to SauceLabs server'
                    ],
                    group   : '60-saucelabs'
                },
                {
                    name    : 'saucelabs-key',
                    desc    : [
                        'The API key in the SauceLabs cloud testing infrastructure'                    
                    ],
                    group   : '60-saucelabs'
                },
                {
                    name    : 'saucelabs',
                    desc    : [
                        'A shortcut option, allowing you to run your tests in the SauceLabs cloud testing infrastructure,',
                        'choosing from various browser/OS combinations. Usage format:\n',
                        '    --saucelabs=<SL_USER>,<SL_KEY>[,<hostname1>,<hostname2>]\n',
                        'If any of the <SL_USER>, <SL_KEY> or <hostname1,2> is missing, the following environment variables',
                        'will be read: SL_USER, SL_KEY, SL_TUNNEL',
                        'This option will:\n',
                        '1) Establish a tunnel from SauceLabs to the local machine, if your webserver listens on domain(s),\n',
                        'different from `localhost`, then specify it as `hostname1,2`\n',
                        '2) Set the value of the "--host" option, pointing to the SauceLabs servers\n',
                        'Use the --cap options to specify the desired browser and OS.', 
                        'More info: https://github.com/SeleniumHQ/selenium/wiki/DesiredCapabilities',
                        'You can do the 1) manually, using a provided wrapper for SauceLabs binaries:\n',
                        '    /bin/sc -u <USER> -k <API_KEY> -t <hostname1>,<hostname2>\n',
                        'In such case use only the "--saucelabs-user", "--saucelabs-key" options'
                    ],
                    group   : '60-saucelabs'
                },
                {
                    name    : 'saucelabs_',
                    desc    : [
                        'Same as --saucelabs but does not start tunnel.'
                    ],
                    group   : '60-saucelabs'
                },
                {
                    name    : 'saucelabs-enable-video',
                    desc    : [
                        'Enable video recording of all test pages in the SauceLabs. Note, that enabling it',
                        'will increase the workload of the VM with the browser, which, in our experience,',
                        'may cause sporadic network failures.'
                    ],
                    group   : '60-saucelabs'
                },
                {
                    name    : 'saucelabs-enable-screenshots',
                    desc    : [
                        'Enable screenshots after every command, sent to the test page. Note, that enabling it',
                        'will increase the workload of the VM with the browser, which, in our experience,',
                        'may cause sporadic network failures.'
                    ],
                    group   : '60-saucelabs'
                },
                {
                    name    : 'saucelabs-tunnel-identifier',
                    desc    : [
                        "The id of the Sauce Connect tunnel to use. Ignored if you've used the --saucelabs shortcut"
                    ],
                    group   : '60-saucelabs'
                }
            ]
        }
    },
    
    
    methods : {
        
        initialize : function () {
            var me      = this
            
            process.on('uncaughtException', function (err) {
                console.log("Caught unhandled exception: ", err, err.stack);
                
                me.gracefulShutdown()
            });
            
            process.on('unhandledRejection', function (err) {
                console.log("Caught unhandled rejection: ", err, err.stack);
                
                me.gracefulShutdown()
            });            
            
            process.on('SIGTERM', this.gracefulShutdown.bind(this));
            process.on('SIGINT', this.gracefulShutdown.bind(this));
        },
        
        
        prepareOptions : function () {
            var res     = this.SUPERARG(arguments)
            
            if (!process.stdout.isTTY) this.options[ 'no-color' ] = true
            
            return res
        },
        
        
        gracefulShutdown : function () {
            if (this.shutDownStarted) return
            
            this.shutDownStarted    = true
            
            var me                  = this
            SHUTDOWN                = true
            
            return this.destroy().then(function () {
                me.exit(7)
            })
        },
        
        
        createOptionsWrapper : function (cfg) {
            return new Siesta.Launcher.Options.WebDriver({ options : cfg, launcher : this })
        },
        
        
        // this method runs before the "prepareOptions" call
        createRunners : function () {
            var options     = this.options
            
            if (options.bs) 
                return [
                    new Siesta.Launcher.Runner.WebDriverNodeJS.BrowserStack({
                        launcher    : this
                    })
                ]
            
            if (options.sl) 
                return [
                    new Siesta.Launcher.Runner.WebDriverNodeJS.SauceLabs({
                        launcher    : this
                    })
                ]
                
            return [
                new Siesta.Launcher.Runner.WebDriverNodeJS({
                    launcher    : this
                })
            ]
        },
        
        
        printVersion : function () {
            this.SUPERARG(arguments)
            
            var json    = require('selenium-webdriver/package.json')
            
            this.print("Selenium : " + json.version)
            this.print("NodeJS   : " + process.version)
        },
        
        
        buildHarnessUrlQueryParams : function () {
            return Joose.O.extend(this.SUPER(), {
                selenium        : true
            })
        },
        
        
        destroy : function () {
            try {
                // some garbage output from chrome/chromedriver on windows
                require('fs').unlinkSync('debug.log')
            } catch (e) {
            }
            
            return this.SUPER()
        },
        
        
        setup : function () {
            if (!this.options.host) {
                try {
                    require('child_process').spawnSync(
                        'java',
                        [
                            '-cp', this.normalizePath(this.binDir + 'binary/rhino/js.jar'),
                            'org.mozilla.javascript.tools.shell.Main',
                            '-e', "try { var robot = new java.awt.Robot(); robot.mouseMove(0, 0) } catch (e) {}"
                        ]
                    )
                } catch (e) {}
            }
            
            this.setChromeDriverServerPath()
            this.setIEDriverServerPath()
            
            return this.SUPER()
        }
    }
    // eof methods
})

/* footer */
}()