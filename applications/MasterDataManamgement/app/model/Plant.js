Ext.define('MAM.model.Plant', {
    extend: 'Ext.data.Model',
    requires: [
        'MAM.model.Relation'
    ],
    idProperty: 'id',
    fields: [
        { name: 'id', type: 'int', mapping: 'plantId'},
        { name: 'customId', type: 'string'},
        { name: 'type', type: 'string'},
        { name: 'model', type: 'string'},
        { name: 'description', type: 'string'},
        { name: 'manufacturer', type: 'string'},
        { name: 'primaryEnergy', type: 'string'},
        { name: 'active', type: 'boolean'},

        { name: 'startOfOperation', convert: MAM.utils.model.Converts.convertDate}
    ]
});