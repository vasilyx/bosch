Ext.define('MAM.view.person.ListModel', {
    extend: 'Ext.app.ViewModel',
    requires: ['MAM.model.Person', 'MAM.store.Persons'],
    
    alias: 'viewmodel.personListModel',
    
    data: {
        title: 'Person Liste'
    },
    stores: {
        persons: {
           type: 'persons'
        }
    }
});