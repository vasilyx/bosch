Ext.define('MAM.store.MeterPoints', {
    extend: 'Ext.data.Store',
    alias: 'store.meterPoints',
    storeId: 'meterPoints',
    model: 'MAM.model.MeterPoint',
    autoLoad: false,
    pageSize: MAM.Config.List.limit,
    proxy: {
        type: 'ajax',
        url: '/MAM/' + MAM.Config.Backend.Versions + '/meterPoints',
        startParam: 'skip',
        limitParam: 'top',
        extraParams: {
            expand: 'properties',
            totalNumber: true
        },
        headers: {
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty: 'meterPoints',
            totalProperty: 'totalNumber'
        }
    }
});