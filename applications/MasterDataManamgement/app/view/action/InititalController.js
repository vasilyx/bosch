Ext.define('MAM.view.action.InitialController', {
    extend: 'Ext.app.ViewController',
    requires: [
        'Energy.mom.*'
    ],
    alias: 'controller.actionInitialController',
    currentRecord: null,
    listen: {
        controller: {
            '*': {
                'detailsLoaded': 'onDetailsLoaded',
                'openEntity': 'setEntity',
                'setIconSaveStatus': 'setSave',
                'setIconEditStatus': 'setEdit'
            }
        }
    },
    control: {
        'menuitem': {
            click: 'onClickMenuItem'
        },
        'button[cls=icon-create]': {
            click: 'onClickCreate'
        },
        'button[cls=icon-delete]': {
            click: 'onClickDelete'
        },
        'button[cls=icon-edit]': {
            click: 'onClickEdit'
        },
        'button[cls=icon-save]': {
            click: 'onClickSave'
        }
    },

    setEntity:function(entity, record) {
        this.currentRecord = record;
    },

    onClickMenuItem: function( menuitem ) {

        if (!menuitem.hasOwnProperty('action')) return false;

        var window = Ext.create('MAM.view.window.ActionWindow', {
            title: menuitem.text,
            items:  {
                        xtype: menuitem.action,
                        viewModel: {
                            data: {
                                record: this.currentRecord
                            }
                        }
                    }
        });

        window.show();
    },

    onDetailsLoaded: function(entity) {
        if (this.getViewModel().get('entity') !== entity) return;
        this.setIconsInDetailsMode();
    },

    setIconsInDetailsMode: function() {
        Ext.each(this.getView().getDockedItems()[0].items.items, function( item ) {
            if (item.onlyEditMode === true && (!item.hasOwnProperty('menu') || item.getMenu().items.length)) {
                item.setDisabled(false);
            }

            if(item.onlyEditMode === false) {
                item.setDisabled(true);
            }
        });
    },

    onClickCreate: function() {
        var window = Ext.create('MAM.view.window.ActionWindow', {
            title: _tl.get('mam.label.actionPanel.create.' + this.getViewModel().get('entity')),
            items:  {
                xtype: this.getViewModel().get('entity') + 'CreateDetail'
            }
        });

        window.show();
    },

    onClickDelete: function() {
        this.fireEvent(this.getViewModel().get('entity') + 'Delete', this.currentRecord);
    },

    onClickEdit: function() {

        var cls = 'editMode';

        if (this.getViewModel().get('editMode') === true) {
            this.fireEvent(this.getViewModel().get('entity') + 'Edit', false);
            this.getViewModel().set('editMode', false);
            this.getView().down('button[cls=icon-edit]').removeCls(cls);
            this.getView().down('button[cls=icon-edit]').setText( _tl.get('mam.label.actionPanel.edit'));
            this.setSave(this.getViewModel().get('entity'), false);

        } else {
            this.fireEvent(this.getViewModel().get('entity') + 'Edit');
            this.getViewModel().set('editMode', true);
            this.getView().down('button[cls=icon-edit]').addCls(cls);
            this.getView().down('button[cls=icon-edit]').setText( _tl.get('mam.label.actionPanel.cancel'));
        }
    },

    onClickSave: function() {
        this.fireEvent(this.getViewModel().get('entity') + 'Save');
    },

    /**
     *
     * @param entity
     * @param editMode, true if current mode is Edit
     */
    setEdit: function(entity, editMode) {
        if (this.getViewModel().get('entity') !== entity) return;

        this.getViewModel().set('editMode', editMode);
        this.onClickEdit();
    },

    /**
     *
     * @param entity
     * @param isDirty
     */
    setSave: function(entity, isDirty) {
        if (this.getViewModel().get('entity') !== entity) return;

        var saveButton = this.getView().down('button[cls=icon-save]');

        if (isDirty) {
            saveButton.setDisabled(false);
        } else {
            saveButton.setDisabled(true);

        }
    }
});