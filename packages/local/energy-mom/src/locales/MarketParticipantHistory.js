Ext.define("Energy.mom.locales.MarketParticipantHistory", {
    extend: 'Energy.common.language.TranslationPackage',
    alternateClassName: ['LangMarketParticipantHistory'],
    statics: {
        en: {
            save: 'Save',
            cancel: 'Cancel',
            meterPoint: 'Meter Point',
            dateFrom: 'Date from',
            dateTo: 'Date to',
            role: 'Role',
            name: 'Name',
            gln: 'GLN',
            gridArea: 'Grid Area',
            accountingArea: 'Accounting Area',
            comment: 'Comment',
            unlimited: 'Unlimited',
            close: 'Close',
            emptyText: 'No market participants history availalbe.'
        },
        de: {
            save: 'Speichern',
            cancel: 'Abbrechen',
            meterPoint: 'Zählpunkt',
            dateFrom: 'Datum von',
            dateTo: 'Datum bis',
            role: 'Rolle',
            name: 'Name',
            gln: 'GLN',
            gridArea: 'Netzgebiet',
            accountingArea: 'Bilanzierungsgebiet',
            comment: 'Bemerkung',
            unlimited: 'Unbegrenzt',
            close: 'Schließen',
            emptyText: 'Keine Historie vorhanden.'
        }
    }
});