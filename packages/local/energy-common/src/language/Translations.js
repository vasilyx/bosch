/*jsl:option explicit*/
Ext.define('Energy.common.language.Translations', {
	
	singleton: true,
	requires: ['Energy.common.language.Locale'],

	// Attributes:

	locale: null,
	loaded: false,
	translations: null,
	url: null,
	
	// Methods:
	load: function() {
		var me = this;

		for(var i = 0, locales = [Energy.common.language.Locale.getLocale(), Energy.common.language.Locale.getLanguage(), 'de']; i < locales.length; ++i) {
			var locale = locales[i],
				translations = me.loadTranslations(locale, this.url);

			if(translations) {
				me.locale = locale;
				me.loaded = true;
				me.translations = translations;

				return true;
			}
		}

		return false;
	},

	loadTranslations: function(locale, url) {
		var me = this,
			translations = null;

		Ext.log({
			msg: 'Loading translations for locale "' + locale + '"...'
		});
		
		Ext.Ajax.request({
			async: false,
			method: 'GET',
			url: url,
			failure: function(response) {
				Ext.log({
					level: 'error',
					msg: 'Loading translations for locale "' + locale + '" failed'
				});
			},
			success: function(response){
				translations = Ext.JSON.decode(response.responseText);
				
				if(Ext.isObject(translations)) {
					Ext.log({
						msg: 'Translations for locale "' + locale + '" loaded'
					});
				} else {
					translations = null;

					Ext.log({
						level: 'error',
						msg: 'Translations for locale "' + locale + '" are not an object'
					});
				}
			}
		});
		
		return translations;
	},

	get: function(constant, defaultValue) {
		var me = this;

		// Dependency management by lazy loading:
		if((me.loaded === false && me.load() === false) || me.translations === null) {
			return defaultValue === undefined ? constant : defaultValue;
		}

		if(constant in me.translations) {
			return me.translations[constant];
		}
		
		Ext.log({
			level: 'warn',
			msg: 'For locale "' + me.locale + '" constant "' + constant + '" is missing'
		});
        //return undefined if no translation found
		return defaultValue === undefined ? undefined : defaultValue;
	}
});
