Ext.define('MAM.view.meterPoint.Detail', {
    extend: 'Ext.Panel',

    alias: 'widget.meterPointDetail',


    bind: {
        data: '{meterPoint}'
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    scrollable: true,
    
    initComponent: function () {

        var defaults = {
            xtype: 'displayfield',
            labelWidth: 150
        };

        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if(this.responsiveConfig){
            responsiveConfig = this.responsiveConfig;
        }

        var items = [
            {
                xtype: 'container',
                cls: 'bg-grey',
                margin: 10,
                padding: '5 10 0 10',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                defaults: defaults,
                items: this.getNorthItems()

            },
            {
                xtype: 'container',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                padding: '0 20 0 20',
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getWestItems()
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getEastItems()
                    }
                ]
            }
        ];

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    },
    /*
     * Defines the top area items for connection building properties
     */
    getNorthItems: function(){

        var items = [
            {
                fieldLabel: _tl.get('mam.label.meterPoint.customId'),
                name: 'customId',
                flex: 1,
                bind: {
                    value: '{meterPoint.customId}'
                }
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                flex: 1,
                items: [
                    //TODO: New MAM-xxx ticket
                    //we need to get meter point via versions API
                    //GET v2/meterPoints/{id}/versions?expand=all&activeAt={YYYY-MM-dd'T'HH:mm:ss.SSZ}
                    {
                        fieldLabel: _tl.get('mam.label.meterPoint.createdTimeStamp'),
                        flex: 1,
                        name: 'createdTimeStamp',
                        bind: {
                            value: '{meterPoint.createdTimeStamp}'
                        }
                    },
                    {
                        fieldLabel: _tl.get('mam.label.meterPoint.id'),
                        name: 'meterPointId',
                        bind: {
                            value: '{meterPoint.id}'
                        }
                    }
                ]
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the left side of panel
     */
    getWestItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.meterPoint.medium'),
                name: 'medium',
                bind: {
                    value: '{medium}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.measuringType'),
                name: 'measuringType',
                bind: {
                    value: '{meterPoint.measuringType}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.measuringProduct'),
                name: 'measuringProduct',
                bind: {
                    value: '{meterPoint.measuringProduct}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.transformRatio'),
                name: 'transformRatio',
                bind: {
                    value: '{meterPoint.transformRatio}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.hindrance'),
                name: 'hindrance',
                bind: {
                    value: '{meterPoint.hindrance}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.hindranceReason'),
                name: 'hindranceReason',
                bind: {
                    value: '{meterPoint.hindranceReason}'
                }
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the right side of panel
     */
    getEastItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionUnit'),
                name: 'consumptionUnit',
                bind: {
                    value: '{meterPoint.consumptionUnit}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionForecast'),
                name: 'consumptionForecast',
                bind: {
                    value: '{meterPoint.consumptionForecast}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionAverage'),
                name: 'consumptionAverage',
                bind: {
                    value: '{meterPoint.consumptionAverage}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionYear1'),
                name: 'consumptionYear1',
                bind: {
                    value: '{meterPoint.consumptionYear1}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionYear2'),
                name: 'consumptionYear2',
                bind: {
                    value: '{meterPoint.consumptionYear2}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionYear3'),
                name: 'consumptionYear3',
                bind: {
                    value: '{meterPoint.consumptionYear3}'
                }
            }
        ];
        return items;
    }
});