Ext.define('MAM.view.connectionBuilding.Detail', {
    extend: 'Ext.Panel',

    alias: 'widget.connectionBuildingDetail',

    bind: {
        data: '{connectionBuilding}'
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    scrollable: true,
    
    initComponent: function () {

        var defaults = {
            xtype: 'displayfield',
            labelWidth: 150
        };

        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if(this.responsiveConfig){
            responsiveConfig = this.responsiveConfig;
        }

        var items = [
            {
                xtype: 'container',
                cls: 'bg-grey',
                margin: 10,
                padding: '5 10 0 10',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                defaults: defaults,
                items: this.getNorthItems()

            },
            {
                xtype: 'container',
                padding: '0 20 0 20',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getWestItems()
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getEastItems()
                    }
                ]
            }
        ];

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    },
    /*
     * Defines the top area items for connection building properties
     */
    getNorthItems: function(){
        var items = [
            {
                xtype: 'container',
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                flex: 1,
                items: [
                    {
                        fieldLabel: _tl.get('mam.label.connectionBuilding.customId'),
                        name: 'customId',
                        flex: 1,
                        bind: {
                            value: '{connectionBuilding.customId}'
                        }
                    },
                    {
                        fieldLabel: _tl.get('mam.label.connectionBuilding.name'),
                        name: 'name',
                        flex: 1,
                        bind: {
                            value: '{connectionBuilding.name}'
                        }
                    }
                ]
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                flex: 1,
                items: [
                    {
                        fieldLabel: _tl.get('mam.label.connectionBuilding.createdTimeStamp'),
                        flex: 1,
                        name: 'createdTimeStamp',
                        bind: {
                            value: '{connectionBuilding.createdTimeStamp}'
                        }
                    },
                    {
                        fieldLabel: _tl.get('mam.label.connectionBuilding.id'),
                        name: 'connectionBuildingId',
                        bind: {
                            value: '{connectionBuilding.id}'
                        }
                    }
                ]
            }
        ];
        return items;
    },

    /**
     * Defines the area items for gatway properties
     * on the left side of panel
     */
    getWestItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.connectionBuilding.note'),
                name: 'note',
                bind: {
                    value: '{connectionBuilding.note}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.connectionBuilding.streetAndNumber'),
                name: 'streetAndNumber',
                bind: {
                    value: '{connectionBuilding.street} {connectionBuilding.houseNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.connectionBuilding.houseNumberAppendix'),
                name: 'houseNumberAppendix',
                bind: {
                    value: '{connectionBuilding.houseNumberAppendix}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.connectionBuilding.postalCodeAndCity'),
                name: 'postalCodeAndCity',
                bind: {
                    value: '{person.postalCode} {person.city}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.connectionBuilding.country'),
                name: 'country',
                bind: {
                    value: '{connectionBuilding.country}'
                }
            }

        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the right side of panel
     */
    getEastItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.connectionBuilding.municipalityKey'),
                name: 'municipalityKey',
                bind: {
                    value: '{connectionBuilding.municipalityKey}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.connectionBuilding.latitude'),
                name: 'latitude',
                bind: {
                    value: '{connectionBuilding.latitude}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.connectionBuilding.longitude'),
                name: 'longitude',
                bind: {
                    value: '{connectionBuilding.longitude}'
                }
            }

        ];
        return items;
    }
});