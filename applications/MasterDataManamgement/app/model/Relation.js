Ext.define('MAM.model.Relation', {
    extend: 'Ext.data.Model',
    requires: [
        'MAM.utils.data.proxy.MamRest',
        'MAM.utils.Validation'
    ],
    fields: [{
        name: 'hasRelations', mapping: 'relations', convert: function( value ){
            if(value === undefined){
                return false;
            }else if( MAM.utils.Validation.isObjectEmpty(value)){
                return false;
            }else if( value.connectionBuildings === null){ //TODO: This check is made because of MAM bug: mam returns always connectionBuildings with null value
                //check if there are other connections
                var result = false;
                for(var objName in value) {
                    if(value.hasOwnProperty(objName)) {
                        if(value[objName] && value[objName] !== 'connectionBuildings'){
                            result = true;
                        }
                    }
                }

                return result;
            }
            else{
                return true;
            }
        }
    }],
    hasMany: [
        { name: 'meters',  associationKey: 'relations.meters', model: 'MAM.model.MeterDetail' },
        { name: 'gateways',  associationKey: 'relations.gateways', model: 'MAM.model.GatewayDetail' },
        { name: 'meterPoints',  associationKey: 'relations.meterPoints', model: 'MAM.model.MeterPointDetail' },
        { name: 'persons',  associationKey: 'relations.persons', model: 'MAM.model.PersonDetail' },
        { name: 'connectionBuildings',  associationKey: 'relations.connectionBuildings', model: 'MAM.model.ConnectionBuildingDetail' },
        { name: 'marketParticipants',  associationKey: 'relations.marketParticipants', model: 'MAM.model.MarketParticipantDetail' },
        { name: 'plants',  associationKey: 'relations.plants', model: 'MAM.model.PlantDetail' },
        { name: 'simCards',  associationKey: 'relations.simCards', model: 'MAM.model.SimCardDetail' },
        { name: 'transformers',  associationKey: 'relations.transformers', model: 'MAM.model.TransformerDetail' },
        { name: 'meterAddonDevices',  associationKey: 'relations.meterAddonDevices', model: 'MAM.model.MeterAddonDeviceDetail' },
        { name: 'attachments',  associationKey: 'relations.attachments', model: 'MAM.model.AttachmentDetail' }

    ],
    proxy: {
        type: 'mamrest',
        appendId: false,
        headers: {
            'Accept': 'application/json'
        },
        api: {
            create: '/MAM/' + MAM.Config.Backend.Versions + '/relations',
            read: '/MAM/' + MAM.Config.Backend.Versions + '/relations?compType={entity}&compId={id}',
            update: undefined,
            destroy: '/MAM/' + MAM.Config.Backend.Versions + '/relations?compType={entity}&compId={id}'
        },
        actionMethods: {
            create: 'POST',
            update: 'POST',
            read: 'GET',
            destroy: 'DELETE'
        },
        reader: {
            type: 'json',
            transform: {
                fn: function(data) {
                    var newData = { };
                    Ext.each(data.relations, function(item){
                        var obj = {};
                        obj[item.associatedCompType + 'Id'] = item.associatedCompId;
                        Ext.apply(obj, item);
                        var entity = item.associatedCompType+'s';
                        if (!newData.hasOwnProperty(entity)) {
                            newData[entity] = [];
                        }
                        newData[entity].push(obj);
                    });

                    return {relations: newData};
                },
                scope: this
            }
        },
        extraParams: {
            expand: 'none'
        }
    }
});