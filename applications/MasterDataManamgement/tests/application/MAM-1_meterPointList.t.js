StartTest( function(t) {

   var list = Ext.create('TestApplication.application.AbstractList', {t: t, name: 'meterPoint'});

   MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/l/details_1.json');
   MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/l/details_1491165.json');
   //dummy only to mock data and don't get error from browser
   MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/customAttributes_DE0700081632100000000000070238000.json', 1);
   MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/customAttributes_DE0700081632100000000000070238000.json', 1491165);
   MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/relations_DE0700081632100000000000070238000.json', 1); // for related
   MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/relations_DE0700081632100000000000070238000.json', 1491165); // for related
   MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/r/details_1494726.json'); // for related
   MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/r/details_1480156.json'); // for related
   MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/r/details_1035879.json'); // for related
   //end dummy

   list.testListViewArea();
   list.testFilledDataInList(16);
   list.testPageCorrect();
   list.testDetailArea();
   list.testFilterById('DE0700081632100000000000070237967');
   list.testFilterEmpty();
   list.testSelectRow(1); // select second row
   list.testSelectRow(4, 8);
});
