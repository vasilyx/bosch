StartTest( function(t) {

    var list = Ext.create('TestApplication.application.AbstractList', {t: t, name: 'connectionBuilding'});

    MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/l/details_1034414.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/l/details_1034473.json');
    //dummy only to mock data and don't get error from browser
    MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/l/customAttributes_dummy.json', 1034414);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/l/relations_dummy.json', 1034414);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/l/customAttributes_dummy.json', 1034473);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/l/relations_dummy.json', 1034473);

    list.testListViewArea();
    list.testFilledDataInList(23);
    list.testPageCorrect();
    list.testDetailArea();
    list.testFilterById('81095996');
    list.testFilterEmpty();
    list.testSelectRow(1); // select second row
    list.testSelectRow(4, 8);
});