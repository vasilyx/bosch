StartTest(function (t) {

    var list = Ext.create('TestApplication.application.AbstractList', {t: t, name: 'person'});

    var referenceData = {
        customId: 75370251,
        personId: 1477917,
        name: 'Birte',
        streetAndNumber: 'Alexanderstr. 70',
        postalCodeAndCity: '12345 Musterort',
        emailAddress: 'mustername@eMail.de'
    };

    list.testListViewArea();

    t.it('#2 Enter full entity id into filter filed', function(t) {
        t.chain(
            function(next) {
                MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/details_75370251.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/customAttributes_75370251.json', referenceData.personId);
                next();
            },
            { doubleclick : t.getFirstRow(t.cq1('personInitial gridpanel')) },

        { waitForCQ : 'personDetail' },
        { waitForComponentVisible : 'personDetail'},
            {
                action      : 'wait',
                delay       : 1500      // 1500 ms
            },
        function(next) {

            t.cqExists('personDetail', 'person Detail view exist');

            for (var code in referenceData) {
                t.contentLike(t.cq1('personDetail displayfield[name="' + code + '"]'), referenceData[code], 'Found right  #' + code + ' ' + referenceData[code]);
            }
        }
        )
    });

    t.it('#3 Check person Sub Tabs', function (t) {
        t.chain(
            {
                waitForCQ: 'personSubDetail'
            }, function (next) {
                t.cqExists('personSubDetail', 'person Detail Tabs view exist');
                t.elementIsVisible(t.cq1('customAttributesInitial'), 'Tab "Custom Attributes of the Person" is visible');
                t.expect(t.cq1('personSubDetail').items.length).toBe(1);
            }
        );
    });
});