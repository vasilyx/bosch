StartTest(function (t) {

    var referenceData = {
        id: 1491165,
        customId: 'DE0700081632100000000000070237964'
    };

    // f
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/l/details_1.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/l/customAttributes_dummy.json', 1);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/l/relations_dummy.json', 1);

    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/l/details_' + referenceData.id + '.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/l/customAttributes_dummy.json', referenceData.id);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/l/relations_dummy.json', referenceData.id);


    t.it('#1 Click on delete icon - response 409 - warning window', function (t) {
        t.chain(
            {waitForCQ: 'app-main tabbar'},
            {click: 'tabbar=> span.icon-meterPoint'},
            function (next) {
                t.doubleClick(t.compositeQuery('meterPointInitial gridpanel => .x-grid-cell')[1]); // for history
                next();
            }, {
                action: 'wait',
                delay: 1000
            },
            function (next) {
                t.doubleClick(t.compositeQuery('meterPointInitial gridpanel => .x-grid-cell')[4]);
                next();
            },
            {
                action: 'wait',
                delay: 1000
            },
            function (next) {
                t.click(t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-delete]'));
                MOCK_DATA.setMockedResponse('meterPoints/' + referenceData.id, 'delete', 409);
                t.elementIsVisible(t.cq1('commonUiDialogBox'), 'DialogBox is visible');
                t.click(t.cq1('commonUiDialogBox button[text=' + _tl.get('mam.dialogbox.meterPoint.button.yes') + ']'));
                next();
            },
            {
                action: 'wait',
                delay: 1000
            },
            function (next) {
                t.elementIsVisible(t.compositeQuery('commonUiDialogBox => div.icon.warning'), 'Warning is visible');

                t.like(
                    t.compositeQuery('commonUiDialogBox => div.content div div')[0].innerHTML,
                    Ext.String.format(_tl.get('mam.dialogbox.meterPoint.delete.response409'), referenceData.customId),
                    '409 message is right'
                );
                t.click(t.cq1('commonUiDialogBox button'));
            }
        )
    });


    t.it('#2 Click on delete icon - response 404 - warning window', function (t) {
        t.chain(
            function (next) {
                // check Warning then close

                t.click(t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-delete]'));
                MOCK_DATA.setMockedResponse('meterPoints/' + referenceData.id, 'delete', 404);
                t.elementIsVisible(t.cq1('commonUiDialogBox'), 'DialogBox is visible');
                t.click(t.cq1('commonUiDialogBox button[text=' + _tl.get('mam.dialogbox.meterPoint.button.yes') + ']'));

                next();
            },
            {
                action: 'wait',
                delay: 1000
            },
            function (next) {
                t.elementIsVisible(t.compositeQuery('commonUiDialogBox => div.icon.warning'), 'Warning is visible');
                t.like(
                    t.compositeQuery('commonUiDialogBox => div.content div div')[0].innerHTML,
                    Ext.String.format(_tl.get('mam.dialogbox.meterPoint.delete.response404'), referenceData.customId),
                    '404 message is right'
                );

                t.click(t.cq1('commonUiDialogBox button'));
            }
        )
    });

    t.it('#3 Click on delete icon - response 200 - success window', function (t) {
        t.chain(
            function (next) {
                t.click(t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-delete]'));
                MOCK_DATA.setMockedResponse('meterPoints/' + referenceData.id, 'delete', 200);
                t.elementIsVisible(t.cq1('commonUiDialogBox'), 'DialogBox is visible');
                t.click(t.cq1('commonUiDialogBox button[text=' + _tl.get('mam.dialogbox.meterPoint.button.yes') + ']'));

                next();
            },
            {
                action: 'wait',
                delay: 1000
            },
            function (next) {
                t.elementIsVisible(t.compositeQuery('commonUiDialogBox => div.icon.success'), 'Success is visible');
                t.click(t.cq1('commonUiDialogBox button'));
            }
        )
    });

    t.it('#4 Check history and main area', function (t) {
        t.chain(
            function (next) {
                var historyMenu = t.cq1('meterPointInitial historyInitial').getMenu();
                var i = 0;
                historyMenu.items.each(function( item ) {

                    if (item.text == referenceData.customId && item.iconCls == 'submenu-icon-meterPoint') {
                        i++;
                    }
                });

                t.expect(i).toBe(0);
                t.contentNotLike(
                    t.compositeQuery('meterPointInitial commonUiHeader  => div.content.bg-green span.header')[0],
                    referenceData.customId, 'Header without customId'
                );

                t.expect(t.cq1('meterPointDetail')).toBe(undefined);
            }
        )
    });
});
