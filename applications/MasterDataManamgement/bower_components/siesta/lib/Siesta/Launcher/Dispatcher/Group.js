/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Siesta.Launcher.Dispatcher.Group', {
    
    has : {
        elementsById        : Joose.I.Object,
        elements            : Joose.I.Array,
        
        elementsByDescId    : Joose.I.Object,
        
        done                : 0,
        
        maxProcessedCount   : 2
    },
    
    
    methods : {
        
        initialize: function () {
            var elementsByDescId    = this.elementsByDescId
            var elementsById        = this.elementsById
            var elements            = this.elements
            
            var me                  = this
            
            Joose.A.each(elements, function (element, index) {
                if (!(element instanceof Siesta.Launcher.Dispatcher.Element))
                    elements[ index ] = element = new Siesta.Launcher.Dispatcher.Element({
                        descId              : element,
                        belongsTo           : me,
                        maxProcessedCount   : me.maxProcessedCount
                    })
                    
                elementsById[ element.id ] = element
                elementsByDescId[ element.descId ] = element
            })
        },
        
        
        fetchNSpareElements : function (n, asJSON) {
            if (this.allDone()) return null
            
            var els     = []
            
            Joose.A.each(this.elements, function (element, index) {
                if (!element.inProgress && !element.processed) {
                    els.push(asJSON ? element.asJSON() : element)
                    
                    element.startProgress()
                }
                    
                if (els.length == n) return false
            })
            
            return els.length ? els : null
        },
        
        
        getElementById : function (id) {
            return this.elementsById[ id ] || null
        },
        
        
        getElementByDescId : function (id) {
            return this.elementsByDescId[ id ] || null
        },
        
        
        forEachElement : function (func, scope) {
            return Joose.A.each(this.elements, func, scope)
        },
        
        
        allDone : function () {
            return this.done == this.elements.length
        }
    }
})
