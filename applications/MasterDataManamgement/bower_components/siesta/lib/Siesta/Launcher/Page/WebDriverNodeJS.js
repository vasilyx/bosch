/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
!function () {
    
var fs              = require('fs')
var fse             = require('fs-extra')
var child_process   = require('child_process');

Class('Siesta.Launcher.Page.WebDriverNodeJS', {
    
    isa         : Siesta.Launcher.Page.BasePage,
    
    has : {
        driver                  : { required : true },
        
        similarityThreshold     : 0.9
    },
    
    
    methods : {
        
        buildSessionId : function () {
            return String(this.driver.session_.value_.getId())
        },
        
        
        pageShouldNotBeUsedAfterException : function (e) {
            return /Request timeout/.test(e)
        },
        
        // promised method
        runImageMagickCommand : function (command, argv, acceptNonZeroExitCode) {
            var me              = this
            var launcher        = this.launcher
            
            return new Promise(function (callback, errback) {
                child_process.execFile(
                    launcher.binDir + '/binary/imagemagick/' + launcher.getPlatformId() + '/' + command + (launcher.isWindows ? '.exe' : ''), 
                    argv,
                    {},
                    function (error, stdout, stderr) {
                        var output      = "STDOUT:\n" + (stdout || '') + "\nSTDERR:\n" + (stderr || '')
                        var exitCode    = error == null ? 0 : error.code
                        
                        me.debug("IMGCK command: " + command + ', exitCode: ' + exitCode + ', args:  ' + argv.join(' ') + ', output: ' + output)
                        
                        if (error == null || acceptNonZeroExitCode)
                            callback({
                                exitCode        : exitCode,
                                stdout          : stdout,
                                stderr          : stderr
                            })
                        else
                            errback({
                                exitCode        : exitCode,
                                stdout          : stdout,
                                stderr          : stderr
                            })
                    }
                )
            })
        },
        
        
        // promised method
        extentImage : function (fileName, width, height) {
            // ssi - siesta screenshot image
            var tempFileName    = require('tmp').fileSync({ prefix : 'ssi' }).name
            
            return this.runImageMagickCommand('convert', [ 
                '-background', 'none',
                '-gravity', 'NorthWest',
                '-extent', width + 'x' + height,
                fileName,
                tempFileName
            ]).then(function () {
                return tempFileName
            })    
        },
        
        
        // promised method
        cropImage : function (fileName, x, y, width, height) {
            // ssi - siesta screenshot image
            var tempFileName    = require('tmp').fileSync({ prefix : 'ssi' }).name
            
            return this.runImageMagickCommand('convert', [ 
                '-extract', width + 'x' + height + '+' + x + '+' + y,
                fileName,
                tempFileName
            ]).then(function () {
                return tempFileName
            })    
        },
        
        
        // promised method
        getPNGImageSize : function (fileName) {
            var PNG         = require('pngjs').PNG;
            
            return new Promise(function (resolve, reject) {
                var png     = new PNG({})
                
                png.on('metadata', function (metadata) {
                    resolve({
                        width       : metadata.width,
                        height      : metadata.height
                    })
                })
                
                png.on('error', function (error) {
                    reject(error)
                })
                
                fs.createReadStream(fileName).pipe(png)
            })
            
//            return this.runImageMagickCommand('identify', [ 
//                fileName,
//                '-format', '%w %h'
//            ]).then(function (res) {
//                var match       = /(\d+) (\d+)/.exec(res.stdout)
//                
//                return {
//                    width       : Number(match[ 1 ]),
//                    height      : Number(match[ 2 ])
//                }
//            })    
        },
        
        
        // promised method
        equalizeImageSizes : function (fileName1, fileName2) {
            var me      = this
            
            return Promise.all([ this.getPNGImageSize(fileName1), this.getPNGImageSize(fileName1) ]).then(function (results) {
                var image1      = results[ 0 ]
                var image2      = results[ 1 ]
                
                if (image1 instanceof Error || image2 instanceof Error) throw new Error("Something went wrong") 
                
                var maxWidth    = Math.max(image1.width, image2.width)
                var maxHeight   = Math.max(image1.height, image2.height)
                
                if (
                    image1.width != maxWidth || image2.width != maxWidth 
                    || image1.height != maxHeight|| image2.height != maxHeight
                ) {
                    return Promise.all([
                        this.extentImage(fileName1, maxWidth, maxHeight),
                        this.extentImage(fileName2, maxWidth, maxHeight)
                    ]).then(function (results) {
                        return {
                            image1      : results[ 0 ],
                            image2      : results[ 1 ]
                        }
                    })
                } else 
                    return {
                        image1      : fileName1,
                        image2      : fileName2
                    }
            })
            
        },
        
        
        // promised method
        compareImages : function (image1, image2, diffImage, options) {
            var me          = this
            
            return this.equalizeImageSizes(image1, image2).then(function (files) {
                options     = Joose.O.extend(me.dispatcher.screenshotCompareConfig || {
                    fuzz        : '5%',
                    metric      : 'NCC',
                    compose     : 'src-over',
                    'highlight-color'   : '#e500f4'
                }, options || {})
                
                var argv        = []
                
                Joose.O.each(options, function (value, name) {
                    argv.push('-' + name)
                    argv.push(value)
                })
                
                argv.push(files.image1, files.image2, diffImage)
                
                return me.runImageMagickCommand('compare', argv, true).then(function (res) {
                    res.similar     = res.exitCode != 2 && Number(res.stderr) > me.similarityThreshold
                    res.similarity  = Number(res.stderr)
                    
                    return res
                })
            })
        },
        
        
        // promised method
        scaleImage : function (buffImage, scale) {
            throw "YOYO"
            
//            var scaleTransform      = java.awt.geom.AffineTransform.getScaleInstance(scale, scale)
//            
//            var scaleTransformOp    = new java.awt.image.AffineTransformOp(scaleTransform, java.awt.image.AffineTransformOp.TYPE_BICUBIC)
//            
//            var newWidth            = Math.ceil(buffImage.getWidth() * scale)
//            var newHeight           = Math.ceil(buffImage.getHeight() * scale)
//            
//            return scaleTransformOp.filter(buffImage, new java.awt.image.BufferedImage(newWidth, newHeight, buffImage.getType()))
        },
        
        
        // promised method
        screenshot : function (command) {
            var me              = this
            var options         = this.dispatcher.options
            var launcher        = this.launcher
            var baseFileName    = launcher.normalizePath(command.fileName.replace(/(\.png)?$/, '.png'))
            var baseDir         = options[ 'screenshot-dir' ]
            var path            = require('path')
            
            // String() to convert native Java string to JS string
            var fileName        = baseDir ? path.join(baseDir, baseFileName) : baseFileName

            if (!launcher.ensurePathExists(fileName)) {
                return Promise.resolve({ success : false, error : "Can't create target directory for file: " + fileName })
            }
            
            var tempFile        = require('tmp').fileSync({ prefix : 'ssi' }).name
            
            return this.driver.takeScreenshot().then(function (screenshot) {
                fs.writeFileSync(tempFile, screenshot, 'base64')
                
                return me.getPNGImageSize(tempFile)
            }).then(function (size) {
                var k = 1
                
                return me.cropImage(
                    tempFile, 
                    command.x * k, 
                    command.y * k, 
                    Math.min(size.width - command.x * k, command.width * k), 
                    Math.min(size.height - command.y * k, command.height * k)
                )
            }).then(function (croppedFileName) {
                var compareWithPrevious = Boolean(options[ 'screenshot-compare-with-previous' ])
                var compareWithBase     = options[ 'screenshot-compare-with-base' ]
                
                if (!command.skipCompare && (compareWithPrevious && launcher.fileExists(fileName) || compareWithBase)) {
                    var compareToFile
                    
                    if (compareWithPrevious) {
                        compareToFile   = fileName.replace(/\.png$/, '-prev.png')
                        
                        if (launcher.fileExists(compareToFile)) launcher.deleteFile(compareToFile)
                        
                        fs.renameSync(fileName, compareToFile)
                    } 
    
                    fse.copySync(croppedFileName, fileName, { clobber : true })
                    
                    if (compareWithBase) {
                        compareToFile   = path.join(compareWithBase, baseFileName)
                        
                        if (!launcher.fileExists(compareToFile)) return {
                            success     : false,
                            error       : 'Base file `' + path.resolve(compareToFile) + '` does not exists'
                        }
                    }
                    
                    var diffFileName    = fileName.replace(/\.png$/, '-diff.png')
                    
                    return me.compareImages(
                        path.resolve(fileName), path.resolve(compareToFile), path.resolve(diffFileName), command.cfg
                    ).then(function (res) {
                        return res && res.exitCode != 2 ? {
                            success         : true,
                            compare         : compareWithPrevious ? 'previous' : 'base',
                            similar         : res.similar,
                            similarity      : res.similarity,
                            newScreenshot   : path.resolve(fileName),
                            oldScreenshot   : path.resolve(compareToFile)
                        } : {
                            success     : false,
                            error       : 'Error while comparing the images: ' + res.output
                        }
                    })
                    
                } else {
                    fse.copySync(croppedFileName, fileName, { clobber : true })
                    
                    return { success : true, compare : false }
                }
            })
        },
        
        
        shouldStopScriptExecution : function () {
            return SHUTDOWN
        },
        
        
        open : function (url, callback) {
            var driver      = this.driver
            
            // setPosition also updates window size (on MacOS) for some reason, must be done before setSize
            if (this.launcher.isMacOS)
                driver.manage().window().setPosition(1, 1);
                
            // setSize moves the window in the top left corner in SauceLabs, which we don't want
            driver.manage().window().setSize(this.width - 1, this.height - 1);
            
            if (!this.launcher.isMacOS)
                driver.manage().window().setPosition(1, 1);

            driver.get(url).then(function () {
                callback()
            }, function (e) {
                callback(e)
            })
        },
            
            
        close : function () {
            this.SUPER()
            
            // IE driver as anything related to IE sporadically throws exceptions when closing the browser.. 
            try {
                // seems webdriver queues the "driver.quit()" calls and subsequent calls to page creation are delayed
                // so trying to avoid that
                var promise = Promise.resolve(this.driver.quit()).then(function () {
                    return Promise.resolve()
                }, function () {
                    return Promise.resolve()
                })
            } catch (e) {
                this.printError("Sync error while closing page: " + e)
            }
            
            this.driver     = null
            
            return promise
        },
        
        
        // promised method
        executeSmallScriptPromised : function (text, ignoreException) {
            var me          = this
            
            if (this.shouldStopScriptExecution()) return Promise.resolve(null)
            
            return Promise.resolve(this.driver.executeScript(text))[ 'catch' ](function (e) {
                // IE driver as everything related to IE sporadically throws exceptions when executing the code
                // it also throws exception when trying to execute the script on 404 page (all other drivers are ok)
                if (!ignoreException && !me.shouldStopScriptExecution()) {
                    me.debug("<Exception from launcher, pageId=" + me.id + ", sessionId=" + me.getSessionId() + ">")
                    me.debug("    While running small script: " + text.substring(0, 250))
                    me.debug("    Exception: " + e)
                    me.debug("</Exception from launcher>")
                    
                    throw e
                } else
                    return null
            })
        }
    }
    // eof methods
})

}();