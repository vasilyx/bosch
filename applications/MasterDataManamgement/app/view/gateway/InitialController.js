Ext.define('MAM.view.gateway.InitialController', {
    extend: 'Ext.app.ViewController',
    requires: [
        'MAM.model.GatewayDetail'
    ],
    alias: 'controller.gatewayInitialController',

    control: {
        'gatewayInitial gatewayList': {
            itemdblclick: 'onRecordSelect'
        },
        'gatewayInitial': {
            show: 'onShowInitial'
        }
    },
    listen: {
        store: {
            '#gateways': {
                load: 'onGatewaysLoad'
            }
        }
    },
    routes : {
        '/gateway/itemId/:id' : 'loadDetails'
    },

    onShowInitial: function() {
        this.fireEvent('openEntity', MAM.utils.Constants.GATEWAY, this.getViewModel().get('gateway'));
    },

    /**
     * Loads the necessary data for given entity
     * @param rowModel
     * @param record
     */
    onRecordSelect: function(rowModel, record){
        this.loadDetails(record.getId());
    },

    /**
     * Load entity details,s
     * @param id int
     * return void
     */
    loadDetails: function(id) {
        this.fireEvent('toggleLoadingMask', true , _tl.get('mam.label.ui.loading.gatewayDetail'));
        if(!this.getViewModel().get('gateway')){
            this.getView().createGatewayDetailContainer();
        }
        MAM.model.GatewayDetail.load( id, {
            success: this.onDataGatewayLoadSuccess,
            failure: this.onDataGatewayLoadError,
            scope: this
        });
    },
    /**
     * get total count while loading gateways
     * @param store
     * @param records
     */
    onGatewaysLoad: function( store, records ){
        this.getViewModel().set('total', store.getTotalCount());

    },

    /**
     * Loads relations and custom attributes after sucessfull
     * gateway load
     * @param recordDetail
     */
    onDataGatewayLoadSuccess: function( recordDetail ){
        var me = this;
        var viewModel = this.getViewModel();
        viewModel.set('gateway', recordDetail);
        viewModel.getStore('certificates').loadRawData(recordDetail.get('certificates'));

        this.fireEvent('detailsLoaded', MAM.utils.Constants.GATEWAY);
        this.fireEvent('openEntity', MAM.utils.Constants.GATEWAY, recordDetail);

        var tabConfig = MAM.utils.Configuration.getTabConfig( MAM.utils.Constants.GATEWAY );
        //check which tabs are configured
        Ext.each(tabConfig, function( config ){
            if(config.type === MAM.utils.Constants.RELATIONSDETAIL){
                //load gateway relations
                MAM.model.Relation.getProxy().setEntity( MAM.utils.Constants.GATEWAY );
                MAM.model.Relation.getProxy().on('exception', me.onDataGatewayRelationsLoadError , me);
                MAM.model.Relation.load( recordDetail.getId(), {
                    success: me.onDataGaewayRelationsLoadSuccess,
                    failure: me.onDataGatewayRelationsLoadError,
                    scope: me
                });
            }
            if(config.type === MAM.utils.Constants.CUSTOMATTRIBUTESDETAIL){

                //load gateway customAttributes
                MAM.model.CustomAttribute.getProxy().setEntity( MAM.utils.Constants.GATEWAYS );
                MAM.model.CustomAttribute.getProxy().on('exception', me.onDataGatewayCustomAttributesLoadError , me);
                MAM.model.CustomAttribute.load( recordDetail.getId(), {
                    success: me.onDataGatewayCustomAttributeLoadSuccess,
                    failure: me.onDataGatewayCustomAttributesLoadError,
                    scope: me
                });
            }
        });

    },
    /**
     * Creates a realationschange event when gateway relations
     * are sucessull loeaded
     * @param recordDetail
     */
    onDataGaewayRelationsLoadSuccess: function( recordDetail ){
        var viewModel = this.getViewModel();
        viewModel.set('relations', recordDetail);
        viewModel.get('gateway').set('relations', recordDetail);
        this.fireEvent('relationschange', recordDetail, MAM.utils.Constants.GATEWAYS );
        this.fireEvent('toggleLoadingMask', false );
    },
    /**
     * Creates a customattributechange event when gateway
     * custom attributes are successful loaded
     * @param recordDetail
     */
    onDataGatewayCustomAttributeLoadSuccess: function( recordDetail ){
        var viewModel = this.getViewModel();
        viewModel.get('gateway').set('customAttributes', recordDetail);
        this.fireEvent('customattibuteschange', recordDetail, MAM.utils.Constants.GATEWAYS );
    },

    /**
     * Gateway cannot be loaded
     * so process no relations and custom attributes error
     * to display empty data
     */
    onDataGatewayLoadError: function(){
        this.onDataGatewayRelationsLoadError();
        this.onDataGatewayCustomAttributesLoadError();
        this.fireEvent('toggleLoadingMask', false );
    },
    /**
     * relations of gateway can not be loaded
     * fire event relationschange with an empty relation to
     * display empty data
     * @param request
     */
    onDataGatewayRelationsLoadError: function( request ){
        var emptyRecord = Ext.create('MAM.model.Relation');
        this.fireEvent('relationschange', emptyRecord, MAM.utils.Constants.GATEWAYS );
        this.fireEvent('toggleLoadingMask', false );
    },
    /**
     * custom attributes of gateway can not be loaded
     * fire event customattibuteschange with an empty relation to
     * display empty data
     * @param request
     */
    onDataGatewayCustomAttributesLoadError: function( request ){
        var emptyRecord = Ext.create('MAM.model.CustomAttribute');
        this.fireEvent('customattibuteschange', emptyRecord, MAM.utils.Constants.GATEWAYS );
        this.fireEvent('toggleLoadingMask', false );
    }

});