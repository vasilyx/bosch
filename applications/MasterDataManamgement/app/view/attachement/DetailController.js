Ext.define('MAM.view.attachment.DetailController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.attachmentDetailController',

    control: {
        '#': {
            attachmentChange: 'onAttachmentChange'
        }
    },

    onAttachmentChange: function( view, id ){
        var url = MAM.model.AttachmentDocument.getProxy().url;
        url = url.replace('{id}', id);
        MAM.model.AttachmentDocument.getProxy().url = url;

        MAM.model.AttachmentDocument.load( id, {
            success: this.onDataAttachmentDocumentLoadSuccess,
            failure: this.onDataLoadFailure,
            scope: this
        });

        url = url.replace(id, '{id}');
        MAM.model.AttachmentDocument.getProxy().url = url;
    },

    onDataAttachmentDocumentLoadSuccess: function( data, options ){
        var attachment = this.getViewModel().get('attachment');
        var prefix = "data:" + attachment.get('mimeType') + ';base64,';

        var attachmentData = {};

        // If content is not image, show link to download
        if (attachment.get('mimeType').indexOf('image/') !== -1) {
            this.getView().lookupReference('imageContainer').setVisible(true);
            this.getView().lookupReference('attachmentContainer').setVisible(false);
            attachmentData.imgWidth = this.getView().lookupReference('imageContainer').getWidth();
        } else {
            this.getView().lookupReference('imageContainer').setVisible(false);
            this.getView().lookupReference('attachmentContainer').setVisible(true);
        }

        //var packet = Ext.create('Ext.data.amf.Packet');
        //var data = packet.decode(data.responseBytes);

        //var blob = new Blob([data.responseBytes], {type: attachment.get('mimeType')});
            //url = window.URL.createObjectURL(blob);

        //TODO-1: Review 10.01.2017 adjust the view of images
        //in the version 1 we got a base64 string for the attachement
        //now the backend is returning an image


        var string = this.bin2string(data.data.responseBytes);
        attachmentData.data = prefix + btoa( string );

        this.getViewModel().set('document', attachmentData );

    },

    onDataLoadFailure: function( response, options ){
      //  console.log(response, options);
        console.log("Error loading Data");
    },

    bin2string: function (array){
        var result = '';
        Ext.each(array, function( partOrArray ){
            result+= (String.fromCharCode(partOrArray));
        });

        return result;
    }


});