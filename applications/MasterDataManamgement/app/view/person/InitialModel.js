Ext.define('MAM.view.person.InitialModel', {
    extend: 'Ext.app.ViewModel',
    requires: ['MAM.model.Gateway'],

    alias: 'viewmodel.personInitialModel',

    data: {
        person: null,
        total: 0
    },
    formulas: {
        totalText: {
            get: function(get) {
                var total = get('total');
                if(total === 1){
                    return Ext.util.Format.number(total, '0,000') + ' ' + _tl.get('mam.label.person');
                }
                else{
                    return Ext.util.Format.number(total, '0,000') + ' ' + _tl.get('mam.label.persons');
                }
            }
        }
    }
});