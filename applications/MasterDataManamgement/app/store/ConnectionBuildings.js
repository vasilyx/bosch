Ext.define('MAM.store.ConnectionBuildings', {
    extend: 'Ext.data.Store',
    alias: 'store.connectionBuildings',
    storeId: 'connectionBuildings',
    model: 'MAM.model.ConnectionBuilding',
    autoLoad: false,
    pageSize: MAM.Config.List.limit,
    proxy: {
        type: 'ajax',
        url: '/MAM/' + MAM.Config.Backend.Versions + '/connectionBuildings',
        startParam: 'skip',
        limitParam: 'top',
        extraParams: {
            expand: 'properties',
            totalNumber: true
        },
        headers: {
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty: 'connectionBuildings',
            totalProperty: 'totalNumber'
        }
    }
});