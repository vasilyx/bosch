/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Siesta.Launcher.Runner.PhantomJS', {
    
    isa     : Siesta.Launcher.Runner.BaseRunner,
    
    has : {
        pagePollInteval     : 1000,
        
        maxWorkers          : 1
    },
    
    
    methods : {

        createPage : function (callback) {
            var options     = this.dispatcher.options
            
            var config      = {
                runner          : this,
                launcher        : this.launcher,
                dispatcher      : this.dispatcher,
                
                pollInterval    : this.pagePollInteval
            }
            
            if (options.width) config.width     = options.width
            if (options.height) config.height   = options.height
            
            callback(null, new Siesta.Launcher.Page.PhantomJS(config))
        }
    }
})
