/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
!function () {
/* header */

java.lang.Thread.setDefaultUncaughtExceptionHandler({
    uncaughtException       : function (thread, exception) {
        java.lang.System.err.println("ERROR in thread: [" + thread + "]")
        
        exception.printStackTrace()
        
        quit(7)
    }
})
    

var ids         = {}
var timer       = new java.util.Timer();
var counter     = 1; 

setTimeout = sync(function (fn, delay) {
    var id      = counter++;
    ids[ id ]   = new JavaAdapter(java.util.TimerTask, { run : fn })
    
    timer.schedule(ids[ id ], delay);
    
    return id;
})

clearTimeout = sync(function (id) {
    var task    = ids[ id ]
    
    if (task) {
        task.cancel();
        timer.purge();
        
        delete ids[ id ]
    }
})

setInterval = sync(function (fn, delay) {
    var id      = counter++
    ids[ id ]   = new JavaAdapter(java.util.TimerTask, { run : fn })
    
    timer.schedule(ids[ id ], delay, delay)
    
    return id
})

clearInterval = clearTimeout;

SHUTDOWN    = false

java.lang.Runtime.getRuntime().addShutdownHook(java.lang.Thread(function () {
    SHUTDOWN    = true
    
    Joose.O.each(ids, function (id) {
        clearTimeout(id)
    })
}))



Role('Siesta.Launcher.CommandLineTool.RhinoTool', {
    
    does        : [
        Siesta.Launcher.FileSystem.Rhino,
        Siesta.Launcher.CommandLineTool.BaseTool
    ],
    
    methods : {
        
        doExit : function (code) {
            quit(code)
        },
        
        
        getTerminalWidth : function () {
            if (!this.isWindows) return this.terminalWidth
            
            var processBuilder  = new java.lang.ProcessBuilder('cmd.exe', '/C', 'mode')
            
            var tunnelProcess   = processBuilder.start()
            var inputStream     = tunnelProcess.getInputStream()
            var bufferedReader  = new java.io.BufferedReader(new java.io.InputStreamReader(inputStream))
            
            var line
            var output          = ''
            
            while ((line = bufferedReader.readLine()) != null) output += line
            
            tunnelProcess.waitFor()
            
            var match           = /Columns:\s*(\d+)/.exec(output)
            
            return match ? Number(match[ 1 ]) : 80
        },
        
        
        getJavaClass : function (className) {
            var parts       = className.split('.')
            var cls         = Packages
            
            for (var i = 0; i < parts.length; i++) {
                cls         = cls[ parts[ i ]]
                
                if (cls === undefined) return null
            }
            
            return cls
        },
            
            
        sleep : function (time) {
            java.lang.Thread.currentThread().sleep(time)
        },
        
        
        print : function (text, indentLevel) {
            print(this.prepareText(text, false, indentLevel))
        },
        
        
        printErr : function (text, indentLevel) {
            java.lang.System.err.println(this.prepareText(text, false, indentLevel))
        },
        
        
        checkIsWindows : function () {
            return java.lang.System.getProperty("os.name").indexOf("Windows") != -1
        },

        
        checkIsMacOS : function () {
            return java.lang.System.getProperty('os.name').indexOf("Mac") != -1
        },
        
        
        checkIs64Bit : function () {
            return java.lang.System.getProperty('os.arch').indexOf("64") != -1
        }
    }
    // eof methods
})

/* footer */
}()