Ext.define('MAM.model.MarketParticipant', {
    extend: 'Ext.data.Model',
    requires: [
        'MAM.model.Relation'
    ],
    idProperty: 'id',
    fields: [
        { name: 'id', type: 'int', mapping: 'marketParticipantId'},
        { name: 'customId', type: 'string', mapping: 'codeType'},
        { name: 'name', type: 'string'},
        { name: 'role', type: 'string'},
        { name: 'code', type: 'string'},

        { name: 'customAttributes', reference: 'MAM.model.CustomAttribute'},
        { name: 'relations', reference: 'MAM.model.Relation' }
    ]
});