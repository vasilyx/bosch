/*
 * This file is generated and updated by Sencha Cmd. You can edit this file as
 * needed for your application, but these edits will have to be merged by
 * Sencha Cmd when upgrading.
 */
Ext.Loader.setConfig({
    enabled: true
});
Ext.application({
    name: 'MAM',

    extend: 'MAM.Application',

    requires: [
        'MAM.utils.*',
        'MAM.view.main.Main',
        'Energy.common.language.*'
    ],

    // The name of the initial view to create. With the classic toolkit this class
    // will gain a "viewport" plugin if it does not extend Ext.Viewport. With the
    // modern toolkit, the main view will be added to the Viewport.
    //


    //-------------------------------------------------------------------------
    // Most customizations should be made to MAM.Application. If you need to
    // customize this file, doing so below this section reduces the likelihood
    // of merge conflicts when upgrading to new versions of Sencha Cmd.
    //-------------------------------------------------------------------------
    launch: function () {

        /**
         * if running siesta tests, the MockData will be created
         * and intialized here
         */
        if (location.search.match('siestatest')) {
            //Ext.Loader.setPath('TestApplication', 'tests');
            MOCK_DATA = Ext.create('TestApplication.data.MockData');
            MOCK_CONFIG = Ext.create('TestApplication.data.MockConfig');

            // loading test config data for some tests like *_actionBar.t.js
            MAM.Config.ActionPanel = MOCK_CONFIG.actionPanel;
        }

        //shortcuts for localization classes
        _locale = Energy.common.language.Locale;
        _tl = Energy.common.language.Translations;

        //load translation keys for current application
        var url = 'resources/locales/' + _locale.getLocale() + '.json';
        _tl.url = url;
        MAM.utils.Configuration.setMam(MAM.Config);

        this.setMainView('MAM.view.main.Main');
    }
});
