/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
;[ process.stdout, process.stderr ].forEach(function (s) {
    s && s.isTTY && s._handle && s._handle.setBlocking && s._handle.setBlocking(true)
})

Role('Siesta.Launcher.CommandLineTool.NodeJSTool', {
    
    does        : [
        Siesta.Launcher.FileSystem.NodeJS
    ],
    
    
    methods : {
        
        getTerminalWidth : function () {
            return process.stdout.columns
        },
        
        
        print : function (text, indentLevel) {
            process.stdout.write(this.prepareText(text, true, indentLevel))
        },
        
        
        printErr : function (text, indentLevel) {
            process.stderr.write(this.prepareText(text, true, indentLevel))
        },
        
        
        checkIsWindows : function () {
            return process.platform == 'win32'
        },

        
        checkIsMacOS : function () {
            return process.platform == 'darwin'
        },
        
        
        checkIs64Bit : function () {
            return process.arch == 'x64'
        },
        
        
        doExit : function (code) {
            process.exit(code)
        }
    }
    // eof methods
})
