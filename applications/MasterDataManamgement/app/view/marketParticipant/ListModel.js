Ext.define('MAM.view.marketParticipant.ListModel', {
    extend: 'Ext.app.ViewModel',
    requires: ['MAM.model.Meter', 'MAM.store.Meters'],
    
    alias: 'viewmodel.marketParticipantListModel',
    
    data: {
        title: 'Meter Liste'
    },
    stores: {
        marketParticipants: {
           type: 'marketParticipants'
        }
    }
});