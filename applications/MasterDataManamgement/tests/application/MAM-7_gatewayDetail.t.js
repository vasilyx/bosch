StartTest(function (t) {

    var list = Ext.create('TestApplication.application.AbstractList', {t: t, name: 'gateway'}),

        referenceData = {
            "customId": "EPPC0210487408",
            "manufacturer": "PPC",
            "communicationType": "GPRS",
             status: {en: "Installed", de: "Eingebaut"},
            "gatewayId": 5090
        },
        customAttributes = [
            {
                "key": {de: "Lieferscheinnummer", en: "DeliveryNoteNumber"},
                "value": "2015-0104"
            },
            {
                "key": "Hardware Version",
                "value": "Rev. 1A"
            },
            {
                "key": "Parametrier Version",
                "value": ""
            }
        ],
        referenceDataDetail = {
            "customId": "1ITR4444353430",
            "deviceType": "3.HZ-AC-H4-B1",
            "manufacturer": "ITR"
        };

    list.testListViewArea();

    t.it('#2 Enter full entity id into filter filed', function(t) {
        t.chain(
            function(next) {
                MOCK_DATA.getMockedDataDetail('tests/data/detail/gateways/details_EPPC0210487408.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/gateways/customAttributes_EPPC0210487408.json', referenceData.gatewayId);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/gateways/relations_EPPC0210487408.json', referenceData.gatewayId);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/r/details_2713409.json'); // for related
                MOCK_DATA.getMockedDataDetail('tests/data/detail/attachments/details_47.json'); // attachments
                MOCK_DATA.getMockedDataDetail('tests/data/detail/attachments/details_62.json'); // attachments
                MOCK_DATA.getMockedDataDocument('tests/data/document/147.jpg', 147, {'Content-Type' : "image/jpeg"}); // document
                MOCK_DATA.getMockedDataDocument('tests/data/document/62.zip', 62, {'Content-Type' : "application/zip"}); // document
                next();
            },
            { doubleclick : t.getFirstRow(t.cq1('gatewayInitial gridpanel')) },
            { waitForCQ : 'gatewayDetail' },
            { waitForComponentVisible : 'gatewayDetail'},
            {
                action      : 'wait',
                delay       : 1500
            },
            function(next) {
                t.cqExists('gatewayDetail', 'gatewayDetail view exist');

                for (var code in referenceData) {
                    var val = referenceData[code].hasOwnProperty(_locale.getLocale()) ? referenceData[code][_locale.getLocale()] : referenceData[code];
                    t.contentLike(t.cq1('gatewayDetail displayfield[name="' + code + '"]'), val, 'Found right  #' + code + ' ' + val);
                }

                t.elementIsVisible(t.cq1('gatewayDetail grid[name="certificates"]'), 'Certificates grid is visible');
                t.expect(t.cq1('gatewayDetail grid[name="certificates"]').getStore().getTotalCount()).toBe(0);
            }
        )
    });


    //#3 two tabs are visible => Expected Result: Relations of the Gateway and Custom Attributes of the Gateway
    t.it('#3 Two tabs are visible', function (t) {
        t.chain(
            {
                waitForCQ: 'gatewaySubDetail'
            }, function (next) {
                t.cqExists('relationsInitial', 'Relations tab exist');
                t.cqExists('customAttributesInitial', 'Custom Attributes tab exist');
                next();
            }
        );
    });

    //#4 first tab contains relations => Expected Result: 1 relation availalbe (Meter)
    t.it('#4 First tab contains relations', function (t) {
        t.chain(
            {
                click: t.compositeQuery('gatewaySubDetail tabbar => .x-tab')[0]
            }, {
                waitForCQ: 'relationsInitial'
            }, function () {
                var accordionEl = t.cq('relationsInitial #relationAccordion grid');

                t.expect(accordionEl.length).toBe(2);

                t.like(accordionEl[0].getTitle(), Energy.common.language.Translations.get("mam.label.meter"), 'Accordion meter is visible');
            }
        )
    });

    //#5 meter relation view => Expected Result: one Meter and detail view is visible
    t.it('#5 Meter relation view', function (t) {
        t.chain(
            {
                click: t.compositeQuery('gatewayDetail relationsInitial grid => .x-grid-cell')[1],
                waitForComponentVisible: 'meterDetail'
            },
            function (next) {
                t.expect( t.cq1('gatewaySubDetail relationsInitial grid').getStore().getCount()).toBe(1);
                t.elementIsVisible(t.cq1('relationsInitial meterDetail'), 'MeterDetail is visible');

                for (var code in referenceDataDetail) {
                    t.contentLike(t.cq1('relationsInitial meterDetail displayfield[name="' + code + '"]'),
                                        referenceDataDetail[code],
                                        'Found right  #' + code + ' ' + referenceDataDetail[code]);
                }
                next();
            }
        )
    });

    //#6 Attachement relation view
    t.it('#6 Attachement relation view', function (t) {
        t.chain(
            {
                click: t.cq('gatewaySubDetail relationsInitial gridpanel')[1], // attachment
                waitForComponentVisible: 'attachmentDetail'
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            { doubleclick : t.compositeQuery('gatewaySubDetail relationsInitial  gridpanel[itemId=attachments] => .x-grid-cell[textContent=47]')[0] },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                var grid = t.cq('gatewaySubDetail relationsInitial grid')[1];
                t.expect( grid.getStore().getCount()).toBe(2);
                t.elementIsVisible(t.cq1('gatewaySubDetail relationsInitial attachmentDetail'), 'AttachmentDetail is visible');

                var img = t.compositeQuery('gatewaySubDetail relationsInitial attachmentDetail => img')[0];

                t.elementIsVisible(img, 'Attachment image is visible');
                t.isGreater(img.getAttribute('src').length, 39700, 'Image has data');

                t.doubleClick(t.compositeQuery('gatewaySubDetail relationsInitial  gridpanel[itemId=attachments] => .x-grid-cell[textContent=62]')[0]);
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                var a = t.compositeQuery('gatewaySubDetail relationsInitial attachmentDetail => a')[0];
                t.elementIsVisible(a, 'Attachment downloadlink is visible');
                t.isGreater(a.getAttribute('href').length, 200, 'Download has data');

                next();
            }
        )
    });

    //#7 Custom Attributes of the gateway tab => Expected Result: 3 items
    t.it('#7 Custom Attributes of the gateway tab', function (t) {
        t.chain(
            {
                click: t.compositeQuery('gatewaySubDetail tabbar => .x-tab')[1]
            }, {
                waitForCQ: 'customAttributesInitial'
            }, function () {
                var detailsData = t.cq1('customAttributesInitial').getStore(),
                    rows = t.compositeQuery('customAttributesInitial => .x-grid-row');

                t.expect(detailsData.getCount()).toBe(customAttributes.length);
                for (var index in customAttributes) {
                    var keyTranslated = customAttributes[index].key.hasOwnProperty(_locale.getLocale()) ?
                                            customAttributes[index].key[_locale.getLocale()] :
                                                customAttributes[index].key;

                    t.like(detailsData.getAt(index).get('key'), keyTranslated);
                    t.like(detailsData.getAt(index).get('value') || '', customAttributes[index].value || '');
                }
            }
        );
    });
});