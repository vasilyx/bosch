Ext.define("Energy.common.language.DialogBox", {
    extend: 'Energy.common.language.TranslationPackage',
    alternateClassName: ['LangDialogBox'],
    statics: {
        en: {
            Information: 'Information',
            Question: 'Question',
            Success: 'Success',
            Warning: 'Warning',
            Error: 'Error',
            OK: 'OK',
            Yes: 'Yes',
            No: 'No'
        },
        de: {
            Information: 'Information',
            Question: 'Frage',
            Success: 'Erfolgreich',
            Warning: 'Warnung',
            Error: 'Fehler',
            OK: 'OK',
            Yes: 'Ja',
            No: 'Nein'
        }
    }
});