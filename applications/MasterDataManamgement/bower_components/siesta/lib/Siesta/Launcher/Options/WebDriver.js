/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Siesta.Launcher.Options.WebDriver', {
    
    isa         : Siesta.Launcher.Options.Base,
    
    does        : [
        Siesta.Launcher.Options.Proxy,
        Siesta.Launcher.Options.BrowserStack,
        Siesta.Launcher.Options.SauceLabs
    ],
    
    has : {
        knownBrowsers   : {
            init    : {
                firefox         : true,
                'internet explorer' : true,
                safari          : true,
                chrome          : true,
                microsoftedge   : true
            }
        }
    },
    
    
    methods : {
        
        addCapability : function (name, value) {
            var options             = this.options
            
            options.caps[ name ]    = value
            
            options.cap.push(name + '=' + value)
        },
        
        
        getBrowserName : function () {
            var options         = this.options
            
            return options.caps.browserName || options.browser
        },
        

        validate : function () {
            var options         = this.options
            
            // prepare capabilities
            var caps            = options[ 'cap' ] || []
            if (!(caps instanceof Array)) caps = [ caps ]
            
            options.cap         = caps
            options.caps        = {}
            
            Joose.A.each(caps, function (value) {
                var split       = String(value).split(/\s*=\s*/)
                
                options.caps[ split[ 0 ] ] = split[ 1 ]
            })
            
            // need to have "caps" populated for preparing the report file name
            if (this.SUPER() === false) return false
            
            // prepare browser args
            var browserArgs     = options[ 'browser-arg' ] || []
            if (!(browserArgs instanceof Array)) browserArgs = [ browserArgs ]
            
            options[ 'browser-arg' ]    = browserArgs
            
            // prepare browser name, default value should be set outside of the `getBrowserName`
            var browser         = this.getBrowserName() || 'chrome'
            
            if (browser == 'ie') browser = 'internet explorer'
            
            if (!this.knownBrowsers[ browser ]) {
                this.printError("Unknown browser: " + browser)
                
                return false
            }
            
            options.browser     = browser
            
            return true
        },
        
        
        getReportFileNameTemplateReplacer : function () {
            var options         = this.options
            var caps            = options.caps
            
            return function (m, match) {
                if (caps && caps.hasOwnProperty(match)) 
                    return caps[ match ]
                else
                    return options[ match ]
            }
        }
    }
})
