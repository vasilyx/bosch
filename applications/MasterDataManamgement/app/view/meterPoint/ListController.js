Ext.define('MAM.view.meterPoint.ListController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.meterPointListController',
    control: {
        '#': {
            boxready: 'loadMeterPoints'
        }
    },

    loadMeterPoints: function( ){
        var store = this.getViewModel().getStore('meterPoints');
        this.fireEvent('toggleLoadingMask', true , _tl.get('mam.label.ui.loading.meterPoint'));
        this.loadStore( store );
    },
    onCustomIdSearch: function( textfield ){
        var store = this.getView().getStore();

        if(!Ext.isEmpty(textfield.getValue())){
            Ext.merge(store.getProxy().extraParams, {customId: textfield.getValue()});
            this.loadStore( store );
        }
        else{
            delete store.getProxy().extraParams.customId;
            this.loadStore( store );
        }

    },
    loadStore: function( store ){
        store.load({
            scope: this,
            callback: function(){
                this.fireEvent('toggleLoadingMask', false );
            }
        });
    }
});