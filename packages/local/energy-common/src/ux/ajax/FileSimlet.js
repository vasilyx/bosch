/**
 * JSON Simlet.
 */
Ext.define('Energy.common.ux.ajax.FileSimlet', {
    extend: 'Ext.ux.ajax.Simlet',
    alias: 'simlet.fileSimlet',

    /**
     * Performs the action requested by the given XHR and returns an object to be applied
     * on to the XHR (containing `status`, `responseText`, etc.). For the most part,
     * this is delegated to `doMethod` methods on this class, such as `doGet`.
     *
     * @param {Ext.ux.ajax.SimXhr} xhr The simulated XMLHttpRequest instance.
     * @return {Object} The response properties to add to the XMLHttpRequest.
     */


    exec: function (xhr) {
        var me = this,
            ret = {},
            method = 'do' + Ext.String.capitalize(xhr.method.toLowerCase()), // doGet
            fn = me[method];

        if (fn) {
            ret = fn.call(me, me.getCtx(xhr.method, xhr.url, xhr));
        } else {
            ret = { status: 405, statusText: 'Method Not Allowed' };
        }

        if(xhr.simlet.binary){
            ret.response = xhr.simlet.data;
        }

        return ret;
    }

});
