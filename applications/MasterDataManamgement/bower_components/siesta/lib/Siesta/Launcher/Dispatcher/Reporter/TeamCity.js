/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Launcher.Dispatcher.Reporter.TeamCity', {

    has : {
        tcSuiteName     : null,

        tcEncodeRegExp  : /(['\[\]|])/g,

        testSuiteStartPrinted       : false,
        testSuiteFinishPrinted      : false,
        
        teamCityPrintState          : Joose.I.Object
    },


    override : {

        initialize : function () {
            var me      = this

            this.launcher.on('destroy', function () {
                if (me.testSuiteStartPrinted && !me.testSuiteFinishPrinted) me.printTeamCityTestSuiteFinish()
            })

            this.SUPER()
        },


        warn : function (text) {
            this.print("##teamcity[message text='" + this.tcEncode(text) + "' status='WARNING']")

            this.SUPERARG(arguments)
        },


        onTestSuiteStart : function (userAgent, platform) {
            var browserInfo     = this.parseBrowser(userAgent)

            this.tcSuiteName    = this.options[ 'tc-suite' ] || (
                (this.testSuiteName ? this.testSuiteName + ': ' : '') + browserInfo.name + " " + browserInfo.version
            )

            this.print("##teamcity[testSuiteStarted name='" + this.tcEncode(this.tcSuiteName) + "']")

            this.SUPERARG(arguments)

            this.testSuiteStartPrinted  = true
        },


        onTestSuiteEnd : function () {
            this.SUPER()

            this.printTeamCityTestSuiteFinish()
        },


        printTeamCityTestSuiteFinish : function () {
            this.print("##teamcity[testSuiteFinished name='" + this.tcEncode(this.tcSuiteName) + "']")

            this.testSuiteFinishPrinted = true
        },


        printTestBody : function (testResult) {
            if (testResult.ERROR)
                this.print("##teamcity[testFailed name='" + this.tcTestName(testResult) + "' details='" + this.tcEncode(testResult.ERROR) + "']")

            this.SUPERARG(arguments)
        },
        

        printTestHeader : function (testResult) {
            this.print("##teamcity[testStarted name='" + this.tcTestName(testResult) + "']");
            
            this.SUPERARG(arguments)
        },
        
        
        printTestFooter : function (testResult) {
            this.SUPERARG(arguments)
            
            this.print("##teamcity[testFinished name='" + this.tcTestName(testResult) + "']");
        },


        printAssertionInfo : function (assertion, parentTests, printState) {
            this.SUPERARG(arguments)

            if (assertion.type == 'Siesta.Result.Assertion') {
                if (!assertion.isTodo && (assertion.isException || !assertion.passed)) {
                    var text        = this.getAssertionText(assertion)
                    
                    text            = this.getFormattedAssertionText(text, assertion, parentTests, this.teamCityPrintState, true)
    
                    this.print("##teamcity[testFailed name='" + this.tcTestName(parentTests[ 0 ]) + "' details='" + this.tcEncode(text) + "']")
                }
            }
            
            if (assertion.type == 'Siesta.Result.Diagnostic') {
                if (assertion.isWarning) {
                    var text        = this.getAssertionText(assertion)
                    text            = this.getFormattedAssertionText(text, assertion, parentTests, {}, true)
                    
                    this.print("##teamcity[message text='" + this.tcEncode(text) + "' status='WARNING']")
                }
            }
        }
    },


    methods : {

        tcTestName : function (testResult) {
            return this.tcEncode(testResult.name || testResult.url)
        },


        tcEncode : function (text) {
            return String(text).replace(this.tcEncodeRegExp, '|$1').replace(/\r/g, '|r').replace(/\n/g, '|n')
        }
    }

})
