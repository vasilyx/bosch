StartTest(function (t) {

    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/l/details_1.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/customAttributes_DE0700081632100000000000070238000.json', 1);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/relations_DE0700081632100000000000070238000.json', 1); // for related
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/r/details_1494726.json'); // for related
    MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/r/details_1480156.json'); // for related
    MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/r/details_1035879.json'); // for related

    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterReadingsHistory/1.json');

    var referenceData = {
        customId: 'DE0700081634800000000001004896956'
    };

    t.it('#1 open a meter point', function (t) {
        t.chain(
            {waitForCQ: 'app-main tabbar'},

            {click: 'tabbar=> span.icon-meterPoint'},
            {
                action      : 'wait',
                delay       : 1500      // 1500 ms
            },
            function (next) {
                t.doubleClick(t.compositeQuery('meterPointInitial gridpanel => .x-grid-cell')[1]);
                next();
            },
            {
                action      : 'wait',
                delay       : 1500      // 1500 ms
            },
            function (next) {
                t.click(t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-information]'));
                var menu = t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-information]').getMenu();
                t.like(menu.items.items[1].text, _tl.get('mam.label.actionPanel.meterReadings'), 'meterReadings menu item is available');
                next();
            }
        )
    });

    t.it('#2 check first and second group', function (t) {

        t.chain(
            { click : "#main-tabpanel menuitem[action=actionMeterReadings] => .x-menu-item-text" },
            function (next) {
                t.elementIsVisible(t.cq1('window[cls=actionWindow]'), 'Window is opened');
                t.contentLike('actionMeterReadings container(true) => .x-autocontainer-innerCt', referenceData.customId, 'MeterPoint Id is ok');
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {

                var groupsView = t.cq1('window actionMeterReadings grid').getView().getFeature('group');
                t.expect(groupsView.isAllExpanded()).toBe(true);
                t.expect(groupsView.getGridStore().getGroups().items[0].getCount()).toBe(4);
                t.expect(groupsView.getGridStore().getTotalCount()).toBe(12);


                Ext.each(groupsView.getGridStore().getGroups().items[1].items, function(row, k) {

                    t.like( row.get('reason'), 'i');
                    if (k == 0 ) {
                        t.like( row.get('registerName'), '1-1:1.8.0*255');
                    } else {
                        t.like( row.get('registerName'), '1-1:2.8.0*255');
                    }
                });

                next();
            }
        )
    });


    t.it('#3 third group', function (t) {
        t.chain(
            function (next) {

                var groupsView = t.cq1('window actionMeterReadings grid').getView().getFeature('group');
                t.expect(groupsView.isAllExpanded()).toBe(true);
                t.expect(groupsView.getGridStore().getGroups().items[2].getCount()).toBe(6);


                var r1 = [ 'a', 'i', 't'],
                    r2 = [ 'z', 'z', 't'];

                var result1 = 0,
                    result2 = 0;

                Ext.each(groupsView.getGridStore().getGroups().items[2].items, function(row, k) {

                    var index1 = r1.indexOf(row.get('reason'));
                    var index2 = r2.indexOf(row.get('reason'));

                    if (row.get('registerName') === '1-1:1.8.0*255' && index1 !== -1 ) {

                        result1++;
                        r1.splice(index1, 1);

                    } else if (row.get('registerName') == '1-1:2.8.0*255' && index2 !== -1 ) {

                        result2++;
                        r2.splice(index2, 1);
                    }
                });

                t.expect(result1).toBe(3);
                t.expect(result2).toBe(3);

                t.click(t.cq1('#closeButton'));

                t.elementIsNotVisible(t.cq1('window[cls=actionWindow]'), 'Window is hide');
            }
        )
    });
});