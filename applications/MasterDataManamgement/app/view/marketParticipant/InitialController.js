Ext.define('MAM.view.marketParticipant.InitialController', {
    extend: 'Ext.app.ViewController',
    requires: [
        'MAM.model.MarketParticipantDetail',
        'MAM.model.Relation',
        'MAM.model.CustomAttribute'
    ],
    alias: 'controller.marketParticipantInitialController',
    control: {
        'marketParticipantInitial marketParticipantList': {
            itemdblclick: 'onRecordSelect'
        },
        'marketParticipantInitial': {
            show: 'onShowInitial'
        }
    },
    listen: {
        store: {
            '#marketParticipants': {
                load: 'onMarketParticipantsLoad'
            }
        }
    },
    routes : {
        '/marketParticipant/itemId/:id' : 'loadDetails'
    },

    onShowInitial: function() {
        this.fireEvent('openEntity', MAM.utils.Constants.MARKETPARTICIPANT, this.getViewModel().get('marketParticipant'));
    },

    /**
     * Loads the necessary data for given entity
     * @param rowModel
     * @param record
     */
    onRecordSelect: function(rowModel, record){
        this.loadDetails(record.getId());
    },

    /**
     * Load entity details,
     * @param id int
     * return void
     */
    loadDetails: function(id) {
        this.fireEvent('toggleLoadingMask', true , _tl.get('mam.label.ui.loading.marketParticipantDetail'));

        if(!this.getViewModel().get('marketParticipant')){
            this.getView().createMarketParticipantDetailContainer();
        }

        MAM.model.MarketParticipantDetail.load( id, {
            success: this.onDataMarketParticipantLoadSuccess,
            failure: this.onDataMarketParticipantLoadError,
            scope: this
        });
    },

    /**
     * Get the total on meter points load
     * @param store
     * @param records
     */
    onMarketParticipantsLoad: function( store, records ){
        this.getViewModel().set('total', store.getTotalCount());
    },

    /**
     * Loads relations and custom attributes after sucessfull
     * meter point load
     * @param recordDetail
     */
    onDataMarketParticipantLoadSuccess: function( recordDetail ){
        var me = this;
        var viewModel = this.getViewModel();
        viewModel.set('marketParticipant', recordDetail);

        this.fireEvent('detailsLoaded', MAM.utils.Constants.MARKETPARTICIPANT);
        this.fireEvent('openEntity', MAM.utils.Constants.MARKETPARTICIPANT, recordDetail);

        var tabConfig = MAM.utils.Configuration.getTabConfig( MAM.utils.Constants.MARKETPARTICIPANT );
        //check which tabs are configured
        Ext.each(tabConfig, function( config ){
            if(config.type === MAM.utils.Constants.RELATIONSDETAIL){
                //load meter point relations
                MAM.model.Relation.getProxy().setEntity( MAM.utils.Constants.MARKETPARTICIPANT );
                MAM.model.Relation.getProxy().on('exception', me.onDataMarketParticipantRelationsLoadError , me);

                MAM.model.Relation.load( recordDetail.getId(), {
                    success: me.onDataMarketParticipantRelationsLoadSuccess,
                    failure: me.onDataMarketParticipantRelationsLoadError,
                    scope: me
                });
            }
            if(config.type === MAM.utils.Constants.CUSTOMATTRIBUTESDETAIL){

                //load meter point customAttributes
                MAM.model.CustomAttribute.getProxy().setEntity( MAM.utils.Constants.MARKETPARTICIPANTS );
                MAM.model.CustomAttribute.getProxy().on('exception', me.onDataMarketParticipantCustomAttributesLoadError , me);
                MAM.model.CustomAttribute.load( recordDetail.getId(), {
                    success: me.onDataMarketParticipantCustomAttributeLoadSuccess,
                    failure: me.onDataMarketParticipantCustomAttributesLoadError,
                    scope: me
                });
            }
        });

    },
    /**
     * Creates a realationschange event when meter point relations
     * are sucessull loeaded
     * @param recordDetail
     */
    onDataMarketParticipantRelationsLoadSuccess: function( recordDetail ){
        var viewModel = this.getViewModel();
        viewModel.set('relations', recordDetail);
        viewModel.get('marketParticipant').set('relations', recordDetail);
        this.fireEvent('relationschange', recordDetail, MAM.utils.Constants.MARKETPARTICIPANTS );
        this.fireEvent('toggleLoadingMask', false );
    },
    /**
     * Creates a customattributechange event when meter point
     * custom attributes are successful loaded
     * @param recordDetail
     */
    onDataMarketParticipantCustomAttributeLoadSuccess: function( recordDetail ){
        var viewModel = this.getViewModel();
        viewModel.get('marketParticipant').set('customAttributes', recordDetail);
        this.fireEvent('customattibuteschange', recordDetail, MAM.utils.Constants.MARKETPARTICIPANTS );

    },
    /**
     * Meter point cannot be loaded
     * so process no relations and custom attributes error
     * to display empty data
     */
    onDataMarketParticipantLoadError: function(){
        this.onDataMarketParticipantRelationsLoadError();
        this.onDataMarketParticipantCustomAttributesLoadError();
        this.fireEvent('toggleLoadingMask', false );
    },
    /**
     * relations of meter point can not be loaded
     * fire event relationschange with an empty relation to
     * display empty data
     * @param request
     */
    onDataMarketParticipantRelationsLoadError: function( request ){
        var emptyRecord = Ext.create('MAM.model.Relation');
        this.fireEvent('relationschange', emptyRecord, MAM.utils.Constants.MARKETPARTICIPANTS);
        this.fireEvent('toggleLoadingMask', false );
    },
    /**
     * custom attributes of meter point can not be loaded
     * fire event customattibuteschange with an empty relation to
     * display empty data
     * @param request
     */
    onDataMarketParticipantCustomAttributesLoadError: function( request ){
        var emptyRecord = Ext.create('MAM.model.CustomAttribute');
        this.fireEvent('customattibuteschange', emptyRecord, MAM.utils.Constants.MARKETPARTICIPANTS );
        this.fireEvent('toggleLoadingMask', false );
    }
});