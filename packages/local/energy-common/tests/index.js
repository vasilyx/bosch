var harness = new Siesta.Harness.Browser.ExtJS();

harness.configure({
    title: 'Energy-common tests',
    loaderPath  : {
        'Energy.common': '../src'
    },
    preload     : [
        // version of ExtJS used by your application
        // (not needed if you use Sencha Cmd which builds a complete 'all-file' including Ext JS itself)
        '../../../../applications/MasterDataManagement/bootstrap.css', //dependency to Applications CSS
        '../../../../ext/build/ext-all-debug.js',
        {
            text: harness.getLoaderPathHook() //get loaderPath config before executing load mocked data
        }

    ]

});

harness.start(
    {
        group : 'Unit',
        requires: [
            'Energy.common.ui.DisplayMessage'
        ],
        items : [
            'unit-tests/UI-1_dialogBox.t.js'
        ]
    }
);