Ext.define('MAM.model.GatewayDetail', {
    extend: 'MAM.model.Gateway',
    proxy: {
        type: 'rest',
        api: {
            create: undefined,
            read: '/MAM/' + MAM.Config.Backend.Versions + '/gateways',
            update: undefined,
            destroy: undefined
        },
        headers: {
            'Accept': 'application/json'
        },
        extraParams: {
            expand: 'properties'
        },
        reader: {
            type: 'json',
            transform: {
                fn: function( data, request ){
                    //needed to be transformed because of certificates array
                    var newData = data;

                  if(newData.certificates){

                        if(newData.certificates.length > 0){
                            var certificatesItems = [];
                            Ext.each(newData.certificates, function( cert ){
                                var certificate = {};
                                certificate.certificate = cert;
                                certificatesItems.push(certificate);
                            });

                            newData.certificates = certificatesItems;
                        }
                    }

                    return newData;
                }
            }
        }
    }
});