/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
/**
 @class Siesta.Recorder.ExtJS

 Ext JS specific recorder implementation

 */
Class('Siesta.Recorder.ExtJS', {
    isa     : Siesta.Recorder.Recorder,

    has : {
        extractorClass          : Siesta.Recorder.TargetExtractor.ExtJS,
        
        _moveCursorToSelectors   : function() {
            return [
                '.x-menu-item:not(.x-menu-item-separator)', // Can't access child menu items without first visiting each menu item hovered
                '.x-column-header'     // Column menu / resizing access
            ];
        }
    },

    
    methods : {
        
        initialize : function () {
            this.SUPERARG(arguments)
        },

        addMoveCursorAction : function(event, recordOffsets) {
            // If something is being dragged and we're hovering over the drag target, choose moveCursorTo with coordinate
            if (event.target && this.closest(event.target, '[class*=-dd-drag-proxy]', 5)) {
                this.addAction({
                    action          : 'moveCursorTo',

                    target          : [{
                        type        : 'xy',
                        target      : [ event.x, event.y ]
                    }],

                    sourceEvent     : event,
                    options         : event.options
                })
            } else {
                this.SUPERARG(arguments)
            }
        }
    }
    // eof methods
});
