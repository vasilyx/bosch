Ext.define('MAM.view.connectionBuilding.InitialController', {
    extend: 'Ext.app.ViewController',
    requires: [
        'MAM.model.ConnectionBuildingDetail',
        'MAM.model.Relation',
        'MAM.model.CustomAttribute'
    ],
    alias: 'controller.connectionBuildingInitialController',
    control: {
        'connectionBuildingInitial connectionBuildingList': {
            itemdblclick: 'onRecordSelect'
        },
        'connectionBuildingInitial': {
            show: 'onShowInitial'
        }
    },
    listen: {
        store: {
            '#connectionBuildings': {
                load: 'onConnectionBuildingsLoad'
            }
        }
    },
    routes : {
        '/connectionBuilding/itemId/:id' : 'loadDetails'
    },

    onShowInitial: function() {
        this.fireEvent('openEntity', MAM.utils.Constants.CONNECTIONBUILDING, this.getViewModel().get('connectionBuilding'));
    },

    /**
     * Loads the necessary data for given entity
     * @param rowModel
     * @param record
     */
    onRecordSelect: function(rowModel, record){
        this.loadDetails(record.getId());
    },

    /**
     * Load entity details,
     * @param id int
     * return void
     */
    loadDetails: function(id) {
        this.fireEvent('toggleLoadingMask', true , _tl.get('mam.label.ui.loading.connecitonBuildingDetail'));
        if(!this.getViewModel().get('connectionBuilding')){
            this.getView().createConnectionBuildingDetailContainer();
        }

        MAM.model.ConnectionBuildingDetail.load( id, {
            success: this.onDataConnectionBuildingLoadSuccess,
            failure: this.onDataConnectionBuildingLoadError,
            scope: this
        });
    },

    /**
     * get total count while loading connection buildings
     * @param store
     * @param records
     */
    onConnectionBuildingsLoad: function( store, records ){
        this.getViewModel().set('total', store.getTotalCount());

    },

    /**
     * Loads relations and custom attributes after sucessfull 
     * connection building load
     * @param recordDetail
     */
    onDataConnectionBuildingLoadSuccess: function( recordDetail ){
        var me = this;
        var viewModel = this.getViewModel();
        viewModel.set('connectionBuilding', recordDetail);

        this.fireEvent('detailsLoaded', MAM.utils.Constants.CONNECTIONBUILDING);
        this.fireEvent('openEntity', MAM.utils.Constants.CONNECTIONBUILDING, recordDetail);

        var tabConfig = MAM.utils.Configuration.getTabConfig( MAM.utils.Constants.CONNECTIONBUILDING );
        //check which tabs are configured
        Ext.each(tabConfig, function( config ){
            if(config.type === MAM.utils.Constants.RELATIONSDETAIL){
                //load connection building relations
                MAM.model.Relation.getProxy().setEntity( MAM.utils.Constants.CONNECTIONBUILDING );
                MAM.model.Relation.getProxy().on('exception', me.onDataConnectionBuildingRelationsLoadError , me);
                MAM.model.Relation.load( recordDetail.getId(), {
                    success: me.onDataConnectionBuildingRelationsLoadSuccess,
                    failure: me.onDataConnectionBuildingRelationsLoadError,
                    scope: me
                });
            }
            if(config.type === MAM.utils.Constants.CUSTOMATTRIBUTESDETAIL){

                //load connection building customAttributes
                MAM.model.CustomAttribute.getProxy().setEntity( MAM.utils.Constants.CONNECTIONBUILDINGS );
                MAM.model.CustomAttribute.getProxy().on('exception', me.onDataConnectionBuildingCustomAttributesLoadError , me);

                MAM.model.CustomAttribute.load( recordDetail.getId() , {
                    success: me.onDataConnectionBuildingCustomAttributeLoadSuccess,
                    failure: this.onDataConnectionBuildingCustomAttributesLoadError,
                    scope: me
                });
            }
        });

    },
    /**
     * Creates a realationschange event when conncection building relations
     * are sucessull loeaded
     * @param recordDetail
     */
    onDataConnectionBuildingRelationsLoadSuccess: function( recordDetail ){
        var viewModel = this.getViewModel();
        viewModel.set('relations', recordDetail);
        viewModel.get('connectionBuilding').set('relations', recordDetail);
        this.fireEvent('relationschange', recordDetail, MAM.utils.Constants.CONNECTIONBUILDINGS );
        this.fireEvent('toggleLoadingMask', false );
    },
    /**
     * Creates a customattributechange event when connection building
     * custom attributes are successful loaded
     * @param recordDetail
     */
    onDataConnectionBuildingCustomAttributeLoadSuccess: function( recordDetail ){
        var viewModel = this.getViewModel();
        viewModel.get('connectionBuilding').set('customAttributes', recordDetail);
        this.fireEvent('customattibuteschange', recordDetail, MAM.utils.Constants.CONNECTIONBUILDINGS );
    },
    /**
     * Connection building cannot be loaded
     * so process no relations and custom attributes error
     * to display empty data
     */
    onDataConnectionBuildingLoadError: function(){
        this.onDataConnectionBuildingRelationsLoadError();
        this.onDataConnectionBuildingCustomAttributesLoadError();
        this.fireEvent('toggleLoadingMask', false );
    },
    /**
     * relations of connection building can not be loaded
     * fire event relationschange with an empty relation to
     * display empty data
     * @param request
     */
    onDataConnectionBuildingRelationsLoadError: function( request ){
        var emptyRecord = Ext.create('MAM.model.Relation');
        this.fireEvent('relationschange', emptyRecord, MAM.utils.Constants.CONNECTIONBUILDINGS );
        this.fireEvent('toggleLoadingMask', false );
    },
    /**
     * custom attributes of connection building can not be loaded
     * fire event customattibuteschange with an empty relation to
     * display empty data
     * @param request
     */
    onDataConnectionBuildingCustomAttributesLoadError: function( request ){
        var emptyRecord = Ext.create('MAM.model.CustomAttribute');
        this.fireEvent('customattibuteschange', emptyRecord, MAM.utils.Constants.CONNECTIONBUILDINGS );
        this.fireEvent('toggleLoadingMask', false );
    }

});