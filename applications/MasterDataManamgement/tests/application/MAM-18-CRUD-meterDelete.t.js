StartTest(function (t) {

    var referenceData = {
        id: 1494747,
        customId: 'G2217511661163'
    };

    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/details_G22175AAA25172.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/customAttributes_dummy.json', 1494785);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/relations_dummy.json', 1494785);

    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/details_' + referenceData.id + '.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/customAttributes_dummy.json', referenceData.id);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/relations_dummy.json', referenceData.id);


    t.it('#1 Click on delete icon - response 409 - warning window', function (t) {
        t.chain(
            {waitForCQ: 'app-main tabbar'},
            {click: 'tabbar=> span.icon-meter'},
            function (next) {
                t.doubleClick(t.compositeQuery('meterInitial gridpanel => .x-grid-cell')[0]); // for history
                next();
            }, {
                action: 'wait',
                delay: 1000
            },
            function (next) {
                t.doubleClick(t.compositeQuery('meterInitial gridpanel => .x-grid-cell')[1]);
                next();
            },
            {
                action: 'wait',
                delay: 1000
            },
            function (next) {
                t.click(t.cq1('meterInitial actionInitial toolbar button[cls=icon-delete]'));

                MOCK_DATA.setMockedResponse('meters/' + referenceData.id, 'delete', 409);
                t.elementIsVisible(t.cq1('commonUiDialogBox'), 'DialogBox is visible');
                t.click(t.cq1('commonUiDialogBox button[text=' + _tl.get('mam.dialogbox.meter.button.yes') + ']'));
                next();
            },
            {
                action: 'wait',
                delay: 1000
            },
            function (next) {
                t.elementIsVisible(t.compositeQuery('commonUiDialogBox => div.icon.warning'), 'Warning is visible');

                t.like(
                    t.compositeQuery('commonUiDialogBox => div.content div div')[0].innerHTML,
                    Ext.String.format(_tl.get('mam.dialogbox.meter.delete.response409'), referenceData.customId),
                    '409 message is right'
                );
                t.click(t.cq1('commonUiDialogBox button'));
            }
        )
    });


    t.it('#2 Click on delete icon - response 404 - warning window', function (t) {
        t.chain(
            function (next) {
                // check Warning then close

                t.click(t.cq1('meterInitial actionInitial toolbar button[cls=icon-delete]'));
                MOCK_DATA.setMockedResponse('meters/' + referenceData.id, 'delete', 404);
                t.elementIsVisible(t.cq1('commonUiDialogBox'), 'DialogBox is visible');
                t.click(t.cq1('commonUiDialogBox button[text=' + _tl.get('mam.dialogbox.meter.button.yes') + ']'));

                next();
            },
            {
                action: 'wait',
                delay: 1000
            },
            function (next) {
                t.elementIsVisible(t.compositeQuery('commonUiDialogBox => div.icon.warning'), 'Warning is visible');
                t.like(
                    t.compositeQuery('commonUiDialogBox => div.content div div')[0].innerHTML,
                    Ext.String.format(_tl.get('mam.dialogbox.meter.delete.response404'), referenceData.customId),
                    '404 message is right'
                );

                t.click(t.cq1('commonUiDialogBox button'));
            }
        )
    });

    t.it('#3 Click on delete icon - response 200 - success window', function (t) {
        t.chain(
            function (next) {
                t.click(t.cq1('meterInitial actionInitial toolbar button[cls=icon-delete]'));
                MOCK_DATA.setMockedResponse('meters/' + referenceData.id, 'delete', 200);
                t.elementIsVisible(t.cq1('commonUiDialogBox'), 'DialogBox is visible');
                t.click(t.cq1('commonUiDialogBox button[text=' + _tl.get('mam.dialogbox.meter.button.yes') + ']'));

                next();
            },
            {
                action: 'wait',
                delay: 1000
            },
            function (next) {
                t.elementIsVisible(t.compositeQuery('commonUiDialogBox => div.icon.success'), 'Success is visible');
                t.click(t.cq1('commonUiDialogBox button'));
            }
        )
    });

    t.it('#4 Check history and main area', function (t) {
        t.chain(
            function (next) {
                var historyMenu = t.cq1('meterInitial historyInitial').getMenu();
                var i = 0;
                historyMenu.items.each(function( item ) {
                    if (item.text == referenceData.customId && item.iconCls == 'submenu-icon-meter') {
                        i++;
                    }
                });

                t.expect(i).toBe(0);
                t.contentNotLike(
                    t.compositeQuery('meterInitial commonUiHeader  => div.content.bg-green span.header')[0],
                    referenceData.customId, 'Header without customId'
                );

                t.expect(t.cq1('meterDetail')).toBe(undefined);
            }
        )
    });
});
