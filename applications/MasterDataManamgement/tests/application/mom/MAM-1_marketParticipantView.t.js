StartTest(function (t) {

    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/l/details_1.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/customAttributes_DE0700081632100000000000070238000.json', 1);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/relations_DE0700081632100000000000070238000.json', 1); // for related
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/r/details_1494726.json'); // for related
    MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/r/details_1480156.json'); // for related
    MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/r/details_1035879.json'); // for related

    MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipantsHistory/1.json');

    // dummy routes
    MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipants/l/details_7533.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipants/l/details_7530.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipants/l/customAttributes_dummy.json', 7533);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipants/l/customAttributes_dummy.json', 7530);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipants/l/relations_dummy.json', 7533);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipants/l/relations_dummy.json', 7530);

    var referenceData = {
        customId: 'DE0700081634800000000001004896956',
        title: {en: "Market Participant History", de: "Marktpartnerübersicht"}
    };

    t.it('#1 open a meter point', function (t) {
        t.chain(
            {waitForCQ: 'app-main tabbar'},

            {click: 'tabbar=> span.icon-meterPoint'},
            {
                action      : 'wait',
                delay       : 1500      // 1500 ms
            },
            function (next) {
                t.doubleClick(t.compositeQuery('meterPointInitial gridpanel => .x-grid-cell')[1]);
                next();
            },
            {
                action      : 'wait',
                delay       : 1500      // 1500 ms
            },
            function (next) {
                t.click(t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-information]'));
                var menu = t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-information]').getMenu();
                var val = referenceData['title'].hasOwnProperty(_locale.getLocale()) ? referenceData['title'][_locale.getLocale()] : referenceData['title'];

                t.like(menu.items.items[0].text, val, 'marketParticipants menu item is available');
                next();
            }
        )
    });

    t.it('#2 click on view market participant', function (t) {

        t.chain(
            { click : "#main-tabpanel menuitem[action=actionMarketParticipantHistory] => .x-menu-item-text" },
            function (next) {
                t.elementIsVisible(t.cq1('window[cls=actionWindow]'), 'Window is opened');
                t.contentLike('actionMarketParticipantHistory container(true) => .x-autocontainer-innerCt', referenceData.customId, 'MeterPoint Id is ok');
                next();
            },
            {
                action      : 'wait',
                delay       : 1500      // 1500 ms
            },
            function (next) {
                t.expect(t.cq1('window actionMarketParticipantHistory grid').getStore().getCount()).toBeGreaterThan(0);

                t.click(t.cq1('#closeButton'));

                t.elementIsNotVisible(t.cq1('window[cls=actionWindow]'), 'Window is hide');

                next();
            }
        )
    });

   /* t.it('#3 open a gateway', function (t) {
        t.chain(
            {click: 'tabbar=> span.icon-gateway'},
            function (next) {
                var menu = t.cq1('gatewayInitial actionInitial toolbar button[cls=icon-information]').getMenu();
                t.expect(menu.items.length).toBe(0);
                next();
            }
        )
    });

    t.it('#4 open a person', function (t) {
        t.chain(
            {click: 'tabbar=> span.icon-person'},
            function (next) {
                var menu = t.cq1('personInitial actionInitial toolbar button[cls=icon-information]').getMenu();
                t.expect(menu.items.length).toBe(0);
                next();
            }
        )
    });*/

});