Ext.define('MAM.view.marketParticipant.SubDetail', {
    extend: 'Ext.tab.Panel',
    requires: [
        'MAM.view.relations.Initial'
    ],
    alias: 'widget.marketParticipantSubDetail',
    initComponent: function () {
        var items = [];
        var tabConfig = MAM.utils.Configuration.getTabConfig( MAM.utils.Constants.MARKETPARTICIPANT );

        Ext.each(tabConfig, function( tab ){
            items.push({
                xtype: tab.type + 'Initial',
                title: _tl.get("mam.label." + MAM.utils.Constants.MARKETPARTICIPANT + "." + tab.type),
                itemId: MAM.utils.Constants.MARKETPARTICIPANTS + '-tab-' + tab.type
            })
        });

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    }
});