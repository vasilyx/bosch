Ext.define("Energy.common.ui.RequestStatus", {
    extend: 'Ext.Container',
    
    alias: 'widget.commonUiRequestStatus',
    
    fillData: function(data) {
        if(data){
            this.update(data);
        }
    },
    height: 100,
    flex: 1,
    itemId: 'requestStatus',
    tpl: [
        '<tpl for=".">',
        '<div class="requestStatus-bottom-body">',
        '  <div class="info1">',
        '       <div style="font-size: 10px;">{lastRequestTimeLabel}</div>',
        '       <div style="font-size: 10px;">',
        '           <span>{requestTimeStamp}</span>',
        '       </div>',
        '  </div>',
        '  <div class="info2">',
        '        <div style="font-size: 10px;">{requestTypeLabel}</div>',
        '        <div style="font-size: 10px;">{requestType}, {requestDate}</div>',
        '  </div>',
        '</div>',
        '</tpl>'
    ]
});