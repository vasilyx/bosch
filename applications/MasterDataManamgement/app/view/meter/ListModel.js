Ext.define('MAM.view.meter.ListModel', {
    extend: 'Ext.app.ViewModel',
    requires: ['MAM.model.Meter', 'MAM.store.Meters'],
    
    alias: 'viewmodel.meterListModel',
    
    data: {
        title: 'Meter Liste'
    },
    stores: {
        meters: {
           type: 'meters'
        }
    }
});