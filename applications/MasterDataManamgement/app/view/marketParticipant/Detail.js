Ext.define('MAM.view.marketParticipant.Detail', {
    extend: 'Ext.Panel',

    alias: 'widget.marketParticipantDetail',


    bind: {
        data: '{marketParticipant}'
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    scrollable: true,
    
    initComponent: function () {

        var defaults = {
            xtype: 'displayfield',
            labelWidth: 150
        };

        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if(this.responsiveConfig){
            responsiveConfig = this.responsiveConfig;
        }

        var items = [
            {
                xtype: 'container',
                cls: 'bg-grey',
                margin: 10,
                padding: '5 10 0 10',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                defaults: defaults,
                items: this.getNorthItems()

            },
            {
                xtype: 'container',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                padding: '0 20 0 20',
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getWestItems()
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getEastItems()
                    }
                ]
            }
        ];

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    },
    /*
     * Defines the top area items for connection building properties
     */
    getNorthItems: function(){

        var items = [
            {
                fieldLabel: _tl.get('mam.label.marketParticipant.customId'),
                name: 'customId',
                flex: 1,
                bind: {
                    value: '{marketParticipant.customId}'
                }
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                flex: 1,
                items: [
                    {
                        fieldLabel: _tl.get('mam.label.marketParticipant.createdTimeStamp'),
                        flex: 1,
                        name: 'createdTimeStamp',
                        bind: {
                            value: '{marketParticipant.createdTimeStamp}'
                        }
                    },
                    {
                        fieldLabel: _tl.get('mam.label.marketParticipant.id'),
                        name: 'marketParticipantId',
                        bind: {
                            value: '{marketParticipant.id}'
                        }
                    }
                ]
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the left side of panel
     */
    getWestItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.marketParticipant.code'),
                name: 'code',
                bind: {
                    value: '{marketParticipant.code}'
                }
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the right side of panel
     */
    getEastItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.marketParticipant.name'),
                name: 'name',
                bind: {
                    value: '{marketParticipant.name}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.marketParticipant.role'),
                name: 'role',
                bind: {
                    value: '{marketParticipant.role}'
                }
            }
        ];
        return items;
    }
});