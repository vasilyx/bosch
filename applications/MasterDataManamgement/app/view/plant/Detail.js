Ext.define('MAM.view.plant.Detail', {
    extend: 'Ext.Panel',

    alias: 'widget.plantDetail',


    bind: {
        data: '{plant}'
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    scrollable: true,
    
    initComponent: function () {

        var defaults = {
            xtype: 'displayfield',
            labelWidth: 150
        };

        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if(this.responsiveConfig){
            responsiveConfig = this.responsiveConfig;
        }

        var items = [
            {
                xtype: 'container',
                cls: 'bg-grey',
                margin: 10,
                padding: '5 10 0 10',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                defaults: defaults,
                items: this.getNorthItems()

            },
            {
                xtype: 'container',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                padding: '0 20 0 20',
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getWestItems()
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getEastItems()
                    }
                ]
            }
        ];

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    },
    /*
     * Defines the top area items for plant properties
     */
    getNorthItems: function(){

        var items = [
            {
                fieldLabel: _tl.get('mam.label.plant.customId'),
                name: 'customId',
                flex: 1,
                bind: {
                    value: '{plant.customId}'
                }
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                flex: 1,
                items: [
                    {
                        fieldLabel: _tl.get('mam.label.plant.id'),
                        name: 'plantId',
                        bind: {
                            value: '{plant.id}'
                        }
                    }
                ]
            }
        ];
        return items;
    },
    /**
     * Defines the area items for plant properties
     * on the left side of panel
     */
    getWestItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.plant.type'),
                name: 'type',
                bind: {
                    value: '{plant.type}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.plant.model'),
                name: 'model',
                bind: {
                    value: '{plant.model}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.plant.manufacturer'),
                name: 'manufacturer',
                bind: {
                    value: '{plant.manufacturer}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.plant.primaryEnergy'),
                name: 'primaryEnergy',
                bind: {
                    value: '{plant.primaryEnergy}'
                }
            }
        ];
        return items;
    },
    /**
     * Defines the area items for plant properties
     * on the right side of panel
     */
    getEastItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.plant.description'),
                name: 'description',
                bind: {
                    value: '{plant.description}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.plant.active'),
                name: 'active',
                bind: {
                    value: '{plant.active}'
                }
            }
        ];
        return items;
    }
});