Ext.define('MAM.view.meter.CreateDetailController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.meterCreateDetailController',
    control: {
        '#meterCreateForm': {
            validitychange: 'onChangeCreateForm'
        },
        'meterCreateDetail button#saveViewButton': {
            click: 'onSaveView'
        },
        'meterCreateDetail button#saveEditButton': {
            click: 'onSaveEdit'
        },
        'meterCreateDetail button#closeButton': {
            click: 'onCloseCreateWindow'
        }
    },

    onSaveView: function() {
        this.create(false);
    },

    onSaveEdit: function() {
        this.create(true);
    },

    create: function(edit) {
        var record = Ext.create('MAM.model.MeterDetail', {}),
            me = this;
        record.set(this.getView().down('#meterCreateForm').getValues());
        delete record.data.id;
        record.save({
            success: function (request, response) {
                me.onCloseCreateWindow();
                Ext.getStore('meters').reload();
                var data = Ext.JSON.decode(response.getResponse().responseText);

                if (data.hasOwnProperty('id') && data.id) {
                    if (edit === true) {
                        me.fireEvent('openMeterEdit', data.id);
                    } else {
                        me.redirectTo('/'+ MAM.utils.Constants.METER + '/itemId/' + data.id);
                    }

                } else {
                    Ext.create('Energy.common.ui.DialogBox').showAlert('Server didn\'t return entity id that was created');
                }


            },
            failure: function (request, response) {
                switch (response.getError().status) {
                    case 400:
                        Ext.create('Energy.common.ui.DialogBox').showWarning(_tl.get('mam.dialogbox.meter.create.response400'));
                        break;
                    case 409:
                        Ext.create('Energy.common.ui.DialogBox').showWarning(_tl.get('mam.dialogbox.meter.create.response409'));
                        break;
                    default:
                        Ext.create('Energy.common.ui.DialogBox').showWarning('Unknown server response - ' + response.getError().status);
                }
            }
        });
    },

    onCloseCreateWindow: function() {
        if(this.getView().up().getXType() === 'actionWindow'){
            this.getView().up().close();
        }
    },

    onChangeCreateForm: function(form) {
        this.getView().down('#saveViewButton').setDisabled(!form.isValid());
        this.getView().down('#saveEditButton').setDisabled(!form.isValid());
    }
});