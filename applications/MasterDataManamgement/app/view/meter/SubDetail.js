Ext.define('MAM.view.meter.SubDetail', {
    extend: 'Ext.tab.Panel',
    requires: [
        'MAM.view.relations.Initial'
    ],
    alias: 'widget.meterSubDetail',
    initComponent: function () {
        var items = [];
        var tabConfig = MAM.utils.Configuration.getTabConfig( MAM.utils.Constants.METER );

        Ext.each(tabConfig, function( tab ){
           items.push({
               xtype: tab.type + 'Initial',
               title: _tl.get("mam.label." + MAM.utils.Constants.METER + "." + tab.type),
               itemId: MAM.utils.Constants.METERS + '-tab-' + tab.type
           }) 
        });

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    }
});