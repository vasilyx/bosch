Ext.define('MAM.model.action.history.History', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'key' },
        { name: 'entity' },
        { name: 'text' }
    ]
});