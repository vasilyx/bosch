/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Siesta.Harness.Browser.SenchaTouch.meta.extend({
    has         : {
        coverageUnit    : 'extjs_class'
    }
})
