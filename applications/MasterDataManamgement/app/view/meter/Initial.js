Ext.define('MAM.view.meter.Initial', {
    extend: 'Ext.container.Container',

    requires: [
        'MAM.view.meter.List',
        'MAM.view.meter.InitialModel',
        'MAM.view.meter.InitialController',
        'MAM.view.meter.Detail',
        'MAM.view.meter.SubDetail',
        'Energy.common.ui.Header'
    ],

    alias: 'widget.meterInitial',

    viewModel: 'meterInitialModel',
    controller: 'meterInitialController',

    layout: {
       type: 'hbox',
        align: 'stretch',
        resizable: true
    },
    padding: '0 0 0 10',

    initComponent: function () {

       this.callParent(arguments);

        this.createMetersList();
        this.addSelectMessage();
    },

    addSelectMessage: function(){
        this.lookupReference('detailContainer').removeAll();
        this.lookupReference('detailContainer').add({
            xtype: 'commonUiDisplayMessage',
            message: _tl.get('mam.label.ui.selectEntity')

        });
    },

    createMetersList: function(){
        this.add(
            {
                xtype: 'container',
                width: 300,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype:'commonUiHeader',
                        header: _tl.get('mam.label.meters'),
                        bind: {
                            header:  '{totalText}'
                        }

                    },
                    {
                        xtype: 'meterList',

                        flex: 1
                    }
                ]
            },
            {
                xtype: 'container',
                margin: '0 0 0 10',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                flex: 1,
                items: [
                    {
                        xtype:'commonUiHeader',
                        header: _tl.get('mam.label.meter'),
                        colorClass: 'green',
                        showArrow: true,
                        bind: {
                            header: _tl.get('mam.label.meter') + ' {meter.customId}',
                            subheader: '{editModeText}'
                        }

                    },
                    {
                        xtype: 'container',
                        cls: 'border-lrb',
                        reference: 'detailContainer',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        flex: 1
                    }

                ]
            }, {
                xtype: 'actionInitial',
                viewModel: {
                    data: {
                        entity: MAM.utils.Constants.METER
                    }
                }
            }
        );
    },

    createMeterDetailContainer: function(){
        var detail = this.lookupReference('detailContainer');
        detail.removeAll();
        detail.add(
            {
                xtype: 'meterDetail',
                flex: 1
            },
            {
                xtype: 'meterSubDetail',
                flex: 1
            }
        );
        
    },

    createMeterEditDetailContainer: function(){
        var detail = this.lookupReference('detailContainer');
        detail.removeAll();

        this.lookupReference('detailContainer').add(
            {
                xtype: 'meterEditDetail',
                flex: 1
            },
            {
                xtype: 'meterEditSubDetail',
                flex: 1
            }
        );
    }
});