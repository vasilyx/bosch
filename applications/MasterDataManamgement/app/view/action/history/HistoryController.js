Ext.define('MAM.view.action.history.HistoryController', {
    extend: 'Ext.app.ViewController',
    requires: [
        'MAM.store.action.history.History'
    ],
    alias: 'controller.historyController',
    listen: {
        controller: {
            '*': {
                openEntity: 'addEntity',
                removeItem: 'removeItem',
                'setIconEditStatus': 'setEditMode'
            },
            'meterPointInitialController': {
            }
        }
    },
    control: {
        'menu': {
            click: 'onClickMenuItem'
        },
        'button': {
            render:'onButtonRender'
        }
    },

    onButtonRender: function() {
        this.refreshMenu();
    },

    onClickMenuItem: function(menu, menuItem) {
        var storyItem;
        var me = this;

        Ext.getStore('history').each(function(record) {
            if (record.get('text') == menuItem.text) {
                storyItem = record.getData();
            }
        });

        if (storyItem) {
            if (this.getViewModel().get('editMode')) {
                Ext.create('Energy.common.ui.DialogBox').showQuestion(_tl.get("mam.dialogbox." + storyItem.entity + ".editDataLost"),
                    [
                        {
                            text: _tl.get("mam.dialogbox." + storyItem.entity + ".button.yes"),
                            itemId: 'buttonPositive',
                            handler: function () {
                                this.up().up().close();
                                me.getViewModel().set('editMode', false);
                                me.fireEvent('setIconEditStatus', storyItem.entity, true);
                                me.fireEvent('setIconSaveStatus', storyItem.entity, false);
                                me.redirectTo('/'+ storyItem.entity + '/itemId/' + storyItem.key);
                                return true;
                            }
                        },
                        {
                            text: _tl.get("mam.dialogbox." + storyItem.entity + ".button.no"),
                            itemId: 'buttonNegative',
                            handler: function () {
                                this.up().up().close();
                                return false;
                            }
                        }
                    ]


                );
            } else {
                me.redirectTo('/'+ storyItem.entity + '/itemId/' + storyItem.key);
            }
        }
    },

    addEntity:function(entity, record) {

        var storeHistory = Ext.getStore('history');
        storeHistory.sync();

        // no-record, only refresh
        if (record === null) {
            this.refreshMenu();
            return;
        }

        // Remove record with the same text and entity. new record will be added first
        var recordIndex = storeHistory.findBy(
            function(row, id){
                if (row.get('text') === record.get('customId') && row.get('entity') === entity) {
                    return true;
                }
                return false;
            }
        );

        if (recordIndex !==-1) {
            storeHistory.removeAt(recordIndex);
        }

        // Remove last record before insert if we have limit of items
        if (storeHistory.getCount() ==  MAM.utils.Configuration.getHistory().limit) {
            storeHistory.removeAt(MAM.utils.Configuration.getHistory().limit -1);
        }

        var newRecord = MAM.model.action.history.History({text: record.get('customId'), key: record.getId(), entity: entity});
        storeHistory.insert(0, newRecord);

        this.refreshMenu(newRecord);
    },

    refreshMenu: function(newRecord) {
        var storeHistory = Ext.getStore('history');
        storeHistory.sync();

        var menu = this.getView().getMenu();
        menu.removeAll();

        storeHistory.each(function(record) {
            menu.add({
                text: record.get('text'),
                iconCls: 'submenu-icon-' + record.get('entity'),
                hidden: ((record.data === newRecord) ? true : false)
            });
        });
    },

    removeItem: function(record, entity) {
        var storeHistory = Ext.getStore('history');
        var recordIndex = storeHistory.findBy(
            function(row, id){
                if (row.get('text') === record.get('customId') && row.get('entity') === entity) {
                    return true;
                }
                return false;
            }
        );

        if (recordIndex !==-1) {
            storeHistory.removeAt(recordIndex);
        }

        this.refreshMenu();
    },

    setEditMode: function(entity, editMode) {
    //    if (this.getViewModel().get('entity') !== entity) return;

        this.getViewModel().set('editMode', editMode);
    }
});