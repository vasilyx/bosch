StartTest(function (t) {

    referenceData = {
        "meterId": 1491119,
        "customId": "DE0700081632100000000000070238000",
        "consumptionAverage": 566733.0,
        "measuringType": "DIRECT",
        "medium": {
            en: "Electricity",
            de: "Elektrizität"
        },
        "consumptionForecast": 566733.0,
        "consumptionUnit": "kwh",
        "measuringProduct": "714"
    };

    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/detail_DE0700081632100000000000070238000.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/customAttributes_DE0700081632100000000000070238000.json', referenceData.meterId);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/relations_DE0700081632100000000000070238000.json', referenceData.meterId);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/r/details_1494726.json'); // for related
    MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/r/details_1480156.json'); // for related
    MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/r/details_1035879.json'); // for related

    //  list.testListViewArea();

    t.it('#1 Enter full entity id into filter filed', function (t) {
        t.chain(

            { waitForCQ : 'app-main tabbar' },
            { click : 'tabbar=> span.icon-meter'},
            { waitForCQ :  'meterList'},
            { waitForRowsVisible : 'meterList'},
            function (next) {
                t.doubleClick(t.getFirstRow(t.cq1('meterInitial gridpanel')));
                next();
            },
            {waitForCQ: 'meterDetail'},
            {waitForComponentVisible: 'meterDetail'},
            {
                action: 'wait',
                delay: 1500
            },
            function (next) {
                t.cqExists('meterDetail', 'meterDetail view exist');
                /*
                 for (var code in referenceData) {
                 var val = referenceData[code].hasOwnProperty(_locale.getLocale()) ? referenceData[code][_locale.getLocale()] : referenceData[code];
                 t.contentLike(t.cq1('meterPointDetail displayfield[name="' + code + '"]'), val, 'Found right  #' + code + ' ' + val);
                 }*/

            }
        )
    });
});