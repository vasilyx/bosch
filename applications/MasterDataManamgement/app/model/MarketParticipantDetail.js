Ext.define('MAM.model.MarketParticipantDetail', {
    extend: 'MAM.model.MarketParticipant',
    proxy: {
        type: 'rest',
        api: {
            create: undefined,
            read: '/MAM/' + MAM.Config.Backend.Versions + '/marketParticipants',
            update: undefined,
            destroy: undefined
        },
        headers: {
            'Accept': 'application/json'
        },
        extraParams: {
            expand: 'properties'
        }
    }
});