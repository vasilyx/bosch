Ext.define('MAM.model.SimCardDetail', {
    extend: 'MAM.model.SimCard',
    proxy: {
        type: 'rest',
        api: {
            create: undefined,
            read: '/MAM/' + MAM.Config.Backend.Versions + '/simCards',
            update: undefined,
            destroy: undefined
        },
        headers: {
            'Accept': 'application/json'
        },
        extraParams: {
            expand: 'properties'
        }
    }
});