Ext.define('MAM.view.customAttributes.InitialController', {
    extend: 'Ext.app.ViewController',
    requires: [
        'MAM.model.MeterDetail',
        'MAM.model.MeterPointDetail',
        'MAM.model.GatewayDetail',
        'MAM.model.Attribute'
    ],
    alias: 'controller.customAttributesInitialController',

    control: {
        '#addAttribute': {
            click: 'addAttribute'
        },
        'customAttributesEdit actioncolumn': {
            click: 'clickRemoveRecord'
        },
        'customAttributesEdit toolbar': {
            afterRender: 'onGridToolbarRender'
        },
        '#': {
            edit: 'onRowEdit'
        }
    },
    listen: {
        controller: {
            '*': {
                customattibuteschange: 'onCustomAttributesChange'
            }
        }
    },
    /**
     * Model customAttributes of current entity
     * @param customAttributes
     */
    onCustomAttributesChange: function( customAttributes , ownerEntity) {
        var grid = this.getView();
        var currentView = ownerEntity + '-tab-customAttributes';
        if(grid.getItemId() === currentView) {
            var store = this.getViewModel().getStore('customAttributes');
            store.removeAll();
            store.loadData(customAttributes.customAttributes().getRange());
        }
    },

    /**
     * Add new attribute (edit mode)
     * @param button
     */
    addAttribute:function(button) {
        var grid = button.up('gridpanel');
        var rowEditing = grid.getPlugin('rowediting');
        rowEditing.cancelEdit();

        var r = Ext.create('MAM.model.Attribute', {
            key: '',
            value: ''
        });

        grid.getStore().insert(0, r);
        rowEditing.startEdit(0, 0);

        this._addButtonDisable();
    },
    /**
     * Validate custom attribute record not empty
     * @param editor
     * @param context
     */
    onRowEdit: function( editor, context ){
        if(context.record.isValid()){
            this.fireEvent('editedCustomAttributes', MAM.utils.Constants.CUSTOMATTRIBUTESDETAIL, this.getViewModel().get('customAttributes').getRange())
        }
    },

    /**
     * @param grid
     * @param el
     * @param rowIndex
     */
    clickRemoveRecord: function(grid, el, rowIndex) {
        grid.getStore().removeAt(rowIndex);
        this._addButtonDisable();
    },

    onGridToolbarRender: function() {
        this._addButtonDisable();
    },

    _addButtonDisable: function() {
        if (this.getView().getStore().getCount()) {
            this.getView().down('#addAttribute').setDisabled(true);
        } else {
            this.getView().down('#addAttribute').setDisabled(false);
        }
    }
});