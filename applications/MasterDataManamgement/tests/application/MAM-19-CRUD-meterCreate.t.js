StartTest(function (t) {

    var referenceData = {
        id: 1494785,
        customId: 'G22175AAA25172'
    };

    t.it('#1 Click on create - window open', function (t) {
        t.chain(
            {waitForCQ: 'app-main tabbar'},
            {click: 'tabbar=> span.icon-meter'},
            {
                action      : 'wait',
                delay       : 1500
            },

            function (next) {
                t.click(t.cq1('meterInitial actionInitial toolbar button[cls=icon-create]'));
                t.elementIsVisible(t.cq1('meterCreateDetail'), 'Window is visible');
                t.click(t.cq1('meterCreateDetail button#closeButton'));
                t.elementIsNotVisible(t.cq1('meterCreateDetail'), 'Window is invisible');

                t.click(t.cq1('meterInitial actionInitial toolbar button[cls=icon-create]'));
                t.elementIsVisible(t.cq1('meterCreateDetail'), 'Window is visible again');
                next();
            }
        )
    });

    t.it('#2 Checking mandatory fields', function (t) {
        t.chain(
            function (next) {

                t.expect(t.cq1('meterCreateDetail  button#saveViewButton').isDisabled()).toBe(true);
                t.expect(t.cq1('meterCreateDetail  button#saveEditButton').isDisabled()).toBe(true);

                t.type(t.cq1('meterCreateDetail  textfield[name=customId]'), '01234567');
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.type(t.cq1('meterCreateDetail combo[name=medium]'), 'Electricity');
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.click(t.cq1('meterCreateDetail combo[name=customId]'));
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.type(t.cq1('meterCreateDetail combo[name=measuringPrinciple]'), 'Absolute');
                next();
            },{
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.click(t.cq1('meterCreateDetail combo[name=customId]'));
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.type(t.cq1('meterCreateDetail datefield[name=buildInDate]'), '12.12.2012');
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.click(t.cq1('meterCreateDetail combo[name=customId]'));
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.expect(t.cq1('meterCreateDetail button#saveViewButton').isDisabled()).toBe(false);
                t.expect(t.cq1('meterCreateDetail button#saveEditButton').isDisabled()).toBe(false);
                next();
            })}
    );

    t.it('#3 Check 409 response - attempt to create entity with the same Id', function (t) {
        t.chain(function (next) {
                MOCK_DATA.setMockedResponse('meters/', 'post', 409);

                t.click(t.cq1('meterCreateDetail button#saveViewButton'));
                t.elementIsVisible(t.compositeQuery('commonUiDialogBox => div.icon.warning'), 'Warning is visible');
                next();
            },
            {
                action: 'wait',
                delay: 1500
            }
        )
    });

    t.it('#4 After success created. window shows this entity in edit mode.', function (t) {
        t.chain(function (next) {
                t.click(t.cq1('commonUiDialogBox button'));
                MOCK_DATA.setMockedResponse('meters/', 'post', 201, Ext.JSON.encode( {id: referenceData.id}));
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/details_' + referenceData.customId + '.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/customAttributes_dummy.json', referenceData.id);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/relations_dummy.json', referenceData.id);
                t.click(t.cq1('meterCreateDetail button#saveEditButton'));

                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.cqExists('meterEditDetail', 'meterEditDetail view exist');
                t.contentLike(t.cq1('meterEditDetail displayfield[name="customId"]'), referenceData.customId, 'Found right customId');
                t.cqExists('customAttributesEdit', 'customAttributesEdit view exist');
                t.cqExists('relationsEdit', 'relationsEdit view exist');
            })}
    );

    t.it('#5 After success created. window shows this entity in view mode.', function (t) {
        t.chain(
            function (next) {
                t.click(t.cq1('meterInitial actionInitial toolbar button[cls=icon-create]'));
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.type(t.cq1('meterCreateDetail  textfield[name=customId]'), '01234567');
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.click(t.cq1('meterCreateDetail combo[name=customId]'));
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },

            function (next) {
                t.click(t.cq1('meterCreateDetail button#saveViewButton'));
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.cqExists('meterDetail', 'meterDetail view exist');
                t.contentLike(t.cq1('meterDetail displayfield[name="customId"]'), referenceData.customId, 'Found right customId');
                t.cqExists('customAttributesInitial', 'customAttributes view exist');
                t.cqExists('relationsInitial', 'Relations view exist');
            })}
    );
});
