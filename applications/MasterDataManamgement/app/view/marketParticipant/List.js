Ext.define('MAM.view.marketParticipant.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.marketParticipantList',
    requires: [
        'MAM.view.marketParticipant.ListModel',
        'MAM.view.marketParticipant.ListController'
    ],
    viewModel: 'marketParticipantListModel',
    controller: 'marketParticipantListController',
    border: true,
    viewConfig: {
        loadMask: false
    },
    bind: {
        store: '{marketParticipants}'
    },
    dockedItems: [{
        xtype: 'pagingtoolbar',
        reference: 'pagingToolbar',
        bind: {
            store: '{marketParticipants}'
        },
        overflowHandler: 'menu',
        dock: 'bottom',
        displayInfo: true
    }],
    initComponent: function () {
        var columns = [
            {
                text: _tl.get('mam.label.marketParticipant.customId'),
                dataIndex: 'customId',
                flex: 1,
                items: [{
                    xtype: 'textfield',
                    margin: 3,
                    flex: 1,
                    width: 270, //theme error
                    enableKeyEvents: true,
                    listeners: {
                        keyup: 'onCustomIdSearch',
                        buffer: 1000
                    }
                }]
            }
        ];
        Ext.apply(this, {columns: columns});
        this.callParent(arguments);
    }

});