Ext.define('TestApplication.application.AbstractList', {
    config: {
        t: null, // test object
        name : '', // name of list
        nameList: '',
        nameInitial: '',
        nameDetail: ''
    },

    rowsCountOnPage: 50,

    //singleton: true,
    constructor: function(config) {
        this.initConfig(config);
        this.setNameList(config.name + 'List');
        this.setNameInitial(config.name + 'Initial');
        this.setNameDetail(config.name + 'Detail');
    },

    // #1 List view area => Expected Result: List is visible and xxx number of entitites are displayed in header
    testListViewArea: function() {
        var t = this.getT();
        var al = this;
        t.it('#1 List view area', function(t){
            t.chain(

                { waitForCQ : 'app-main tabbar' },
                { click : 'tabbar=> span.icon-' + al.getName()},
                { waitForCQ :  al.getNameList()},

                { waitForRowsVisible : al.getNameList()},

                function(next){
                    var list = t.cq1(al.getNameList());
                    t.elementIsVisible(list, al.getNameList() + ' is visible');
                    t.contentLike(al.getNameInitial() + ' => div.content.bg-darkGrey span.header', Ext.util.Format.number(list.getStore().getTotalCount(), '0,000'), 'Total count of ' + list.getStore().getTotalCount() + ' in header is correct');
                    next();
                }
            );
        });
    },

    // #2 List is filled with Data => Expected Result: 50 Items are listed each page
    testFilledDataInList: function(numRowsLastPage){
        var t = this.getT();
        var al = this;
        t.it('#2 List is filled with Data', function(t){
          t.chain(
                function(next) {
                    // set last page mock data
                    MOCK_DATA.getMockedData('tests/data/' + al.getName() + '3.json');
                    next();

                },
                { click : '#main-tabpanel ' + al.getNameList() + ' #inputItem  => .x-form-text' },
                { target : '#main-tabpanel ' + al.getNameList() + ' #inputItem  => .x-form-text', type : "3[ENTER]", clearExisting:true },
                {
                    action      : 'wait',
                    delay       : 1500      // 1500 ms
                },
                function(next){
                    t.expect(t.cq1(al.getNameList()).getStore().getCount()).toBe(al.rowsCountOnPage);
                    next();
                },
                // number of rows on last page: should be 16
                function(next) {
                    // set last page mock data
                    MOCK_DATA.getMockedData('tests/data/' + al.getName() + 'LastPage.json');
                    next();

                },
            //    { click:  al.getNameList() + ' button[iconCls=x-toolbar-more-icon] => .x-btn-icon-el'},
               // t.click(t.compositeQuery())
               { click: al.getNameList() + ' pagingtoolbar  => a.x-btn-plain-toolbar-small:last'},
                { click: '#main-tabpanel ' + al.getNameList() + ' menuitem[text=letzte Seite] => .x-menu-item-text'},
                {
                    action      : 'wait',
                    delay       : 1500      // 1500 ms
                },
                function(next) {
                    //t.waitForEvent(t.cq1(al.getNameList()).getView(), 'refresh', next);
                    t.expect(t.cq1(al.getNameList()).getStore().getCount()).toBe(numRowsLastPage);
                    next();
                }

            );
        });
    },

    // #3 Paging is displayed correct => Expected Result: sum of items : 50 = number of pages
    testPageCorrect: function() {
        var t = this.getT();
        var al = this;
        var pageInputItem = '#main-tabpanel ' + al.getNameList() + ' #inputItem  => .x-form-text';
        t.it('#3 Paging is displayed correct', function(t){
            t.chain(
                // mock request again with 50 items
                function(next) {
                    // set last page mock data
                    MOCK_DATA.getMockedData('tests/data/' + al.getName() + '.json');
                    next();

                },
                { click : pageInputItem },
                { target : pageInputItem, type : "1[ENTER]", clearExisting:true },
                {
                    action      : 'wait',
                    delay       : 1500      // 100 ms
                },

                function(next){
                    var grid = t.cq1(al.getNameList());
                    t.expect(grid.getStore().getCount()).toBe(al.rowsCountOnPage);
                    t.elementIsVisible(t.cq( al.getNameList() + ' pagingtoolbar tbtext')[1], 'Pages counter is visible');
                    t.contentLike(t.cq( al.getNameList() + ' pagingtoolbar tbtext')[1], Math.ceil(grid.getStore().getTotalCount()/al.rowsCountOnPage), 'Number of pages is correct');
                    next();
                }
            );
        });
    },

    // #4 Detail view area => Expected Result: Detail view should be empty
    testDetailArea: function() {
        var t = this.getT();
        var al = this;
        t.it('#4 Detail view area', function(t){
            t.chain(
                function(next){
                    t.cqNotExists(al.getNameDetail(), 'Detail view is not visible');
                    next();
                }
            );
        });
    },

    // #5 Enter full entity id into filter filed => Expected Result: one entitiy is shown in the list
    testFilterById: function(search) {
        var t = this.getT();
        var al = this;
        t.it('#5 Enter full entity id into filter filed', function(t) {
            t.chain(
                { waitForCQ : al.getNameList() },

                { waitForRowsVisible : al.getNameList() },


                { type : search + '[ENTER]',
                        target : t.cq1(al.getNameList() + ' textfield'), clearExisting:true }, // filter textfield
                function(next) {
                    // set last page mock data
                    MOCK_DATA.getMockedData('tests/data/' + al.getName() + 'Filter.json');
                    next();

                },
                {
                    action      : 'wait',
                    delay       : 1500      // 1500 ms
                },
                function(next){
                    t.contentLike(al.getNameInitial() + ' => div.content.bg-darkGrey span.header',
                        Ext.util.Format.number(t.cq1(al.getNameList()).getStore().getTotalCount(), '0,000'),
                        'Total count in header is correct');

                    t.expect(t.cq1(al.getNameList()).getStore().getCount()).toBe(1);
                    next();
                }
            )
        });
    },

    // #6 Remove filtered entity => Expected Result: 50 Items are listed each page and xxx number of entitites (same value like tc #1) are displayed in header
    testFilterEmpty: function() {
        var t = this.getT();
        var al = this;
        t.it('#6 Remove filtered entity', function(t) {
            t.chain(
                { type : '[ENTER]', target : t.cq1(al.getNameList() + ' textfield'), clearExisting:true},
                function(next) {
                    // set full page mock data
                    MOCK_DATA.getMockedData('tests/data/' + al.getName() + '.json');
                    next();
                },
                function(next) {
                    t.waitForEvent(t.cq1(al.getNameInitial() + ' gridpanel').getView(), 'refresh', next);
                },
                function(next) {
                    t.expect(t.cq1(al.getNameList()).getStore().getCount()).toBe(al.rowsCountOnPage);
                }
            )
        });
    },

    // #7 Select entity => Expected Result: Detail view is shown and filled with data (id of entity should match with selected entity)
    /**
     * @param {int} num - number of row
     * @param {int} testNum - number of test, only for title, default 7
     */
    testSelectRow: function(num, testNum) {
        var t = this.getT();
        var al = this;

        t.it('#' + (testNum > 0 ? testNum: '7') + ' Select entity num=' + (num+1), function(t) {
            t.chain(
                { doubleclick : t.compositeQuery(al.getNameInitial() + ' gridpanel => .x-grid-cell')[num] },
                {
                    action      : 'wait',
                    delay       : 1500      // 1500 ms
                },
                function(next) {
                    var customId = t.cq1(al.getNameInitial() + ' gridpanel').getStore().getAt(num).get('customId');
                    t.contentLike(al.getNameInitial() + ' => div.content.bg-green span.header', customId, 'Found entity ID');
                    t.cqExists(al.getNameDetail(), 'Found ' + al.getName() + ' details');
                    next();
                },
                { waitForComponentVisible : al.getNameDetail() }
            )
        });
    }
});

