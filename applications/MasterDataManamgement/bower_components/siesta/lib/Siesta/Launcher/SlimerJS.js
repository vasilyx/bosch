/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Siesta.Launcher.SlimerJS', {
    
    isa         : Siesta.Launcher.PhantomJS,
    
    does        : Siesta.Launcher.FileSystem.SlimerJS,
    
    has : {
        executableName          : 'slimerjs',
        knownOptions            : {
            init : [
            ]
        }
    },
    
    
    methods : {
        
        // this method runs before the "prepareOptions" call
        createRunners : function () {
            return [
                new Siesta.Launcher.Runner.SlimerJS({
                    launcher    : this
                })
            ]
        },
        
        
        printVersion : function () {
            this.SUPERARG(arguments)
            
            var version     = slimer.version
            
            this.print("SlimerJS " + version.major + '.' + version.minor + '.' + version.patch)
        },
        
        
        doExit : function (code) {
            slimer.exit(code)
        }
    }
    // eof methods
})
