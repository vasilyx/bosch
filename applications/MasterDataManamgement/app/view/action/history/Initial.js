Ext.define('MAM.view.action.history.Initial', {
    extend: 'Ext.button.Button',
    controller: 'historyController',
    requires: ['MAM.utils.Configuration'],
    alias: 'widget.historyInitial',
    viewModel: 'actionHistoryInitialModel',
    text: '',
    cls: 'icon-history',
    arrowAlign: 'bottom',
    overCls: ['icon-over-history', 'btn-over'],
    style: 'border-radius:0',
    height: 60,
    width: 80,
    menu: [],

    initComponent: function () {
        this.text = _tl.get('mam.label.actionPanel.history');

        var storeHistory = Ext.create('MAM.store.action.history.History');
        storeHistory.load();

        this.callParent(arguments);
    }
});