Ext.define('TestApplication.data.MockConfig', {

    actionPanel: {
        rules: {
            meterPoint: ['create', 'edit', 'save', 'delete'],
            gateway: ['edit', 'save'],
            meter: ['create', 'edit', 'save', 'delete'],
            person: ['edit', 'save', 'delete'],
            connectionBuilding: ['edit']
        },
        subMenu: {
            views: [
                {
                    text: 'mam.label.actionPanel.marketParticipantHistory',
                    entity: ['meterPoint'],
                    action: 'actionMarketParticipantHistory'
                },
                {
                    text: 'mam.label.actionPanel.meterReadings',
                    action: 'actionMeterReadings',
                    entity: ['meterPoint']
                }
            ],
            actions: [
                {
                    text: 'mam.label.actionPanel.getLogData',
                    entity: ['gateway']
                },
                {
                    text: 'mam.label.actionPanel.onDemandDelivery',
                    entity: ['meterPoint', 'gateway']
                }
            ]
        }
    }

});