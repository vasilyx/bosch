StartTest(function (t) {

    var list = Ext.create('TestApplication.application.AbstractList', {t: t, name: 'meterPoint'}),

        referenceData = {
            "meterPointId": 1491119,
            "customId": "DE0700081632100000000000070238000",
            "consumptionAverage": 566733.0,
            "measuringType": "DIRECT",
            "medium": {
                    en: "Electricity",
                    de: "Elektrizität"
            },
            "consumptionForecast": 566733.0,
            "consumptionUnit": "kwh",
            "measuringProduct": "714"
        },
        customAttributes = [
            {
                "key": {
                    en: "Consumption Type",
                    de: "Verbrauchstyp"
                },
                "value": "G"
            }
        ],
        referenceDataDetail = {
            "customId": "G22175AAA25185",
            "deviceType": "22175",
            "manufacturer": "GASELAN"
        },
        personRelation = {
            "customId": "70380913",
            "name": "Georg Mustermann"
        },
        buildingRelation = {
            "customId": "81078588",
            "municipalityKey": "12060005"
        };

    list.testListViewArea();

    t.it('#2 Enter full entity id into filter filed', function (t) {
        t.chain(
            function (next) {
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/detail_DE0700081632100000000000070238000.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/customAttributes_DE0700081632100000000000070238000.json', referenceData.meterPointId);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/relations_DE0700081632100000000000070238000.json', referenceData.meterPointId);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/r/details_1494726.json'); // for related
                MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/r/details_1480156.json'); // for related
                MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/r/details_1035879.json'); // for related
                next();
            },
            {doubleclick: t.getFirstRow(t.cq1('meterPointInitial gridpanel'))},
            {waitForCQ: 'meterPointDetail'},
            {waitForComponentVisible: 'meterPointDetail'},
            {
                action: 'wait',
                delay: 1500      // 1500 ms
            },
            function (next) {
                t.cqExists('meterPointDetail', 'meterPointDetail view exist');

                for (var code in referenceData) {
                    var val = referenceData[code].hasOwnProperty(_locale.getLocale()) ? referenceData[code][_locale.getLocale()] : referenceData[code];
                    t.contentLike(t.cq1('meterPointDetail displayfield[name="' + code + '"]'), val, 'Found right  #' + code + ' ' + val);
                }

            }
        )
    });

    //two tabs are visible => Expected Result: Relations of the Meter Point and Custom Attributes of the Meter Point
    t.it('#3 Two tabs are visible', function (t) {
        t.chain(
            {
                waitForCQ: 'meterPointSubDetail'
            }, function (next) {
                t.cqExists('relationsInitial', 'Relations tab exist');
                t.cqExists('customAttributesInitial', 'Custom Attributes tab exist');
                next();
            }
        );
    });

    //#4 first tab contains relations => Expected Result: 3 relations availalbe (Meter, Persons, Connection Buildings)
    t.it('#4 First tab contains relations', function (t) {
        t.chain(
            {
                click: t.compositeQuery('meterPointSubDetail tabbar => .x-tab')[0]
            }, {
                waitForCQ: 'meterDetail'
            }, function () {
                var accordionEl = t.cq('relationsInitial #relationAccordion grid');

                t.expect(accordionEl.length).toBe(3);

                t.like(accordionEl[0].getTitle(), Energy.common.language.Translations.get("mam.label.meter"), 'Accordion meter is visible');
                t.like(accordionEl[1].getTitle(), Energy.common.language.Translations.get("mam.label.persons"), 'Accordion persons is visible');
                t.like(accordionEl[2].getTitle(), Energy.common.language.Translations.get("mam.label.connectionBuildings"), 'Accordion connectionBuildings is visible');
            }
        )
    });

    //#5 meter relation view => Expected Result: one Meter and detail view is visible (Check MeterId: G22175AAA25185, Device Type: 22715 and Manufacturer: GASELAN)
    t.it('#5 Meter relation view', function (t) {
        t.chain(
            {
                click: t.compositeQuery('meterPointDetail relationsInitial grid => .x-grid-cell')[1],
                waitForComponentVisible: 'meterDetail'
            },
            function (next) {
                t.expect(t.cq1('meterPointSubDetail relationsInitial grid').getStore().getCount()).toBe(1);
                t.elementIsVisible(t.cq1('relationsInitial meterDetail'), 'MeterDetail is visible');

                for (var code in referenceDataDetail) {
                    t.contentLike(t.cq1('relationsInitial meterDetail displayfield[name="' + code + '"]'), referenceDataDetail[code], 'Found right  #' + code + ' ' + referenceDataDetail[code]);
                }
            }
        )
    });

    //#6 persons relation view => Expected Result: one Person and detail view is visible (Check Person Id 70380913, Full Name: Georg Mustermann)
    t.it('#6 Persons relation view', function (t) {
        t.chain(
            function (next) {
                var accordionEl = t.cq1('relationsInitial #relationAccordion');
                accordionEl.items.getAt(1).expand(); // person
                t.expect(t.cq('meterPointSubDetail relationsInitial grid')[1].getStore().getCount()).toBe(1); // person
                next();
            },
            {doubleclick: t.getFirstRow(t.cq('meterPointSubDetail relationsInitial grid')[1])},

            {
                waitForComponentVisible: 'personDetail'
            },
            function (next) {

                t.elementIsVisible(t.cq1('relationsInitial personDetail'), 'PersonDetail is visible');

                for (var code in personRelation) {
                    t.contentLike(t.cq1('relationsInitial personDetail displayfield[name="' + code + '"]'), personRelation[code], 'Found right  #' + code + ' ' + personRelation[code]);
                }
            }
        )
    });

    //#7 connection building relation view => Expected Result: one Connection Building and detail view is visible (Check Connection Building Id 81078588, Municipality Key (AGS): 12060005)
    t.it('#7 Connection building relation view', function (t) {
        t.chain(
            function (next) {
                var accordionEl = t.cq1('relationsInitial #relationAccordion');
                accordionEl.items.getAt(2).expand(); // connectionBuilding
                t.expect(t.cq('meterPointSubDetail relationsInitial grid')[2].getStore().getCount()).toBe(1); // person
                next();
            },
            {doubleclick: t.getFirstRow(t.cq('meterPointSubDetail relationsInitial grid')[2])},

            {
                waitForComponentVisible: 'connectionBuildingDetail'
            },
            function (next) {

                t.elementIsVisible(t.cq1('relationsInitial connectionBuildingDetail'), 'ConnectionBuildingDetail is visible');

                for (var code in buildingRelation) {
                    t.contentLike(t.cq1('relationsInitial connectionBuildingDetail displayfield[name="' + code + '"]'), buildingRelation[code], 'Found right  #' + code + ' ' + buildingRelation[code]);
                }
            }
        )
    });

    //#8 Custom Attributes of the Meter Point tab => Expected Result: one item with values key: Consumption Type and Value: G
    t.it('#8 Custom Attributes of the Meter Point tab', function (t) {

        t.chain(
            {
                click: t.compositeQuery('meterPointSubDetail tabbar => .x-tab')[1]
            }, {
                waitForCQ: 'connectionBuildingDetail'
            }, function () {
                t.elementIsVisible(t.cq1('connectionBuildingDetail'), 'connectionBuildings tab is visible');
                //TODO: DET_CONNB (review 25.11.2016) detail view values test
            }
        )
    });

    //#8 Custom Attributes of the Meter Point tab => Expected Result: one item with values key: Consumption Type and Value: G
    t.it('#8 Custom Attributes of the Meter Point tab', function (t) {
        t.chain(
            {
                click: t.compositeQuery('meterPointSubDetail tabbar => .x-tab')[1]
            }, {
                waitForCQ: 'customAttributesInitial'
            }, function () {
                var detailsData = t.cq1('customAttributesInitial').getStore(),
                    rows = t.compositeQuery('customAttributesInitial => .x-grid-row');

                t.expect(detailsData.getCount()).toBe(customAttributes.length);
                for (var index in customAttributes) {
                    var keyTranslated = customAttributes[index].key.hasOwnProperty(_locale.getLocale()) ? customAttributes[index].key[_locale.getLocale()] : customAttributes[index].key;

                    t.like(detailsData.getAt(index).get('key'), keyTranslated);
                    t.like(detailsData.getAt(index).get('value') || '', customAttributes[index].value || '');
                }
            }
        );
    });
});