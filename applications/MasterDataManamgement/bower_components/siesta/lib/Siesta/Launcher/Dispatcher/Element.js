/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Siesta.Launcher.Dispatcher.Element', {
    
    does    : [
        Siesta.Util.Role.HasUniqueGeneratedId
    ],
    
    has : {
        descId              : null,
        
        inProgress          : false,
        
        processed           : false,
        processedCount      : 0,
        maxProcessedCount   : 2,
        
        result              : null,
        
        belongsTo           : { required : true },
        
        resultPrinted       : false
    },
    
    
    methods : {
        
        startProgress : function () {
            if (this.processed) throw new Error("Element already processed" + this.descId)
            if (this.inProgress) throw new Error("Element already in progress" + this.descId)
            
            this.inProgress     = true
        },
        
        
        canRunAgain : function (withThisTime) {
            return this.processedCount + (withThisTime ? 1 : 0) < this.maxProcessedCount
        },
        
        
        reset : function (noError) {
            if (!this.inProgress) throw new Error("Element is not in progress" + this.descId)
            if (this.processed) throw new Error("Element already processed" + this.descId)
            
            if (noError) {
                this.inProgress     = false
            } else {
                if (!this.canRunAgain(true)) {
                    this.endProgress({
                        url     : this.descId,
                        ERROR   : "Failed to run the test: " + this.descId 
                    })
                    
                    return true
                } else {
                    this.processedCount++
                    this.inProgress     = false
                }
            }
        },
        
        
        setProcessed : function (value) {
            if (this.processed != value) {
                this.processed  = value
                
                value ? this.belongsTo.done++ : this.belongsTo.done--
            }
        },
        
        
        endProgress : function (result) {
            if (this.processed) throw new Error("Element already processed" + this.descId)
            if (!this.inProgress) throw new Error("Element is not in progress: " + this.descId)
            
            this.processedCount++
            
            this.inProgress     = false
            this.result         = result
            
            this.setProcessed(true)
        },
        
        
        asJSON : function () {
            return {
                automationElementId : this.id,
                descId              : this.descId
            }
        },
        
        
        canPrintResultImmediately : function () {
            var result      = this.result
            
            return Boolean(result.ERROR || result.passed || result.isTodo)
        }
    }
})
