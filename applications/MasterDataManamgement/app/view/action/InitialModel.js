Ext.define('MAM.view.action.InitialModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.actionInitialModel',
    data: {
        entity: null,
        editMode: false
    }
});
