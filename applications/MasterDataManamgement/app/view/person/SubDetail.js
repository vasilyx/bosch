Ext.define('MAM.view.person.SubDetail', {
    extend: 'Ext.tab.Panel',
    requires: [
        'MAM.view.relations.Initial'
    ],
    alias: 'widget.personSubDetail',
    //plain: true,
    initComponent: function () {
        var items = [];
        var tabConfig = MAM.utils.Configuration.getTabConfig( MAM.utils.Constants.PERSON );

        Ext.each(tabConfig, function( tab ){
           items.push({
               xtype: tab.type + 'Initial',
               title: _tl.get("mam.label." + MAM.utils.Constants.PERSON + "." + tab.type),
               itemId: MAM.utils.Constants.PERSONS + '-tab-' + tab.type
           }) 
        });

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    }
});