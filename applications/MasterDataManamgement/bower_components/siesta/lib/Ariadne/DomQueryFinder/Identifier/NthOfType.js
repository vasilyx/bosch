/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Ariadne.DomQueryFinder.Identifier.NthOfType', {
    
    isa         : Ariadne.QueryFinder.Identifier,
    
    
    methods : {
        
        identify : function (target, root, maze) {
            var parentNode      = target.parentNode
            if (!parentNode) return null
            
            var siblings        = parentNode.children
            
            var counter         = 0
            
            var tagName         = target.tagName.toLowerCase()
            
            for (var i = 0; i < siblings.length; i++) {
                if (siblings[ i ].tagName.toLowerCase() == tagName) {
                    counter++
                    
                    if (siblings[ i ] == target) break
                }
            }
            
            return {
                query           : ':nth-of-type(' + counter + ')',
                weight          : 1e6
            }
        }
    }
});
