/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Launcher.CommandLineTool.WebDriverRhinoTool', {
    
    does    : [
        Siesta.Launcher.CommandLineTool.WebDriverGenericTool
    ],
    
    methods : {
        
        setChromeDriverServerPath : function () {
            java.lang.System.setProperty("webdriver.chrome.driver", this.getChromeDriverServerPath(true))
        },
        
        
        setIEDriverServerPath : function () {
            if (!this.isWindows) return
            
            java.lang.System.setProperty("webdriver.ie.driver", this.getIEDriverServerPath(true))
        }
    }
    // eof methods
})
