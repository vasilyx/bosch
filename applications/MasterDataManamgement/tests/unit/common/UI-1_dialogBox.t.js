StartTest(function(t) {
    t.diag("Creating dialogBox windows");


    var reference = {
        message: 'Shakespeare\'s first plays were written in the conventional style of the day. He wrote them in a stylised language that does not always spring naturally from the needs of the characters or the drama'
    };

    // create an message box of information type without buttons and with displayTime of 3 sec: expected result: message box is shown with the right icon without buttons and dissapears after 5 sec
    t.it('#1 create an message box of information type', function (t) {
        t.chain(
           function (next) {
               Ext.create('Energy.common.ui.DialogBox').showInformation(reference.message, false);
               t.elementIsVisible(t.cq1('commonUiDialogBox'), 'Information dialogBox is visible');
               t.elementIsVisible(t.compositeQuery('commonUiDialogBox => div.icon.information'), 'Information icon is visible');

               t.cqNotExists('commonUiDialogBox button', 'Button is not created');
               next();
           },
            {
                action      : 'wait',
                delay       : 5000 // live time of element 3000
            },
            function (next) {
                t.elementIsNotVisible(t.cq1('commonUiDialogBox'), 'Information dialogBox is destroyed');
            }
        );
    });

    // create an message box of succes type and OK button: expected result: message box is shown with OK button and the right icon
    t.it('#2 Create an message box of success type', function (t) {
        t.chain(
            function (next) {
                Ext.create('Energy.common.ui.DialogBox').showSuccess();
                t.elementIsVisible(t.cq1('commonUiDialogBox'), 'Success dialogBox is visible');
                t.elementIsVisible(t.compositeQuery('commonUiDialogBox => div.icon.success'), 'Success icon is visible');

                t.cqExists('commonUiDialogBox button', 'Button is created');
                t.click(t.cq1('commonUiDialogBox button'));
                t.elementIsNotVisible(t.cq1('commonUiDialogBox'), 'Success dialogBox was closed after click button');
            }
        );
    });

    // create an message box of question type and Save and Cancel button: expected result: message box is shown with Save and Cancel button and the right icon
    t.it('#3 Create an message box of question type', function (t) {
        t.chain(
            function (next) {
                 var win = Ext.create('Energy.common.ui.DialogBox', {});
                 win.showQuestion(reference.message, [
                    {
                        text: 'Save'
                    },
                    {
                        text: 'Cancel'
                    }
                ]);

                t.elementIsVisible(t.cq1('commonUiDialogBox'), 'Question dialogBox is visible');
                t.elementIsVisible(t.compositeQuery('commonUiDialogBox => div.icon.question'), 'Question icon is visible');
                t.elementIsVisible(t.cq1('commonUiDialogBox button[text=Save]'), 'Button "Save" is visible');
                t.elementIsVisible(t.cq1('commonUiDialogBox button[text=Cancel]'), 'Button "Cancel" is visible');
                win.close();
            }
        );
    });

    // create an message box of error type and OK button: expected result: message box is shown with OK button and the right icon
    t.it('#4 Create an message box of error type', function (t) {
        t.chain(
            function (next) {
                var win = Ext.create('Energy.common.ui.DialogBox');
                win.showError(reference.message, false);
                t.elementIsVisible(t.cq1('commonUiDialogBox'), 'Error dialogBox is visible');
                t.elementIsVisible(t.compositeQuery('commonUiDialogBox => div.icon.error'), 'Error icon is visible');
                win.close();
            }
        );
    });

    // create a message box with huge content (a component with 700px height) and OK button: expected result: gird is displayed an scrollbars are shown
    t.it('#5 Create an message with huge content', function (t) {
        t.chain(
            function (next) {
                var i = 0;
                var message = '';
                while (i < 20) {
                    message += reference.message;
                    i++;
                }

                var win = Ext.create('Energy.common.ui.DialogBox', {
                    msgContent: message
                });

                win.show();
                t.elementIsVisible(t.cq1('commonUiDialogBox'), 'DialogBox is visible');
                t.expect(t.cq1('commonUiDialogBox').getHeight()).toBeLessThan(700); // TODO found out another way to check scrollbar in element
                win.close();
            }
        );
    });

    // create an message box of warning type and OK button: message like a object
    t.it('#6 Create an message box of warning type and message like a object', function (t) {
        t.chain(
            function (next) {
                Ext.create('Energy.common.ui.DialogBox').showWarning([{xtype: 'checkbox', boxLabel: 'first'}, {xtype: 'checkbox', boxLabel: 'second'}], false);
                t.elementIsVisible(t.cq1('commonUiDialogBox'), 'Warning dialogBox is visible');
                t.elementIsVisible(t.cq1('commonUiDialogBox checkbox'), 'Checkbox in dialogBox is visible');
                t.elementIsVisible(t.compositeQuery('commonUiDialogBox => div.icon.warning'), 'Warning icon is visible');
            }
        );
    });
});