Ext.define('MAM.view.main.Main', {
    extend: 'Ext.Panel',
    xtype: 'app-main',
    requires: [
        'Ext.plugin.Viewport',
        'MAM.utils.Configuration',
        'MAM.utils.Constants',
        'MAM.view.main.MainController',
        'MAM.view.main.MainModel',
        'MAM.view.*'
    ],

    controller: 'main',
    viewModel: 'main',

    layout: 'fit',

    initComponent: function () {

        var items = [
        {
            xtype: 'tabpanel',
            ui: 'navigation',
            itemId: 'main-tabpanel',
            border: true,
            tabPosition: 'left',
            tabRotation: 0,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            defaults: {
                iconAlign: 'top',
                textAlign: 'center',
                bodyPadding: 15
            },
            items: this.createNavigation()
        }];
        Ext.apply(this, {items: items});
        this.callParent(arguments);
    },
    createNavigation: function(){
        var items = [];
        Ext.each(MAM.utils.Configuration.getNavConfig(), function( item ){
            items.push({
                title: _tl.get('mam.label.' + item.label + '.navigation' ),
                xtype: item.view,
                tabConfig: {
                    mamType: item.type
                },
                iconCls: 'icon-' + item.type,
                flex: 1
            });
        });
        return items;
    }
});
