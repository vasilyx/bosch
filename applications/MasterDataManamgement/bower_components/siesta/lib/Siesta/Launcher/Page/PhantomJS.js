/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Siesta.Launcher.Page.PhantomJS', {
    
    isa         : Siesta.Launcher.Page.BasePage,
    
    has : {
        page            : {
            lazy        : 'buildPage'
        }
    },
    
    
    methods : {
        
        buildPage : function () {
            var me      = this
            
            return require("webpage").create({
                viewportSize        : {
                    width       : this.width,
                    height      : this.height
                },
                
                onConsoleMessage : function (msg) {
                    console.log(msg)
                }
            })
        },
        
        
        screenshot : function (command) {
//            var tempFile        = this.driver.getScreenshotAs(selenium.OutputType.FILE)
//            
//            var fileName        = this.launcher.normalizePath(command.fileName.replace(/(\.png)?$/, '.png'))
//            
//            this.launcher.ensurePathExists(fileName)
//            
//            org.apache.commons.io.FileUtils.copyFile(tempFile, new java.io.File(fileName))            
        },
        
        
        open : function (url, callback) {
            var page        = this.getPage()
            
            page.open(url, function (status) {
                if (status == 'success') 
                    // TODO need to properly wait for activeHarness.setupDone flag to be set
                    // (in the openHarness method)
                    setTimeout(callback, 100)
                else
                    callback("Can't open page: " + url)
            })
        },
            
            
        close : function () {
            this.debug("Close page PhantomJS")
            
            try {
                this.getPage().close()
            } catch (e) {
                this.debug("Exception during `page.close()`: " + e)
            }
            
            this.page   = null
            
            return this.SUPER()
        },
        
        
        executeSmallScriptPromised : function (text, ignoreException) {
            var me          = this
            
            return new Promise(function (CONT, THROW) {
                var func    = eval('(function() {' + text + '})')
                
                try {
                    var res = me.getPage().evaluate(func)
                } catch (e) {
                    if (!ignoreException) {
                        me.print("<Exception from launcher>")
                        me.print("    While running small script: " + text.substring(0, 150))
                        me.print("    Exception: " + e)
                        me.print("</Exception from launcher>")
                        
                        THROW(e)
                    }
                    
                    CONT(null)
                    
                    return
                }
                
                CONT(res)
            })
        }
    }
    // eof methods
})
