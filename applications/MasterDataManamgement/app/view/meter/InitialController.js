Ext.define('MAM.view.meter.InitialController', {
    extend: 'Ext.app.ViewController',
    requires: [
        'MAM.model.MeterDetail'
    ],
    alias: 'controller.meterInitialController',

    control: {
        'meterInitial meterList': {
            itemdblclick: 'onRecordSelect'
        },
        'meterInitial': {
            show: 'onShowInitial'
        },
        '#meterEditForm': {
            dirtychange: 'onChangeEditForm'
        },
        '#addRegister': {
            click: 'onAddRegister'
        },
        '#meterEditForm actioncolumn': {
            click: 'onRemoveRegister'
        }
    },
     listen: {
         store: {
            '#meters': {
                load: 'onMetersLoad'
             }
         },
         controller: {
             'actionInitialController': {
                 'meterDelete': 'onDelete',
                 'meterEdit': 'onEdit',
                 'meterSave': 'onSave'
             },
             'relationsInitialController': {
                 'metersRelationAdded': 'setDirtyEntity',
                 'metersRelationRemoved': 'setDirtyEntity'
             },
             'customAttributesInitialController': {
                 'editedCustomAttributes': 'setDirtyEntity'
             },
             '*': {
                 'openMeterEdit': 'openEdit'
             }
         }
     },
    routes : {
        '/meter/itemId/:id' : 'loadDetails'
    },

    onShowInitial: function() {
        this.fireEvent('openEntity', MAM.utils.Constants.METER, this.getViewModel().get('meter'));
    },

    /**
     * Loads the necessary data for given entity
     * @param rowModel
     * @param record
     */
    onRecordSelect: function(rowModel, record){
        var me = this;

        if (this.getViewModel().get('editMode')) {
            Ext.create('Energy.common.ui.DialogBox').showQuestion(_tl.get("mam.dialogbox.meter.editDataLost"),
                [
                    {
                        text: _tl.get("mam.dialogbox.meter.button.yes"),
                        itemId: 'buttonPositive',
                        handler: function () {
                            this.up().up().close();
                            me.loadDetails(record.getId());
                            return true;
                        }
                    },
                    {
                        text: _tl.get("mam.dialogbox.meter.button.no"),
                        itemId: 'buttonNegative',
                        handler: function () {
                            this.up().up().close();
                            return false;
                        }
                    }
                ]

            );
        } else {
            this.loadDetails(record.getId());
        }
    },

    /**
     * Load entity details,
     * @param id int
     * return void
     */
    loadDetails: function(id) {
        this.fireEvent('toggleLoadingMask', true, _tl.get('mam.label.ui.loading.meterDetail'));
        this.getViewModel().set('editMode', false);
        this.getView().createMeterDetailContainer();

        MAM.model.MeterDetail.load( id, {
            success: this.onDataMeterLoadSuccess,
            failure: this.onDataMeterLoadError,
            scope: this
        });
    },

    /**
     * get total count while loading persons
     * @param store
     * @param records
     */
    onMetersLoad: function( store, records ){
        this.getViewModel().set('total', store.getTotalCount());

    },

    /**
     * Loads relations and custom attributes after sucessfull
     * meter load
     * @param recordDetail
     */
    onDataMeterLoadSuccess: function( recordDetail ){
        var me = this;
        var viewModel = this.getViewModel();
        viewModel.set('meter', recordDetail);
        viewModel.getStore('registers').loadRawData(recordDetail.get('registers'));

        this.fireEvent('detailsLoaded', MAM.utils.Constants.METER);
        this.fireEvent('openEntity', MAM.utils.Constants.METER, recordDetail);

        var tabConfig = MAM.utils.Configuration.getTabConfig( MAM.utils.Constants.METER );
        //check which tabs are configured
        Ext.each(tabConfig, function( config ){
            if(config.type === MAM.utils.Constants.RELATIONSDETAIL){
                //load meter relations
                MAM.model.Relation.getProxy().setEntity( MAM.utils.Constants.METER );
                MAM.model.Relation.getProxy().on('exception', me.onDataMeterRelationsLoadError , me);
                MAM.model.Relation.load( recordDetail.getId(), {
                    success: me.onDataMeterRelationsLoadSuccess,
                    failure: me.onDataMeterRelationsLoadError,
                    scope: me
                });
            }
            if(config.type === MAM.utils.Constants.CUSTOMATTRIBUTESDETAIL){

                //set custom attributes empty
                viewModel.set('customAttributes', null );

                //load meter customAttributes
                MAM.model.CustomAttribute.getProxy().setEntity( MAM.utils.Constants.METERS );
                MAM.model.CustomAttribute.getProxy().on('exception', me.onDataMeterCustomAttributesLoadError , me);

                MAM.model.CustomAttribute.load( recordDetail.getId(), {
                    success: me.onDataMeterCustomAttributeLoadSuccess,
                    failure: me.onDataMeterCustomAttributesLoadError,
                    scope: me
                });
            }
        });

    },
    /**
     * Creates a realationschange event when meter relations
     * are sucessull loeaded
     * @param recordDetail
     */
    onDataMeterRelationsLoadSuccess: function( recordDetail ){
        var viewModel = this.getViewModel();
        viewModel.set('relations', recordDetail);
        viewModel.get('meter').set('relations', recordDetail);
        this.fireEvent('relationschange', recordDetail, MAM.utils.Constants.METERS );
        this.fireEvent('toggleLoadingMask', false );
    },
    /**
     * Creates a customattributechange event when meter
     * custom attributes are successful loaded
     * @param recordDetail
     * @param operation
     */
    onDataMeterCustomAttributeLoadSuccess: function( recordDetail ){
        var viewModel = this.getViewModel();
        viewModel.get('meter').set('customAttributes', recordDetail);
        this.fireEvent('customattibuteschange', recordDetail, MAM.utils.Constants.METERS );
    },
    /**
     * Meter cannot be loaded
     * so process no relations and custom attributes error
     * to display empty data
     */
    onDataMeterLoadError: function(){
        this.onDataMeterRelationsLoadError();
        this.onDataMeterCustomAttributesLoadError();
        this.fireEvent('toggleLoadingMask', false );
    },
    /**
     * relations of meter can not be loaded
     * fire event relationschange with an empty relation to
     * display empty data
     * @param request
     */
    onDataMeterRelationsLoadError: function( request ){
        var emptyRecord = Ext.create('MAM.model.Relation');
        this.fireEvent('relationschange', emptyRecord, MAM.utils.Constants.METERS);
        this.fireEvent('toggleLoadingMask', false );
    },
    /**
     * custom attributes of meter can not be loaded
     * fire event customattibuteschange with an empty relation to
     * display empty data
     * @param request
     */
    onDataMeterCustomAttributesLoadError: function( request ){
        var emptyRecord = Ext.create('MAM.model.CustomAttribute');
        this.fireEvent('customattibuteschange', emptyRecord, MAM.utils.Constants.METERS );
        this.fireEvent('toggleLoadingMask', false );
    },

    onRemoveRegister: function(grid, el, rowIndex) {
        grid.getStore().removeAt(rowIndex);
    },

    onAddRegister: function(button) {
        var grid = button.up('gridpanel');
        var rowEditing = grid.getPlugin('rowediting');
        rowEditing.cancelEdit();

        var r = Ext.create('MAM.model.Register', {
            name: '',
            billingRelevant: ''
        });

        grid.getStore().insert(0, r);
        rowEditing.startEdit(0, 0);
    },

    onDelete: function (record) {
        var me = this;
        var win = Ext.create('Energy.common.ui.DialogBox', {});

        win.showQuestion(Ext.String.format(_tl.get('mam.dialogbox.meter.delete.question'), record.get('customId')), [
            {
                text: _tl.get('mam.dialogbox.meter.button.no'),
                handler: function () {
                    win.close();
                }
            },
            {
                text: _tl.get('mam.dialogbox.meter.button.yes'),
                handler: function () {
                    win.close();

                    record.erase({
                        success: function (request, response) {
                            Ext.getStore('meters').reload();
                            me.fireEvent('removeItem', record, MAM.utils.Constants.METER);
                            me.getView().addSelectMessage();
                            me.getViewModel().cleanData();
                            Ext.create('Energy.common.ui.DialogBox').showSuccess(Ext.String.format(_tl.get('mam.dialogbox.meter.delete.response200'), record.get('customId')));
                        },
                        failure: function (request, response) {

                            switch (response.getError().status) {
                                case 404:
                                    Ext.create('Energy.common.ui.DialogBox').showWarning(Ext.String.format(_tl.get('mam.dialogbox.meter.delete.response404'), record.get('customId')));
                                    break;
                                case 409:
                                    Ext.create('Energy.common.ui.DialogBox').showWarning(Ext.String.format(_tl.get('mam.dialogbox.meter.delete.response409'), record.get('customId')));
                                    break;
                            }
                        }
                    });
                }
            }
        ]);
    },

    openEdit: function (recordId) {
        var me = this;

        this.loadMeterDetails(recordId)
            .then(this.loadMeterRelation)
            .then(function(response){
                    return new Ext.Promise(function (resolve, reject) {
                        me.onDataMeterRelationsLoadSuccess(response);
                        resolve(response.getId());
                    });
                },
                function(response){
                    return new Ext.Promise(function (resolve, reject) {
                        me.onDataMeterRelationsLoadSuccess(response);
                        reject(response);
                    })
                })
            .then(this.loadMeterCustomAttribute)
            .then(function(response){
                    return new Ext.Promise(function (resolve, reject) {
                        me.onDataMeterCustomAttributeLoadSuccess(response);
                        resolve(response.getId());
                    })},
                function(response){
                    return new Ext.Promise(function (resolve, reject) {
                        me.onDataMeterCustomAttributesLoadError();
                        reject(response);
                    })
                })
            .then(function(response){
                return new Ext.Promise(function (resolve, reject) {
                    me.onEdit();
                    resolve();
                });
            });

        var tabConfig = MAM.utils.Configuration.getTabConfig(MAM.utils.Constants.METER);
        //check which tabs are configured
        Ext.each(tabConfig, function (config) {
            if (config.type === MAM.utils.Constants.RELATIONSDETAIL) {

            }
            if (config.type === MAM.utils.Constants.CUSTOMATTRIBUTESDETAIL) {

            }
        });
    },

    loadMeterDetails: function (id) {
        var me = this;
        return new Ext.Promise(function (resolve, reject) {
            MAM.model.MeterDetail.load(id,
                {
                    success: function (recordDetail) {
                        var viewModel = me.getViewModel();
                        viewModel.set('meter', recordDetail);
                        me.fireEvent('detailsLoaded', MAM.utils.Constants.METER);
                        me.fireEvent('openEntity', MAM.utils.Constants.METER, recordDetail);

                        resolve(recordDetail);
                    },
                    failure: function (response) {
                        me.onDataMeterLoadError();
                        reject(response);
                    }
                })
        });
    },

    loadMeterRelation: function (record) {
        var id = record.getId();
        MAM.model.Relation.getProxy().setEntity(MAM.utils.Constants.METER);

        return new Ext.Promise(function (resolve, reject) {
            MAM.model.Relation.load(id,
                {
                    success: function (response) {
                        resolve(response);
                    },
                    failure: function (response) {
                        reject(response);
                    }
                });
        })
    },
    loadMeterCustomAttribute: function (id) {
        MAM.model.CustomAttribute.getProxy().setEntity(MAM.utils.Constants.METERS);

        return new Ext.Promise(function (resolve, reject) {
            MAM.model.CustomAttribute.load(id,
                {
                    success: function (response) {
                        resolve(response);
                    }
                    ,
                    failure: function (response) {
                        //   me.onDataMeterPointCustomAttributesLoadError();
                        reject(response);
                    }
                });
        });
    },


    onEdit: function (status) {
        this.getViewModel().set('editMode', true);

        if (status === false) {
            var record = this.getViewModel().get('meter');
            this.loadDetails(record.getId());
            return;
        }

        this.getView().createMeterEditDetailContainer();
        this.getView().down('#meterEditForm').loadRecord(this.getViewModel().get('meter')); // returned because dirtychange doesnt work without it
        this.fireEvent('relationschangeedit', this.getViewModel().get('relations'), MAM.utils.Constants.METERS, this.getViewModel().get('meter'));
        this.fireEvent('customattibuteschange', this.getViewModel().get('meter').get('customAttributes'), MAM.utils.Constants.METERS);
    },

    onChangeEditForm: function () {
        this.setDirtyEntity();
    },

    /**
     * sends the dirty data
     * @param dirty
     * @param data
     */
    setDirtyEntity: function (dirty, data) {
        if (typeof(dirty) !== "undefined") {
            this.fireEvent('setIconSaveStatus', MAM.utils.Constants.METER, dirty);
            if(dirty === MAM.utils.Constants.CUSTOMATTRIBUTESDETAIL){
                var mp = this.getViewModel().get(MAM.utils.Constants.METER);
                if(mp && data.length){
                    mp.get('customAttributes').customAttributes().loadData(data)
                }
            }
        } else {
            var record = this.getViewModel().get(MAM.utils.Constants.METER);
            var relations = record.get('relations');

            var form = this.getView().down('#meterEditForm');

            var addedRelatitonList = this._getAddedRelationList(relations);
            var deletedRelatitonList = this._getDeleteRelationList(relations);

            if ((addedRelatitonList || deletedRelatitonList || form.isDirty()) && form.isValid()) {
                dirty = true;
            } else {
                dirty = false;
            }

            this.fireEvent('setIconSaveStatus', MAM.utils.Constants.METER, dirty);
        }
    },

    _getDeleteRelationList: function (relations) {

        if (!relations) return [];

        var result = {};

        for (key in relations.get('relations')) {

            if (this.getView().query('#relationAccordion grid#' + key).length === 0)  continue;

            var grid = this.getView().query('#relationAccordion grid#' + key)[0];

            relations.get('relations')[key].forEach(function (k, val) {
                if (grid.getStore().find('id', k.id) === -1) {

                    if (!result.hasOwnProperty(key)) result[key] = [];
                    result[key].push(k);
                }
            });
        }

        return result;
    },

    _getAddedRelationList: function (relations) {

        var relationGridList = this.getView().query('#relationAccordion grid');
        var result = {};

        relationGridList.forEach(function (grid, val) {

            var key = grid.getItemId();

            if (relations && relations.get('relations').hasOwnProperty(key)) {

                var data = grid.getStore().getRange();

                data.forEach(function (dataRow, val) {

                    var setNew = true;

                    relations.get('relations')[key].forEach(function (relRow, val) {
                        if (relRow.id === dataRow.getId()) {
                            setNew = false;
                        }
                    });

                    if (setNew) {
                        if (!result.hasOwnProperty(key)) result[key] = [];
                        result[key].push(dataRow);
                    }
                });

            } else if (grid.getStore().getCount()) {
                if (!result.hasOwnProperty(key)) result[key] = [];
                result[key] = grid.getStore().getData();
            }
        });

        return result;
    },

    onSave: function () {
        var me = this;
        var record = this.getViewModel().get(MAM.utils.Constants.METER);

        record.set(this.getView().down('#' + MAM.utils.Constants.METER + 'EditForm').getValues());

        var registers = [];

        this.getView().down('#' + MAM.utils.Constants.METER + 'EditForm gridpanel').getStore().getRange().forEach(function(val) {
            registers.push({name: val.get('name'), 'billingRelevant': val.get('billingRelevant')});
        });

        record.set('registers', registers);

        var customAttributes = record.get('customAttributes');
        var relations = record.get('relations');

        delete record.data.relations;
        delete record.data.customAttributes;

        var promisesArray = [this._saveEntity(record)];

        customAttributes.customAttributes().getData().each(function (row) {

            var attr = Ext.create('MAM.model.CustomAttribute', {});

            attr.set({
                'customAttributes': [{
                    key: row.get('key'),
                    value: row.get('value')
                }],
                'entity': MAM.utils.Constants.METERS,
                'id': record.getId()
            });
            attr.phantom = row.phantom;


            if (row.phantom === true) {
                attr.mamRestCreate = true;
            }

            promisesArray.push(me._saveCustomAttributes(attr));
        });

        customAttributes.customAttributes().removed.forEach(function (row) {
            var attr = Ext.create('MAM.model.CustomAttribute', {});
            attr.set({
                key: row.get('key'),
                'entity': MAM.utils.Constants.METERS,
                'id': record.getId()
            });

            attr.phantom = row.phantom;

            promisesArray.push(me._deleteCustomAttribute(attr));
        });

        var addedRelatitonList = me._getAddedRelationList(relations);

        if (addedRelatitonList) {
            for (key in addedRelatitonList) {

                addedRelatitonList[key].each(function (k) {
                    var relation = Ext.create('MAM.model.Relation', {
                        associatedCompType: 'meter',
                        "relationType": "parent",
                        associatedCompId: record.getId(),
                        "compId": k.getId(),
                        "compType": key.substr(0, key.length - 1) // remove 's
                    });

                    promisesArray.push(me._saveRelation(relation));
                });
            }
        }

        var deletedRelatitonList = me._getDeleteRelationList(relations);

        if (deletedRelatitonList) {
            for (key in deletedRelatitonList) {

                deletedRelatitonList[key].forEach(function (k, v) {
                    var relation = Ext.create('MAM.model.Relation', {});

                    relation.setId(k.id);
                    MAM.model.Relation.getProxy().setEntity(key.substr(0, key.length - 1));
                    relation.phantom = false;

                    promisesArray.push(me._deleteRelation(relation));
                });
            }
        }

        var resultingPromise = Ext.Promise.all(promisesArray);

        resultingPromise
            .then(function () {
                Ext.create('Energy.common.ui.DialogBox').showSuccess(Ext.String.format(_tl.get('mam.dialogbox.meter.update.response200'), record.get('customId')));
                me.fireEvent('setIconEditStatus', MAM.utils.Constants.METER, true);
                me.fireEvent('setIconSaveStatus', MAM.utils.Constants.METER, false);
                me.getViewModel().set('editMode', false);
            })
            ['catch'](function (response) {
            switch (response.getError().status) {
                case 400:
                    Ext.create('Energy.common.ui.DialogBox').showWarning(Ext.String.format(_tl.get('mam.dialogbox.meter.update.response404'), record.get('customId')));
                    break;
                case 404:
                    Ext.create('Energy.common.ui.DialogBox').showWarning(Ext.String.format(_tl.get('mam.dialogbox.meter.update.response404'), record.get('customId')));
                    break;
                case 409:
                    Ext.create('Energy.common.ui.DialogBox').showWarning(Ext.String.format(_tl.get('mam.dialogbox.meter.update.response409'), record.get('customId')));
                    break;
            }
            me.fireEvent('setIconEditStatus', MAM.utils.Constants.METER, true);
            me.fireEvent('setIconSaveStatus', MAM.utils.Constants.METER, false);
            me.getViewModel().set('editMode', false);
        })/* Dont know how to use _always_ here
         ['always'](function () {
         me.fireEvent('setIconEditStatus', MAM.utils.Constants.METERPOINT, true);
         me.fireEvent('setIconSaveStatus', MAM.utils.Constants.METERPOINT, false);
         this.getViewModel().set('editMode', false);
         });*/
    },

    _deleteCustomAttribute: function (record) {
        return new Ext.Promise(function (resolve, reject) {
            record.erase({
                success: function (request, response) {
                    resolve();
                },
                failure: function (request, response) {
                    reject(response);
                }
            });
        });
    },

    _saveRelation: function (record) {
        return new Ext.Promise(function (resolve, reject) {

            record.save({
                success: function (request, response) {
                    resolve();
                },
                failure: function (request, response) {
                    reject(response);
                }
            });
        });
    },

    _saveEntity: function (record) {
        return new Ext.Promise(function (resolve, reject) {

            record.save({
                success: function (request, response) {
                    resolve();
                },
                failure: function (request, response) {
                    reject(response);
                }
            });
        });
    },

    _saveCustomAttributes: function (record) {
        return new Ext.Promise(function (resolve, reject) {

            record.save({
                success: function (request, response) {
                    resolve();
                },
                failure: function (request, response) {
                    reject(response);
                }
            });
        });
    },

    _deleteRelation: function (record) {
        return new Ext.Promise(function (resolve, reject) {
            record.erase({
                success: function (request, response) {
                    resolve();
                },
                failure: function (request, response) {
                    reject(response);
                }
            });
        });
    }
});