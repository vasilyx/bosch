Ext.define('MAM.view.meterPoint.ListModel', {
    extend: 'Ext.app.ViewModel',
    requires: ['MAM.model.Meter', 'MAM.store.Meters'],
    
    alias: 'viewmodel.meterPointListModel',
    
    data: {
        title: 'Meter Liste'
    },
    stores: {
        meterPoints: {
           type: 'meterPoints'
        }
    }
});