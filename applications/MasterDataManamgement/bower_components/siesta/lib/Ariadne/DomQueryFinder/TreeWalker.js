/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Ariadne.DomQueryFinder.TreeWalker', {
    
    methods         : {
        
        getParent   : function (el) {
            return el.parentNode
        },
        
        
        contains : function (parentEl, childEl) {
            return parentEl.contains(childEl)
        }
    }
});
