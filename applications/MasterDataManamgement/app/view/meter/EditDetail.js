Ext.define('MAM.view.meter.EditDetail', {
    extend: 'Ext.Panel',

    alias: 'widget.meterEditDetail',

    bind: {
        data: '{meter}'
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    padding: '0 0 10 0',
    scrollable: true,

    initComponent: function () {

        var defaults = {
            xtype: 'textfield',
            labelWidth: 150
        };

        var editForm = {
            xtype: 'form',
            trackResetOnLoad: true,
            id: 'meterEditForm',
            items: []
        };

        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if (this.responsiveConfig) {
            responsiveConfig = this.responsiveConfig;
        }

        editForm.items = [
            {
                xtype: 'container',
                cls: 'bg-grey',
                margin: 10,
                padding: '5 10 0 10',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                defaults: defaults,
                items: this.getNorthItems()

            },
            {
                xtype: 'container',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                padding: '0 20 0 20',
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        layout: 'form',
                        padding: '0 40 0 0',
                        defaults: defaults,
                        items: this.getWestItems()
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        layout: 'form',
                        defaults: defaults,
                        items: this.getEastItems()
                    }
                ]
            }
        ];

       /* editForm.items = [
            {
                xtype: 'container',
                cls: 'bg-grey',
                margin: 10,
                padding: '5 10 0 10',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                items: this.getNorthItems()

            },
            {
                xtype: 'container',
                padding: '0 20 0 20',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getWestItems()
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getEastItems()
                    }
                ]
            },
            {
                xtype: 'tabpanel',
                padding: '0 20 0 20',
                plain: true,
                items: this.getSouthItems()

            }
        ];*/

        Ext.apply(this, {items: editForm});
        this.callParent(arguments);
    },
    /*
     * Defines the top area items for connection building properties
     */
    getNorthItems: function () {
        var items = [
            {
                fieldLabel: _tl.get('mam.label.meter.customId'),
                name: 'customId',
                flex: 1,
                xtype: 'displayfield',
                bind: {
                    value: '{meter.customId}'
                }
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                flex: 1,
                items: [
                    {
                        fieldLabel: _tl.get('mam.label.meter.createdTimeStamp'),
                        flex: 1,
                        name: 'createdTimeStamp',
                        bind: {
                            value: '{meter.createdTimeStamp}'
                        }
                    },
                    {
                        fieldLabel: _tl.get('mam.label.meter.id'),
                        name: 'meterId',
                        bind: {
                            value: '{meter.id}'
                        }
                    }
                ]
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the left side of panel
     */
    getWestItems: function () {
        var items = [
            {
                xtype: 'combo',
                name: 'medium',
                allowBlank: false,
                forceSelection: true,
                fieldLabel: _tl.get('mam.label.meter.medium'),
                displayField: 'name',
                valueField: 'code',
                width: 'auto',
                store: new Ext.data.JsonStore({
                    autoLoad: true,
                    proxy: {
                        type: 'ajax',
                        url: MAM.utils.Configuration.getUrlEnumProperty(MAM.utils.Constants.METER, 'medium'),
                        reader: {
                            type: 'json',
                            rootProperty: 'medium',
                            transform: {
                                fn: function (data) {
                                    for (k in data.medium) {
                                        data.medium[k].name = _tl.get('mam.medium.' + data.medium[k].name);
                                    }
                                    return data;
                                },
                                scope: this
                            }
                        }
                    }
                }),
                bind: {
                    value: '{meter.medium}'
                }
            },
            {
                xtype: 'combo',
                name: 'measuringPrinciple',
                allowBlank: false,
                forceSelection: true,
                fieldLabel: _tl.get('mam.label.meter.measuringPrinciple'),
                displayField: 'name',
                valueField: 'code',
                width: 'auto',
                store: new Ext.data.JsonStore({
                    autoLoad: true,
                    proxy: {
                        type: 'ajax',
                        url: MAM.utils.Configuration.getUrlEnumProperty(MAM.utils.Constants.METER, 'measuringPrinciple'),
                        reader: {
                            type: 'json',
                            rootProperty: 'measuringPrinciple',
                            transform: {
                                fn: function (data) {
                                    for (k in data.measuringPrinciple) {
                                        data.measuringPrinciple[k].name = _tl.get('mam.measuringPrinciple.' + data.measuringPrinciple[k].name);
                                    }
                                    return data;
                                },
                                scope: this
                            }
                        }
                    }
                }),
                bind: {
                    value: '{meter.measuringPrinciple}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.buildInDate'),
                name: 'buildInDate',
                xtype: 'datefield',
                format: 'd.m.Y',
                allowBlank: false,
                submitFormat: 'Y-m-d',
                bind: {
                    value: '{meter.buildInDate}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.buildOutDate'),
                name: 'buildOutDate',
                xtype: 'datefield',
                format: 'd.m.Y',
                submitFormat: 'Y-m-d',
                bind: {
                    value: '{meter.buildOutDate}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.status'),
                name: 'status',
                bind: {
                    value: '{status}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.deviceType'),
                name: 'deviceType',
                bind: {
                    value: '{meter.deviceType}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.deviceClass'),
                name: 'deviceClass',
                bind: {
                    value: '{deviceClass}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.deviceKind'),
                name: 'deviceKind',
                bind: {
                    value: '{meter.deviceKind}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.manufacturer'),
                name: 'manufacturer',
                bind: {
                    value: '{meter.manufacturer}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.factoryNumber'),
                name: 'factoryNumber',
                bind: {
                    value: '{meter.factoryNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.deliveryNoteNumber'),
                name: 'deliveryNoteNumber',
                bind: {
                    value: '{meter.deliveryNoteNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.energyFlowDirection'),
                name: 'energyFlowDirection',
                bind: {
                    value: '{meter.energyFlowDirection}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.mountingType'),
                name: 'mountingType',
                bind: {
                    value: '{meter.mountingType}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.meterReadingType'),
                name: 'meterReadingType',
                bind: {
                    value: '{meter.meterReadingType}'
                }
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the right side of panel
     */
    getEastItems: function () {
        var items = [
            {
                fieldLabel: _tl.get('mam.label.meter.lotNumber'),
                name: 'lotNumber',
                bind: {
                    value: '{meter.lotNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.examinationNumber'),
                name: 'examinationNumber',
                bind: {
                    value: '{meter.examinationNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.charge'),
                name: 'charge',
                bind: {
                    value: '{meter.charge}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.interface'),
                name: 'interface',
                bind: {
                    value: '{meter.interface}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.comAddress'),
                name: 'comAddress',
                bind: {
                    value: '{meter.comAddress}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.serverId'),
                name: 'serverId',
                bind: {
                    value: '{meter.serverId}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.publicKey'),
                name: 'publicKey',
                bind: {
                    value: '{meter.publicKey}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.keyM'),
                name: 'keyM',
                bind: {
                    value: '{meter.keyM}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.calibrationYear'),
                name: 'calibrationYear',
                bind: {
                    value: '{meter.calibrationYear}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.calibrationEndYear'),
                name: 'calibrationEndYear',
                bind: {
                    value: '{meter.calibrationEndYear}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.constructionYear'),
                name: 'constructionYear',
                bind: {
                    value: '{meter.constructionYear}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.meterSize'),
                name: 'meterSize',
                bind: {
                    value: '{meter.meterSize}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.tariffCount'),
                name: 'tariffCount',
                bind: {
                    value: '{meter.tariffCount}'
                }
            }];
        return items;
    },
    /**
     * Defines the area items for meter properties
     * on the bottom side of panel
     * mostyl parameters collected in a grid
     */
    getSouthItems: function () {
        var items = [
            {
                xtype: 'gridpanel',
                border: true,
                title: _tl.get('mam.label.meter.registers'),
                emptyText: _tl.get('mam.label.ui.noData'),
                bind: {
                    store: '{registers}'
                },
                plugins: [
                    {
                        clicksToMoveEditor: 1,
                        autoCancel: false,
                        ptype: 'rowediting',
                        pluginId: 'rowediting'
                    }
                ],
                viewConfig: {
                    loadMask: false
                },
                tbar: [{
                    text: _tl.get("mam.edit.registers.addRegister"),
                    itemId: 'addRegister'
                }],
                columns: [
                    {
                        text: _tl.get("mam.label.meter.register.name"),
                        dataIndex: 'name',
                        editor: {
                            xtype: 'textfield',
                            allowBlank: false
                        },
                        flex: 1
                    },
                    {
                        text: _tl.get("mam.label.meter.register.billingRelevant"),
                        dataIndex: 'billingRelevant',
                        editor: {
                            xtype: 'combo',
                            store: new Ext.data.JsonStore({
                                autoLoad: true,
                                proxy: {
                                    type: 'ajax',
                                    url: MAM.utils.Configuration.getUrlEnumProperty(MAM.utils.Constants.METER, 'billingRelevant'),
                                    reader: {
                                        type: 'json',
                                        rootProperty: 'billingRelevant',
                                        transform: {
                                            fn: function (data) {
                                                for (k in data.billingRelevant) {
                                                    data.billingRelevant[k].name = _tl.get('mam.label.meter.billingRelevant.' + data.billingRelevant[k].name);
                                                }
                                                return data;
                                            },
                                            scope: this
                                        }
                                    }
                                }
                            }),
                            queryMode: 'local',
                            displayField: 'name',
                            valueField: 'code',
                            allowBlank: false
                        },
                        flex: 1
                    },
                    {
                        icon: 'resources/images/edit/delete16.png',
                        xtype: 'actioncolumn',
                        width: 20,
                        tooltip: _tl.get("mam.edit.registers.removeRegister")
                    }]
            }

        ];
        return items;
    }
});