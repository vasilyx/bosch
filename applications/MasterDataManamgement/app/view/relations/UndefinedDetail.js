Ext.define('MAM.view.relations.UndefinedDetail', {
    extend: 'Ext.Panel',

    alias: 'widget.undefinedDetail',

    bind: {
        data: '{undefinedRelation}'
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    scrollable: true,

    initComponent: function () {

        var defaults = {
            xtype: 'displayfield',
            labelWidth: 150
        };

        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if(this.responsiveConfig){
            responsiveConfig = this.responsiveConfig;
        }

        var items = [
            {
                xtype: 'container',
                cls: 'bg-grey',
                margin: 10,
                padding: '5 10 0 10',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                defaults: defaults,
                items: this.getNorthItems()

            },
            {
                xtype: 'container',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                padding: '0 20 0 20',
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getWestItems()
                    }
                ]
            }
        ];

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    },
    /*
     * Defines the top area items for connection building properties
     */
    getNorthItems: function(){

        var items = [
            {
                fieldLabel: _tl.get('mam.relation.relationId'),
                name: 'customId',
                flex: 1,
                bind: {
                    value: '{undefinedRelation.relationId}'
                }
            }
        ];
        return items;
    },

    getWestItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.relation.associatedCompType'),
                flex: 1,
                name: 'associatedCompType',
                bind: {
                    value: '{undefinedRelation.associatedCompType}'
                }
            },
            {
                fieldLabel: _tl.get('mam.relation.associatedCompId'),
                name: 'associatedCompId',
                bind: {
                    value: '{undefinedRelation.associatedCompId}'
                }
            },
            {
                fieldLabel: _tl.get('mam.relation.startTS'),
                name: 'startTS',
                bind: {
                    value: '{undefinedRelation.startTS}'
                }
            },
            {
                fieldLabel: _tl.get('mam.relation.endTS'),
                name: 'endTS',
                bind: {
                    value: '{undefinedRelation.endTS}'
                }
            }
        ];
        return items;
    }
});
