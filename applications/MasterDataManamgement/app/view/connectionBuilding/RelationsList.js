Ext.define('MAM.view.connectionBuilding.RelationsList', {
    extend: 'MAM.view.relations.EntityList',
    requires: [
        'MAM.view.relations.EntityList'
    ],
    alias: 'widget.connectionBuildingsRelationsList',

    getToolbar: function () {
        return [{
            xtype: 'textfield',
            name: 'customId',
            hideLabel: true,
            width: 200
        }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'name',
                hideLabel: true,
                width: 120
            }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'postalCode',
                hideLabel: true,
                width: 120
            }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'note',
                hideLabel: true,
                flex: 1
            }, this.getButtonClear()
        ];
    },

    getColumns: function () {
        return [
            {text: _tl.get("mam.label.connectionBuilding.customId"), dataIndex: 'customId', width: 250},
            {text: _tl.get("mam.label.connectionBuilding.name"), dataIndex: 'name', width: 170},
            {text: _tl.get("mam.label.connectionBuilding.postalCode"), dataIndex: 'postalCode', width: 170},
            {text: _tl.get("mam.label.connectionBuilding.note"), dataIndex: 'note', flex: 1}
        ];
    },

    initComponent: function () {

        this.callParent(arguments);
    }
});