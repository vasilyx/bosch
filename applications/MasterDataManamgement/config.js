var MAM = {};
MAM.Config = {
    Navigation: [
        {
            type: 'meterPoint',
            label: 'meterPoints',
            view: 'meterPointInitial',
            subDetail: [{
                type: 'relations',
                editAllowed: ['meter', 'gateway', 'person', 'connectionBuilding', 'marketParticipant']
            },
            {
                type: 'customAttributes',
                keys: ['consumptionType']
            }]
        },
        {
            type: 'gateway',
            label: 'gateways',
            view: 'gatewayInitial',
            subDetail: [{
                type: 'relations'
            },{
                type: 'customAttributes',
                keys: []
            }]
        },
        {
            type: 'meter',
            label: 'meters',
            view: 'meterInitial',
            subDetail: [{
                type: 'relations',
                editAllowed: ['gateway', 'person', 'connectionBuilding', 'marketParticipant']
            },{
                type: 'customAttributes',
                keys: ['deviceGroup', 'changeProcess']
            }]
        },{
            type: 'person',
            label: 'persons',
            view: 'personInitial',
            subDetail: [{
                type: 'customAttributes',
                keys: []

            }]
        },{
            type: 'connectionBuilding',
            label: 'connectionBuildings',
            view: 'connectionBuildingInitial',
            subDetail: [{
                type: 'relations'
            },{
                type: 'customAttributes',
                keys: ['businessAreaId', 'politicalAreaId', 'maintenanceAreaId', 'readingAreaId']
            }]
        },{
            type: 'marketParticipant',
            label: 'marketParticipants',
            view: 'marketParticipantInitial',
            subDetail: [{
                type: 'relations'
            },{
                type: 'customAttributes',
                keys: []
            }]
        }
    ],
    List: {
        limit: 50,
        total: 9999 //Default, because MAM is not returnig total
    },
    Backend: {
        Versions: 'v2',
        UrlEnumProperties: {
            meterPoint: {
                medium: 'resources/data/meterPointsEnum.json',
                status: 'resources/data/meterPointsEnum.json',
                measuringType: 'resources/data/meterPointsEnum.json',
                consumptionUnit: 'resources/data/meterPointsEnum.json',
                transformRatio: 'resources/data/meterPointsEnum.json'
            },
            meter: {
                medium: 'resources/data/metersEnum.json',
                measuringPrinciple: 'resources/data/metersEnum.json',
                billingRelevant: 'resources/data/metersEnum.json'
            }
        }
    },
    History: {
        limit: 20
    },
    Relations: {
        // defined entities
        definedEntityList: ['meters', 'gateways', 'persons', 'connectionBuildings',
                    'marketParticipants', 'meterPoints', 'attachments',
                    'plants', 'simCards', 'transformers', 'meterAddonDevices'
        ]
    },
    ActionPanel: {
        rules: {
            meterPoint: ['create', 'edit', 'save', 'delete'],
            gateway: ['edit', 'save'],
            meter: ['create', 'edit', 'save', 'delete'],
            person: ['edit', 'save', 'delete'],
            connectionBuilding: ['edit'],
            marketParticipant: ['edit', 'save', 'delete']
        },
        subMenu: {
            views: [
                {
                    text: 'mam.label.actionPanel.marketParticipantHistory',
                    entity: ['meterPoint'],
                    action: 'actionMarketParticipantHistory'
                },
                {
                    text: 'mam.label.actionPanel.meterReadings',
                    action: 'actionMeterReadings',
                    entity: ['meterPoint']
                }
            ],
            actions: [

            ]
        }
    }
};

var Ext = Ext || {}; // Ext namespace won't be defined yet...

// This function is called by the Microloader after it has performed basic
// device detection. The results are provided in the "tags" object. You can
// use these tags here or even add custom tags. These can be used by platform
// filters in your manifest or by platformConfig expressions in your app.
//
Ext.beforeLoad = function (tags) {
    var s = location.search,  // the query string (ex "?foo=1&bar")
        profile;

    // For testing look for "?classic" or "?modern" in the URL to override
    // device detection default.
    //
    if (s.match(/\bclassic\b/)) {
        profile = 'classic';
    }
    else if (s.match(/\bmodern\b/)) {
        profile = 'modern';
    }
    else {
        profile = tags.desktop ? 'classic' : 'modern';
        //profile = tags.phone ? 'modern' : 'classic';
    }

    Ext.manifest = profile; // this name must match a build profile name

    // This function is called once the manifest is available but before
    // any data is pulled from it.
    //
    //return function (manifest) {
    // peek at / modify the manifest object
    //};

};
