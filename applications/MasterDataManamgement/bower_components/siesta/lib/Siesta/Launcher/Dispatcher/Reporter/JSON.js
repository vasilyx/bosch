/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Launcher.Dispatcher.Reporter.JSON', {
    
    requires    : [ 
        'allPassed', 'getAllResults' 
    ],
    
    
    methods : {
        
        generateJsonReport : function (options) {
            var report      = {
                testSuiteName       : this.testSuiteName || '',
                
                startDate           : this.startDate,
                endDate             : this.endDate || (new Date() - 0),
                
                passed              : this.allPassed(),
                
                testCases           : this.getAllResults()
            }
            
            if (this.options.build) report.build = this.options.build
            
            return JSON.stringify(report, function (key, value) {
                if (key == 'automationElementId' || key == 'id' || key == 'parentId') return undefined
                
                return value
            })
        },
        
        
        generateJsonsReport : function (options) {
            var report      = {
                testSuiteName       : this.testSuiteName || '',
                
                startDate           : this.startDate,
                endDate             : this.endDate || (new Date() - 0),
                
                passed              : this.allPassed(),
                
                testCases           : this.getAllResultsStructured().items
            }
            
            if (this.options.build) report.build = this.options.build
            
            return JSON.stringify(report, function (key, value) {
                if (key == 'automationElementId' || key == 'groups' || key == 'id' || key == 'parentId') return undefined
                
                return value
            })
        },
        
        
        generateHtmlReport : function (options) {
            return this.generateJsonsReport(options)
        }
    }
})
