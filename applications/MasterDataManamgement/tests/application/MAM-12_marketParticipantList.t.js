StartTest( function(t) {

    var list = Ext.create('TestApplication.application.AbstractList', {t: t, name: 'marketParticipant'});

    MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipants/l/details_2.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipants/l/details_5.json');

    //dummy only to mock data and don't get error from browser
    MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipants/l/customAttributes_dummy.json', 2);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipants/l/relations_dummy.json', 2);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipants/l/customAttributes_dummy.json', 5);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipants/l/relations_dummy.json', 5);
    //end dummy

    list.testListViewArea();
    list.testFilledDataInList(19);
    list.testPageCorrect();
    list.testDetailArea();
    list.testFilterById('TEST_1477665249378');
    list.testFilterEmpty();
    list.testSelectRow(1); // select second row
    list.testSelectRow(4, 8);
});
