Ext.define('MAM.view.simCard.Detail', {
    extend: 'Ext.Panel',

    alias: 'widget.simCardDetail',


    bind: {
        data: '{simCard}'
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    scrollable: true,
    
    initComponent: function () {

        var defaults = {
            xtype: 'displayfield',
            labelWidth: 150
        };

        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if(this.responsiveConfig){
            responsiveConfig = this.responsiveConfig;
        }

        var items = [
            {
                xtype: 'container',
                cls: 'bg-grey',
                margin: 10,
                padding: '5 10 0 10',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                defaults: defaults,
                items: this.getNorthItems()

            },
            {
                xtype: 'container',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                padding: '0 20 0 20',
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getWestItems()
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getEastItems()
                    }
                ]
            }
        ];

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    },
    /*
     * Defines the top area items for simCard properties
     */
    getNorthItems: function(){

        var items = [
            {
                fieldLabel: _tl.get('mam.label.simCard.customId'),
                name: 'customId',
                flex: 1,
                bind: {
                    value: '{simCard.customId}'
                }
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                flex: 1,
                items: [
                    {
                        fieldLabel: _tl.get('mam.label.simCard.activationDate'),
                        flex: 1,
                        name: 'activationDate',
                        bind: {
                            value: '{simCard.activationDate}'
                        }
                    },
                    {
                        fieldLabel: _tl.get('mam.label.simCard.id'),
                        name: 'simCardId',
                        bind: {
                            value: '{simCard.id}'
                        }
                    }
                ]
            }
        ];
        return items;
    },
    /**
     * Defines the area items for simCard properties
     * on the left side of panel
     */
    getWestItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.simCard.iccid'),
                name: 'iccid',
                bind: {
                    value: '{simCard.iccid}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.simCard.format'),
                name: 'format',
                bind: {
                    value: '{simCard.format}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.simCard.status'),
                name: 'status',
                bind: {
                    value: '{simCard.status}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.simCard.provider'),
                name: 'provider',
                bind: {
                    value: '{simCard.provider}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.simCard.serviceProfile'),
                name: 'serviceProfile',
                bind: {
                    value: '{simCard.serviceProfile}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.simCard.tariffName'),
                name: 'tariffName',
                bind: {
                    value: '{simCard.tariffName}'
                }
            }
        ];
        return items;
    },
    /**
     * Defines the area items for simCard properties
     * on the right side of panel
     */
    getEastItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.simCard.activationDate'),
                name: 'activationDate',
                bind: {
                    value: '{simCard.activationDate}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.simCard.phoneNumber'),
                name: 'phoneNumber',
                bind: {
                    value: '{simCard.phoneNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.simCard.apn'),
                name: 'apn',
                bind: {
                    value: '{simCard.apn}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.simCard.compType'),
                name: 'compType',
                bind: {
                    value: '{simCard.compType}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.simCard.ipAddress'),
                name: 'ipAddress',
                bind: {
                    value: '{simCard.ipAddress}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.simCard.subnetMask'),
                name: 'subnetMask',
                bind: {
                    value: '{simCard.subnetMask}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.simCard.defaultGateway'),
                name: 'defaultGateway',
                bind: {
                    value: '{simCard.defaultGateway}'
                }
            }
        ];
        return items;
    }
});