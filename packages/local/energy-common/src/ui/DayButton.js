Ext.define("Energy.common.ui.DayButton", {
    extend: 'Ext.button.Button',
    alias: 'widget.commonUiDayButton',
    config: {
        scale: 'large',
        cls: 'appDateButton',
        subtext: 'dateOfButton',
        type: 'typeOfButton',
        textAlign: 'left',
        isSelected: false,
        isFirst: false, /* have to be set at vertical buttons, needed for border creation*/
        isLast: false, /* have to be set at vertical buttons, needed for border creation*/
        buttonState: 'ok',
        dateFormat: 'd.m.Y H:i:s',
        heigth: 60,
        inclusive: false,
        buttonAlign: 'hor', /* buttonAlig hor is for horizontal or ver for vertical */
        buttonOrientation: 'top', /* orientation to align active button image */
        imagePath: '/CertificateManagement/classic/resources/energy-common/dayButton/'
    },
    initComponent: function (config) {
        this.initConfig(config);
        this.callParent(config);
    },
    listeners: {
        'beforerender': function (button, eOpts) {
            var path;
            if (button.up().imagePath != undefined) {
                path = button.up().imagePath;
            } else {
                path = button.imagePath;
            }
            var buttonImage = path + 'dayButtonSelectedBg_bottom.png';
            if (button.buttonAlign == 'hor' && button.buttonOrientation == 'top') {
                buttonImage = path + 'dayButtonSelectedBg_top.png';
            } else if (button.buttonAlign == 'hor' && button.buttonOrientation == 'bottom') {
                buttonImage = path + 'dayButtonSelectedBg_bottom.png';
            } else if (button.buttonAlign == 'ver' && button.buttonOrientation == 'left') {
                buttonImage = path + 'dayButtonSelectedBg_left.png';
            } else if (button.buttonAlign == 'ver' && button.buttonOrientation == 'right') {
                buttonImage = path + 'dayButtonSelectedBg_right.png';
            }
            if (button.buttonAlign == 'ver') {
                if (!button.isFirst && !button.isLast) {
                    button.addCls('noBorder');
                }
            }
            button.addCls('active');
            var template = '<span id="{id}-btnWrap" class="{baseCls}-wrap<tpl if="splitCls">{splitCls}</tpl>{childElCls} preselect" unselectable="on">' +
                '<span id="{id}-btnEl" class="{baseCls}-button app-button-body"><span id="{id}-btnInnerEl" class="{baseCls}-inner {innerCls}{childElCls}" unselectable="on">{text}</span>' +
                '<span id="{id}-btnInnerElSubtext" class="{baseCls}-inner {innerCls}{childElCls} appSubtext" unselectable="on">' +
                button.getSubtext(button) + '</span>' +
                '<span role="img" id="{id}-btnIconEl" class="{baseCls}-icon-el {iconCls}{childElCls} {glyphCls}" unselectable="on" style="<tpl if="iconUrl">' +
                'background-image:url({iconUrl});</tpl><tpl if="glyph && glyphFontFamily">font-family:{glyphFontFamily};</tpl>"><tpl if="glyph">&#{glyph};</tpl>' +
                '<tpl if="iconCls || iconUrl">&#160;</tpl></span></span></span><tpl if="closable"><span id="{id}-closeEl" class="{baseCls}-close-btn" title="{closeText}" tabIndex="0"></span></tpl>' +
                '<img src="' + buttonImage + '" class="appSelButtonImg ' + button.buttonAlign + ' ' + button.buttonOrientation + '"/>';
            button.renderTpl = new Ext.XTemplate(template);
        }
    },
    getSubtext: function (button) {
        var startDate = new Date();
        var prevDate = new Date();
        if (button.inclusive) {
            prevDate.setDate(startDate.getDate() - 6);
        } else {
            prevDate.setDate(startDate.getDate() - 7);
        }
        //prev 3 month
        var prevMonthDate = new Date();
        if (button.inclusive) {
            prevMonthDate.setMonth(startDate.getMonth() - 2);
        } else {
            prevMonthDate.setMonth(startDate.getMonth() - 3);
        }
        //button date format shown at button
        if (button.subtext != 'dateOfButton') {
            return button.subtext;
        } else {
            if (button.type == 'currentDay') {
                return Ext.Date.format(startDate, 'd.m.Y');
            } else if (button.type == 'lastSevenDays') {
                return Ext.Date.format(prevDate, 'd.m.Y') + ' - ' + Ext.Date.format(startDate, 'd.m.Y');
            } else if (button.type == 'currentMonth') {
                return Ext.Date.format(startDate, 'F Y');
            } else if (button.type == 'lastThreeMonths') {
                return Ext.Date.format(prevMonthDate, 'F Y') + ' - ' + Ext.Date.format(startDate, 'F Y');
            } else if (button.type == 'currentYear') {
                return Ext.Date.format(startDate, 'Y');
            }
        }
    },
    markButtonActive: function (clickedButton, state) {
        //remove all buttons class selected
        var daysButtons = clickedButton.up();
        var buttonsLength = daysButtons.items.length;
        for (i = 0; i < buttonsLength; i++) {
            var cur = daysButtons.items.getAt(i);
            cur.removeCls('appDateButtonSelected');
            cur.removeCls('active');
            cur.addCls('appDateButton');
            cur.setIsSelected(false);
        }
        //set current button as selected
        clickedButton.removeCls('appDateButton');
        clickedButton.addCls('appDateButtonSelected');
        if (state == '') {
            clickedButton.addCls('active');
        } else {
            clickedButton.addCls(state);
        }
        clickedButton.setIsSelected(true);
    }
}); 