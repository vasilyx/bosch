/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Launcher.CommandLineTool.WebDriverGenericTool', {
    
    has : {
    },
    
    
    methods : {
        
        getChromeDriverServerPath : function (includeFileName) {
            var binDir              = this.binDir
            
            if (this.isWindows)
                return binDir + "\\binary\\chromedriver\\windows" + (includeFileName ? "\\chromedriver.exe" : "")
            else {
                var binPath
            
                if (this.isMacOS)
                    binPath         = "/binary/chromedriver/macos"
                else {
                    // linux branch
                    if (this.is64Bit)
                        binPath     = "/binary/chromedriver/linux64"
                    else
                        binPath     = "/binary/chromedriver/linux32"
                }
                
                return binDir + binPath + (includeFileName ? "/chromedriver" : "")
            }
        },
        
        
        getIEDriverServerPath : function (includeFileName) {
            if (!this.isWindows) return
            
            var binDir          = this.binDir
            var binPath
            
            if (this.is64Bit)
                binPath         = "\\binary\\iedriver_win64"
            else
                binPath         = "\\binary\\iedriver_win32"
                
            return binDir + binPath + (includeFileName ? "\\IEDriverServer.exe" : "")
        }
    }
    // eof methods
})
