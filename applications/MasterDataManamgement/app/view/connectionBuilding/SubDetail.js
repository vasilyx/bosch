Ext.define('MAM.view.connectionBuilding.SubDetail', {
    extend: 'Ext.tab.Panel',
    requires: [
        'MAM.view.relations.Initial'
    ],
    alias: 'widget.connectionBuildingSubDetail',
    //plain: true,
    initComponent: function () {
        var items = [];
        var tabConfig = MAM.utils.Configuration.getTabConfig( MAM.utils.Constants.CONNECTIONBUILDING );

        Ext.each(tabConfig, function( tab ){
           items.push({
               xtype: tab.type + 'Initial',
               title: _tl.get("mam.label." + MAM.utils.Constants.CONNECTIONBUILDING + "." + tab.type),
               itemId: MAM.utils.Constants.CONNECTIONBUILDINGS + '-tab-' + tab.type
           }) 
        });

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    }
});