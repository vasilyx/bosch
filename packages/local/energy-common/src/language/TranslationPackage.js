Ext.define("Energy.common.language.TranslationPackage", {
    requires: [
       'Energy.common.language.Locale'
    ],
    inheritableStatics: {
        en: {},
        de: {},

        /**
         * Get translation
         * @param msg string
         * @returns string
         */
        get: function(msg) {
            var local = Energy.common.language.Locale.getLocale();
            if (this[local].hasOwnProperty(msg)) {
                return this[local][msg];
            } else {
                console.log('Translation missed in ' + this.$className + '[' + local + '].' + msg);
                return msg;
            }
        },
        /**
         * check if msg has translation
         * @param msg string
         * @returns {boolean}
         */
        isset: function(msg) {
            if (this[Energy.common.language.Locale.getLocale()].hasOwnProperty(msg)) {
                return true;
            } else {
                return false;
            }
        }
    }
});