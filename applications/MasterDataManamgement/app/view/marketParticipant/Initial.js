Ext.define('MAM.view.marketParticipant.Initial', {
    extend: 'Ext.container.Container',

    requires: [
        'MAM.view.marketParticipant.List',
        'MAM.view.marketParticipant.InitialModel',
        'MAM.view.marketParticipant.InitialController',
        'MAM.view.marketParticipant.Detail',
        'MAM.view.marketParticipant.SubDetail',
        'MAM.view.relations.Initial',
        'MAM.view.customAttributes.Initial',
        'Energy.common.ui.Header'
    ],

    alias: 'widget.marketParticipantInitial',

    viewModel: 'marketParticipantInitialModel',
    controller: 'marketParticipantInitialController',

    layout: {
       type: 'hbox',
        align: 'stretch',
        resizable: true
    },
    padding: '0 0 0 10',

    initComponent: function () {

        this.callParent(arguments);

        this.createMarketParticipantsList();
        this.addSelectMessage();
    },
    addSelectMessage: function(){
        this.lookupReference('detailContainer').removeAll();
        this.lookupReference('detailContainer').add({
            xtype: 'commonUiDisplayMessage',
            message: _tl.get('mam.label.ui.selectEntity')

        });
    },

    createMarketParticipantsList: function(){
        this.add({
            xtype: 'container',
                width: 300,
            layout: {
            type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype:'commonUiHeader',
                    header: _tl.get('mam.label.marketParticipants'),
                    bind: {
                        header:  '{totalText}'
                    }

                },
                {
                    xtype: 'marketParticipantList',

                    flex: 1
                }
            ]
        },{
            xtype: 'container',
            margin: '0 0 0 10',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            flex: 1,
            items: [{
                xtype:'commonUiHeader',
                header: _tl.get('mam.label.marketParticipant'),
                colorClass: 'green',
                showArrow: true,
                bind: {
                    header:  _tl.get('mam.label.marketParticipant')  + ' {marketParticipant.customId}',
                    subheader: _tl.get('mam.label.mode.view') 
                }

            },
                {
                    xtype: 'container',
                    cls: 'border-lrb',
                    reference: 'detailContainer',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    flex: 1
                }]
        },
            {
                xtype: 'actionInitial',
                viewModel: {
                    data: {
                        entity: MAM.utils.Constants.MARKETPARTICIPANT
                    }
                }
            });
    },

    createMarketParticipantDetailContainer: function(){
        var detail = this.lookupReference('detailContainer');
        detail.removeAll();

        this.lookupReference('detailContainer').add(
            {
                xtype: 'marketParticipantDetail',
                flex: 1
            },
            {
                xtype: 'marketParticipantSubDetail',
                flex: 1
            }
        );
        
    }

});