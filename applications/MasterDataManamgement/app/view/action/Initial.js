Ext.define('MAM.view.action.Initial', {
    extend: 'Ext.Panel',
    controller: 'actionInitialController',
    requires: [
        'MAM.utils.Configuration',
        'MAM.view.action.InitialController',
        'MAM.view.action.InitialModel'
    ],
    alias: 'widget.actionInitial',
    cls: 'actionInitial',
    width: 80,
    border: true,
    viewModel: 'actionInitialModel',
    iconViews:{
        text: 'views',
        cls: 'information',
        menu: [],
        onlyEditMode: true,
        disabled: true
    },
    iconActions:{
        text: 'actions',
        cls: 'action',
        menu: [],
        onlyEditMode: true,
        disabled: true
    },
    iconCreate:{
        text: 'create',
        margin: '30 0 0 0',
        cls: 'create'
    },
    iconEdit:{
        text: 'edit',
        cls: 'edit',
        onlyEditMode: true,
        disabled: true
    },
    iconDelete:{
        text: 'delete',
        cls: 'delete',
        onlyEditMode: true,
        disabled: true

    },
    iconSave:{
        text: 'save',
        cls: 'save',
        //   onlyEditMode: true,  // uncomment after implementation
        disabled: true
    },
    iconHelp:{
        text: 'help',
        cls: 'help',
        disabled: true // remove after implementation
    },

    initComponent: function () {

        Ext.apply(this, {dockedItems: [{
                xtype: 'toolbar',
                defaults: {
                    xtype: 'button',
                    margin: '0 0 0 0'
                },
                dock: 'left',
                items: this.buildMenu()}]
            }
        );
        this.callParent(arguments);
    },



    getDefaultListIcons:function() {
        return [
            this.iconViews,
            this.iconActions,
            this.iconCreate,
            this.iconEdit,
            this.iconSave,
            this.iconDelete,
            this.iconHelp
        ]
    },

    checkRules: function(rule){
        var rr =  ['create', 'delete', 'edit', 'save' ];
        var me = this;
        if (Ext.Array.contains(rr, rule) &&
            !Ext.Array.contains(MAM.utils.Configuration.getActionPanelRules(me.getViewModel().get('entity')), rule)
        ) {
            return false;
        }

        return true;
    },

    buildMenu: function() {
        var items = [];
        var me = this;

        items.push({xtype:'historyInitial'});

        Ext.each(me.getDefaultListIcons(), function( item ){

            if (!me.checkRules(item.text)) return; // continue

            var submenu = {};

            if (item.hasOwnProperty('menu')) {

                submenu = MAM.utils.Configuration.getActionPanelSubMenu(item.text, me.getViewModel().get('entity'));

                if (submenu.items.length) {
                    submenu = {menu: submenu};
                } else {
                    item.disabled = true;
                }
            }

            var merged = Ext.merge({}, item, {
                text: _tl.get('mam.label.actionPanel.' + item.text),
                cls: 'icon-' + item.cls,
                arrowAlign:'bottom',
                overCls: ['icon-over-' + item.cls, 'btn-over'],
                style:'border-radius:0',
                height: 60,
                width: me.width
            }, submenu);

            items.push(merged);
        });

        return items;
    }
});