/*
 * msgTitle: The title of user acknowledge
 * msgContent: The content of user acknowledge - more detailed message
 * msgIcon: The icon, whos showed beside the text
 * 	        possible types are: success, warning, error
 * imagePath: path to the images displayed in window
 * 
 */


Ext.define("Energy.common.ui.UserAcknowledge", {
    extend: 'Ext.window.Window',
    alias: 'widget.commonUiUserAcknowledge',
    shadow: false,
    /*height: 120,*/
    minHeight: 120,
    width: 300,
    header: false,
    preventHeader: true,
    closeable: false,
    msgTitle: null,
    msgContent: null,
    msgIcon: null,
    imagePath: 'resources/energy-common/message/',
    style: {
        opacity: 0
    },
    layout: 'auto',
    initComponent: function(config){

        var items = [];

        items.push({

            xtype: 'container',
            style: {
                padding: '15px'
            },
            html: this.getGeneratedHtml()

        });
        this.items = items;

        this.initConfig(config);
        this.callParent(config);
    },

    getGeneratedHtml: function(){



        var htmlContent;

        if(this.msgTitle){
            htmlContent = '<div style="padding-top:5px;font-weight:bold;font-size:14px;">' + this.msgTitle + '</div>';
        }
        if(this.msgContent){

            if(this.msgIcon){
                htmlContent = htmlContent + '<div style="position:relative;top:10px;"><img style="float:left;" src="' + this.imagePath + this.msgIcon + '.png" />';
                htmlContent = htmlContent + '<span style="position:relative;top:2px;left:10px;">' + this.msgContent + '</span></div>';
            }
            else{
                htmlContent = htmlContent + '<span >' + this.msgContent + '</span>';
            }

        }

        return htmlContent;

    },

    listeners: {
        show: function (w) {

            var body = Ext.getBody();
            var windowWidth = w.width;
            var xpos = (body.getViewSize().width/2) - (windowWidth/2);
            w.setPosition(xpos, 0);

            Ext.create('Ext.fx.Anim', {
                target: w.getEl(),
                duration: 5000,
                keyframes : {
                    '0%': {
                        x: xpos,
                        y: 0,
                        opacity: 0
                    },
                    '60%': {
                        x: xpos,
                        y: 120,
                        opacity: 1
                    },
                    '80%': {
                    },
                    '100%': {
                        x: xpos,
                        y: 0,
                        opacity: 0
                    }
                }
            });
        }
    }
});