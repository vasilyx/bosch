Ext.define('Energy.common.im.model.IMAuthentication', {
    extend: 'Ext.data.Model',
    requires: [
        'Energy.common.im.model.Role',
        'Energy.common.im.model.Permission'
    ],
    fields: [
        { name: 'userid', type: 'string', mapping: 'user.id'},
        { name: 'userName', type: 'string', mapping: 'user.name'},
        { name: 'tenantId', type: 'string', mapping: 'currentTenant.id'},
        { name: 'tenantName', type: 'string', mapping: 'currentTenant.name'}
    ],
    hasMany: [
        { name: 'userRoles', associationKey: 'tenantRelatedData.roles', model: 'Energy.common.im.model.Role'},
        { name: 'userPermissions', associationKey: 'tenantRelatedData.permissions', model: 'Energy.common.im.model.Permission'}
    ],
    proxy: {
        type: 'ajax',
        url: '/im/authentication/current',
        autoLoad: false,
        headers: {
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            transform: function(data){
                var newData = data;

                if(Ext.isArray(data.tenantRelatedData)){
                    var currentTenant = data.currentTenant.id;
                    Ext.each(newData.tenantRelatedData, function( tenantData ){
                        if(tenantData.tenantId === currentTenant){
                            //reorder tenantRelated data to match the model requirements
                            newData.tenantRelatedData = {};
                            newData.tenantRelatedData.roles = tenantData.roles || [];
                            newData.tenantRelatedData.permissions = tenantData.permissions || [];
                            return;
                        }
                    });
                }


                return newData;
            }
        }
    }
});