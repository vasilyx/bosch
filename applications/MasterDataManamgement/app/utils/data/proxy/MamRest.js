Ext.define('MAM.utils.data.proxy.MamRest', {
    extend: 'Ext.data.proxy.Rest',
    alias: 'proxy.mamrest',
    config: {
        entity: undefined
    },
    buildUrl: function (request) {
        var me = this,
            operation = request.getOperation(),
            records = operation.getRecords(),
            record = records ? records[0] : null,
            format = me.getFormat(),
            url = me.getUrl(request),
            id, params;


        if (record && (!record.phantom || record.mamRestCreate === true)) {
            id = record.getId();
        } else {
            id = operation.getId();
        }

        if (me.isValidId(id)) {
            url = url.replace('{id}', id);
            if (this.getEntity()) {
                url = url.replace('{entity}', this.getEntity());
            }
        }

        // need refactoring this class.
        if (url.indexOf('{key}') !== -1) {
            url = url.replace('{key}', record.get('key'));
        }

        request.setUrl(url);
        return Ext.data.proxy.Rest.superclass.buildUrl.apply(this, arguments);
        //return me.callParent([request]);
    }
});