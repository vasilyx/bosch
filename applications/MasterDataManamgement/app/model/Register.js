Ext.define('MAM.model.Register', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'name', type: 'string' },
        { name: 'billingRelevant'}
    ]
});