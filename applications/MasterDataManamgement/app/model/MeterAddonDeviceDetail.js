Ext.define('MAM.model.MeterAddonDeviceDetail', {
    extend: 'MAM.model.MeterAddonDevice',
    proxy: {
        type: 'rest',
        api: {
            create: undefined,
            read: '/MAM/' + MAM.Config.Backend.Versions + '/meterAddonDevices',
            update: undefined,
            destroy: undefined
        },
        headers: {
            'Accept': 'application/json'
        },
        extraParams: {
            expand: 'properties'
        }
    }
});