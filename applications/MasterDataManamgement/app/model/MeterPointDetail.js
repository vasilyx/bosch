Ext.define('MAM.model.MeterPointDetail', {
    extend: 'MAM.model.MeterPoint',
    proxy: {
        type: 'rest',
        api: {
            create: '/MAM/' + MAM.Config.Backend.Versions + '/meterPoints/',
            read: '/MAM/' + MAM.Config.Backend.Versions + '/meterPoints',
            update: '/MAM/' + MAM.Config.Backend.Versions + '/meterPoints/',
            destroy: '/MAM/' + MAM.Config.Backend.Versions + '/meterPoints/'
        },
        actionMethods: {
            create: 'POST',
            read: 'GET',
            update: 'PUT',
            destroy: 'DELETE'
        },
        reader: {
            type: 'json'
        },
        writer: {
            writeAllFields: true,
            type: 'json',
            transform: {
                fn: function(data, request) {

                    //TODO: quickfix: delete validFrom, causes error in backend
                    delete data.validFrom

                    // for backend
                    if ( request.getRecords()) {
                        data.customId = request.getRecords()[0].get('customId');
                    }
                    return data;
                },
                scope: this
            }
        },
        headers: {
            'Accept': 'application/json'
        },
        extraParams: {
            expand: 'properties'
        }
    }
});