Ext.define('MAM.utils.model.Converts', {
    singleton: true,
    convertDate: function ( value ) {
        return value ? Ext.Date.format(new Date( value ), 'd.m.Y H:i:s') : '';
    },
    convertDateWithoutHour: function ( value ) {
        return value ? Ext.Date.format(new Date( value ), 'd.m.Y') : '';
    },
    convertEmptyInt: function( value ){
        return value ? value : '';
    },
    convertCustomAttribute: function ( value ){
        var tl = _tl.get('mam.label.customAttribute.' + value);
        if(tl){
            return tl;
        }
        else{
            return value;
        }
    },
    convertDeviceClass: function( value ){
        var tl = _tl.get('mam.deviceClass.' + value.toLowerCase());
        return tl ? tl : value ;
    },
    convertMeterStatus: function( value ){
        return value ? _tl.get('mam.meter.status.' + value.toLowerCase()) : value ;
    },
    convertGatewayStatus: function( value ){
        return value ? _tl.get('mam.gateway.status.' + value.toLowerCase()) : value ;
    },
    convertMedium: function( value ){
       return value ? _tl.get('mam.medium.' + value.toLowerCase()) : value ;
    },
    convertMeasuringType: function( value ){
        return value ? _tl.get('mam.measuringType.' + value.toLowerCase()) : value ;
    },
    convertMeasuringPrinciple: function( value ){
        return value ? _tl.get('mam.measuringPrinciple.' + value.toLowerCase()) : value ;
    },
    convertConfigStatus: function( value ){
        return value ? _tl.get('mam.configStatus.' + value.toLowerCase()) : value ;
    }
});