Ext.define('MAM.store.Persons', {
    extend: 'Ext.data.Store',
    alias: 'store.persons',
    storeId: 'persons',
    model: 'MAM.model.Person',
    autoLoad: false,
    pageSize: MAM.Config.List.limit,
    proxy: {
        type: 'ajax',
        url: '/MAM/' + MAM.Config.Backend.Versions + '/persons',
        startParam: 'skip',
        limitParam: 'top',
        extraParams: {
            expand: 'properties',
            totalNumber: true
        },
        headers: {
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty: 'persons',
            totalProperty: 'totalNumber'
        }
    }
});