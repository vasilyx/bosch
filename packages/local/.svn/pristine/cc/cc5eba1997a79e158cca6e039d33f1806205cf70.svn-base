Ext.define('Energy.mom.store.MarketParticipants', {
    extend: 'Ext.data.Store',
    alias: 'store.momMarketParticipants',
    storeId: 'momMarketParticipants',
    model: 'Energy.mom.model.MarketParticipant',
    autoLoad: false,
    pageSize: 25,
    proxy: {
        type: 'ajax',
        appendId: false,
         url: '/MOM/v1/meterPoints/{0}/marketParticipants/history',
        startParam: 'skip',
        limitParam: 'top',
        extraParams: {
            expand: 'properties',
            totalNumber: true
        },
        headers: {
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty: 'timeSlices',
            totalProperty: 'totalNumber',
            transform: {
                fn: function(data) {
                    var newData = [];

                    var lang = Energy.common.language.Translations;

                    var convert = function( value ) {
                        if (value == '9999-12-31T23:59:59.999Z' || !value) {
                            return lang.get('energy.mom.label.unlimited');
                        }

                        return value ? Ext.Date.format(new Date( value ), 'd.m.Y H:i:s') : '';
                    };

                    var getGroupTitle = function(from, to) {

                        if (convert(from) === lang.get('energy.mom.label.unlimited') || convert(to) === lang.get('energy.mom.label.unlimited') ) {
                            return lang.get('energy.mom.label.unlimited');
                        } else {
                            return convert(from) + ' - ' + convert(to);
                        }
                    }

                    Ext.each(data.body.timeSlices, function(item){
                        Ext.each(item.marketParticipants, function(marketParticipant){

                            var state = 'noactive';
                            var currentTime = new Date();

                            if ((currentTime > new Date(item.from)) && (currentTime < new Date(item.to))) {
                                state = 'active';
                            };

                            newData.push({
                                name: marketParticipant.name,
                                role: marketParticipant.role,
                                code: marketParticipant.code,
                                from: convert(item.from),
                                to: convert(item.to),
                                state: state,
                                groupTitle: getGroupTitle(item.from, item.to)
                            });
                        });

                    });

                    return {'timeSlices': newData};
                },
                scope: this
            }
        }
    }
});