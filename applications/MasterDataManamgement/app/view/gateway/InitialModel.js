Ext.define('MAM.view.gateway.InitialModel', {
    extend: 'Ext.app.ViewModel',
    requires: ['MAM.model.Attribute'],

    alias: 'viewmodel.gatewayInitialModel',

    data: {
        gateway: null,
        total: 0
    },

    stores: {
        certificates: {
            model: 'MAM.model.Certificate',
            autoLoad: false,
            proxy: {
                type: 'memory'
            }
        }
    },
    formulas: {
        configStatus: function(get){
            return  MAM.utils.model.Converts.convertConfigStatus( get('gateway.configStatus') );
        },
        status: function(get){
            return  MAM.utils.model.Converts.convertMeterStatus( get('gateway.status'));
        },
        totalText: {
            get: function(get) {
                var total = get('total');
                if(total === 1){
                    return Ext.util.Format.number(total, '0,000') + ' ' + _tl.get('mam.label.gateway');
                }
                else{
                    return Ext.util.Format.number(total, '0,000') + ' ' + _tl.get('mam.label.gateways');
                }
            }
        }
    }
});