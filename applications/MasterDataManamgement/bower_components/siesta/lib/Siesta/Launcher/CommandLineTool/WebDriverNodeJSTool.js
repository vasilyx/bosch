/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Launcher.CommandLineTool.WebDriverNodeJSTool', {
    
    does    : [
        Siesta.Launcher.CommandLineTool.WebDriverGenericTool
    ],
    
    methods : {
        
        setChromeDriverServerPath : function () {
            process.env.PATH    = this.getChromeDriverServerPath() + (this.isWindows ? ';' : ':') + process.env.PATH
        },
        
        
        setIEDriverServerPath : function () {
            if (!this.isWindows) return
            
            process.env.PATH    = this.getIEDriverServerPath() + ";" + process.env.PATH
        }
    }
    // eof methods
})
