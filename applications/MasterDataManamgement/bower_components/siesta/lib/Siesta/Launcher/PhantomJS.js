/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Siesta.Launcher.PhantomJS', {
    
    isa         : Siesta.Launcher.BaseLauncher,
    
    does        : Siesta.Launcher.FileSystem.PhantomJS,
    
    has : {
        executableName          : 'phantomjs',
        
        knownOptions            : {
            init : [
            ]
        }
    },
    
    
    methods : {
        
        processArguments : function () {
            var res     = this.SUPERARG(arguments)
            
            if (this.isWindows) res.options[ 'no-color' ] = true
            
            return res
        },
        
        
        getTerminalWidth : function () {
            if (!this.isWindows) return this.terminalWidth
            
            // TODO
            return 120
            
//            var process     = require("child_process")
//            
//            console.log(process.execFileSync)
//            console.log(process.execSync)
//            console.log(process.spawnSync)
            
//            process.execFile("cmd.exe", [ "/C", "mode" ], null, function (err, stdout, stderr) {
//            var match           = /Columns:\s*(\d+)/.exec(output)
//            
//            return match ? Number(match[ 1 ]) : 80
//            })
        },
        
        
        // this method runs before the "prepareOptions" call
        createRunners : function () {
            return [
                new Siesta.Launcher.Runner.PhantomJS({
                    launcher    : this
                })
            ]
        },
        
        
        printVersion : function () {
            this.SUPERARG(arguments)
            
            var version     = phantom.version
            
            this.print("PhantomJS: " + version.major + '.' + version.minor + '.' + version.patch)
        },
        
        
        doExit : function (code) {
            phantom.exit(code)
        },
        
        
        print : function (text, level) {
            require('system').stdout.writeLine(this.prepareText(text, false, level))
        },
        
        
        printErr : function (text, level) {
            require('system').stderr.writeLine(this.prepareText(text, false, level))
        },
        
        
        checkIsWindows : function () {
            return /^win/i.test(require('system').os.name)
        },

        
        checkIsMacOS : function () {
            return null
        },
        
        
        checkIs64Bit : function () {
            return null
        },
        
        
        buildHarnessUrlQueryParams : function () {
            return Joose.O.extend(this.SUPER(), {
                phantom     : true
            })
        }
    }
    // eof methods
})
