Ext.define('MAM.view.connectionBuilding.ListController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.connectionBuildingListController',
    control: {
        '#': {
            boxready: 'loadConnectionBuildingss'
        }
    },

    loadConnectionBuildingss: function( ){
        var store = this.getViewModel().getStore('connectionBuildings');
        this.fireEvent('toggleLoadingMask', true , _tl.get('mam.label.ui.loading.connectionBuilding'));
        this.loadStore( store );
    },

    onCustomIdSearch: function( textfield ){
        var store = this.getView().getStore();

        if(!Ext.isEmpty(textfield.getValue())){
            Ext.merge(store.getProxy().extraParams, {customId: textfield.getValue()});
            this.loadStore( store );
        }
        else{
            delete store.getProxy().extraParams.customId;
            this.loadStore( store );
        }

    },
    loadStore: function( store ){
        store.load({
            scope: this,
            callback: function(){
                this.fireEvent('toggleLoadingMask', false );
            }
        });
    }
});