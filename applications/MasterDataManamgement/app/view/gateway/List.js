Ext.define('MAM.view.gateway.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gatewayList',
    requires: [
        'MAM.view.gateway.ListModel',
        'MAM.view.gateway.ListController'
    ],
    viewModel: 'gatewayListModel',
    controller: 'listController',
    border: true,
    bind: {
        store: '{gateways}'
    },
    viewConfig: {
        loadMask: false
    },
    dockedItems: [{
        xtype: 'pagingtoolbar',
        reference: 'pagingToolbar',
        bind: {
            store: '{gateways}'
        },
        overflowHandler: 'menu',
        dock: 'bottom',
        displayInfo: true
    }],
    initComponent: function () {
        var columns = [
            {
                text: _tl.get('mam.label.gateway.customId'),
                dataIndex: 'customId',
                flex: 1,
                items: [{
                    xtype: 'textfield',
                    margin: 3,
                    flex: 1,
                    width: 270, //theme error
                    enableKeyEvents: true,
                    listeners: {
                        keyup: 'onCustomIdSearch',
                        buffer: 1000
                    }
                }]
            }
        ];
        Ext.apply(this, {columns: columns});
        this.callParent(arguments);
    }

});