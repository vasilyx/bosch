/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
!function () {
/* header */
    
var fs      = require('fs')
var fse     = require('fs-extra')
var path    = require('path')
    
Role('Siesta.Launcher.FileSystem.NodeJS', {
    
    does        : Siesta.Launcher.FileSystem.Base,
    
    has : {
    },
    
    
    methods : {
        
        fileExists : function (fileName, mode) {
            fileName        = this.normalizePath(fileName)
            
            try {
                fs.accessSync(fileName, mode)
            } catch (e) {
                return false
            }
            
            return true
        },
        
        
        deleteFile : function (fileName) {
            try {
                fs.unlinkSync(fileName)
            } catch (e) {
                return false
            }
            
            return true
        },
        
        
        readFile : function (fileName, encoding) {
            fileName        = this.normalizePath(fileName)
            
            return fs.readFileSync(fileName, encoding === null ? null : 'utf8')
        },
        
        
        saveFile : function (fileName, content, encoding) {
            fileName        = this.normalizePath(fileName)
            
            fse.outputFileSync(fileName, content, encoding === null ? null : 'utf8')
        },
        
        
        copyFile : function (source, dest) {
            this.saveFile(dest, this.readFile(source, null), null)            
        },
        
        
        copyTree : function (source, dest) {
            fse.copySync(source, dest, { clobber : true })
        },
        
        
        normalizePath : function (str) {
            return path.resolve(str)
        },
        
        // following methods are not part of the interface (only used in webdriver launcher, but not in phantomjs) 
        
        ensurePathExists : function (fileName) {
            fileName        = this.normalizePath(fileName)
            
            try {
                fse.ensureDirSync(path.dirname(fileName))
            } catch (e) {
                return false
            }
            
            return true
        }
    }
})

/* footer */
}()