/**
 * JSON Simlet.
 */
Ext.define('Energy.common.ux.ajax.PlainJsonSimlet', {
    extend: 'Ext.ux.ajax.DataSimlet',
    alias: 'simlet.plainjson',

    doGet: function (ctx) {
        var me = this,
            data = me.getData(ctx);
            page = me.getPage(ctx, data),
            ret = me.callParent(arguments), // pick up status/statusText
            response = {};

        response = page;

        if (ctx.groupSpec) {
            response.summaryData = me.getSummary(ctx, data, page);
        }

        ret.responseText = Ext.encode(response);

        return ret;
    },

    doDelete: function (ctx) {
        var me = this,
            xhr = ctx.xhr;

        me.callParent(arguments);
        return {status: xhr.simlet.status, statusText: xhr.simlet.statusText};
    },

    doPut: function (ctx) {
        return this.doGet(ctx);
    },

    doPost: function (ctx) {
        var me = this;

        var resultSimplet;
        me.manager.simlets.forEach(function(simlet) {

            if (simlet.hasOwnProperty('method') && simlet.method == 'post') {

                if ((me.url.charAt(me.url.length-1) === '/' && simlet.url ===  me.url)
                    || (me.url.charAt(me.url.length-1) !== '/' && simlet.url ===  me.url + '/')) {
                    resultSimplet = simlet;
                }

            }
        });

        return resultSimplet;
    }
});
