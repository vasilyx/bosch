/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Ariadne.DomQueryFinder.Identifier.TagName', {
    
    isa         : Ariadne.QueryFinder.Identifier,
    
    
    methods : {
        
        identify : function (target, root, maze) {
            return {
                query           : target.tagName.toLowerCase(),
                leading         : true
            }
        }
    }
});
