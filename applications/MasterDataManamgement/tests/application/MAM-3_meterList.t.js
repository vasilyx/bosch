StartTest( function(t) {

    var list = Ext.create('TestApplication.application.AbstractList', {t: t, name: 'meter'});

    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/details_1494747.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/details_1494794.json');
    //dummy only to mock data and don't get error from browser
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/customAttributes_dummy.json', 1494747);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/relations_dummy.json', 1494747);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/customAttributes_dummy.json', 1494794);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/relations_dummy.json', 1494794);

    list.testListViewArea();
    list.testFilledDataInList(21);
    list.testPageCorrect();
    list.testDetailArea();
    list.testFilterById('G22175AAA65994');
    list.testFilterEmpty();
    list.testSelectRow(1); // select second row
    list.testSelectRow(4, 8);
});
