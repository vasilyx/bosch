Ext.define("Energy.common.ui.NavigationButton", {
    extend: 'Ext.button.Button',
    alias: 'widget.commonUiNavigationButton',
    config: {
        scale: 'large',
        cls: 'appNavButton',
        textAlign: 'left',
        isSelected: false,
        isFirst: false, /* have to be set at vertical buttons, needed for border creation*/
        isLast: false, /* have to be set at vertical buttons, needed for border creation*/
        height: 50,
        buttonAlign: 'hor', /* buttonAlig hor is for horizontal or ver for vertical */
        buttonOrientation: 'top', /* orientation to align active button image */
        imagePath: '/CertificateManagement/classic/resources/energy-common/dayButton/'
    },
    initComponent: function (config) {
        if (this.isSelected) {
            this.removeCls('appNavButton');
            this.addCls('appNavButtonSelected');
        }
        this.initConfig(config);
        this.callParent(config);
    },
    listeners: {
        'beforerender': function (button, eOpts) {
            var path;
            if (button.up().imagePath != undefined) {
                path = button.up().imagePath;
            } else {
                path = button.imagePath;
            }
            var buttonImage = path + 'dayButtonSelectedBg_bottom.png';
            if (button.buttonAlign == 'hor' && button.buttonOrientation == 'top') {
                buttonImage = path + 'dayButtonSelectedBg_top.png';
            } else if (button.buttonAlign == 'hor' && button.buttonOrientation == 'bottom') {
                buttonImage = path + 'dayButtonSelectedBg_bottom.png';
            } else if (button.buttonAlign == 'ver' && button.buttonOrientation == 'left') {
                buttonImage = path + 'dayButtonSelectedBg_left.png';
            } else if (button.buttonAlign == 'ver' && button.buttonOrientation == 'right') {
                buttonImage = path + 'dayButtonSelectedBg_right.png';
            }
            if (button.buttonAlign == 'ver') {
                if (!button.isFirst && !button.isLast) {
                    button.addCls('noBorder');
                }
            }
            button.addCls('active');
            var template = '<span id="{id}-btnWrap" class="{baseCls}-wrap<tpl if="splitCls">{splitCls}</tpl>{childElCls} preselect" unselectable="on">' +
                '<span id="{id}-btnEl" class="{baseCls}-button app-button-body"><span id="{id}-btnInnerEl" class="{baseCls}-inner {innerCls}{childElCls}" unselectable="on">{text}</span>' +
                '<span role="img" id="{id}-btnIconEl" class="{baseCls}-icon-el {iconCls}{childElCls} {glyphCls}" unselectable="on" style="<tpl if="iconUrl">' +
                'background-image:url({iconUrl});</tpl><tpl if="glyph && glyphFontFamily">font-family:{glyphFontFamily};</tpl>"><tpl if="glyph">&#{glyph};</tpl>' +
                '<tpl if="iconCls || iconUrl">&#160;</tpl></span></span></span><tpl if="closable"><span id="{id}-closeEl" class="{baseCls}-close-btn" title="{closeText}" tabIndex="0"></span></tpl>' +
                '<img src="' + buttonImage + '" class="appNavButtonImg ' + button.buttonAlign + ' ' + button.buttonOrientation + '"/>';
            button.renderTpl = new Ext.XTemplate(template);
        }
    },
    markButtonActive: function (clickedButton) {
        //remove all buttons class selected
        var navButtons = clickedButton.up();
        var buttonsLength = navButtons.items.length;
        for (var i = 0; i < buttonsLength; i++) {
            var cur = navButtons.items.getAt(i);
            if (cur.getXType() == 'NavigationButton') {
                cur.removeCls('appNavButtonSelected');
                cur.removeCls('active');
                cur.addCls('appNavButton');
                cur.setIsSelected(false);
            }
        }
        //set current button as selected
        clickedButton.removeCls('appNavButton');
        clickedButton.addCls('appNavButtonSelected');
        clickedButton.addCls('active');
        clickedButton.setIsSelected(true);
    }
}); 