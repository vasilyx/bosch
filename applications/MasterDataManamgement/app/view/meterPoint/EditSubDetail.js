Ext.define('MAM.view.meterPoint.EditSubDetail', {
    extend: 'Ext.tab.Panel',
    requires: [
        'MAM.view.relations.Edit',
        'MAM.view.customAttributes.Edit',
        'MAM.view.customAttributes.InitialController'
    ],
    alias: 'widget.meterPointEditSubDetail',

    initComponent: function () {
        var items = [];
        var tabConfig = MAM.utils.Configuration.getTabConfig( MAM.utils.Constants.METERPOINT );

        Ext.each(tabConfig, function( tab ){

            if (tab.type == 'customAttributes') {
                items.push({
                    xtype: tab.type + 'Edit',
                    title: _tl.get("mam.label." + MAM.utils.Constants.METERPOINT + "." + tab.type),
                    itemId: MAM.utils.Constants.METERPOINTS + '-tab-' + tab.type,
                    viewModel : {
                        type: 'customAttributesInitialModel',
                        data: {
                            entity: MAM.utils.Constants.METERPOINT
                        }
                    }
                });
            } else {
                items.push({
                    xtype: tab.type + 'Edit',
                    title: _tl.get("mam.label." + MAM.utils.Constants.METERPOINT + "." + tab.type),
                    itemId: MAM.utils.Constants.METERPOINTS + '-tab-' + tab.type
                });
            }

        });

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    }
});