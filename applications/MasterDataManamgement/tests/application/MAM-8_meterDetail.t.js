StartTest(function (t) {

    var list = Ext.create('TestApplication.application.AbstractList', {t: t, name: 'meter'});
        list.testListViewArea();

    var referenceData = {
        customId: 'G22175AAA25172',
        meterId: 1494785,
        status: {en: "Installed", de: "Eingebaut"},
        deviceType: 22175,
        lotNumber: '04931',
        manufacturer: 'GASELAN',
        calibrationYear: 1999,
        calibrationEndYear: 2015,
        medium: {en: "Electricity", de: "Elektrizität"},
        measuringPrinciple: {en: "Absolute", de: "Absolut"}
    };

    var referenceDataDetail = {
        customId: 'DE0700081632100000000000070238321',
        medium: {en: "Electricity", de: "Elektrizität"},
        consumptionUnit: 'kwh'
    };

    var referenceDataCustomAttributes = {
        de: {
            "Wechselverfahren": 'L',
            "Gerätegruppe": 'G4',
            "Wechselverfahrenname": 'Stichprobenverfahren',
            "Gerätetypname": 'BGZ/Kunststoffmembrane'
        },
        en: {
            "ChangeProcess": 'L',
            "Device Group": 'G4',
            "Change Process Name": 'Stichprobenverfahren',
            "Device Type Name": 'BGZ/Kunststoffmembrane'
        }
    };

    t.it('#2 Enter full entity id into filter filed', function(t) {
        t.chain(
            function(next) {
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/details_G22175AAA25172.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/customAttributes_G22175AAA25172.json', referenceData.meterId);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/relations_G22175AAA25172.json', referenceData.meterId);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/r/details_1491237.json'); // for related
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/r/details_1491238.json'); // for related
                next();
            },
            { doubleclick : t.getFirstRow(t.cq1('meterInitial gridpanel')) },
            { waitForCQ : 'meterDetail' },
            { waitForComponentVisible : 'meterDetail'},
            {
                action      : 'wait',
                delay       : 1500      // 1500 ms
            },
            function(next) {

                t.cqExists('meterDetail', 'Meter Detail view exist');

                for (var code in referenceData) {
                    var val = referenceData[code].hasOwnProperty(_locale.getLocale()) ? referenceData[code][_locale.getLocale()] : referenceData[code];
                    t.contentLike(t.cq1('meterDetail displayfield[name="' + code + '"]'), val, 'Found right  #' + code + ' ' + val);
                }

                t.expect(t.cq1('meterDetail grid[reference="meterRegisters"]').getStore().getTotalCount()).toBe(2);
                Ext.each(t.cq1('meterDetail grid[reference="meterRegisters"]').getStore().getRange(), function(value)
                {
                    t.expect(value.get('name')).toBe('7-20:3.0.0');
                });
            }
        )
    });

    t.it('#3 Check person Sub Tabs', function (t) {
        t.chain(
            {
                waitForCQ: 'meterSubDetail',
                waitForCQ: 'customAttributesInitial'
            }, function (next) {
                t.cqExists('meterSubDetail', 'meter Detail Tabs view exist');
                t.elementIsVisible(t.cq1('relationsInitial'), 'Tab "Relations" is visible');
                t.cqExists('relationsInitial', 'Relations tab exist');
                t.cqExists('customAttributesInitial', 'Custom Attributes tab exist');
                next();
            }
        );
    });
    //#4 first tab contains relations => Expected Result: 1 relation availalbe (Meter Point)
    //#4 First tab contains relations => Expected Result: 1 relation availalbe (Meter Point)
    t.it('#4 First tab contains relations', function (t) {
        t.chain(
            {
                waitForCQ: 'meterSubDetail',
                waitForComponentVisible: 'relationDetail'
            },
            {
                click: t.compositeQuery('meterSubDetail tabbar => .x-tab')[0]
            }, {
                waitForCQ: 'relationsInitial'
            }, function (next) {
                t.elementIsVisible(t.cq1('meterSubDetail relationsInitial grid'), 'Relations is visible');
                t.expect(t.cq1('meterSubDetail').items.length).toBe(2);

                var accordionEl = t.cq('relationsInitial #relationAccordion grid');
                t.like(accordionEl[0].getTitle(), Energy.common.language.Translations.get("mam.label.meterPoints"), 'Accordion meter is visible');
                t.expect(accordionEl.length).toBe(1);

            }
        );
    });

    //#5 meter point relation view => Expected Result: two Meter Points and detail view is visible
    //(Check Meter Point Id: DE0700081632100000000000070238321, Medium: Electricity and Consumption Unit: kwh)
    t.it('#5 Meter point relation view', function (t) {
        t.chain(
            {
                click: t.compositeQuery('meterSubDetail relationsInitial grid => .x-grid-cell')[1],
                waitForComponentVisible: 'meterPointDetail'
            },
             function (next) {
                 t.expect( t.cq1('meterSubDetail relationsInitial grid').getStore().getCount()).toBe(2);
                 t.elementIsVisible(t.cq1('relationsInitial meterPointDetail'), 'MeterPointDetail is visible');

                 for (var code in referenceDataDetail) {
                     var val = referenceDataDetail[code].hasOwnProperty(_locale.getLocale()) ? referenceDataDetail[code][_locale.getLocale()] : referenceDataDetail[code];
                     t.contentLike(t.cq1('relationsInitial meterPointDetail displayfield[name="' + code + '"]'), val, 'Found right  #' + code + ' ' + val);
                     //TODO: (review 25.11.2016) check medium und consumption unit in detail view is missing - "referenceDataDetail"
                     //TODO: MAM-2750 check medium language sensitive (key)
                     //EN: Electricity
                     //DE: Elektrizität

                 }
            }
        );
    });

    //#6 Custom Attributes of the meter tab => Expected Result: four items with values
    // key: Device Group and Value: G4,
    // key: ChangeProcess and Value: L,
    // key: Change Process Name and Value: Stichprobenverfahren,
    // key: Device Type Name and Value: BGZ/Kunststoffmembrane
    t.it('#6 Custom Attributes of the meter tab', function (t) {
        t.chain(
            {
                click: t.compositeQuery('meterSubDetail tabbar => .x-tab')[1]
            },
            function (next) {
                t.expect( t.cq1('customAttributesInitial').getStore().getCount()).toBe(4);

                for (var code in referenceDataCustomAttributes[_locale.getLocale()]) {
                    t.like(t.cq1('customAttributesInitial').getStore().findRecord('key', code).get('value'), referenceDataCustomAttributes[_locale.getLocale()][code]);
                }
            }
        );
    });
});