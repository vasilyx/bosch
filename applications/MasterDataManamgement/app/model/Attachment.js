Ext.define('MAM.model.Attachment', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'id',  type: 'int', mapping: 'attachmentId'},
        { name: 'customId', mapping: 'id'}, // Only for displaing in block in v2 API customId - null
        { name: 'description' },
        { name: 'documentType' },
        { name: 'mimeType' },
        { name: 'filename' },
        { name: 'timestamp',  convert: MAM.utils.model.Converts.convertDate },
        { name: 'fileTimestamp',  convert: MAM.utils.model.Converts.convertDate }
    ]
});