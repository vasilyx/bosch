/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Siesta.Harness.Browser.meta.extend({

    has : {
        commandId           : 1,
        
        commands            : null,
        commandCallbacks    : null
    },

    
    override : {
        
        start : function () {
            this.commands               = []
            this.commandCallbacks       = {}
            
            this.on('beforescreenshot', this.onBeforeScreenshot, this)
            this.on('screenshot', this.onScreenshot, this)
            
            this.SUPERARG(arguments)
        },
        
        
        onCommandDone : function (commandId, result) {
            this.lastActivity           = new Date()
            
            var callback                = this.commandCallbacks[ commandId ]
            delete this.commandCallbacks[ commandId ]
            
            try {
                callback.call(this, result)
            } catch (e) {
                // this should not happen generally, saving the exception to check the logs
                return e + ''
            }
        },
        
        
        queueCommand : function (command, callback) {
            this.lastActivity           = new Date()
            
            var id                      = this.commandId++
            
            command.id                  = id
            this.commandCallbacks[ id ] = callback
            
            this.commands.push(command)
        },

        
        flushAutomationCommands : function () {
            var commands                = this.commands
            
            if (commands.length) {
                this.commands           = []
                
                return commands
            }
        },
        
        
        onBeforeScreenshot : function (event) {
            document.body.scrollTop = document.body.scrollLeft = 0
            
            this.showForcedIFrameScreenshot(event.source)
        },
        
        
        onScreenshot : function (event) {
            this.hideForcedIFrameScreenshot(event.source)
        }
    }
})


