Ext.define('Energy.common.ui.SearchFilter', {
    extend: 'Ext.form.field.Trigger',
    alias: 'widget.commonUiSearchFilter',
    style: {
    	top: '5px',
    	position: 'relative'
    },
    triggerCls: 'x-form-clear-trigger',
    /*trigger2Cls: 'x-form-search-trigger',*/
    onTriggerClick: function() {
    	this.setValue('');
        this.setFilter(this.up().dataIndex, '');
    },
    /*onTrigger2Click: function() {
        this.setFilter(this.up().dataIndex, this.getValue());
    },*/
    setFilter: function(filterId, value){
        var store = this.up('grid').getStore();
        if(value){
            store.removeFilter(filterId, false); 
            var filter = {id: filterId, property: filterId, value: value};
            if(this.anyMatch) {filter.anyMatch = this.anyMatch;}
            if(this.caseSensitive) {filter.caseSensitive = this.caseSensitive;}
            if(this.exactMatch) {filter.exactMatch = this.exactMatch;}
            if(this.operator) {filter.operator = this.operator;}
            store.addFilter(filter);
        
        } else {
            store.filters.removeAtKey(filterId);
            store.reload();
        }
    },
    listeners: {
        render: function(){
            var me = this;
            me.ownerCt.on('resize', function(){
                me.setWidth(this.getEl().getWidth());
            });
        },
        change: function() {
            if(this.autoSearch) {this.setFilter(this.up().dataIndex, this.getValue());}
        }
    }/*,
    initComponent : function() {
        /*this.on("focus", function() {
            console.log("Trigger focused");
            //the problem point.
            //I do some processing here and then 
            //in some case I do the following:
            console.log(this.getName());
            //when this is called the BLUR of this trigger field isn't fired.
        });
        this.on("blur", function(comp, event) {
            console.log("Trigger blurred");
            console.log(comp.getName());
            console.log(comp);
            console.log(event);
        });
        this.callParent();
    }*/
});


