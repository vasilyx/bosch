Ext.define('MAM.view.relations.Initial', {
    extend: 'Ext.container.Container',

    requires: [
        'MAM.view.relations.InitialController',
        'MAM.view.relations.InitialModel',
        'MAM.view.meter.List',
        'MAM.view.gateway.List',
        'Energy.common.ui.DisplayMessage'
    ],

    alias: 'widget.relationsInitial',

    viewModel: 'relationsInitialModel',
    controller: 'relationsInitialController',
    border: false,
    flex: 1,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    initComponent: function () {

        this.callParent(arguments);
        if (Ext.isEmpty(this.title)) {
            this.title = _tl.get("mam.label.tab.relations");
        }

        this.createEmptyContainer();
    },

    createEmptyContainer: function () {
        this.add({
            xtype: 'panel',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            flex: 1,
            margin: '0 10 0 0',
            reference: 'relationDetail'
        });
    },

    createRelationContainer: function () {
        this.add(
            {
                xtype: 'panel',
                itemId: 'relationAccordion',
                reference: 'relationAccordion',
                width: 300,
                cls: 'relationAccordion',
                layout: {
                    type: 'accordion'
                }
            },
            {
                xtype: 'panel',
                flex: 1,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                reference: 'relationDetail'
            }
        );

    },
    createRelationPanel: function (entity, entities, relationCount) {

        this.lookupReference('relationAccordion').add({
            xtype: 'grid',
            title: _tl.get('mam.label.' + entities) + ' (' + relationCount + ')',
            bind: {
                store: '{' + entities + '}'
            },
            itemId: entities,
            viewConfig: {
                loadMask: false
            },
            flex: 1,
            columns: [{
                text: _tl.get('mam.label.' + entity + '.customId'),
                xtype: 'templatecolumn',
                tpl: '{customId} {associatedCompType} {associatedCompId}',
                renderer: function(a, b, record) {
                    if (record.get('customId')) {
                        return record.get('customId');
                    } else {
                        return record.get('associatedCompType') + ' ' +  record.get('associatedCompId');

                    }
                },
                flex: 1
            }]
        })
    },
    showEmptyMessage: function () {
        this.lookupReference('relationDetail').removeAll();
        this.lookupReference('relationDetail').add({
            xtype: 'commonUiDisplayMessage',
            message: _tl.get('mam.label.ui.noData')

        });

    }

});