/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
!function () {
/* header */

var scriptIdGen         = 1

Role('Siesta.Launcher.Script.CanExecute', {
    
    requires    : [ 'executeSmallScriptPromised', 'print' ],
    
    does        : [
        Siesta.Launcher.Script.CanSplitTextIntoChunks
    ],
    
    has : {
        maxMessageSize                  : 700000
    },
    
    
    methods : {
    
        shouldStopScriptExecution : function () {
            return false
        },
        
    
        executeScript : function (text, ignoreException) {
            var me              = this
            var scriptId        = scriptIdGen++
            
            var chunks          = this.splitIntoChunks(text, this.maxMessageSize)
            var promise         = Promise.resolve()
            
            Joose.A.each(chunks, function (chunk, index) {
                var isLastChunk = index == chunks.length - 1
                
                promise         = promise.then(function () {
                    if (me.shouldStopScriptExecution()) return null
                    
                    return me.executeSmallScriptPromised(
                        "return Siesta.Harness.Browser.Automation.ScriptExecutor().acceptScriptChunk(" +
                            scriptId + ", " +
                            me.maxMessageSize + ", " +
                            chunk + ", " + 
                            index + ", " +
                            isLastChunk +
                        ")",
                        ignoreException
                    )
                })
            })
            
            return promise.then(function (result) {
                if (me.shouldStopScriptExecution()) return null
                
                if (!result) throw "Wrong `executeScript` flow"
                
                result                  = JSON.parse(result)
                
                if (result.exception) {
                    if (!ignoreException) {
                        me.print("<Exception from launcher>")
                        me.print("    While running big script: " + text.substring(0, 150))
                        me.print("    Exception: " + result.exception)
                        me.print("</Exception from launcher>")
                        
                        throw result.exception
                    }
                    
                    return null
                }
                
                return me.combineScriptResults(scriptId, result)
            })
        },
        
        combineScriptResults : function (scriptId, result, resultChunks) {
            var me          = this
            resultChunks    = resultChunks || []
            
            resultChunks.push(result.chunk)
            
            if (result.isLastChunk || me.shouldStopScriptExecution()) {
                return JSON.parse(resultChunks.join(''))
            } else {
                if (me.shouldStopScriptExecution()) return Promise.resolve(null)
                
                return me.executeSmallScriptPromised(
                    "return Siesta.Harness.Browser.Automation.ScriptExecutor().retrieveScriptResult(" +
                        scriptId + ", " +
                        resultChunks.length +
                    ")"
                ).then(function (result) {
                    result      = JSON.parse(result)
                    
                    return me.combineScriptResults(scriptId, result, resultChunks)
                })
            }
        }
    }
})


/* footer */
}()