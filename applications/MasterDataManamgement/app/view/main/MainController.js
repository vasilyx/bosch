Ext.define('MAM.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',
    listen: {
        controller: {
            '*': {
                toggleLoadingMask: 'toggleLoadingMask'
            }
        }
    },
    routes : {
        '/:entity/itemId/:id' : 'setTab'
    },

    setTab: function(entity) {
        var entity = entity + 'Initial';
        var panel = this.getView().items.items[0];
        Ext.each(panel.items.items, function( item, k ){
            if (item.getXType() === entity ) {
                panel.setActiveItem(k);
            }
        });
    },

    /**
     * enable or disable a loading mask to the main component
     * @param acitvate
     * @param text
     */
    toggleLoadingMask: function( acitvate, text ){
        var message = _tl.get('mam.label.ui.loadingData');
        if(text){
            message = text;
        }
        if(acitvate){
            this.getView().setLoading( message );
        }
        else{
            this.getView().setLoading( acitvate );
        }

    }
});
