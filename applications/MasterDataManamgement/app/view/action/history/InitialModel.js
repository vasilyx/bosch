Ext.define('MAM.view.action.history.InitialModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.actionHistoryInitialModel',
    data: {
        editMode: false
    }
});
