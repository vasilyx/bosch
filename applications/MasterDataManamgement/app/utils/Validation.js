Ext.define('MAM.utils.Validation', {
    singleton: true,
    isObjectEmpty: function ( obj ) {
        return Object.keys(obj).length === 0 && obj.constructor === Object;
    }
    
});