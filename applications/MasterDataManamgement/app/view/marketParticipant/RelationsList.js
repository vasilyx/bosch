Ext.define('MAM.view.merketParticipant.RelationsList', {
    extend: 'MAM.view.relations.EntityList',
    requires: [
        'MAM.view.relations.EntityList'
    ],
    alias: 'widget.marketParticipantsRelationsList',

    getToolbar: function () {
        return [{
            xtype: 'textfield',
            name: 'codeType',
            hideLabel: true,
            width: 200
        }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'name',
                hideLabel: true,
                width: 120
            }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'code',
                hideLabel: true,
                width: 120
            }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'role',
                hideLabel: true,
                flex: 1
            }, this.getButtonClear()
        ];
    },

    getColumns: function () {
        return [
            {text: _tl.get("mam.label.marketParticipant.customId"), dataIndex: 'codeType', width: 250},
            {text: _tl.get("mam.label.marketParticipant.name"), dataIndex: 'name', width: 170},
            {text: _tl.get("mam.label.marketParticipant.code"), dataIndex: 'code', width: 170},
            {text: _tl.get("mam.label.marketParticipant.role"), dataIndex: 'role', flex: 1}
        ];
    },

    initComponent: function () {

        this.callParent(arguments);
    }
});