/**
 * Displays a user information message in a div with an icon
 * Usage example:
 * {
 *  xtype: 'DisplayMessage'
 *  message: 'my display text',
 *  type: 'error' //information is default, warning
 *  }
 */

Ext.define("Energy.common.ui.DisplayMessage", {
    extend: 'Ext.Container',
    alias: 'widget.commonUiDisplayMessage',
    
    cls: 'energy-message',
    margin: 10,
    style: {
        paddingTop: '10px',
        paddingLeft: '20px',
        paddingRight: '20px'
    },

    tpl: new Ext.XTemplate(
        '<span>',
        '{message}',
        '</span>'
    ),

    data: {message: ''},


    initComponent: function(){
        if (this.message){
            this.fillData({message: this.message});
        }
        this.setType(this.type || 'information');
        this.callParent();
    },

    fillData: function(data) {
        if(data){
            this.update(data);
        }
    },

    setType: function(type) {
        var typeMapping = {
            'information': 'energy-info',
            'warning': 'energy-warning',
            'error': 'energy-error',
            'success': 'energy-success',
            'question': 'energy-question'
        };

        // Set the current css class to the element
        this.addCls(typeMapping[type]);
        delete typeMapping[type];

        // Remove all other type specific css classes from element
        for (var property in typeMapping) {
            if (typeMapping.hasOwnProperty(property)) {
                this.removeCls(typeMapping[property]);
            }
        }

    }
});