Ext.define('MAM.view.person.Detail', {
    extend: 'Ext.Panel',

    alias: 'widget.personDetail',

    bind: {
        data: '{person}'
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    scrollable: true,
    
    initComponent: function () {

        var defaults = {
            xtype: 'displayfield',
            labelWidth: 150
        };

        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if(this.responsiveConfig){
            responsiveConfig = this.responsiveConfig;
        }

        var items = [
            {
                xtype: 'container',
                cls: 'bg-grey',
                margin: 10,
                padding: '5 10 0 10',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                defaults: defaults,
                items: this.getNorthItems()

            },
            {
                xtype: 'container',
                padding: '0 20 0 20',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getWestItems()
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getEastItems()
                    }
                ]
            }
        ];

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    },
    /*
     * Defines the top area items for connection building properties
     */
    getNorthItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.person.customId'),
                name: 'customId',
                flex: 1,
                bind: {
                    value: '{person.customId}'
                }
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                flex: 1,
                items: [
                    {
                        fieldLabel: _tl.get('mam.label.person.createdTimeStamp'),
                        flex: 1,
                        name: 'createdTimeStamp',
                        bind: {
                            value: '{person.createdTimeStamp}'
                        }
                    },
                    {
                        fieldLabel: _tl.get('mam.label.person.id'),
                        name: 'personId',
                        bind: {
                            value: '{person.id}'
                        }
                    }
                ]
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the left side of panel
     */
    getWestItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.person.salutation'),
                name: 'salutation',
                bind: {
                    value: '{person.salutation} {person.title}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.person.fullName'),
                name: 'name',
                bind: {
                    value: '{person.firstName} {person.name}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.person.nameAffix'),
                name: 'nameAffix',
                bind: {
                    value: '{person.nameAffix}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.person.streetAndNumber'),
                name: 'streetAndNumber',
                bind: {
                    value: '{person.street} {person.houseNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.person.houseNumberAppendix'),
                name: 'houseNumberAppendix',
                bind: {
                    value: '{person.houseNumberAppendix}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.person.postBoxCode'),
                name: 'postBoxCode',
                bind: {
                    value: '{person.postBoxCode}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.person.postalCodeAndCity'),
                name: 'postalCodeAndCity',
                bind: {
                    value: '{person.postalCode} {person.city}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.person.country'),
                name: 'country',
                bind: {
                    value: '{person.country}'
                }
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the right side of panel
     */
    getEastItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.person.phoneNumber'),
                name: 'phoneNumber',
                bind: {
                    value: '{person.phoneNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.person.mobileNumber'),
                name: 'mobileNumber',
                bind: {
                    value: '{person.mobileNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.person.emailAddress'),
                name: 'emailAddress',
                bind: {
                    value: '{person.emailAddress}'
                }
            }
            ];
        return items;
    }
});