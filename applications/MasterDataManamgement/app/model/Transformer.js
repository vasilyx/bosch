Ext.define('MAM.model.Transformer', {
    extend: 'Ext.data.Model',
    requires: [
        'MAM.model.Relation'
    ],
    idProperty: 'id',
    fields: [
        { name: 'id', type: 'int', mapping: 'transformerId'},
        { name: 'customId', type: 'string'},
        { name: 'deviceClass', type: 'string'},
        { name: 'deviceType', type: 'string'},
        { name: 'lotNumber', type: 'int'},

        { name: 'transformerConstant', type: 'string'},
        { name: 'transformerPhase', type: 'string'},
        { name: 'examinationNumber', type: 'string'},
        { name: 'status', type: 'string'},

        { name: 'buildInDate', convert: MAM.utils.model.Converts.convertDate},
        { name: 'buildOutDate', convert: MAM.utils.model.Converts.convertDate},
        { name: 'calibrationEndYear', convert: MAM.utils.model.Converts.convertDate},
        { name: 'calibrationYear', convert: MAM.utils.model.Converts.convertDate}
    ]
});