Ext.define('MAM.model.Certificate', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'certificate', type: 'string' }
    ]
});