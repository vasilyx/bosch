/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Siesta.Test.Browser.meta.extend({
    
    after : {
        
        cleanupContextBeforeStart : function () {
            // cleanup the coverage information (can remain from previous test in case of disabled sandbox)
            var coverage    = this.global.__coverage__
            
            if (!this.parent && coverage) {
                Joose.O.each(coverage, function (info, unitName) {
                    
                    Joose.O.each(info.s, function (value, name) {
                        info.s[ name ]  = 0
                    })
                    
                    Joose.O.each(info.b, function (value, name) {
                        Joose.A.each(info.b[ name ], function (value, index) {
                            info.b[ name ][ index ] = 0
                        })
                    })
                    
                    Joose.O.each(info.f, function (value, name) {
                        info.f[ name ]  = 0
                    })
                })
            }
        },
        
        
        onTestFinalize : function () {
            // test can be finalized even before it has started (no "global" yet) in case of
            // user restart
            if (!this.global) return
            
            if (!this.parent && this.contentManager && this.contentManager.addRawCoverageResultsFrom(this.global, this)) this.fireEvent('hassomecoverageinfo')
        }
    },

    methods : {

    }
});
