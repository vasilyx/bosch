﻿/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
if (typeof Ext != "undefined") {;
Ext.define("Ext.draw.ContainerBase",{extend:Ext.panel.Panel,previewTitleText:"Chart Preview",previewAltText:"Chart preview",layout:"container",addElementListener:function(){var b=this,a=arguments;if(b.rendered){b.el.on.apply(b.el,a)}else{b.on("render",function(){b.el.on.apply(b.el,a)})}},removeElementListener:function(){var b=this,a=arguments;if(b.rendered){b.el.un.apply(b.el,a)}},afterRender:function(){this.callParent(arguments);this.initAnimator()},getItems:function(){var b=this,a=b.items;if(!a||!a.isMixedCollection){b.initItems()}return b.items},onRender:function(){this.callParent(arguments);this.element=this.el;this.innerElement=this.body},setItems:function(a){this.items=a;return a},setSurfaceSize:function(b,a){this.resizeHandler({width:b,height:a});this.renderFrame()},onResize:function(c,a,b,e){var d=this;d.callParent([c,a,b,e]);d.setBodySize({width:c,height:a})},preview:function(){var a=this.getImage();new Ext.window.Window({title:this.previewTitleText,closeable:true,renderTo:Ext.getBody(),autoShow:true,maximizeable:true,maximized:true,border:true,layout:{type:"hbox",pack:"center",align:"middle"},items:{xtype:"container",items:{xtype:"image",mode:"img",cls:Ext.baseCSSPrefix+"chart-image",alt:this.previewAltText,src:a.data,listeners:{afterrender:function(){var e=this,b=e.imgEl.dom,d=a.type==="svg"?1:(window.devicePixelRatio||1),c;if(!b.naturalWidth||!b.naturalHeight){b.onload=function(){var g=b.naturalWidth,f=b.naturalHeight;e.setWidth(Math.floor(g/d));e.setHeight(Math.floor(f/d))}}else{c=e.getSize();e.setWidth(Math.floor(c.width/d));e.setHeight(Math.floor(c.height/d))}}}}}})},privates:{getTargetEl:function(){return this.innerElement},reattachToBody:function(){var a=this;if(a.pendingDetachSize){a.onBodyResize()}a.pendingDetachSize=false;a.callParent()}}});Ext.define("Ext.draw.SurfaceBase",{extend:Ext.Widget,getOwnerBody:function(){return this.ownerCt.body},destroy:function(){var a=this;if(a.hasListeners.destroy){a.fireEvent("destroy",a)}a.callParent()}});Ext.define("Ext.draw.Color",{statics:{colorToHexRe:/(.*?)rgb\((\d+),\s*(\d+),\s*(\d+)\)/,rgbToHexRe:/\s*rgb\((\d+),\s*(\d+),\s*(\d+)\)/,rgbaToHexRe:/\s*rgba\((\d+),\s*(\d+),\s*(\d+),\s*([\.\d]+)\)/,hexRe:/\s*#([0-9a-fA-F][0-9a-fA-F]?)([0-9a-fA-F][0-9a-fA-F]?)([0-9a-fA-F][0-9a-fA-F]?)\s*/,NONE:"none",RGBA_NONE:"rgba(0, 0, 0, 0)"},isColor:true,lightnessFactor:0.2,constructor:function(d,b,a,c){this.setRGB(d,b,a,c)},setRGB:function(e,c,a,d){var b=this;b.r=Math.min(255,Math.max(0,e));b.g=Math.min(255,Math.max(0,c));b.b=Math.min(255,Math.max(0,a));if(d===undefined){b.a=1}else{b.a=Math.min(1,Math.max(0,d))}},getGrayscale:function(){return this.r*0.3+this.g*0.59+this.b*0.11},getHSL:function(){var i=this,a=i.r/255,f=i.g/255,j=i.b/255,k=Math.max(a,f,j),d=Math.min(a,f,j),m=k-d,e,n=0,c=0.5*(k+d);if(d!==k){n=(c<=0.5)?m/(k+d):m/(2-k-d);if(a===k){e=60*(f-j)/m}else{if(f===k){e=120+60*(j-a)/m}else{e=240+60*(a-f)/m}}if(e<0){e+=360}if(e>=360){e-=360}}return[e,n,c]},getHSV:function(){var i=this,a=i.r/255,f=i.g/255,j=i.b/255,k=Math.max(a,f,j),d=Math.min(a,f,j),c=k-d,e,m=0,l=k;if(d!=k){m=l?c/l:0;if(a===k){e=60*(f-j)/c}else{if(f===k){e=60*(j-a)/c+120}else{e=60*(a-f)/c+240}}if(e<0){e+=360}if(e>=360){e-=360}}return[e,m,l]},setHSL:function(g,f,e){var i=this,d=Math.abs,j,b,a;g=(g%360+360)%360;f=f>1?1:f<0?0:f;e=e>1?1:e<0?0:e;if(f===0||g===null){e*=255;i.setRGB(e,e,e)}else{g/=60;j=f*(1-d(2*e-1));b=j*(1-d(g%2-1));a=e-j/2;a*=255;j*=255;b*=255;switch(Math.floor(g)){case 0:i.setRGB(j+a,b+a,a);break;case 1:i.setRGB(b+a,j+a,a);break;case 2:i.setRGB(a,j+a,b+a);break;case 3:i.setRGB(a,b+a,j+a);break;case 4:i.setRGB(b+a,a,j+a);break;case 5:i.setRGB(j+a,a,b+a);break}}return i},setHSV:function(f,e,d){var g=this,i,b,a;f=(f%360+360)%360;e=e>1?1:e<0?0:e;d=d>1?1:d<0?0:d;if(e===0||f===null){d*=255;g.setRGB(d,d,d)}else{f/=60;i=d*e;b=i*(1-Math.abs(f%2-1));a=d-i;a*=255;i*=255;b*=255;switch(Math.floor(f)){case 0:g.setRGB(i+a,b+a,a);break;case 1:g.setRGB(b+a,i+a,a);break;case 2:g.setRGB(a,i+a,b+a);break;case 3:g.setRGB(a,b+a,i+a);break;case 4:g.setRGB(b+a,a,i+a);break;case 5:g.setRGB(i+a,a,b+a);break}}return g},createLighter:function(b){if(!b&&b!==0){b=this.lightnessFactor}var a=this.getHSL();a[2]=Ext.Number.constrain(a[2]+b,0,1);return Ext.draw.Color.fromHSL(a[0],a[1],a[2])},createDarker:function(a){if(!a&&a!==0){a=this.lightnessFactor}return this.createLighter(-a)},toString:function(){var f=this,c=Math.round;if(f.a===1){var e=c(f.r).toString(16),d=c(f.g).toString(16),a=c(f.b).toString(16);e=(e.length===1)?"0"+e:e;d=(d.length===1)?"0"+d:d;a=(a.length===1)?"0"+a:a;return["#",e,d,a].join("")}else{return"rgba("+[c(f.r),c(f.g),c(f.b),f.a===0?0:f.a.toFixed(15)].join(", ")+")"}},toHex:function(b){if(Ext.isArray(b)){b=b[0]}if(!Ext.isString(b)){return""}if(b.substr(0,1)==="#"){return b}var e=Ext.draw.Color.colorToHexRe.exec(b);if(Ext.isArray(e)){var f=parseInt(e[2],10),d=parseInt(e[3],10),a=parseInt(e[4],10),c=a|(d<<8)|(f<<16);return e[1]+"#"+("000000"+c.toString(16)).slice(-6)}else{return""}},setFromString:function(j){var e,h,f,c,d=1,i=parseInt;if(j===Ext.draw.Color.NONE){this.r=this.g=this.b=this.a=0;return this}if((j.length===4||j.length===7)&&j.substr(0,1)==="#"){e=j.match(Ext.draw.Color.hexRe);if(e){h=i(e[1],16)>>0;f=i(e[2],16)>>0;c=i(e[3],16)>>0;if(j.length===4){h+=(h*16);f+=(f*16);c+=(c*16)}}}else{if((e=j.match(Ext.draw.Color.rgbToHexRe))){h=+e[1];f=+e[2];c=+e[3]}else{if((e=j.match(Ext.draw.Color.rgbaToHexRe))){h=+e[1];f=+e[2];c=+e[3];d=+e[4]}else{if(Ext.draw.Color.ColorList.hasOwnProperty(j.toLowerCase())){return this.setFromString(Ext.draw.Color.ColorList[j.toLowerCase()])}}}}if(typeof h==="undefined"){return this}this.r=h;this.g=f;this.b=c;this.a=d;return this}},function(){var a=new this();this.addStatics({fly:function(f,e,c,d){switch(arguments.length){case 1:a.setFromString(f);break;case 3:case 4:a.setRGB(f,e,c,d);break;default:return null}return a},ColorList:{aliceblue:"#f0f8ff",antiquewhite:"#faebd7",aqua:"#00ffff",aquamarine:"#7fffd4",azure:"#f0ffff",beige:"#f5f5dc",bisque:"#ffe4c4",black:"#000000",blanchedalmond:"#ffebcd",blue:"#0000ff",blueviolet:"#8a2be2",brown:"#a52a2a",burlywood:"#deb887",cadetblue:"#5f9ea0",chartreuse:"#7fff00",chocolate:"#d2691e",coral:"#ff7f50",cornflowerblue:"#6495ed",cornsilk:"#fff8dc",crimson:"#dc143c",cyan:"#00ffff",darkblue:"#00008b",darkcyan:"#008b8b",darkgoldenrod:"#b8860b",darkgray:"#a9a9a9",darkgreen:"#006400",darkkhaki:"#bdb76b",darkmagenta:"#8b008b",darkolivegreen:"#556b2f",darkorange:"#ff8c00",darkorchid:"#9932cc",darkred:"#8b0000",darksalmon:"#e9967a",darkseagreen:"#8fbc8f",darkslateblue:"#483d8b",darkslategray:"#2f4f4f",darkturquoise:"#00ced1",darkviolet:"#9400d3",deeppink:"#ff1493",deepskyblue:"#00bfff",dimgray:"#696969",dodgerblue:"#1e90ff",firebrick:"#b22222",floralwhite:"#fffaf0",forestgreen:"#228b22",fuchsia:"#ff00ff",gainsboro:"#dcdcdc",ghostwhite:"#f8f8ff",gold:"#ffd700",goldenrod:"#daa520",gray:"#808080",green:"#008000",greenyellow:"#adff2f",honeydew:"#f0fff0",hotpink:"#ff69b4",indianred:"#cd5c5c",indigo:"#4b0082",ivory:"#fffff0",khaki:"#f0e68c",lavender:"#e6e6fa",lavenderblush:"#fff0f5",lawngreen:"#7cfc00",lemonchiffon:"#fffacd",lightblue:"#add8e6",lightcoral:"#f08080",lightcyan:"#e0ffff",lightgoldenrodyellow:"#fafad2",lightgray:"#d3d3d3",lightgrey:"#d3d3d3",lightgreen:"#90ee90",lightpink:"#ffb6c1",lightsalmon:"#ffa07a",lightseagreen:"#20b2aa",lightskyblue:"#87cefa",lightslategray:"#778899",lightsteelblue:"#b0c4de",lightyellow:"#ffffe0",lime:"#00ff00",limegreen:"#32cd32",linen:"#faf0e6",magenta:"#ff00ff",maroon:"#800000",mediumaquamarine:"#66cdaa",mediumblue:"#0000cd",mediumorchid:"#ba55d3",mediumpurple:"#9370d8",mediumseagreen:"#3cb371",mediumslateblue:"#7b68ee",mediumspringgreen:"#00fa9a",mediumturquoise:"#48d1cc",mediumvioletred:"#c71585",midnightblue:"#191970",mintcream:"#f5fffa",mistyrose:"#ffe4e1",moccasin:"#ffe4b5",navajowhite:"#ffdead",navy:"#000080",oldlace:"#fdf5e6",olive:"#808000",olivedrab:"#6b8e23",orange:"#ffa500",orangered:"#ff4500",orchid:"#da70d6",palegoldenrod:"#eee8aa",palegreen:"#98fb98",paleturquoise:"#afeeee",palevioletred:"#d87093",papayawhip:"#ffefd5",peachpuff:"#ffdab9",peru:"#cd853f",pink:"#ffc0cb",plum:"#dda0dd",powderblue:"#b0e0e6",purple:"#800080",red:"#ff0000",rosybrown:"#bc8f8f",royalblue:"#4169e1",saddlebrown:"#8b4513",salmon:"#fa8072",sandybrown:"#f4a460",seagreen:"#2e8b57",seashell:"#fff5ee",sienna:"#a0522d",silver:"#c0c0c0",skyblue:"#87ceeb",slateblue:"#6a5acd",slategray:"#708090",snow:"#fffafa",springgreen:"#00ff7f",steelblue:"#4682b4",tan:"#d2b48c",teal:"#008080",thistle:"#d8bfd8",tomato:"#ff6347",turquoise:"#40e0d0",violet:"#ee82ee",wheat:"#f5deb3",white:"#ffffff",whitesmoke:"#f5f5f5",yellow:"#ffff00",yellowgreen:"#9acd32"},fromHSL:function(d,c,b){return(new this(0,0,0,0)).setHSL(d,c,b)},fromHSV:function(d,c,b){return(new this(0,0,0,0)).setHSL(d,c,b)},fromString:function(b){return(new this(0,0,0,0)).setFromString(b)},create:function(b){if(b instanceof this){return b}else{if(Ext.isArray(b)){return new Ext.draw.Color(b[0],b[1],b[2],b[3])}else{if(Ext.isString(b)){return Ext.draw.Color.fromString(b)}else{if(arguments.length>2){return new Ext.draw.Color(arguments[0],arguments[1],arguments[2],arguments[3])}else{return new Ext.draw.Color(0,0,0,0)}}}}}})});Ext.define("Ext.draw.sprite.AnimationParser",function(){function a(d,c,b){return d+(c-d)*b}return{singleton:true,attributeRe:/^url\(#([a-zA-Z\-]+)\)$/,color:{parseInitial:function(c,b){if(Ext.isString(c)){c=Ext.draw.Color.create(c)}if(Ext.isString(b)){b=Ext.draw.Color.create(b)}if((c instanceof Ext.draw.Color)&&(b instanceof Ext.draw.Color)){return[[c.r,c.g,c.b,c.a],[b.r,b.g,b.b,b.a]]}else{return[c||b,b||c]}},compute:function(d,c,b){if(!Ext.isArray(d)||!Ext.isArray(c)){return c||d}else{return[a(d[0],c[0],b),a(d[1],c[1],b),a(d[2],c[2],b),a(d[3],c[3],b)]}},serve:function(c){var b=Ext.draw.Color.fly(c[0],c[1],c[2],c[3]);return b.toString()}},number:{parse:function(b){return b===null?null:+b},compute:function(d,c,b){if(!Ext.isNumber(d)||!Ext.isNumber(c)){return c||d}else{return a(d,c,b)}}},angle:{parseInitial:function(c,b){if(b-c>Math.PI){b-=Math.PI*2}else{if(b-c<-Math.PI){b+=Math.PI*2}}return[c,b]},compute:function(d,c,b){if(!Ext.isNumber(d)||!Ext.isNumber(c)){return c||d}else{return a(d,c,b)}}},path:{parseInitial:function(m,n){var c=m.toStripes(),o=n.toStripes(),e,d,k=c.length,p=o.length,h,f,b,g=o[p-1],l=[g[g.length-2],g[g.length-1]];for(e=k;e<p;e++){c.push(c[k-1].slice(0))}for(e=p;e<k;e++){o.push(l.slice(0))}b=c.length;o.path=n;o.temp=new Ext.draw.Path();for(e=0;e<b;e++){h=c[e];f=o[e];k=h.length;p=f.length;o.temp.commands.push("M");for(d=p;d<k;d+=6){f.push(l[0],l[1],l[0],l[1],l[0],l[1])}g=o[o.length-1];l=[g[g.length-2],g[g.length-1]];for(d=k;d<p;d+=6){h.push(l[0],l[1],l[0],l[1],l[0],l[1])}for(e=0;e<f.length;e++){f[e]-=h[e]}for(e=2;e<f.length;e+=6){o.temp.commands.push("C")}}return[c,o]},compute:function(c,l,m){if(m>=1){return l.path}var e=0,f=c.length,d=0,b,k,h,n=l.temp.params,g=0;for(;e<f;e++){k=c[e];h=l[e];b=k.length;for(d=0;d<b;d++){n[g++]=h[d]*m+k[d]}}return l.temp}},data:{compute:function(h,j,k,g){var m=h.length-1,b=j.length-1,e=Math.max(m,b),d,l,c;if(!g||g===h){g=[]}g.length=e+1;for(c=0;c<=e;c++){d=h[Math.min(c,m)];l=j[Math.min(c,b)];if(Ext.isNumber(d)){if(!Ext.isNumber(l)){l=0}g[c]=(l-d)*k+d}else{g[c]=l}}return g}},text:{compute:function(d,c,b){return d.substr(0,Math.round(d.length*(1-b)))+c.substr(Math.round(c.length*(1-b)))}},limited:"number",limited01:"number"}});(function(){if(!Ext.global.Float32Array){var a=function(d){if(typeof d==="number"){this.length=d}else{if("length" in d){this.length=d.length;for(var c=0,b=d.length;c<b;c++){this[c]=+d[c]}}}};a.prototype=[];Ext.global.Float32Array=a}})();Ext.define("Ext.draw.Draw",{singleton:true,radian:Math.PI/180,pi2:Math.PI*2,reflectFn:function(b){return b},rad:function(a){return(a%360)*this.radian},degrees:function(a){return(a/this.radian)%360},isBBoxIntersect:function(b,a,c){c=c||0;return(Math.max(b.x,a.x)-c>Math.min(b.x+b.width,a.x+a.width))||(Math.max(b.y,a.y)-c>Math.min(b.y+b.height,a.y+a.height))},isPointInBBox:function(a,c,b){return !!b&&a>=b.x&&a<=(b.x+b.width)&&c>=b.y&&c<=(b.y+b.height)},spline:function(m){var e,c,k=m.length,b,h,l,f,a=0,g=new Float32Array(m.length),n=new Float32Array(m.length*3-2);g[0]=0;g[k-1]=0;for(e=1;e<k-1;e++){g[e]=(m[e+1]+m[e-1]-2*m[e])-g[e-1];a=1/(4-a);g[e]*=a}for(e=k-2;e>0;e--){a=3.732050807568877+48.248711305964385/(-13.928203230275537+Math.pow(0.07179676972449123,e));g[e]-=g[e+1]*a}f=m[0];b=f-g[0];for(e=0,c=0;e<k-1;c+=3){l=f;h=b;e++;f=m[e];b=f-g[e];n[c]=l;n[c+1]=(b+2*h)/3;n[c+2]=(b*2+h)/3}n[c]=f;return n},getAnchors:function(e,d,i,h,t,s,o){o=o||4;var n=Math.PI,p=n/2,k=Math.abs,a=Math.sin,b=Math.cos,f=Math.atan,r,q,g,j,m,l,v,u,c;r=(i-e)/o;q=(t-i)/o;if((h>=d&&h>=s)||(h<=d&&h<=s)){g=j=p}else{g=f((i-e)/k(h-d));if(d<h){g=n-g}j=f((t-i)/k(h-s));if(s<h){j=n-j}}c=p-((g+j)%(n*2))/2;if(c>p){c-=n}g+=c;j+=c;m=i-r*a(g);l=h+r*b(g);v=i+q*a(j);u=h+q*b(j);if((h>d&&l<d)||(h<d&&l>d)){m+=k(d-l)*(m-i)/(l-h);l=d}if((h>s&&u<s)||(h<s&&u>s)){v-=k(s-u)*(v-i)/(u-h);u=s}return{x1:m,y1:l,x2:v,y2:u}},smooth:function(l,j,o){var k=l.length,h,g,c,b,q,p,n,m,f=[],e=[],d,a;for(d=0;d<k-1;d++){h=l[d];g=j[d];if(d===0){n=h;m=g;f.push(n);e.push(m);if(k===1){break}}c=l[d+1];b=j[d+1];q=l[d+2];p=j[d+2];if(!Ext.isNumber(q+p)){f.push(n,c,c);e.push(m,b,b);break}a=this.getAnchors(h,g,c,b,q,p,o);f.push(n,a.x1,c);e.push(m,a.y1,b);n=a.x2;m=a.y2}return{smoothX:f,smoothY:e}},beginUpdateIOS:Ext.os.is.iOS?function(){this.iosUpdateEl=Ext.getBody().createChild({style:"position: absolute; top: 0px; bottom: 0px; left: 0px; right: 0px; background: rgba(0,0,0,0.001); z-index: 100000"})}:Ext.emptyFn,endUpdateIOS:function(){this.iosUpdateEl=Ext.destroy(this.iosUpdateEl)}});Ext.define("Ext.draw.gradient.Gradient",{isGradient:true,config:{stops:[]},applyStops:function(f){var e=[],d=f.length,c,b,a;for(c=0;c<d;c++){b=f[c];a=b.color;if(!(a&&a.isColor)){a=Ext.draw.Color.fly(a||Ext.draw.Color.NONE)}e.push({offset:Math.min(1,Math.max(0,"offset" in b?b.offset:b.position||0)),color:a.toString()})}e.sort(function(h,g){return h.offset-g.offset});return e},onClassExtended:function(a,b){if(!b.alias&&b.type){b.alias="gradient."+b.type}},constructor:function(a){this.initConfig(a)},generateGradient:Ext.emptyFn});Ext.define("Ext.draw.gradient.GradientDefinition",{singleton:true,urlStringRe:/^url\(#([\w\-]+)\)$/,gradients:{},add:function(a){var b=this.gradients,c,e,d;for(c=0,e=a.length;c<e;c++){d=a[c];if(Ext.isString(d.id)){b[d.id]=d}}},get:function(d){var a=this.gradients,b=d.match(this.urlStringRe),c;if(b&&b[1]&&(c=a[b[1]])){return c||d}return d}});Ext.define("Ext.draw.sprite.AttributeParser",{singleton:true,attributeRe:/^url\(#([a-zA-Z\-]+)\)$/,"default":Ext.identityFn,string:function(a){return String(a)},number:function(a){if(Ext.isNumber(+a)){return a}},angle:function(a){if(Ext.isNumber(a)){a%=Math.PI*2;if(a<-Math.PI){a+=Math.PI*2}else{if(a>=Math.PI){a-=Math.PI*2}}return a}},data:function(a){if(Ext.isArray(a)){return a.slice()}else{if(a instanceof Float32Array){return new Float32Array(a)}}},bool:function(a){return !!a},color:function(a){if(a instanceof Ext.draw.Color){return a.toString()}else{if(a instanceof Ext.draw.gradient.Gradient){return a}else{if(!a){return Ext.draw.Color.NONE}else{if(Ext.isString(a)){if(a.substr(0,3)==="url"){a=Ext.draw.gradient.GradientDefinition.get(a);if(Ext.isString(a)){return a}}else{return Ext.draw.Color.fly(a).toString()}}}}}if(a.type==="linear"){return Ext.create("Ext.draw.gradient.Linear",a)}else{if(a.type==="radial"){return Ext.create("Ext.draw.gradient.Radial",a)}else{if(a.type==="pattern"){return Ext.create("Ext.draw.gradient.Pattern",a)}else{return Ext.draw.Color.NONE}}}},limited:function(a,b){return function(c){c=+c;return Ext.isNumber(c)?Math.min(Math.max(c,a),b):undefined}},limited01:function(a){a=+a;return Ext.isNumber(a)?Math.min(Math.max(a,0),1):undefined},enums:function(){var d={},a=Array.prototype.slice.call(arguments,0),b,c;for(b=0,c=a.length;b<c;b++){d[a[b]]=true}return function(e){return e in d?e:undefined}}});Ext.define("Ext.draw.sprite.AttributeDefinition",{config:{defaults:{$value:{},lazy:true},aliases:{},animationProcessors:{},processors:{$value:{},lazy:true},dirtyTriggers:{},triggers:{},updaters:{}},inheritableStatics:{processorFactoryRe:/^(\w+)\(([\w\-,]*)\)$/},spriteClass:null,constructor:function(a){var b=this;b.initConfig(a)},applyDefaults:function(b,a){a=Ext.apply(a||{},this.normalize(b));return a},applyAliases:function(b,a){return Ext.apply(a||{},b)},applyProcessors:function(e,i){this.getAnimationProcessors();var j=i||{},h=Ext.draw.sprite.AttributeParser,a=this.self.processorFactoryRe,g={},d,b,c,f;for(b in e){f=e[b];if(typeof f==="string"){c=f.match(a);if(c){f=h[c[1]].apply(h,c[2].split(","))}else{if(h[f]){g[b]=f;d=true;f=h[f]}}}j[b]=f}if(d){this.setAnimationProcessors(g)}return j},applyAnimationProcessors:function(c,a){var e=Ext.draw.sprite.AnimationParser,b,d;if(!a){a={}}for(b in c){d=c[b];if(d==="none"){a[b]=null}else{if(Ext.isString(d)&&!(b in a)){if(d in e){while(Ext.isString(e[d])){d=e[d]}a[b]=e[d]}}else{if(Ext.isObject(d)){a[b]=d}}}}return a},updateDirtyTriggers:function(a){this.setTriggers(a)},applyTriggers:function(b,c){if(!c){c={}}for(var a in b){c[a]=b[a].split(",")}return c},applyUpdaters:function(b,a){return Ext.apply(a||{},b)},batchedNormalize:function(f,n){if(!f){return{}}var j=this.getProcessors(),d=this.getAliases(),a=f.translation||f.translate,o={},g,h,b,e,p,c,m,l,k;if("rotation" in f){p=f.rotation}else{p=("rotate" in f)?f.rotate:undefined}if("scaling" in f){c=f.scaling}else{c=("scale" in f)?f.scale:undefined}if(typeof c!=="undefined"){if(Ext.isNumber(c)){o.scalingX=c;o.scalingY=c}else{if("x" in c){o.scalingX=c.x}if("y" in c){o.scalingY=c.y}if("centerX" in c){o.scalingCenterX=c.centerX}if("centerY" in c){o.scalingCenterY=c.centerY}}}if(typeof p!=="undefined"){if(Ext.isNumber(p)){p=Ext.draw.Draw.rad(p);o.rotationRads=p}else{if("rads" in p){o.rotationRads=p.rads}else{if("degrees" in p){if(Ext.isArray(p.degrees)){o.rotationRads=Ext.Array.map(p.degrees,function(i){return Ext.draw.Draw.rad(i)})}else{o.rotationRads=Ext.draw.Draw.rad(p.degrees)}}}if("centerX" in p){o.rotationCenterX=p.centerX}if("centerY" in p){o.rotationCenterY=p.centerY}}}if(typeof a!=="undefined"){if("x" in a){o.translationX=a.x}if("y" in a){o.translationY=a.y}}if("matrix" in f){m=Ext.draw.Matrix.create(f.matrix);k=m.split();o.matrix=m;o.rotationRads=k.rotation;o.rotationCenterX=0;o.rotationCenterY=0;o.scalingX=k.scaleX;o.scalingY=k.scaleY;o.scalingCenterX=0;o.scalingCenterY=0;o.translationX=k.translateX;o.translationY=k.translateY}for(b in f){e=f[b];if(typeof e==="undefined"){continue}else{if(Ext.isArray(e)){if(b in d){b=d[b]}if(b in j){o[b]=[];for(g=0,h=e.length;g<h;g++){l=j[b].call(this,e[g]);if(typeof l!=="undefined"){o[b][g]=l}}}else{if(n){o[b]=e}}}else{if(b in d){b=d[b]}if(b in j){e=j[b].call(this,e);if(typeof e!=="undefined"){o[b]=e}}else{if(n){o[b]=e}}}}}return o},normalize:function(i,j){if(!i){return{}}var f=this.getProcessors(),d=this.getAliases(),a=i.translation||i.translate,k={},b,e,l,c,h,g;if("rotation" in i){l=i.rotation}else{l=("rotate" in i)?i.rotate:undefined}if("scaling" in i){c=i.scaling}else{c=("scale" in i)?i.scale:undefined}if(a){if("x" in a){k.translationX=a.x}if("y" in a){k.translationY=a.y}}if(typeof c!=="undefined"){if(Ext.isNumber(c)){k.scalingX=c;k.scalingY=c}else{if("x" in c){k.scalingX=c.x}if("y" in c){k.scalingY=c.y}if("centerX" in c){k.scalingCenterX=c.centerX}if("centerY" in c){k.scalingCenterY=c.centerY}}}if(typeof l!=="undefined"){if(Ext.isNumber(l)){l=Ext.draw.Draw.rad(l);k.rotationRads=l}else{if("rads" in l){k.rotationRads=l.rads}else{if("degrees" in l){k.rotationRads=Ext.draw.Draw.rad(l.degrees)}}if("centerX" in l){k.rotationCenterX=l.centerX}if("centerY" in l){k.rotationCenterY=l.centerY}}}if("matrix" in i){h=Ext.draw.Matrix.create(i.matrix);g=h.split();k.matrix=h;k.rotationRads=g.rotation;k.rotationCenterX=0;k.rotationCenterY=0;k.scalingX=g.scaleX;k.scalingY=g.scaleY;k.scalingCenterX=0;k.scalingCenterY=0;k.translationX=g.translateX;k.translationY=g.translateY}for(b in i){e=i[b];if(typeof e==="undefined"){continue}if(b in d){b=d[b]}if(b in f){e=f[b].call(this,e);if(typeof e!=="undefined"){k[b]=e}}else{if(j){k[b]=e}}}return k},setBypassingNormalization:function(a,c,b){return c.pushDown(a,b)},set:function(a,c,b){b=this.normalize(b);return this.setBypassingNormalization(a,c,b)}});Ext.define("Ext.draw.Matrix",{isMatrix:true,statics:{createAffineMatrixFromTwoPair:function(h,t,g,s,k,o,i,j){var v=g-h,u=s-t,e=i-k,q=j-o,d=1/(v*v+u*u),p=v*e+u*q,n=e*u-v*q,m=-p*h-n*t,l=n*h-p*t;return new this(p*d,-n*d,n*d,p*d,m*d+k,l*d+o)},createPanZoomFromTwoPair:function(q,e,p,c,h,s,n,g){if(arguments.length===2){return this.createPanZoomFromTwoPair.apply(this,q.concat(e))}var k=p-q,j=c-e,d=(q+p)*0.5,b=(e+c)*0.5,o=n-h,a=g-s,f=(h+n)*0.5,l=(s+g)*0.5,m=k*k+j*j,i=o*o+a*a,t=Math.sqrt(i/m);return new this(t,0,0,t,f-t*d,l-t*b)},fly:(function(){var a=null,b=function(c){a.elements=c;return a};return function(c){if(!a){a=new Ext.draw.Matrix()}a.elements=c;Ext.draw.Matrix.fly=b;return a}})(),create:function(a){if(a instanceof this){return a}return new this(a)}},constructor:function(e,d,a,f,c,b){if(e&&e.length===6){this.elements=e.slice()}else{if(e!==undefined){this.elements=[e,d,a,f,c,b]}else{this.elements=[1,0,0,1,0,0]}}},prepend:function(a,l,h,g,m,k){var b=this.elements,d=b[0],j=b[1],e=b[2],c=b[3],i=b[4],f=b[5];b[0]=a*d+h*j;b[1]=l*d+g*j;b[2]=a*e+h*c;b[3]=l*e+g*c;b[4]=a*i+h*f+m;b[5]=l*i+g*f+k;return this},prependMatrix:function(a){return this.prepend.apply(this,a.elements)},append:function(a,l,h,g,m,k){var b=this.elements,d=b[0],j=b[1],e=b[2],c=b[3],i=b[4],f=b[5];b[0]=a*d+l*e;b[1]=a*j+l*c;b[2]=h*d+g*e;b[3]=h*j+g*c;b[4]=m*d+k*e+i;b[5]=m*j+k*c+f;return this},appendMatrix:function(a){return this.append.apply(this,a.elements)},set:function(f,e,a,g,c,b){var d=this.elements;d[0]=f;d[1]=e;d[2]=a;d[3]=g;d[4]=c;d[5]=b;return this},inverse:function(i){var g=this.elements,o=g[0],m=g[1],l=g[2],k=g[3],j=g[4],h=g[5],n=1/(o*k-m*l);o*=n;m*=n;l*=n;k*=n;if(i){i.set(k,-m,-l,o,l*h-k*j,m*j-o*h);return i}else{return new Ext.draw.Matrix(k,-m,-l,o,l*h-k*j,m*j-o*h)}},translate:function(a,c,b){if(b){return this.prepend(1,0,0,1,a,c)}else{return this.append(1,0,0,1,a,c)}},scale:function(f,e,c,a,b){var d=this;if(e==null){e=f}if(c===undefined){c=0}if(a===undefined){a=0}if(b){return d.prepend(f,0,0,e,c-c*f,a-a*e)}else{return d.append(f,0,0,e,c-c*f,a-a*e)}},rotate:function(g,e,c,b){var d=this,f=Math.cos(g),a=Math.sin(g);e=e||0;c=c||0;if(b){return d.prepend(f,a,-a,f,e-f*e+c*a,c-f*c-e*a)}else{return d.append(f,a,-a,f,e-f*e+c*a,c-f*c-e*a)}},rotateFromVector:function(a,h,c){var e=this,g=Math.sqrt(a*a+h*h),f=a/g,b=h/g;if(c){return e.prepend(f,b,-b,f,0,0)}else{return e.append(f,b,-b,f,0,0)}},clone:function(){return new Ext.draw.Matrix(this.elements)},flipX:function(){return this.append(-1,0,0,1,0,0)},flipY:function(){return this.append(1,0,0,-1,0,0)},skewX:function(a){return this.append(1,0,Math.tan(a),1,0,0)},skewY:function(a){return this.append(1,Math.tan(a),0,1,0,0)},shearX:function(a){return this.append(1,0,a,1,0,0)},shearY:function(a){return this.append(1,a,0,1,0,0)},reset:function(){return this.set(1,0,0,1,0,0)},precisionCompensate:function(j,g){var c=this.elements,f=c[0],e=c[1],i=c[2],h=c[3],d=c[4],b=c[5],a=e*i-f*h;g.b=j*e/f;g.c=j*i/h;g.d=j;g.xx=f/j;g.yy=h/j;g.dx=(b*f*i-d*f*h)/a/j;g.dy=(d*e*h-b*f*h)/a/j},precisionCompensateRect:function(j,g){var b=this.elements,f=b[0],e=b[1],i=b[2],h=b[3],c=b[4],a=b[5],d=i/f;g.b=j*e/f;g.c=j*d;g.d=j*h/f;g.xx=f/j;g.yy=f/j;g.dx=(a*i-c*h)/(e*d-h)/j;g.dy=-(a*f-c*e)/(e*d-h)/j},x:function(a,c){var b=this.elements;return a*b[0]+c*b[2]+b[4]},y:function(a,c){var b=this.elements;return a*b[1]+c*b[3]+b[5]},get:function(b,a){return +this.elements[b+a*2].toFixed(4)},transformPoint:function(b){var c=this.elements,a,d;if(b.isPoint){a=b.x;d=b.y}else{a=b[0];d=b[1]}return[a*c[0]+d*c[2]+c[4],a*c[1]+d*c[3]+c[5]]},transformBBox:function(q,i,j){var b=this.elements,d=q.x,r=q.y,g=q.width*0.5,o=q.height*0.5,a=b[0],s=b[1],n=b[2],k=b[3],e=d+g,c=r+o,p,f,m;if(i){g-=i;o-=i;m=[Math.sqrt(b[0]*b[0]+b[2]*b[2]),Math.sqrt(b[1]*b[1]+b[3]*b[3])];p=Math.abs(g*a)+Math.abs(o*n)+Math.abs(m[0]*i);f=Math.abs(g*s)+Math.abs(o*k)+Math.abs(m[1]*i)}else{p=Math.abs(g*a)+Math.abs(o*n);f=Math.abs(g*s)+Math.abs(o*k)}if(!j){j={}}j.x=e*a+c*n+b[4]-p;j.y=e*s+c*k+b[5]-f;j.width=p+p;j.height=f+f;return j},transformList:function(e){var b=this.elements,a=b[0],h=b[2],l=b[4],k=b[1],g=b[3],j=b[5],f=e.length,c,d;for(d=0;d<f;d++){c=e[d];e[d]=[c[0]*a+c[1]*h+l,c[0]*k+c[1]*g+j]}return e},isIdentity:function(){var a=this.elements;return a[0]===1&&a[1]===0&&a[2]===0&&a[3]===1&&a[4]===0&&a[5]===0},isEqual:function(a){var c=a&&a.isMatrix?a.elements:a,b=this.elements;return b[0]===c[0]&&b[1]===c[1]&&b[2]===c[2]&&b[3]===c[3]&&b[4]===c[4]&&b[5]===c[5]},equals:function(a){return this.isEqual(a)},toArray:function(){var a=this.elements;return[a[0],a[2],a[4],a[1],a[3],a[5]]},toVerticalArray:function(){return this.elements.slice()},toString:function(){var a=this;return[a.get(0,0),a.get(0,1),a.get(1,0),a.get(1,1),a.get(2,0),a.get(2,1)].join(",")},toContext:function(a){a.transform.apply(a,this.elements);return this},toSvg:function(){var a=this.elements;return"matrix("+a[0].toFixed(9)+","+a[1].toFixed(9)+","+a[2].toFixed(9)+","+a[3].toFixed(9)+","+a[4].toFixed(9)+","+a[5].toFixed(9)+")"},getScaleX:function(){var a=this.elements;return Math.sqrt(a[0]*a[0]+a[2]*a[2])},getScaleY:function(){var a=this.elements;return Math.sqrt(a[1]*a[1]+a[3]*a[3])},getXX:function(){return this.elements[0]},getXY:function(){return this.elements[1]},getYX:function(){return this.elements[2]},getYY:function(){return this.elements[3]},getDX:function(){return this.elements[4]},getDY:function(){return this.elements[5]},split:function(){var b=this.elements,d=b[0],c=b[1],e=b[3],a={translateX:b[4],translateY:b[5]};a.rotate=a.rotation=Math.atan2(c,d);a.scaleX=d/Math.cos(a.rotate);a.scaleY=e/d*a.scaleX;return a}},function(){function b(e,c,d){e[c]={get:function(){return this.elements[d]},set:function(f){this.elements[d]=f}}}if(Object.defineProperties){var a={};b(a,"a",0);b(a,"b",1);b(a,"c",2);b(a,"d",3);b(a,"e",4);b(a,"f",5);Object.defineProperties(this.prototype,a)}this.prototype.multiply=this.prototype.appendMatrix});Ext.define("Ext.draw.modifier.Modifier",{mixins:{observable:Ext.mixin.Observable},config:{previous:null,next:null,sprite:null},constructor:function(a){this.mixins.observable.constructor.call(this,a)},updateNext:function(a){if(a){a.setPrevious(this)}},updatePrevious:function(a){if(a){a.setNext(this)}},prepareAttributes:function(a){if(this._previous){this._previous.prepareAttributes(a)}},popUp:function(a,b){if(this._next){this._next.popUp(a,b)}else{Ext.apply(a,b)}},pushDown:function(a,c){if(this._previous){return this._previous.pushDown(a,c)}else{for(var b in c){if(c[b]===a[b]){delete c[b]}}return c}}});Ext.define("Ext.draw.modifier.Target",{extend:Ext.draw.modifier.Modifier,alias:"modifier.target",statics:{uniqueId:0},prepareAttributes:function(a){var b=this.getPrevious();if(b){b.prepareAttributes(a)}a.attributeId="attribute-"+Ext.draw.modifier.Target.uniqueId++;if(!a.hasOwnProperty("canvasAttributes")){a.bbox={plain:{dirty:true},transform:{dirty:true}};a.dirty=true;a.pendingUpdaters={};a.canvasAttributes={};a.matrix=new Ext.draw.Matrix();a.inverseMatrix=new Ext.draw.Matrix()}},applyChanges:function(f,k){Ext.apply(f,k);var l=this.getSprite(),o=f.pendingUpdaters,h=l.self.def.getTriggers(),p,a,m,b,e,n,d,c,g;for(b in k){e=true;if((p=h[b])){l.scheduleUpdaters(f,p,[b])}if(f.template&&k.removeFromInstance&&k.removeFromInstance[b]){delete f[b]}}if(!e){return}if(o.canvas){n=o.canvas;delete o.canvas;for(d=0,g=n.length;d<g;d++){b=n[d];f.canvasAttributes[b]=f[b]}}if(f.hasOwnProperty("children")){a=f.children;for(d=0,g=a.length;d<g;d++){m=a[d];Ext.apply(m.pendingUpdaters,o);if(n){for(c=0;c<n.length;c++){b=n[c];m.canvasAttributes[b]=m[b]}}l.callUpdaters(m)}}l.setDirty(true);l.callUpdaters(f)},popUp:function(a,b){this.applyChanges(a,b)},pushDown:function(a,b){var c=this.getPrevious();if(c){b=c.pushDown(a,b)}this.applyChanges(a,b);return b}});Ext.define("Ext.draw.TimingFunctions",function(){var g=Math.pow,j=Math.sin,m=Math.cos,l=Math.sqrt,e=Math.PI,b=["quad","cube","quart","quint"],c={pow:function(o,i){return g(o,i||6)},expo:function(i){return g(2,8*(i-1))},circ:function(i){return 1-l(1-i*i)},sine:function(i){return 1-j((1-i)*e/2)},back:function(i,o){o=o||1.616;return i*i*((o+1)*i-o)},bounce:function(q){for(var o=0,i=1;1;o+=i,i/=2){if(q>=(7-4*o)/11){return i*i-g((11-6*o-11*q)/4,2)}}},elastic:function(o,i){return g(2,10*--o)*m(20*o*e*(i||1)/3)}},k={},a,f,d;function h(i){return function(o){return g(o,i)}}function n(i,o){k[i+"In"]=function(p){return o(p)};k[i+"Out"]=function(p){return 1-o(1-p)};k[i+"InOut"]=function(p){return(p<=0.5)?o(2*p)/2:(2-o(2*(1-p)))/2}}for(d=0,f=b.length;d<f;++d){c[b[d]]=h(d+2)}for(a in c){n(a,c[a])}k.linear=Ext.identityFn;k.easeIn=k.quadIn;k.easeOut=k.quadOut;k.easeInOut=k.quadInOut;return{singleton:true,easingMap:k}},function(a){Ext.apply(a,a.easingMap)});Ext.define("Ext.draw.Animator",{singleton:true,frameCallbacks:{},frameCallbackId:0,scheduled:0,frameStartTimeOffset:Ext.now(),animations:[],running:false,animationTime:function(){return Ext.AnimationQueue.frameStartTime-this.frameStartTimeOffset},add:function(b){var a=this;if(!a.contains(b)){a.animations.push(b);a.ignite();if("fireEvent" in b){b.fireEvent("animationstart",b)}}},remove:function(d){var c=this,e=c.animations,b=0,a=e.length;for(;b<a;++b){if(e[b]===d){e.splice(b,1);if("fireEvent" in d){d.fireEvent("animationend",d)}return}}},contains:function(a){return Ext.Array.indexOf(this.animations,a)>-1},empty:function(){return this.animations.length===0},step:function(d){var c=this,f=c.animations,e,a=0,b=f.length;for(;a<b;a++){e=f[a];e.step(d);if(!e.animating){f.splice(a,1);a--;b--;if(e.fireEvent){e.fireEvent("animationend",e)}}}},schedule:function(c,a){a=a||this;var b="frameCallback"+(this.frameCallbackId++);if(Ext.isString(c)){c=a[c]}Ext.draw.Animator.frameCallbacks[b]={fn:c,scope:a,once:true};this.scheduled++;Ext.draw.Animator.ignite();return b},scheduleIf:function(e,b){b=b||this;var c=Ext.draw.Animator.frameCallbacks,a,d;if(Ext.isString(e)){e=b[e]}for(d in c){a=c[d];if(a.once&&a.fn===e&&a.scope===b){return null}}return this.schedule(e,b)},cancel:function(a){if(Ext.draw.Animator.frameCallbacks[a]&&Ext.draw.Animator.frameCallbacks[a].once){this.scheduled--;delete Ext.draw.Animator.frameCallbacks[a]}},addFrameCallback:function(c,a){a=a||this;if(Ext.isString(c)){c=a[c]}var b="frameCallback"+(this.frameCallbackId++);Ext.draw.Animator.frameCallbacks[b]={fn:c,scope:a};return b},removeFrameCallback:function(a){delete Ext.draw.Animator.frameCallbacks[a]},fireFrameCallbacks:function(){var c=this.frameCallbacks,d,b,a;for(d in c){a=c[d];b=a.fn;if(Ext.isString(b)){b=a.scope[b]}b.call(a.scope);if(c[d]&&a.once){this.scheduled--;delete c[d]}}},handleFrame:function(){this.step(this.animationTime());this.fireFrameCallbacks();if(!this.scheduled&&this.empty()){Ext.AnimationQueue.stop(this.handleFrame,this);this.running=false;Ext.draw.Draw.endUpdateIOS()}},ignite:function(){if(!this.running){this.running=true;Ext.AnimationQueue.start(this.handleFrame,this);Ext.draw.Draw.beginUpdateIOS()}}});Ext.define("Ext.draw.modifier.Animation",{extend:Ext.draw.modifier.Modifier,alias:"modifier.animation",config:{easing:Ext.identityFn,duration:0,customEasings:{},customDurations:{},customDuration:null},constructor:function(a){var b=this;b.anyAnimation=b.anySpecialAnimations=false;b.animating=0;b.animatingPool=[];b.callParent([a])},prepareAttributes:function(a){if(!a.hasOwnProperty("timers")){a.animating=false;a.timers={};a.animationOriginal=Ext.Object.chain(a);a.animationOriginal.prototype=a}if(this._previous){this._previous.prepareAttributes(a.animationOriginal)}},updateSprite:function(a){this.setConfig(a.config.fx)},updateDuration:function(a){this.anyAnimation=a>0},applyEasing:function(a){if(typeof a==="string"){a=Ext.draw.TimingFunctions.easingMap[a]}return a},applyCustomEasings:function(a,e){e=e||{};var g,d,b,h,c,f;for(d in a){g=true;h=a[d];b=d.split(",");if(typeof h==="string"){h=Ext.draw.TimingFunctions.easingMap[h]}for(c=0,f=b.length;c<f;c++){e[b[c]]=h}}if(g){this.anySpecialAnimations=g}return e},setEasingOn:function(a,e){a=Ext.Array.from(a).slice();var c={},d=a.length,b=0;for(;b<d;b++){c[a[b]]=e}this.setCustomEasings(c)},clearEasingOn:function(a){a=Ext.Array.from(a,true);var b=0,c=a.length;for(;b<c;b++){delete this._customEasings[a[b]]}},applyCustomDurations:function(g,h){h=h||{};var e,c,f,a,b,d;for(c in g){e=true;f=g[c];a=c.split(",");for(b=0,d=a.length;b<d;b++){h[a[b]]=f}}if(e){this.anySpecialAnimations=e}return h},applyCustomDuration:function(a,b){if(a){this.getCustomDurations();this.setCustomDurations(a)}},setDurationOn:function(b,e){b=Ext.Array.from(b).slice();var a={},c=0,d=b.length;for(;c<d;c++){a[b[c]]=e}this.setCustomDurations(a)},clearDurationOn:function(a){a=Ext.Array.from(a,true);var b=0,c=a.length;for(;b<c;b++){delete this._customDurations[a[b]]}},setAnimating:function(a,b){var e=this,d=e.animatingPool;if(a.animating!==b){a.animating=b;if(b){d.push(a);if(e.animating===0){Ext.draw.Animator.add(e)}e.animating++}else{for(var c=d.length;c--;){if(d[c]===a){d.splice(c,1)}}e.animating=d.length}}},setAttrs:function(r,t){var s=this,m=r.timers,h=s._sprite.self.def._animationProcessors,f=s._easing,e=s._duration,j=s._customDurations,i=s._customEasings,g=s.anySpecialAnimations,n=s.anyAnimation||g,o=r.animationOriginal,d=false,k,u,l,p,c,q,a;if(!n){for(u in t){if(r[u]===t[u]){delete t[u]}else{r[u]=t[u]}delete o[u];delete m[u]}return t}else{for(u in t){l=t[u];p=r[u];if(l!==p&&p!==undefined&&p!==null&&(c=h[u])){q=f;a=e;if(g){if(u in i){q=i[u]}if(u in j){a=j[u]}}if(p&&p.isGradient||l&&l.isGradient){a=0}if(a){if(!m[u]){m[u]={}}k=m[u];k.start=0;k.easing=q;k.duration=a;k.compute=c.compute;k.serve=c.serve||Ext.identityFn;k.remove=t.removeFromInstance&&t.removeFromInstance[u];if(c.parseInitial){var b=c.parseInitial(p,l);k.source=b[0];k.target=b[1]}else{if(c.parse){k.source=c.parse(p);k.target=c.parse(l)}else{k.source=p;k.target=l}}o[u]=l;delete t[u];d=true;continue}else{delete o[u]}}else{delete o[u]}delete m[u]}}if(d&&!r.animating){s.setAnimating(r,true)}return t},updateAttributes:function(g){if(!g.animating){return{}}var h={},e=false,d=g.timers,f=g.animationOriginal,c=Ext.draw.Animator.animationTime(),a,b,i;if(g.lastUpdate===c){return null}for(a in d){b=d[a];if(!b.start){b.start=c;i=0}else{i=(c-b.start)/b.duration}if(i>=1){h[a]=f[a];delete f[a];if(d[a].remove){h.removeFromInstance=h.removeFromInstance||{};h.removeFromInstance[a]=true}delete d[a]}else{h[a]=b.serve(b.compute(b.source,b.target,b.easing(i),g[a]));e=true}}g.lastUpdate=c;this.setAnimating(g,e);return h},pushDown:function(a,b){b=this.callParent([a.animationOriginal,b]);return this.setAttrs(a,b)},popUp:function(a,b){a=a.prototype;b=this.setAttrs(a,b);if(this._next){return this._next.popUp(a,b)}else{return Ext.apply(a,b)}},step:function(g){var f=this,c=f.animatingPool.slice(),e=c.length,b=0,a,d;for(;b<e;b++){a=c[b];d=f.updateAttributes(a);if(d&&f._next){f._next.popUp(a,d)}}},stop:function(){this.step();var d=this,b=d.animatingPool,a,c;for(a=0,c=b.length;a<c;a++){b[a].animating=false}d.animatingPool.length=0;d.animating=0;Ext.draw.Animator.remove(d)},destroy:function(){this.animatingPool.length=0;this.animating=0;this.callParent()}});Ext.define("Ext.draw.modifier.Highlight",{extend:Ext.draw.modifier.Modifier,alias:"modifier.highlight",config:{enabled:false,highlightStyle:null},preFx:true,applyHighlightStyle:function(b,a){a=a||{};if(this.getSprite()){Ext.apply(a,this.getSprite().self.def.normalize(b))}else{Ext.apply(a,b)}return a},prepareAttributes:function(a){if(!a.hasOwnProperty("highlightOriginal")){a.highlighted=false;a.highlightOriginal=Ext.Object.chain(a);a.highlightOriginal.prototype=a;a.highlightOriginal.removeFromInstance={}}if(this._previous){this._previous.prepareAttributes(a.highlightOriginal)}},updateSprite:function(b,a){if(b){if(this.getHighlightStyle()){this._highlightStyle=b.self.def.normalize(this.getHighlightStyle())}this.setHighlightStyle(b.config.highlight)}b.self.def.setConfig({defaults:{highlighted:false},processors:{highlighted:"bool"}});this.setSprite(b)},filterChanges:function(a,d){var e=this,f=a.highlightOriginal,c=e.getHighlightStyle(),b;if(a.highlighted){for(b in d){if(c.hasOwnProperty(b)){f[b]=d[b];delete d[b]}}}for(b in d){if(b!=="highlighted"&&f[b]===d[b]){delete d[b]}}return d},pushDown:function(e,g){var f=this.getHighlightStyle(),c=e.highlightOriginal,i=c.removeFromInstance,d,a,h,b;if(g.hasOwnProperty("highlighted")){d=g.highlighted;delete g.highlighted;if(this._previous){g=this._previous.pushDown(c,g)}g=this.filterChanges(e,g);if(d!==e.highlighted){if(d){for(a in f){if(a in g){c[a]=g[a]}else{h=e.template&&e.template.ownAttr;if(h&&!e.prototype.hasOwnProperty(a)){i[a]=true;c[a]=h.animationOriginal[a]}else{b=c.timers[a];if(b&&b.remove){i[a]=true}c[a]=e[a]}}if(c[a]!==f[a]){g[a]=f[a]}}}else{for(a in f){if(!(a in g)){g[a]=c[a]}delete c[a]}g.removeFromInstance=g.removeFromInstance||{};Ext.apply(g.removeFromInstance,i);c.removeFromInstance={}}g.highlighted=d}}else{if(this._previous){g=this._previous.pushDown(c,g)}g=this.filterChanges(e,g)}return g},popUp:function(a,b){b=this.filterChanges(a,b);Ext.draw.modifier.Modifier.prototype.popUp.call(this,a,b)}});Ext.define("Ext.draw.sprite.Sprite",{alias:"sprite.sprite",mixins:{observable:Ext.mixin.Observable},isSprite:true,statics:{defaultHitTestOptions:{fill:true,stroke:true}},inheritableStatics:{def:{processors:{strokeStyle:"color",fillStyle:"color",strokeOpacity:"limited01",fillOpacity:"limited01",lineWidth:"number",lineCap:"enums(butt,round,square)",lineJoin:"enums(round,bevel,miter)",lineDash:"data",lineDashOffset:"number",miterLimit:"number",shadowColor:"color",shadowOffsetX:"number",shadowOffsetY:"number",shadowBlur:"number",globalAlpha:"limited01",globalCompositeOperation:"enums(source-over,destination-over,source-in,destination-in,source-out,destination-out,source-atop,destination-atop,lighter,xor,copy)",hidden:"bool",transformFillStroke:"bool",zIndex:"number",translationX:"number",translationY:"number",rotationRads:"number",rotationCenterX:"number",rotationCenterY:"number",scalingX:"number",scalingY:"number",scalingCenterX:"number",scalingCenterY:"number",constrainGradients:"bool"},aliases:{stroke:"strokeStyle",fill:"fillStyle",color:"fillStyle","stroke-width":"lineWidth","stroke-linecap":"lineCap","stroke-linejoin":"lineJoin","stroke-miterlimit":"miterLimit","text-anchor":"textAlign",opacity:"globalAlpha",translateX:"translationX",translateY:"translationY",rotateRads:"rotationRads",rotateCenterX:"rotationCenterX",rotateCenterY:"rotationCenterY",scaleX:"scalingX",scaleY:"scalingY",scaleCenterX:"scalingCenterX",scaleCenterY:"scalingCenterY"},defaults:{hidden:false,zIndex:0,strokeStyle:"none",fillStyle:"none",lineWidth:1,lineDash:[],lineDashOffset:0,lineCap:"butt",lineJoin:"miter",miterLimit:10,shadowColor:"none",shadowOffsetX:0,shadowOffsetY:0,shadowBlur:0,globalAlpha:1,strokeOpacity:1,fillOpacity:1,transformFillStroke:false,translationX:0,translationY:0,rotationRads:0,rotationCenterX:null,rotationCenterY:null,scalingX:1,scalingY:1,scalingCenterX:null,scalingCenterY:null,constrainGradients:false},triggers:{zIndex:"zIndex",globalAlpha:"canvas",globalCompositeOperation:"canvas",transformFillStroke:"canvas",strokeStyle:"canvas",fillStyle:"canvas",strokeOpacity:"canvas",fillOpacity:"canvas",lineWidth:"canvas",lineCap:"canvas",lineJoin:"canvas",lineDash:"canvas",lineDashOffset:"canvas",miterLimit:"canvas",shadowColor:"canvas",shadowOffsetX:"canvas",shadowOffsetY:"canvas",shadowBlur:"canvas",translationX:"transform",translationY:"transform",rotationRads:"transform",rotationCenterX:"transform",rotationCenterY:"transform",scalingX:"transform",scalingY:"transform",scalingCenterX:"transform",scalingCenterY:"transform",constrainGradients:"canvas"},updaters:{bbox:"bboxUpdater",zIndex:function(a){a.dirtyZIndex=true},transform:function(a){a.dirtyTransform=true;a.bbox.transform.dirty=true}}}},config:{parent:null,surface:null},onClassExtended:function(d,c){var b=d.superclass.self.def.initialConfig,e=c.inheritableStatics&&c.inheritableStatics.def,a;if(e){a=Ext.Object.merge({},b,e);d.def=new Ext.draw.sprite.AttributeDefinition(a);delete c.inheritableStatics.def}else{d.def=new Ext.draw.sprite.AttributeDefinition(b)}d.def.spriteClass=d},constructor:function(b){var d=this,c=d.self.def,e=c.getDefaults(),a;b=Ext.isObject(b)?b:{};d.id=b.id||Ext.id(null,"ext-sprite-");d.attr={};d.mixins.observable.constructor.apply(d,arguments);a=Ext.Array.from(b.modifiers,true);d.prepareModifiers(a);d.initializeAttributes();d.setAttributes(e,true);d.setAttributes(b)},getDirty:function(){return this.attr.dirty},setDirty:function(b){this.attr.dirty=b;if(b){var a=this.getParent();if(a){a.setDirty(true)}}},addModifier:function(a,b){var c=this;if(!(a instanceof Ext.draw.modifier.Modifier)){a=Ext.factory(a,null,null,"modifier")}a.setSprite(c);if(a.preFx||a.config&&a.config.preFx){if(c.fx.getPrevious()){c.fx.getPrevious().setNext(a)}a.setNext(c.fx)}else{c.topModifier.getPrevious().setNext(a);a.setNext(c.topModifier)}if(b){c.initializeAttributes()}return a},prepareModifiers:function(d){var c=this,a,b;c.topModifier=new Ext.draw.modifier.Target({sprite:c});c.fx=new Ext.draw.modifier.Animation({sprite:c});c.fx.setNext(c.topModifier);for(a=0,b=d.length;a<b;a++){c.addModifier(d[a],false)}},getAnimation:function(){return this.fx},setAnimation:function(a){this.fx.setConfig(a)},initializeAttributes:function(){this.topModifier.prepareAttributes(this.attr)},callUpdaters:function(d){var e=this,h=d.pendingUpdaters,i=e.self.def.getUpdaters(),c=false,a=false,b,g,f;e.callUpdaters=Ext.emptyFn;do{c=false;for(g in h){c=true;b=h[g];delete h[g];f=i[g];if(typeof f==="string"){f=e[f]}if(f){f.call(e,d,b)}}a=a||c}while(c);delete e.callUpdaters;if(a){e.setDirty(true)}},scheduleUpdaters:function(a,e,c){var f;if(c){for(var b=0,d=e.length;b<d;b++){f=e[b];this.scheduleUpdater(a,f,c)}}else{for(f in e){c=e[f];this.scheduleUpdater(a,f,c)}}},scheduleUpdater:function(a,c,b){b=b||[];var d=a.pendingUpdaters;if(c in d){if(b.length){d[c]=Ext.Array.merge(d[c],b)}}else{d[c]=b}},setAttributes:function(d,g,c){var a=this.attr,b,e,f;if(g){if(c){this.topModifier.pushDown(a,d)}else{f={};for(b in d){e=d[b];if(e!==a[b]){f[b]=e}}this.topModifier.pushDown(a,f)}}else{this.topModifier.pushDown(a,this.self.def.normalize(d))}},setAttributesBypassingNormalization:function(b,a){return this.setAttributes(b,true,a)},bboxUpdater:function(b){var c=b.rotationRads!==0,a=b.scalingX!==1||b.scalingY!==1,d=b.rotationCenterX===null||b.rotationCenterY===null,e=b.scalingCenterX===null||b.scalingCenterY===null;b.bbox.plain.dirty=true;b.bbox.transform.dirty=true;if(c&&d||a&&e){this.scheduleUpdater(b,"transform")}},getBBox:function(d){var e=this,a=e.attr,f=a.bbox,c=f.plain,b=f.transform;if(c.dirty){e.updatePlainBBox(c);c.dirty=false}if(!d){e.applyTransformations();if(b.dirty){e.updateTransformedBBox(b,c);b.dirty=false}return b}return c},updatePlainBBox:Ext.emptyFn,updateTransformedBBox:function(a,b){this.attr.matrix.transformBBox(b,0,a)},getBBoxCenter:function(a){var b=this.getBBox(a);if(b){return[b.x+b.width*0.5,b.y+b.height*0.5]}else{return[0,0]}},hide:function(){this.attr.hidden=true;this.setDirty(true);return this},show:function(){this.attr.hidden=false;this.setDirty(true);return this},useAttributes:function(i,f){this.applyTransformations();var d=this.attr,h=d.canvasAttributes,e=h.strokeStyle,g=h.fillStyle,b=h.lineDash,c=h.lineDashOffset,a;if(e){if(e.isGradient){i.strokeStyle="black";i.strokeGradient=e}else{i.strokeGradient=false}}if(g){if(g.isGradient){i.fillStyle="black";i.fillGradient=g}else{i.fillGradient=false}}if(b){i.setLineDash(b)}if(Ext.isNumber(c+i.lineDashOffset)){i.lineDashOffset=c}for(a in h){if(h[a]!==undefined&&h[a]!==i[a]){i[a]=h[a]}}this.setGradientBBox(i,f)},setGradientBBox:function(b,c){var a=this.attr;if(a.constrainGradients){b.setGradientBBox({x:c[0],y:c[1],width:c[2],height:c[3]})}else{b.setGradientBBox(this.getBBox(a.transformFillStroke))}},applyTransformations:function(b){if(!b&&!this.attr.dirtyTransform){return}var r=this,k=r.attr,p=r.getBBoxCenter(true),g=p[0],f=p[1],q=k.translationX,o=k.translationY,j=k.scalingX,i=k.scalingY===null?k.scalingX:k.scalingY,m=k.scalingCenterX===null?g:k.scalingCenterX,l=k.scalingCenterY===null?f:k.scalingCenterY,s=k.rotationRads,e=k.rotationCenterX===null?g:k.rotationCenterX,d=k.rotationCenterY===null?f:k.rotationCenterY,c=Math.cos(s),a=Math.sin(s),n,h;if(j===1&&i===1){m=0;l=0}if(s===0){e=0;d=0}n=m*(1-j)-e;h=l*(1-i)-d;k.matrix.elements=[c*j,a*j,-a*i,c*i,c*n-a*h+e+q,a*n+c*h+d+o];k.matrix.inverse(k.inverseMatrix);k.dirtyTransform=false;k.bbox.transform.dirty=true},transform:function(b,c){var a=this.attr,e=a.matrix,d;if(b&&b.isMatrix){d=b.elements}else{d=b}e.prepend.apply(e,d.slice());e.inverse(a.inverseMatrix);if(c){this.updateTransformAttributes()}a.dirtyTransform=false;a.bbox.transform.dirty=true;this.setDirty(true);return this},updateTransformAttributes:function(){var a=this.attr,b=a.matrix.split();a.rotationRads=b.rotate;a.rotationCenterX=0;a.rotationCenterY=0;a.scalingX=b.scaleX;a.scalingY=b.scaleY;a.scalingCenterX=0;a.scalingCenterY=0;a.translationX=b.translateX;a.translationY=b.translateY},resetTransform:function(b){var a=this.attr;a.matrix.reset();a.inverseMatrix.reset();if(!b){this.updateTransformAttributes()}a.dirtyTransform=false;a.bbox.transform.dirty=true;this.setDirty(true);return this},setTransform:function(a,b){this.resetTransform(true);this.transform.call(this,a,b);return this},preRender:Ext.emptyFn,render:Ext.emptyFn,hitTest:function(b,c){if(this.isVisible()){var a=b[0],f=b[1],e=this.getBBox(),d=e&&a>=e.x&&a<=(e.x+e.width)&&f>=e.y&&f<=(e.y+e.height);if(d){return{sprite:this}}}return null},isVisible:function(){var e=this.attr,f=this.getParent(),g=f&&(f.isSurface||f.isVisible()),d=g&&!e.hidden&&e.globalAlpha,b=Ext.draw.Color.NONE,a=Ext.draw.Color.RGBA_NONE,c=e.fillOpacity&&e.fillStyle!==b&&e.fillStyle!==a,i=e.strokeOpacity&&e.strokeStyle!==b&&e.strokeStyle!==a,h=d&&(c||i);return !!h},repaint:function(){var a=this.getSurface();if(a){a.renderFrame()}},remove:function(){var a=this.getSurface();if(a&&a.isSurface){return a.remove(this)}return null},destroy:function(){var b=this,a=b.topModifier,c;while(a){c=a;a=a.getPrevious();c.destroy()}delete b.attr;b.remove();if(b.fireEvent("beforedestroy",b)!==false){b.fireEvent("destroy",b)}b.callParent()}},function(){this.def=new Ext.draw.sprite.AttributeDefinition(this.def);this.def.spriteClass=this});Ext.define("Ext.draw.Path",{statics:{pathRe:/,?([achlmqrstvxz]),?/gi,pathRe2:/-/gi,pathSplitRe:/\s|,/g},svgString:"",constructor:function(a){var b=this;b.commands=[];b.params=[];b.cursor=null;b.startX=0;b.startY=0;if(a){b.fromSvgString(a)}},clear:function(){var a=this;a.params.length=0;a.commands.length=0;a.cursor=null;a.startX=0;a.startY=0;a.dirt()},dirt:function(){this.svgString=""},moveTo:function(a,c){var b=this;if(!b.cursor){b.cursor=[a,c]}b.params.push(a,c);b.commands.push("M");b.startX=a;b.startY=c;b.cursor[0]=a;b.cursor[1]=c;b.dirt()},lineTo:function(a,c){var b=this;if(!b.cursor){b.cursor=[a,c];b.params.push(a,c);b.commands.push("M")}else{b.params.push(a,c);b.commands.push("L")}b.cursor[0]=a;b.cursor[1]=c;b.dirt()},bezierCurveTo:function(c,e,b,d,a,g){var f=this;if(!f.cursor){f.moveTo(c,e)}f.params.push(c,e,b,d,a,g);f.commands.push("C");f.cursor[0]=a;f.cursor[1]=g;f.dirt()},quadraticCurveTo:function(b,e,a,d){var c=this;if(!c.cursor){c.moveTo(b,e)}c.bezierCurveTo((2*b+c.cursor[0])/3,(2*e+c.cursor[1])/3,(2*b+a)/3,(2*e+d)/3,a,d)},closePath:function(){var a=this;if(a.cursor){a.cursor=null;a.commands.push("Z");a.dirt()}},arcTo:function(A,f,z,d,j,i,v){var E=this;if(i===undefined){i=j}if(v===undefined){v=0}if(!E.cursor){E.moveTo(A,f);return}if(j===0||i===0){E.lineTo(A,f);return}z-=A;d-=f;var B=E.cursor[0]-A,g=E.cursor[1]-f,C=z*g-d*B,b,a,l,r,k,q,x=Math.sqrt(B*B+g*g),u=Math.sqrt(z*z+d*d),t,e,c;if(C===0){E.lineTo(A,f);return}if(i!==j){b=Math.cos(v);a=Math.sin(v);l=b/j;r=a/i;k=-a/j;q=b/i;var D=l*B+r*g;g=k*B+q*g;B=D;D=l*z+r*d;d=k*z+q*d;z=D}else{B/=j;g/=i;z/=j;d/=i}e=B*u+z*x;c=g*u+d*x;t=1/(Math.sin(Math.asin(Math.abs(C)/(x*u))*0.5)*Math.sqrt(e*e+c*c));e*=t;c*=t;var o=(e*B+c*g)/(B*B+g*g),m=(e*z+c*d)/(z*z+d*d);var n=B*o-e,p=g*o-c,h=z*m-e,y=d*m-c,w=Math.atan2(p,n),s=Math.atan2(y,h);if(C>0){if(s<w){s+=Math.PI*2}}else{if(w<s){w+=Math.PI*2}}if(i!==j){e=b*e*j-a*c*i+A;c=a*c*i+b*c*i+f;E.lineTo(b*j*n-a*i*p+e,a*j*n+b*i*p+c);E.ellipse(e,c,j,i,v,w,s,C<0)}else{e=e*j+A;c=c*i+f;E.lineTo(j*n+e,i*p+c);E.ellipse(e,c,j,i,v,w,s,C<0)}},ellipse:function(h,f,c,a,q,n,d,e){var o=this,g=o.params,b=g.length,m,l,k;if(d-n>=Math.PI*2){o.ellipse(h,f,c,a,q,n,n+Math.PI,e);o.ellipse(h,f,c,a,q,n+Math.PI,d,e);return}if(!e){if(d<n){d+=Math.PI*2}m=o.approximateArc(g,h,f,c,a,q,n,d)}else{if(n<d){n+=Math.PI*2}m=o.approximateArc(g,h,f,c,a,q,d,n);for(l=b,k=g.length-2;l<k;l+=2,k-=2){var p=g[l];g[l]=g[k];g[k]=p;p=g[l+1];g[l+1]=g[k+1];g[k+1]=p}}if(!o.cursor){o.cursor=[g[g.length-2],g[g.length-1]];o.commands.push("M")}else{o.cursor[0]=g[g.length-2];o.cursor[1]=g[g.length-1];o.commands.push("L")}for(l=2;l<m;l+=6){o.commands.push("C")}o.dirt()},arc:function(b,f,a,d,c,e){this.ellipse(b,f,a,a,0,d,c,e)},rect:function(b,e,c,a){if(c==0||a==0){return}var d=this;d.moveTo(b,e);d.lineTo(b+c,e);d.lineTo(b+c,e+a);d.lineTo(b,e+a);d.closePath()},approximateArc:function(s,i,f,o,n,d,x,v){var e=Math.cos(d),z=Math.sin(d),k=Math.cos(x),l=Math.sin(x),q=e*k*o-z*l*n,y=-e*l*o-z*k*n,p=z*k*o+e*l*n,w=-z*l*o+e*k*n,m=Math.PI/2,r=2,j=q,u=y,h=p,t=w,b=0.547443256150549,C,g,A,a,B,c;v-=x;if(v<0){v+=Math.PI*2}s.push(q+i,p+f);while(v>=m){s.push(j+u*b+i,h+t*b+f,j*b+u+i,h*b+t+f,u+i,t+f);r+=6;v-=m;C=j;j=u;u=-C;C=h;h=t;t=-C}if(v){g=(0.3294738052815987+0.012120855841304373*v)*v;A=Math.cos(v);a=Math.sin(v);B=A+g*a;c=a-g*A;s.push(j+u*g+i,h+t*g+f,j*B+u*c+i,h*B+t*c+f,j*A+u*a+i,h*A+t*a+f);r+=6}return r},arcSvg:function(j,h,r,m,w,t,c){if(j<0){j=-j}if(h<0){h=-h}var x=this,u=x.cursor[0],f=x.cursor[1],a=(u-t)/2,y=(f-c)/2,d=Math.cos(r),s=Math.sin(r),o=a*d+y*s,v=-a*s+y*d,i=o/j,g=v/h,p=i*i+g*g,e=(u+t)*0.5,b=(f+c)*0.5,l=0,k=0;if(p>=1){p=Math.sqrt(p);j*=p;h*=p}else{p=Math.sqrt(1/p-1);if(m===w){p=-p}l=p*j*g;k=-p*h*i;e+=d*l-s*k;b+=s*l+d*k}var q=Math.atan2((v-k)/h,(o-l)/j),n=Math.atan2((-v-k)/h,(-o-l)/j)-q;if(w){if(n<=0){n+=Math.PI*2}}else{if(n>=0){n-=Math.PI*2}}x.ellipse(e,b,j,h,r,q,q+n,1-w)},fromSvgString:function(e){if(!e){return}var m=this,h,l={a:7,c:6,h:1,l:2,m:2,q:4,s:4,t:2,v:1,z:0,A:7,C:6,H:1,L:2,M:2,Q:4,S:4,T:2,V:1,Z:0},k="",g,f,c=0,b=0,d=false,j,n,a;if(Ext.isString(e)){h=e.replace(Ext.draw.Path.pathRe," $1 ").replace(Ext.draw.Path.pathRe2," -").split(Ext.draw.Path.pathSplitRe)}else{if(Ext.isArray(e)){h=e.join(",").split(Ext.draw.Path.pathSplitRe)}}for(j=0,n=0;j<h.length;j++){if(h[j]!==""){h[n++]=h[j]}}h.length=n;m.clear();for(j=0;j<h.length;){k=d;d=h[j];a=(d.toUpperCase()!==d);j++;switch(d){case"M":m.moveTo(c=+h[j],b=+h[j+1]);j+=2;while(j<n&&!l.hasOwnProperty(h[j])){m.lineTo(c=+h[j],b=+h[j+1]);j+=2}break;case"L":m.lineTo(c=+h[j],b=+h[j+1]);j+=2;while(j<n&&!l.hasOwnProperty(h[j])){m.lineTo(c=+h[j],b=+h[j+1]);j+=2}break;case"A":while(j<n&&!l.hasOwnProperty(h[j])){m.arcSvg(+h[j],+h[j+1],+h[j+2]*Math.PI/180,+h[j+3],+h[j+4],c=+h[j+5],b=+h[j+6]);j+=7}break;case"C":while(j<n&&!l.hasOwnProperty(h[j])){m.bezierCurveTo(+h[j],+h[j+1],g=+h[j+2],f=+h[j+3],c=+h[j+4],b=+h[j+5]);j+=6}break;case"Z":m.closePath();break;case"m":m.moveTo(c+=+h[j],b+=+h[j+1]);j+=2;while(j<n&&!l.hasOwnProperty(h[j])){m.lineTo(c+=+h[j],b+=+h[j+1]);j+=2}break;case"l":m.lineTo(c+=+h[j],b+=+h[j+1]);j+=2;while(j<n&&!l.hasOwnProperty(h[j])){m.lineTo(c+=+h[j],b+=+h[j+1]);j+=2}break;case"a":while(j<n&&!l.hasOwnProperty(h[j])){m.arcSvg(+h[j],+h[j+1],+h[j+2]*Math.PI/180,+h[j+3],+h[j+4],c+=+h[j+5],b+=+h[j+6]);j+=7}break;case"c":while(j<n&&!l.hasOwnProperty(h[j])){m.bezierCurveTo(c+(+h[j]),b+(+h[j+1]),g=c+(+h[j+2]),f=b+(+h[j+3]),c+=+h[j+4],b+=+h[j+5]);j+=6}break;case"z":m.closePath();break;case"s":if(!(k==="c"||k==="C"||k==="s"||k==="S")){g=c;f=b}while(j<n&&!l.hasOwnProperty(h[j])){m.bezierCurveTo(c+c-g,b+b-f,g=c+(+h[j]),f=b+(+h[j+1]),c+=+h[j+2],b+=+h[j+3]);j+=4}break;case"S":if(!(k==="c"||k==="C"||k==="s"||k==="S")){g=c;f=b}while(j<n&&!l.hasOwnProperty(h[j])){m.bezierCurveTo(c+c-g,b+b-f,g=+h[j],f=+h[j+1],c=(+h[j+2]),b=(+h[j+3]));j+=4}break;case"q":while(j<n&&!l.hasOwnProperty(h[j])){m.quadraticCurveTo(g=c+(+h[j]),f=b+(+h[j+1]),c+=+h[j+2],b+=+h[j+3]);j+=4}break;case"Q":while(j<n&&!l.hasOwnProperty(h[j])){m.quadraticCurveTo(g=+h[j],f=+h[j+1],c=+h[j+2],b=+h[j+3]);j+=4}break;case"t":if(!(k==="q"||k==="Q"||k==="t"||k==="T")){g=c;f=b}while(j<n&&!l.hasOwnProperty(h[j])){m.quadraticCurveTo(g=c+c-g,f=b+b-f,c+=+h[j+1],b+=+h[j+2]);j+=2}break;case"T":if(!(k==="q"||k==="Q"||k==="t"||k==="T")){g=c;f=b}while(j<n&&!l.hasOwnProperty(h[j])){m.quadraticCurveTo(g=c+c-g,f=b+b-f,c=(+h[j+1]),b=(+h[j+2]));j+=2}break;case"h":while(j<n&&!l.hasOwnProperty(h[j])){m.lineTo(c+=+h[j],b);j++}break;case"H":while(j<n&&!l.hasOwnProperty(h[j])){m.lineTo(c=+h[j],b);j++}break;case"v":while(j<n&&!l.hasOwnProperty(h[j])){m.lineTo(c,b+=+h[j]);j++}break;case"V":while(j<n&&!l.hasOwnProperty(h[j])){m.lineTo(c,b=+h[j]);j++}break}}},clone:function(){var a=this,b=new Ext.draw.Path();b.params=a.params.slice(0);b.commands=a.commands.slice(0);b.cursor=a.cursor?a.cursor.slice(0):null;b.startX=a.startX;b.startY=a.startY;b.svgString=a.svgString;return b},transform:function(j){if(j.isIdentity()){return}var a=j.getXX(),f=j.getYX(),m=j.getDX(),l=j.getXY(),e=j.getYY(),k=j.getDY(),b=this.params,c=0,d=b.length,h,g;for(;c<d;c+=2){h=b[c];g=b[c+1];b[c]=h*a+g*f+m;b[c+1]=h*l+g*e+k}this.dirt()},getDimension:function(f){if(!f){f={}}if(!this.commands||!this.commands.length){f.x=0;f.y=0;f.width=0;f.height=0;return f}f.left=Infinity;f.top=Infinity;f.right=-Infinity;f.bottom=-Infinity;var d=0,c=0,b=this.commands,g=this.params,e=b.length,a,h;for(;d<e;d++){switch(b[d]){case"M":case"L":a=g[c];h=g[c+1];f.left=Math.min(a,f.left);f.top=Math.min(h,f.top);f.right=Math.max(a,f.right);f.bottom=Math.max(h,f.bottom);c+=2;break;case"C":this.expandDimension(f,a,h,g[c],g[c+1],g[c+2],g[c+3],a=g[c+4],h=g[c+5]);c+=6;break}}f.x=f.left;f.y=f.top;f.width=f.right-f.left;f.height=f.bottom-f.top;return f},getDimensionWithTransform:function(n,f){if(!this.commands||!this.commands.length){if(!f){f={}}f.x=0;f.y=0;f.width=0;f.height=0;return f}f.left=Infinity;f.top=Infinity;f.right=-Infinity;f.bottom=-Infinity;var a=n.getXX(),k=n.getYX(),q=n.getDX(),p=n.getXY(),h=n.getYY(),o=n.getDY(),e=0,d=0,b=this.commands,c=this.params,g=b.length,m,l;for(;e<g;e++){switch(b[e]){case"M":case"L":m=c[d]*a+c[d+1]*k+q;l=c[d]*p+c[d+1]*h+o;f.left=Math.min(m,f.left);f.top=Math.min(l,f.top);f.right=Math.max(m,f.right);f.bottom=Math.max(l,f.bottom);d+=2;break;case"C":this.expandDimension(f,m,l,c[d]*a+c[d+1]*k+q,c[d]*p+c[d+1]*h+o,c[d+2]*a+c[d+3]*k+q,c[d+2]*p+c[d+3]*h+o,m=c[d+4]*a+c[d+5]*k+q,l=c[d+4]*p+c[d+5]*h+o);d+=6;break}}if(!f){f={}}f.x=f.left;f.y=f.top;f.width=f.right-f.left;f.height=f.bottom-f.top;return f},expandDimension:function(i,d,p,k,g,j,e,c,o){var m=this,f=i.left,a=i.right,q=i.top,n=i.bottom,h=m.dim||(m.dim=[]);m.curveDimension(d,k,j,c,h);f=Math.min(f,h[0]);a=Math.max(a,h[1]);m.curveDimension(p,g,e,o,h);q=Math.min(q,h[0]);n=Math.max(n,h[1]);i.left=f;i.right=a;i.top=q;i.bottom=n},curveDimension:function(p,n,k,j,h){var i=3*(-p+3*(n-k)+j),g=6*(p-2*n+k),f=-3*(p-n),o,m,e=Math.min(p,j),l=Math.max(p,j),q;if(i===0){if(g===0){h[0]=e;h[1]=l;return}else{o=-f/g;if(0<o&&o<1){m=this.interpolate(p,n,k,j,o);e=Math.min(e,m);l=Math.max(l,m)}}}else{q=g*g-4*i*f;if(q>=0){q=Math.sqrt(q);o=(q-g)/2/i;if(0<o&&o<1){m=this.interpolate(p,n,k,j,o);e=Math.min(e,m);l=Math.max(l,m)}if(q>0){o-=q/i;if(0<o&&o<1){m=this.interpolate(p,n,k,j,o);e=Math.min(e,m);l=Math.max(l,m)}}}}h[0]=e;h[1]=l},interpolate:function(f,e,j,i,g){if(g===0){return f}if(g===1){return i}var h=(1-g)/g;return g*g*g*(i+h*(3*j+h*(3*e+h*f)))},fromStripes:function(g){var e=this,c=0,d=g.length,b,a,f;e.clear();for(;c<d;c++){f=g[c];e.params.push.apply(e.params,f);e.commands.push("M");for(b=2,a=f.length;b<a;b+=6){e.commands.push("C")}}if(!e.cursor){e.cursor=[]}e.cursor[0]=e.params[e.params.length-2];e.cursor[1]=e.params[e.params.length-1];e.dirt()},toStripes:function(k){var o=k||[],p,n,m,b,a,h,g,f,e,c=this.commands,d=this.params,l=c.length;for(f=0,e=0;f<l;f++){switch(c[f]){case"M":p=[h=b=d[e++],g=a=d[e++]];o.push(p);break;case"L":n=d[e++];m=d[e++];p.push((b+b+n)/3,(a+a+m)/3,(b+n+n)/3,(a+m+m)/3,b=n,a=m);break;case"C":p.push(d[e++],d[e++],d[e++],d[e++],b=d[e++],a=d[e++]);break;case"Z":n=h;m=g;p.push((b+b+n)/3,(a+a+m)/3,(b+n+n)/3,(a+m+m)/3,b=n,a=m);break}}return o},updateSvgString:function(){var b=[],a=this.commands,f=this.params,e=a.length,d=0,c=0;for(;d<e;d++){switch(a[d]){case"M":b.push("M"+f[c]+","+f[c+1]);c+=2;break;case"L":b.push("L"+f[c]+","+f[c+1]);c+=2;break;case"C":b.push("C"+f[c]+","+f[c+1]+" "+f[c+2]+","+f[c+3]+" "+f[c+4]+","+f[c+5]);c+=6;break;case"Z":b.push("Z");break}}this.svgString=b.join("")},toString:function(){if(!this.svgString){this.updateSvgString()}return this.svgString}});Ext.define("Ext.draw.sprite.Path",{extend:Ext.draw.sprite.Sprite,alias:["sprite.path","Ext.draw.Sprite"],type:"path",isPath:true,inheritableStatics:{def:{processors:{path:function(b,a){if(!(b instanceof Ext.draw.Path)){b=new Ext.draw.Path(b)}return b}},aliases:{d:"path"},triggers:{path:"bbox"},updaters:{path:function(a){var b=a.path;if(!b||b.bindAttr!==a){b=new Ext.draw.Path();b.bindAttr=a;a.path=b}b.clear();this.updatePath(b,a);this.scheduleUpdater(a,"bbox",["path"])}}}},updatePlainBBox:function(a){if(this.attr.path){this.attr.path.getDimension(a)}},updateTransformedBBox:function(a){if(this.attr.path){this.attr.path.getDimensionWithTransform(this.attr.matrix,a)}},render:function(b,c){var d=this.attr.matrix,a=this.attr;if(!a.path||a.path.params.length===0){return}d.toContext(c);c.appendPath(a.path);c.fillStroke(a)},updatePath:function(b,a){}});Ext.define("Ext.draw.sprite.Circle",{extend:Ext.draw.sprite.Path,alias:"sprite.circle",type:"circle",inheritableStatics:{def:{processors:{cx:"number",cy:"number",r:"number"},aliases:{radius:"r",x:"cx",y:"cy",centerX:"cx",centerY:"cy"},defaults:{cx:0,cy:0,r:4},triggers:{cx:"path",cy:"path",r:"path"}}},updatePlainBBox:function(c){var b=this.attr,a=b.cx,e=b.cy,d=b.r;c.x=a-d;c.y=e-d;c.width=d+d;c.height=d+d},updateTransformedBBox:function(d){var g=this.attr,f=g.cx,e=g.cy,a=g.r,h=g.matrix,j=h.getScaleX(),i=h.getScaleY(),c,b;c=j*a;b=i*a;d.x=h.x(f,e)-c;d.y=h.y(f,e)-b;d.width=c+c;d.height=b+b},updatePath:function(b,a){b.arc(a.cx,a.cy,a.r,0,Math.PI*2,false)}});Ext.define("Ext.draw.sprite.Arc",{extend:Ext.draw.sprite.Circle,alias:"sprite.arc",type:"arc",inheritableStatics:{def:{processors:{startAngle:"number",endAngle:"number",anticlockwise:"bool"},aliases:{from:"startAngle",to:"endAngle",start:"startAngle",end:"endAngle"},defaults:{startAngle:0,endAngle:Math.PI*2,anticlockwise:false},triggers:{startAngle:"path",endAngle:"path",anticlockwise:"path"}}},updatePath:function(b,a){b.arc(a.cx,a.cy,a.r,a.startAngle,a.endAngle,a.anticlockwise)}});Ext.define("Ext.draw.sprite.Arrow",{extend:Ext.draw.sprite.Path,alias:"sprite.arrow",inheritableStatics:{def:{processors:{x:"number",y:"number",size:"number"},defaults:{x:0,y:0,size:4},triggers:{x:"path",y:"path",size:"path"}}},updatePath:function(d,b){var c=b.size*1.5,a=b.x-b.lineWidth/2,e=b.y;d.fromSvgString("M".concat(a-c*0.7,",",e-c*0.4,"l",[c*0.6,0,0,-c*0.4,c,c*0.8,-c,c*0.8,0,-c*0.4,-c*0.6,0],"z"))}});Ext.define("Ext.draw.sprite.Composite",{extend:Ext.draw.sprite.Sprite,alias:"sprite.composite",type:"composite",isComposite:true,config:{sprites:[]},constructor:function(){this.sprites=[];this.sprites.map={};this.callParent(arguments)},add:function(c){if(!c){return null}if(!c.isSprite){c=Ext.create("sprite."+c.type,c);c.setParent(this);c.setSurface(this.getSurface())}var d=this,a=d.attr,b=c.applyTransformations;c.applyTransformations=function(){if(c.attr.dirtyTransform){a.dirtyTransform=true;a.bbox.plain.dirty=true;a.bbox.transform.dirty=true}b.call(c)};d.sprites.push(c);d.sprites.map[c.id]=c.getId();a.bbox.plain.dirty=true;a.bbox.transform.dirty=true;return c},updateSurface:function(a){for(var b=0,c=this.sprites.length;b<c;b++){this.sprites[b].setSurface(a)}},addAll:function(b){if(b.isSprite||b.type){this.add(b)}else{if(Ext.isArray(b)){var a=0;while(a<b.length){this.add(b[a++])}}}},updatePlainBBox:function(g){var e=this,b=Infinity,h=-Infinity,f=Infinity,a=-Infinity,j,k,c,d;for(c=0,d=e.sprites.length;c<d;c++){j=e.sprites[c];j.applyTransformations();k=j.getBBox();if(b>k.x){b=k.x}if(h<k.x+k.width){h=k.x+k.width}if(f>k.y){f=k.y}if(a<k.y+k.height){a=k.y+k.height}}g.x=b;g.y=f;g.width=h-b;g.height=a-f},render:function(a,b,f){var d=this.attr.matrix,c,e;d.toContext(b);for(c=0,e=this.sprites.length;c<e;c++){a.renderSprite(this.sprites[c],f)}},destroy:function(){var c=this,d=c.sprites,b=d.length,a;c.callParent();for(a=0;a<b;a++){d[a].destroy()}d.length=0}});Ext.define("Ext.draw.sprite.Cross",{extend:Ext.draw.sprite.Path,alias:"sprite.cross",inheritableStatics:{def:{processors:{x:"number",y:"number",size:"number"},defaults:{x:0,y:0,size:4},triggers:{x:"path",y:"path",size:"path"}}},updatePath:function(d,b){var c=b.size/1.7,a=b.x-b.lineWidth/2,e=b.y;d.fromSvgString("M".concat(a-c,",",e,"l",[-c,-c,c,-c,c,c,c,-c,c,c,-c,c,c,c,-c,c,-c,-c,-c,c,-c,-c,"z"]))}});Ext.define("Ext.draw.sprite.Diamond",{extend:Ext.draw.sprite.Path,alias:"sprite.diamond",inheritableStatics:{def:{processors:{x:"number",y:"number",size:"number"},defaults:{x:0,y:0,size:4},triggers:{x:"path",y:"path",size:"path"}}},updatePath:function(d,b){var c=b.size*1.25,a=b.x-b.lineWidth/2,e=b.y;d.fromSvgString(["M",a,e-c,"l",c,c,-c,c,-c,-c,c,-c,"z"])}});Ext.define("Ext.draw.sprite.Ellipse",{extend:Ext.draw.sprite.Path,alias:"sprite.ellipse",type:"ellipse",inheritableStatics:{def:{processors:{cx:"number",cy:"number",rx:"number",ry:"number",axisRotation:"number"},aliases:{radius:"r",x:"cx",y:"cy",centerX:"cx",centerY:"cy",radiusX:"rx",radiusY:"ry"},defaults:{cx:0,cy:0,rx:1,ry:1,axisRotation:0},triggers:{cx:"path",cy:"path",rx:"path",ry:"path",axisRotation:"path"}}},updatePlainBBox:function(c){var b=this.attr,a=b.cx,f=b.cy,e=b.rx,d=b.ry;c.x=a-e;c.y=f-d;c.width=e+e;c.height=d+d},updateTransformedBBox:function(d){var i=this.attr,f=i.cx,e=i.cy,c=i.rx,b=i.ry,l=b/c,m=i.matrix.clone(),a,q,k,j,p,o,n,g;m.append(1,0,0,l,0,e*(1-l));a=m.getXX();k=m.getYX();p=m.getDX();q=m.getXY();j=m.getYY();o=m.getDY();n=Math.sqrt(a*a+k*k)*c;g=Math.sqrt(q*q+j*j)*c;d.x=f*a+e*k+p-n;d.y=f*q+e*j+o-g;d.width=n+n;d.height=g+g},updatePath:function(b,a){b.ellipse(a.cx,a.cy,a.rx,a.ry,a.axisRotation,0,Math.PI*2,false)}});Ext.define("Ext.draw.sprite.EllipticalArc",{extend:Ext.draw.sprite.Ellipse,alias:"sprite.ellipticalArc",type:"ellipticalArc",inheritableStatics:{def:{processors:{startAngle:"number",endAngle:"number",anticlockwise:"bool"},aliases:{from:"startAngle",to:"endAngle",start:"startAngle",end:"endAngle"},defaults:{startAngle:0,endAngle:Math.PI*2,anticlockwise:false},triggers:{startAngle:"path",endAngle:"path",anticlockwise:"path"}}},updatePath:function(b,a){b.ellipse(a.cx,a.cy,a.rx,a.ry,a.axisRotation,a.startAngle,a.endAngle,a.anticlockwise)}});Ext.define("Ext.draw.sprite.Rect",{extend:Ext.draw.sprite.Path,alias:"sprite.rect",type:"rect",inheritableStatics:{def:{processors:{x:"number",y:"number",width:"number",height:"number",radius:"number"},aliases:{},triggers:{x:"path",y:"path",width:"path",height:"path",radius:"path"},defaults:{x:0,y:0,width:8,height:8,radius:0}}},updatePlainBBox:function(b){var a=this.attr;b.x=a.x;b.y=a.y;b.width=a.width;b.height=a.height},updateTransformedBBox:function(a,b){this.attr.matrix.transformBBox(b,this.attr.radius,a)},updatePath:function(f,d){var c=d.x,g=d.y,e=d.width,b=d.height,a=Math.min(d.radius,Math.abs(d.height)*0.5,Math.abs(d.width)*0.5);if(a===0){f.rect(c,g,e,b)}else{f.moveTo(c+a,g);f.arcTo(c+e,g,c+e,g+b,a);f.arcTo(c+e,g+b,c,g+b,a);f.arcTo(c,g+b,c,g,a);f.arcTo(c,g,c+a,g,a)}}});Ext.define("Ext.draw.sprite.Image",{extend:Ext.draw.sprite.Rect,alias:"sprite.image",type:"image",statics:{imageLoaders:{}},inheritableStatics:{def:{processors:{src:"string"},defaults:{src:"",width:null,height:null}}},render:function(c,o){var j=this,h=j.attr,n=h.matrix,a=h.src,l=h.x,k=h.y,b=h.width,m=h.height,g=Ext.draw.sprite.Image.imageLoaders[a],f,d,e;if(g&&g.done){n.toContext(o);d=g.image;o.drawImage(d,l,k,b||(d.naturalWidth||d.width)/c.devicePixelRatio,m||(d.naturalHeight||d.height)/c.devicePixelRatio)}else{if(!g){f=new Image();g=Ext.draw.sprite.Image.imageLoaders[a]={image:f,done:false,pendingSprites:[j],pendingSurfaces:[c]};f.width=b;f.height=m;f.onload=function(){if(!g.done){g.done=true;for(e=0;e<g.pendingSprites.length;e++){g.pendingSprites[e].setDirty(true)}for(e in g.pendingSurfaces){g.pendingSurfaces[e].renderFrame()}}};f.src=a}else{Ext.Array.include(g.pendingSprites,j);Ext.Array.include(g.pendingSurfaces,c)}}}});Ext.define("Ext.draw.sprite.Instancing",{extend:Ext.draw.sprite.Sprite,alias:"sprite.instancing",type:"instancing",isInstancing:true,config:{template:null},instances:null,applyTemplate:function(a){if(!a.isSprite){if(!a.xclass&&!a.type){a.type="circle"}a=Ext.create(a.xclass||"sprite."+a.type,a)}a.setParent(this);return a},updateTemplate:function(a,b){if(b){delete b.ownAttr}a.setSurface(this.getSurface());a.ownAttr=a.attr;this.clearAll()},updateSurface:function(a){var b=this.getTemplate();if(b){b.setSurface(a)}},get:function(a){return this.instances[a]},getCount:function(){return this.instances.length},clearAll:function(){var a=this.getTemplate();a.attr.children=this.instances=[];this.position=0},createInstance:function(d,f,c){var e=this.getTemplate(),b=e.attr,a=Ext.Object.chain(b);e.topModifier.prepareAttributes(a);e.attr=a;e.setAttributes(d,f,c);a.template=e;this.instances.push(a);e.attr=b;this.position++;return a},getBBox:function(){return null},getBBoxFor:function(b,d){var c=this.getTemplate(),a=c.attr,e;c.attr=this.instances[b];e=c.getBBox(d);c.attr=a;return e},isVisible:function(){var b=this.attr,c=this.getParent(),a;a=c&&c.isSurface&&!b.hidden&&b.globalAlpha;return !!a},isInstanceVisible:function(c){var e=this,d=e.getTemplate(),b=d.attr,f=e.instances,a=false;if(!Ext.isNumber(c)||c<0||c>=f.length||!e.isVisible()){return a}d.attr=f[c];a=d.isVisible(point,options);d.attr=b;return a},render:function(b,l,d,h){var g=this,j=g.getTemplate(),k=g.attr.matrix,c=j.attr,a=g.instances,e,f=g.position;k.toContext(l);j.preRender(b,l,d,h);j.useAttributes(l,h);for(e=0;e<f;e++){if(a[e].dirtyZIndex){break}}for(e=0;e<f;e++){if(a[e].hidden){continue}l.save();j.attr=a[e];j.useAttributes(l,h);j.render(b,l,d,h);l.restore()}j.attr=c},setAttributesFor:function(c,e,f){var d=this.getTemplate(),b=d.attr,a=this.instances[c];if(!a){return}d.attr=a;if(f){e=Ext.apply({},e)}else{e=d.self.def.normalize(e)}d.topModifier.pushDown(a,e);d.attr=b},destroy:function(){var b=this,a=b.getTemplate();b.instances=null;if(a){a.destroy()}b.callParent()}});Ext.define("Ext.draw.sprite.Line",{extend:Ext.draw.sprite.Sprite,alias:"sprite.line",type:"line",inheritableStatics:{def:{processors:{fromX:"number",fromY:"number",toX:"number",toY:"number"},defaults:{fromX:0,fromY:0,toX:1,toY:1,strokeStyle:"black"},aliases:{x1:"fromX",y1:"fromY",x2:"toX",y2:"toY"}}},updateLineBBox:function(b,i,s,g,r,f){var o=this.attr,q=o.matrix,h=o.lineWidth/2,m,l,d,c,k,j,n;if(i){n=q.transformPoint([s,g]);s=n[0];g=n[1];n=q.transformPoint([r,f]);r=n[0];f=n[1]}m=Math.min(s,r);d=Math.max(s,r);l=Math.min(g,f);c=Math.max(g,f);var t=Math.atan2(d-m,c-l),a=Math.sin(t),e=Math.cos(t),k=h*e,j=h*a;m-=k;l-=j;d+=k;c+=j;b.x=m;b.y=l;b.width=d-m;b.height=c-l},updatePlainBBox:function(b){var a=this.attr;this.updateLineBBox(b,false,a.fromX,a.fromY,a.toX,a.toY)},updateTransformedBBox:function(b,c){var a=this.attr;this.updateLineBBox(b,true,a.fromX,a.fromY,a.toX,a.toY)},render:function(b,c){var a=this.attr,d=this.attr.matrix;d.toContext(c);c.beginPath();c.moveTo(a.fromX,a.fromY);c.lineTo(a.toX,a.toY);c.stroke()}});Ext.define("Ext.draw.sprite.Plus",{extend:Ext.draw.sprite.Path,alias:"sprite.plus",inheritableStatics:{def:{processors:{x:"number",y:"number",size:"number"},defaults:{x:0,y:0,size:4},triggers:{x:"path",y:"path",size:"path"}}},updatePath:function(d,b){var c=b.size/1.3,a=b.x-b.lineWidth/2,e=b.y;d.fromSvgString("M".concat(a-c/2,",",e-c/2,"l",[0,-c,c,0,0,c,c,0,0,c,-c,0,0,c,-c,0,0,-c,-c,0,0,-c,"z"]))}});Ext.define("Ext.draw.sprite.Sector",{extend:Ext.draw.sprite.Path,alias:"sprite.sector",type:"sector",inheritableStatics:{def:{processors:{centerX:"number",centerY:"number",startAngle:"number",endAngle:"number",startRho:"number",endRho:"number",margin:"number"},aliases:{rho:"endRho"},triggers:{centerX:"path,bbox",centerY:"path,bbox",startAngle:"path,bbox",endAngle:"path,bbox",startRho:"path,bbox",endRho:"path,bbox",margin:"path,bbox"},defaults:{centerX:0,centerY:0,startAngle:0,endAngle:0,startRho:0,endRho:150,margin:0,path:"M 0,0"}}},getMidAngle:function(){return this.midAngle||0},updatePath:function(j,h){var g=Math.min(h.startAngle,h.endAngle),c=Math.max(h.startAngle,h.endAngle),b=this.midAngle=(g+c)*0.5,d=h.margin,f=h.centerX,e=h.centerY,i=Math.min(h.startRho,h.endRho),a=Math.max(h.startRho,h.endRho);if(d){f+=d*Math.cos(b);e+=d*Math.sin(b)}j.moveTo(f+i*Math.cos(g),e+i*Math.sin(g));j.lineTo(f+a*Math.cos(g),e+a*Math.sin(g));j.arc(f,e,a,g,c,false);j.lineTo(f+i*Math.cos(c),e+i*Math.sin(c));j.arc(f,e,i,c,g,true)}});Ext.define("Ext.draw.sprite.Square",{extend:Ext.draw.sprite.Rect,alias:"sprite.square",inheritableStatics:{def:{processors:{size:"number"},defaults:{size:4},triggers:{size:"size"},updaters:{size:function(a){var c=a.size,b=a.lineWidth/2;this.setAttributes({x:a.x-c-b,y:a.y-c,height:2*c,width:2*c})}}}}});Ext.define("Ext.draw.TextMeasurer",{singleton:true,measureDiv:null,measureCache:{},precise:Ext.isIE8,measureDivTpl:{tag:"div",style:{overflow:"hidden",position:"relative","float":"left",width:0,height:0},children:{tag:"div",style:{display:"block",position:"absolute",x:-100000,y:-100000,padding:0,margin:0,"z-index":-100000,"white-space":"nowrap"}}},actualMeasureText:function(g,b){var e=Ext.draw.TextMeasurer,f=e.measureDiv,a=100000,c;if(!f){var d=Ext.Element.create({style:{overflow:"hidden",position:"relative","float":"left",width:0,height:0}});e.measureDiv=f=Ext.Element.create({style:{position:"absolute",x:a,y:a,"z-index":-a,"white-space":"nowrap",display:"block",padding:0,margin:0}});Ext.getBody().appendChild(d);d.appendChild(f)}if(b){f.setStyle({font:b,lineHeight:"normal"})}f.setText("("+g+")");c=f.getSize();f.setText("()");c.width-=f.getSize().width;return c},measureTextSingleLine:function(h,d){if(this.precise){return this.preciseMeasureTextSingleLine(h,d)}h=h.toString();var a=this.measureCache,g=h.split(""),c=0,j=0,l,b,e,f,k;if(!a[d]){a[d]={}}a=a[d];if(a[h]){return a[h]}for(e=0,f=g.length;e<f;e++){b=g[e];if(!(l=a[b])){k=this.actualMeasureText(b,d);l=a[b]=k}c+=l.width;j=Math.max(j,l.height)}return a[h]={width:c,height:j}},preciseMeasureTextSingleLine:function(c,a){c=c.toString();var b=this.measureDiv||(this.measureDiv=Ext.getBody().createChild(this.measureDivTpl).down("div"));b.setStyle({font:a||""});return Ext.util.TextMetrics.measure(b,c)},measureText:function(e,b){var h=e.split("\n"),d=h.length,f=0,a=0,j,c,g;if(d===1){return this.measureTextSingleLine(e,b)}g=[];for(c=0;c<d;c++){j=this.measureTextSingleLine(h[c],b);g.push(j);f+=j.height;a=Math.max(a,j.width)}return{width:a,height:f,sizes:g}}});Ext.define("Ext.draw.sprite.Text",function(){var d={"xx-small":true,"x-small":true,small:true,medium:true,large:true,"x-large":true,"xx-large":true};var b={normal:true,bold:true,bolder:true,lighter:true,100:true,200:true,300:true,400:true,500:true,600:true,700:true,800:true,900:true};var a={start:"start",left:"start",center:"center",middle:"center",end:"end",right:"end"};var c={top:"top",hanging:"hanging",middle:"middle",center:"middle",alphabetic:"alphabetic",ideographic:"ideographic",bottom:"bottom"};return{extend:Ext.draw.sprite.Sprite,alias:"sprite.text",type:"text",lineBreakRe:/\r?\n/g,inheritableStatics:{def:{animationProcessors:{text:"text"},processors:{x:"number",y:"number",text:"string",fontSize:function(e){if(Ext.isNumber(+e)){return e+"px"}else{if(e.match(Ext.dom.Element.unitRe)){return e}else{if(e in d){return e}}}},fontStyle:"enums(,italic,oblique)",fontVariant:"enums(,small-caps)",fontWeight:function(e){if(e in b){return String(e)}else{return""}},fontFamily:"string",textAlign:function(e){return a[e]||"center"},textBaseline:function(e){return c[e]||"alphabetic"},font:"string"},aliases:{"font-size":"fontSize","font-family":"fontFamily","font-weight":"fontWeight","font-variant":"fontVariant","text-anchor":"textAlign"},defaults:{fontStyle:"",fontVariant:"",fontWeight:"",fontSize:"10px",fontFamily:"sans-serif",font:"10px sans-serif",textBaseline:"alphabetic",textAlign:"start",strokeStyle:"rgba(0, 0, 0, 0)",fillStyle:"#000",x:0,y:0,text:""},triggers:{fontStyle:"fontX,bbox",fontVariant:"fontX,bbox",fontWeight:"fontX,bbox",fontSize:"fontX,bbox",fontFamily:"fontX,bbox",font:"font,bbox,canvas",textBaseline:"bbox",textAlign:"bbox",x:"bbox",y:"bbox",text:"bbox"},updaters:{fontX:"makeFontShorthand",font:"parseFontShorthand"}}},constructor:function(e){if(e&&e.font){e=Ext.clone(e);for(var f in e){if(f!=="font"&&f.indexOf("font")===0){delete e[f]}}}Ext.draw.sprite.Sprite.prototype.constructor.call(this,e)},fontValuesMap:{italic:"fontStyle",oblique:"fontStyle","small-caps":"fontVariant",bold:"fontWeight",bolder:"fontWeight",lighter:"fontWeight","100":"fontWeight","200":"fontWeight","300":"fontWeight","400":"fontWeight","500":"fontWeight","600":"fontWeight","700":"fontWeight","800":"fontWeight","900":"fontWeight","xx-small":"fontSize","x-small":"fontSize",small:"fontSize",medium:"fontSize",large:"fontSize","x-large":"fontSize","xx-large":"fontSize"},makeFontShorthand:function(e){var f=[];if(e.fontStyle){f.push(e.fontStyle)}if(e.fontVariant){f.push(e.fontVariant)}if(e.fontWeight){f.push(e.fontWeight)}if(e.fontSize){f.push(e.fontSize)}if(e.fontFamily){f.push(e.fontFamily)}this.setAttributes({font:f.join(" ")},true)},parseFontShorthand:function(j){var m=j.font,k=m.length,l={},n=this.fontValuesMap,e=0,i,g,f,h;while(e<k&&i!==-1){i=m.indexOf(" ",e);if(i<0){f=m.substr(e)}else{if(i>e){f=m.substr(e,i-e)}else{continue}}g=f.indexOf("/");if(g>0){f=f.substr(0,g)}else{if(g===0){continue}}if(f!=="normal"&&f!=="inherit"){h=n[f];if(h){l[h]=f}else{if(f.match(Ext.dom.Element.unitRe)){l.fontSize=f}else{l.fontFamily=m.substr(e);break}}}e=i+1}if(!l.fontStyle){l.fontStyle=""}if(!l.fontVariant){l.fontVariant=""}if(!l.fontWeight){l.fontWeight=""}this.setAttributes(l,true)},fontProperties:{fontStyle:true,fontVariant:true,fontWeight:true,fontSize:true,fontFamily:true},setAttributes:function(g,i,e){var f,h;if(g&&g.font){h={};for(f in g){if(!(f in this.fontProperties)){h[f]=g[f]}}g=h}this.callParent([g,i,e])},getBBox:function(g){var h=this,f=h.attr.bbox.plain,e=h.getSurface();if(f.dirty){h.updatePlainBBox(f);f.dirty=false}if(e.getInherited().rtl&&e.getFlipRtlText()){h.updatePlainBBox(f,true)}return h.callParent([g])},rtlAlignments:{start:"end",center:"center",end:"start"},updatePlainBBox:function(k,B){var C=this,w=C.attr,o=w.x,n=w.y,q=[],t=w.font,r=w.text,s=w.textBaseline,l=w.textAlign,u=(B&&C.oldSize)?C.oldSize:(C.oldSize=Ext.draw.TextMeasurer.measureText(r,t)),z=C.getSurface(),p=z.getInherited().rtl,v=p&&z.getFlipRtlText(),h=z.getRect(),f=u.sizes,g=u.height,j=u.width,m=f?f.length:0,e,A=0;switch(s){case"hanging":case"top":break;case"ideographic":case"bottom":n-=g;break;case"alphabetic":n-=g*0.8;break;case"middle":n-=g*0.5;break}if(v){o=h[2]-h[0]-o;l=C.rtlAlignments[l]}switch(l){case"start":if(p){for(;A<m;A++){e=f[A].width;q.push(-(j-e))}}break;case"end":o-=j;if(p){break}for(;A<m;A++){e=f[A].width;q.push(j-e)}break;case"center":o-=j*0.5;for(;A<m;A++){e=f[A].width;q.push((p?-1:1)*(j-e)*0.5)}break}w.textAlignOffsets=q;k.x=o;k.y=n;k.width=j;k.height=g},setText:function(e){this.setAttributes({text:e},true)},render:function(e,q,k){var h=this,g=h.attr,p=Ext.draw.Matrix.fly(g.matrix.elements.slice(0)),o=h.getBBox(true),s=g.textAlignOffsets,m=Ext.draw.Color.RGBA_NONE,l,j,f,r,n;if(g.text.length===0){return}r=g.text.split(h.lineBreakRe);n=o.height/r.length;l=g.bbox.plain.x;j=g.bbox.plain.y+n*0.78;p.toContext(q);if(e.getInherited().rtl){l+=g.bbox.plain.width}for(f=0;f<r.length;f++){if(q.fillStyle!==m){q.fillText(r[f],l+(s[f]||0),j+n*f)}if(q.strokeStyle!==m){q.strokeText(r[f],l+(s[f]||0),j+n*f)}}}}});Ext.define("Ext.draw.sprite.Tick",{extend:Ext.draw.sprite.Line,alias:"sprite.tick",inheritableStatics:{def:{processors:{x:"number",y:"number",size:"number"},defaults:{x:0,y:0,size:4},triggers:{x:"tick",y:"tick",size:"tick"},updaters:{tick:function(b){var d=b.size*1.5,c=b.lineWidth/2,a=b.x,e=b.y;this.setAttributes({fromX:a-c,fromY:e-d,toX:a-c,toY:e+d})}}}}});Ext.define("Ext.draw.sprite.Triangle",{extend:Ext.draw.sprite.Path,alias:"sprite.triangle",inheritableStatics:{def:{processors:{x:"number",y:"number",size:"number"},defaults:{x:0,y:0,size:4},triggers:{x:"path",y:"path",size:"path"}}},updatePath:function(d,b){var c=b.size*2.2,a=b.x,e=b.y;d.fromSvgString("M".concat(a,",",e,"m0-",c*0.58,"l",c*0.5,",",c*0.87,"-",c,",0z"))}});Ext.define("Ext.draw.gradient.Linear",{extend:Ext.draw.gradient.Gradient,type:"linear",config:{degrees:0,radians:0},applyRadians:function(b,a){if(Ext.isNumber(b)){return b}return a},applyDegrees:function(b,a){if(Ext.isNumber(b)){return b}return a},updateRadians:function(a){this.setDegrees(Ext.draw.Draw.degrees(a))},updateDegrees:function(a){this.setRadians(Ext.draw.Draw.rad(a))},generateGradient:function(q,o){var c=this.getRadians(),p=Math.cos(c),j=Math.sin(c),m=o.width,f=o.height,d=o.x+m*0.5,b=o.y+f*0.5,n=this.getStops(),g=n.length,k,a,e;if(Ext.isNumber(d+b)&&f>0&&m>0){a=(Math.sqrt(f*f+m*m)*Math.abs(Math.cos(c-Math.atan(f/m))))/2;k=q.createLinearGradient(d+p*a,b+j*a,d-p*a,b-j*a);for(e=0;e<g;e++){k.addColorStop(n[e].offset,n[e].color)}return k}return Ext.draw.Color.NONE}});Ext.define("Ext.draw.gradient.Radial",{extend:Ext.draw.gradient.Gradient,type:"radial",config:{start:{x:0,y:0,r:0},end:{x:0,y:0,r:1}},applyStart:function(a,b){if(!b){return a}var c={x:b.x,y:b.y,r:b.r};if("x" in a){c.x=a.x}else{if("centerX" in a){c.x=a.centerX}}if("y" in a){c.y=a.y}else{if("centerY" in a){c.y=a.centerY}}if("r" in a){c.r=a.r}else{if("radius" in a){c.r=a.radius}}return c},applyEnd:function(b,a){if(!a){return b}var c={x:a.x,y:a.y,r:a.r};if("x" in b){c.x=b.x}else{if("centerX" in b){c.x=b.centerX}}if("y" in b){c.y=b.y}else{if("centerY" in b){c.y=b.centerY}}if("r" in b){c.r=b.r}else{if("radius" in b){c.r=b.radius}}return c},generateGradient:function(n,m){var a=this.getStart(),b=this.getEnd(),k=m.width*0.5,d=m.height*0.5,j=m.x+k,f=m.y+d,g=n.createRadialGradient(j+a.x*k,f+a.y*d,a.r*Math.max(k,d),j+b.x*k,f+b.y*d,b.r*Math.max(k,d)),l=this.getStops(),e=l.length,c;for(c=0;c<e;c++){g.addColorStop(l[c].offset,l[c].color)}return g}});Ext.define("Ext.draw.Surface",{extend:Ext.draw.SurfaceBase,xtype:"surface",devicePixelRatio:window.devicePixelRatio||window.screen.deviceXDPI/window.screen.logicalXDPI,deprecated:{"5.1.0":{statics:{methods:{stableSort:function(a){return Ext.Array.sort(a,function(d,c){return d.attr.zIndex-c.attr.zIndex})}}}}},config:{cls:Ext.baseCSSPrefix+"surface",rect:null,background:null,items:[],dirty:false,flipRtlText:false},isSurface:true,isPendingRenderFrame:false,dirtyPredecessorCount:0,constructor:function(a){var b=this;b.predecessors=[];b.successors=[];b.map={};b.callParent([a]);b.matrix=new Ext.draw.Matrix();b.inverseMatrix=b.matrix.inverse()},roundPixel:function(a){return Math.round(this.devicePixelRatio*a)/this.devicePixelRatio},waitFor:function(a){var b=this,c=b.predecessors;if(!Ext.Array.contains(c,a)){c.push(a);a.successors.push(b);if(a.getDirty()){b.dirtyPredecessorCount++}}},updateDirty:function(d){var c=this.successors,e=c.length,b=0,a;for(;b<e;b++){a=c[b];if(d){a.dirtyPredecessorCount++;a.setDirty(true)}else{a.dirtyPredecessorCount--;if(a.dirtyPredecessorCount===0&&a.isPendingRenderFrame){a.renderFrame()}}}},applyBackground:function(a,b){this.setDirty(true);if(Ext.isString(a)){a={fillStyle:a}}return Ext.factory(a,Ext.draw.sprite.Rect,b)},applyRect:function(a,b){if(b&&a[0]===b[0]&&a[1]===b[1]&&a[2]===b[2]&&a[3]===b[3]){return}if(Ext.isArray(a)){return[a[0],a[1],a[2],a[3]]}else{if(Ext.isObject(a)){return[a.x||a.left,a.y||a.top,a.width||(a.right-a.left),a.height||(a.bottom-a.top)]}}},updateRect:function(i){var h=this,c=i[0],f=i[1],g=c+i[2],a=f+i[3],e=h.getBackground(),d=h.element;d.setLocalXY(Math.floor(c),Math.floor(f));d.setSize(Math.ceil(g-Math.floor(c)),Math.ceil(a-Math.floor(f)));if(e){e.setAttributes({x:0,y:0,width:Math.ceil(g-Math.floor(c)),height:Math.ceil(a-Math.floor(f))})}h.setDirty(true)},resetTransform:function(){this.matrix.set(1,0,0,1,0,0);this.inverseMatrix.set(1,0,0,1,0,0);this.setDirty(true)},get:function(a){return this.map[a]||this.getItems()[a]},add:function(){var g=this,e=Array.prototype.slice.call(arguments),j=Ext.isArray(e[0]),a=g.map,c=[],f,k,h,b,d;f=Ext.Array.clean(j?e[0]:e);if(!f.length){return c}for(b=0,d=f.length;b<d;b++){k=f[b];h=null;if(k.isSprite&&!a[k.getId()]){h=k}else{if(!a[k.id]){h=this.createItem(k)}}if(h){a[h.getId()]=h;c.push(h);h.setParent(g);h.setSurface(g);g.onAdd(h)}}f=g.getItems();if(f){f.push.apply(f,c)}g.dirtyZIndex=true;g.setDirty(true);if(!j&&c.length===1){return c[0]}else{return c}},onAdd:Ext.emptyFn,remove:function(a,c){var b=this,e,d;if(a){if(a.charAt){a=b.map[a]}if(!a||!a.isSprite){return null}if(a.isDestroyed||a.isDestroying){return a}e=a.getId();d=b.map[e];delete b.map[e];if(c){a.destroy()}if(!d){return a}a.setParent(null);a.setSurface(null);Ext.Array.remove(b.getItems(),a);b.dirtyZIndex=true;b.setDirty(true)}return a||null},removeAll:function(d){var a=this.getItems(),b=a.length-1,c;if(d){for(;b>=0;b--){a[b].destroy()}}else{for(;b>=0;b--){c=a[b];c.setParent(null);c.setSurface(null)}}a.length=0;this.map={};this.dirtyZIndex=true},applyItems:function(a){if(this.getItems()){this.removeAll(true)}return Ext.Array.from(this.add(a))},createItem:function(a){return Ext.create(a.xclass||"sprite."+a.type,a)},getBBox:function(f,b){var f=Ext.Array.from(f),c=Infinity,h=-Infinity,g=Infinity,a=-Infinity,j,k,d,e;for(d=0,e=f.length;d<e;d++){j=f[d];k=j.getBBox(b);if(c>k.x){c=k.x}if(h<k.x+k.width){h=k.x+k.width}if(g>k.y){g=k.y}if(a<k.y+k.height){a=k.y+k.height}}return{x:c,y:g,width:h-c,height:a-g}},emptyRect:[0,0,0,0],getEventXY:function(d){var g=this,f=g.getInherited().rtl,c=d.getXY(),a=g.getOwnerBody(),i=a.getXY(),h=g.getRect()||g.emptyRect,j=[],b;if(f){b=a.getWidth();j[0]=i[0]-c[0]-h[0]+b}else{j[0]=c[0]-i[0]-h[0]}j[1]=c[1]-i[1]-h[1];return j},clear:Ext.emptyFn,orderByZIndex:function(){var d=this,a=d.getItems(),e=false,b,c;if(d.getDirty()){for(b=0,c=a.length;b<c;b++){if(a[b].attr.dirtyZIndex){e=true;break}}if(e){Ext.Array.sort(a,function(g,f){return g.attr.zIndex-f.attr.zIndex});this.setDirty(true)}for(b=0,c=a.length;b<c;b++){a[b].attr.dirtyZIndex=false}}},repaint:function(){var a=this;a.repaint=Ext.emptyFn;Ext.defer(function(){delete a.repaint;a.element.repaint()},1)},renderFrame:function(){var g=this;if(!g.element){return}if(g.dirtyPredecessorCount>0){g.isPendingRenderFrame=true;return}var f=g.getRect(),c=g.getBackground(),a=g.getItems(),e,b,d;if(!f){return}g.orderByZIndex();if(g.getDirty()){g.clear();g.clearTransform();if(c){g.renderSprite(c)}for(b=0,d=a.length;b<d;b++){e=a[b];if(g.renderSprite(e)===false){return}e.attr.textPositionCount=g.textPosition}g.setDirty(false)}},renderSprite:Ext.emptyFn,clearTransform:Ext.emptyFn,destroy:function(){var a=this;a.removeAll(true);a.predecessors=null;a.successors=null;a.callParent()}});Ext.define("Ext.draw.engine.SvgContext",{toSave:["strokeOpacity","strokeStyle","fillOpacity","fillStyle","globalAlpha","lineWidth","lineCap","lineJoin","lineDash","lineDashOffset","miterLimit","shadowOffsetX","shadowOffsetY","shadowBlur","shadowColor","globalCompositeOperation","position","fillGradient","strokeGradient"],strokeOpacity:1,strokeStyle:"none",fillOpacity:1,fillStyle:"none",lineDash:[],lineDashOffset:0,globalAlpha:1,lineWidth:1,lineCap:"butt",lineJoin:"miter",miterLimit:10,shadowOffsetX:0,shadowOffsetY:0,shadowBlur:0,shadowColor:"none",globalCompositeOperation:"src",urlStringRe:/^url\(#([\w\-]+)\)$/,constructor:function(a){this.surface=a;this.state=[];this.matrix=new Ext.draw.Matrix();this.path=null;this.clear()},clear:function(){this.group=this.surface.mainGroup;this.position=0;this.path=null},getElement:function(a){return this.surface.getSvgElement(this.group,a,this.position++)},removeElement:function(d){var d=Ext.fly(d),h,g,b,f,a,e,c;if(!d){return}if(d.dom.tagName==="g"){a=d.dom.gradients;for(c in a){a[c].destroy()}}else{h=d.getAttribute("fill");g=d.getAttribute("stroke");b=h&&h.match(this.urlStringRe);f=g&&g.match(this.urlStringRe);if(b&&b[1]){e=Ext.fly(b[1]);if(e){e.destroy()}}if(f&&f[1]){e=Ext.fly(f[1]);if(e){e.destroy()}}}d.destroy()},save:function(){var c=this.toSave,e={},d=this.getElement("g"),b,a;for(a=0;a<c.length;a++){b=c[a];if(b in this){e[b]=this[b]}}this.position=0;e.matrix=this.matrix.clone();this.state.push(e);this.group=d;return d},restore:function(){var d=this.toSave,e=this.state.pop(),c=this.group.dom.childNodes,b,a;while(c.length>this.position){this.removeElement(c[c.length-1])}for(a=0;a<d.length;a++){b=d[a];if(b in e){this[b]=e[b]}else{delete this[b]}}this.setTransform.apply(this,e.matrix.elements);this.group=this.group.getParent()},transform:function(f,b,e,g,d,c){if(this.path){var a=Ext.draw.Matrix.fly([f,b,e,g,d,c]).inverse();this.path.transform(a)}this.matrix.append(f,b,e,g,d,c)},setTransform:function(e,a,d,f,c,b){if(this.path){this.path.transform(this.matrix)}this.matrix.reset();this.transform(e,a,d,f,c,b)},scale:function(a,b){this.transform(a,0,0,b,0,0)},rotate:function(d){var c=Math.cos(d),a=Math.sin(d),b=-Math.sin(d),e=Math.cos(d);this.transform(c,a,b,e,0,0)},translate:function(a,b){this.transform(1,0,0,1,a,b)},setGradientBBox:function(a){this.bbox=a},beginPath:function(){this.path=new Ext.draw.Path()},moveTo:function(a,b){if(!this.path){this.beginPath()}this.path.moveTo(a,b);this.path.element=null},lineTo:function(a,b){if(!this.path){this.beginPath()}this.path.lineTo(a,b);this.path.element=null},rect:function(b,d,c,a){this.moveTo(b,d);this.lineTo(b+c,d);this.lineTo(b+c,d+a);this.lineTo(b,d+a);this.closePath()},strokeRect:function(b,d,c,a){this.beginPath();this.rect(b,d,c,a);this.stroke()},fillRect:function(b,d,c,a){this.beginPath();this.rect(b,d,c,a);this.fill()},closePath:function(){if(!this.path){this.beginPath()}this.path.closePath();this.path.element=null},arcSvg:function(d,a,f,g,c,b,e){if(!this.path){this.beginPath()}this.path.arcSvg(d,a,f,g,c,b,e);this.path.element=null},arc:function(b,f,a,d,c,e){if(!this.path){this.beginPath()}this.path.arc(b,f,a,d,c,e);this.path.element=null},ellipse:function(a,h,g,f,d,c,b,e){if(!this.path){this.beginPath()}this.path.ellipse(a,h,g,f,d,c,b,e);this.path.element=null},arcTo:function(b,e,a,d,g,f,c){if(!this.path){this.beginPath()}this.path.arcTo(b,e,a,d,g,f,c);this.path.element=null},bezierCurveTo:function(d,f,b,e,a,c){if(!this.path){this.beginPath()}this.path.bezierCurveTo(d,f,b,e,a,c);this.path.element=null},strokeText:function(d,a,e){d=String(d);if(this.strokeStyle){var b=this.getElement("text"),c=this.surface.getSvgElement(b,"tspan",0);this.surface.setElementAttributes(b,{x:a,y:e,transform:this.matrix.toSvg(),stroke:this.strokeStyle,fill:"none",opacity:this.globalAlpha,"stroke-opacity":this.strokeOpacity,style:"font: "+this.font,"stroke-dasharray":this.lineDash.join(","),"stroke-dashoffset":this.lineDashOffset});if(this.lineDash.length){this.surface.setElementAttributes(b,{"stroke-dasharray":this.lineDash.join(","),"stroke-dashoffset":this.lineDashOffset})}if(c.dom.firstChild){c.dom.removeChild(c.dom.firstChild)}this.surface.setElementAttributes(c,{"alignment-baseline":"alphabetic"});c.dom.appendChild(document.createTextNode(Ext.String.htmlDecode(d)))}},fillText:function(d,a,e){d=String(d);if(this.fillStyle){var b=this.getElement("text"),c=this.surface.getSvgElement(b,"tspan",0);this.surface.setElementAttributes(b,{x:a,y:e,transform:this.matrix.toSvg(),fill:this.fillStyle,opacity:this.globalAlpha,"fill-opacity":this.fillOpacity,style:"font: "+this.font});if(c.dom.firstChild){c.dom.removeChild(c.dom.firstChild)}this.surface.setElementAttributes(c,{"alignment-baseline":"alphabetic"});c.dom.appendChild(document.createTextNode(Ext.String.htmlDecode(d)))}},drawImage:function(c,k,i,l,e,p,n,a,g){var f=this,d=f.getElement("image"),j=k,h=i,b=typeof l==="undefined"?c.width:l,m=typeof e==="undefined"?c.height:e,o=null;if(typeof g!=="undefined"){o=k+" "+i+" "+l+" "+e;j=p;h=n;b=a;m=g}d.dom.setAttributeNS("http://www.w3.org/1999/xlink","href",c.src);f.surface.setElementAttributes(d,{viewBox:o,x:j,y:h,width:b,height:m,opacity:f.globalAlpha,transform:f.matrix.toSvg()})},fill:function(){if(!this.path){return}if(this.fillStyle){var c,a=this.fillGradient,d=this.bbox,b=this.path.element;if(!b){c=this.path.toString();b=this.path.element=this.getElement("path");this.surface.setElementAttributes(b,{d:c,transform:this.matrix.toSvg()})}this.surface.setElementAttributes(b,{fill:a&&d?a.generateGradient(this,d):this.fillStyle,"fill-opacity":this.fillOpacity*this.globalAlpha})}},stroke:function(){if(!this.path){return}if(this.strokeStyle){var c,b=this.strokeGradient,d=this.bbox,a=this.path.element;if(!a||!this.path.svgString){c=this.path.toString();if(!c){return}a=this.path.element=this.getElement("path");this.surface.setElementAttributes(a,{fill:"none",d:c,transform:this.matrix.toSvg()})}this.surface.setElementAttributes(a,{stroke:b&&d?b.generateGradient(this,d):this.strokeStyle,"stroke-linecap":this.lineCap,"stroke-linejoin":this.lineJoin,"stroke-width":this.lineWidth,"stroke-opacity":this.strokeOpacity*this.globalAlpha,"stroke-dasharray":this.lineDash.join(","),"stroke-dashoffset":this.lineDashOffset});if(this.lineDash.length){this.surface.setElementAttributes(a,{"stroke-dasharray":this.lineDash.join(","),"stroke-dashoffset":this.lineDashOffset})}}},fillStroke:function(a,e){var b=this,d=b.fillStyle,g=b.strokeStyle,c=b.fillOpacity,f=b.strokeOpacity;if(e===undefined){e=a.transformFillStroke}if(!e){a.inverseMatrix.toContext(b)}if(d&&c!==0){b.fill()}if(g&&f!==0){b.stroke()}},appendPath:function(a){this.path=a.clone()},setLineDash:function(a){this.lineDash=a},getLineDash:function(){return this.lineDash},createLinearGradient:function(d,g,b,e){var f=this,c=f.surface.getNextDef("linearGradient"),a=f.group.dom.gradients||(f.group.dom.gradients={}),h;f.surface.setElementAttributes(c,{x1:d,y1:g,x2:b,y2:e,gradientUnits:"userSpaceOnUse"});h=new Ext.draw.engine.SvgContext.Gradient(f,f.surface,c);a[c.dom.id]=h;return h},createRadialGradient:function(b,j,d,a,i,c){var g=this,e=g.surface.getNextDef("radialGradient"),f=g.group.dom.gradients||(g.group.dom.gradients={}),h;g.surface.setElementAttributes(e,{fx:b,fy:j,cx:a,cy:i,r:c,gradientUnits:"userSpaceOnUse"});h=new Ext.draw.engine.SvgContext.Gradient(g,g.surface,e,d/c);f[e.dom.id]=h;return h}});Ext.define("Ext.draw.engine.SvgContext.Gradient",{statics:{map:{}},constructor:function(c,a,d,b){var f=this.statics().map,e;e=f[d.dom.id];if(e){e.element=null}f[d.dom.id]=this;this.ctx=c;this.surface=a;this.element=d;this.position=0;this.compression=b||0},addColorStop:function(d,b){var c=this.surface.getSvgElement(this.element,"stop",this.position++),a=this.compression;this.surface.setElementAttributes(c,{offset:(((1-a)*d+a)*100).toFixed(2)+"%","stop-color":b,"stop-opacity":Ext.draw.Color.fly(b).a.toFixed(15)})},toString:function(){var a=this.element.dom.childNodes;while(a.length>this.position){Ext.fly(a[a.length-1]).destroy()}return"url(#"+this.element.getId()+")"},destroy:function(){var b=this.statics().map,a=this.element;if(a&&a.dom){delete b[a.dom.id];a.destroy()}this.callParent()}});Ext.define("Ext.draw.engine.Svg",{extend:Ext.draw.Surface,statics:{BBoxTextCache:{}},config:{highPrecision:false},getElementConfig:function(){return{reference:"element",style:{position:"absolute"},children:[{reference:"innerElement",style:{width:"100%",height:"100%",position:"relative"},children:[{tag:"svg",reference:"svgElement",namespace:"http://www.w3.org/2000/svg",width:"100%",height:"100%",version:1.1}]}]}},constructor:function(a){var b=this;b.callParent([a]);b.mainGroup=b.createSvgNode("g");b.defElement=b.createSvgNode("defs");b.svgElement.appendChild(b.mainGroup);b.svgElement.appendChild(b.defElement);b.ctx=new Ext.draw.engine.SvgContext(b)},createSvgNode:function(a){var b=document.createElementNS("http://www.w3.org/2000/svg",a);return Ext.get(b)},getSvgElement:function(d,b,a){var c;if(d.dom.childNodes.length>a){c=d.dom.childNodes[a];if(c.tagName===b){return Ext.get(c)}else{Ext.destroy(c)}}c=Ext.get(this.createSvgNode(b));if(a===0){d.insertFirst(c)}else{c.insertAfter(Ext.fly(d.dom.childNodes[a-1]))}c.cache={};return c},setElementAttributes:function(d,b){var f=d.dom,a=d.cache,c,e;for(c in b){e=b[c];if(a[c]!==e){a[c]=e;f.setAttribute(c,e)}}},getNextDef:function(a){return this.getSvgElement(this.defElement,a,this.defPosition++)},clearTransform:function(){var a=this;a.mainGroup.set({transform:a.matrix.toSvg()})},clear:function(){this.ctx.clear();this.defPosition=0},renderSprite:function(b){var d=this,c=d.getRect(),a=d.ctx;if(b.attr.hidden||b.attr.globalAlpha===0){a.save();a.restore();return}b.element=a.save();b.preRender(this);b.useAttributes(a,c);if(false===b.render(this,a,[0,0,c[2],c[3]])){return false}b.setDirty(false);a.restore()},flatten:function(e,b){var c='<?xml version="1.0" standalone="yes"?>',f=Ext.getClassName(this),a,g,d;c+='<svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" width="'+e.width+'" height="'+e.height+'">';for(d=0;d<b.length;d++){a=b[d];if(Ext.getClassName(a)!==f){continue}g=a.getRect();c+='<g transform="translate('+g[0]+","+g[1]+')">';c+=this.serializeNode(a.svgElement.dom);c+="</g>"}c+="</svg>";return{data:"data:image/svg+xml;utf8,"+encodeURIComponent(c),type:"svg"}},serializeNode:function(d){var b="",c,f,a,e;if(d.nodeType===document.TEXT_NODE){return d.nodeValue}b+="<"+d.nodeName;if(d.attributes.length){for(c=0,f=d.attributes.length;c<f;c++){a=d.attributes[c];b+=" "+a.name+'="'+a.value+'"'}}b+=">";if(d.childNodes&&d.childNodes.length){for(c=0,f=d.childNodes.length;c<f;c++){e=d.childNodes[c];b+=this.serializeNode(e)}}b+="</"+d.nodeName+">";return b},destroy:function(){var a=this;a.ctx.destroy();a.mainGroup.destroy();delete a.mainGroup;delete a.ctx;a.callParent()},remove:function(a,b){if(a&&a.element){if(this.ctx){this.ctx.removeElement(a.element)}else{a.element.destroy()}a.element=null}this.callParent(arguments)}});Ext.draw||(Ext.draw={});Ext.draw.engine||(Ext.draw.engine={});Ext.draw.engine.excanvas=true;if(!document.createElement("canvas").getContext){(function(){var ab=Math;var n=ab.round;var l=ab.sin;var A=ab.cos;var H=ab.abs;var N=ab.sqrt;var d=10;var f=d/2;var z=+navigator.userAgent.match(/MSIE ([\d.]+)?/)[1];function y(){return this.context_||(this.context_=new D(this))}var t=Array.prototype.slice;function g(j,m,p){var i=t.call(arguments,2);return function(){return j.apply(m,i.concat(t.call(arguments)))}}function af(i){return String(i).replace(/&/g,"&amp;").replace(/"/g,"&quot;")}function Y(m,j,i){Ext.onReady(function(){if(!m.namespaces[j]){m.namespaces.add(j,i,"#default#VML")}})}function R(j){Y(j,"g_vml_","urn:schemas-microsoft-com:vml");Y(j,"g_o_","urn:schemas-microsoft-com:office:office");if(!j.styleSheets.ex_canvas_){var i=j.createStyleSheet();i.owningElement.id="ex_canvas_";i.cssText="canvas{display:inline-block;overflow:hidden;text-align:left;width:300px;height:150px}"}}R(document);var e={init:function(i){var j=i||document;j.createElement("canvas");j.attachEvent("onreadystatechange",g(this.init_,this,j))},init_:function(p){var m=p.getElementsByTagName("canvas");for(var j=0;j<m.length;j++){this.initElement(m[j])}},initElement:function(j){if(!j.getContext){j.getContext=y;R(j.ownerDocument);j.innerHTML="";j.attachEvent("onpropertychange",x);j.attachEvent("onresize",W);var i=j.attributes;if(i.width&&i.width.specified){j.style.width=i.width.nodeValue+"px"}else{j.width=j.clientWidth}if(i.height&&i.height.specified){j.style.height=i.height.nodeValue+"px"}else{j.height=j.clientHeight}}return j}};function x(j){var i=j.srcElement;switch(j.propertyName){case"width":i.getContext().clearRect();i.style.width=i.attributes.width.nodeValue+"px";i.firstChild.style.width=i.clientWidth+"px";break;case"height":i.getContext().clearRect();i.style.height=i.attributes.height.nodeValue+"px";i.firstChild.style.height=i.clientHeight+"px";break}}function W(j){var i=j.srcElement;if(i.firstChild){i.firstChild.style.width=i.clientWidth+"px";i.firstChild.style.height=i.clientHeight+"px"}}e.init();var k=[];for(var ae=0;ae<16;ae++){for(var ad=0;ad<16;ad++){k[ae*16+ad]=ae.toString(16)+ad.toString(16)}}function B(){return[[1,0,0],[0,1,0],[0,0,1]]}function J(p,m){var j=B();for(var i=0;i<3;i++){for(var ah=0;ah<3;ah++){var Z=0;for(var ag=0;ag<3;ag++){Z+=p[i][ag]*m[ag][ah]}j[i][ah]=Z}}return j}function v(j,i){i.fillStyle=j.fillStyle;i.lineCap=j.lineCap;i.lineJoin=j.lineJoin;i.lineDash=j.lineDash;i.lineWidth=j.lineWidth;i.miterLimit=j.miterLimit;i.shadowBlur=j.shadowBlur;i.shadowColor=j.shadowColor;i.shadowOffsetX=j.shadowOffsetX;i.shadowOffsetY=j.shadowOffsetY;i.strokeStyle=j.strokeStyle;i.globalAlpha=j.globalAlpha;i.font=j.font;i.textAlign=j.textAlign;i.textBaseline=j.textBaseline;i.arcScaleX_=j.arcScaleX_;i.arcScaleY_=j.arcScaleY_;i.lineScale_=j.lineScale_}var b={aliceblue:"#F0F8FF",antiquewhite:"#FAEBD7",aquamarine:"#7FFFD4",azure:"#F0FFFF",beige:"#F5F5DC",bisque:"#FFE4C4",black:"#000000",blanchedalmond:"#FFEBCD",blueviolet:"#8A2BE2",brown:"#A52A2A",burlywood:"#DEB887",cadetblue:"#5F9EA0",chartreuse:"#7FFF00",chocolate:"#D2691E",coral:"#FF7F50",cornflowerblue:"#6495ED",cornsilk:"#FFF8DC",crimson:"#DC143C",cyan:"#00FFFF",darkblue:"#00008B",darkcyan:"#008B8B",darkgoldenrod:"#B8860B",darkgray:"#A9A9A9",darkgreen:"#006400",darkgrey:"#A9A9A9",darkkhaki:"#BDB76B",darkmagenta:"#8B008B",darkolivegreen:"#556B2F",darkorange:"#FF8C00",darkorchid:"#9932CC",darkred:"#8B0000",darksalmon:"#E9967A",darkseagreen:"#8FBC8F",darkslateblue:"#483D8B",darkslategray:"#2F4F4F",darkslategrey:"#2F4F4F",darkturquoise:"#00CED1",darkviolet:"#9400D3",deeppink:"#FF1493",deepskyblue:"#00BFFF",dimgray:"#696969",dimgrey:"#696969",dodgerblue:"#1E90FF",firebrick:"#B22222",floralwhite:"#FFFAF0",forestgreen:"#228B22",gainsboro:"#DCDCDC",ghostwhite:"#F8F8FF",gold:"#FFD700",goldenrod:"#DAA520",grey:"#808080",greenyellow:"#ADFF2F",honeydew:"#F0FFF0",hotpink:"#FF69B4",indianred:"#CD5C5C",indigo:"#4B0082",ivory:"#FFFFF0",khaki:"#F0E68C",lavender:"#E6E6FA",lavenderblush:"#FFF0F5",lawngreen:"#7CFC00",lemonchiffon:"#FFFACD",lightblue:"#ADD8E6",lightcoral:"#F08080",lightcyan:"#E0FFFF",lightgoldenrodyellow:"#FAFAD2",lightgreen:"#90EE90",lightgrey:"#D3D3D3",lightpink:"#FFB6C1",lightsalmon:"#FFA07A",lightseagreen:"#20B2AA",lightskyblue:"#87CEFA",lightslategray:"#778899",lightslategrey:"#778899",lightsteelblue:"#B0C4DE",lightyellow:"#FFFFE0",limegreen:"#32CD32",linen:"#FAF0E6",magenta:"#FF00FF",mediumaquamarine:"#66CDAA",mediumblue:"#0000CD",mediumorchid:"#BA55D3",mediumpurple:"#9370DB",mediumseagreen:"#3CB371",mediumslateblue:"#7B68EE",mediumspringgreen:"#00FA9A",mediumturquoise:"#48D1CC",mediumvioletred:"#C71585",midnightblue:"#191970",mintcream:"#F5FFFA",mistyrose:"#FFE4E1",moccasin:"#FFE4B5",navajowhite:"#FFDEAD",oldlace:"#FDF5E6",olivedrab:"#6B8E23",orange:"#FFA500",orangered:"#FF4500",orchid:"#DA70D6",palegoldenrod:"#EEE8AA",palegreen:"#98FB98",paleturquoise:"#AFEEEE",palevioletred:"#DB7093",papayawhip:"#FFEFD5",peachpuff:"#FFDAB9",peru:"#CD853F",pink:"#FFC0CB",plum:"#DDA0DD",powderblue:"#B0E0E6",rosybrown:"#BC8F8F",royalblue:"#4169E1",saddlebrown:"#8B4513",salmon:"#FA8072",sandybrown:"#F4A460",seagreen:"#2E8B57",seashell:"#FFF5EE",sienna:"#A0522D",skyblue:"#87CEEB",slateblue:"#6A5ACD",slategray:"#708090",slategrey:"#708090",snow:"#FFFAFA",springgreen:"#00FF7F",steelblue:"#4682B4",tan:"#D2B48C",thistle:"#D8BFD8",tomato:"#FF6347",turquoise:"#40E0D0",violet:"#EE82EE",wheat:"#F5DEB3",whitesmoke:"#F5F5F5",yellowgreen:"#9ACD32"};function M(j){var p=j.indexOf("(",3);var i=j.indexOf(")",p+1);var m=j.substring(p+1,i).split(",");if(m.length!=4||j.charAt(3)!="a"){m[3]=1}return m}function c(i){return parseFloat(i)/100}function r(j,m,i){return Math.min(i,Math.max(m,j))}function I(ag){var i,ai,aj,ah,ak,Z;ah=parseFloat(ag[0])/360%360;if(ah<0){ah++}ak=r(c(ag[1]),0,1);Z=r(c(ag[2]),0,1);if(ak==0){i=ai=aj=Z}else{var j=Z<0.5?Z*(1+ak):Z+ak-Z*ak;var m=2*Z-j;i=a(m,j,ah+1/3);ai=a(m,j,ah);aj=a(m,j,ah-1/3)}return"#"+k[Math.floor(i*255)]+k[Math.floor(ai*255)]+k[Math.floor(aj*255)]}function a(j,i,m){if(m<0){m++}if(m>1){m--}if(6*m<1){return j+(i-j)*6*m}else{if(2*m<1){return i}else{if(3*m<2){return j+(i-j)*(2/3-m)*6}else{return j}}}}var C={};function F(j){if(j in C){return C[j]}var ag,Z=1;j=String(j);if(j.charAt(0)=="#"){ag=j}else{if(/^rgb/.test(j)){var p=M(j);var ag="#",ah;for(var m=0;m<3;m++){if(p[m].indexOf("%")!=-1){ah=Math.floor(c(p[m])*255)}else{ah=+p[m]}ag+=k[r(ah,0,255)]}Z=+p[3]}else{if(/^hsl/.test(j)){var p=M(j);ag=I(p);Z=p[3]}else{ag=b[j]||j}}}return C[j]={color:ag,alpha:Z}}var o={style:"normal",variant:"normal",weight:"normal",size:10,family:"sans-serif"};var L={};function E(i){if(L[i]){return L[i]}var p=document.createElement("div");var m=p.style;try{m.font=i}catch(j){}return L[i]={style:m.fontStyle||o.style,variant:m.fontVariant||o.variant,weight:m.fontWeight||o.weight,size:m.fontSize||o.size,family:m.fontFamily||o.family}}function u(m,j){var i={};for(var ah in m){i[ah]=m[ah]}var ag=parseFloat(j.currentStyle.fontSize),Z=parseFloat(m.size);if(typeof m.size=="number"){i.size=m.size}else{if(m.size.indexOf("px")!=-1){i.size=Z}else{if(m.size.indexOf("em")!=-1){i.size=ag*Z}else{if(m.size.indexOf("%")!=-1){i.size=(ag/100)*Z}else{if(m.size.indexOf("pt")!=-1){i.size=Z/0.75}else{i.size=ag}}}}}i.size*=0.981;return i}function ac(i){return i.style+" "+i.variant+" "+i.weight+" "+i.size+"px "+i.family}var s={butt:"flat",round:"round"};function S(i){return s[i]||"square"}function D(i){this.m_=B();this.mStack_=[];this.aStack_=[];this.currentPath_=[];this.strokeStyle="#000";this.fillStyle="#000";this.lineWidth=1;this.lineJoin="miter";this.lineDash=[];this.lineCap="butt";this.miterLimit=d*1;this.globalAlpha=1;this.font="10px sans-serif";this.textAlign="left";this.textBaseline="alphabetic";this.canvas=i;var m="width:"+i.clientWidth+"px;height:"+i.clientHeight+"px;overflow:hidden;position:absolute";var j=i.ownerDocument.createElement("div");j.style.cssText=m;i.appendChild(j);var p=j.cloneNode(false);p.style.backgroundColor="red";p.style.filter="alpha(opacity=0)";i.appendChild(p);this.element_=j;this.arcScaleX_=1;this.arcScaleY_=1;this.lineScale_=1}var q=D.prototype;q.clearRect=function(){if(this.textMeasureEl_){this.textMeasureEl_.removeNode(true);this.textMeasureEl_=null}this.element_.innerHTML=""};q.beginPath=function(){this.currentPath_=[]};q.moveTo=function(j,i){var m=V(this,j,i);this.currentPath_.push({type:"moveTo",x:m.x,y:m.y});this.currentX_=m.x;this.currentY_=m.y};q.lineTo=function(j,i){var m=V(this,j,i);this.currentPath_.push({type:"lineTo",x:m.x,y:m.y});this.currentX_=m.x;this.currentY_=m.y};q.bezierCurveTo=function(m,j,ak,aj,ai,ag){var i=V(this,ai,ag);var ah=V(this,m,j);var Z=V(this,ak,aj);K(this,ah,Z,i)};function K(i,Z,m,j){i.currentPath_.push({type:"bezierCurveTo",cp1x:Z.x,cp1y:Z.y,cp2x:m.x,cp2y:m.y,x:j.x,y:j.y});i.currentX_=j.x;i.currentY_=j.y}q.quadraticCurveTo=function(ai,m,j,i){var ah=V(this,ai,m);var ag=V(this,j,i);var aj={x:this.currentX_+2/3*(ah.x-this.currentX_),y:this.currentY_+2/3*(ah.y-this.currentY_)};var Z={x:aj.x+(ag.x-this.currentX_)/3,y:aj.y+(ag.y-this.currentY_)/3};K(this,aj,Z,ag)};q.arc=function(al,aj,ak,ag,j,m){ak*=d;var ap=m?"at":"wa";var am=al+A(ag)*ak-f;var ao=aj+l(ag)*ak-f;var i=al+A(j)*ak-f;var an=aj+l(j)*ak-f;if(am==i&&!m){am+=0.125}var Z=V(this,al,aj);var ai=V(this,am,ao);var ah=V(this,i,an);this.currentPath_.push({type:ap,x:Z.x,y:Z.y,radius:ak,xStart:ai.x,yStart:ai.y,xEnd:ah.x,yEnd:ah.y})};q.rect=function(m,j,i,p){this.moveTo(m,j);this.lineTo(m+i,j);this.lineTo(m+i,j+p);this.lineTo(m,j+p);this.closePath()};q.strokeRect=function(m,j,i,p){var Z=this.currentPath_;this.beginPath();this.moveTo(m,j);this.lineTo(m+i,j);this.lineTo(m+i,j+p);this.lineTo(m,j+p);this.closePath();this.stroke();this.currentPath_=Z};q.fillRect=function(m,j,i,p){var Z=this.currentPath_;this.beginPath();this.moveTo(m,j);this.lineTo(m+i,j);this.lineTo(m+i,j+p);this.lineTo(m,j+p);this.closePath();this.fill();this.currentPath_=Z};q.createLinearGradient=function(j,p,i,m){var Z=new U("gradient");Z.x0_=j;Z.y0_=p;Z.x1_=i;Z.y1_=m;return Z};q.createRadialGradient=function(p,ag,m,j,Z,i){var ah=new U("gradientradial");ah.x0_=p;ah.y0_=ag;ah.r0_=m;ah.x1_=j;ah.y1_=Z;ah.r1_=i;return ah};q.drawImage=function(an,j){var ah,Z,aj,ar,al,ak,ao,av;var ai=an.runtimeStyle.width;var am=an.runtimeStyle.height;an.runtimeStyle.width="auto";an.runtimeStyle.height="auto";var ag=an.width;var aq=an.height;an.runtimeStyle.width=ai;an.runtimeStyle.height=am;if(arguments.length==3){ah=arguments[1];Z=arguments[2];al=ak=0;ao=aj=ag;av=ar=aq}else{if(arguments.length==5){ah=arguments[1];Z=arguments[2];aj=arguments[3];ar=arguments[4];al=ak=0;ao=ag;av=aq}else{if(arguments.length==9){al=arguments[1];ak=arguments[2];ao=arguments[3];av=arguments[4];ah=arguments[5];Z=arguments[6];aj=arguments[7];ar=arguments[8]}else{throw Error("Invalid number of arguments")}}}var au=V(this,ah,Z);var at=[];var i=10;var p=10;var ap=this.m_;at.push(" <g_vml_:group",' coordsize="',d*i,",",d*p,'"',' coordorigin="0,0"',' style="width:',n(i*ap[0][0]),"px;height:",n(p*ap[1][1]),"px;position:absolute;","top:",n(au.y/d),"px;left:",n(au.x/d),"px; rotation:",n(Math.atan(ap[0][1]/ap[1][1])*180/Math.PI),";");at.push('" >','<g_vml_:image src="',an.src,'"',' style="width:',d*aj,"px;"," height:",d*ar,'px"',' cropleft="',al/ag,'"',' croptop="',ak/aq,'"',' cropright="',(ag-al-ao)/ag,'"',' cropbottom="',(aq-ak-av)/aq,'"'," />","</g_vml_:group>");this.element_.insertAdjacentHTML("BeforeEnd",at.join(""))};q.setLineDash=function(i){if(i.length===1){i=i.slice();i[1]=i[0]}this.lineDash=i};q.getLineDash=function(){return this.lineDash};q.stroke=function(ak){var ai=[];var m=10;var al=10;ai.push("<g_vml_:shape",' filled="',!!ak,'"',' style="position:absolute;width:',m,"px;height:",al,'px;left:0px;top:0px;"',' coordorigin="0,0"',' coordsize="',d*m,",",d*al,'"',' stroked="',!ak,'"',' path="');var Z={x:null,y:null};var aj={x:null,y:null};for(var ag=0;ag<this.currentPath_.length;ag++){var j=this.currentPath_[ag];var ah;switch(j.type){case"moveTo":ah=j;ai.push(" m ",n(j.x),",",n(j.y));break;case"lineTo":ai.push(" l ",n(j.x),",",n(j.y));break;case"close":ai.push(" x ");j=null;break;case"bezierCurveTo":ai.push(" c ",n(j.cp1x),",",n(j.cp1y),",",n(j.cp2x),",",n(j.cp2y),",",n(j.x),",",n(j.y));break;case"at":case"wa":ai.push(" ",j.type," ",n(j.x-this.arcScaleX_*j.radius),",",n(j.y-this.arcScaleY_*j.radius)," ",n(j.x+this.arcScaleX_*j.radius),",",n(j.y+this.arcScaleY_*j.radius)," ",n(j.xStart),",",n(j.yStart)," ",n(j.xEnd),",",n(j.yEnd));break}if(j){if(Z.x==null||j.x<Z.x){Z.x=j.x}if(aj.x==null||j.x>aj.x){aj.x=j.x}if(Z.y==null||j.y<Z.y){Z.y=j.y}if(aj.y==null||j.y>aj.y){aj.y=j.y}}}ai.push(' ">');if(!ak){w(this,ai)}else{G(this,ai,Z,aj)}ai.push("</g_vml_:shape>");this.element_.insertAdjacentHTML("beforeEnd",ai.join(""))};function w(m,ag){var j=F(m.strokeStyle);var p=j.color;var Z=j.alpha*m.globalAlpha;var i=m.lineScale_*m.lineWidth;if(i<1){Z*=i}ag.push("<g_vml_:stroke",' opacity="',Z,'"',' joinstyle="',m.lineJoin,'"',' dashstyle="',m.lineDash.join(" "),'"',' miterlimit="',m.miterLimit,'"',' endcap="',S(m.lineCap),'"',' weight="',i,'px"',' color="',p,'" />')}function G(aq,ai,aK,ar){var aj=aq.fillStyle;var aB=aq.arcScaleX_;var aA=aq.arcScaleY_;var j=ar.x-aK.x;var p=ar.y-aK.y;if(aj instanceof U){var an=0;var aF={x:0,y:0};var ax=0;var am=1;if(aj.type_=="gradient"){var al=aj.x0_/aB;var m=aj.y0_/aA;var ak=aj.x1_/aB;var aM=aj.y1_/aA;var aJ=V(aq,al,m);var aI=V(aq,ak,aM);var ag=aI.x-aJ.x;var Z=aI.y-aJ.y;an=Math.atan2(ag,Z)*180/Math.PI;if(an<0){an+=360}if(an<0.000001){an=0}}else{var aJ=V(aq,aj.x0_,aj.y0_);aF={x:(aJ.x-aK.x)/j,y:(aJ.y-aK.y)/p};j/=aB*d;p/=aA*d;var aD=ab.max(j,p);ax=2*aj.r0_/aD;am=2*aj.r1_/aD-ax}var av=aj.colors_;av.sort(function(aN,i){return aN.offset-i.offset});var ap=av.length;var au=av[0].color;var at=av[ap-1].color;var az=av[0].alpha*aq.globalAlpha;var ay=av[ap-1].alpha*aq.globalAlpha;var aE=[];for(var aH=0;aH<ap;aH++){var ao=av[aH];aE.push(ao.offset*am+ax+" "+ao.color)}ai.push('<g_vml_:fill type="',aj.type_,'"',' method="none" focus="100%"',' color="',au,'"',' color2="',at,'"',' colors="',aE.join(","),'"',' opacity="',ay,'"',' g_o_:opacity2="',az,'"',' angle="',an,'"',' focusposition="',aF.x,",",aF.y,'" />')}else{if(aj instanceof T){if(j&&p){var ah=-aK.x;var aC=-aK.y;ai.push("<g_vml_:fill",' position="',ah/j*aB*aB,",",aC/p*aA*aA,'"',' type="tile"',' src="',aj.src_,'" />')}}else{var aL=F(aq.fillStyle);var aw=aL.color;var aG=aL.alpha*aq.globalAlpha;ai.push('<g_vml_:fill color="',aw,'" opacity="',aG,'" />')}}}q.fill=function(){this.$stroke(true)};q.closePath=function(){this.currentPath_.push({type:"close"})};function V(j,Z,p){var i=j.m_;return{x:d*(Z*i[0][0]+p*i[1][0]+i[2][0])-f,y:d*(Z*i[0][1]+p*i[1][1]+i[2][1])-f}}q.save=function(){var i={};v(this,i);this.aStack_.push(i);this.mStack_.push(this.m_);this.m_=J(B(),this.m_)};q.restore=function(){if(this.aStack_.length){v(this.aStack_.pop(),this);this.m_=this.mStack_.pop()}};function h(i){return isFinite(i[0][0])&&isFinite(i[0][1])&&isFinite(i[1][0])&&isFinite(i[1][1])&&isFinite(i[2][0])&&isFinite(i[2][1])}function aa(j,i,p){if(!h(i)){return}j.m_=i;if(p){var Z=i[0][0]*i[1][1]-i[0][1]*i[1][0];j.lineScale_=N(H(Z))}}q.translate=function(m,j){var i=[[1,0,0],[0,1,0],[m,j,1]];aa(this,J(i,this.m_),false)};q.rotate=function(j){var p=A(j);var m=l(j);var i=[[p,m,0],[-m,p,0],[0,0,1]];aa(this,J(i,this.m_),false)};q.scale=function(m,j){this.arcScaleX_*=m;this.arcScaleY_*=j;var i=[[m,0,0],[0,j,0],[0,0,1]];aa(this,J(i,this.m_),true)};q.transform=function(Z,p,ah,ag,j,i){var m=[[Z,p,0],[ah,ag,0],[j,i,1]];aa(this,J(m,this.m_),true)};q.setTransform=function(ag,Z,ai,ah,p,j){var i=[[ag,Z,0],[ai,ah,0],[p,j,1]];aa(this,i,true)};q.drawText_=function(am,ak,aj,ap,ai){var ao=this.m_,at=1000,j=0,ar=at,ah={x:0,y:0},ag=[];var i=u(E(this.font),this.element_);var p=ac(i);var au=this.element_.currentStyle;var Z=this.textAlign.toLowerCase();switch(Z){case"left":case"center":case"right":break;case"end":Z=au.direction=="ltr"?"right":"left";break;case"start":Z=au.direction=="rtl"?"right":"left";break;default:Z="left"}switch(this.textBaseline){case"hanging":case"top":ah.y=i.size/1.75;break;case"middle":break;default:case null:case"alphabetic":case"ideographic":case"bottom":ah.y=-i.size/3;break}switch(Z){case"right":j=at;ar=0.05;break;case"center":j=ar=at/2;break}var aq=V(this,ak+ah.x,aj+ah.y);ag.push('<g_vml_:line from="',-j,' 0" to="',ar,' 0.05" ',' coordsize="100 100" coordorigin="0 0"',' filled="',!ai,'" stroked="',!!ai,'" style="position:absolute;width:1px;height:1px;left:0px;top:0px;">');if(ai){w(this,ag)}else{G(this,ag,{x:-j,y:0},{x:ar,y:i.size})}var an=ao[0][0].toFixed(3)+","+ao[1][0].toFixed(3)+","+ao[0][1].toFixed(3)+","+ao[1][1].toFixed(3)+",0,0";var al=n(aq.x/d)+","+n(aq.y/d);ag.push('<g_vml_:skew on="t" matrix="',an,'" ',' offset="',al,'" origin="',j,' 0" />','<g_vml_:path textpathok="true" />','<g_vml_:textpath on="true" string="',af(am),'" style="v-text-align:',Z,";font:",af(p),'" /></g_vml_:line>');this.element_.insertAdjacentHTML("beforeEnd",ag.join(""))};q.fillText=function(m,i,p,j){this.drawText_(m,i,p,j,false)};q.strokeText=function(m,i,p,j){this.drawText_(m,i,p,j,true)};q.measureText=function(m){if(!this.textMeasureEl_){var i='<span style="position:absolute;top:-20000px;left:0;padding:0;margin:0;border:none;white-space:pre;"></span>';this.element_.insertAdjacentHTML("beforeEnd",i);this.textMeasureEl_=this.element_.lastChild}var j=this.element_.ownerDocument;this.textMeasureEl_.innerHTML="";this.textMeasureEl_.style.font=this.font;this.textMeasureEl_.appendChild(j.createTextNode(m));return{width:this.textMeasureEl_.offsetWidth}};q.clip=function(){};q.arcTo=function(){};q.createPattern=function(j,i){return new T(j,i)};function U(i){this.type_=i;this.x0_=0;this.y0_=0;this.r0_=0;this.x1_=0;this.y1_=0;this.r1_=0;this.colors_=[]}U.prototype.addColorStop=function(j,i){i=F(i);this.colors_.push({offset:j,color:i.color,alpha:i.alpha})};function T(j,i){Q(j);switch(i){case"repeat":case null:case"":this.repetition_="repeat";break;case"repeat-x":case"repeat-y":case"no-repeat":this.repetition_=i;break;default:O("SYNTAX_ERR")}this.src_=j.src;this.width_=j.width;this.height_=j.height}function O(i){throw new P(i)}function Q(i){if(!i||i.nodeType!=1||i.tagName!="IMG"){O("TYPE_MISMATCH_ERR")}if(i.readyState!="complete"){O("INVALID_STATE_ERR")}}function P(i){this.code=this[i];this.message=i+": DOM Exception "+this.code}var X=P.prototype=new Error();X.INDEX_SIZE_ERR=1;X.DOMSTRING_SIZE_ERR=2;X.HIERARCHY_REQUEST_ERR=3;X.WRONG_DOCUMENT_ERR=4;X.INVALID_CHARACTER_ERR=5;X.NO_DATA_ALLOWED_ERR=6;X.NO_MODIFICATION_ALLOWED_ERR=7;X.NOT_FOUND_ERR=8;X.NOT_SUPPORTED_ERR=9;X.INUSE_ATTRIBUTE_ERR=10;X.INVALID_STATE_ERR=11;X.SYNTAX_ERR=12;X.INVALID_MODIFICATION_ERR=13;X.NAMESPACE_ERR=14;X.INVALID_ACCESS_ERR=15;X.VALIDATION_ERR=16;X.TYPE_MISMATCH_ERR=17;G_vmlCanvasManager=e;CanvasRenderingContext2D=D;CanvasGradient=U;CanvasPattern=T;DOMException=P})()}Ext.define("Ext.draw.engine.Canvas",{extend:Ext.draw.Surface,config:{highPrecision:false},statics:{contextOverrides:{setGradientBBox:function(a){this.bbox=a},fill:function(){var c=this.fillStyle,a=this.fillGradient,b=this.fillOpacity,d=this.globalAlpha,e=this.bbox;if(c!==Ext.draw.Color.RGBA_NONE&&b!==0){if(a&&e){this.fillStyle=a.generateGradient(this,e)}if(b!==1){this.globalAlpha=d*b}this.$fill();if(b!==1){this.globalAlpha=d}if(a&&e){this.fillStyle=c}}},stroke:function(){var e=this.strokeStyle,c=this.strokeGradient,a=this.strokeOpacity,b=this.globalAlpha,d=this.bbox;if(e!==Ext.draw.Color.RGBA_NONE&&a!==0){if(c&&d){this.strokeStyle=c.generateGradient(this,d)}if(a!==1){this.globalAlpha=b*a}this.$stroke();if(a!==1){this.globalAlpha=b}if(c&&d){this.strokeStyle=e}}},fillStroke:function(d,e){var j=this,i=this.fillStyle,h=this.fillOpacity,f=this.strokeStyle,c=this.strokeOpacity,b=j.shadowColor,a=j.shadowBlur,g=Ext.draw.Color.RGBA_NONE;if(e===undefined){e=d.transformFillStroke}if(!e){d.inverseMatrix.toContext(j)}if(i!==g&&h!==0){j.fill();j.shadowColor=g;j.shadowBlur=0}if(f!==g&&c!==0){j.stroke()}j.shadowColor=b;j.shadowBlur=a},setLineDash:function(a){if(this.$setLineDash){this.$setLineDash(a)}},getLineDash:function(){if(this.$getLineDash){return this.$getLineDash()}},ellipse:function(g,e,c,a,j,b,f,d){var i=Math.cos(j),h=Math.sin(j);this.transform(i*c,h*c,-h*a,i*a,g,e);this.arc(0,0,1,b,f,d);this.transform(i/c,-h/a,h/c,i/a,-(i*g+h*e)/c,(h*g-i*e)/a)},appendPath:function(f){var e=this,c=0,b=0,a=f.commands,g=f.params,d=a.length;e.beginPath();for(;c<d;c++){switch(a[c]){case"M":e.moveTo(g[b],g[b+1]);b+=2;break;case"L":e.lineTo(g[b],g[b+1]);b+=2;break;case"C":e.bezierCurveTo(g[b],g[b+1],g[b+2],g[b+3],g[b+4],g[b+5]);b+=6;break;case"Z":e.closePath();break}}},save:function(){var c=this.toSave,d=c.length,e=d&&{},b=0,a;for(;b<d;b++){a=c[b];if(a in this){e[a]=this[a]}}this.state.push(e);this.$save()},restore:function(){var b=this.state.pop(),a;if(b){for(a in b){this[a]=b[a]}}this.$restore()}}},splitThreshold:3000,toSave:["fillGradient","strokeGradient"],element:{reference:"element",style:{position:"absolute"},children:[{reference:"innerElement",style:{width:"100%",height:"100%",position:"relative"}}]},createCanvas:function(){var c=Ext.Element.create({tag:"canvas",cls:Ext.baseCSSPrefix+"surface-canvas"});window.G_vmlCanvasManager&&G_vmlCanvasManager.initElement(c.dom);var d=Ext.draw.engine.Canvas.contextOverrides,a=c.dom.getContext("2d"),b;if(a.ellipse){delete d.ellipse}a.state=[];a.toSave=this.toSave;for(b in d){a["$"+b]=a[b]}Ext.apply(a,d);if(this.getHighPrecision()){this.enablePrecisionCompensation(a)}else{this.disablePrecisionCompensation(a)}this.innerElement.appendChild(c);this.canvases.push(c);this.contexts.push(a)},updateHighPrecision:function(d){var e=this.contexts,c=e.length,b,a;for(b=0;b<c;b++){a=e[b];if(d){this.enablePrecisionCompensation(a)}else{this.disablePrecisionCompensation(a)}}},precisionNames:["rect","fillRect","strokeRect","clearRect","moveTo","lineTo","arc","arcTo","save","restore","updatePrecisionCompensate","setTransform","transform","scale","translate","rotate","quadraticCurveTo","bezierCurveTo","createLinearGradient","createRadialGradient","fillText","strokeText","drawImage"],disablePrecisionCompensation:function(b){var a=Ext.draw.engine.Canvas.contextOverrides,f=this.precisionNames,e=f.length,d,c;for(d=0;d<e;d++){c=f[d];if(!(c in a)){delete b[c]}}this.setDirty(true)},enablePrecisionCompensation:function(j){var c=this,a=1,g=1,l=0,k=0,i=new Ext.draw.Matrix(),b=[],e={},d=Ext.draw.engine.Canvas.contextOverrides,h=j.constructor.prototype;var f={toSave:c.toSave,rect:function(m,p,n,o){return h.rect.call(this,m*a+l,p*g+k,n*a,o*g)},fillRect:function(m,p,n,o){this.updatePrecisionCompensateRect();h.fillRect.call(this,m*a+l,p*g+k,n*a,o*g);this.updatePrecisionCompensate()},strokeRect:function(m,p,n,o){this.updatePrecisionCompensateRect();h.strokeRect.call(this,m*a+l,p*g+k,n*a,o*g);this.updatePrecisionCompensate()},clearRect:function(m,p,n,o){return h.clearRect.call(this,m*a+l,p*g+k,n*a,o*g)},moveTo:function(m,n){return h.moveTo.call(this,m*a+l,n*g+k)},lineTo:function(m,n){return h.lineTo.call(this,m*a+l,n*g+k)},arc:function(n,r,m,p,o,q){this.updatePrecisionCompensateRect();h.arc.call(this,n*a+l,r*a+k,m*a,p,o,q);this.updatePrecisionCompensate()},arcTo:function(o,q,n,p,m){this.updatePrecisionCompensateRect();h.arcTo.call(this,o*a+l,q*g+k,n*a+l,p*g+k,m*a);this.updatePrecisionCompensate()},save:function(){b.push(i);i=i.clone();d.save.call(this);h.save.call(this)},restore:function(){i=b.pop();d.restore.call(this);h.restore.call(this);this.updatePrecisionCompensate()},updatePrecisionCompensate:function(){i.precisionCompensate(c.devicePixelRatio,e);a=e.xx;g=e.yy;l=e.dx;k=e.dy;h.setTransform.call(this,c.devicePixelRatio,e.b,e.c,e.d,0,0)},updatePrecisionCompensateRect:function(){i.precisionCompensateRect(c.devicePixelRatio,e);a=e.xx;g=e.yy;l=e.dx;k=e.dy;h.setTransform.call(this,c.devicePixelRatio,e.b,e.c,e.d,0,0)},setTransform:function(q,o,n,m,r,p){i.set(q,o,n,m,r,p);this.updatePrecisionCompensate()},transform:function(q,o,n,m,r,p){i.append(q,o,n,m,r,p);this.updatePrecisionCompensate()},scale:function(n,m){this.transform(n,0,0,m,0,0)},translate:function(n,m){this.transform(1,0,0,1,n,m)},rotate:function(o){var n=Math.cos(o),m=Math.sin(o);this.transform(n,m,-m,n,0,0)},quadraticCurveTo:function(n,p,m,o){h.quadraticCurveTo.call(this,n*a+l,p*g+k,m*a+l,o*g+k)},bezierCurveTo:function(r,p,o,n,m,q){h.bezierCurveTo.call(this,r*a+l,p*g+k,o*a+l,n*g+k,m*a+l,q*g+k)},createLinearGradient:function(n,p,m,o){this.updatePrecisionCompensateRect();var q=h.createLinearGradient.call(this,n*a+l,p*g+k,m*a+l,o*g+k);this.updatePrecisionCompensate();return q},createRadialGradient:function(p,r,o,n,q,m){this.updatePrecisionCompensateRect();var s=h.createLinearGradient.call(this,p*a+l,r*a+k,o*a,n*a+l,q*a+k,m*a);this.updatePrecisionCompensate();return s},fillText:function(o,m,p,n){h.setTransform.apply(this,i.elements);if(typeof n==="undefined"){h.fillText.call(this,o,m,p)}else{h.fillText.call(this,o,m,p,n)}this.updatePrecisionCompensate()},strokeText:function(o,m,p,n){h.setTransform.apply(this,i.elements);if(typeof n==="undefined"){h.strokeText.call(this,o,m,p)}else{h.strokeText.call(this,o,m,p,n)}this.updatePrecisionCompensate()},fill:function(){var m=this.fillGradient,n=this.bbox;this.updatePrecisionCompensateRect();if(m&&n){this.fillStyle=m.generateGradient(this,n)}h.fill.call(this);this.updatePrecisionCompensate()},stroke:function(){var m=this.strokeGradient,n=this.bbox;this.updatePrecisionCompensateRect();if(m&&n){this.strokeStyle=m.generateGradient(this,n)}h.stroke.call(this);this.updatePrecisionCompensate()},drawImage:function(u,s,r,q,p,o,n,m,t){switch(arguments.length){case 3:return h.drawImage.call(this,u,s*a+l,r*g+k);case 5:return h.drawImage.call(this,u,s*a+l,r*g+k,q*a,p*g);case 9:return h.drawImage.call(this,u,s,r,q,p,o*a+l,n*g*k,m*a,t*g)}}};Ext.apply(j,f);this.setDirty(true)},updateRect:function(a){this.callParent([a]);var C=this,p=Math.floor(a[0]),e=Math.floor(a[1]),g=Math.ceil(a[0]+a[2]),B=Math.ceil(a[1]+a[3]),u=C.devicePixelRatio,D=C.canvases,d=g-p,y=B-e,n=Math.round(C.splitThreshold/u),c=C.xSplits=Math.ceil(d/n),f=C.ySplits=Math.ceil(y/n),v,s,q,A,z,x,o,m;for(s=0,z=0;s<f;s++,z+=n){for(v=0,A=0;v<c;v++,A+=n){q=s*c+v;if(q>=D.length){C.createCanvas()}x=D[q].dom;x.style.left=A+"px";x.style.top=z+"px";m=Math.min(n,y-z);if(m*u!==x.height){x.height=m*u;x.style.height=m+"px"}o=Math.min(n,d-A);if(o*u!==x.width){x.width=o*u;x.style.width=o+"px"}C.applyDefaults(C.contexts[q])}}for(q+=1;q<D.length;q++){D[q].destroy()}C.activeCanvases=c*f;D.length=C.activeCanvases;C.clear()},clearTransform:function(){var f=this,a=f.xSplits,g=f.ySplits,d=f.contexts,h=f.splitThreshold,l=f.devicePixelRatio,e,c,b,m;for(e=0;e<a;e++){for(c=0;c<g;c++){b=c*a+e;m=d[b];m.translate(-h*e,-h*c);m.scale(l,l);f.matrix.toContext(m)}}},renderSprite:function(q){var C=this,b=C.getRect(),e=C.matrix,g=q.getParent(),v=Ext.draw.Matrix.fly([1,0,0,1,0,0]),p=C.splitThreshold/C.devicePixelRatio,c=C.xSplits,m=C.ySplits,A,z,s,a,r,o,d=0,B,n=0,f,l=b[2],y=b[3],x,u,t;while(g&&(g!==C)){v.prependMatrix(g.matrix||g.attr&&g.attr.matrix);g=g.getParent()}v.prependMatrix(e);a=q.getBBox();if(a){a=v.transformBBox(a)}q.preRender(C);if(q.attr.hidden||q.attr.globalAlpha===0){q.setDirty(false);return}for(u=0,z=0;u<m;u++,z+=p){for(x=0,A=0;x<c;x++,A+=p){t=u*c+x;s=C.contexts[t];r=Math.min(p,l-A);o=Math.min(p,y-z);d=A;B=d+r;n=z;f=n+o;if(a){if(a.x>B||a.x+a.width<d||a.y>f||a.y+a.height<n){continue}}s.save();q.useAttributes(s,b);if(false===q.render(C,s,[d,n,r,o],b)){return false}s.restore()}}q.setDirty(false)},flatten:function(n,a){var k=document.createElement("canvas"),f=Ext.getClassName(this),g=this.devicePixelRatio,l=k.getContext("2d"),b,c,h,e,d,m;k.width=Math.ceil(n.width*g);k.height=Math.ceil(n.height*g);for(e=0;e<a.length;e++){b=a[e];if(Ext.getClassName(b)!==f){continue}h=b.getRect();for(d=0;d<b.canvases.length;d++){c=b.canvases[d];m=c.getOffsetsTo(c.getParent());l.drawImage(c.dom,(h[0]+m[0])*g,(h[1]+m[1])*g)}}return{data:k.toDataURL(),type:"png"}},applyDefaults:function(a){var b=Ext.draw.Color.RGBA_NONE;a.strokeStyle=b;a.fillStyle=b;a.textAlign="start";a.textBaseline="alphabetic";a.miterLimit=1},clear:function(){var d=this,e=d.activeCanvases,c,b,a;for(c=0;c<e;c++){b=d.canvases[c].dom;a=d.contexts[c];a.setTransform(1,0,0,1,0,0);a.clearRect(0,0,b.width,b.height)}d.setDirty(true)},destroy:function(){var c=this,a,b=c.canvases.length;for(a=0;a<b;a++){c.contexts[a]=null;c.canvases[a].destroy();c.canvases[a]=null}delete c.contexts;delete c.canvases;c.callParent()},privates:{initElement:function(){var a=this;a.callParent();a.canvases=[];a.contexts=[];a.activeCanvases=(a.xSplits=0)*(a.ySplits=0)}}},function(){var c=this,b=c.prototype,a=10000000000;if(Ext.os.is.Android4&&Ext.browser.is.Chrome){a=3000}else{if(Ext.is.iOS){a=2200}}b.splitThreshold=a});Ext.define("Ext.draw.Container",{extend:Ext.draw.ContainerBase,alternateClassName:"Ext.draw.Component",xtype:"draw",defaultType:"surface",isDrawContainer:true,engine:"Ext.draw.engine.Canvas",config:{cls:Ext.baseCSSPrefix+"draw-container",resizeHandler:null,sprites:null,gradients:[]},defaultDownloadServerUrl:"http://svg.sencha.io",supportedFormats:["png","pdf","jpeg","gif"],supportedOptions:{version:Ext.isNumber,data:Ext.isString,format:function(a){return Ext.Array.indexOf(this.supportedFormats,a)>=0},filename:Ext.isString,width:Ext.isNumber,height:Ext.isNumber,scale:Ext.isNumber,pdf:Ext.isObject,jpeg:Ext.isObject},initAnimator:function(){this.frameCallbackId=Ext.draw.Animator.addFrameCallback("renderFrame",this)},applyGradients:function(b){var a=[],c,f,d,e;if(!Ext.isArray(b)){return a}for(c=0,f=b.length;c<f;c++){d=b[c];if(!Ext.isObject(d)){continue}if(typeof d.type!=="string"){d.type="linear"}if(d.angle){d.degrees=d.angle;delete d.angle}if(Ext.isObject(d.stops)){d.stops=(function(i){var g=[],h;for(e in i){h=i[e];h.offset=e/100;g.push(h)}return g})(d.stops)}a.push(d)}Ext.draw.gradient.GradientDefinition.add(a);return a},applySprites:function(f){if(!f){return}f=Ext.Array.from(f);var e=f.length,b=[],d,a,c;for(d=0;d<e;d++){c=f[d];a=c.surface;if(!(a&&a.isSurface)){if(Ext.isString(a)){a=this.getSurface(a)}else{a=this.getSurface("main")}}c=a.add(c);b.push(c)}return b},onBodyResize:function(){var b=this.element,a;if(!b){return}a=b.getSize();if(a.width&&a.height){this.setBodySize(a)}},setBodySize:function(c){var d=this,b=d.getResizeHandler()||d.defaultResizeHandler,a;d.fireEvent("bodyresize",d,c);a=b.call(d,c);if(a!==false){d.renderFrame()}},defaultResizeHandler:function(a){this.getItems().each(function(b){b.setRect([0,0,a.width,a.height])})},getSurface:function(d){d=this.getId()+"-"+(d||"main");var c=this,b=c.getItems(),a=b.get(d);if(!a){a=c.add({xclass:c.engine,id:d});c.onBodyResize()}return a},renderFrame:function(){var e=this,a=e.getItems(),b,d,c;for(b=0,d=a.length;b<d;b++){c=a.items[b];if(c.isSurface){c.renderFrame()}}},getImage:function(k){var l=this.innerElement.getSize(),a=Array.prototype.slice.call(this.items.items),d,g,c=this.surfaceZIndexes,f,e,b,h;for(e=1;e<a.length;e++){b=a[e];h=c[b.type];f=e-1;while(f>=0&&c[a[f].type]>h){a[f+1]=a[f];f--}a[f+1]=b}d=a[0].flatten(l,a);if(k==="image"){g=new Image();g.src=d.data;d.data=g;return d}if(k==="stream"){d.data=d.data.replace(/^data:image\/[^;]+/,"data:application/octet-stream");return d}return d},download:function(d){var e=this,a=[],b,c,f;d=Ext.apply({version:2,data:e.getImage().data},d);for(c in d){if(d.hasOwnProperty(c)){f=d[c];if(c in e.supportedOptions){if(e.supportedOptions[c].call(e,f)){a.push({tag:"input",type:"hidden",name:c,value:Ext.String.htmlEncode(Ext.isObject(f)?Ext.JSON.encode(f):f)})}}}}b=Ext.dom.Helper.markup({tag:"html",children:[{tag:"head"},{tag:"body",children:[{tag:"form",method:"POST",action:d.url||e.defaultDownloadServerUrl,children:a},{tag:"script",type:"text/javascript",children:'document.getElementsByTagName("form")[0].submit();'}]}]});window.open("","ImageDownload_"+Date.now()).document.write(b)},destroy:function(){var a=this.frameCallbackId;if(a){Ext.draw.Animator.removeFrameCallback(a)}this.callParent()}},function(){if(location.search.match("svg")){Ext.draw.Container.prototype.engine="Ext.draw.engine.Svg"}else{if((Ext.os.is.BlackBerry&&Ext.os.version.getMajor()===10)||(Ext.browser.is.AndroidStock4&&(Ext.os.version.getMinor()===1||Ext.os.version.getMinor()===2||Ext.os.version.getMinor()===3))){Ext.draw.Container.prototype.engine="Ext.draw.engine.Svg"}}});Ext.define("Ext.chart.theme.Base",{mixins:{factoryable:Ext.mixin.Factoryable},factoryConfig:{type:"chart.theme"},isTheme:true,config:{baseColor:null,colors:undefined,gradients:null,chart:{defaults:{background:"white"}},axis:{defaults:{label:{x:0,y:0,textBaseline:"middle",textAlign:"center",fontSize:"default",fontFamily:"default",fontWeight:"default",fillStyle:"black"},title:{fillStyle:"black",fontSize:"default*1.23",fontFamily:"default",fontWeight:"default"},style:{strokeStyle:"black"},grid:{strokeStyle:"rgb(221, 221, 221)"}},top:{style:{textPadding:5}},bottom:{style:{textPadding:5}}},series:{defaults:{label:{fillStyle:"black",strokeStyle:"none",fontFamily:"default",fontWeight:"default",fontSize:"default*1.077",textBaseline:"middle",textAlign:"center"},labelOverflowPadding:5}},sprites:{text:{fontSize:"default",fontWeight:"default",fontFamily:"default",fillStyle:"black"}},seriesThemes:undefined,markerThemes:{type:["circle","cross","plus","square","triangle","diamond"]},useGradients:false,background:null},colorDefaults:["#94ae0a","#115fa6","#a61120","#ff8809","#ffd13e","#a61187","#24ad9a","#7c7474","#a66111"],constructor:function(a){this.initConfig(a);this.resolveDefaults()},defaultRegEx:/^default([+\-/\*]\d+(?:\.\d+)?)?$/,defaultOperators:{"*":function(b,a){return b*a},"+":function(b,a){return b+a},"-":function(b,a){return b-a}},resolveDefaults:function(){var a=this;Ext.onReady(function(){var f=Ext.clone(a.getSprites()),e=Ext.clone(a.getAxis()),d=Ext.clone(a.getSeries()),g,c,b;if(!a.superclass.defaults){g=Ext.getBody().createChild({tag:"div",cls:"x-component"});a.superclass.defaults={fontFamily:g.getStyle("fontFamily"),fontWeight:g.getStyle("fontWeight"),fontSize:parseFloat(g.getStyle("fontSize")),fontVariant:g.getStyle("fontVariant"),fontStyle:g.getStyle("fontStyle")};g.destroy()}a.replaceDefaults(f.text);a.setSprites(f);for(c in e){b=e[c];a.replaceDefaults(b.label);a.replaceDefaults(b.title)}a.setAxis(e);for(c in d){b=d[c];a.replaceDefaults(b.label)}a.setSeries(d)})},replaceDefaults:function(h){var e=this,g=e.superclass.defaults,a=e.defaultRegEx,d,f,c,b;if(Ext.isObject(h)){for(d in g){c=a.exec(h[d]);if(c){f=g[d];c=c[1];if(c){b=e.defaultOperators[c.charAt(0)];f=Math.round(b(f,parseFloat(c.substr(1))))}h[d]=f}}}},applyBaseColor:function(c){var a,b;if(c){a=c.isColor?c:Ext.draw.Color.fromString(c);b=a.getHSL()[2];if(b<0.15){a=a.createLighter(0.3)}else{if(b<0.3){a=a.createLighter(0.15)}else{if(b>0.85){a=a.createDarker(0.3)}else{if(b>0.7){a=a.createDarker(0.15)}}}}this.setColors([a.createDarker(0.3).toString(),a.createDarker(0.15).toString(),a.toString(),a.createLighter(0.12).toString(),a.createLighter(0.24).toString(),a.createLighter(0.31).toString()])}return c},applyColors:function(a){return a||this.colorDefaults},updateUseGradients:function(a){if(a){this.updateGradients({type:"linear",degrees:90})}},updateBackground:function(a){if(a){var b=this.getChart();b.defaults.background=a;this.setChart(b)}},updateGradients:function(a){var c=this.getColors(),e=[],h,b,d,f,g;if(Ext.isObject(a)){for(f=0,g=c&&c.length||0;f<g;f++){b=Ext.draw.Color.fromString(c[f]);if(b){d=b.createLighter(0.15).toString();h=Ext.apply(Ext.Object.chain(a),{stops:[{offset:1,color:b.toString()},{offset:0,color:d.toString()}]});e.push(h)}}this.setColors(e)}},applySeriesThemes:function(a){this.getBaseColor();this.getUseGradients();this.getGradients();var b=this.getColors();if(!a){a={fillStyle:Ext.Array.clone(b),strokeStyle:Ext.Array.map(b,function(d){var c=Ext.draw.Color.fromString(d.stops?d.stops[0].color:d);return c.createDarker(0.15).toString()})}}return a}});Ext.define("Ext.chart.theme.Default",{extend:Ext.chart.theme.Base,singleton:true,alias:["chart.theme.default","chart.theme.Base"]});Ext.define("Ext.chart.Markers",{extend:Ext.draw.sprite.Instancing,isMarkers:true,defaultCategory:"default",constructor:function(){this.callParent(arguments);this.categories={};this.revisions={}},destroy:function(){this.categories=null;this.revisions=null;this.callParent()},getMarkerFor:function(b,a){if(b in this.categories){var c=this.categories[b];if(a in c){return this.get(c[a])}}},clear:function(a){a=a||this.defaultCategory;if(!(a in this.revisions)){this.revisions[a]=1}else{this.revisions[a]++}},putMarkerFor:function(e,b,c,h,f){e=e||this.defaultCategory;var d=this,g=d.categories[e]||(d.categories[e]={}),a;if(c in g){d.setAttributesFor(g[c],b,h)}else{g[c]=d.getCount();d.createInstance(b,h)}a=d.get(g[c]);if(a){a.category=e;if(!f){a.revision=d.revisions[e]||(d.revisions[e]=1)}}},getMarkerBBoxFor:function(c,a,b){if(c in this.categories){var d=this.categories[c];if(a in d){return this.getBBoxFor(d[a],b)}}},getBBox:function(){return null},render:function(a,l,b){var f=this,k=f.revisions,j=f.attr.matrix,h=f.getTemplate(),d=h.attr,g,c,e;j.toContext(l);h.preRender(a,l,b);h.useAttributes(l,b);for(c=0,e=f.instances.length;c<e;c++){g=f.get(c);if(g.hidden||g.revision!==k[g.category]){continue}l.save();h.attr=g;h.useAttributes(l,b);h.render(a,l,b);l.restore()}h.attr=d}});Ext.define("Ext.chart.label.Callout",{extend:Ext.draw.modifier.Modifier,prepareAttributes:function(a){if(!a.hasOwnProperty("calloutOriginal")){a.calloutOriginal=Ext.Object.chain(a);a.calloutOriginal.prototype=a}if(this._previous){this._previous.prepareAttributes(a.calloutOriginal)}},setAttrs:function(e,h){var d=e.callout,i=e.calloutOriginal,l=e.bbox.plain,c=(l.width||0)+e.labelOverflowPadding,m=(l.height||0)+e.labelOverflowPadding,p,o;if("callout" in h){d=h.callout}if("callout" in h||"calloutPlaceX" in h||"calloutPlaceY" in h||"x" in h||"y" in h){var n="rotationRads" in h?i.rotationRads=h.rotationRads:i.rotationRads,g="x" in h?(i.x=h.x):i.x,f="y" in h?(i.y=h.y):i.y,b="calloutPlaceX" in h?h.calloutPlaceX:e.calloutPlaceX,a="calloutPlaceY" in h?h.calloutPlaceY:e.calloutPlaceY,k="calloutVertical" in h?h.calloutVertical:e.calloutVertical,j;n%=Math.PI*2;if(Math.cos(n)<0){n=(n+Math.PI)%(Math.PI*2)}if(n>Math.PI){n-=Math.PI*2}if(k){n=n*(1-d)-Math.PI/2*d;j=c;c=m;m=j}else{n=n*(1-d)}h.rotationRads=n;h.x=g*(1-d)+b*d;h.y=f*(1-d)+a*d;p=b-g;o=a-f;if(Math.abs(o*c)>Math.abs(p*m)){if(o>0){h.calloutEndX=h.x-(m/2)*(p/o)*d;h.calloutEndY=h.y-(m/2)*d}else{h.calloutEndX=h.x+(m/2)*(p/o)*d;h.calloutEndY=h.y+(m/2)*d}}else{if(p>0){h.calloutEndX=h.x-c/2;h.calloutEndY=h.y-(c/2)*(o/p)*d}else{h.calloutEndX=h.x+c/2;h.calloutEndY=h.y+(c/2)*(o/p)*d}}if(h.calloutStartX&&h.calloutStartY){h.calloutHasLine=(p>0&&h.calloutStartX<h.calloutEndX)||(p<=0&&h.calloutStartX>h.calloutEndX)||(o>0&&h.calloutStartY<h.calloutEndY)||(o<=0&&h.calloutStartY>h.calloutEndY)}else{h.calloutHasLine=true}}return h},pushDown:function(a,b){b=this.callParent([a.calloutOriginal,b]);return this.setAttrs(a,b)},popUp:function(a,b){a=a.prototype;b=this.setAttrs(a,b);if(this._next){return this._next.popUp(a,b)}else{return Ext.apply(a,b)}}});Ext.define("Ext.chart.label.Label",{extend:Ext.draw.sprite.Text,inheritableStatics:{def:{processors:{callout:"limited01",calloutHasLine:"bool",calloutPlaceX:"number",calloutPlaceY:"number",calloutStartX:"number",calloutStartY:"number",calloutEndX:"number",calloutEndY:"number",calloutColor:"color",calloutWidth:"number",calloutVertical:"bool",labelOverflowPadding:"number",display:"enums(none,under,over,rotate,insideStart,insideEnd,inside,outside)",orientation:"enums(horizontal,vertical)",renderer:"default"},defaults:{callout:0,calloutHasLine:true,calloutPlaceX:0,calloutPlaceY:0,calloutStartX:0,calloutStartY:0,calloutEndX:0,calloutEndY:0,calloutWidth:1,calloutVertical:false,calloutColor:"black",labelOverflowPadding:5,display:"none",orientation:"",renderer:null},triggers:{callout:"transform",calloutPlaceX:"transform",calloutPlaceY:"transform",labelOverflowPadding:"transform",calloutRotation:"transform",display:"hidden"},updaters:{hidden:function(a){a.hidden=a.display==="none"}}}},config:{fx:{customDurations:{callout:200}},field:null,calloutLine:true},applyCalloutLine:function(a){if(a){return Ext.apply({},a)}},prepareModifiers:function(){this.callParent(arguments);this.calloutModifier=new Ext.chart.label.Callout({sprite:this});this.fx.setNext(this.calloutModifier);this.calloutModifier.setNext(this.topModifier)},render:function(b,c){var e=this,a=e.attr,d=a.calloutColor;c.save();c.globalAlpha*=a.callout;if(c.globalAlpha>0&&a.calloutHasLine){if(d&&d.isGradient){d=d.getStops()[0].color}c.strokeStyle=d;c.fillStyle=d;c.lineWidth=a.calloutWidth;c.beginPath();c.moveTo(e.attr.calloutStartX,e.attr.calloutStartY);c.lineTo(e.attr.calloutEndX,e.attr.calloutEndY);c.stroke();c.beginPath();c.arc(e.attr.calloutStartX,e.attr.calloutStartY,1*a.calloutWidth,0,2*Math.PI,true);c.fill();c.beginPath();c.arc(e.attr.calloutEndX,e.attr.calloutEndY,1*a.calloutWidth,0,2*Math.PI,true);c.fill()}c.restore();Ext.draw.sprite.Text.prototype.render.apply(e,arguments)}});Ext.define("Ext.chart.series.Series",{mixins:[Ext.mixin.Observable,Ext.mixin.Bindable],isSeries:true,defaultBindProperty:"store",type:null,seriesType:"sprite",identifiablePrefix:"ext-line-",observableType:"series",darkerStrokeRatio:0.15,config:{chart:null,title:null,renderer:null,showInLegend:true,triggerAfterDraw:false,style:{},subStyle:{},themeStyle:{},colors:null,useDarkerStrokeColor:true,store:null,label:{},labelOverflowPadding:null,showMarkers:true,marker:null,markerSubStyle:null,itemInstancing:null,background:null,highlightItem:null,surface:null,overlaySurface:null,hidden:false,highlight:false,highlightCfg:{merge:function(a){return a},$value:{fillStyle:"yellow",strokeStyle:"red"}},animation:null,tooltip:null},directions:[],sprites:null,themeColorCount:function(){return 1},isStoreDependantColorCount:false,themeMarkerCount:function(){return 0},getFields:function(f){var e=this,a=[],c,b,d;for(b=0,d=f.length;b<d;b++){c=e["get"+f[b]+"Field"]();if(Ext.isArray(c)){a.push.apply(a,c)}else{a.push(c)}}return a},applyAnimation:function(a,b){if(!a){a={duration:0}}else{if(a===true){a={easing:"easeInOut",duration:500}}}return b?Ext.apply({},a,b):a},getAnimation:function(){var a=this.getChart();if(a&&a.animationSuspendCount){return{duration:0}}else{return this.callParent()}},updateTitle:function(a){var j=this,g=j.getChart();if(!g||g.isInitializing){return}a=Ext.Array.from(a);var c=g.getSeries(),b=Ext.Array.indexOf(c,j),e=g.getLegendStore(),h=j.getYField(),d,l,k,f;if(e.getCount()&&b!==-1){f=h?Math.min(a.length,h.length):a.length;for(d=0;d<f;d++){k=a[d];l=e.getAt(b+d);if(k&&l){l.set("name",k)}}}},applyHighlight:function(a,b){if(Ext.isObject(a)){a=Ext.merge({},this.config.highlightCfg,a)}else{if(a===true){a=this.config.highlightCfg}}return Ext.apply(b||{},a)},updateHighlight:function(a){this.getStyle();if(!Ext.Object.isEmpty(a)){this.addItemHighlight()}},updateHighlightCfg:function(a){if(!Ext.Object.equals(a,this.defaultConfig.highlightCfg)){this.addItemHighlight()}},applyItemInstancing:function(a,b){return Ext.merge(b||{},a)},setAttributesForItem:function(c,d){var b=c&&c.sprite,a;if(b){if(b.itemsMarker&&c.category==="items"){b.putMarker(c.category,d,c.index,false,true)}if(b.isMarkerHolder&&c.category==="markers"){b.putMarker(c.category,d,c.index,false,true)}else{if(b.isInstancing){b.setAttributesFor(c.index,d)}else{if(Ext.isArray(b)){for(a=0;a<b.length;a++){b[a].setAttributes(d)}}else{b.setAttributes(d)}}}}},getBBoxForItem:function(a){if(a&&a.sprite){if(a.sprite.itemsMarker&&a.category==="items"){return a.sprite.getMarkerBBox(a.category,a.index)}else{if(a.sprite instanceof Ext.draw.sprite.Instancing){return a.sprite.getBBoxFor(a.index)}else{return a.sprite.getBBox()}}}return null},applyHighlightItem:function(d,a){if(d===a){return}if(Ext.isObject(d)&&Ext.isObject(a)){var c=d.sprite===a.sprite,b=d.index===a.index;if(c&&b){return}}return d},updateHighlightItem:function(b,a){this.setAttributesForItem(a,{highlighted:false});this.setAttributesForItem(b,{highlighted:true})},constructor:function(a){var b=this,c;a=a||{};if(a.tips){a=Ext.apply({tooltip:a.tips},a)}if(a.highlightCfg){a=Ext.apply({highlight:a.highlightCfg},a)}if("id" in a){c=a.id}else{if("id" in b.config){c=b.config.id}else{c=b.getId()}}b.setId(c);b.sprites=[];b.dataRange=[];b.mixins.observable.constructor.call(b,a);b.initBindable()},lookupViewModel:function(a){var b=this.getChart();return b?b.lookupViewModel(a):null},applyTooltip:function(c,b){var a=Ext.apply({xtype:"tooltip",renderer:Ext.emptyFn,constrainPosition:true,shrinkWrapDock:true,autoHide:true,offsetX:10,offsetY:10},c);return Ext.create(a)},updateTooltip:function(){this.addItemHighlight()},addItemHighlight:function(){var d=this.getChart();if(!d){return}var e=d.getInteractions(),c,a,b;for(c=0;c<e.length;c++){a=e[c];if(a.isItemHighlight||a.isItemEdit){b=true;break}}if(!b){e.push("itemhighlight");d.setInteractions(e)}},showTooltip:function(l,m){var d=this,n=d.getTooltip(),j,a,i,f,h,k,g,e,b,c;if(!n){return}clearTimeout(d.tooltipTimeout);b=n.config;if(n.trackMouse){m[0]+=b.offsetX;m[1]+=b.offsetY}else{j=l.sprite;a=j.getSurface();i=Ext.get(a.getId());if(i){k=l.series.getBBoxForItem(l);g=k.x+k.width/2;e=k.y+k.height/2;h=a.matrix.transformPoint([g,e]);f=i.getXY();c=a.getInherited().rtl;g=c?f[0]+i.getWidth()-h[0]:f[0]+h[0];e=f[1]+h[1];m=[g,e]}}Ext.callback(n.renderer,n.scope,[n,l.record,l],0,d);n.show(m)},hideTooltip:function(b){var a=this,c=a.getTooltip();if(!c){return}clearTimeout(a.tooltipTimeout);a.tooltipTimeout=Ext.defer(function(){c.hide()},1)},applyStore:function(a){return a&&Ext.StoreManager.lookup(a)},getStore:function(){return this._store||this.getChart()&&this.getChart().getStore()},updateStore:function(b,a){var h=this,g=h.getChart(),c=g&&g.getStore(),f,j,e,d;a=a||c;if(a&&a!==b){a.un({datachanged:"onDataChanged",update:"onDataChanged",scope:h})}if(b){b.on({datachanged:"onDataChanged",update:"onDataChanged",scope:h});f=h.getSprites();for(d=0,e=f.length;d<e;d++){j=f[d];if(j.setStore){j.setStore(b)}}h.onDataChanged()}h.fireEvent("storechange",h,b,a)},onStoreChange:function(b,a,c){if(!this._store){this.updateStore(a,c)}},coordinate:function(o,m,e){var l=this,p=l.getStore(),h=l.getHidden(),k=p.getData().items,b=l["get"+o+"Axis"](),f={min:Infinity,max:-Infinity},q=l["fieldCategory"+o]||[o],g=l.getFields(q),d,n,c,a={},j=l.getSprites();if(j.length>0){if(!Ext.isBoolean(h)||!h){for(d=0;d<q.length;d++){n=g[d];c=l.coordinateData(k,n,b);l.getRangeOfData(c,f);a["data"+q[d]]=c}}l.dataRange[m]=f.min;l.dataRange[m+e]=f.max;a["dataMin"+o]=f.min;a["dataMax"+o]=f.max;if(b){b.range=null;a["range"+o]=b.getRange()}for(d=0;d<j.length;d++){j[d].setAttributes(a)}}},coordinateData:function(b,h,d){var g=[],f=b.length,e=d&&d.getLayout(),c,a;for(c=0;c<f;c++){a=b[c].data[h];if(!Ext.isEmpty(a,true)){if(e){g[c]=e.getCoordFor(a,h,c,b)}else{g[c]=+a}}else{g[c]=a}}return g},getRangeOfData:function(g,b){var e=g.length,d=b.min,a=b.max,c,f;for(c=0;c<e;c++){f=g[c];if(f<d){d=f}if(f>a){a=f}}b.min=d;b.max=a},updateLabelData:function(){var h=this,l=h.getStore(),g=l.getData().items,f=h.getSprites(),a=h.getLabel().getTemplate(),n=Ext.Array.from(a.getField()),c,b,e,d,m,k;if(!f.length||!n.length){return}for(c=0;c<f.length;c++){d=[];m=f[c];k=m.getField();if(Ext.Array.indexOf(n,k)<0){k=n[c]}for(b=0,e=g.length;b<e;b++){d.push(g[b].get(k))}m.setAttributes({labels:d})}},processData:function(){if(!this.getStore()){return}var d=this,f=this.directions,a,c=f.length,e,b;for(a=0;a<c;a++){e=f[a];b=d["get"+e+"Axis"]();if(b){b.processData(d);continue}if(d["coordinate"+e]){d["coordinate"+e]()}}d.updateLabelData()},applyBackground:function(a){if(this.getChart()){this.getSurface().setBackground(a);return this.getSurface().getBackground()}else{return a}},updateChart:function(d,a){var c=this,b=c._store;if(a){a.un("axeschange","onAxesChange",c);c.clearSprites();c.setSurface(null);c.setOverlaySurface(null);a.unregister(c);c.onChartDetached(a);if(!b){c.updateStore(null)}}if(d){c.setSurface(d.getSurface("series"));c.setOverlaySurface(d.getSurface("overlay"));d.on("axeschange","onAxesChange",c);if(d.getAxes()){c.onAxesChange(d)}c.onChartAttached(d);d.register(c);if(!b){c.updateStore(d.getStore())}}},onAxesChange:function(h){var k=this,g=h.getAxes(),c,a={},b={},e=false,j=this.directions,l,d,f;for(d=0,f=j.length;d<f;d++){l=j[d];b[l]=k.getFields(k["fieldCategory"+l])}for(d=0,f=g.length;d<f;d++){c=g[d];if(!a[c.getDirection()]){a[c.getDirection()]=[c]}else{a[c.getDirection()].push(c)}}for(d=0,f=j.length;d<f;d++){l=j[d];if(k["get"+l+"Axis"]()){continue}if(a[l]){c=k.findMatchingAxis(a[l],b[l]);if(c){k["set"+l+"Axis"](c);if(c.getNeedHighPrecision()){e=true}}}}this.getSurface().setHighPrecision(e)},findMatchingAxis:function(f,e){var d,c,b,a;for(b=0;b<f.length;b++){d=f[b];c=d.getFields();if(!c.length){return d}else{if(e){for(a=0;a<e.length;a++){if(Ext.Array.indexOf(c,e[a])>=0){return d}}}}}},onChartDetached:function(a){var b=this;b.fireEvent("chartdetached",a,b);a.un("storechange","onStoreChange",b)},onChartAttached:function(a){var b=this;b.setBackground(b.getBackground());b.fireEvent("chartattached",a,b);a.on("storechange","onStoreChange",b);b.processData()},updateOverlaySurface:function(a){var b=this;if(a){if(b.getLabel()){b.getOverlaySurface().add(b.getLabel())}}},applyLabel:function(a,b){if(!b){b=new Ext.chart.Markers({zIndex:10});b.setTemplate(new Ext.chart.label.Label(a))}else{b.getTemplate().setAttributes(a)}return b},createItemInstancingSprite:function(c,b){var e=this,f=new Ext.chart.Markers(),a,d;f.setAttributes({zIndex:Number.MAX_VALUE});a=Ext.apply({},b);if(e.getHighlight()){a.highlight=e.getHighlight();a.modifiers=["highlight"]}f.setTemplate(a);d=f.getTemplate();d.setAttributes(e.getStyle());d.fx.on("animationstart","onSpriteAnimationStart",this);d.fx.on("animationend","onSpriteAnimationEnd",this);c.bindMarker("items",f);e.getSurface().add(f);return f},getDefaultSpriteConfig:function(){return{type:this.seriesType,renderer:this.getRenderer()}},updateRenderer:function(c){var b=this,a=b.getChart(),d;if(a&&a.isInitializing){return}d=b.getSprites();if(d.length){d[0].setAttributes({renderer:c||null});if(a&&!a.isInitializing){a.redraw()}}},updateShowMarkers:function(a){var d=this.getSprites(),b=d&&d[0],c=b&&b.getMarker("markers");if(c){c.getTemplate().setAttributes({hidden:!a})}},createSprite:function(){var f=this,a=f.getSurface(),e=f.getItemInstancing(),d=a.add(f.getDefaultSpriteConfig()),b=f.getMarker(),g,c;d.setAttributes(f.getStyle());d.setSeries(f);if(e){d.itemsMarker=f.createItemInstancingSprite(d,e)}if(d.bindMarker){if(b){g=new Ext.chart.Markers();c=Ext.Object.merge({},b);if(f.getHighlight()){c.highlight=f.getHighlight();c.modifiers=["highlight"]}g.setTemplate(c);g.getTemplate().fx.setCustomDurations({translationX:0,translationY:0});d.dataMarker=g;d.bindMarker("markers",g);f.getOverlaySurface().add(g)}if(f.getLabel().getTemplate().getField()){d.bindMarker("labels",f.getLabel())}}if(d.setStore){d.setStore(f.getStore())}d.fx.on("animationstart","onSpriteAnimationStart",f);d.fx.on("animationend","onSpriteAnimationEnd",f);f.sprites.push(d);return d},getSprites:Ext.emptyFn,onDataChanged:function(){var d=this,c=d.getChart(),b=c&&c.getStore(),a=d.getStore();if(a!==b){d.processData()}},isXType:function(a){return a==="series"},getItemId:function(){return this.getId()},applyThemeStyle:function(e,a){var b=this,d,c;d=e&&e.subStyle&&e.subStyle.fillStyle;c=d&&e.subStyle.strokeStyle;if(d&&!c){e.subStyle.strokeStyle=b.getStrokeColorsFromFillColors(d)}d=e&&e.markerSubStyle&&e.markerSubStyle.fillStyle;c=d&&e.markerSubStyle.strokeStyle;if(d&&!c){e.markerSubStyle.strokeStyle=b.getStrokeColorsFromFillColors(d)}return Ext.apply(a||{},e)},applyStyle:function(c,b){var a=Ext.ClassManager.get(Ext.ClassManager.getNameByAlias("sprite."+this.seriesType));if(a&&a.def){c=a.def.normalize(c)}return Ext.apply({},c,b)},applySubStyle:function(b,c){var a=Ext.ClassManager.get(Ext.ClassManager.getNameByAlias("sprite."+this.seriesType));if(a&&a.def){b=a.def.batchedNormalize(b,true)}return Ext.merge({},c,b)},applyMarker:function(c,a){var d=(c&&c.type)||(a&&a.type)||"circle",b=Ext.ClassManager.get(Ext.ClassManager.getNameByAlias("sprite."+d));if(b&&b.def){c=b.def.normalize(Ext.isObject(c)?c:{},true);c.type=d}return Ext.merge(a||{},c)},applyMarkerSubStyle:function(c,a){var d=(c&&c.type)||(a&&a.type)||"circle",b=Ext.ClassManager.get(Ext.ClassManager.getNameByAlias("sprite."+d));if(b&&b.def){c=b.def.batchedNormalize(c,true)}return Ext.merge(a||{},c)},updateHidden:function(b){var a=this;a.getColors();a.getSubStyle();a.setSubStyle({hidden:b});a.processData();a.doUpdateStyles();if(!Ext.isArray(b)){a.updateLegendStore(b)}},updateLegendStore:function(f,b){var e=this,d=e.getChart(),c=d.getLegendStore(),g=e.getId(),a;if(c){if(arguments.length>1){a=c.findBy(function(h){return h.get("series")===g&&h.get("index")===b});if(a!==-1){a=c.getAt(a)}}else{a=c.findRecord("series",g)}if(a&&a.get("disabled")!==f){a.set("disabled",f)}}},setHiddenByIndex:function(a,c){var b=this;if(Ext.isArray(b.getHidden())){b.getHidden()[a]=c;b.updateHidden(b.getHidden());b.updateLegendStore(c,a)}else{b.setHidden(c)}},getStrokeColorsFromFillColors:function(a){var c=this,e=c.getUseDarkerStrokeColor(),b=(Ext.isNumber(e)?e:c.darkerStrokeRatio),d;if(e){d=Ext.Array.map(a,function(f){f=Ext.isString(f)?f:f.stops[0].color;f=Ext.draw.Color.fromString(f);return f.createDarker(b).toString()})}else{d=Ext.Array.clone(a)}return d},updateThemeColors:function(b){var c=this,d=c.getThemeStyle(),a=Ext.Array.clone(b),f=c.getStrokeColorsFromFillColors(b),e={fillStyle:a,strokeStyle:f};d.subStyle=Ext.apply(d.subStyle||{},e);d.markerSubStyle=Ext.apply(d.markerSubStyle||{},e);c.doUpdateStyles()},themeOnlyIfConfigured:{},updateTheme:function(d){var h=this,a=d.getSeries(),n=h.getInitialConfig(),c=h.defaultConfig,f=h.getConfigurator().configs,j=a.defaults,k=a[h.type],g=h.themeOnlyIfConfigured,l,i,o,b,m,e;a=Ext.merge({},j,k);for(l in a){i=a[l];e=f[l];if(i!==null&&i!==undefined&&e){m=n[l];o=Ext.isObject(i);b=m===c[l];if(o){if(b&&g[l]){continue}i=Ext.merge({},i,m)}if(b||o){h[e.names.set](i)}}}},updateChartColors:function(a){var b=this;if(!b.getColors()){b.updateThemeColors(a)}},updateColors:function(a){this.updateThemeColors(a)},updateStyle:function(){this.doUpdateStyles()},updateSubStyle:function(){this.doUpdateStyles()},updateThemeStyle:function(){this.doUpdateStyles()},doUpdateStyles:function(){var g=this,h=g.sprites,d=g.getItemInstancing(),c=0,f=h&&h.length,a=g.getConfig("showMarkers",true),b=g.getMarker(),e;for(;c<f;c++){e=g.getStyleByIndex(c);if(d){h[c].itemsMarker.getTemplate().setAttributes(e)}h[c].setAttributes(e);if(b&&h[c].dataMarker){h[c].dataMarker.getTemplate().setAttributes(g.getMarkerStyleByIndex(c))}}},getStyleWithTheme:function(){var b=this,c=b.getThemeStyle(),d=(c&&c.style)||{},a=Ext.applyIf(Ext.apply({},b.getStyle()),d);return a},getSubStyleWithTheme:function(){var c=this,d=c.getThemeStyle(),a=(d&&d.subStyle)||{},b=Ext.applyIf(Ext.apply({},c.getSubStyle()),a);return b},getStyleByIndex:function(b){var e=this,h=e.getThemeStyle(),d,g,c,f,a={};d=e.getStyle();g=(h&&h.style)||{};c=e.styleDataForIndex(e.getSubStyle(),b);f=e.styleDataForIndex((h&&h.subStyle),b);Ext.apply(a,g);Ext.apply(a,f);Ext.apply(a,d);Ext.apply(a,c);return a},getMarkerStyleByIndex:function(d){var g=this,c=g.getThemeStyle(),a,e,k,j,b,l,h,f,m={};a=g.getStyle();e=(c&&c.style)||{};k=g.styleDataForIndex(g.getSubStyle(),d);if(k.hasOwnProperty("hidden")){k.hidden=k.hidden||!this.getConfig("showMarkers",true)}j=g.styleDataForIndex((c&&c.subStyle),d);b=g.getMarker();l=(c&&c.marker)||{};h=g.getMarkerSubStyle();f=g.styleDataForIndex((c&&c.markerSubStyle),d);Ext.apply(m,e);Ext.apply(m,j);Ext.apply(m,l);Ext.apply(m,f);Ext.apply(m,a);Ext.apply(m,k);Ext.apply(m,b);Ext.apply(m,h);return m},styleDataForIndex:function(d,c){var e,b,a={};if(d){for(b in d){e=d[b];if(Ext.isArray(e)){a[b]=e[c%e.length]}else{a[b]=e}}}return a},getItemForPoint:Ext.emptyFn,getItemByIndex:function(a,e){var d=this,f=d.getSprites(),b=f&&f[0],c;if(!b){return}if(e===undefined&&b.isMarkerHolder){e=d.getItemInstancing()?"items":"markers"}else{if(!e||e===""||e==="sprites"){b=f[a]}}if(b){c={series:d,category:e,index:a,record:d.getStore().getData().items[a],field:d.getYField(),sprite:b};return c}},onSpriteAnimationStart:function(a){this.fireEvent("animationstart",this,a)},onSpriteAnimationEnd:function(a){this.fireEvent("animationend",this,a)},resolveListenerScope:function(e){var d=this,a=Ext._namedScopes[e],c=d.getChart(),b;if(!a){b=c?c.resolveListenerScope(e,false):(e||d)}else{if(a.isThis){b=d}else{if(a.isController){b=c?c.resolveListenerScope(e,false):d}else{if(a.isSelf){b=c?c.resolveListenerScope(e,false):d;if(b===c&&!c.getInheritedConfig("defaultListenerScope")){b=d}}}}}return b},provideLegendInfo:function(a){a.push({name:this.getTitle()||this.getId(),mark:"black",disabled:this.getHidden(),series:this.getId(),index:0})},clearSprites:function(){var d=this.sprites,b,a,c;for(a=0,c=d.length;a<c;a++){b=d[a];if(b&&b.isSprite){b.destroy()}}this.sprites=[]},destroy:function(){var b=this,a=b._store,c=b.getConfig("tooltip",true);if(a&&a.getAutoDestroy()){Ext.destroy(a)}b.setChart(null);b.clearListeners();if(c){Ext.destroy(c);clearTimeout(b.tooltipTimeout)}b.callParent()}});Ext.define("Ext.chart.interactions.Abstract",{xtype:"interaction",mixins:{observable:Ext.mixin.Observable},config:{gestures:{tap:"onGesture"},chart:null,enabled:true},throttleGap:0,stopAnimationBeforeSync:false,constructor:function(a){var b=this,c;a=a||{};if("id" in a){c=a.id}else{if("id" in b.config){c=b.config.id}else{c=b.getId()}}b.setId(c);b.mixins.observable.constructor.call(b,a)},initialize:Ext.emptyFn,updateChart:function(c,a){var b=this;if(a===c){return}if(a){a.unregister(b);b.removeChartListener(a)}if(c){c.register(b);b.addChartListener()}},updateEnabled:function(a){var c=this,b=c.getChart();if(b){if(a){c.addChartListener()}else{c.removeChartListener(b)}}},onGesture:Ext.emptyFn,getItemForEvent:function(d){var b=this,a=b.getChart(),c=a.getEventXY(d);return a.getItemForPoint(c[0],c[1])},getItemsForEvent:function(d){var b=this,a=b.getChart(),c=a.getEventXY(d);return a.getItemsForPoint(c[0],c[1])},addChartListener:function(){var c=this,b=c.getChart(),e=c.getGestures(),a;if(!c.getEnabled()){return}function d(f,g){b.addElementListener(f,c.listeners[f]=function(j){var i=c.getLocks(),h;if(c.getEnabled()&&(!(f in i)||i[f]===c)){h=(Ext.isFunction(g)?g:c[g]).apply(this,arguments);if(h===false&&j&&j.stopPropagation){j.stopPropagation()}return h}},c)}c.listeners=c.listeners||{};for(a in e){d(a,e[a])}},removeChartListener:function(c){var d=this,e=d.getGestures(),b;function a(f){var g=d.listeners[f];if(g){c.removeElementListener(f,g);delete d.listeners[f]}}if(d.listeners){for(b in e){a(b)}}},lockEvents:function(){var d=this,c=d.getLocks(),a=Array.prototype.slice.call(arguments),b=a.length;while(b--){c[a[b]]=d}},unlockEvents:function(){var c=this.getLocks(),a=Array.prototype.slice.call(arguments),b=a.length;while(b--){delete c[a[b]]}},getLocks:function(){var a=this.getChart();return a.lockedEvents||(a.lockedEvents={})},isMultiTouch:function(){if(Ext.browser.is.IE10){return true}return !Ext.os.is.Desktop},initializeDefaults:Ext.emptyFn,doSync:function(){var b=this,a=b.getChart();if(b.syncTimer){clearTimeout(b.syncTimer);b.syncTimer=null}if(b.stopAnimationBeforeSync){a.animationSuspendCount++}a.redraw();if(b.stopAnimationBeforeSync){a.animationSuspendCount--}b.syncThrottle=Date.now()+b.throttleGap},sync:function(){var a=this;if(a.throttleGap&&Ext.frameStartTime<a.syncThrottle){if(a.syncTimer){return}a.syncTimer=Ext.defer(function(){a.doSync()},a.throttleGap)}else{a.doSync()}},getItemId:function(){return this.getId()},isXType:function(a){return a==="interaction"},destroy:function(){var a=this;a.setChart(null);delete a.listeners;a.callParent()}},function(){if(Ext.os.is.Android4){this.prototype.throttleGap=40}});Ext.define("Ext.chart.MarkerHolder",{extend:Ext.Mixin,mixinConfig:{id:"markerHolder",after:{constructor:"constructor",preRender:"preRender"},before:{destroy:"destroy"}},isMarkerHolder:true,surfaceMatrix:null,inverseSurfaceMatrix:null,deprecated:{6:{methods:{getBoundMarker:{message:"Please use the 'getMarker' method instead.",fn:function(b){var a=this.boundMarkers[b];return a?[a]:a}}}}},constructor:function(){this.boundMarkers={};this.cleanRedraw=false},bindMarker:function(b,a){var c=this,d=c.boundMarkers;if(a&&a.isMarkers){c.releaseMarker(b);d[b]=a;a.on("destroy",c.onMarkerDestroy,c)}},onMarkerDestroy:function(a){this.releaseMarker(a)},releaseMarker:function(a){var c=this.boundMarkers,b;if(a&&a.isMarkers){for(b in c){if(c[b]===a){delete c[b];break}}}else{b=a;a=c[b];delete c[b]}return a||null},getMarker:function(a){return this.boundMarkers[a]||null},preRender:function(){var f=this,g=f.getId(),d=f.boundMarkers,e=f.getParent(),c,a,b;if(f.surfaceMatrix){b=f.surfaceMatrix.set(1,0,0,1,0,0)}else{b=f.surfaceMatrix=new Ext.draw.Matrix()}f.cleanRedraw=!f.attr.dirty;if(!f.cleanRedraw){for(c in d){a=d[c];if(a){a.clear(g)}}}while(e&&e.attr&&e.attr.matrix){b.prependMatrix(e.attr.matrix);e=e.getParent()}b.prependMatrix(e.matrix);f.surfaceMatrix=b;f.inverseSurfaceMatrix=b.inverse(f.inverseSurfaceMatrix)},putMarker:function(d,a,c,g,e){var b=this.boundMarkers[d],f=this.getId();if(b){b.putMarkerFor(f,a,c,g,e)}},getMarkerBBox:function(c,b,d){var a=this.boundMarkers[c],e=this.getId();if(a){return a.getMarkerBBoxFor(e,b,d)}},destroy:function(){var c=this.boundMarkers,b,a;for(b in c){a=c[b];a.destroy()}}});Ext.define("Ext.chart.axis.sprite.Axis",{extend:Ext.draw.sprite.Sprite,alias:"sprite.axis",type:"axis",mixins:{markerHolder:Ext.chart.MarkerHolder},inheritableStatics:{def:{processors:{grid:"bool",axisLine:"bool",minorTicks:"bool",minorTickSize:"number",majorTicks:"bool",majorTickSize:"number",length:"number",startGap:"number",endGap:"number",dataMin:"number",dataMax:"number",visibleMin:"number",visibleMax:"number",position:"enums(left,right,top,bottom,angular,radial,gauge)",minStepSize:"number",estStepSize:"number",titleOffset:"number",textPadding:"number",min:"number",max:"number",centerX:"number",centerY:"number",radius:"number",totalAngle:"number",baseRotation:"number",data:"default",enlargeEstStepSizeByText:"bool"},defaults:{grid:false,axisLine:true,minorTicks:false,minorTickSize:3,majorTicks:true,majorTickSize:5,length:0,startGap:0,endGap:0,visibleMin:0,visibleMax:1,dataMin:0,dataMax:1,position:"",minStepSize:0,estStepSize:20,min:0,max:1,centerX:0,centerY:0,radius:1,baseRotation:0,data:null,titleOffset:0,textPadding:0,scalingCenterY:0,scalingCenterX:0,strokeStyle:"black",enlargeEstStepSizeByText:false},triggers:{minorTickSize:"bbox",majorTickSize:"bbox",position:"bbox,layout",axisLine:"bbox,layout",min:"layout",max:"layout",length:"layout",minStepSize:"layout",estStepSize:"layout",data:"layout",dataMin:"layout",dataMax:"layout",visibleMin:"layout",visibleMax:"layout",enlargeEstStepSizeByText:"layout"},updaters:{layout:"layoutUpdater"}}},config:{label:null,layout:null,segmenter:null,renderer:null,layoutContext:null,axis:null},thickness:0,stepSize:0,getBBox:function(){return null},defaultRenderer:function(a){return this.segmenter.renderer(a,this)},layoutUpdater:function(){var h=this,f=h.getAxis().getChart();if(f.isInitializing){return}var e=h.attr,d=h.getLayout(),g=f.getInherited().rtl,b=e.dataMin+(e.dataMax-e.dataMin)*e.visibleMin,i=e.dataMin+(e.dataMax-e.dataMin)*e.visibleMax,c=e.position,a={attr:e,segmenter:h.getSegmenter(),renderer:h.defaultRenderer};if(c==="left"||c==="right"){e.translationX=0;e.translationY=i*e.length/(i-b);e.scalingX=1;e.scalingY=-e.length/(i-b);e.scalingCenterY=0;e.scalingCenterX=0;h.applyTransformations(true)}else{if(c==="top"||c==="bottom"){if(g){e.translationX=e.length+b*e.length/(i-b)+1}else{e.translationX=-b*e.length/(i-b)}e.translationY=0;e.scalingX=(g?-1:1)*e.length/(i-b);e.scalingY=1;e.scalingCenterY=0;e.scalingCenterX=0;h.applyTransformations(true)}}if(d){d.calculateLayout(a);h.setLayoutContext(a)}},iterate:function(e,j){var c,g,a,b,h,d,k=Ext.Array.some,m=Math.abs,f;if(e.getLabel){if(e.min<e.from){j.call(this,e.min,e.getLabel(e.min),-1,e)}for(c=0;c<=e.steps;c++){j.call(this,e.get(c),e.getLabel(c),c,e)}if(e.max>e.to){j.call(this,e.max,e.getLabel(e.max),e.steps+1,e)}}else{b=this.getAxis();h=b.floatingAxes;d=[];f=(e.to-e.from)/(e.steps+1);if(b.getFloating()){for(a in h){d.push(h[a])}}function l(i){return !d.length||k(d,function(n){return m(n-i)>f})}if(e.min<e.from&&l(e.min)){j.call(this,e.min,e.min,-1,e)}for(c=0;c<=e.steps;c++){g=e.get(c);if(l(g)){j.call(this,g,g,c,e)}}if(e.max>e.to&&l(e.max)){j.call(this,e.max,e.max,e.steps+1,e)}}},renderTicks:function(l,m,s,p){var v=this,k=v.attr,u=k.position,n=k.matrix,e=0.5*k.lineWidth,f=n.getXX(),i=n.getDX(),j=n.getYY(),h=n.getDY(),o=s.majorTicks,d=k.majorTickSize,a=s.minorTicks,r=k.minorTickSize;if(o){switch(u){case"right":function q(w){return function(x,z,y){x=l.roundPixel(x*j+h)+e;m.moveTo(0,x);m.lineTo(w,x)}}v.iterate(o,q(d));a&&v.iterate(a,q(r));break;case"left":function t(w){return function(x,z,y){x=l.roundPixel(x*j+h)+e;m.moveTo(p[2]-w,x);m.lineTo(p[2],x)}}v.iterate(o,t(d));a&&v.iterate(a,t(r));break;case"bottom":function c(w){return function(x,z,y){x=l.roundPixel(x*f+i)-e;m.moveTo(x,0);m.lineTo(x,w)}}v.iterate(o,c(d));a&&v.iterate(a,c(r));break;case"top":function b(w){return function(x,z,y){x=l.roundPixel(x*f+i)-e;m.moveTo(x,p[3]);m.lineTo(x,p[3]-w)}}v.iterate(o,b(d));a&&v.iterate(a,b(r));break;case"angular":v.iterate(o,function(w,y,x){w=w/(k.max+1)*Math.PI*2+k.baseRotation;m.moveTo(k.centerX+(k.length)*Math.cos(w),k.centerY+(k.length)*Math.sin(w));m.lineTo(k.centerX+(k.length+d)*Math.cos(w),k.centerY+(k.length+d)*Math.sin(w))});break;case"gauge":var g=v.getGaugeAngles();v.iterate(o,function(w,y,x){w=(w-k.min)/(k.max-k.min+1)*k.totalAngle-k.totalAngle+g.start;m.moveTo(k.centerX+(k.length)*Math.cos(w),k.centerY+(k.length)*Math.sin(w));m.lineTo(k.centerX+(k.length+d)*Math.cos(w),k.centerY+(k.length+d)*Math.sin(w))});break}}},renderLabels:function(E,q,D,K){var o=this,k=o.attr,i=0.5*k.lineWidth,u=k.position,y=k.matrix,A=k.textPadding,x=y.getXX(),d=y.getDX(),g=y.getYY(),c=y.getDY(),n=0,I=D.majorTicks,G=Math.max(k.majorTickSize,k.minorTickSize)+k.lineWidth,f=Ext.draw.Draw.isBBoxIntersect,F=o.getLabel(),J,s,r=null,w=0,b=0,m=D.segmenter,B=o.getRenderer(),t=o.getAxis(),z=t.getTitle(),a=z&&z.attr.text!==""&&z.getBBox(),l,h=null,p,C,v,e,H;if(I&&F&&!F.attr.hidden){J=F.attr.font;if(q.font!==J){q.font=J}F.setAttributes({translationX:0,translationY:0},true);F.applyTransformations();l=F.attr.inverseMatrix.elements.slice(0);switch(u){case"left":e=a?a.x+a.width:0;switch(F.attr.textAlign){case"start":H=E.roundPixel(e+d)-i;break;case"end":H=E.roundPixel(K[2]-G+d)-i;break;default:H=E.roundPixel(e+(K[2]-e-G)/2+d)-i}F.setAttributes({translationX:H},true);break;case"right":e=a?K[2]-a.x:0;switch(F.attr.textAlign){case"start":H=E.roundPixel(G+d)+i;break;case"end":H=E.roundPixel(K[2]-e+d)+i;break;default:H=E.roundPixel(G+(K[2]-G-e)/2+d)+i}F.setAttributes({translationX:H},true);break;case"top":e=a?a.y+a.height:0;F.setAttributes({translationY:E.roundPixel(e+(K[3]-e-G)/2)-i},true);break;case"bottom":e=a?K[3]-a.y:0;F.setAttributes({translationY:E.roundPixel(G+(K[3]-G-e)/2)+i},true);break;case"radial":F.setAttributes({translationX:k.centerX},true);break;case"angular":F.setAttributes({translationY:k.centerY},true);break;case"gauge":F.setAttributes({translationY:k.centerY},true);break}if(u==="left"||u==="right"){o.iterate(I,function(L,N,M){if(N===undefined){return}if(B){v=Ext.callback(B,null,[t,N,D,r],0,t)}else{v=m.renderer(N,D,r)}r=N;F.setAttributes({text:String(v),translationY:E.roundPixel(L*g+c)},true);F.applyTransformations();n=Math.max(n,F.getBBox().width+G);if(n<=o.thickness){C=Ext.draw.Matrix.fly(F.attr.matrix.elements.slice(0));p=C.prepend.apply(C,l).transformBBox(F.getBBox(true));if(h&&!f(p,h,A)){return}E.renderSprite(F);h=p;w+=p.height;b++}})}else{if(u==="top"||u==="bottom"){o.iterate(I,function(L,N,M){if(N===undefined){return}if(B){v=Ext.callback(B,null,[t,N,D,r],0,t)}else{v=m.renderer(N,D,r)}r=N;F.setAttributes({text:String(v),translationX:E.roundPixel(L*x+d)},true);F.applyTransformations();n=Math.max(n,F.getBBox().height+G);if(n<=o.thickness){C=Ext.draw.Matrix.fly(F.attr.matrix.elements.slice(0));p=C.prepend.apply(C,l).transformBBox(F.getBBox(true));if(h&&!f(p,h,A)){return}E.renderSprite(F);h=p;w+=p.width;b++}})}else{if(u==="radial"){o.iterate(I,function(L,N,M){if(N===undefined){return}if(B){v=Ext.callback(B,null,[t,N,D,r],0,t)}else{v=m.renderer(N,D,r)}r=N;if(typeof v!=="undefined"){F.setAttributes({text:String(v),translationX:k.centerX-E.roundPixel(L)/k.max*k.length*Math.cos(k.baseRotation+Math.PI/2),translationY:k.centerY-E.roundPixel(L)/k.max*k.length*Math.sin(k.baseRotation+Math.PI/2)},true);F.applyTransformations();p=F.attr.matrix.transformBBox(F.getBBox(true));if(h&&!f(p,h)){return}E.renderSprite(F);h=p;w+=p.width;b++}})}else{if(u==="angular"){s=k.majorTickSize+k.lineWidth*0.5+(parseInt(F.attr.fontSize,10)||10)/2;o.iterate(I,function(L,N,M){if(N===undefined){return}if(B){v=Ext.callback(B,null,[t,N,D,r],0,t)}else{v=m.renderer(N,D,r)}r=N;n=Math.max(n,Math.max(k.majorTickSize,k.minorTickSize)+(k.lineCap!=="butt"?k.lineWidth*0.5:0));if(typeof v!=="undefined"){var O=L/(k.max+1)*Math.PI*2+k.baseRotation;F.setAttributes({text:String(v),translationX:k.centerX+(k.length+s)*Math.cos(O),translationY:k.centerY+(k.length+s)*Math.sin(O)},true);F.applyTransformations();p=F.attr.matrix.transformBBox(F.getBBox(true));if(h&&!f(p,h)){return}E.renderSprite(F);h=p;w+=p.width;b++}})}else{if(u==="gauge"){var j=o.getGaugeAngles();o.iterate(I,function(L,N,M){if(N===undefined){return}if(B){v=Ext.callback(B,null,[t,N,D,r],0,t)}else{v=m.renderer(N,D,r)}r=N;if(typeof v!=="undefined"){var O=(L-k.min)/(k.max-k.min+1)*k.totalAngle-k.totalAngle+j.start;F.setAttributes({text:String(v),translationX:k.centerX+(k.length+10)*Math.cos(O),translationY:k.centerY+(k.length+10)*Math.sin(O)},true);F.applyTransformations();p=F.attr.matrix.transformBBox(F.getBBox(true));if(h&&!f(p,h)){return}E.renderSprite(F);h=p;w+=p.width;b++}})}}}}}if(k.enlargeEstStepSizeByText&&b){w/=b;w+=G;w*=2;if(k.estStepSize<w){k.estStepSize=w}}if(Math.abs(o.thickness-(n))>1){o.thickness=n;k.bbox.plain.dirty=true;k.bbox.transform.dirty=true;o.doThicknessChanged();return false}}},renderAxisLine:function(a,i,e,c){var h=this,g=h.attr,b=g.lineWidth*0.5,j=g.position,d,f;if(g.axisLine&&g.length){switch(j){case"left":d=a.roundPixel(c[2])-b;i.moveTo(d,-g.endGap);i.lineTo(d,g.length+g.startGap+1);break;case"right":i.moveTo(b,-g.endGap);i.lineTo(b,g.length+g.startGap+1);break;case"bottom":i.moveTo(-g.startGap,b);i.lineTo(g.length+g.endGap,b);break;case"top":d=a.roundPixel(c[3])-b;i.moveTo(-g.startGap,d);i.lineTo(g.length+g.endGap,d);break;case"angular":i.moveTo(g.centerX+g.length,g.centerY);i.arc(g.centerX,g.centerY,g.length,0,Math.PI*2,true);break;case"gauge":f=h.getGaugeAngles();i.moveTo(g.centerX+Math.cos(f.start)*g.length,g.centerY+Math.sin(f.start)*g.length);i.arc(g.centerX,g.centerY,g.length,f.start,f.end,true);break}}},getGaugeAngles:function(){var a=this,c=a.attr.totalAngle,b;if(c<=Math.PI){b=(Math.PI-c)*0.5}else{b=-(Math.PI*2-c)*0.5}b=Math.PI*2-b;return{start:b,end:b-c}},renderGridLines:function(m,n,s,r){var t=this,b=t.getAxis(),l=t.attr,p=l.matrix,d=l.startGap,a=l.endGap,c=p.getXX(),k=p.getYY(),h=p.getDX(),g=p.getDY(),u=l.position,f=b.getGridAlignment(),q=s.majorTicks,e,o,i;if(l.grid){if(q){if(u==="left"||u==="right"){i=l.min*k+g+a+d;t.iterate(q,function(j,w,v){e=j*k+g+a;t.putMarker(f+"-"+(v%2?"odd":"even"),{y:e,height:i-e},o=v,true);i=e});o++;e=0;t.putMarker(f+"-"+(o%2?"odd":"even"),{y:e,height:i-e},o,true)}else{if(u==="top"||u==="bottom"){i=l.min*c+h+d;if(d){t.putMarker(f+"-even",{x:0,width:i},-1,true)}t.iterate(q,function(j,w,v){e=j*c+h+d;t.putMarker(f+"-"+(v%2?"odd":"even"),{x:e,width:i-e},o=v,true);i=e});o++;e=l.length+l.startGap+l.endGap;t.putMarker(f+"-"+(o%2?"odd":"even"),{x:e,width:i-e},o,true)}else{if(u==="radial"){t.iterate(q,function(j,w,v){if(!j){return}e=j/l.max*l.length;t.putMarker(f+"-"+(v%2?"odd":"even"),{scalingX:e,scalingY:e},v,true);i=e})}else{if(u==="angular"){t.iterate(q,function(j,w,v){if(!l.length){return}e=j/(l.max+1)*Math.PI*2+l.baseRotation;t.putMarker(f+"-"+(v%2?"odd":"even"),{rotationRads:e,rotationCenterX:0,rotationCenterY:0,scalingX:l.length,scalingY:l.length},v,true);i=e})}}}}}}},renderLimits:function(o){var t=this,a=t.getAxis(),h=a.getChart(),p=h.getInnerPadding(),d=Ext.Array.from(a.getLimits());if(!d.length){return}var r=a.limits.surface.getRect(),m=t.attr,n=m.matrix,u=m.position,k=Ext.Object.chain,v=a.limits.titles,c,j,b,s,l,q,f,g,e;v.instances=[];v.position=0;if(u==="left"||u==="right"){for(q=0,f=d.length;q<f;q++){s=k(d[q]);!s.line&&(s.line={});l=Ext.isString(s.value)?a.getCoordFor(s.value):s.value;l=l*n.getYY()+n.getDY();s.line.y=l+p.top;s.line.strokeStyle=s.line.strokeStyle||m.strokeStyle;t.putMarker("horizontal-limit-lines",s.line,q,true);if(s.line.title){v.createInstance(s.line.title);c=v.getBBoxFor(v.position-1);j=s.line.title.position||(u==="left"?"start":"end");switch(j){case"start":g=10;break;case"end":g=r[2]-10;break;case"middle":g=r[2]/2;break}v.setAttributesFor(v.position-1,{x:g,y:s.line.y-c.height/2,textAlign:j,fillStyle:s.line.title.fillStyle||s.line.strokeStyle})}}}else{if(u==="top"||u==="bottom"){for(q=0,f=d.length;q<f;q++){s=k(d[q]);!s.line&&(s.line={});l=Ext.isString(s.value)?a.getCoordFor(s.value):s.value;l=l*n.getXX()+n.getDX();s.line.x=l+p.left;s.line.strokeStyle=s.line.strokeStyle||m.strokeStyle;t.putMarker("vertical-limit-lines",s.line,q,true);if(s.line.title){v.createInstance(s.line.title);c=v.getBBoxFor(v.position-1);j=s.line.title.position||(u==="top"?"end":"start");switch(j){case"start":e=r[3]-c.width/2-10;break;case"end":e=c.width/2+10;break;case"middle":e=r[3]/2;break}v.setAttributesFor(v.position-1,{x:s.line.x+c.height/2,y:e,fillStyle:s.line.title.fillStyle||s.line.strokeStyle,rotationRads:Math.PI/2})}}}else{if(u==="radial"){for(q=0,f=d.length;q<f;q++){s=k(d[q]);!s.line&&(s.line={});l=Ext.isString(s.value)?a.getCoordFor(s.value):s.value;if(l>m.max){continue}l=l/m.max*m.length;s.line.cx=m.centerX;s.line.cy=m.centerY;s.line.scalingX=l;s.line.scalingY=l;s.line.strokeStyle=s.line.strokeStyle||m.strokeStyle;t.putMarker("circular-limit-lines",s.line,q,true);if(s.line.title){v.createInstance(s.line.title);c=v.getBBoxFor(v.position-1);v.setAttributesFor(v.position-1,{x:m.centerX,y:m.centerY-l-c.height/2,fillStyle:s.line.title.fillStyle||s.line.strokeStyle})}}}else{if(u==="angular"){for(q=0,f=d.length;q<f;q++){s=k(d[q]);!s.line&&(s.line={});l=Ext.isString(s.value)?a.getCoordFor(s.value):s.value;l=l/(m.max+1)*Math.PI*2+m.baseRotation;s.line.translationX=m.centerX;s.line.translationY=m.centerY;s.line.rotationRads=l;s.line.rotationCenterX=0;s.line.rotationCenterY=0;s.line.scalingX=m.length;s.line.scalingY=m.length;s.line.strokeStyle=s.line.strokeStyle||m.strokeStyle;t.putMarker("radial-limit-lines",s.line,q,true);if(s.line.title){v.createInstance(s.line.title);c=v.getBBoxFor(v.position-1);b=((l>-0.5*Math.PI&&l<0.5*Math.PI)||(l>1.5*Math.PI&&l<2*Math.PI))?1:-1;v.setAttributesFor(v.position-1,{x:m.centerX+0.5*m.length*Math.cos(l)+b*c.height/2*Math.sin(l),y:m.centerY+0.5*m.length*Math.sin(l)-b*c.height/2*Math.cos(l),rotationRads:b===1?l:l-Math.PI,fillStyle:s.line.title.fillStyle||s.line.strokeStyle})}}}else{if(u==="gauge"){}}}}}},doThicknessChanged:function(){var a=this.getAxis();if(a){a.onThicknessChanged()}},render:function(a,c,d){var e=this,b=e.getLayoutContext();if(b){if(false===e.renderLabels(a,c,b,d)){return false}c.beginPath();e.renderTicks(a,c,b,d);e.renderAxisLine(a,c,b,d);e.renderGridLines(a,c,b,d);e.renderLimits(d);c.stroke()}}});Ext.define("Ext.chart.axis.segmenter.Segmenter",{config:{axis:null},constructor:function(a){this.initConfig(a)},renderer:function(b,a){return String(b)},from:function(a){return a},diff:Ext.emptyFn,align:Ext.emptyFn,add:Ext.emptyFn,preferredStep:Ext.emptyFn});Ext.define("Ext.chart.axis.segmenter.Names",{extend:Ext.chart.axis.segmenter.Segmenter,alias:"segmenter.names",renderer:function(b,a){return b},diff:function(b,a,c){return Math.floor(a-b)},align:function(c,b,a){return Math.floor(c)},add:function(c,b,a){return c+b},preferredStep:function(c,a,b,d){return{unit:1,step:1}}});Ext.define("Ext.chart.axis.segmenter.Numeric",{extend:Ext.chart.axis.segmenter.Segmenter,alias:"segmenter.numeric",isNumeric:true,renderer:function(b,a){return b.toFixed(Math.max(0,a.majorTicks.unit.fixes))},diff:function(b,a,c){return Math.floor((a-b)/c.scale)},align:function(c,b,a){return Math.floor(c/(a.scale*b))*a.scale*b},add:function(c,b,a){return c+b*a.scale},preferredStep:function(c,b){var a=Math.floor(Math.log(b)*Math.LOG10E),d=Math.pow(10,a);b/=d;if(b<2){b=2}else{if(b<5){b=5}else{if(b<10){b=10;a++}}}return{unit:{fixes:-a,scale:d},step:b}},exactStep:function(c,b){var a=Math.floor(Math.log(b)*Math.LOG10E),d=Math.pow(10,a);return{unit:{fixes:-a+(b%d===0?0:1),scale:1},step:b}},adjustByMajorUnit:function(e,g,c){var d=c[0],b=c[1],a=e*g,f=d%a;if(f!==0){c[0]=d-f+(d<0?-a:0)}f=b%a;if(f!==0){c[1]=b-f+(b>0?a:0)}}});Ext.define("Ext.chart.axis.segmenter.Time",{extend:Ext.chart.axis.segmenter.Segmenter,alias:"segmenter.time",config:{step:null},renderer:function(c,b){var a=Ext.Date;switch(b.majorTicks.unit){case"y":return a.format(c,"Y");case"mo":return a.format(c,"Y-m");case"d":return a.format(c,"Y-m-d")}return a.format(c,"Y-m-d\nH:i:s")},from:function(a){return new Date(a)},diff:function(b,a,c){if(isFinite(b)){b=new Date(b)}if(isFinite(a)){a=new Date(a)}return Ext.Date.diff(b,a,c)},align:function(a,c,b){if(b==="d"&&c>=7){a=Ext.Date.align(a,"d",c);a.setDate(a.getDate()-a.getDay()+1);return a}else{return Ext.Date.align(a,b,c)}},add:function(c,b,a){return Ext.Date.add(new Date(c),a,b)},stepUnits:[[Ext.Date.YEAR,1,2,5,10,20,50,100,200,500],[Ext.Date.MONTH,1,3,6],[Ext.Date.DAY,1,7,14],[Ext.Date.HOUR,1,6,12],[Ext.Date.MINUTE,1,5,15,30],[Ext.Date.SECOND,1,5,15,30],[Ext.Date.MILLI,1,2,5,10,20,50,100,200,500]],preferredStep:function(b,e){if(this.getStep()){return this.getStep()}var f=new Date(+b),g=new Date(+b+Math.ceil(e)),d=this.stepUnits,l,k,h,c,a;for(c=0;c<d.length;c++){k=d[c][0];h=this.diff(f,g,k);if(h>0){for(a=1;a<d[c].length;a++){if(h<=d[c][a]){l={unit:k,step:d[c][a]};break}}if(!l){c--;l={unit:d[c][0],step:1}}break}}if(!l){l={unit:Ext.Date.DAY,step:1}}return l}});Ext.define("Ext.chart.axis.layout.Layout",{mixins:{observable:Ext.mixin.Observable},config:{axis:null},constructor:function(a){this.mixins.observable.constructor.call(this,a)},processData:function(b){var e=this,c=e.getAxis(),f=c.getDirection(),g=c.boundSeries,a,d;if(b){b["coordinate"+f]()}else{for(a=0,d=g.length;a<d;a++){g[a]["coordinate"+f]()}}},calculateMajorTicks:function(a){var f=this,e=a.attr,d=e.max-e.min,i=d/Math.max(1,e.length)*(e.visibleMax-e.visibleMin),h=e.min+d*e.visibleMin,b=e.min+d*e.visibleMax,g=e.estStepSize*i,c=f.snapEnds(a,e.min,e.max,g);if(c){f.trimByRange(a,c,h,b);a.majorTicks=c}},calculateMinorTicks:function(a){if(this.snapMinorEnds){a.minorTicks=this.snapMinorEnds(a)}},calculateLayout:function(b){var c=this,a=b.attr;if(a.length===0){return null}if(a.majorTicks){c.calculateMajorTicks(b);if(a.minorTicks){c.calculateMinorTicks(b)}}},snapEnds:Ext.emptyFn,trimByRange:function(b,f,i,a){var g=b.segmenter,j=f.unit,h=g.diff(f.from,i,j),d=g.diff(f.from,a,j),c=Math.max(0,Math.ceil(h/f.step)),e=Math.min(f.steps,Math.floor(d/f.step));if(e<f.steps){f.to=g.add(f.from,e*f.step,j)}if(f.max>a){f.max=f.to}if(f.from<i){f.from=g.add(f.from,c*f.step,j);while(f.from<i){c++;f.from=g.add(f.from,f.step,j)}}if(f.min<i){f.min=f.from}f.steps=e-c}});Ext.define("Ext.chart.axis.layout.Discrete",{extend:Ext.chart.axis.layout.Layout,alias:"axisLayout.discrete",isDiscrete:true,processData:function(){var f=this,d=f.getAxis(),c=d.boundSeries,g=d.getDirection(),b,e,a;f.labels=[];f.labelMap={};for(b=0,e=c.length;b<e;b++){a=c[b];if(a["get"+g+"Axis"]()===d){a["coordinate"+g]()}}d.getSprites()[0].setAttributes({data:f.labels});f.fireEvent("datachange",f.labels)},calculateLayout:function(a){a.data=this.labels;this.callParent([a])},calculateMajorTicks:function(a){var g=this,f=a.attr,d=a.data,e=f.max-f.min,j=e/Math.max(1,f.length)*(f.visibleMax-f.visibleMin),i=f.min+e*f.visibleMin,b=f.min+e*f.visibleMax,h=f.estStepSize*j;var c=g.snapEnds(a,Math.max(0,f.min),Math.min(f.max,d.length-1),h);if(c){g.trimByRange(a,c,i,b);a.majorTicks=c}},snapEnds:function(e,d,a,b){b=Math.ceil(b);var c=Math.floor((a-d)/b),f=e.data;return{min:d,max:a,from:d,to:c*b+d,step:b,steps:c,unit:1,getLabel:function(g){return f[this.from+this.step*g]},get:function(g){return this.from+this.step*g}}},trimByRange:function(b,f,h,a){var i=f.unit,g=Math.ceil((h-f.from)/i)*i,d=Math.floor((a-f.from)/i)*i,c=Math.max(0,Math.ceil(g/f.step)),e=Math.min(f.steps,Math.floor(d/f.step));if(e<f.steps){f.to=e}if(f.max>a){f.max=f.to}if(f.from<h&&f.step>0){f.from=f.from+c*f.step*i;while(f.from<h){c++;f.from+=f.step*i}}if(f.min<h){f.min=f.from}f.steps=e-c},getCoordFor:function(c,d,a,b){this.labels.push(c);return this.labels.length-1}});Ext.define("Ext.chart.axis.layout.CombineDuplicate",{extend:Ext.chart.axis.layout.Discrete,alias:"axisLayout.combineDuplicate",getCoordFor:function(d,e,b,c){if(!(d in this.labelMap)){var a=this.labelMap[d]=this.labels.length;this.labels.push(d);return a}return this.labelMap[d]}});Ext.define("Ext.chart.axis.layout.Continuous",{extend:Ext.chart.axis.layout.Layout,alias:"axisLayout.continuous",isContinuous:true,config:{adjustMinimumByMajorUnit:false,adjustMaximumByMajorUnit:false},getCoordFor:function(c,d,a,b){return +c},snapEnds:function(a,d,i,h){var f=a.segmenter,c=this.getAxis(),l=c.getMajorTickSteps(),e=l&&f.exactStep?f.exactStep(d,(i-d)/l):f.preferredStep(d,h),k=e.unit,b=e.step,j=f.align(d,b,k),g=(l||f.diff(d,i,k))+1;return{min:f.from(d),max:f.from(i),from:j,to:f.add(j,g*b,k),step:b,steps:g,unit:k,get:function(m){return f.add(this.from,this.step*m,k)}}},snapMinorEnds:function(a){var e=a.majorTicks,m=this.getAxis().getMinorTickSteps(),f=a.segmenter,d=e.min,i=e.max,k=e.from,l=e.unit,b=e.step/m,n=b*l.scale,j=k-d,c=Math.floor(j/n),h=c+Math.floor((i-e.to)/n)+1,g=e.steps*m+h;return{min:d,max:i,from:d+j%n,to:f.add(k,g*b,l),step:b,steps:g,unit:l,get:function(o){return(o%m+c+1!==0)?f.add(this.from,this.step*o,l):null}}}});Ext.define("Ext.chart.axis.Axis",{xtype:"axis",mixins:{observable:Ext.mixin.Observable},isAxis:true,config:{position:"bottom",fields:[],label:undefined,grid:false,limits:null,renderer:null,chart:null,style:null,margin:0,titleMargin:4,background:null,minimum:NaN,maximum:NaN,reconcileRange:false,minZoom:1,maxZoom:10000,layout:"continuous",segmenter:"numeric",hidden:false,majorTickSteps:0,minorTickSteps:0,adjustByMajorUnit:true,title:null,increment:0.5,length:0,center:null,radius:null,totalAngle:Math.PI,rotation:null,labelInSpan:null,visibleRange:[0,1],needHighPrecision:false,linkedTo:null,floating:null},titleOffset:0,spriteAnimationCount:0,prevMin:0,prevMax:1,boundSeries:[],sprites:null,surface:null,range:null,xValues:[],yValues:[],masterAxis:null,applyRotation:function(b){var a=Math.PI*2;return(b%a+Math.PI)%a-Math.PI},updateRotation:function(b){var c=this.getSprites(),a=this.getPosition();if(!this.getHidden()&&a==="angular"&&c[0]){c[0].setAttributes({baseRotation:b})}},applyTitle:function(c,b){var a;if(Ext.isString(c)){c={text:c}}if(!b){b=Ext.create("sprite.text",c);if((a=this.getSurface())){a.add(b)}}else{b.setAttributes(c)}return b},applyFloating:function(b,a){if(b===null){b={value:null,alongAxis:null}}else{if(Ext.isNumber(b)){b={value:b,alongAxis:null}}}if(Ext.isObject(b)){if(a&&a.alongAxis){delete this.getChart().getAxis(a.alongAxis).floatingAxes[this.getId()]}return b}return a},constructor:function(a){var b=this,c;b.sprites=[];b.labels=[];b.floatingAxes={};a=a||{};if(a.position==="angular"){a.style=a.style||{};a.style.estStepSize=1}if("id" in a){c=a.id}else{if("id" in b.config){c=b.config.id}else{c=b.getId()}}b.setId(c);b.mixins.observable.constructor.apply(b,arguments)},getAlignment:function(){switch(this.getPosition()){case"left":case"right":return"vertical";case"top":case"bottom":return"horizontal";case"radial":return"radial";case"angular":return"angular"}},getGridAlignment:function(){switch(this.getPosition()){case"left":case"right":return"horizontal";case"top":case"bottom":return"vertical";case"radial":return"circular";case"angular":return"radial"}},getSurface:function(){var e=this,d=e.getChart();if(d&&!e.surface){var b=e.surface=d.getSurface(e.getId(),"axis"),c=e.gridSurface=d.getSurface("main"),a=e.getSprites()[0],f=e.getGridAlignment();c.waitFor(b);e.getGrid();if(e.getLimits()&&f){f=f.replace("3d","");e.limits={surface:d.getSurface("overlay"),lines:new Ext.chart.Markers(),titles:new Ext.draw.sprite.Instancing()};e.limits.lines.setTemplate({xclass:"grid."+f});e.limits.lines.getTemplate().setAttributes({strokeStyle:"black"},true);e.limits.surface.add(e.limits.lines);a.bindMarker(f+"-limit-lines",e.limits.lines);e.limitTitleTpl=new Ext.draw.sprite.Text();e.limits.titles.setTemplate(e.limitTitleTpl);e.limits.surface.add(e.limits.titles);d.on("redraw",e.renderLimits,e)}}return e.surface},applyGrid:function(a){if(a===true){return{}}return a},updateGrid:function(b){var e=this,d=e.getChart();if(!d){e.on({chartattached:Ext.bind(e.updateGrid,e,[b]),single:true});return}var c=e.gridSurface,a=e.getSprites()[0],f=e.getGridAlignment(),g;if(b){g=e.gridSpriteEven;if(!g){g=e.gridSpriteEven=new Ext.chart.Markers();g.setTemplate({xclass:"grid."+f});c.add(g);a.bindMarker(f+"-even",g)}if(Ext.isObject(b)){g.getTemplate().setAttributes(b);if(Ext.isObject(b.even)){g.getTemplate().setAttributes(b.even)}}g=e.gridSpriteOdd;if(!g){g=e.gridSpriteOdd=new Ext.chart.Markers();g.setTemplate({xclass:"grid."+f});c.add(g);a.bindMarker(f+"-odd",g)}if(Ext.isObject(b)){g.getTemplate().setAttributes(b);if(Ext.isObject(b.odd)){g.getTemplate().setAttributes(b.odd)}}}},renderLimits:function(){this.getSprites()[0].renderLimits()},getCoordFor:function(c,d,a,b){return this.getLayout().getCoordFor(c,d,a,b)},applyPosition:function(a){return a.toLowerCase()},applyLength:function(b,a){return b>0?b:a},applyLabel:function(b,a){if(!a){a=new Ext.draw.sprite.Text({})}if(this.limitTitleTpl){this.limitTitleTpl.setAttributes(b)}a.setAttributes(b);return a},applyLayout:function(b,a){b=Ext.factory(b,null,a,"axisLayout");b.setAxis(this);return b},applySegmenter:function(a,b){a=Ext.factory(a,null,b,"segmenter");a.setAxis(this);return a},updateMinimum:function(){this.range=null},updateMaximum:function(){this.range=null},hideLabels:function(){this.getSprites()[0].setDirty(true);this.setLabel({hidden:true})},showLabels:function(){this.getSprites()[0].setDirty(true);this.setLabel({hidden:false})},renderFrame:function(){this.getSurface().renderFrame()},updateChart:function(d,b){var c=this,a;if(b){b.unregister(c);b.un("serieschange",c.onSeriesChange,c);b.un("redraw",c.renderLimits,c);c.linkAxis();c.fireEvent("chartdetached",b,c)}if(d){d.on("serieschange",c.onSeriesChange,c);c.surface=null;a=c.getSurface();c.getLabel().setSurface(a);a.add(c.getSprites());a.add(c.getTitle());d.register(c);c.fireEvent("chartattached",d,c)}},applyBackground:function(a){var b=Ext.ClassManager.getByAlias("sprite.rect");return b.def.normalize(a)},processData:function(){this.getLayout().processData();this.range=null},getDirection:function(){return this.getChart().getDirectionForAxis(this.getPosition())},isSide:function(){var a=this.getPosition();return a==="left"||a==="right"},applyFields:function(a){return Ext.Array.from(a)},applyVisibleRange:function(a,c){this.getChart();if(a[0]>a[1]){var b=a[0];a[0]=a[1];a[0]=b}if(a[1]===a[0]){a[1]+=1/this.getMaxZoom()}if(a[1]>a[0]+1){a[0]=0;a[1]=1}else{if(a[0]<0){a[1]-=a[0];a[0]=0}else{if(a[1]>1){a[0]-=a[1]-1;a[1]=1}}}if(c&&a[0]===c[0]&&a[1]===c[1]){return undefined}return a},updateVisibleRange:function(a){this.fireEvent("visiblerangechange",this,a)},onSeriesChange:function(e){var f=this,b=e.getSeries(),j="get"+f.getDirection()+"Axis",g=[],c,d=b.length,a,h;for(c=0;c<d;c++){if(this===b[c][j]()){g.push(b[c])}}f.boundSeries=g;a=f.getLinkedTo();h=!Ext.isEmpty(a)&&e.getAxis(a);if(h){f.linkAxis(h)}else{f.getLayout().processData()}},linkAxis:function(a){var c=this;function b(f,d,e){e.getLayout()[f]("datachange","onDataChange",d);e[f]("rangechange","onMasterAxisRangeChange",d)}if(c.masterAxis){b("un",c,c.masterAxis);c.masterAxis=null}if(a){if(a.type!==this.type){Ext.Error.raise("Linked axes must be of the same type.")}b("on",c,a);c.onDataChange(a.getLayout().labels);c.onMasterAxisRangeChange(a,a.range);c.setStyle(Ext.apply({},c.config.style,a.config.style));c.setTitle(Ext.apply({},c.config.title,a.config.title));c.setLabel(Ext.apply({},c.config.label,a.config.label));c.masterAxis=a}},onDataChange:function(a){this.getLayout().labels=a},onMasterAxisRangeChange:function(b,a){this.range=a},applyRange:function(a){if(!a){return this.dataRange.slice(0)}else{return[a[0]===null?this.dataRange[0]:a[0],a[1]===null?this.dataRange[1]:a[1]]}},getRange:function(){var m=this;if(m.range){return m.range}else{if(m.masterAxis){return m.masterAxis.range}}if(Ext.isNumber(m.getMinimum()+m.getMaximum())){return m.range=[m.getMinimum(),m.getMaximum()]}var d=Infinity,n=-Infinity,o=m.boundSeries,h=m.getLayout(),l=m.getSegmenter(),p=m.getVisibleRange(),b="get"+m.getDirection()+"Range",a,j,g,f,e,k;for(e=0,k=o.length;e<k;e++){f=o[e];var c=f[b]();if(c){if(c[0]<d){d=c[0]}if(c[1]>n){n=c[1]}}}if(!isFinite(n)){n=m.prevMax}if(!isFinite(d)){d=m.prevMin}if(m.getLabelInSpan()||d===n){n+=m.getIncrement();d-=m.getIncrement()}if(Ext.isNumber(m.getMinimum())){d=m.getMinimum()}else{m.prevMin=d}if(Ext.isNumber(m.getMaximum())){n=m.getMaximum()}else{m.prevMax=n}m.range=[Ext.Number.correctFloat(d),Ext.Number.correctFloat(n)];if(m.getReconcileRange()){m.reconcileRange()}if(m.getAdjustByMajorUnit()&&l.adjustByMajorUnit&&!m.getMajorTickSteps()){j=Ext.Object.chain(m.getSprites()[0].attr);j.min=m.range[0];j.max=m.range[1];j.visibleMin=p[0];j.visibleMax=p[1];a={attr:j,segmenter:l};h.calculateLayout(a);g=a.majorTicks;if(g){l.adjustByMajorUnit(g.step,g.unit.scale,m.range);j.min=m.range[0];j.max=m.range[1];delete a.majorTicks;h.calculateLayout(a);g=a.majorTicks;l.adjustByMajorUnit(g.step,g.unit.scale,m.range)}else{if(!m.hasClearRangePending){m.hasClearRangePending=true;m.getChart().on("layout","clearRange",m)}}}if(!Ext.Array.equals(m.range,m.oldRange||[])){m.fireEvent("rangechange",m,m.range);m.oldRange=m.range}return m.range},clearRange:function(){delete this.hasClearRangePending;this.range=null},reconcileRange:function(){var e=this,g=e.getChart().getAxes(),f=e.getDirection(),b,d,c,a;if(!g){return}for(b=0,d=g.length;b<d;b++){c=g[b];a=c.getRange();if(c===e||c.getDirection()!==f||!a||!c.getReconcileRange()){continue}if(a[0]<e.range[0]){e.range[0]=a[0]}if(a[1]>e.range[1]){e.range[1]=a[1]}}},applyStyle:function(c,b){var a=Ext.ClassManager.getByAlias("sprite."+this.seriesType);if(a&&a.def){c=a.def.normalize(c)}b=Ext.apply(b||{},c);return b},themeOnlyIfConfigured:{grid:true},updateTheme:function(d){var i=this,k=d.getAxis(),e=i.getPosition(),o=i.getInitialConfig(),c=i.defaultConfig,g=i.getConfigurator().configs,a=k.defaults,n=k[e],h=i.themeOnlyIfConfigured,l,j,p,b,m,f;k=Ext.merge({},a,n);for(l in k){j=k[l];f=g[l];if(j!==null&&j!==undefined&&f){m=o[l];p=Ext.isObject(j);b=m===c[l];if(p){if(b&&h[l]){continue}j=Ext.merge({},j,m)}if(b||p){i[f.names.set](j)}}}},updateCenter:function(b){var e=this.getSprites(),a=e[0],d=b[0],c=b[1];if(a){a.setAttributes({centerX:d,centerY:c})}if(this.gridSpriteEven){this.gridSpriteEven.getTemplate().setAttributes({translationX:d,translationY:c,rotationCenterX:d,rotationCenterY:c})}if(this.gridSpriteOdd){this.gridSpriteOdd.getTemplate().setAttributes({translationX:d,translationY:c,rotationCenterX:d,rotationCenterY:c})}},getSprites:function(){if(!this.getChart()){return}var i=this,e=i.getRange(),f=i.getPosition(),g=i.getChart(),c=g.getAnimation(),d,a,b=i.getLength(),h=i.superclass;if(c===false){c={duration:0}}if(e){a=Ext.applyIf({position:f,axis:i,min:e[0],max:e[1],length:b,grid:i.getGrid(),hidden:i.getHidden(),titleOffset:i.titleOffset,layout:i.getLayout(),segmenter:i.getSegmenter(),totalAngle:i.getTotalAngle(),label:i.getLabel()},i.getStyle());if(!i.sprites.length){while(!h.xtype){h=h.superclass}d=Ext.create("sprite."+h.xtype,a);d.fx.setCustomDurations({baseRotation:0});d.fx.on("animationstart","onAnimationStart",i);d.fx.on("animationend","onAnimationEnd",i);d.setLayout(i.getLayout());d.setSegmenter(i.getSegmenter());d.setLabel(i.getLabel());i.sprites.push(d);i.updateTitleSprite()}else{d=i.sprites[0];d.setAnimation(c);d.setAttributes(a)}if(i.getRenderer()){d.setRenderer(i.getRenderer())}}return i.sprites},updateTitleSprite:function(){var f=this,b=f.getLength();if(!f.sprites[0]||!Ext.isNumber(b)){return}var h=this.sprites[0].thickness,a=f.getSurface(),g=f.getTitle(),e=f.getPosition(),c=f.getMargin(),i=f.getTitleMargin(),d=a.roundPixel(b/2);if(g){switch(e){case"top":g.setAttributes({x:d,y:c+i/2,textBaseline:"top",textAlign:"center"},true);g.applyTransformations();f.titleOffset=g.getBBox().height+i;break;case"bottom":g.setAttributes({x:d,y:h+i/2,textBaseline:"top",textAlign:"center"},true);g.applyTransformations();f.titleOffset=g.getBBox().height+i;break;case"left":g.setAttributes({x:c+i/2,y:d,textBaseline:"top",textAlign:"center",rotationCenterX:c+i/2,rotationCenterY:d,rotationRads:-Math.PI/2},true);g.applyTransformations();f.titleOffset=g.getBBox().width+i;break;case"right":g.setAttributes({x:h-c+i/2,y:d,textBaseline:"bottom",textAlign:"center",rotationCenterX:h+i/2,rotationCenterY:d,rotationRads:Math.PI/2},true);g.applyTransformations();f.titleOffset=g.getBBox().width+i;break}}},onThicknessChanged:function(){this.getChart().onThicknessChanged()},getThickness:function(){if(this.getHidden()){return 0}return(this.sprites[0]&&this.sprites[0].thickness||1)+this.titleOffset+this.getMargin()},onAnimationStart:function(){this.spriteAnimationCount++;if(this.spriteAnimationCount===1){this.fireEvent("animationstart",this)}},onAnimationEnd:function(){this.spriteAnimationCount--;if(this.spriteAnimationCount===0){this.fireEvent("animationend",this)}},getItemId:function(){return this.getId()},getAncestorIds:function(){return[this.getChart().getId()]},isXType:function(a){return a==="axis"},resolveListenerScope:function(e){var d=this,a=Ext._namedScopes[e],c=d.getChart(),b;if(!a){b=c?c.resolveListenerScope(e,false):(e||d)}else{if(a.isThis){b=d}else{if(a.isController){b=c?c.resolveListenerScope(e,false):d}else{if(a.isSelf){b=c?c.resolveListenerScope(e,false):d;if(b===c&&!c.getInheritedConfig("defaultListenerScope")){b=d}}}}}return b},destroy:function(){var a=this;a.setChart(null);a.surface.destroy();a.surface=null;a.callParent()}});Ext.define("Ext.chart.LegendBase",{extend:Ext.view.View,config:{tpl:['<div class="',Ext.baseCSSPrefix,'legend-container">','<tpl for=".">','<div class="',Ext.baseCSSPrefix,'legend-item">',"<span ",'class="',Ext.baseCSSPrefix,"legend-item-marker {[ values.disabled ? Ext.baseCSSPrefix + 'legend-inactive' : '' ]}\" ",'style="background:{mark};">',"</span>{name}","</div>","</tpl>","</div>"],nodeContainerSelector:"div."+Ext.baseCSSPrefix+"legend-container",itemSelector:"div."+Ext.baseCSSPrefix+"legend-item",docked:"bottom"},setDocked:function(d){var c=this,a=c.ownerCt,b;c.docked=d;switch(d){case"top":case"bottom":c.addCls(Ext.baseCSSPrefix+"horizontal");b="hbox";break;case"left":case"right":c.removeCls(Ext.baseCSSPrefix+"horizontal");b="vbox";break}if(a){a.setDocked(d)}},setStore:function(a){this.bindStore(a)},clearViewEl:function(){this.callParent(arguments);Ext.removeNode(this.getNodeContainer())},onItemClick:function(a,c,b,d){this.callParent(arguments);this.toggleItem(b)}});Ext.define("Ext.chart.Legend",{xtype:"legend",extend:Ext.chart.LegendBase,config:{baseCls:Ext.baseCSSPrefix+"legend",padding:5,rect:null,disableSelection:true,toggleable:true},toggleItem:function(c){if(!this.getToggleable()){return}var b=this.getStore(),h=0,e,g=true,d,f,a;if(b){f=b.getCount();for(d=0;d<f;d++){a=b.getAt(d);if(a.get("disabled")){h++}}g=f-h>1;a=b.getAt(c);if(a){e=a.get("disabled");if(e||g){a.set("disabled",!e)}}}}});Ext.define("Ext.chart.AbstractChart",{extend:Ext.draw.Container,isChart:true,defaultBindProperty:"store",config:{store:"ext-empty-store",theme:"default",style:null,animation:!Ext.isIE8,series:[],axes:[],legend:null,colors:null,insetPadding:{top:10,left:10,right:10,bottom:10},background:null,interactions:[],mainRect:null,resizeHandler:null,highlightItem:null},animationSuspendCount:0,chartLayoutSuspendCount:0,axisThicknessSuspendCount:0,isThicknessChanged:false,surfaceZIndexes:{background:0,main:1,grid:2,series:3,axis:4,chart:5,overlay:6,events:7},constructor:function(a){var b=this;b.itemListeners={};b.surfaceMap={};b.chartComponents={};b.isInitializing=true;b.suspendChartLayout();b.animationSuspendCount++;b.callParent(arguments);delete b.isInitializing;b.getSurface("main");b.getSurface("chart").setFlipRtlText(b.getInherited().rtl);b.getSurface("overlay").waitFor(b.getSurface("series"));b.animationSuspendCount--;b.resumeChartLayout()},applyAnimation:function(a,b){if(!a){a={duration:0}}else{if(a===true){a={easing:"easeInOut",duration:500}}}return b?Ext.apply({},a,b):a},getAnimation:function(){if(this.animationSuspendCount){return{duration:0}}else{return this.callParent()}},applyInsetPadding:function(b,a){if(!Ext.isObject(b)){return Ext.util.Format.parseBox(b)}else{if(!a){return b}else{return Ext.apply(a,b)}}},suspendAnimation:function(){var d=this,c=d.getSeries(),e=c.length,b=-1,a;d.animationSuspendCount++;if(d.animationSuspendCount===1){while(++b<e){a=c[b];a.setAnimation(a.getAnimation())}}},resumeAnimation:function(){var d=this,c=d.getSeries(),f=c.length,b=-1,a,e;d.animationSuspendCount--;if(d.animationSuspendCount===0){while(++b<f){a=c[b];e=a.getAnimation();a.setAnimation(e.duration&&e||d.getAnimation())}}},suspendChartLayout:function(){this.chartLayoutSuspendCount++;if(this.chartLayoutSuspendCount===1){if(this.scheduledLayoutId){this.layoutInSuspension=true;this.cancelChartLayout()}else{this.layoutInSuspension=false}}},resumeChartLayout:function(){this.chartLayoutSuspendCount--;if(this.chartLayoutSuspendCount===0){if(this.layoutInSuspension){this.scheduleLayout()}}},cancelChartLayout:function(){if(this.scheduledLayoutId){Ext.draw.Animator.cancel(this.scheduledLayoutId);this.scheduledLayoutId=null}},scheduleLayout:function(){var a=this;if(a.allowSchedule()&&!a.scheduledLayoutId){a.scheduledLayoutId=Ext.draw.Animator.schedule("doScheduleLayout",a)}},allowSchedule:function(){return true},doScheduleLayout:function(){if(this.chartLayoutSuspendCount){this.layoutInSuspension=true}else{this.performLayout()}},suspendThicknessChanged:function(){this.axisThicknessSuspendCount++},resumeThicknessChanged:function(){if(this.axisThicknessSuspendCount>0){this.axisThicknessSuspendCount--;if(this.axisThicknessSuspendCount===0&&this.isThicknessChanged){this.onThicknessChanged()}}},onThicknessChanged:function(){if(this.axisThicknessSuspendCount===0){this.isThicknessChanged=false;this.performLayout()}else{this.isThicknessChanged=true}},applySprites:function(b){var a=this.getSurface("chart");b=Ext.Array.from(b);a.removeAll(true);a.add(b);return b},initItems:function(){var a=this.items,b,d,c;if(a&&!a.isMixedCollection){this.items=[];a=Ext.Array.from(a);for(b=0,d=a.length;b<d;b++){c=a[b];if(c.type){Ext.raise("To add custom sprites to the chart use the 'sprites' config.")}else{this.items.push(c)}}}this.callParent()},applyBackground:function(c,e){var b=this.getSurface("background"),d,a,f;if(c){if(e){d=e.attr.width;a=e.attr.height;f=e.type===(c.type||"rect")}if(c.isSprite){e=c}else{if(c.type==="image"&&Ext.isString(c.src)){if(f){e.setAttributes({src:c.src})}else{b.remove(e,true);e=b.add(c)}}else{if(f){e.setAttributes({fillStyle:c})}else{b.remove(e,true);e=b.add({type:"rect",fillStyle:c,fx:{customDurations:{x:0,y:0,width:0,height:0}}})}}}}if(d&&a){e.setAttributes({width:d,height:a})}e.setAnimation(this.getAnimation());return e},getLegendStore:function(){return this.legendStore},refreshLegendStore:function(){if(this.getLegendStore()){var d,e,c=this.getSeries(),b,a=[];if(c){for(d=0,e=c.length;d<e;d++){b=c[d];if(b.getShowInLegend()){b.provideLegendInfo(a)}}}this.getLegendStore().setData(a)}},resetLegendStore:function(){var c=this.getLegendStore(),e,d,a,b;if(c){e=this.getLegendStore().getData().items;for(d=0,a=e.length;d<a;d++){b=e[d];b.beginEdit();b.set("disabled",false);b.commit()}}},onUpdateLegendStore:function(b,a){var d=this.getSeries(),c;if(a&&d){c=d.map[a.get("series")];if(c){c.setHiddenByIndex(a.get("index"),a.get("disabled"));this.redraw()}}},defaultResizeHandler:function(a){this.scheduleLayout();return false},applyMainRect:function(a,b){if(!b){return a}this.getSeries();this.getAxes();if(a[0]===b[0]&&a[1]===b[1]&&a[2]===b[2]&&a[3]===b[3]){return b}else{return a}},register:function(a){var b=this.chartComponents,c=a.getId();b[c]=a},unregister:function(a){var b=this.chartComponents,c=a.getId();delete b[c]},get:function(a){return this.chartComponents[a]},getAxis:function(a){if(a instanceof Ext.chart.axis.Axis){return a}else{if(Ext.isNumber(a)){return this.getAxes()[a]}else{if(Ext.isString(a)){return this.get(a)}}}},getSurface:function(b,c){b=b||"main";c=c||b;var d=this,a=this.callParent([b]),f=d.surfaceZIndexes,e=d.surfaceMap;if(c in f){a.element.setStyle("zIndex",f[c])}if(!e[c]){e[c]=[]}if(Ext.Array.indexOf(e[c],a)<0){a.type=c;e[c].push(a);a.on("destroy",d.forgetSurface,d)}return a},forgetSurface:function(a){var d=this.surfaceMap;if(!d||this.isDestroying){return}var c=d[a.type],b=c?Ext.Array.indexOf(c,a):-1;if(b>=0){c.splice(b,1)}},applyAxes:function(b,k){var l=this,g={left:"right",right:"left"},m=[],c,d,e,a,f,h,j;l.animationSuspendCount++;l.getStore();if(!k){k=[];k.map={}}j=k.map;m.map={};b=Ext.Array.from(b,true);for(f=0,h=b.length;f<h;f++){c=b[f];if(!c){continue}if(c instanceof Ext.chart.axis.Axis){d=j[c.getId()];c.setChart(l)}else{c=Ext.Object.chain(c);e=c.linkedTo;a=c.id;if(Ext.isNumber(e)){c=Ext.merge({},b[e],c)}else{if(Ext.isString(e)){Ext.Array.each(b,function(i){if(i.id===c.linkedTo){c=Ext.merge({},i,c);return false}})}}c.id=a;c.chart=l;if(l.getInherited().rtl){c.position=g[c.position]||c.position}a=c.getId&&c.getId()||c.id;c=Ext.factory(c,null,d=j[a],"axis")}if(c){m.push(c);m.map[c.getId()]=c;if(!d){c.on("animationstart","onAnimationStart",l);c.on("animationend","onAnimationEnd",l)}}}for(f in j){if(!m.map[f]){j[f].destroy()}}l.animationSuspendCount--;return m},updateAxes:function(){if(!this.isDestroying){this.scheduleLayout()}},circularCopyArray:function(e,f,d){var c=[],b,a=e&&e.length;if(a){for(b=0;b<d;b++){c.push(e[(f+b)%a])}}return c},circularCopyObject:function(f,g,d){var c=this,b,e,a={};if(d){for(b in f){if(f.hasOwnProperty(b)){e=f[b];if(Ext.isArray(e)){a[b]=c.circularCopyArray(e,g,d)}else{a[b]=e}}}}return a},getColors:function(){var b=this,a=b.config.colors,c=b.getTheme();if(Ext.isArray(a)&&a.length>0){a=b.applyColors(a)}return a||(c&&c.getColors())},applyColors:function(a){a=Ext.Array.map(a,function(b){if(Ext.isString(b)){return b}else{return b.toString()}});return a},updateColors:function(c){var k=this,e=k.getTheme(),a=c||(e&&e.getColors()),l=0,f=k.getSeries(),d=f&&f.length,g,j,b,h;if(a.length){for(g=0;g<d;g++){j=f[g];h=j.themeColorCount();b=k.circularCopyArray(a,l,h);l+=h;j.updateChartColors(b)}}k.refreshLegendStore()},applyTheme:function(a){if(a&&a.isTheme){return a}return Ext.Factory.chartTheme(a)},updateTheme:function(g){var e=this,f=e.getAxes(),d=e.getSeries(),a=e.getColors(),c,b;e.updateChartTheme(g);for(b=0;b<f.length;b++){f[b].updateTheme(g)}for(b=0;b<d.length;b++){c=d[b];c.updateTheme(g)}e.updateSpriteTheme(g);e.updateColors(a);e.redraw()},themeOnlyIfConfigured:{},updateChartTheme:function(c){var i=this,k=c.getChart(),n=i.getInitialConfig(),b=i.defaultConfig,e=i.getConfigurator().configs,f=k.defaults,g=k[i.xtype],h=i.themeOnlyIfConfigured,l,j,o,a,m,d;k=Ext.merge({},f,g);for(l in k){j=k[l];d=e[l];if(j!==null&&j!==undefined&&d){m=n[l];o=Ext.isObject(j);a=m===b[l];if(o){if(a&&h[l]){continue}j=Ext.merge({},j,m)}if(a||o){i[d.names.set](j)}}}},updateSpriteTheme:function(c){this.getSprites();var j=this,e=j.getSurface("chart"),h=e.getItems(),m=c.getSprites(),k,a,l,f,d,b,g;for(b=0,g=h.length;b<g;b++){k=h[b];a=m[k.type];if(a){f={};d=k.type==="text";for(l in a){if(!(l in k.config)){if(!(d&&l.indexOf("font")===0&&k.config.font)){f[l]=a[l]}}}k.setAttributes(f)}}},addSeries:function(b){var a=this.getSeries();Ext.Array.push(a,b);this.setSeries(a)},removeSeries:function(d){d=Ext.Array.from(d);var b=this.getSeries(),f=[],a=d.length,g={},c,e;for(c=0;c<a;c++){e=d[c];if(typeof e!=="string"){e=e.getId()}g[e]=true}for(c=0,a=b.length;c<a;c++){if(!g[b[c].getId()]){f.push(b[c])}}this.setSeries(f)},applySeries:function(e,d){var g=this,j=[],h,a,c,f,b;g.animationSuspendCount++;g.getAxes();if(d){h=d.map}else{d=[];h=d.map={}}j.map={};e=Ext.Array.from(e,true);for(c=0,f=e.length;c<f;c++){b=e[c];if(!b){continue}a=h[b.getId&&b.getId()||b.id];if(b instanceof Ext.chart.series.Series){if(a&&a!==b){a.destroy()}b.setChart(g)}else{if(Ext.isObject(b)){if(a){a.setConfig(b);b=a}else{if(Ext.isString(b)){b={type:b}}b.chart=g;b=Ext.create(b.xclass||("series."+b.type),b);b.on("animationstart","onAnimationStart",g);b.on("animationend","onAnimationEnd",g)}}}j.push(b);j.map[b.getId()]=b}for(c in h){if(!j.map[h[c].getId()]){h[c].destroy()}}g.animationSuspendCount--;return j},applyLegend:function(b,a){return Ext.factory(b,Ext.chart.Legend,a)},updateLegend:function(b,a){if(a){a.destroy()}if(b){this.getItems();this.legendStore=new Ext.data.Store({autoDestroy:true,fields:["id","name","mark","disabled","series","index"]});b.setStore(this.legendStore);this.refreshLegendStore();this.legendStore.on("update","onUpdateLegendStore",this)}},updateSeries:function(b,a){var c=this;if(c.isDestroying){return}c.animationSuspendCount++;c.fireEvent("serieschange",c,b,a);c.refreshLegendStore();if(!Ext.isEmpty(b)){c.updateTheme(c.getTheme())}c.scheduleLayout();c.animationSuspendCount--},applyInteractions:function(h,d){if(!d){d=[];d.map={}}var g=this,a=[],c=d.map,e,f,b;a.map={};h=Ext.Array.from(h,true);for(e=0,f=h.length;e<f;e++){b=h[e];if(!b){continue}b=Ext.factory(b,null,c[b.getId&&b.getId()||b.id],"interaction");if(b){b.setChart(g);a.push(b);a.map[b.getId()]=b}}for(e in c){if(!a.map[e]){c[e].destroy()}}return a},getInteraction:function(e){var f=this.getInteractions(),a=f&&f.length,c=null,b,d;if(a){for(d=0;d<a;++d){b=f[d];if(b.type===e){c=b;break}}}return c},applyStore:function(a){return a&&Ext.StoreManager.lookup(a)},updateStore:function(a,c){var b=this;if(c){c.un({datachanged:"onDataChanged",update:"onDataChanged",scope:b,order:"after"});if(c.autoDestroy){c.destroy()}}if(a){a.on({datachanged:"onDataChanged",update:"onDataChanged",scope:b,order:"after"})}b.fireEvent("storechange",b,a,c);b.onDataChanged()},redraw:function(){this.fireEvent("redraw",this)},performLayout:function(){var d=this,b=d.getChartSize(true),c=[0,0,b.width,b.height],a=d.getBackground();d.hasFirstLayout=true;d.fireEvent("layout",d);d.cancelChartLayout();d.getSurface("background").setRect(c);d.getSurface("chart").setRect(c);a.setAttributes({width:b.width,height:b.height})},getChartSize:function(b){var a=this;if(b){a.chartSize=null}return a.chartSize||(a.chartSize=a.innerElement.getSize())},getEventXY:function(a){return this.getSurface().getEventXY(a)},getItemForPoint:function(h,g){var f=this,a=f.getSeries(),e=f.getMainRect(),d=a.length,b=f.hasFirstLayout?d-1:-1,c,j;if(!(e&&h>=0&&h<=e[2]&&g>=0&&g<=e[3])){return null}for(;b>=0;b--){c=a[b];j=c.getItemForPoint(h,g);if(j){return j}}return null},getItemsForPoint:function(h,g){var f=this,a=f.getSeries(),d=a.length,b=f.hasFirstLayout?d-1:-1,e=[],c,j;for(;b>=0;b--){c=a[b];j=c.getItemForPoint(h,g);if(j){e.push(j)}}return e},onAnimationStart:function(){this.fireEvent("animationstart",this)},onAnimationEnd:function(){this.fireEvent("animationend",this)},onDataChanged:function(){var d=this;if(d.isInitializing){return}var c=d.getMainRect(),a=d.getStore(),b=d.getSeries(),e=d.getAxes();if(!a||!e||!b){return}if(!c){d.on({redraw:d.onDataChanged,scope:d,single:true});return}d.processData();d.redraw()},recordCount:0,processData:function(){var g=this,e=g.getStore().getCount(),c=g.getSeries(),f=c.length,d=false,b=0,a;for(;b<f;b++){a=c[b];a.processData();if(!d&&a.isStoreDependantColorCount){d=true}}if(d&&e>g.recordCount){g.updateColors(g.getColors());g.recordCount=e}},bindStore:function(a){this.setStore(a)},applyHighlightItem:function(f,a){if(f===a){return}if(Ext.isObject(f)&&Ext.isObject(a)){var e=f,d=a,c=e.sprite&&(e.sprite[0]||e.sprite),b=d.sprite&&(d.sprite[0]||d.sprite);if(c===b&&e.index===d.index){return}}return f},updateHighlightItem:function(b,a){if(a){a.series.setAttributesForItem(a,{highlighted:false})}if(b){b.series.setAttributesForItem(b,{highlighted:true});this.fireEvent("itemhighlight",this,b,a)}this.fireEvent("itemhighlightchange",this,b,a)},destroyChart:function(){var f=this,d=f.getLegend(),g=f.getAxes(),c=f.getSeries(),h=f.getInteractions(),b=[],a,e;f.surfaceMap=null;for(a=0,e=h.length;a<e;a++){h[a].destroy()}for(a=0,e=g.length;a<e;a++){g[a].destroy()}for(a=0,e=c.length;a<e;a++){c[a].destroy()}f.setInteractions(b);f.setAxes(b);f.setSeries(b);if(d){d.destroy();f.setLegend(null)}f.legendStore=null;f.setStore(null);f.cancelChartLayout()},getRefItems:function(b){var g=this,e=g.getSeries(),h=g.getAxes(),a=g.getInteractions(),c=[],d,f;for(d=0,f=e.length;d<f;d++){c.push(e[d]);if(e[d].getRefItems){c.push.apply(c,e[d].getRefItems(b))}}for(d=0,f=h.length;d<f;d++){c.push(h[d]);if(h[d].getRefItems){c.push.apply(c,h[d].getRefItems(b))}}for(d=0,f=a.length;d<f;d++){c.push(a[d]);if(a[d].getRefItems){c.push.apply(c,a[d].getRefItems(b))}}return c}});Ext.define("Ext.chart.overrides.AbstractChart",{override:"Ext.chart.AbstractChart",updateLegend:function(b,a){var c;this.callParent([b,a]);if(b){c=b.docked;this.addDocked({dock:c,xtype:"panel",shrinkWrap:true,scrollable:true,layout:{type:c==="top"||c==="bottom"?"hbox":"vbox",pack:"center"},items:b,cls:Ext.baseCSSPrefix+"legend-panel"})}},performLayout:function(){if(this.isVisible(true)){return this.callParent()}this.cancelChartLayout();return false},afterComponentLayout:function(c,a,b,d){this.callParent([c,a,b,d]);this.scheduleLayout()},allowSchedule:function(){return this.rendered},onDestroy:function(){this.destroyChart();this.callParent(arguments)}});Ext.define("Ext.chart.grid.HorizontalGrid",{extend:Ext.draw.sprite.Sprite,alias:"grid.horizontal",inheritableStatics:{def:{processors:{x:"number",y:"number",width:"number",height:"number"},defaults:{x:0,y:0,width:1,height:1,strokeStyle:"#DDD"}}},render:function(b,c,e){var a=this.attr,f=b.roundPixel(a.y),d=c.lineWidth*0.5;c.beginPath();c.rect(e[0]-b.matrix.getDX(),f+d,+e[2],a.height);c.fill();c.beginPath();c.moveTo(e[0]-b.matrix.getDX(),f+d);c.lineTo(e[0]+e[2]-b.matrix.getDX(),f+d);c.stroke()}});Ext.define("Ext.chart.grid.VerticalGrid",{extend:Ext.draw.sprite.Sprite,alias:"grid.vertical",inheritableStatics:{def:{processors:{x:"number",y:"number",width:"number",height:"number"},defaults:{x:0,y:0,width:1,height:1,strokeStyle:"#DDD"}}},render:function(c,d,f){var b=this.attr,a=c.roundPixel(b.x),e=d.lineWidth*0.5;d.beginPath();d.rect(a-e,f[1]-c.matrix.getDY(),b.width,f[3]);d.fill();d.beginPath();d.moveTo(a-e,f[1]-c.matrix.getDY());d.lineTo(a-e,f[1]+f[3]-c.matrix.getDY());d.stroke()}});Ext.define("Ext.chart.CartesianChart",{extend:Ext.chart.AbstractChart,alternateClassName:"Ext.chart.Chart",xtype:["cartesian","chart"],isCartesian:true,config:{flipXY:false,innerRect:[0,0,1,1],innerPadding:{top:0,left:0,right:0,bottom:0}},applyInnerPadding:function(b,a){if(!Ext.isObject(b)){return Ext.util.Format.parseBox(b)}else{if(!a){return b}else{return Ext.apply(a,b)}}},getDirectionForAxis:function(a){var b=this.getFlipXY();if(a==="left"||a==="right"){if(b){return"X"}else{return"Y"}}else{if(b){return"Y"}else{return"X"}}},performLayout:function(){var A=this;A.animationSuspendCount++;if(A.callParent()===false){--A.animationSuspendCount;return}A.suspendThicknessChanged();var d=A.getSurface("chart").getRect(),o=d[2],n=d[3],z=A.getAxes(),b,q=A.getSeries(),h,l,a,f=A.getInsetPadding(),v=A.getInnerPadding(),r,c,e=Ext.apply({},f),u,p,s,k,m,y,t,x,g,j=A.getInherited().rtl,w=A.getFlipXY();if(o<=0||n<=0){return}for(x=0;x<z.length;x++){b=z[x];l=b.getSurface();m=b.getFloating();y=m?m.value:null;a=b.getThickness();switch(b.getPosition()){case"top":l.setRect([0,e.top+1,o,a]);break;case"bottom":l.setRect([0,n-(e.bottom+a),o,a]);break;case"left":l.setRect([e.left,0,a,n]);break;case"right":l.setRect([o-(e.right+a),0,a,n]);break}if(y===null){e[b.getPosition()]+=a}}o-=e.left+e.right;n-=e.top+e.bottom;u=[e.left,e.top,o,n];e.left+=v.left;e.top+=v.top;e.right+=v.right;e.bottom+=v.bottom;p=o-v.left-v.right;s=n-v.top-v.bottom;A.setInnerRect([e.left,e.top,p,s]);if(p<=0||s<=0){return}A.setMainRect(u);A.getSurface().setRect(u);for(x=0,g=A.surfaceMap.grid&&A.surfaceMap.grid.length;x<g;x++){c=A.surfaceMap.grid[x];c.setRect(u);c.matrix.set(1,0,0,1,v.left,v.top);c.matrix.inverse(c.inverseMatrix)}for(x=0;x<z.length;x++){b=z[x];l=b.getSurface();t=l.matrix;k=t.elements;switch(b.getPosition()){case"top":case"bottom":k[4]=e.left;b.setLength(p);break;case"left":case"right":k[5]=e.top;b.setLength(s);break}b.updateTitleSprite();t.inverse(l.inverseMatrix)}for(x=0,g=q.length;x<g;x++){h=q[x];r=h.getSurface();r.setRect(u);if(w){if(j){r.matrix.set(0,-1,-1,0,v.left+p,v.top+s)}else{r.matrix.set(0,-1,1,0,v.left,v.top+s)}}else{r.matrix.set(1,0,0,-1,v.left,v.top+s)}r.matrix.inverse(r.inverseMatrix);h.getOverlaySurface().setRect(u)}A.redraw();A.animationSuspendCount--;A.resumeThicknessChanged()},refloatAxes:function(){var h=this,g=h.getAxes(),o=(g&&g.length)||0,c,d,n,f,l,b,k,r=h.getChartSize(),q=h.getInsetPadding(),p=h.getInnerPadding(),a=r.width-q.left-q.right,m=r.height-q.top-q.bottom,j,e;for(e=0;e<o;e++){c=g[e];f=c.getFloating();l=f?f.value:null;if(l===null){delete c.floatingAtCoord;continue}d=c.getSurface();n=d.getRect();if(!n){continue}n=n.slice();b=h.getAxis(f.alongAxis);if(b){j=b.getAlignment()==="horizontal";if(Ext.isString(l)){l=b.getCoordFor(l)}b.floatingAxes[c.getId()]=l;k=b.getSprites()[0].attr.matrix;if(j){l=l*k.getXX()+k.getDX();c.floatingAtCoord=l+p.left+p.right}else{l=l*k.getYY()+k.getDY();c.floatingAtCoord=l+p.top+p.bottom}}else{j=c.getAlignment()==="horizontal";if(j){c.floatingAtCoord=l+p.top+p.bottom}else{c.floatingAtCoord=l+p.left+p.right}l=d.roundPixel(0.01*l*(j?m:a))}switch(c.getPosition()){case"top":n[1]=q.top+p.top+l-n[3]+1;break;case"bottom":n[1]=q.top+p.top+(b?l:m-l);break;case"left":n[0]=q.left+p.left+l-n[2];break;case"right":n[0]=q.left+p.left+(b?l:a-l)-1;break}d.setRect(n)}},redraw:function(){var C=this,r=C.getSeries(),z=C.getAxes(),b=C.getMainRect(),p,t,w=C.getInnerPadding(),f,l,s,e,q,A,v,g,d,c,a,k,n,y=C.getFlipXY(),x=1000,m,u,h,o,B;if(!b){return}p=b[2]-w.left-w.right;t=b[3]-w.top-w.bottom;for(A=0;A<r.length;A++){h=r[A];if((c=h.getXAxis())){n=c.getVisibleRange();l=c.getRange();l=[l[0]+(l[1]-l[0])*n[0],l[0]+(l[1]-l[0])*n[1]]}else{l=h.getXRange()}if((a=h.getYAxis())){n=a.getVisibleRange();s=a.getRange();s=[s[0]+(s[1]-s[0])*n[0],s[0]+(s[1]-s[0])*n[1]]}else{s=h.getYRange()}q={visibleMinX:l[0],visibleMaxX:l[1],visibleMinY:s[0],visibleMaxY:s[1],innerWidth:p,innerHeight:t,flipXY:y};f=h.getSprites();for(v=0,g=f.length;v<g;v++){o=f[v];m=o.attr.zIndex;if(m<x){m+=(A+1)*100+x;o.attr.zIndex=m;B=o.getMarker("items");if(B){u=B.attr.zIndex;if(u===Number.MAX_VALUE){B.attr.zIndex=m}else{if(u<x){B.attr.zIndex=m+u}}}}o.setAttributes(q,true)}}for(A=0;A<z.length;A++){d=z[A];e=d.isSide();f=d.getSprites();k=d.getRange();n=d.getVisibleRange();q={dataMin:k[0],dataMax:k[1],visibleMin:n[0],visibleMax:n[1]};if(e){q.length=t;q.startGap=w.bottom;q.endGap=w.top}else{q.length=p;q.startGap=w.left;q.endGap=w.right}for(v=0,g=f.length;v<g;v++){f[v].setAttributes(q,true)}}C.renderFrame();C.callParent(arguments)},renderFrame:function(){this.refloatAxes();this.callParent()}});Ext.define("Ext.chart.axis.Category",{extend:Ext.chart.axis.Axis,alias:"axis.category",type:"category",config:{layout:"combineDuplicate",segmenter:"names"}});Ext.define("Ext.chart.axis.Numeric",{extend:Ext.chart.axis.Axis,type:"numeric",alias:["axis.numeric","axis.radial"],config:{layout:"continuous",segmenter:"numeric",aggregator:"double"}});Ext.define("Ext.chart.series.Cartesian",{extend:Ext.chart.series.Series,config:{xField:null,yField:null,xAxis:null,yAxis:null},directions:["X","Y"],fieldCategoryX:["X"],fieldCategoryY:["Y"],applyXAxis:function(a,b){return this.getChart().getAxis(a)||b},applyYAxis:function(a,b){return this.getChart().getAxis(a)||b},updateXAxis:function(a){a.processData(this)},updateYAxis:function(a){a.processData(this)},coordinateX:function(){return this.coordinate("X",0,2)},coordinateY:function(){return this.coordinate("Y",1,2)},getItemForPoint:function(a,g){if(this.getSprites()){var f=this,d=f.getSprites()[0],b=f.getStore(),e,c;if(f.getHidden()){return null}if(d){c=d.getIndexNearPoint(a,g);if(c!==-1){e={series:f,category:f.getItemInstancing()?"items":"markers",index:c,record:b.getData().items[c],field:f.getYField(),sprite:d};return e}}}},createSprite:function(){var c=this,a=c.callParent(),b=c.getChart(),d=c.getXAxis();a.setAttributes({flipXY:b.getFlipXY(),xAxis:d});if(a.setAggregator&&d&&d.getAggregator){if(d.getAggregator){a.setAggregator({strategy:d.getAggregator()})}else{a.setAggregator({})}}return a},getSprites:function(){var d=this,c=this.getChart(),e=d.getAnimation()||c&&c.getAnimation(),b=d.getItemInstancing(),f=d.sprites,a;if(!c){return[]}if(!f.length){a=d.createSprite()}else{a=f[0]}if(e){if(b){a.itemsMarker.getTemplate().setAnimation(e)}a.setAnimation(e)}return f},provideLegendInfo:function(d){var b=this,a=b.getSubStyleWithTheme(),c=a.fillStyle;if(Ext.isArray(c)){c=c[0]}d.push({name:b.getTitle()||b.getYField()||b.getId(),mark:(Ext.isObject(c)?c.stops&&c.stops[0].color:c)||a.strokeStyle||"black",disabled:b.getHidden(),series:b.getId(),index:0})},getXRange:function(){return[this.dataRange[0],this.dataRange[2]]},getYRange:function(){return[this.dataRange[1],this.dataRange[3]]}});Ext.define("Ext.chart.series.StackedCartesian",{extend:Ext.chart.series.Cartesian,config:{stacked:true,splitStacks:true,fullStack:false,fullStackTotal:100,hidden:[]},spriteAnimationCount:0,themeColorCount:function(){var b=this,a=b.getYField();return Ext.isArray(a)?a.length:1},updateStacked:function(){this.processData()},updateSplitStacks:function(){this.processData()},coordinateY:function(){return this.coordinateStacked("Y",1,2)},coordinateStacked:function(D,e,m){var F=this,f=F.getStore(),r=f.getData().items,B=r.length,c=F["get"+D+"Axis"](),x=F.getHidden(),a=F.getSplitStacks(),z=F.getFullStack(),l=F.getFullStackTotal(),p={min:0,max:0},n=F["fieldCategory"+D],C=[],o=[],E=[],h,A=F.getStacked(),g=F.getSprites(),q=[],w,v,u,s,H,y,b,d,G,t;if(!g.length){return}for(w=0;w<n.length;w++){d=n[w];s=F.getFields([d]);H=s.length;for(v=0;v<B;v++){C[v]=0;o[v]=0;E[v]=0}for(v=0;v<H;v++){if(!x[v]){q[v]=F.coordinateData(r,s[v],c)}}if(A&&z){y=[];if(a){b=[]}for(v=0;v<B;v++){y[v]=0;if(a){b[v]=0}for(u=0;u<H;u++){G=q[u];if(!G){continue}G=G[v];if(G>=0||!a){y[v]+=G}else{if(G<0){b[v]+=G}}}}}for(v=0;v<H;v++){t={};if(x[v]){t["dataStart"+d]=C;t["data"+d]=C;g[v].setAttributes(t);continue}G=q[v];if(A){h=[];for(u=0;u<B;u++){if(!G[u]){G[u]=0}if(G[u]>=0||!a){if(z&&y[u]){G[u]*=l/y[u]}C[u]=o[u];o[u]+=G[u];h[u]=o[u]}else{if(z&&b[u]){G[u]*=l/b[u]}C[u]=E[u];E[u]+=G[u];h[u]=E[u]}}t["dataStart"+d]=C;t["data"+d]=h;F.getRangeOfData(C,p);F.getRangeOfData(h,p)}else{t["dataStart"+d]=C;t["data"+d]=G;F.getRangeOfData(G,p)}g[v].setAttributes(t)}}F.dataRange[e]=p.min;F.dataRange[e+m]=p.max;t={};t["dataMin"+D]=p.min;t["dataMax"+D]=p.max;for(w=0;w<g.length;w++){g[w].setAttributes(t)}},getFields:function(f){var e=this,a=[],c,b,d;for(b=0,d=f.length;b<d;b++){c=e["get"+f[b]+"Field"]();if(Ext.isArray(c)){a.push.apply(a,c)}else{a.push(c)}}return a},updateLabelOverflowPadding:function(a){this.getLabel().setAttributes({labelOverflowPadding:a})},getSprites:function(){var k=this,j=k.getChart(),c=k.getAnimation()||j&&j.getAnimation(),f=k.getFields(k.fieldCategoryY),b=k.getItemInstancing(),h=k.sprites,l,e=k.getHidden(),g=false,d,a=f.length;if(!j){return[]}for(d=0;d<a;d++){l=h[d];if(!l){l=k.createSprite();l.setAttributes({zIndex:-d});l.setField(f[d]);g=true;e.push(false);if(b){l.itemsMarker.getTemplate().setAttributes(k.getStyleByIndex(d))}else{l.setAttributes(k.getStyleByIndex(d))}}if(c){if(b){l.itemsMarker.getTemplate().setAnimation(c)}l.setAnimation(c)}}if(g){k.updateHidden(e)}return h},getItemForPoint:function(k,j){if(this.getSprites()){var h=this,b,g,m,a=h.getItemInstancing(),f=h.getSprites(),l=h.getStore(),c=h.getHidden(),n,d,e;for(b=0,g=f.length;b<g;b++){if(!c[b]){m=f[b];d=m.getIndexNearPoint(k,j);if(d!==-1){e=h.getYField();n={series:h,index:d,category:a?"items":"markers",record:l.getData().items[d],field:typeof e==="string"?e:e[b],sprite:m};return n}}}return null}},provideLegendInfo:function(e){var g=this,f=g.getSprites(),h=g.getTitle(),j=g.getYField(),d=g.getHidden(),k=f.length===1,b,l,c,a;for(c=0;c<f.length;c++){b=g.getStyleByIndex(c);l=b.fillStyle;if(h){if(Ext.isArray(h)){a=h[c]}else{if(k){a=h}}}else{if(Ext.isArray(j)){a=j[c]}else{a=g.getId()}}e.push({name:a,mark:(Ext.isObject(l)?l.stops&&l.stops[0].color:l)||b.strokeStyle||"black",disabled:d[c],series:g.getId(),index:c})}},onSpriteAnimationStart:function(a){this.spriteAnimationCount++;if(this.spriteAnimationCount===1){this.fireEvent("animationstart")}},onSpriteAnimationEnd:function(a){this.spriteAnimationCount--;if(this.spriteAnimationCount===0){this.fireEvent("animationend")}}});Ext.define("Ext.chart.series.sprite.Series",{extend:Ext.draw.sprite.Sprite,mixins:{markerHolder:Ext.chart.MarkerHolder},inheritableStatics:{def:{processors:{dataMinX:"number",dataMaxX:"number",dataMinY:"number",dataMaxY:"number",rangeX:"data",rangeY:"data",dataX:"data",dataY:"data"},defaults:{dataMinX:0,dataMaxX:1,dataMinY:0,dataMaxY:1,rangeX:null,rangeY:null,dataX:null,dataY:null},triggers:{dataX:"bbox",dataY:"bbox",dataMinX:"bbox",dataMaxX:"bbox",dataMinY:"bbox",dataMaxY:"bbox"}}},config:{store:null,series:null,field:null}});Ext.define("Ext.chart.series.sprite.Cartesian",{extend:Ext.chart.series.sprite.Series,inheritableStatics:{def:{processors:{labels:"default",labelOverflowPadding:"number",selectionTolerance:"number",flipXY:"bool",renderer:"default",visibleMinX:"number",visibleMinY:"number",visibleMaxX:"number",visibleMaxY:"number",innerWidth:"number",innerHeight:"number"},defaults:{labels:null,labelOverflowPadding:10,selectionTolerance:20,flipXY:false,renderer:null,transformFillStroke:false,visibleMinX:0,visibleMinY:0,visibleMaxX:1,visibleMaxY:1,innerWidth:1,innerHeight:1},triggers:{dataX:"dataX,bbox",dataY:"dataY,bbox",visibleMinX:"panzoom",visibleMinY:"panzoom",visibleMaxX:"panzoom",visibleMaxY:"panzoom",innerWidth:"panzoom",innerHeight:"panzoom"},updaters:{dataX:function(a){this.processDataX();this.scheduleUpdater(a,"dataY",["dataY"])},dataY:function(){this.processDataY()},panzoom:function(c){var e=c.visibleMaxX-c.visibleMinX,d=c.visibleMaxY-c.visibleMinY,b=c.flipXY?c.innerHeight:c.innerWidth,g=!c.flipXY?c.innerHeight:c.innerWidth,a=this.getSurface(),f=a?a.getInherited().rtl:false;if(f&&!c.flipXY){c.translationX=b+c.visibleMinX*b/e}else{c.translationX=-c.visibleMinX*b/e}c.translationY=-c.visibleMinY*g/d;c.scalingX=(f&&!c.flipXY?-1:1)*b/e;c.scalingY=g/d;c.scalingCenterX=0;c.scalingCenterY=0;this.applyTransformations(true)}}}},processDataY:Ext.emptyFn,processDataX:Ext.emptyFn,updatePlainBBox:function(b){var a=this.attr;b.x=a.dataMinX;b.y=a.dataMinY;b.width=a.dataMaxX-a.dataMinX;b.height=a.dataMaxY-a.dataMinY},binarySearch:function(d){var b=this.attr.dataX,f=0,a=b.length;if(d<=b[0]){return f}if(d>=b[a-1]){return a-1}while(f+1<a){var c=(f+a)>>1,e=b[c];if(e===d){return c}else{if(e<d){f=c}else{a=c}}}return f},render:function(b,c,g){var f=this,a=f.attr,e=a.inverseMatrix.clone();e.appendMatrix(b.inverseMatrix);if(a.dataX===null||a.dataX===undefined){return}if(a.dataY===null||a.dataY===undefined){return}if(e.getXX()*e.getYX()||e.getXY()*e.getYY()){console.log("Cartesian Series sprite does not support rotation/sheering");return}var d=e.transformList([[g[0]-1,g[3]+1],[g[0]+g[2]+1,-1]]);d=d[0].concat(d[1]);f.renderClipped(b,c,d,g)},renderClipped:Ext.emptyFn,getIndexNearPoint:function(f,e){var w=this,q=w.attr.matrix,h=w.attr.dataX,g=w.attr.dataY,k=w.attr.selectionTolerance,t,r,c=-1,j=q.clone().prependMatrix(w.surfaceMatrix).inverse(),u=j.transformPoint([f,e]),b=j.transformPoint([f-k,e-k]),n=j.transformPoint([f+k,e+k]),a=Math.min(b[0],n[0]),s=Math.max(b[0],n[0]),l=Math.min(b[1],n[1]),d=Math.max(b[1],n[1]),m,v,o,p;for(o=0,p=h.length;o<p;o++){m=h[o];v=g[o];if(m>a&&m<s&&v>l&&v<d){if(c===-1||(Math.abs(m-u[0])<t)&&(Math.abs(v-u[1])<r)){t=Math.abs(m-u[0]);r=Math.abs(v-u[1]);c=o}}}return c}});Ext.define("Ext.chart.series.sprite.StackedCartesian",{extend:Ext.chart.series.sprite.Cartesian,inheritableStatics:{def:{processors:{groupCount:"number",groupOffset:"number",dataStartY:"data"},defaults:{selectionTolerance:20,groupCount:1,groupOffset:0,dataStartY:null},triggers:{dataStartY:"dataY,bbox"}}},getIndexNearPoint:function(e,d){var o=this,q=o.attr.matrix,h=o.attr.dataX,f=o.attr.dataY,u=o.attr.dataStartY,l=o.attr.selectionTolerance,s=0.5,r=Infinity,b=-1,k=q.clone().prependMatrix(this.surfaceMatrix).inverse(),t=k.transformPoint([e,d]),a=k.transformPoint([e-l,d-l]),n=k.transformPoint([e+l,d+l]),m=Math.min(a[1],n[1]),c=Math.max(a[1],n[1]),j,g;for(var p=0;p<h.length;p++){if(Math.min(u[p],f[p])<=c&&m<=Math.max(u[p],f[p])){j=Math.abs(h[p]-t[0]);g=Math.max(-Math.min(f[p]-t[1],t[1]-u[p]),0);if(j<s&&g<=r){s=j;r=g;b=p}}}return b}});Ext.define("Ext.chart.series.sprite.Bar",{alias:"sprite.barSeries",extend:Ext.chart.series.sprite.StackedCartesian,inheritableStatics:{def:{processors:{minBarWidth:"number",maxBarWidth:"number",minGapWidth:"number",radius:"number",inGroupGapWidth:"number"},defaults:{minBarWidth:2,maxBarWidth:100,minGapWidth:5,inGroupGapWidth:3,radius:0}}},drawLabel:function(k,i,s,h,o){var q=this,n=q.attr,f=q.getMarker("labels"),d=f.getTemplate(),l=q.labelCfg||(q.labelCfg={}),c=q.surfaceMatrix,j=n.labelOverflowPadding,b=d.attr.display,m=d.attr.orientation,g,e,a,r,t,p;l.x=c.x(i,h);l.y=c.y(i,h);if(!n.flipXY){l.rotationRads=-Math.PI*0.5}else{l.rotationRads=0}l.calloutVertical=!n.flipXY;switch(m){case"horizontal":l.rotationRads=0;l.calloutVertical=false;break;case"vertical":l.rotationRads=-Math.PI*0.5;l.calloutVertical=true;break}l.text=k;if(d.attr.renderer){p=[k,f,l,{store:q.getStore()},o];r=Ext.callback(d.attr.renderer,null,p,0,q.getSeries());if(typeof r==="string"){l.text=r}else{if(typeof r==="object"){if("text" in r){l.text=r.text}t=true}}}a=q.getMarkerBBox("labels",o,true);if(!a){q.putMarker("labels",l,o);a=q.getMarkerBBox("labels",o,true)}e=(a.width/2+j);if(s>h){e=-e}if((m==="horizontal"&&n.flipXY)||(m==="vertical"&&!n.flipXY)||!m){g=(b==="insideStart")?s+e:h-e}else{g=(b==="insideStart")?s+j*2:h-j*2}l.x=c.x(i,g);l.y=c.y(i,g);g=(b==="insideStart")?s-e:h+e;l.calloutPlaceX=c.x(i,g);l.calloutPlaceY=c.y(i,g);g=(b==="insideStart")?s:h;l.calloutStartX=c.x(i,g);l.calloutStartY=c.y(i,g);if(s>h){e=-e}if(Math.abs(h-s)<=e*2||b==="outside"){l.callout=1}else{l.callout=0}if(t){Ext.apply(l,r)}q.putMarker("labels",l,o)},drawBar:function(l,b,d,c,h,k,a,e){var g=this,j={},f=g.attr.renderer,i;j.x=c;j.y=h;j.width=k-c;j.height=a-h;j.radius=g.attr.radius;if(f){i=Ext.callback(f,null,[g,j,{store:g.getStore()},e],0,g.getSeries());Ext.apply(j,i)}g.putMarker("items",j,e,!f)},renderClipped:function(G,u,F,C){if(this.cleanRedraw){return}var q=this,o=q.attr,w=o.dataX,v=o.dataY,H=o.labels,n=o.dataStartY,m=o.groupCount,E=o.groupOffset-(m-1)*0.5,z=o.inGroupGapWidth,t=u.lineWidth,D=o.matrix,B=D.elements[0],j=D.elements[3],e=D.elements[4],d=G.roundPixel(D.elements[5])-1,J=(B<0?-1:1)*B-o.minGapWidth,k=(Math.min(J,o.maxBarWidth)-z*(m-1))/m,A=G.roundPixel(Math.max(o.minBarWidth,k)),c=q.surfaceMatrix,g,I,b,h,K,a,l=0.5*o.lineWidth,L=Math.min(F[0],F[2]),x=Math.max(F[0],F[2]),y=Math.max(0,Math.floor(L)),p=Math.min(w.length-1,Math.ceil(x)),f=H&&q.getMarker("labels"),s,r;for(K=y;K<=p;K++){s=n?n[K]:0;r=v[K];a=w[K]*B+e+E*(A+z);g=G.roundPixel(a-A/2)+l;h=G.roundPixel(r*j+d+t);I=G.roundPixel(a+A/2)-l;b=G.roundPixel(s*j+d+t);q.drawBar(u,G,F,g,h-l,I,b-l,K);if(f&&H[K]!=null){q.drawLabel(H[K],a,b,h,K)}q.putMarker("markers",{translationX:c.x(a,h),translationY:c.y(a,h)},K,true)}},getIndexNearPoint:function(l,k){var m=this,g=m.attr,h=g.dataX,a=m.getSurface(),b=a.getRect()||[0,0,0,0],j=b[3],e,d,c,n,f=-1;if(g.flipXY){e=j-k;if(a.getInherited().rtl){d=b[2]-l}else{d=l}}else{e=l;d=j-k}for(c=0;c<h.length;c++){n=m.getMarkerBBox("items",c);if(Ext.draw.Draw.isPointInBBox(e,d,n)){f=c;break}}return f}});Ext.define("Ext.chart.series.Bar",{extend:Ext.chart.series.StackedCartesian,alias:"series.bar",type:"bar",seriesType:"barSeries",config:{itemInstancing:{type:"rect",fx:{customDurations:{x:0,y:0,width:0,height:0,radius:0}}}},getItemForPoint:function(a,f){if(this.getSprites()){var d=this,c=d.getChart(),e=c.getInnerPadding(),b=c.getInherited().rtl;arguments[0]=a+(b?e.right:-e.left);arguments[1]=f+e.bottom;return d.callParent(arguments)}},updateXAxis:function(a){a.setLabelInSpan(true);this.callParent(arguments)},updateHidden:function(a){this.callParent(arguments);this.updateStacked()},updateStacked:function(c){var e=this,g=e.getSprites(),d=g.length,f=[],a={},b;for(b=0;b<d;b++){if(!g[b].attr.hidden){f.push(g[b])}}d=f.length;if(e.getStacked()){a.groupCount=1;a.groupOffset=0;for(b=0;b<d;b++){f[b].setAttributes(a)}}else{a.groupCount=f.length;for(b=0;b<d;b++){a.groupOffset=b;f[b].setAttributes(a)}}e.callParent(arguments)}});Ext.define("Siesta.Harness.Browser.UI.CoverageChart",{extend:Ext.chart.Chart,alias:"widget.coveragechart",border:false,margin:"35% 35%",animate:{easing:"bounceOut",duration:600},store:{fields:["name","value"]},axes:[{type:"numeric",position:"left",fields:["value"],minimum:0,maximum:100,label:{renderer:Ext.util.Format.numberRenderer("0"),fill:"#555"},grid:{odd:{stroke:"#efefef"},even:{stroke:"#efefef"}}},{type:"category",position:"bottom",fields:["name"],label:{fill:"#555"}}],series:[{type:"bar",axis:"left",xField:"name",yField:"value",label:{display:"outside","text-anchor":"middle",field:"value",orientation:"horizontal",fill:"#aaa",renderer:function(g,c,f,e,d,h,a,b){return g+"%"}},renderer:function(d,e,c,b,a){c.fill="rgb(89, 205, 82)";return c}}]});Ext.define("Siesta.Harness.Browser.Model.CoverageUnit",{extend:Ext.data.Model,idProperty:"id",fields:["id","url","text","reportNode",{name:"iconCls",defaultValue:"x-fa fa-cube"}],getLineCoverage:function(){return this.getCoverageInPercent("lines")},getStatementCoverage:function(){return this.getCoverageInPercent("statements")},getBranchCoverage:function(){return this.getCoverageInPercent("branches")},getFunctionCoverage:function(){return this.getCoverageInPercent("functions")},getCoverageInPercent:function(a){var b=this.data.reportNode;if(b){return b.metrics[a].pct}}},function(){Ext.data.NodeInterface.decorate(this);this.override({expand:function(){Ext.suspendLayouts();this.callParent(arguments);Ext.resumeLayouts()}})});Ext.define("Siesta.Harness.Browser.UI.CoverageReport",{extend:Ext.Container,alias:"widget.coveragereport",cls:"st-cov-report-panel",layout:"border",htmlReport:null,treePanel:null,treeStore:null,chart:null,sourcePanel:null,contentContainer:null,toggleButtons:null,standalone:false,dataUrl:null,harness:null,expandLevels:2,initComponent:function(){var c=this;var b=this.standalone;var a=this.treeStore=new Siesta.Harness.Browser.Model.FilterableTreeStore({model:"Siesta.Harness.Browser.Model.CoverageUnit",proxy:"memory",rootVisible:true});Ext.apply(this,{border:!this.standalone,items:[{xtype:"treepanel",border:false,region:"west",enableColumnHide:false,enableColumnMove:false,enableColumnResize:false,sortableColumns:false,rowLines:false,tbar:{cls:"siesta-toolbar",height:45,border:false,items:[b?null:{text:Siesta.Resource("Siesta.Harness.Browser.UI.CoverageReport","closeText"),scale:"medium",style:"margin:0 10px 0 0;",handler:this.onBackToMainUI,scope:this},{xtype:"treefilter",cls:"filterfield",hasAndCheck:function(){var d=Ext.pluck(c.toggleButtons,"pressed");return !d[0]||!d[1]||!d[2]},andChecker:this.levelFilter,andCheckerScope:this,margin:"3 0 0 0",store:a,filterField:"text",width:180},"->",{xtype:"label",text:Siesta.Resource("Siesta.Harness.Browser.UI.CoverageReport","showText"),margin:"0 5 0 0"},{text:Siesta.Resource("Siesta.Harness.Browser.UI.CoverageReport","highText"),level:"high",enableToggle:true,pressed:true,scale:"medium",cls:"st-cov-level-high",toggleHandler:this.toggleLevels,scope:this,margin:"0 5 0 0"},{text:Siesta.Resource("Siesta.Harness.Browser.UI.CoverageReport","mediumText"),level:"med",enableToggle:true,scale:"medium",pressed:true,cls:"st-cov-level-medium",toggleHandler:this.toggleLevels,scope:this,margin:"0 5 0 0"},{text:Siesta.Resource("Siesta.Harness.Browser.UI.CoverageReport","lowText"),level:"low",enableToggle:true,scale:"medium",pressed:true,cls:"st-cov-level-low",toggleHandler:this.toggleLevels,scope:this,style:"margin:0 5px 0 0"}]},viewType:"filterabletreeview",viewConfig:{store:a.nodeStore,trackOver:false,rootVisible:true},cls:"st-cov-report-tree-panel",flex:1,split:true,rootVisible:true,columns:[{xtype:"treecolumn",dataIndex:"text",menuDisabled:true,flex:3},{text:Siesta.Resource("Siesta.Harness.Browser.UI.CoverageReport","statementsText"),flex:1,menuDisabled:true,tdCls:"cov-result-cell",renderer:this.getMetricsRenderer("statements")},{text:Siesta.Resource("Siesta.Harness.Browser.UI.CoverageReport","branchesText"),flex:1,menuDisabled:true,tdCls:"cov-result-cell",renderer:this.getMetricsRenderer("branches")},{text:Siesta.Resource("Siesta.Harness.Browser.UI.CoverageReport","functionsText"),flex:1,menuDisabled:true,tdCls:"cov-result-cell",renderer:this.getMetricsRenderer("functions")},{text:Siesta.Resource("Siesta.Harness.Browser.UI.CoverageReport","linesText"),flex:1,menuDisabled:true,tdCls:"cov-result-cell",renderer:this.getMetricsRenderer("lines")}],store:a,listeners:{selectionchange:this.onClassSelected,scope:this}},{xtype:"container",cls:"coveragechart-container",slot:"contentContainer",region:"center",layout:{type:"card",deferredRendering:true},items:[{xtype:"coveragechart"},{xtype:"panel",slot:"sourcePanel",flex:1,autoScroll:true,bodyPadding:"4 0 4 10",border:false,cls:"st-cov-report-source-panel"}]}]});this.callParent();this.contentContainer=this.down("[slot=contentContainer]");this.chart=this.down("chart");this.sourcePanel=this.down("[slot=sourcePanel]");this.toggleButtons=this.query("button[level]");this.treePanel=this.down("treepanel");if(b){this.on("afterrender",function(){var d=new Ext.LoadMask({target:this,msg:Siesta.Resource("Siesta.Harness.Browser.UI.CoverageReport","loadingText")});d.show();Ext.Ajax.request({url:this.dataUrl,success:function(e){d.hide();c.loadHtmlReport(Ext.JSON.decode(e.responseText))},failure:function(){d.hide();Ext.Msg.show({title:Siesta.Resource("Siesta.Harness.Browser.UI.CoverageReport","loadingErrorText"),msg:Siesta.Resource("Siesta.Harness.Browser.UI.CoverageReport","loadingErrorMessageText")+c.dataUrl,buttons:Ext.MessageBox.OK,icon:Ext.MessageBox.ERROR})}})},this,{single:true})}else{if(this.htmlReport){this.loadHtmlReport(this.htmlReport)}}if(this.harness){this.harness.on("testsuiteend",this.onTestSuiteEnd,this)}},onTestSuiteEnd:function(a){if(this.harness&&this.isVisible()){this.loadHtmlReport(this.harness.generateCoverageHtmlReport(false))}},loadHtmlReport:function(a){this.htmlReport=a;var g=this.treeStore;var f=a.coverageUnit=="file";var d=this.down("treefilter");var b=this.treePanel.getSelectionModel();var e=b.getSelected();var c=e.length>0?e.first().getId():null;g.setRootNode(this.generateFilesTree(a.htmlReport.root));if(c&&g.getNodeById(c)){b.select(g.getNodeById(c))}this.down("treecolumn").setText(f?"File name":"Class name");d.emptyText=f?"Filter files":"Filter classes";d.applyEmptyText();this.loadChartData(g.getRootNode())},levelFilter:function(a){var b=a.getStatementCoverage();if(b===undefined){return false}return this.toggleButtons[b>=80?0:b>=50?1:2].pressed},toggleLevels:function(){this.down("treefilter").refreshFilter()},getCoverageLevelClass:function(a){if(typeof a=="number"){return"st-cov-level-"+(a>=80?"high":a>=50?"medium":"low")}else{return""}},getMetricsRenderer:function(b){var a=this;return function(f,c,g){var d=g.get("reportNode");if(!d){return""}var e=d.metrics[b];c.tdCls=a.getCoverageLevelClass(e.pct);return'<span class="st-cov-stat-pct-'+b+'">'+Math.round(e.pct)+'%</span> <span class="st-cov-stat-abs-'+b+'">('+e.covered+"/"+e.total+")</span>"}},generateFilesTree:function(b,d){var a=this;d=d||0;if(b.kind==="dir"){var c=b.relativeName||b.fullName;return{id:b.fullName,url:b.fullName,leaf:false,expanded:d<this.expandLevels,text:c=="/"&&this.htmlReport.coverageUnit=="extjs_class"?Siesta.Resource("Siesta.Harness.Browser.UI.CoverageReport","globalNamespaceText"):c,reportNode:b,children:Ext.Array.map(b.children,function(e){return a.generateFilesTree(e,d+1)}).sort(this.treeSorterFn)}}else{return{id:b.fullName,url:b.fullName,leaf:true,text:b.relativeName,reportNode:b}}},treeSorterFn:function(b,a){if(!b.leaf&&a.leaf){return -1}if(b.leaf&&!a.leaf){return 1}if(b.leaf===a.leaf){return b.text<a.text?-1:1}},onClassSelected:function(b,c){var a=c[0];if(!a){return}if(a.isLeaf()&&!this.htmlReport.coverageNoSource){this.sourcePanel.update(a.get("reportNode").html);prettyPrint();this.contentContainer.getLayout().setActiveItem(1)}else{this.loadChartData(a);this.contentContainer.getLayout().setActiveItem(0)}},onBackToMainUI:function(){this.fireEvent("backtomainui",this)},loadChartData:function(a){this.chart.store.loadData([{name:"Statements",value:Math.round(a.getStatementCoverage())},{name:"Branches",value:Math.round(a.getBranchCoverage())},{name:"Functions",value:Math.round(a.getFunctionCoverage())},{name:"Lines",value:Math.round(a.getLineCoverage())}])}});;
};
window.PR_SHOULD_USE_CONTINUATION=true;(function(){var h=["break,continue,do,else,for,if,return,while"];var u=[h,"auto,case,char,const,default,double,enum,extern,float,goto,int,long,register,short,signed,sizeof,static,struct,switch,typedef,union,unsigned,void,volatile"];var p=[u,"catch,class,delete,false,import,new,operator,private,protected,public,this,throw,true,try,typeof"];var l=[p,"alignof,align_union,asm,axiom,bool,concept,concept_map,const_cast,constexpr,decltype,dynamic_cast,explicit,export,friend,inline,late_check,mutable,namespace,nullptr,reinterpret_cast,static_assert,static_cast,template,typeid,typename,using,virtual,where"];var x=[p,"abstract,boolean,byte,extends,final,finally,implements,import,instanceof,null,native,package,strictfp,super,synchronized,throws,transient"];var R=[x,"as,base,by,checked,decimal,delegate,descending,dynamic,event,fixed,foreach,from,group,implicit,in,interface,internal,into,is,lock,object,out,override,orderby,params,partial,readonly,ref,sbyte,sealed,stackalloc,string,select,uint,ulong,unchecked,unsafe,ushort,var"];var r="all,and,by,catch,class,else,extends,false,finally,for,if,in,is,isnt,loop,new,no,not,null,of,off,on,or,return,super,then,true,try,unless,until,when,while,yes";var w=[p,"debugger,eval,export,function,get,null,set,undefined,var,with,Infinity,NaN"];var s="caller,delete,die,do,dump,elsif,eval,exit,foreach,for,goto,if,import,last,local,my,next,no,our,print,package,redo,require,sub,undef,unless,until,use,wantarray,while,BEGIN,END";var I=[h,"and,as,assert,class,def,del,elif,except,exec,finally,from,global,import,in,is,lambda,nonlocal,not,or,pass,print,raise,try,with,yield,False,True,None"];var f=[h,"alias,and,begin,case,class,def,defined,elsif,end,ensure,false,in,module,next,nil,not,or,redo,rescue,retry,self,super,then,true,undef,unless,until,when,yield,BEGIN,END"];var H=[h,"case,done,elif,esac,eval,fi,function,in,local,set,then,until"];var A=[l,R,w,s+I,f,H];var e=/^(DIR|FILE|vector|(de|priority_)?queue|list|stack|(const_)?iterator|(multi)?(set|map)|bitset|u?(int|float)\d*)/;var C="str";var z="kwd";var j="com";var O="typ";var G="lit";var L="pun";var F="pln";var m="tag";var E="dec";var J="src";var P="atn";var n="atv";var N="nocode";var M="(?:^^\\.?|[+-]|\\!|\\!=|\\!==|\\#|\\%|\\%=|&|&&|&&=|&=|\\(|\\*|\\*=|\\+=|\\,|\\-=|\\->|\\/|\\/=|:|::|\\;|<|<<|<<=|<=|=|==|===|>|>=|>>|>>=|>>>|>>>=|\\?|\\@|\\[|\\^|\\^=|\\^\\^|\\^\\^=|\\{|\\||\\|=|\\|\\||\\|\\|=|\\~|break|case|continue|delete|do|else|finally|instanceof|return|throw|try|typeof)\\s*";function k(Z){var ad=0;var S=false;var ac=false;for(var V=0,U=Z.length;V<U;++V){var ae=Z[V];if(ae.ignoreCase){ac=true}else{if(/[a-z]/i.test(ae.source.replace(/\\u[0-9a-f]{4}|\\x[0-9a-f]{2}|\\[^ux]/gi,""))){S=true;ac=false;break}}}var Y={b:8,t:9,n:10,v:11,f:12,r:13};function ab(ah){var ag=ah.charCodeAt(0);if(ag!==92){return ag}var af=ah.charAt(1);ag=Y[af];if(ag){return ag}else{if("0"<=af&&af<="7"){return parseInt(ah.substring(1),8)}else{if(af==="u"||af==="x"){return parseInt(ah.substring(2),16)}else{return ah.charCodeAt(1)}}}}function T(af){if(af<32){return(af<16?"\\x0":"\\x")+af.toString(16)}var ag=String.fromCharCode(af);if(ag==="\\"||ag==="-"||ag==="["||ag==="]"){ag="\\"+ag}return ag}function X(am){var aq=am.substring(1,am.length-1).match(new RegExp("\\\\u[0-9A-Fa-f]{4}|\\\\x[0-9A-Fa-f]{2}|\\\\[0-3][0-7]{0,2}|\\\\[0-7]{1,2}|\\\\[\\s\\S]|-|[^-\\\\]","g"));var ak=[];var af=[];var ao=aq[0]==="^";for(var ar=ao?1:0,aj=aq.length;ar<aj;++ar){var ah=aq[ar];if(/\\[bdsw]/i.test(ah)){ak.push(ah)}else{var ag=ab(ah);var al;if(ar+2<aj&&"-"===aq[ar+1]){al=ab(aq[ar+2]);ar+=2}else{al=ag}af.push([ag,al]);if(!(al<65||ag>122)){if(!(al<65||ag>90)){af.push([Math.max(65,ag)|32,Math.min(al,90)|32])}if(!(al<97||ag>122)){af.push([Math.max(97,ag)&~32,Math.min(al,122)&~32])}}}}af.sort(function(av,au){return(av[0]-au[0])||(au[1]-av[1])});var ai=[];var ap=[NaN,NaN];for(var ar=0;ar<af.length;++ar){var at=af[ar];if(at[0]<=ap[1]+1){ap[1]=Math.max(ap[1],at[1])}else{ai.push(ap=at)}}var an=["["];if(ao){an.push("^")}an.push.apply(an,ak);for(var ar=0;ar<ai.length;++ar){var at=ai[ar];an.push(T(at[0]));if(at[1]>at[0]){if(at[1]+1>at[0]){an.push("-")}an.push(T(at[1]))}}an.push("]");return an.join("")}function W(al){var aj=al.source.match(new RegExp("(?:\\[(?:[^\\x5C\\x5D]|\\\\[\\s\\S])*\\]|\\\\u[A-Fa-f0-9]{4}|\\\\x[A-Fa-f0-9]{2}|\\\\[0-9]+|\\\\[^ux0-9]|\\(\\?[:!=]|[\\(\\)\\^]|[^\\x5B\\x5C\\(\\)\\^]+)","g"));var ah=aj.length;var an=[];for(var ak=0,am=0;ak<ah;++ak){var ag=aj[ak];if(ag==="("){++am}else{if("\\"===ag.charAt(0)){var af=+ag.substring(1);if(af&&af<=am){an[af]=-1}}}}for(var ak=1;ak<an.length;++ak){if(-1===an[ak]){an[ak]=++ad}}for(var ak=0,am=0;ak<ah;++ak){var ag=aj[ak];if(ag==="("){++am;if(an[am]===undefined){aj[ak]="(?:"}}else{if("\\"===ag.charAt(0)){var af=+ag.substring(1);if(af&&af<=am){aj[ak]="\\"+an[am]}}}}for(var ak=0,am=0;ak<ah;++ak){if("^"===aj[ak]&&"^"!==aj[ak+1]){aj[ak]=""}}if(al.ignoreCase&&S){for(var ak=0;ak<ah;++ak){var ag=aj[ak];var ai=ag.charAt(0);if(ag.length>=2&&ai==="["){aj[ak]=X(ag)}else{if(ai!=="\\"){aj[ak]=ag.replace(/[a-zA-Z]/g,function(ao){var ap=ao.charCodeAt(0);return"["+String.fromCharCode(ap&~32,ap|32)+"]"})}}}}return aj.join("")}var aa=[];for(var V=0,U=Z.length;V<U;++V){var ae=Z[V];if(ae.global||ae.multiline){throw new Error(""+ae)}aa.push("(?:"+W(ae)+")")}return new RegExp(aa.join("|"),ac?"gi":"g")}function a(V){var U=/(?:^|\s)nocode(?:\s|$)/;var X=[];var T=0;var Z=[];var W=0;var S;if(V.currentStyle){S=V.currentStyle.whiteSpace}else{if(window.getComputedStyle){S=document.defaultView.getComputedStyle(V,null).getPropertyValue("white-space")}}var Y=S&&"pre"===S.substring(0,3);function aa(ab){switch(ab.nodeType){case 1:if(U.test(ab.className)){return}for(var ae=ab.firstChild;ae;ae=ae.nextSibling){aa(ae)}var ad=ab.nodeName;if("BR"===ad||"LI"===ad){X[W]="\n";Z[W<<1]=T++;Z[(W++<<1)|1]=ab}break;case 3:case 4:var ac=ab.nodeValue;if(ac.length){if(!Y){ac=ac.replace(/[ \t\r\n]+/g," ")}else{ac=ac.replace(/\r\n?/g,"\n")}X[W]=ac;Z[W<<1]=T;T+=ac.length;Z[(W++<<1)|1]=ab}break}}aa(V);return{sourceCode:X.join("").replace(/\n$/,""),spans:Z}}function B(S,U,W,T){if(!U){return}var V={sourceCode:U,basePos:S};W(V);T.push.apply(T,V.decorations)}var v=/\S/;function o(S){var V=undefined;for(var U=S.firstChild;U;U=U.nextSibling){var T=U.nodeType;V=(T===1)?(V?S:U):(T===3)?(v.test(U.nodeValue)?S:V):V}return V===S?undefined:V}function g(U,T){var S={};var V;(function(){var ad=U.concat(T);var ah=[];var ag={};for(var ab=0,Z=ad.length;ab<Z;++ab){var Y=ad[ab];var ac=Y[3];if(ac){for(var ae=ac.length;--ae>=0;){S[ac.charAt(ae)]=Y}}var af=Y[1];var aa=""+af;if(!ag.hasOwnProperty(aa)){ah.push(af);ag[aa]=null}}ah.push(/[\0-\uffff]/);V=k(ah)})();var X=T.length;var W=function(ah){var Z=ah.sourceCode,Y=ah.basePos;var ad=[Y,F];var af=0;var an=Z.match(V)||[];var aj={};for(var ae=0,aq=an.length;ae<aq;++ae){var ag=an[ae];var ap=aj[ag];var ai=void 0;var am;if(typeof ap==="string"){am=false}else{var aa=S[ag.charAt(0)];if(aa){ai=ag.match(aa[1]);ap=aa[0]}else{for(var ao=0;ao<X;++ao){aa=T[ao];ai=ag.match(aa[1]);if(ai){ap=aa[0];break}}if(!ai){ap=F}}am=ap.length>=5&&"lang-"===ap.substring(0,5);if(am&&!(ai&&typeof ai[1]==="string")){am=false;ap=J}if(!am){aj[ag]=ap}}var ab=af;af+=ag.length;if(!am){ad.push(Y+ab,ap)}else{var al=ai[1];var ak=ag.indexOf(al);var ac=ak+al.length;if(ai[2]){ac=ag.length-ai[2].length;ak=ac-al.length}var ar=ap.substring(5);B(Y+ab,ag.substring(0,ak),W,ad);B(Y+ab+ak,al,q(ar,al),ad);B(Y+ab+ac,ag.substring(ac),W,ad)}}ah.decorations=ad};return W}function i(T){var W=[],S=[];if(T.tripleQuotedStrings){W.push([C,/^(?:\'\'\'(?:[^\'\\]|\\[\s\S]|\'{1,2}(?=[^\']))*(?:\'\'\'|$)|\"\"\"(?:[^\"\\]|\\[\s\S]|\"{1,2}(?=[^\"]))*(?:\"\"\"|$)|\'(?:[^\\\']|\\[\s\S])*(?:\'|$)|\"(?:[^\\\"]|\\[\s\S])*(?:\"|$))/,null,"'\""])}else{if(T.multiLineStrings){W.push([C,/^(?:\'(?:[^\\\']|\\[\s\S])*(?:\'|$)|\"(?:[^\\\"]|\\[\s\S])*(?:\"|$)|\`(?:[^\\\`]|\\[\s\S])*(?:\`|$))/,null,"'\"`"])}else{W.push([C,/^(?:\'(?:[^\\\'\r\n]|\\.)*(?:\'|$)|\"(?:[^\\\"\r\n]|\\.)*(?:\"|$))/,null,"\"'"])}}if(T.verbatimStrings){S.push([C,/^@\"(?:[^\"]|\"\")*(?:\"|$)/,null])}var Y=T.hashComments;if(Y){if(T.cStyleComments){if(Y>1){W.push([j,/^#(?:##(?:[^#]|#(?!##))*(?:###|$)|.*)/,null,"#"])}else{W.push([j,/^#(?:(?:define|elif|else|endif|error|ifdef|include|ifndef|line|pragma|undef|warning)\b|[^\r\n]*)/,null,"#"])}S.push([C,/^<(?:(?:(?:\.\.\/)*|\/?)(?:[\w-]+(?:\/[\w-]+)+)?[\w-]+\.h|[a-z]\w*)>/,null])}else{W.push([j,/^#[^\r\n]*/,null,"#"])}}if(T.cStyleComments){S.push([j,/^\/\/[^\r\n]*/,null]);S.push([j,/^\/\*[\s\S]*?(?:\*\/|$)/,null])}if(T.regexLiterals){var X=("/(?=[^/*])(?:[^/\\x5B\\x5C]|\\x5C[\\s\\S]|\\x5B(?:[^\\x5C\\x5D]|\\x5C[\\s\\S])*(?:\\x5D|$))+/");S.push(["lang-regex",new RegExp("^"+M+"("+X+")")])}var V=T.types;if(V){S.push([O,V])}var U=(""+T.keywords).replace(/^ | $/g,"");if(U.length){S.push([z,new RegExp("^(?:"+U.replace(/[\s,]+/g,"|")+")\\b"),null])}W.push([F,/^\s+/,null," \r\n\t\xA0"]);S.push([G,/^@[a-z_$][a-z_$@0-9]*/i,null],[O,/^(?:[@_]?[A-Z]+[a-z][A-Za-z_$@0-9]*|\w+_t\b)/,null],[F,/^[a-z_$][a-z_$@0-9]*/i,null],[G,new RegExp("^(?:0x[a-f0-9]+|(?:\\d(?:_\\d+)*\\d*(?:\\.\\d*)?|\\.\\d\\+)(?:e[+\\-]?\\d+)?)[a-z]*","i"),null,"0123456789"],[F,/^\\[\s\S]?/,null],[L,/^.[^\s\w\.$@\'\"\`\/\#\\]*/,null]);return g(W,S)}var K=i({keywords:A,hashComments:true,cStyleComments:true,multiLineStrings:true,regexLiterals:true});function Q(V,ag){var U=/(?:^|\s)nocode(?:\s|$)/;var ab=/\r\n?|\n/;var ac=V.ownerDocument;var S;if(V.currentStyle){S=V.currentStyle.whiteSpace}else{if(window.getComputedStyle){S=ac.defaultView.getComputedStyle(V,null).getPropertyValue("white-space")}}var Z=S&&"pre"===S.substring(0,3);var af=ac.createElement("LI");while(V.firstChild){af.appendChild(V.firstChild)}var W=[af];function ae(al){switch(al.nodeType){case 1:if(U.test(al.className)){break}if("BR"===al.nodeName){ad(al);if(al.parentNode){al.parentNode.removeChild(al)}}else{for(var an=al.firstChild;an;an=an.nextSibling){ae(an)}}break;case 3:case 4:if(Z){var am=al.nodeValue;var aj=am.match(ab);if(aj){var ai=am.substring(0,aj.index);al.nodeValue=ai;var ah=am.substring(aj.index+aj[0].length);if(ah){var ak=al.parentNode;ak.insertBefore(ac.createTextNode(ah),al.nextSibling)}ad(al);if(!ai){al.parentNode.removeChild(al)}}}break}}function ad(ak){while(!ak.nextSibling){ak=ak.parentNode;if(!ak){return}}function ai(al,ar){var aq=ar?al.cloneNode(false):al;var ao=al.parentNode;if(ao){var ap=ai(ao,1);var an=al.nextSibling;ap.appendChild(aq);for(var am=an;am;am=an){an=am.nextSibling;ap.appendChild(am)}}return aq}var ah=ai(ak.nextSibling,0);for(var aj;(aj=ah.parentNode)&&aj.nodeType===1;){ah=aj}W.push(ah)}for(var Y=0;Y<W.length;++Y){ae(W[Y])}if(ag===(ag|0)){W[0].setAttribute("value",ag)}var aa=ac.createElement("OL");aa.className="linenums";var X=Math.max(0,((ag-1))|0)||0;for(var Y=0,T=W.length;Y<T;++Y){af=W[Y];af.className="L"+((Y+X)%10);if(!af.firstChild){af.appendChild(ac.createTextNode("\xA0"))}aa.appendChild(af)}V.appendChild(aa)}function D(ac){var aj=/\bMSIE\b/.test(navigator.userAgent);var am=/\n/g;var al=ac.sourceCode;var an=al.length;var V=0;var aa=ac.spans;var T=aa.length;var ah=0;var X=ac.decorations;var Y=X.length;var Z=0;X[Y]=an;var ar,aq;for(aq=ar=0;aq<Y;){if(X[aq]!==X[aq+2]){X[ar++]=X[aq++];X[ar++]=X[aq++]}else{aq+=2}}Y=ar;for(aq=ar=0;aq<Y;){var at=X[aq];var ab=X[aq+1];var W=aq+2;while(W+2<=Y&&X[W+1]===ab){W+=2}X[ar++]=at;X[ar++]=ab;aq=W}Y=X.length=ar;var ae=null;while(ah<T){var af=aa[ah];var S=aa[ah+2]||an;var ag=X[Z];var ap=X[Z+2]||an;var W=Math.min(S,ap);var ak=aa[ah+1];var U;if(ak.nodeType!==1&&(U=al.substring(V,W))){if(aj){U=U.replace(am,"\r")}ak.nodeValue=U;var ai=ak.ownerDocument;var ao=ai.createElement("SPAN");ao.className=X[Z+1];var ad=ak.parentNode;ad.replaceChild(ao,ak);ao.appendChild(ak);if(V<S){aa[ah+1]=ak=ai.createTextNode(al.substring(W,S));ad.insertBefore(ak,ao.nextSibling)}}V=W;if(V>=S){ah+=2}if(V>=ap){Z+=2}}}var t={};function c(U,V){for(var S=V.length;--S>=0;){var T=V[S];if(!t.hasOwnProperty(T)){t[T]=U}else{if(window.console){console.warn("cannot override language handler %s",T)}}}}function q(T,S){if(!(T&&t.hasOwnProperty(T))){T=/^\s*</.test(S)?"default-markup":"default-code"}return t[T]}c(K,["default-code"]);c(g([],[[F,/^[^<?]+/],[E,/^<!\w[^>]*(?:>|$)/],[j,/^<\!--[\s\S]*?(?:-\->|$)/],["lang-",/^<\?([\s\S]+?)(?:\?>|$)/],["lang-",/^<%([\s\S]+?)(?:%>|$)/],[L,/^(?:<[%?]|[%?]>)/],["lang-",/^<xmp\b[^>]*>([\s\S]+?)<\/xmp\b[^>]*>/i],["lang-js",/^<script\b[^>]*>([\s\S]*?)(<\/script\b[^>]*>)/i],["lang-css",/^<style\b[^>]*>([\s\S]*?)(<\/style\b[^>]*>)/i],["lang-in.tag",/^(<\/?[a-z][^<>]*>)/i]]),["default-markup","htm","html","mxml","xhtml","xml","xsl"]);c(g([[F,/^[\s]+/,null," \t\r\n"],[n,/^(?:\"[^\"]*\"?|\'[^\']*\'?)/,null,"\"'"]],[[m,/^^<\/?[a-z](?:[\w.:-]*\w)?|\/?>$/i],[P,/^(?!style[\s=]|on)[a-z](?:[\w:-]*\w)?/i],["lang-uq.val",/^=\s*([^>\'\"\s]*(?:[^>\'\"\s\/]|\/(?=\s)))/],[L,/^[=<>\/]+/],["lang-js",/^on\w+\s*=\s*\"([^\"]+)\"/i],["lang-js",/^on\w+\s*=\s*\'([^\']+)\'/i],["lang-js",/^on\w+\s*=\s*([^\"\'>\s]+)/i],["lang-css",/^style\s*=\s*\"([^\"]+)\"/i],["lang-css",/^style\s*=\s*\'([^\']+)\'/i],["lang-css",/^style\s*=\s*([^\"\'>\s]+)/i]]),["in.tag"]);c(g([],[[n,/^[\s\S]+/]]),["uq.val"]);c(i({keywords:l,hashComments:true,cStyleComments:true,types:e}),["c","cc","cpp","cxx","cyc","m"]);c(i({keywords:"null,true,false"}),["json"]);c(i({keywords:R,hashComments:true,cStyleComments:true,verbatimStrings:true,types:e}),["cs"]);c(i({keywords:x,cStyleComments:true}),["java"]);c(i({keywords:H,hashComments:true,multiLineStrings:true}),["bsh","csh","sh"]);c(i({keywords:I,hashComments:true,multiLineStrings:true,tripleQuotedStrings:true}),["cv","py"]);c(i({keywords:s,hashComments:true,multiLineStrings:true,regexLiterals:true}),["perl","pl","pm"]);c(i({keywords:f,hashComments:true,multiLineStrings:true,regexLiterals:true}),["rb"]);c(i({keywords:w,cStyleComments:true,regexLiterals:true}),["js"]);c(i({keywords:r,hashComments:3,cStyleComments:true,multilineStrings:true,tripleQuotedStrings:true,regexLiterals:true}),["coffee"]);c(g([],[[C,/^[\s\S]+/]]),["regex"]);function d(V){var U=V.langExtension;try{var S=a(V.sourceNode);var T=S.sourceCode;V.sourceCode=T;V.spans=S.spans;V.basePos=0;q(U,T)(V);D(V)}catch(W){if("console" in window){console.log(W&&W.stack?W.stack:W)}}}function y(W,V,U){var S=document.createElement("PRE");S.innerHTML=W;if(U){Q(S,U)}var T={langExtension:V,numberLines:U,sourceNode:S};d(T);return S.innerHTML}function b(ad){function Y(af){return document.getElementsByTagName(af)}var ac=[Y("pre"),Y("code"),Y("xmp")];var T=[];for(var aa=0;aa<ac.length;++aa){for(var Z=0,V=ac[aa].length;Z<V;++Z){T.push(ac[aa][Z])}}ac=null;var W=Date;if(!W.now){W={now:function(){return +(new Date)}}}var X=0;var S;var ab=/\blang(?:uage)?-([\w.]+)(?!\S)/;var ae=/\bprettyprint\b/;function U(){var ag=(window.PR_SHOULD_USE_CONTINUATION?W.now()+250:Infinity);for(;X<T.length&&W.now()<ag;X++){var aj=T[X];var ai=aj.className;if(ai.indexOf("prettyprint")>=0){var ah=ai.match(ab);var am;if(!ah&&(am=o(aj))&&"CODE"===am.tagName){ah=am.className.match(ab)}if(ah){ah=ah[1]}var al=false;for(var ak=aj.parentNode;ak;ak=ak.parentNode){if((ak.tagName==="pre"||ak.tagName==="code"||ak.tagName==="xmp")&&ak.className&&ak.className.indexOf("prettyprint")>=0){al=true;break}}if(!al){var af=aj.className.match(/\blinenums\b(?::(\d+))?/);af=af?af[1]&&af[1].length?+af[1]:true:false;if(af){Q(aj,af)}S={langExtension:ah,sourceNode:aj,numberLines:af};d(S)}}}if(X<T.length){setTimeout(U,250)}else{if(ad){ad()}}}U()}window.prettyPrintOne=y;window.prettyPrint=b;window.PR={createSimpleLexer:g,registerLangHandler:c,sourceDecorator:i,PR_ATTRIB_NAME:P,PR_ATTRIB_VALUE:n,PR_COMMENT:j,PR_DECLARATION:E,PR_KEYWORD:z,PR_LITERAL:G,PR_NOCODE:N,PR_PLAIN:F,PR_PUNCTUATION:L,PR_SOURCE:J,PR_STRING:C,PR_TAG:m,PR_TYPE:O}})();PR.registerLangHandler(PR.createSimpleLexer([],[[PR.PR_DECLARATION,/^<!\w[^>]*(?:>|$)/],[PR.PR_COMMENT,/^<\!--[\s\S]*?(?:-\->|$)/],[PR.PR_PUNCTUATION,/^(?:<[%?]|[%?]>)/],["lang-",/^<\?([\s\S]+?)(?:\?>|$)/],["lang-",/^<%([\s\S]+?)(?:%>|$)/],["lang-",/^<xmp\b[^>]*>([\s\S]+?)<\/xmp\b[^>]*>/i],["lang-handlebars",/^<script\b[^>]*type\s*=\s*['"]?text\/x-handlebars-template['"]?\b[^>]*>([\s\S]*?)(<\/script\b[^>]*>)/i],["lang-js",/^<script\b[^>]*>([\s\S]*?)(<\/script\b[^>]*>)/i],["lang-css",/^<style\b[^>]*>([\s\S]*?)(<\/style\b[^>]*>)/i],["lang-in.tag",/^(<\/?[a-z][^<>]*>)/i],[PR.PR_DECLARATION,/^{{[#^>/]?\s*[\w.][^}]*}}/],[PR.PR_DECLARATION,/^{{&?\s*[\w.][^}]*}}/],[PR.PR_DECLARATION,/^{{{>?\s*[\w.][^}]*}}}/],[PR.PR_COMMENT,/^{{![^}]*}}/]]),["handlebars","hbs"]);PR.registerLangHandler(PR.createSimpleLexer([[PR.PR_PLAIN,/^[ \t\r\n\f]+/,null," \t\r\n\f"]],[[PR.PR_STRING,/^\"(?:[^\n\r\f\\\"]|\\(?:\r\n?|\n|\f)|\\[\s\S])*\"/,null],[PR.PR_STRING,/^\'(?:[^\n\r\f\\\']|\\(?:\r\n?|\n|\f)|\\[\s\S])*\'/,null],["lang-css-str",/^url\(([^\)\"\']*)\)/i],[PR.PR_KEYWORD,/^(?:url|rgb|\!important|@import|@page|@media|@charset|inherit)(?=[^\-\w]|$)/i,null],["lang-css-kw",/^(-?(?:[_a-z]|(?:\\[0-9a-f]+ ?))(?:[_a-z0-9\-]|\\(?:\\[0-9a-f]+ ?))*)\s*:/i],[PR.PR_COMMENT,/^\/\*[^*]*\*+(?:[^\/*][^*]*\*+)*\//],[PR.PR_COMMENT,/^(?:<!--|-->)/],[PR.PR_LITERAL,/^(?:\d+|\d*\.\d+)(?:%|[a-z]+)?/i],[PR.PR_LITERAL,/^#(?:[0-9a-f]{3}){1,2}/i],[PR.PR_PLAIN,/^-?(?:[_a-z]|(?:\\[\da-f]+ ?))(?:[_a-z\d\-]|\\(?:\\[\da-f]+ ?))*/i],[PR.PR_PUNCTUATION,/^[^\s\w\'\"]+/]]),["css"]);PR.registerLangHandler(PR.createSimpleLexer([],[[PR.PR_KEYWORD,/^-?(?:[_a-z]|(?:\\[\da-f]+ ?))(?:[_a-z\d\-]|\\(?:\\[\da-f]+ ?))*/i]]),["css-kw"]);PR.registerLangHandler(PR.createSimpleLexer([],[[PR.PR_STRING,/^[^\)\"\']+/]]),["css-str"]);
;
if (!Function.prototype.bind) Function.prototype.bind = function (scope) { var me = this; return function () { return me.apply(scope, arguments) } };
/*
  Copyright (C) 2012 Ariya Hidayat <ariya.hidayat@gmail.com>
  Copyright (C) 2012 Mathias Bynens <mathias@qiwi.be>
  Copyright (C) 2012 Joost-Wim Boekesteijn <joost-wim@boekesteijn.nl>
  Copyright (C) 2012 Kris Kowal <kris.kowal@cixar.com>
  Copyright (C) 2012 Yusuke Suzuki <utatane.tea@gmail.com>
  Copyright (C) 2012 Arpad Borsos <arpad.borsos@googlemail.com>
  Copyright (C) 2011 Ariya Hidayat <ariya.hidayat@gmail.com>

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*jslint bitwise:true plusplus:true */
/*global esprima:true, define:true, exports:true, window: true,
throwError: true, createLiteral: true, generateStatement: true,
parseAssignmentExpression: true, parseBlock: true, parseExpression: true,
parseFunctionDeclaration: true, parseFunctionExpression: true,
parseFunctionSourceElements: true, parseVariableIdentifier: true,
parseLeftHandSideExpression: true,
parseStatement: true, parseSourceElement: true */

(function (root, factory) {
    'use strict';

    // Universal Module Definition (UMD) to support AMD, CommonJS/Node.js,
    // Rhino, and plain browser loading.
    if (typeof define === 'function' && define.amd) {
        define(['exports'], factory);
    } else if (typeof exports !== 'undefined') {
        factory(exports);
    } else {
        factory((root.esprima = {}));
    }
}(this, function (exports) {
    'use strict';

    var Token,
        TokenName,
        Syntax,
        PropertyKind,
        Messages,
        Regex,
        source,
        strict,
        index,
        lineNumber,
        lineStart,
        length,
        buffer,
        state,
        extra;

    Token = {
        BooleanLiteral: 1,
        EOF: 2,
        Identifier: 3,
        Keyword: 4,
        NullLiteral: 5,
        NumericLiteral: 6,
        Punctuator: 7,
        StringLiteral: 8
    };

    TokenName = {};
    TokenName[Token.BooleanLiteral] = 'Boolean';
    TokenName[Token.EOF] = '<end>';
    TokenName[Token.Identifier] = 'Identifier';
    TokenName[Token.Keyword] = 'Keyword';
    TokenName[Token.NullLiteral] = 'Null';
    TokenName[Token.NumericLiteral] = 'Numeric';
    TokenName[Token.Punctuator] = 'Punctuator';
    TokenName[Token.StringLiteral] = 'String';

    Syntax = {
        AssignmentExpression: 'AssignmentExpression',
        ArrayExpression: 'ArrayExpression',
        BlockStatement: 'BlockStatement',
        BinaryExpression: 'BinaryExpression',
        BreakStatement: 'BreakStatement',
        CallExpression: 'CallExpression',
        CatchClause: 'CatchClause',
        ConditionalExpression: 'ConditionalExpression',
        ContinueStatement: 'ContinueStatement',
        DoWhileStatement: 'DoWhileStatement',
        DebuggerStatement: 'DebuggerStatement',
        EmptyStatement: 'EmptyStatement',
        ExpressionStatement: 'ExpressionStatement',
        ForStatement: 'ForStatement',
        ForInStatement: 'ForInStatement',
        FunctionDeclaration: 'FunctionDeclaration',
        FunctionExpression: 'FunctionExpression',
        Identifier: 'Identifier',
        IfStatement: 'IfStatement',
        Literal: 'Literal',
        LabeledStatement: 'LabeledStatement',
        LogicalExpression: 'LogicalExpression',
        MemberExpression: 'MemberExpression',
        NewExpression: 'NewExpression',
        ObjectExpression: 'ObjectExpression',
        Program: 'Program',
        Property: 'Property',
        ReturnStatement: 'ReturnStatement',
        SequenceExpression: 'SequenceExpression',
        SwitchStatement: 'SwitchStatement',
        SwitchCase: 'SwitchCase',
        ThisExpression: 'ThisExpression',
        ThrowStatement: 'ThrowStatement',
        TryStatement: 'TryStatement',
        UnaryExpression: 'UnaryExpression',
        UpdateExpression: 'UpdateExpression',
        VariableDeclaration: 'VariableDeclaration',
        VariableDeclarator: 'VariableDeclarator',
        WhileStatement: 'WhileStatement',
        WithStatement: 'WithStatement'
    };

    PropertyKind = {
        Data: 1,
        Get: 2,
        Set: 4
    };

    // Error messages should be identical to V8.
    Messages = {
        UnexpectedToken:  'Unexpected token %0',
        UnexpectedNumber:  'Unexpected number',
        UnexpectedString:  'Unexpected string',
        UnexpectedIdentifier:  'Unexpected identifier',
        UnexpectedReserved:  'Unexpected reserved word',
        UnexpectedEOS:  'Unexpected end of input',
        NewlineAfterThrow:  'Illegal newline after throw',
        InvalidRegExp: 'Invalid regular expression',
        UnterminatedRegExp:  'Invalid regular expression: missing /',
        InvalidLHSInAssignment:  'Invalid left-hand side in assignment',
        InvalidLHSInForIn:  'Invalid left-hand side in for-in',
        MultipleDefaultsInSwitch: 'More than one default clause in switch statement',
        NoCatchOrFinally:  'Missing catch or finally after try',
        UnknownLabel: 'Undefined label \'%0\'',
        Redeclaration: '%0 \'%1\' has already been declared',
        IllegalContinue: 'Illegal continue statement',
        IllegalBreak: 'Illegal break statement',
        IllegalReturn: 'Illegal return statement',
        StrictModeWith:  'Strict mode code may not include a with statement',
        StrictCatchVariable:  'Catch variable may not be eval or arguments in strict mode',
        StrictVarName:  'Variable name may not be eval or arguments in strict mode',
        StrictParamName:  'Parameter name eval or arguments is not allowed in strict mode',
        StrictParamDupe: 'Strict mode function may not have duplicate parameter names',
        StrictFunctionName:  'Function name may not be eval or arguments in strict mode',
        StrictOctalLiteral:  'Octal literals are not allowed in strict mode.',
        StrictDelete:  'Delete of an unqualified identifier in strict mode.',
        StrictDuplicateProperty:  'Duplicate data property in object literal not allowed in strict mode',
        AccessorDataProperty:  'Object literal may not have data and accessor property with the same name',
        AccessorGetSet:  'Object literal may not have multiple get/set accessors with the same name',
        StrictLHSAssignment:  'Assignment to eval or arguments is not allowed in strict mode',
        StrictLHSPostfix:  'Postfix increment/decrement may not have eval or arguments operand in strict mode',
        StrictLHSPrefix:  'Prefix increment/decrement may not have eval or arguments operand in strict mode',
        StrictReservedWord:  'Use of future reserved word in strict mode'
    };

    // See also tools/generate-unicode-regex.py.
    Regex = {
        NonAsciiIdentifierStart: new RegExp('[\xaa\xb5\xba\xc0-\xd6\xd8-\xf6\xf8-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0370-\u0374\u0376\u0377\u037a-\u037d\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0481\u048a-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05d0-\u05ea\u05f0-\u05f2\u0620-\u064a\u066e\u066f\u0671-\u06d3\u06d5\u06e5\u06e6\u06ee\u06ef\u06fa-\u06fc\u06ff\u0710\u0712-\u072f\u074d-\u07a5\u07b1\u07ca-\u07ea\u07f4\u07f5\u07fa\u0800-\u0815\u081a\u0824\u0828\u0840-\u0858\u08a0\u08a2-\u08ac\u0904-\u0939\u093d\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097f\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bd\u09ce\u09dc\u09dd\u09df-\u09e1\u09f0\u09f1\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a59-\u0a5c\u0a5e\u0a72-\u0a74\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abd\u0ad0\u0ae0\u0ae1\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3d\u0b5c\u0b5d\u0b5f-\u0b61\u0b71\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bd0\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c33\u0c35-\u0c39\u0c3d\u0c58\u0c59\u0c60\u0c61\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbd\u0cde\u0ce0\u0ce1\u0cf1\u0cf2\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d\u0d4e\u0d60\u0d61\u0d7a-\u0d7f\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0e01-\u0e30\u0e32\u0e33\u0e40-\u0e46\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb0\u0eb2\u0eb3\u0ebd\u0ec0-\u0ec4\u0ec6\u0edc-\u0edf\u0f00\u0f40-\u0f47\u0f49-\u0f6c\u0f88-\u0f8c\u1000-\u102a\u103f\u1050-\u1055\u105a-\u105d\u1061\u1065\u1066\u106e-\u1070\u1075-\u1081\u108e\u10a0-\u10c5\u10c7\u10cd\u10d0-\u10fa\u10fc-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u1380-\u138f\u13a0-\u13f4\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u16ee-\u16f0\u1700-\u170c\u170e-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176c\u176e-\u1770\u1780-\u17b3\u17d7\u17dc\u1820-\u1877\u1880-\u18a8\u18aa\u18b0-\u18f5\u1900-\u191c\u1950-\u196d\u1970-\u1974\u1980-\u19ab\u19c1-\u19c7\u1a00-\u1a16\u1a20-\u1a54\u1aa7\u1b05-\u1b33\u1b45-\u1b4b\u1b83-\u1ba0\u1bae\u1baf\u1bba-\u1be5\u1c00-\u1c23\u1c4d-\u1c4f\u1c5a-\u1c7d\u1ce9-\u1cec\u1cee-\u1cf1\u1cf5\u1cf6\u1d00-\u1dbf\u1e00-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u2071\u207f\u2090-\u209c\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u2160-\u2188\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cee\u2cf2\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d80-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u2e2f\u3005-\u3007\u3021-\u3029\u3031-\u3035\u3038-\u303c\u3041-\u3096\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fcc\ua000-\ua48c\ua4d0-\ua4fd\ua500-\ua60c\ua610-\ua61f\ua62a\ua62b\ua640-\ua66e\ua67f-\ua697\ua6a0-\ua6ef\ua717-\ua71f\ua722-\ua788\ua78b-\ua78e\ua790-\ua793\ua7a0-\ua7aa\ua7f8-\ua801\ua803-\ua805\ua807-\ua80a\ua80c-\ua822\ua840-\ua873\ua882-\ua8b3\ua8f2-\ua8f7\ua8fb\ua90a-\ua925\ua930-\ua946\ua960-\ua97c\ua984-\ua9b2\ua9cf\uaa00-\uaa28\uaa40-\uaa42\uaa44-\uaa4b\uaa60-\uaa76\uaa7a\uaa80-\uaaaf\uaab1\uaab5\uaab6\uaab9-\uaabd\uaac0\uaac2\uaadb-\uaadd\uaae0-\uaaea\uaaf2-\uaaf4\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uabc0-\uabe2\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf900-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\ufb1d\ufb1f-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe70-\ufe74\ufe76-\ufefc\uff21-\uff3a\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc]'),
        NonAsciiIdentifierPart: new RegExp('[\xaa\xb5\xba\xc0-\xd6\xd8-\xf6\xf8-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0300-\u0374\u0376\u0377\u037a-\u037d\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0481\u0483-\u0487\u048a-\u0527\u0531-\u0556\u0559\u0561-\u0587\u0591-\u05bd\u05bf\u05c1\u05c2\u05c4\u05c5\u05c7\u05d0-\u05ea\u05f0-\u05f2\u0610-\u061a\u0620-\u0669\u066e-\u06d3\u06d5-\u06dc\u06df-\u06e8\u06ea-\u06fc\u06ff\u0710-\u074a\u074d-\u07b1\u07c0-\u07f5\u07fa\u0800-\u082d\u0840-\u085b\u08a0\u08a2-\u08ac\u08e4-\u08fe\u0900-\u0963\u0966-\u096f\u0971-\u0977\u0979-\u097f\u0981-\u0983\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bc-\u09c4\u09c7\u09c8\u09cb-\u09ce\u09d7\u09dc\u09dd\u09df-\u09e3\u09e6-\u09f1\u0a01-\u0a03\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a3c\u0a3e-\u0a42\u0a47\u0a48\u0a4b-\u0a4d\u0a51\u0a59-\u0a5c\u0a5e\u0a66-\u0a75\u0a81-\u0a83\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abc-\u0ac5\u0ac7-\u0ac9\u0acb-\u0acd\u0ad0\u0ae0-\u0ae3\u0ae6-\u0aef\u0b01-\u0b03\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3c-\u0b44\u0b47\u0b48\u0b4b-\u0b4d\u0b56\u0b57\u0b5c\u0b5d\u0b5f-\u0b63\u0b66-\u0b6f\u0b71\u0b82\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bbe-\u0bc2\u0bc6-\u0bc8\u0bca-\u0bcd\u0bd0\u0bd7\u0be6-\u0bef\u0c01-\u0c03\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c33\u0c35-\u0c39\u0c3d-\u0c44\u0c46-\u0c48\u0c4a-\u0c4d\u0c55\u0c56\u0c58\u0c59\u0c60-\u0c63\u0c66-\u0c6f\u0c82\u0c83\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbc-\u0cc4\u0cc6-\u0cc8\u0cca-\u0ccd\u0cd5\u0cd6\u0cde\u0ce0-\u0ce3\u0ce6-\u0cef\u0cf1\u0cf2\u0d02\u0d03\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d-\u0d44\u0d46-\u0d48\u0d4a-\u0d4e\u0d57\u0d60-\u0d63\u0d66-\u0d6f\u0d7a-\u0d7f\u0d82\u0d83\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0dca\u0dcf-\u0dd4\u0dd6\u0dd8-\u0ddf\u0df2\u0df3\u0e01-\u0e3a\u0e40-\u0e4e\u0e50-\u0e59\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb9\u0ebb-\u0ebd\u0ec0-\u0ec4\u0ec6\u0ec8-\u0ecd\u0ed0-\u0ed9\u0edc-\u0edf\u0f00\u0f18\u0f19\u0f20-\u0f29\u0f35\u0f37\u0f39\u0f3e-\u0f47\u0f49-\u0f6c\u0f71-\u0f84\u0f86-\u0f97\u0f99-\u0fbc\u0fc6\u1000-\u1049\u1050-\u109d\u10a0-\u10c5\u10c7\u10cd\u10d0-\u10fa\u10fc-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u135d-\u135f\u1380-\u138f\u13a0-\u13f4\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u16ee-\u16f0\u1700-\u170c\u170e-\u1714\u1720-\u1734\u1740-\u1753\u1760-\u176c\u176e-\u1770\u1772\u1773\u1780-\u17d3\u17d7\u17dc\u17dd\u17e0-\u17e9\u180b-\u180d\u1810-\u1819\u1820-\u1877\u1880-\u18aa\u18b0-\u18f5\u1900-\u191c\u1920-\u192b\u1930-\u193b\u1946-\u196d\u1970-\u1974\u1980-\u19ab\u19b0-\u19c9\u19d0-\u19d9\u1a00-\u1a1b\u1a20-\u1a5e\u1a60-\u1a7c\u1a7f-\u1a89\u1a90-\u1a99\u1aa7\u1b00-\u1b4b\u1b50-\u1b59\u1b6b-\u1b73\u1b80-\u1bf3\u1c00-\u1c37\u1c40-\u1c49\u1c4d-\u1c7d\u1cd0-\u1cd2\u1cd4-\u1cf6\u1d00-\u1de6\u1dfc-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u200c\u200d\u203f\u2040\u2054\u2071\u207f\u2090-\u209c\u20d0-\u20dc\u20e1\u20e5-\u20f0\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u2160-\u2188\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d7f-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u2de0-\u2dff\u2e2f\u3005-\u3007\u3021-\u302f\u3031-\u3035\u3038-\u303c\u3041-\u3096\u3099\u309a\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fcc\ua000-\ua48c\ua4d0-\ua4fd\ua500-\ua60c\ua610-\ua62b\ua640-\ua66f\ua674-\ua67d\ua67f-\ua697\ua69f-\ua6f1\ua717-\ua71f\ua722-\ua788\ua78b-\ua78e\ua790-\ua793\ua7a0-\ua7aa\ua7f8-\ua827\ua840-\ua873\ua880-\ua8c4\ua8d0-\ua8d9\ua8e0-\ua8f7\ua8fb\ua900-\ua92d\ua930-\ua953\ua960-\ua97c\ua980-\ua9c0\ua9cf-\ua9d9\uaa00-\uaa36\uaa40-\uaa4d\uaa50-\uaa59\uaa60-\uaa76\uaa7a\uaa7b\uaa80-\uaac2\uaadb-\uaadd\uaae0-\uaaef\uaaf2-\uaaf6\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uabc0-\uabea\uabec\uabed\uabf0-\uabf9\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf900-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\ufb1d-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe00-\ufe0f\ufe20-\ufe26\ufe33\ufe34\ufe4d-\ufe4f\ufe70-\ufe74\ufe76-\ufefc\uff10-\uff19\uff21-\uff3a\uff3f\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc]')
    };

    // Ensure the condition is true, otherwise throw an error.
    // This is only to have a better contract semantic, i.e. another safety net
    // to catch a logic error. The condition shall be fulfilled in normal case.
    // Do NOT use this to enforce a certain condition on any user input.

    function assert(condition, message) {
        if (!condition) {
            throw new Error('ASSERT: ' + message);
        }
    }

    function sliceSource(from, to) {
        return source.slice(from, to);
    }

    if (typeof 'esprima'[0] === 'undefined') {
        sliceSource = function sliceArraySource(from, to) {
            return source.slice(from, to).join('');
        };
    }

    function isDecimalDigit(ch) {
        return '0123456789'.indexOf(ch) >= 0;
    }

    function isHexDigit(ch) {
        return '0123456789abcdefABCDEF'.indexOf(ch) >= 0;
    }

    function isOctalDigit(ch) {
        return '01234567'.indexOf(ch) >= 0;
    }


    // 7.2 White Space

    function isWhiteSpace(ch) {
        return (ch === ' ') || (ch === '\u0009') || (ch === '\u000B') ||
            (ch === '\u000C') || (ch === '\u00A0') ||
            (ch.charCodeAt(0) >= 0x1680 &&
             '\u1680\u180E\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\uFEFF'.indexOf(ch) >= 0);
    }

    // 7.3 Line Terminators

    function isLineTerminator(ch) {
        return (ch === '\n' || ch === '\r' || ch === '\u2028' || ch === '\u2029');
    }

    // 7.6 Identifier Names and Identifiers

    function isIdentifierStart(ch) {
        return (ch === '$') || (ch === '_') || (ch === '\\') ||
            (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') ||
            ((ch.charCodeAt(0) >= 0x80) && Regex.NonAsciiIdentifierStart.test(ch));
    }

    function isIdentifierPart(ch) {
        return (ch === '$') || (ch === '_') || (ch === '\\') ||
            (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') ||
            ((ch >= '0') && (ch <= '9')) ||
            ((ch.charCodeAt(0) >= 0x80) && Regex.NonAsciiIdentifierPart.test(ch));
    }

    // 7.6.1.2 Future Reserved Words

    function isFutureReservedWord(id) {
        switch (id) {

        // Future reserved words.
        case 'class':
        case 'enum':
        case 'export':
        case 'extends':
        case 'import':
        case 'super':
            return true;
        }

        return false;
    }

    function isStrictModeReservedWord(id) {
        switch (id) {

        // Strict Mode reserved words.
        case 'implements':
        case 'interface':
        case 'package':
        case 'private':
        case 'protected':
        case 'public':
        case 'static':
        case 'yield':
        case 'let':
            return true;
        }

        return false;
    }

    function isRestrictedWord(id) {
        return id === 'eval' || id === 'arguments';
    }

    // 7.6.1.1 Keywords

    function isKeyword(id) {
        var keyword = false;
        switch (id.length) {
        case 2:
            keyword = (id === 'if') || (id === 'in') || (id === 'do');
            break;
        case 3:
            keyword = (id === 'var') || (id === 'for') || (id === 'new') || (id === 'try');
            break;
        case 4:
            keyword = (id === 'this') || (id === 'else') || (id === 'case') || (id === 'void') || (id === 'with');
            break;
        case 5:
            keyword = (id === 'while') || (id === 'break') || (id === 'catch') || (id === 'throw');
            break;
        case 6:
            keyword = (id === 'return') || (id === 'typeof') || (id === 'delete') || (id === 'switch');
            break;
        case 7:
            keyword = (id === 'default') || (id === 'finally');
            break;
        case 8:
            keyword = (id === 'function') || (id === 'continue') || (id === 'debugger');
            break;
        case 10:
            keyword = (id === 'instanceof');
            break;
        }

        if (keyword) {
            return true;
        }

        switch (id) {
        // Future reserved words.
        // 'const' is specialized as Keyword in V8.
        case 'const':
            return true;

        // For compatiblity to SpiderMonkey and ES.next
        case 'yield':
        case 'let':
            return true;
        }

        if (strict && isStrictModeReservedWord(id)) {
            return true;
        }

        return isFutureReservedWord(id);
    }

    // 7.4 Comments

    function skipComment() {
        var ch, blockComment, lineComment;

        blockComment = false;
        lineComment = false;

        while (index < length) {
            ch = source[index];

            if (lineComment) {
                ch = source[index++];
                if (isLineTerminator(ch)) {
                    lineComment = false;
                    if (ch === '\r' && source[index] === '\n') {
                        ++index;
                    }
                    ++lineNumber;
                    lineStart = index;
                }
            } else if (blockComment) {
                if (isLineTerminator(ch)) {
                    if (ch === '\r' && source[index + 1] === '\n') {
                        ++index;
                    }
                    ++lineNumber;
                    ++index;
                    lineStart = index;
                    if (index >= length) {
                        throwError({}, Messages.UnexpectedToken, 'ILLEGAL');
                    }
                } else {
                    ch = source[index++];
                    if (index >= length) {
                        throwError({}, Messages.UnexpectedToken, 'ILLEGAL');
                    }
                    if (ch === '*') {
                        ch = source[index];
                        if (ch === '/') {
                            ++index;
                            blockComment = false;
                        }
                    }
                }
            } else if (ch === '/') {
                ch = source[index + 1];
                if (ch === '/') {
                    index += 2;
                    lineComment = true;
                } else if (ch === '*') {
                    index += 2;
                    blockComment = true;
                    if (index >= length) {
                        throwError({}, Messages.UnexpectedToken, 'ILLEGAL');
                    }
                } else {
                    break;
                }
            } else if (isWhiteSpace(ch)) {
                ++index;
            } else if (isLineTerminator(ch)) {
                ++index;
                if (ch ===  '\r' && source[index] === '\n') {
                    ++index;
                }
                ++lineNumber;
                lineStart = index;
            } else {
                break;
            }
        }
    }

    function scanHexEscape(prefix) {
        var i, len, ch, code = 0;

        len = (prefix === 'u') ? 4 : 2;
        for (i = 0; i < len; ++i) {
            if (index < length && isHexDigit(source[index])) {
                ch = source[index++];
                code = code * 16 + '0123456789abcdef'.indexOf(ch.toLowerCase());
            } else {
                return '';
            }
        }
        return String.fromCharCode(code);
    }

    function scanIdentifier() {
        var ch, start, id, restore;

        ch = source[index];
        if (!isIdentifierStart(ch)) {
            return;
        }

        start = index;
        if (ch === '\\') {
            ++index;
            if (source[index] !== 'u') {
                return;
            }
            ++index;
            restore = index;
            ch = scanHexEscape('u');
            if (ch) {
                if (ch === '\\' || !isIdentifierStart(ch)) {
                    return;
                }
                id = ch;
            } else {
                index = restore;
                id = 'u';
            }
        } else {
            id = source[index++];
        }

        while (index < length) {
            ch = source[index];
            if (!isIdentifierPart(ch)) {
                break;
            }
            if (ch === '\\') {
                ++index;
                if (source[index] !== 'u') {
                    return;
                }
                ++index;
                restore = index;
                ch = scanHexEscape('u');
                if (ch) {
                    if (ch === '\\' || !isIdentifierPart(ch)) {
                        return;
                    }
                    id += ch;
                } else {
                    index = restore;
                    id += 'u';
                }
            } else {
                id += source[index++];
            }
        }

        // There is no keyword or literal with only one character.
        // Thus, it must be an identifier.
        if (id.length === 1) {
            return {
                type: Token.Identifier,
                value: id,
                lineNumber: lineNumber,
                lineStart: lineStart,
                range: [start, index]
            };
        }

        if (isKeyword(id)) {
            return {
                type: Token.Keyword,
                value: id,
                lineNumber: lineNumber,
                lineStart: lineStart,
                range: [start, index]
            };
        }

        // 7.8.1 Null Literals

        if (id === 'null') {
            return {
                type: Token.NullLiteral,
                value: id,
                lineNumber: lineNumber,
                lineStart: lineStart,
                range: [start, index]
            };
        }

        // 7.8.2 Boolean Literals

        if (id === 'true' || id === 'false') {
            return {
                type: Token.BooleanLiteral,
                value: id,
                lineNumber: lineNumber,
                lineStart: lineStart,
                range: [start, index]
            };
        }

        return {
            type: Token.Identifier,
            value: id,
            lineNumber: lineNumber,
            lineStart: lineStart,
            range: [start, index]
        };
    }

    // 7.7 Punctuators

    function scanPunctuator() {
        var start = index,
            ch1 = source[index],
            ch2,
            ch3,
            ch4;

        // Check for most common single-character punctuators.

        if (ch1 === ';' || ch1 === '{' || ch1 === '}') {
            ++index;
            return {
                type: Token.Punctuator,
                value: ch1,
                lineNumber: lineNumber,
                lineStart: lineStart,
                range: [start, index]
            };
        }

        if (ch1 === ',' || ch1 === '(' || ch1 === ')') {
            ++index;
            return {
                type: Token.Punctuator,
                value: ch1,
                lineNumber: lineNumber,
                lineStart: lineStart,
                range: [start, index]
            };
        }

        // Dot (.) can also start a floating-point number, hence the need
        // to check the next character.

        ch2 = source[index + 1];
        if (ch1 === '.' && !isDecimalDigit(ch2)) {
            return {
                type: Token.Punctuator,
                value: source[index++],
                lineNumber: lineNumber,
                lineStart: lineStart,
                range: [start, index]
            };
        }

        // Peek more characters.

        ch3 = source[index + 2];
        ch4 = source[index + 3];

        // 4-character punctuator: >>>=

        if (ch1 === '>' && ch2 === '>' && ch3 === '>') {
            if (ch4 === '=') {
                index += 4;
                return {
                    type: Token.Punctuator,
                    value: '>>>=',
                    lineNumber: lineNumber,
                    lineStart: lineStart,
                    range: [start, index]
                };
            }
        }

        // 3-character punctuators: === !== >>> <<= >>=

        if (ch1 === '=' && ch2 === '=' && ch3 === '=') {
            index += 3;
            return {
                type: Token.Punctuator,
                value: '===',
                lineNumber: lineNumber,
                lineStart: lineStart,
                range: [start, index]
            };
        }

        if (ch1 === '!' && ch2 === '=' && ch3 === '=') {
            index += 3;
            return {
                type: Token.Punctuator,
                value: '!==',
                lineNumber: lineNumber,
                lineStart: lineStart,
                range: [start, index]
            };
        }

        if (ch1 === '>' && ch2 === '>' && ch3 === '>') {
            index += 3;
            return {
                type: Token.Punctuator,
                value: '>>>',
                lineNumber: lineNumber,
                lineStart: lineStart,
                range: [start, index]
            };
        }

        if (ch1 === '<' && ch2 === '<' && ch3 === '=') {
            index += 3;
            return {
                type: Token.Punctuator,
                value: '<<=',
                lineNumber: lineNumber,
                lineStart: lineStart,
                range: [start, index]
            };
        }

        if (ch1 === '>' && ch2 === '>' && ch3 === '=') {
            index += 3;
            return {
                type: Token.Punctuator,
                value: '>>=',
                lineNumber: lineNumber,
                lineStart: lineStart,
                range: [start, index]
            };
        }

        // 2-character punctuators: <= >= == != ++ -- << >> && ||
        // += -= *= %= &= |= ^= /=

        if (ch2 === '=') {
            if ('<>=!+-*%&|^/'.indexOf(ch1) >= 0) {
                index += 2;
                return {
                    type: Token.Punctuator,
                    value: ch1 + ch2,
                    lineNumber: lineNumber,
                    lineStart: lineStart,
                    range: [start, index]
                };
            }
        }

        if (ch1 === ch2 && ('+-<>&|'.indexOf(ch1) >= 0)) {
            if ('+-<>&|'.indexOf(ch2) >= 0) {
                index += 2;
                return {
                    type: Token.Punctuator,
                    value: ch1 + ch2,
                    lineNumber: lineNumber,
                    lineStart: lineStart,
                    range: [start, index]
                };
            }
        }

        // The remaining 1-character punctuators.

        if ('[]<>+-*%&|^!~?:=/'.indexOf(ch1) >= 0) {
            return {
                type: Token.Punctuator,
                value: source[index++],
                lineNumber: lineNumber,
                lineStart: lineStart,
                range: [start, index]
            };
        }
    }

    // 7.8.3 Numeric Literals

    function scanNumericLiteral() {
        var number, start, ch;

        ch = source[index];
        assert(isDecimalDigit(ch) || (ch === '.'),
            'Numeric literal must start with a decimal digit or a decimal point');

        start = index;
        number = '';
        if (ch !== '.') {
            number = source[index++];
            ch = source[index];

            // Hex number starts with '0x'.
            // Octal number starts with '0'.
            if (number === '0') {
                if (ch === 'x' || ch === 'X') {
                    number += source[index++];
                    while (index < length) {
                        ch = source[index];
                        if (!isHexDigit(ch)) {
                            break;
                        }
                        number += source[index++];
                    }

                    if (number.length <= 2) {
                        // only 0x
                        throwError({}, Messages.UnexpectedToken, 'ILLEGAL');
                    }

                    if (index < length) {
                        ch = source[index];
                        if (isIdentifierStart(ch)) {
                            throwError({}, Messages.UnexpectedToken, 'ILLEGAL');
                        }
                    }
                    return {
                        type: Token.NumericLiteral,
                        value: parseInt(number, 16),
                        lineNumber: lineNumber,
                        lineStart: lineStart,
                        range: [start, index]
                    };
                } else if (isOctalDigit(ch)) {
                    number += source[index++];
                    while (index < length) {
                        ch = source[index];
                        if (!isOctalDigit(ch)) {
                            break;
                        }
                        number += source[index++];
                    }

                    if (index < length) {
                        ch = source[index];
                        if (isIdentifierStart(ch) || isDecimalDigit(ch)) {
                            throwError({}, Messages.UnexpectedToken, 'ILLEGAL');
                        }
                    }
                    return {
                        type: Token.NumericLiteral,
                        value: parseInt(number, 8),
                        octal: true,
                        lineNumber: lineNumber,
                        lineStart: lineStart,
                        range: [start, index]
                    };
                }

                // decimal number starts with '0' such as '09' is illegal.
                if (isDecimalDigit(ch)) {
                    throwError({}, Messages.UnexpectedToken, 'ILLEGAL');
                }
            }

            while (index < length) {
                ch = source[index];
                if (!isDecimalDigit(ch)) {
                    break;
                }
                number += source[index++];
            }
        }

        if (ch === '.') {
            number += source[index++];
            while (index < length) {
                ch = source[index];
                if (!isDecimalDigit(ch)) {
                    break;
                }
                number += source[index++];
            }
        }

        if (ch === 'e' || ch === 'E') {
            number += source[index++];

            ch = source[index];
            if (ch === '+' || ch === '-') {
                number += source[index++];
            }

            ch = source[index];
            if (isDecimalDigit(ch)) {
                number += source[index++];
                while (index < length) {
                    ch = source[index];
                    if (!isDecimalDigit(ch)) {
                        break;
                    }
                    number += source[index++];
                }
            } else {
                ch = 'character ' + ch;
                if (index >= length) {
                    ch = '<end>';
                }
                throwError({}, Messages.UnexpectedToken, 'ILLEGAL');
            }
        }

        if (index < length) {
            ch = source[index];
            if (isIdentifierStart(ch)) {
                throwError({}, Messages.UnexpectedToken, 'ILLEGAL');
            }
        }

        return {
            type: Token.NumericLiteral,
            value: parseFloat(number),
            lineNumber: lineNumber,
            lineStart: lineStart,
            range: [start, index]
        };
    }

    // 7.8.4 String Literals

    function scanStringLiteral() {
        var str = '', quote, start, ch, code, unescaped, restore, octal = false;

        quote = source[index];
        assert((quote === '\'' || quote === '"'),
            'String literal must starts with a quote');

        start = index;
        ++index;

        while (index < length) {
            ch = source[index++];

            if (ch === quote) {
                quote = '';
                break;
            } else if (ch === '\\') {
                ch = source[index++];
                if (!isLineTerminator(ch)) {
                    switch (ch) {
                    case 'n':
                        str += '\n';
                        break;
                    case 'r':
                        str += '\r';
                        break;
                    case 't':
                        str += '\t';
                        break;
                    case 'u':
                    case 'x':
                        restore = index;
                        unescaped = scanHexEscape(ch);
                        if (unescaped) {
                            str += unescaped;
                        } else {
                            index = restore;
                            str += ch;
                        }
                        break;
                    case 'b':
                        str += '\b';
                        break;
                    case 'f':
                        str += '\f';
                        break;
                    case 'v':
                        str += '\x0B';
                        break;

                    default:
                        if (isOctalDigit(ch)) {
                            code = '01234567'.indexOf(ch);

                            // \0 is not octal escape sequence
                            if (code !== 0) {
                                octal = true;
                            }

                            if (index < length && isOctalDigit(source[index])) {
                                octal = true;
                                code = code * 8 + '01234567'.indexOf(source[index++]);

                                // 3 digits are only allowed when string starts
                                // with 0, 1, 2, 3
                                if ('0123'.indexOf(ch) >= 0 &&
                                        index < length &&
                                        isOctalDigit(source[index])) {
                                    code = code * 8 + '01234567'.indexOf(source[index++]);
                                }
                            }
                            str += String.fromCharCode(code);
                        } else {
                            str += ch;
                        }
                        break;
                    }
                } else {
                    ++lineNumber;
                    if (ch ===  '\r' && source[index] === '\n') {
                        ++index;
                    }
                }
            } else if (isLineTerminator(ch)) {
                break;
            } else {
                str += ch;
            }
        }

        if (quote !== '') {
            throwError({}, Messages.UnexpectedToken, 'ILLEGAL');
        }

        return {
            type: Token.StringLiteral,
            value: str,
            octal: octal,
            lineNumber: lineNumber,
            lineStart: lineStart,
            range: [start, index]
        };
    }

    function scanRegExp() {
        var str, ch, start, pattern, flags, value, classMarker = false, restore, terminated = false;

        buffer = null;
        skipComment();

        start = index;
        ch = source[index];
        assert(ch === '/', 'Regular expression literal must start with a slash');
        str = source[index++];

        while (index < length) {
            ch = source[index++];
            str += ch;
            if (classMarker) {
                if (ch === ']') {
                    classMarker = false;
                }
            } else {
                if (ch === '\\') {
                    ch = source[index++];
                    // ECMA-262 7.8.5
                    if (isLineTerminator(ch)) {
                        throwError({}, Messages.UnterminatedRegExp);
                    }
                    str += ch;
                } else if (ch === '/') {
                    terminated = true;
                    break;
                } else if (ch === '[') {
                    classMarker = true;
                } else if (isLineTerminator(ch)) {
                    throwError({}, Messages.UnterminatedRegExp);
                }
            }
        }

        if (!terminated) {
            throwError({}, Messages.UnterminatedRegExp);
        }

        // Exclude leading and trailing slash.
        pattern = str.substr(1, str.length - 2);

        flags = '';
        while (index < length) {
            ch = source[index];
            if (!isIdentifierPart(ch)) {
                break;
            }

            ++index;
            if (ch === '\\' && index < length) {
                ch = source[index];
                if (ch === 'u') {
                    ++index;
                    restore = index;
                    ch = scanHexEscape('u');
                    if (ch) {
                        flags += ch;
                        str += '\\u';
                        for (; restore < index; ++restore) {
                            str += source[restore];
                        }
                    } else {
                        index = restore;
                        flags += 'u';
                        str += '\\u';
                    }
                } else {
                    str += '\\';
                }
            } else {
                flags += ch;
                str += ch;
            }
        }

        try {
            value = new RegExp(pattern, flags);
        } catch (e) {
            throwError({}, Messages.InvalidRegExp);
        }

        return {
            literal: str,
            value: value,
            range: [start, index]
        };
    }

    function isIdentifierName(token) {
        return token.type === Token.Identifier ||
            token.type === Token.Keyword ||
            token.type === Token.BooleanLiteral ||
            token.type === Token.NullLiteral;
    }

    function advance() {
        var ch, token;

        skipComment();

        if (index >= length) {
            return {
                type: Token.EOF,
                lineNumber: lineNumber,
                lineStart: lineStart,
                range: [index, index]
            };
        }

        token = scanPunctuator();
        if (typeof token !== 'undefined') {
            return token;
        }

        ch = source[index];

        if (ch === '\'' || ch === '"') {
            return scanStringLiteral();
        }

        if (ch === '.' || isDecimalDigit(ch)) {
            return scanNumericLiteral();
        }

        token = scanIdentifier();
        if (typeof token !== 'undefined') {
            return token;
        }

        throwError({}, Messages.UnexpectedToken, 'ILLEGAL');
    }

    function lex() {
        var token;

        if (buffer) {
            index = buffer.range[1];
            lineNumber = buffer.lineNumber;
            lineStart = buffer.lineStart;
            token = buffer;
            buffer = null;
            return token;
        }

        buffer = null;
        return advance();
    }

    function lookahead() {
        var pos, line, start;

        if (buffer !== null) {
            return buffer;
        }

        pos = index;
        line = lineNumber;
        start = lineStart;
        buffer = advance();
        index = pos;
        lineNumber = line;
        lineStart = start;

        return buffer;
    }

    // Return true if there is a line terminator before the next token.

    function peekLineTerminator() {
        var pos, line, start, found;

        pos = index;
        line = lineNumber;
        start = lineStart;
        skipComment();
        found = lineNumber !== line;
        index = pos;
        lineNumber = line;
        lineStart = start;

        return found;
    }

    // Throw an exception

    function throwError(token, messageFormat) {
        var error,
            args = Array.prototype.slice.call(arguments, 2),
            msg = messageFormat.replace(
                /%(\d)/g,
                function (whole, index) {
                    return args[index] || '';
                }
            );

        if (typeof token.lineNumber === 'number') {
            error = new Error('Line ' + token.lineNumber + ': ' + msg);
            error.index = token.range[0];
            error.lineNumber = token.lineNumber;
            error.column = token.range[0] - lineStart + 1;
        } else {
            error = new Error('Line ' + lineNumber + ': ' + msg);
            error.index = index;
            error.lineNumber = lineNumber;
            error.column = index - lineStart + 1;
        }

        throw error;
    }

    function throwErrorTolerant() {
        try {
            throwError.apply(null, arguments);
        } catch (e) {
            if (extra.errors) {
                extra.errors.push(e);
            } else {
                throw e;
            }
        }
    }


    // Throw an exception because of the token.

    function throwUnexpected(token) {
        if (token.type === Token.EOF) {
            throwError(token, Messages.UnexpectedEOS);
        }

        if (token.type === Token.NumericLiteral) {
            throwError(token, Messages.UnexpectedNumber);
        }

        if (token.type === Token.StringLiteral) {
            throwError(token, Messages.UnexpectedString);
        }

        if (token.type === Token.Identifier) {
            throwError(token, Messages.UnexpectedIdentifier);
        }

        if (token.type === Token.Keyword) {
            if (isFutureReservedWord(token.value)) {
                throwError(token, Messages.UnexpectedReserved);
            } else if (strict && isStrictModeReservedWord(token.value)) {
                throwErrorTolerant(token, Messages.StrictReservedWord);
                return;
            }
            throwError(token, Messages.UnexpectedToken, token.value);
        }

        // BooleanLiteral, NullLiteral, or Punctuator.
        throwError(token, Messages.UnexpectedToken, token.value);
    }

    // Expect the next token to match the specified punctuator.
    // If not, an exception will be thrown.

    function expect(value) {
        var token = lex();
        if (token.type !== Token.Punctuator || token.value !== value) {
            throwUnexpected(token);
        }
    }

    // Expect the next token to match the specified keyword.
    // If not, an exception will be thrown.

    function expectKeyword(keyword) {
        var token = lex();
        if (token.type !== Token.Keyword || token.value !== keyword) {
            throwUnexpected(token);
        }
    }

    // Return true if the next token matches the specified punctuator.

    function match(value) {
        var token = lookahead();
        return token.type === Token.Punctuator && token.value === value;
    }

    // Return true if the next token matches the specified keyword

    function matchKeyword(keyword) {
        var token = lookahead();
        return token.type === Token.Keyword && token.value === keyword;
    }

    // Return true if the next token is an assignment operator

    function matchAssign() {
        var token = lookahead(),
            op = token.value;

        if (token.type !== Token.Punctuator) {
            return false;
        }
        return op === '=' ||
            op === '*=' ||
            op === '/=' ||
            op === '%=' ||
            op === '+=' ||
            op === '-=' ||
            op === '<<=' ||
            op === '>>=' ||
            op === '>>>=' ||
            op === '&=' ||
            op === '^=' ||
            op === '|=';
    }

    function consumeSemicolon() {
        var token, line;

        // Catch the very common case first.
        if (source[index] === ';') {
            lex();
            return;
        }

        line = lineNumber;
        skipComment();
        if (lineNumber !== line) {
            return;
        }

        if (match(';')) {
            lex();
            return;
        }

        token = lookahead();
        if (token.type !== Token.EOF && !match('}')) {
            throwUnexpected(token);
        }
    }

    // Return true if provided expression is LeftHandSideExpression

    function isLeftHandSide(expr) {
        return expr.type === Syntax.Identifier || expr.type === Syntax.MemberExpression;
    }

    // 11.1.4 Array Initialiser

    function parseArrayInitialiser() {
        var elements = [];

        expect('[');

        while (!match(']')) {
            if (match(',')) {
                lex();
                elements.push(null);
            } else {
                elements.push(parseAssignmentExpression());

                if (!match(']')) {
                    expect(',');
                }
            }
        }

        expect(']');

        return {
            type: Syntax.ArrayExpression,
            elements: elements
        };
    }

    // 11.1.5 Object Initialiser

    function parsePropertyFunction(param, first) {
        var previousStrict, body;

        previousStrict = strict;
        body = parseFunctionSourceElements();
        if (first && strict && isRestrictedWord(param[0].name)) {
            throwErrorTolerant(first, Messages.StrictParamName);
        }
        strict = previousStrict;

        return {
            type: Syntax.FunctionExpression,
            id: null,
            params: param,
            defaults: [],
            body: body,
            rest: null,
            generator: false,
            expression: false
        };
    }

    function parseObjectPropertyKey() {
        var token = lex();

        // Note: This function is called only from parseObjectProperty(), where
        // EOF and Punctuator tokens are already filtered out.

        if (token.type === Token.StringLiteral || token.type === Token.NumericLiteral) {
            if (strict && token.octal) {
                throwErrorTolerant(token, Messages.StrictOctalLiteral);
            }
            return createLiteral(token);
        }

        return {
            type: Syntax.Identifier,
            name: token.value
        };
    }

    function parseObjectProperty() {
        var token, key, id, param;

        token = lookahead();

        if (token.type === Token.Identifier) {

            id = parseObjectPropertyKey();

            // Property Assignment: Getter and Setter.

            if (token.value === 'get' && !match(':')) {
                key = parseObjectPropertyKey();
                expect('(');
                expect(')');
                return {
                    type: Syntax.Property,
                    key: key,
                    value: parsePropertyFunction([]),
                    kind: 'get'
                };
            } else if (token.value === 'set' && !match(':')) {
                key = parseObjectPropertyKey();
                expect('(');
                token = lookahead();
                if (token.type !== Token.Identifier) {
                    expect(')');
                    throwErrorTolerant(token, Messages.UnexpectedToken, token.value);
                    return {
                        type: Syntax.Property,
                        key: key,
                        value: parsePropertyFunction([]),
                        kind: 'set'
                    };
                } else {
                    param = [ parseVariableIdentifier() ];
                    expect(')');
                    return {
                        type: Syntax.Property,
                        key: key,
                        value: parsePropertyFunction(param, token),
                        kind: 'set'
                    };
                }
            } else {
                expect(':');
                return {
                    type: Syntax.Property,
                    key: id,
                    value: parseAssignmentExpression(),
                    kind: 'init'
                };
            }
        } else if (token.type === Token.EOF || token.type === Token.Punctuator) {
            throwUnexpected(token);
        } else {
            key = parseObjectPropertyKey();
            expect(':');
            return {
                type: Syntax.Property,
                key: key,
                value: parseAssignmentExpression(),
                kind: 'init'
            };
        }
    }

    function parseObjectInitialiser() {
        var properties = [], property, name, kind, map = {}, toString = String;

        expect('{');

        while (!match('}')) {
            property = parseObjectProperty();

            if (property.key.type === Syntax.Identifier) {
                name = property.key.name;
            } else {
                name = toString(property.key.value);
            }
            kind = (property.kind === 'init') ? PropertyKind.Data : (property.kind === 'get') ? PropertyKind.Get : PropertyKind.Set;
            if (Object.prototype.hasOwnProperty.call(map, name)) {
                if (map[name] === PropertyKind.Data) {
                    if (strict && kind === PropertyKind.Data) {
                        throwErrorTolerant({}, Messages.StrictDuplicateProperty);
                    } else if (kind !== PropertyKind.Data) {
                        throwErrorTolerant({}, Messages.AccessorDataProperty);
                    }
                } else {
                    if (kind === PropertyKind.Data) {
                        throwErrorTolerant({}, Messages.AccessorDataProperty);
                    } else if (map[name] & kind) {
                        throwErrorTolerant({}, Messages.AccessorGetSet);
                    }
                }
                map[name] |= kind;
            } else {
                map[name] = kind;
            }

            properties.push(property);

            if (!match('}')) {
                expect(',');
            }
        }

        expect('}');

        return {
            type: Syntax.ObjectExpression,
            properties: properties
        };
    }

    // 11.1.6 The Grouping Operator

    function parseGroupExpression() {
        var expr;

        expect('(');

        expr = parseExpression();

        expect(')');

        return expr;
    }


    // 11.1 Primary Expressions

    function parsePrimaryExpression() {
        var token = lookahead(),
            type = token.type;

        if (type === Token.Identifier) {
            return {
                type: Syntax.Identifier,
                name: lex().value
            };
        }

        if (type === Token.StringLiteral || type === Token.NumericLiteral) {
            if (strict && token.octal) {
                throwErrorTolerant(token, Messages.StrictOctalLiteral);
            }
            return createLiteral(lex());
        }

        if (type === Token.Keyword) {
            if (matchKeyword('this')) {
                lex();
                return {
                    type: Syntax.ThisExpression
                };
            }

            if (matchKeyword('function')) {
                return parseFunctionExpression();
            }
        }

        if (type === Token.BooleanLiteral) {
            lex();
            token.value = (token.value === 'true');
            return createLiteral(token);
        }

        if (type === Token.NullLiteral) {
            lex();
            token.value = null;
            return createLiteral(token);
        }

        if (match('[')) {
            return parseArrayInitialiser();
        }

        if (match('{')) {
            return parseObjectInitialiser();
        }

        if (match('(')) {
            return parseGroupExpression();
        }

        if (match('/') || match('/=')) {
            return createLiteral(scanRegExp());
        }

        return throwUnexpected(lex());
    }

    // 11.2 Left-Hand-Side Expressions

    function parseArguments() {
        var args = [];

        expect('(');

        if (!match(')')) {
            while (index < length) {
                args.push(parseAssignmentExpression());
                if (match(')')) {
                    break;
                }
                expect(',');
            }
        }

        expect(')');

        return args;
    }

    function parseNonComputedProperty() {
        var token = lex();

        if (!isIdentifierName(token)) {
            throwUnexpected(token);
        }

        return {
            type: Syntax.Identifier,
            name: token.value
        };
    }

    function parseNonComputedMember() {
        expect('.');

        return parseNonComputedProperty();
    }

    function parseComputedMember() {
        var expr;

        expect('[');

        expr = parseExpression();

        expect(']');

        return expr;
    }

    function parseNewExpression() {
        var expr;

        expectKeyword('new');

        expr = {
            type: Syntax.NewExpression,
            callee: parseLeftHandSideExpression(),
            'arguments': []
        };

        if (match('(')) {
            expr['arguments'] = parseArguments();
        }

        return expr;
    }

    function parseLeftHandSideExpressionAllowCall() {
        var expr;

        expr = matchKeyword('new') ? parseNewExpression() : parsePrimaryExpression();

        while (match('.') || match('[') || match('(')) {
            if (match('(')) {
                expr = {
                    type: Syntax.CallExpression,
                    callee: expr,
                    'arguments': parseArguments()
                };
            } else if (match('[')) {
                expr = {
                    type: Syntax.MemberExpression,
                    computed: true,
                    object: expr,
                    property: parseComputedMember()
                };
            } else {
                expr = {
                    type: Syntax.MemberExpression,
                    computed: false,
                    object: expr,
                    property: parseNonComputedMember()
                };
            }
        }

        return expr;
    }


    function parseLeftHandSideExpression() {
        var expr;

        expr = matchKeyword('new') ? parseNewExpression() : parsePrimaryExpression();

        while (match('.') || match('[')) {
            if (match('[')) {
                expr = {
                    type: Syntax.MemberExpression,
                    computed: true,
                    object: expr,
                    property: parseComputedMember()
                };
            } else {
                expr = {
                    type: Syntax.MemberExpression,
                    computed: false,
                    object: expr,
                    property: parseNonComputedMember()
                };
            }
        }

        return expr;
    }

    // 11.3 Postfix Expressions

    function parsePostfixExpression() {
        var expr = parseLeftHandSideExpressionAllowCall(), token;

        token = lookahead();
        if (token.type !== Token.Punctuator) {
            return expr;
        }

        if ((match('++') || match('--')) && !peekLineTerminator()) {
            // 11.3.1, 11.3.2
            if (strict && expr.type === Syntax.Identifier && isRestrictedWord(expr.name)) {
                throwErrorTolerant({}, Messages.StrictLHSPostfix);
            }

            if (!isLeftHandSide(expr)) {
                throwError({}, Messages.InvalidLHSInAssignment);
            }

            expr = {
                type: Syntax.UpdateExpression,
                operator: lex().value,
                argument: expr,
                prefix: false
            };
        }

        return expr;
    }

    // 11.4 Unary Operators

    function parseUnaryExpression() {
        var token, expr;

        token = lookahead();
        if (token.type !== Token.Punctuator && token.type !== Token.Keyword) {
            return parsePostfixExpression();
        }

        if (match('++') || match('--')) {
            token = lex();
            expr = parseUnaryExpression();
            // 11.4.4, 11.4.5
            if (strict && expr.type === Syntax.Identifier && isRestrictedWord(expr.name)) {
                throwErrorTolerant({}, Messages.StrictLHSPrefix);
            }

            if (!isLeftHandSide(expr)) {
                throwError({}, Messages.InvalidLHSInAssignment);
            }

            expr = {
                type: Syntax.UpdateExpression,
                operator: token.value,
                argument: expr,
                prefix: true
            };
            return expr;
        }

        if (match('+') || match('-') || match('~') || match('!')) {
            expr = {
                type: Syntax.UnaryExpression,
                operator: lex().value,
                argument: parseUnaryExpression(),
                prefix: true
            };
            return expr;
        }

        if (matchKeyword('delete') || matchKeyword('void') || matchKeyword('typeof')) {
            expr = {
                type: Syntax.UnaryExpression,
                operator: lex().value,
                argument: parseUnaryExpression(),
                prefix: true
            };
            if (strict && expr.operator === 'delete' && expr.argument.type === Syntax.Identifier) {
                throwErrorTolerant({}, Messages.StrictDelete);
            }
            return expr;
        }

        return parsePostfixExpression();
    }

    // 11.5 Multiplicative Operators

    function parseMultiplicativeExpression() {
        var expr = parseUnaryExpression();

        while (match('*') || match('/') || match('%')) {
            expr = {
                type: Syntax.BinaryExpression,
                operator: lex().value,
                left: expr,
                right: parseUnaryExpression()
            };
        }

        return expr;
    }

    // 11.6 Additive Operators

    function parseAdditiveExpression() {
        var expr = parseMultiplicativeExpression();

        while (match('+') || match('-')) {
            expr = {
                type: Syntax.BinaryExpression,
                operator: lex().value,
                left: expr,
                right: parseMultiplicativeExpression()
            };
        }

        return expr;
    }

    // 11.7 Bitwise Shift Operators

    function parseShiftExpression() {
        var expr = parseAdditiveExpression();

        while (match('<<') || match('>>') || match('>>>')) {
            expr = {
                type: Syntax.BinaryExpression,
                operator: lex().value,
                left: expr,
                right: parseAdditiveExpression()
            };
        }

        return expr;
    }
    // 11.8 Relational Operators

    function parseRelationalExpression() {
        var expr, previousAllowIn;

        previousAllowIn = state.allowIn;
        state.allowIn = true;

        expr = parseShiftExpression();

        while (match('<') || match('>') || match('<=') || match('>=') || (previousAllowIn && matchKeyword('in')) || matchKeyword('instanceof')) {
            expr = {
                type: Syntax.BinaryExpression,
                operator: lex().value,
                left: expr,
                right: parseShiftExpression()
            };
        }

        state.allowIn = previousAllowIn;
        return expr;
    }

    // 11.9 Equality Operators

    function parseEqualityExpression() {
        var expr = parseRelationalExpression();

        while (match('==') || match('!=') || match('===') || match('!==')) {
            expr = {
                type: Syntax.BinaryExpression,
                operator: lex().value,
                left: expr,
                right: parseRelationalExpression()
            };
        }

        return expr;
    }

    // 11.10 Binary Bitwise Operators

    function parseBitwiseANDExpression() {
        var expr = parseEqualityExpression();

        while (match('&')) {
            lex();
            expr = {
                type: Syntax.BinaryExpression,
                operator: '&',
                left: expr,
                right: parseEqualityExpression()
            };
        }

        return expr;
    }

    function parseBitwiseXORExpression() {
        var expr = parseBitwiseANDExpression();

        while (match('^')) {
            lex();
            expr = {
                type: Syntax.BinaryExpression,
                operator: '^',
                left: expr,
                right: parseBitwiseANDExpression()
            };
        }

        return expr;
    }

    function parseBitwiseORExpression() {
        var expr = parseBitwiseXORExpression();

        while (match('|')) {
            lex();
            expr = {
                type: Syntax.BinaryExpression,
                operator: '|',
                left: expr,
                right: parseBitwiseXORExpression()
            };
        }

        return expr;
    }

    // 11.11 Binary Logical Operators

    function parseLogicalANDExpression() {
        var expr = parseBitwiseORExpression();

        while (match('&&')) {
            lex();
            expr = {
                type: Syntax.LogicalExpression,
                operator: '&&',
                left: expr,
                right: parseBitwiseORExpression()
            };
        }

        return expr;
    }

    function parseLogicalORExpression() {
        var expr = parseLogicalANDExpression();

        while (match('||')) {
            lex();
            expr = {
                type: Syntax.LogicalExpression,
                operator: '||',
                left: expr,
                right: parseLogicalANDExpression()
            };
        }

        return expr;
    }

    // 11.12 Conditional Operator

    function parseConditionalExpression() {
        var expr, previousAllowIn, consequent;

        expr = parseLogicalORExpression();

        if (match('?')) {
            lex();
            previousAllowIn = state.allowIn;
            state.allowIn = true;
            consequent = parseAssignmentExpression();
            state.allowIn = previousAllowIn;
            expect(':');

            expr = {
                type: Syntax.ConditionalExpression,
                test: expr,
                consequent: consequent,
                alternate: parseAssignmentExpression()
            };
        }

        return expr;
    }

    // 11.13 Assignment Operators

    function parseAssignmentExpression() {
        var token, expr;

        token = lookahead();
        expr = parseConditionalExpression();

        if (matchAssign()) {
            // LeftHandSideExpression
            if (!isLeftHandSide(expr)) {
                throwError({}, Messages.InvalidLHSInAssignment);
            }

            // 11.13.1
            if (strict && expr.type === Syntax.Identifier && isRestrictedWord(expr.name)) {
                throwErrorTolerant(token, Messages.StrictLHSAssignment);
            }

            expr = {
                type: Syntax.AssignmentExpression,
                operator: lex().value,
                left: expr,
                right: parseAssignmentExpression()
            };
        }

        return expr;
    }

    // 11.14 Comma Operator

    function parseExpression() {
        var expr = parseAssignmentExpression();

        if (match(',')) {
            expr = {
                type: Syntax.SequenceExpression,
                expressions: [ expr ]
            };

            while (index < length) {
                if (!match(',')) {
                    break;
                }
                lex();
                expr.expressions.push(parseAssignmentExpression());
            }

        }
        return expr;
    }

    // 12.1 Block

    function parseStatementList() {
        var list = [],
            statement;

        while (index < length) {
            if (match('}')) {
                break;
            }
            statement = parseSourceElement();
            if (typeof statement === 'undefined') {
                break;
            }
            list.push(statement);
        }

        return list;
    }

    function parseBlock() {
        var block;

        expect('{');

        block = parseStatementList();

        expect('}');

        return {
            type: Syntax.BlockStatement,
            body: block
        };
    }

    // 12.2 Variable Statement

    function parseVariableIdentifier() {
        var token = lex();

        if (token.type !== Token.Identifier) {
            throwUnexpected(token);
        }

        return {
            type: Syntax.Identifier,
            name: token.value
        };
    }

    function parseVariableDeclaration(kind) {
        var id = parseVariableIdentifier(),
            init = null;

        // 12.2.1
        if (strict && isRestrictedWord(id.name)) {
            throwErrorTolerant({}, Messages.StrictVarName);
        }

        if (kind === 'const') {
            expect('=');
            init = parseAssignmentExpression();
        } else if (match('=')) {
            lex();
            init = parseAssignmentExpression();
        }

        return {
            type: Syntax.VariableDeclarator,
            id: id,
            init: init
        };
    }

    function parseVariableDeclarationList(kind) {
        var list = [];

        do {
            list.push(parseVariableDeclaration(kind));
            if (!match(',')) {
                break;
            }
            lex();
        } while (index < length);

        return list;
    }

    function parseVariableStatement() {
        var declarations;

        expectKeyword('var');

        declarations = parseVariableDeclarationList();

        consumeSemicolon();

        return {
            type: Syntax.VariableDeclaration,
            declarations: declarations,
            kind: 'var'
        };
    }

    // kind may be `const` or `let`
    // Both are experimental and not in the specification yet.
    // see http://wiki.ecmascript.org/doku.php?id=harmony:const
    // and http://wiki.ecmascript.org/doku.php?id=harmony:let
    function parseConstLetDeclaration(kind) {
        var declarations;

        expectKeyword(kind);

        declarations = parseVariableDeclarationList(kind);

        consumeSemicolon();

        return {
            type: Syntax.VariableDeclaration,
            declarations: declarations,
            kind: kind
        };
    }

    // 12.3 Empty Statement

    function parseEmptyStatement() {
        expect(';');

        return {
            type: Syntax.EmptyStatement
        };
    }

    // 12.4 Expression Statement

    function parseExpressionStatement() {
        var expr = parseExpression();

        consumeSemicolon();

        return {
            type: Syntax.ExpressionStatement,
            expression: expr
        };
    }

    // 12.5 If statement

    function parseIfStatement() {
        var test, consequent, alternate;

        expectKeyword('if');

        expect('(');

        test = parseExpression();

        expect(')');

        consequent = parseStatement();

        if (matchKeyword('else')) {
            lex();
            alternate = parseStatement();
        } else {
            alternate = null;
        }

        return {
            type: Syntax.IfStatement,
            test: test,
            consequent: consequent,
            alternate: alternate
        };
    }

    // 12.6 Iteration Statements

    function parseDoWhileStatement() {
        var body, test, oldInIteration;

        expectKeyword('do');

        oldInIteration = state.inIteration;
        state.inIteration = true;

        body = parseStatement();

        state.inIteration = oldInIteration;

        expectKeyword('while');

        expect('(');

        test = parseExpression();

        expect(')');

        if (match(';')) {
            lex();
        }

        return {
            type: Syntax.DoWhileStatement,
            body: body,
            test: test
        };
    }

    function parseWhileStatement() {
        var test, body, oldInIteration;

        expectKeyword('while');

        expect('(');

        test = parseExpression();

        expect(')');

        oldInIteration = state.inIteration;
        state.inIteration = true;

        body = parseStatement();

        state.inIteration = oldInIteration;

        return {
            type: Syntax.WhileStatement,
            test: test,
            body: body
        };
    }

    function parseForVariableDeclaration() {
        var token = lex();

        return {
            type: Syntax.VariableDeclaration,
            declarations: parseVariableDeclarationList(),
            kind: token.value
        };
    }

    function parseForStatement() {
        var init, test, update, left, right, body, oldInIteration;

        init = test = update = null;

        expectKeyword('for');

        expect('(');

        if (match(';')) {
            lex();
        } else {
            if (matchKeyword('var') || matchKeyword('let')) {
                state.allowIn = false;
                init = parseForVariableDeclaration();
                state.allowIn = true;

                if (init.declarations.length === 1 && matchKeyword('in')) {
                    lex();
                    left = init;
                    right = parseExpression();
                    init = null;
                }
            } else {
                state.allowIn = false;
                init = parseExpression();
                state.allowIn = true;

                if (matchKeyword('in')) {
                    // LeftHandSideExpression
                    if (!isLeftHandSide(init)) {
                        throwError({}, Messages.InvalidLHSInForIn);
                    }

                    lex();
                    left = init;
                    right = parseExpression();
                    init = null;
                }
            }

            if (typeof left === 'undefined') {
                expect(';');
            }
        }

        if (typeof left === 'undefined') {

            if (!match(';')) {
                test = parseExpression();
            }
            expect(';');

            if (!match(')')) {
                update = parseExpression();
            }
        }

        expect(')');

        oldInIteration = state.inIteration;
        state.inIteration = true;

        body = parseStatement();

        state.inIteration = oldInIteration;

        if (typeof left === 'undefined') {
            return {
                type: Syntax.ForStatement,
                init: init,
                test: test,
                update: update,
                body: body
            };
        }

        return {
            type: Syntax.ForInStatement,
            left: left,
            right: right,
            body: body,
            each: false
        };
    }

    // 12.7 The continue statement

    function parseContinueStatement() {
        var token, label = null;

        expectKeyword('continue');

        // Optimize the most common form: 'continue;'.
        if (source[index] === ';') {
            lex();

            if (!state.inIteration) {
                throwError({}, Messages.IllegalContinue);
            }

            return {
                type: Syntax.ContinueStatement,
                label: null
            };
        }

        if (peekLineTerminator()) {
            if (!state.inIteration) {
                throwError({}, Messages.IllegalContinue);
            }

            return {
                type: Syntax.ContinueStatement,
                label: null
            };
        }

        token = lookahead();
        if (token.type === Token.Identifier) {
            label = parseVariableIdentifier();

            if (!Object.prototype.hasOwnProperty.call(state.labelSet, label.name)) {
                throwError({}, Messages.UnknownLabel, label.name);
            }
        }

        consumeSemicolon();

        if (label === null && !state.inIteration) {
            throwError({}, Messages.IllegalContinue);
        }

        return {
            type: Syntax.ContinueStatement,
            label: label
        };
    }

    // 12.8 The break statement

    function parseBreakStatement() {
        var token, label = null;

        expectKeyword('break');

        // Optimize the most common form: 'break;'.
        if (source[index] === ';') {
            lex();

            if (!(state.inIteration || state.inSwitch)) {
                throwError({}, Messages.IllegalBreak);
            }

            return {
                type: Syntax.BreakStatement,
                label: null
            };
        }

        if (peekLineTerminator()) {
            if (!(state.inIteration || state.inSwitch)) {
                throwError({}, Messages.IllegalBreak);
            }

            return {
                type: Syntax.BreakStatement,
                label: null
            };
        }

        token = lookahead();
        if (token.type === Token.Identifier) {
            label = parseVariableIdentifier();

            if (!Object.prototype.hasOwnProperty.call(state.labelSet, label.name)) {
                throwError({}, Messages.UnknownLabel, label.name);
            }
        }

        consumeSemicolon();

        if (label === null && !(state.inIteration || state.inSwitch)) {
            throwError({}, Messages.IllegalBreak);
        }

        return {
            type: Syntax.BreakStatement,
            label: label
        };
    }

    // 12.9 The return statement

    function parseReturnStatement() {
        var token, argument = null;

        expectKeyword('return');

        if (!state.inFunctionBody) {
            throwErrorTolerant({}, Messages.IllegalReturn);
        }

        // 'return' followed by a space and an identifier is very common.
        if (source[index] === ' ') {
            if (isIdentifierStart(source[index + 1])) {
                argument = parseExpression();
                consumeSemicolon();
                return {
                    type: Syntax.ReturnStatement,
                    argument: argument
                };
            }
        }

        if (peekLineTerminator()) {
            return {
                type: Syntax.ReturnStatement,
                argument: null
            };
        }

        if (!match(';')) {
            token = lookahead();
            if (!match('}') && token.type !== Token.EOF) {
                argument = parseExpression();
            }
        }

        consumeSemicolon();

        return {
            type: Syntax.ReturnStatement,
            argument: argument
        };
    }

    // 12.10 The with statement

    function parseWithStatement() {
        var object, body;

        if (strict) {
            throwErrorTolerant({}, Messages.StrictModeWith);
        }

        expectKeyword('with');

        expect('(');

        object = parseExpression();

        expect(')');

        body = parseStatement();

        return {
            type: Syntax.WithStatement,
            object: object,
            body: body
        };
    }

    // 12.10 The swith statement

    function parseSwitchCase() {
        var test,
            consequent = [],
            statement;

        if (matchKeyword('default')) {
            lex();
            test = null;
        } else {
            expectKeyword('case');
            test = parseExpression();
        }
        expect(':');

        while (index < length) {
            if (match('}') || matchKeyword('default') || matchKeyword('case')) {
                break;
            }
            statement = parseStatement();
            if (typeof statement === 'undefined') {
                break;
            }
            consequent.push(statement);
        }

        return {
            type: Syntax.SwitchCase,
            test: test,
            consequent: consequent
        };
    }

    function parseSwitchStatement() {
        var discriminant, cases, clause, oldInSwitch, defaultFound;

        expectKeyword('switch');

        expect('(');

        discriminant = parseExpression();

        expect(')');

        expect('{');

        if (match('}')) {
            lex();
            return {
                type: Syntax.SwitchStatement,
                discriminant: discriminant
            };
        }

        cases = [];

        oldInSwitch = state.inSwitch;
        state.inSwitch = true;
        defaultFound = false;

        while (index < length) {
            if (match('}')) {
                break;
            }
            clause = parseSwitchCase();
            if (clause.test === null) {
                if (defaultFound) {
                    throwError({}, Messages.MultipleDefaultsInSwitch);
                }
                defaultFound = true;
            }
            cases.push(clause);
        }

        state.inSwitch = oldInSwitch;

        expect('}');

        return {
            type: Syntax.SwitchStatement,
            discriminant: discriminant,
            cases: cases
        };
    }

    // 12.13 The throw statement

    function parseThrowStatement() {
        var argument;

        expectKeyword('throw');

        if (peekLineTerminator()) {
            throwError({}, Messages.NewlineAfterThrow);
        }

        argument = parseExpression();

        consumeSemicolon();

        return {
            type: Syntax.ThrowStatement,
            argument: argument
        };
    }

    // 12.14 The try statement

    function parseCatchClause() {
        var param;

        expectKeyword('catch');

        expect('(');
        if (match(')')) {
            throwUnexpected(lookahead());
        }

        param = parseVariableIdentifier();
        // 12.14.1
        if (strict && isRestrictedWord(param.name)) {
            throwErrorTolerant({}, Messages.StrictCatchVariable);
        }

        expect(')');

        return {
            type: Syntax.CatchClause,
            param: param,
            body: parseBlock()
        };
    }

    function parseTryStatement() {
        var block, handlers = [], finalizer = null;

        expectKeyword('try');

        block = parseBlock();

        if (matchKeyword('catch')) {
            handlers.push(parseCatchClause());
        }

        if (matchKeyword('finally')) {
            lex();
            finalizer = parseBlock();
        }

        if (handlers.length === 0 && !finalizer) {
            throwError({}, Messages.NoCatchOrFinally);
        }

        return {
            type: Syntax.TryStatement,
            block: block,
            guardedHandlers: [],
            handlers: handlers,
            finalizer: finalizer
        };
    }

    // 12.15 The debugger statement

    function parseDebuggerStatement() {
        expectKeyword('debugger');

        consumeSemicolon();

        return {
            type: Syntax.DebuggerStatement
        };
    }

    // 12 Statements

    function parseStatement() {
        var token = lookahead(),
            expr,
            labeledBody;

        if (token.type === Token.EOF) {
            throwUnexpected(token);
        }

        if (token.type === Token.Punctuator) {
            switch (token.value) {
            case ';':
                return parseEmptyStatement();
            case '{':
                return parseBlock();
            case '(':
                return parseExpressionStatement();
            default:
                break;
            }
        }

        if (token.type === Token.Keyword) {
            switch (token.value) {
            case 'break':
                return parseBreakStatement();
            case 'continue':
                return parseContinueStatement();
            case 'debugger':
                return parseDebuggerStatement();
            case 'do':
                return parseDoWhileStatement();
            case 'for':
                return parseForStatement();
            case 'function':
                return parseFunctionDeclaration();
            case 'if':
                return parseIfStatement();
            case 'return':
                return parseReturnStatement();
            case 'switch':
                return parseSwitchStatement();
            case 'throw':
                return parseThrowStatement();
            case 'try':
                return parseTryStatement();
            case 'var':
                return parseVariableStatement();
            case 'while':
                return parseWhileStatement();
            case 'with':
                return parseWithStatement();
            default:
                break;
            }
        }

        expr = parseExpression();

        // 12.12 Labelled Statements
        if ((expr.type === Syntax.Identifier) && match(':')) {
            lex();

            if (Object.prototype.hasOwnProperty.call(state.labelSet, expr.name)) {
                throwError({}, Messages.Redeclaration, 'Label', expr.name);
            }

            state.labelSet[expr.name] = true;
            labeledBody = parseStatement();
            delete state.labelSet[expr.name];

            return {
                type: Syntax.LabeledStatement,
                label: expr,
                body: labeledBody
            };
        }

        consumeSemicolon();

        return {
            type: Syntax.ExpressionStatement,
            expression: expr
        };
    }

    // 13 Function Definition

    function parseFunctionSourceElements() {
        var sourceElement, sourceElements = [], token, directive, firstRestricted,
            oldLabelSet, oldInIteration, oldInSwitch, oldInFunctionBody;

        expect('{');

        while (index < length) {
            token = lookahead();
            if (token.type !== Token.StringLiteral) {
                break;
            }

            sourceElement = parseSourceElement();
            sourceElements.push(sourceElement);
            if (sourceElement.expression.type !== Syntax.Literal) {
                // this is not directive
                break;
            }
            directive = sliceSource(token.range[0] + 1, token.range[1] - 1);
            if (directive === 'use strict') {
                strict = true;
                if (firstRestricted) {
                    throwErrorTolerant(firstRestricted, Messages.StrictOctalLiteral);
                }
            } else {
                if (!firstRestricted && token.octal) {
                    firstRestricted = token;
                }
            }
        }

        oldLabelSet = state.labelSet;
        oldInIteration = state.inIteration;
        oldInSwitch = state.inSwitch;
        oldInFunctionBody = state.inFunctionBody;

        state.labelSet = {};
        state.inIteration = false;
        state.inSwitch = false;
        state.inFunctionBody = true;

        while (index < length) {
            if (match('}')) {
                break;
            }
            sourceElement = parseSourceElement();
            if (typeof sourceElement === 'undefined') {
                break;
            }
            sourceElements.push(sourceElement);
        }

        expect('}');

        state.labelSet = oldLabelSet;
        state.inIteration = oldInIteration;
        state.inSwitch = oldInSwitch;
        state.inFunctionBody = oldInFunctionBody;

        return {
            type: Syntax.BlockStatement,
            body: sourceElements
        };
    }

    function parseFunctionDeclaration() {
        var id, param, params = [], body, token, stricted, firstRestricted, message, previousStrict, paramSet;

        expectKeyword('function');
        token = lookahead();
        id = parseVariableIdentifier();
        if (strict) {
            if (isRestrictedWord(token.value)) {
                throwErrorTolerant(token, Messages.StrictFunctionName);
            }
        } else {
            if (isRestrictedWord(token.value)) {
                firstRestricted = token;
                message = Messages.StrictFunctionName;
            } else if (isStrictModeReservedWord(token.value)) {
                firstRestricted = token;
                message = Messages.StrictReservedWord;
            }
        }

        expect('(');

        if (!match(')')) {
            paramSet = {};
            while (index < length) {
                token = lookahead();
                param = parseVariableIdentifier();
                if (strict) {
                    if (isRestrictedWord(token.value)) {
                        stricted = token;
                        message = Messages.StrictParamName;
                    }
                    if (Object.prototype.hasOwnProperty.call(paramSet, token.value)) {
                        stricted = token;
                        message = Messages.StrictParamDupe;
                    }
                } else if (!firstRestricted) {
                    if (isRestrictedWord(token.value)) {
                        firstRestricted = token;
                        message = Messages.StrictParamName;
                    } else if (isStrictModeReservedWord(token.value)) {
                        firstRestricted = token;
                        message = Messages.StrictReservedWord;
                    } else if (Object.prototype.hasOwnProperty.call(paramSet, token.value)) {
                        firstRestricted = token;
                        message = Messages.StrictParamDupe;
                    }
                }
                params.push(param);
                paramSet[param.name] = true;
                if (match(')')) {
                    break;
                }
                expect(',');
            }
        }

        expect(')');

        previousStrict = strict;
        body = parseFunctionSourceElements();
        if (strict && firstRestricted) {
            throwError(firstRestricted, message);
        }
        if (strict && stricted) {
            throwErrorTolerant(stricted, message);
        }
        strict = previousStrict;

        return {
            type: Syntax.FunctionDeclaration,
            id: id,
            params: params,
            defaults: [],
            body: body,
            rest: null,
            generator: false,
            expression: false
        };
    }

    function parseFunctionExpression() {
        var token, id = null, stricted, firstRestricted, message, param, params = [], body, previousStrict, paramSet;

        expectKeyword('function');

        if (!match('(')) {
            token = lookahead();
            id = parseVariableIdentifier();
            if (strict) {
                if (isRestrictedWord(token.value)) {
                    throwErrorTolerant(token, Messages.StrictFunctionName);
                }
            } else {
                if (isRestrictedWord(token.value)) {
                    firstRestricted = token;
                    message = Messages.StrictFunctionName;
                } else if (isStrictModeReservedWord(token.value)) {
                    firstRestricted = token;
                    message = Messages.StrictReservedWord;
                }
            }
        }

        expect('(');

        if (!match(')')) {
            paramSet = {};
            while (index < length) {
                token = lookahead();
                param = parseVariableIdentifier();
                if (strict) {
                    if (isRestrictedWord(token.value)) {
                        stricted = token;
                        message = Messages.StrictParamName;
                    }
                    if (Object.prototype.hasOwnProperty.call(paramSet, token.value)) {
                        stricted = token;
                        message = Messages.StrictParamDupe;
                    }
                } else if (!firstRestricted) {
                    if (isRestrictedWord(token.value)) {
                        firstRestricted = token;
                        message = Messages.StrictParamName;
                    } else if (isStrictModeReservedWord(token.value)) {
                        firstRestricted = token;
                        message = Messages.StrictReservedWord;
                    } else if (Object.prototype.hasOwnProperty.call(paramSet, token.value)) {
                        firstRestricted = token;
                        message = Messages.StrictParamDupe;
                    }
                }
                params.push(param);
                paramSet[param.name] = true;
                if (match(')')) {
                    break;
                }
                expect(',');
            }
        }

        expect(')');

        previousStrict = strict;
        body = parseFunctionSourceElements();
        if (strict && firstRestricted) {
            throwError(firstRestricted, message);
        }
        if (strict && stricted) {
            throwErrorTolerant(stricted, message);
        }
        strict = previousStrict;

        return {
            type: Syntax.FunctionExpression,
            id: id,
            params: params,
            defaults: [],
            body: body,
            rest: null,
            generator: false,
            expression: false
        };
    }

    // 14 Program

    function parseSourceElement() {
        var token = lookahead();

        if (token.type === Token.Keyword) {
            switch (token.value) {
            case 'const':
            case 'let':
                return parseConstLetDeclaration(token.value);
            case 'function':
                return parseFunctionDeclaration();
            default:
                return parseStatement();
            }
        }

        if (token.type !== Token.EOF) {
            return parseStatement();
        }
    }

    function parseSourceElements() {
        var sourceElement, sourceElements = [], token, directive, firstRestricted;

        while (index < length) {
            token = lookahead();
            if (token.type !== Token.StringLiteral) {
                break;
            }

            sourceElement = parseSourceElement();
            sourceElements.push(sourceElement);
            if (sourceElement.expression.type !== Syntax.Literal) {
                // this is not directive
                break;
            }
            directive = sliceSource(token.range[0] + 1, token.range[1] - 1);
            if (directive === 'use strict') {
                strict = true;
                if (firstRestricted) {
                    throwErrorTolerant(firstRestricted, Messages.StrictOctalLiteral);
                }
            } else {
                if (!firstRestricted && token.octal) {
                    firstRestricted = token;
                }
            }
        }

        while (index < length) {
            sourceElement = parseSourceElement();
            if (typeof sourceElement === 'undefined') {
                break;
            }
            sourceElements.push(sourceElement);
        }
        return sourceElements;
    }

    function parseProgram() {
        var program;
        strict = false;
        program = {
            type: Syntax.Program,
            body: parseSourceElements()
        };
        return program;
    }

    // The following functions are needed only when the option to preserve
    // the comments is active.

    function addComment(type, value, start, end, loc) {
        assert(typeof start === 'number', 'Comment must have valid position');

        // Because the way the actual token is scanned, often the comments
        // (if any) are skipped twice during the lexical analysis.
        // Thus, we need to skip adding a comment if the comment array already
        // handled it.
        if (extra.comments.length > 0) {
            if (extra.comments[extra.comments.length - 1].range[1] > start) {
                return;
            }
        }

        extra.comments.push({
            type: type,
            value: value,
            range: [start, end],
            loc: loc
        });
    }

    function scanComment() {
        var comment, ch, loc, start, blockComment, lineComment;

        comment = '';
        blockComment = false;
        lineComment = false;

        while (index < length) {
            ch = source[index];

            if (lineComment) {
                ch = source[index++];
                if (isLineTerminator(ch)) {
                    loc.end = {
                        line: lineNumber,
                        column: index - lineStart - 1
                    };
                    lineComment = false;
                    addComment('Line', comment, start, index - 1, loc);
                    if (ch === '\r' && source[index] === '\n') {
                        ++index;
                    }
                    ++lineNumber;
                    lineStart = index;
                    comment = '';
                } else if (index >= length) {
                    lineComment = false;
                    comment += ch;
                    loc.end = {
                        line: lineNumber,
                        column: length - lineStart
                    };
                    addComment('Line', comment, start, length, loc);
                } else {
                    comment += ch;
                }
            } else if (blockComment) {
                if (isLineTerminator(ch)) {
                    if (ch === '\r' && source[index + 1] === '\n') {
                        ++index;
                        comment += '\r\n';
                    } else {
                        comment += ch;
                    }
                    ++lineNumber;
                    ++index;
                    lineStart = index;
                    if (index >= length) {
                        throwError({}, Messages.UnexpectedToken, 'ILLEGAL');
                    }
                } else {
                    ch = source[index++];
                    if (index >= length) {
                        throwError({}, Messages.UnexpectedToken, 'ILLEGAL');
                    }
                    comment += ch;
                    if (ch === '*') {
                        ch = source[index];
                        if (ch === '/') {
                            comment = comment.substr(0, comment.length - 1);
                            blockComment = false;
                            ++index;
                            loc.end = {
                                line: lineNumber,
                                column: index - lineStart
                            };
                            addComment('Block', comment, start, index, loc);
                            comment = '';
                        }
                    }
                }
            } else if (ch === '/') {
                ch = source[index + 1];
                if (ch === '/') {
                    loc = {
                        start: {
                            line: lineNumber,
                            column: index - lineStart
                        }
                    };
                    start = index;
                    index += 2;
                    lineComment = true;
                    if (index >= length) {
                        loc.end = {
                            line: lineNumber,
                            column: index - lineStart
                        };
                        lineComment = false;
                        addComment('Line', comment, start, index, loc);
                    }
                } else if (ch === '*') {
                    start = index;
                    index += 2;
                    blockComment = true;
                    loc = {
                        start: {
                            line: lineNumber,
                            column: index - lineStart - 2
                        }
                    };
                    if (index >= length) {
                        throwError({}, Messages.UnexpectedToken, 'ILLEGAL');
                    }
                } else {
                    break;
                }
            } else if (isWhiteSpace(ch)) {
                ++index;
            } else if (isLineTerminator(ch)) {
                ++index;
                if (ch ===  '\r' && source[index] === '\n') {
                    ++index;
                }
                ++lineNumber;
                lineStart = index;
            } else {
                break;
            }
        }
    }

    function filterCommentLocation() {
        var i, entry, comment, comments = [];

        for (i = 0; i < extra.comments.length; ++i) {
            entry = extra.comments[i];
            comment = {
                type: entry.type,
                value: entry.value
            };
            if (extra.range) {
                comment.range = entry.range;
            }
            if (extra.loc) {
                comment.loc = entry.loc;
            }
            comments.push(comment);
        }

        extra.comments = comments;
    }

    function collectToken() {
        var start, loc, token, range, value;

        skipComment();
        start = index;
        loc = {
            start: {
                line: lineNumber,
                column: index - lineStart
            }
        };

        token = extra.advance();
        loc.end = {
            line: lineNumber,
            column: index - lineStart
        };

        if (token.type !== Token.EOF) {
            range = [token.range[0], token.range[1]];
            value = sliceSource(token.range[0], token.range[1]);
            extra.tokens.push({
                type: TokenName[token.type],
                value: value,
                range: range,
                loc: loc
            });
        }

        return token;
    }

    function collectRegex() {
        var pos, loc, regex, token;

        skipComment();

        pos = index;
        loc = {
            start: {
                line: lineNumber,
                column: index - lineStart
            }
        };

        regex = extra.scanRegExp();
        loc.end = {
            line: lineNumber,
            column: index - lineStart
        };

        // Pop the previous token, which is likely '/' or '/='
        if (extra.tokens.length > 0) {
            token = extra.tokens[extra.tokens.length - 1];
            if (token.range[0] === pos && token.type === 'Punctuator') {
                if (token.value === '/' || token.value === '/=') {
                    extra.tokens.pop();
                }
            }
        }

        extra.tokens.push({
            type: 'RegularExpression',
            value: regex.literal,
            range: [pos, index],
            loc: loc
        });

        return regex;
    }

    function filterTokenLocation() {
        var i, entry, token, tokens = [];

        for (i = 0; i < extra.tokens.length; ++i) {
            entry = extra.tokens[i];
            token = {
                type: entry.type,
                value: entry.value
            };
            if (extra.range) {
                token.range = entry.range;
            }
            if (extra.loc) {
                token.loc = entry.loc;
            }
            tokens.push(token);
        }

        extra.tokens = tokens;
    }

    function createLiteral(token) {
        return {
            type: Syntax.Literal,
            value: token.value
        };
    }

    function createRawLiteral(token) {
        return {
            type: Syntax.Literal,
            value: token.value,
            raw: sliceSource(token.range[0], token.range[1])
        };
    }

    function createLocationMarker() {
        var marker = {};

        marker.range = [index, index];
        marker.loc = {
            start: {
                line: lineNumber,
                column: index - lineStart
            },
            end: {
                line: lineNumber,
                column: index - lineStart
            }
        };

        marker.end = function () {
            this.range[1] = index;
            this.loc.end.line = lineNumber;
            this.loc.end.column = index - lineStart;
        };

        marker.applyGroup = function (node) {
            if (extra.range) {
                node.groupRange = [this.range[0], this.range[1]];
            }
            if (extra.loc) {
                node.groupLoc = {
                    start: {
                        line: this.loc.start.line,
                        column: this.loc.start.column
                    },
                    end: {
                        line: this.loc.end.line,
                        column: this.loc.end.column
                    }
                };
            }
        };

        marker.apply = function (node) {
            if (extra.range) {
                node.range = [this.range[0], this.range[1]];
            }
            if (extra.loc) {
                node.loc = {
                    start: {
                        line: this.loc.start.line,
                        column: this.loc.start.column
                    },
                    end: {
                        line: this.loc.end.line,
                        column: this.loc.end.column
                    }
                };
            }
        };

        return marker;
    }

    function trackGroupExpression() {
        var marker, expr;

        skipComment();
        marker = createLocationMarker();
        expect('(');

        expr = parseExpression();

        expect(')');

        marker.end();
        marker.applyGroup(expr);

        return expr;
    }

    function trackLeftHandSideExpression() {
        var marker, expr;

        skipComment();
        marker = createLocationMarker();

        expr = matchKeyword('new') ? parseNewExpression() : parsePrimaryExpression();

        while (match('.') || match('[')) {
            if (match('[')) {
                expr = {
                    type: Syntax.MemberExpression,
                    computed: true,
                    object: expr,
                    property: parseComputedMember()
                };
                marker.end();
                marker.apply(expr);
            } else {
                expr = {
                    type: Syntax.MemberExpression,
                    computed: false,
                    object: expr,
                    property: parseNonComputedMember()
                };
                marker.end();
                marker.apply(expr);
            }
        }

        return expr;
    }

    function trackLeftHandSideExpressionAllowCall() {
        var marker, expr;

        skipComment();
        marker = createLocationMarker();

        expr = matchKeyword('new') ? parseNewExpression() : parsePrimaryExpression();

        while (match('.') || match('[') || match('(')) {
            if (match('(')) {
                expr = {
                    type: Syntax.CallExpression,
                    callee: expr,
                    'arguments': parseArguments()
                };
                marker.end();
                marker.apply(expr);
            } else if (match('[')) {
                expr = {
                    type: Syntax.MemberExpression,
                    computed: true,
                    object: expr,
                    property: parseComputedMember()
                };
                marker.end();
                marker.apply(expr);
            } else {
                expr = {
                    type: Syntax.MemberExpression,
                    computed: false,
                    object: expr,
                    property: parseNonComputedMember()
                };
                marker.end();
                marker.apply(expr);
            }
        }

        return expr;
    }

    function filterGroup(node) {
        var n, i, entry;

        n = (Object.prototype.toString.apply(node) === '[object Array]') ? [] : {};
        for (i in node) {
            if (node.hasOwnProperty(i) && i !== 'groupRange' && i !== 'groupLoc') {
                entry = node[i];
                if (entry === null || typeof entry !== 'object' || entry instanceof RegExp) {
                    n[i] = entry;
                } else {
                    n[i] = filterGroup(entry);
                }
            }
        }
        return n;
    }

    function wrapTrackingFunction(range, loc) {

        return function (parseFunction) {

            function isBinary(node) {
                return node.type === Syntax.LogicalExpression ||
                    node.type === Syntax.BinaryExpression;
            }

            function visit(node) {
                var start, end;

                if (isBinary(node.left)) {
                    visit(node.left);
                }
                if (isBinary(node.right)) {
                    visit(node.right);
                }

                if (range) {
                    if (node.left.groupRange || node.right.groupRange) {
                        start = node.left.groupRange ? node.left.groupRange[0] : node.left.range[0];
                        end = node.right.groupRange ? node.right.groupRange[1] : node.right.range[1];
                        node.range = [start, end];
                    } else if (typeof node.range === 'undefined') {
                        start = node.left.range[0];
                        end = node.right.range[1];
                        node.range = [start, end];
                    }
                }
                if (loc) {
                    if (node.left.groupLoc || node.right.groupLoc) {
                        start = node.left.groupLoc ? node.left.groupLoc.start : node.left.loc.start;
                        end = node.right.groupLoc ? node.right.groupLoc.end : node.right.loc.end;
                        node.loc = {
                            start: start,
                            end: end
                        };
                    } else if (typeof node.loc === 'undefined') {
                        node.loc = {
                            start: node.left.loc.start,
                            end: node.right.loc.end
                        };
                    }
                }
            }

            return function () {
                var marker, node;

                skipComment();

                marker = createLocationMarker();
                node = parseFunction.apply(null, arguments);
                marker.end();

                if (range && typeof node.range === 'undefined') {
                    marker.apply(node);
                }

                if (loc && typeof node.loc === 'undefined') {
                    marker.apply(node);
                }

                if (isBinary(node)) {
                    visit(node);
                }

                return node;
            };
        };
    }

    function patch() {

        var wrapTracking;

        if (extra.comments) {
            extra.skipComment = skipComment;
            skipComment = scanComment;
        }

        if (extra.raw) {
            extra.createLiteral = createLiteral;
            createLiteral = createRawLiteral;
        }

        if (extra.range || extra.loc) {

            extra.parseGroupExpression = parseGroupExpression;
            extra.parseLeftHandSideExpression = parseLeftHandSideExpression;
            extra.parseLeftHandSideExpressionAllowCall = parseLeftHandSideExpressionAllowCall;
            parseGroupExpression = trackGroupExpression;
            parseLeftHandSideExpression = trackLeftHandSideExpression;
            parseLeftHandSideExpressionAllowCall = trackLeftHandSideExpressionAllowCall;

            wrapTracking = wrapTrackingFunction(extra.range, extra.loc);

            extra.parseAdditiveExpression = parseAdditiveExpression;
            extra.parseAssignmentExpression = parseAssignmentExpression;
            extra.parseBitwiseANDExpression = parseBitwiseANDExpression;
            extra.parseBitwiseORExpression = parseBitwiseORExpression;
            extra.parseBitwiseXORExpression = parseBitwiseXORExpression;
            extra.parseBlock = parseBlock;
            extra.parseFunctionSourceElements = parseFunctionSourceElements;
            extra.parseCatchClause = parseCatchClause;
            extra.parseComputedMember = parseComputedMember;
            extra.parseConditionalExpression = parseConditionalExpression;
            extra.parseConstLetDeclaration = parseConstLetDeclaration;
            extra.parseEqualityExpression = parseEqualityExpression;
            extra.parseExpression = parseExpression;
            extra.parseForVariableDeclaration = parseForVariableDeclaration;
            extra.parseFunctionDeclaration = parseFunctionDeclaration;
            extra.parseFunctionExpression = parseFunctionExpression;
            extra.parseLogicalANDExpression = parseLogicalANDExpression;
            extra.parseLogicalORExpression = parseLogicalORExpression;
            extra.parseMultiplicativeExpression = parseMultiplicativeExpression;
            extra.parseNewExpression = parseNewExpression;
            extra.parseNonComputedProperty = parseNonComputedProperty;
            extra.parseObjectProperty = parseObjectProperty;
            extra.parseObjectPropertyKey = parseObjectPropertyKey;
            extra.parsePostfixExpression = parsePostfixExpression;
            extra.parsePrimaryExpression = parsePrimaryExpression;
            extra.parseProgram = parseProgram;
            extra.parsePropertyFunction = parsePropertyFunction;
            extra.parseRelationalExpression = parseRelationalExpression;
            extra.parseStatement = parseStatement;
            extra.parseShiftExpression = parseShiftExpression;
            extra.parseSwitchCase = parseSwitchCase;
            extra.parseUnaryExpression = parseUnaryExpression;
            extra.parseVariableDeclaration = parseVariableDeclaration;
            extra.parseVariableIdentifier = parseVariableIdentifier;

            parseAdditiveExpression = wrapTracking(extra.parseAdditiveExpression);
            parseAssignmentExpression = wrapTracking(extra.parseAssignmentExpression);
            parseBitwiseANDExpression = wrapTracking(extra.parseBitwiseANDExpression);
            parseBitwiseORExpression = wrapTracking(extra.parseBitwiseORExpression);
            parseBitwiseXORExpression = wrapTracking(extra.parseBitwiseXORExpression);
            parseBlock = wrapTracking(extra.parseBlock);
            parseFunctionSourceElements = wrapTracking(extra.parseFunctionSourceElements);
            parseCatchClause = wrapTracking(extra.parseCatchClause);
            parseComputedMember = wrapTracking(extra.parseComputedMember);
            parseConditionalExpression = wrapTracking(extra.parseConditionalExpression);
            parseConstLetDeclaration = wrapTracking(extra.parseConstLetDeclaration);
            parseEqualityExpression = wrapTracking(extra.parseEqualityExpression);
            parseExpression = wrapTracking(extra.parseExpression);
            parseForVariableDeclaration = wrapTracking(extra.parseForVariableDeclaration);
            parseFunctionDeclaration = wrapTracking(extra.parseFunctionDeclaration);
            parseFunctionExpression = wrapTracking(extra.parseFunctionExpression);
            parseLeftHandSideExpression = wrapTracking(parseLeftHandSideExpression);
            parseLogicalANDExpression = wrapTracking(extra.parseLogicalANDExpression);
            parseLogicalORExpression = wrapTracking(extra.parseLogicalORExpression);
            parseMultiplicativeExpression = wrapTracking(extra.parseMultiplicativeExpression);
            parseNewExpression = wrapTracking(extra.parseNewExpression);
            parseNonComputedProperty = wrapTracking(extra.parseNonComputedProperty);
            parseObjectProperty = wrapTracking(extra.parseObjectProperty);
            parseObjectPropertyKey = wrapTracking(extra.parseObjectPropertyKey);
            parsePostfixExpression = wrapTracking(extra.parsePostfixExpression);
            parsePrimaryExpression = wrapTracking(extra.parsePrimaryExpression);
            parseProgram = wrapTracking(extra.parseProgram);
            parsePropertyFunction = wrapTracking(extra.parsePropertyFunction);
            parseRelationalExpression = wrapTracking(extra.parseRelationalExpression);
            parseStatement = wrapTracking(extra.parseStatement);
            parseShiftExpression = wrapTracking(extra.parseShiftExpression);
            parseSwitchCase = wrapTracking(extra.parseSwitchCase);
            parseUnaryExpression = wrapTracking(extra.parseUnaryExpression);
            parseVariableDeclaration = wrapTracking(extra.parseVariableDeclaration);
            parseVariableIdentifier = wrapTracking(extra.parseVariableIdentifier);
        }

        if (typeof extra.tokens !== 'undefined') {
            extra.advance = advance;
            extra.scanRegExp = scanRegExp;

            advance = collectToken;
            scanRegExp = collectRegex;
        }
    }

    function unpatch() {
        if (typeof extra.skipComment === 'function') {
            skipComment = extra.skipComment;
        }

        if (extra.raw) {
            createLiteral = extra.createLiteral;
        }

        if (extra.range || extra.loc) {
            parseAdditiveExpression = extra.parseAdditiveExpression;
            parseAssignmentExpression = extra.parseAssignmentExpression;
            parseBitwiseANDExpression = extra.parseBitwiseANDExpression;
            parseBitwiseORExpression = extra.parseBitwiseORExpression;
            parseBitwiseXORExpression = extra.parseBitwiseXORExpression;
            parseBlock = extra.parseBlock;
            parseFunctionSourceElements = extra.parseFunctionSourceElements;
            parseCatchClause = extra.parseCatchClause;
            parseComputedMember = extra.parseComputedMember;
            parseConditionalExpression = extra.parseConditionalExpression;
            parseConstLetDeclaration = extra.parseConstLetDeclaration;
            parseEqualityExpression = extra.parseEqualityExpression;
            parseExpression = extra.parseExpression;
            parseForVariableDeclaration = extra.parseForVariableDeclaration;
            parseFunctionDeclaration = extra.parseFunctionDeclaration;
            parseFunctionExpression = extra.parseFunctionExpression;
            parseGroupExpression = extra.parseGroupExpression;
            parseLeftHandSideExpression = extra.parseLeftHandSideExpression;
            parseLeftHandSideExpressionAllowCall = extra.parseLeftHandSideExpressionAllowCall;
            parseLogicalANDExpression = extra.parseLogicalANDExpression;
            parseLogicalORExpression = extra.parseLogicalORExpression;
            parseMultiplicativeExpression = extra.parseMultiplicativeExpression;
            parseNewExpression = extra.parseNewExpression;
            parseNonComputedProperty = extra.parseNonComputedProperty;
            parseObjectProperty = extra.parseObjectProperty;
            parseObjectPropertyKey = extra.parseObjectPropertyKey;
            parsePrimaryExpression = extra.parsePrimaryExpression;
            parsePostfixExpression = extra.parsePostfixExpression;
            parseProgram = extra.parseProgram;
            parsePropertyFunction = extra.parsePropertyFunction;
            parseRelationalExpression = extra.parseRelationalExpression;
            parseStatement = extra.parseStatement;
            parseShiftExpression = extra.parseShiftExpression;
            parseSwitchCase = extra.parseSwitchCase;
            parseUnaryExpression = extra.parseUnaryExpression;
            parseVariableDeclaration = extra.parseVariableDeclaration;
            parseVariableIdentifier = extra.parseVariableIdentifier;
        }

        if (typeof extra.scanRegExp === 'function') {
            advance = extra.advance;
            scanRegExp = extra.scanRegExp;
        }
    }

    function stringToArray(str) {
        var length = str.length,
            result = [],
            i;
        for (i = 0; i < length; ++i) {
            result[i] = str.charAt(i);
        }
        return result;
    }

    function parse(code, options) {
        var program, toString;

        toString = String;
        if (typeof code !== 'string' && !(code instanceof String)) {
            code = toString(code);
        }

        source = code;
        index = 0;
        lineNumber = (source.length > 0) ? 1 : 0;
        lineStart = 0;
        length = source.length;
        buffer = null;
        state = {
            allowIn: true,
            labelSet: {},
            inFunctionBody: false,
            inIteration: false,
            inSwitch: false
        };

        extra = {};
        if (typeof options !== 'undefined') {
            extra.range = (typeof options.range === 'boolean') && options.range;
            extra.loc = (typeof options.loc === 'boolean') && options.loc;
            extra.raw = (typeof options.raw === 'boolean') && options.raw;
            if (typeof options.tokens === 'boolean' && options.tokens) {
                extra.tokens = [];
            }
            if (typeof options.comment === 'boolean' && options.comment) {
                extra.comments = [];
            }
            if (typeof options.tolerant === 'boolean' && options.tolerant) {
                extra.errors = [];
            }
        }

        if (length > 0) {
            if (typeof source[0] === 'undefined') {
                // Try first to convert to a string. This is good as fast path
                // for old IE which understands string indexing for string
                // literals only and not for string object.
                if (code instanceof String) {
                    source = code.valueOf();
                }

                // Force accessing the characters via an array.
                if (typeof source[0] === 'undefined') {
                    source = stringToArray(code);
                }
            }
        }

        patch();
        try {
            program = parseProgram();
            if (typeof extra.comments !== 'undefined') {
                filterCommentLocation();
                program.comments = extra.comments;
            }
            if (typeof extra.tokens !== 'undefined') {
                filterTokenLocation();
                program.tokens = extra.tokens;
            }
            if (typeof extra.errors !== 'undefined') {
                program.errors = extra.errors;
            }
            if (extra.range || extra.loc) {
                program.body = filterGroup(program.body);
            }
        } catch (e) {
            throw e;
        } finally {
            unpatch();
            extra = {};
        }

        return program;
    }

    // Sync with package.json.
    exports.version = '1.0.3';

    exports.parse = parse;

    // Deep copy.
    exports.Syntax = (function () {
        var name, types = {};

        if (typeof Object.create === 'function') {
            types = Object.create(null);
        }

        for (name in Syntax) {
            if (Syntax.hasOwnProperty(name)) {
                types[name] = Syntax[name];
            }
        }

        if (typeof Object.freeze === 'function') {
            Object.freeze(types);
        }

        return types;
    }());

}));
/* vim: set sw=4 ts=4 et tw=80 : */
;
// Acorn is a tiny, fast JavaScript parser written in JavaScript.
//
// Acorn was written by Marijn Haverbeke and released under an MIT
// license. The Unicode regexps (for identifiers and whitespace) were
// taken from [Esprima](http://esprima.org) by Ariya Hidayat.
//
// Git repositories for Acorn are available at
//
//     http://marijnhaverbeke.nl/git/acorn
//     https://github.com/marijnh/acorn.git
//
// Please use the [github bug tracker][ghbt] to report issues.
//
// [ghbt]: https://github.com/marijnh/acorn/issues
//
// This file defines the main parser interface. The library also comes
// with a [error-tolerant parser][dammit] and an
// [abstract syntax tree walker][walk], defined in other files.
//
// [dammit]: acorn_loose.js
// [walk]: util/walk.js

(function(mod) {
  if (typeof exports == "object" && typeof module == "object") return mod(exports); // CommonJS
  if (typeof define == "function" && define.amd) return define(["exports"], mod); // AMD
  mod(this.acorn || (this.acorn = {})); // Plain browser env
})(function(exports) {
  "use strict";

  exports.version = "0.3.1";

  // The main exported interface (under `self.acorn` when in the
  // browser) is a `parse` function that takes a code string and
  // returns an abstract syntax tree as specified by [Mozilla parser
  // API][api], with the caveat that the SpiderMonkey-specific syntax
  // (`let`, `yield`, inline XML, etc) is not recognized.
  //
  // [api]: https://developer.mozilla.org/en-US/docs/SpiderMonkey/Parser_API

  var options, input, inputLen, sourceFile;

  exports.parse = function(inpt, opts) {
    input = String(inpt); inputLen = input.length;
    setOptions(opts);
    initTokenState();
    return parseTopLevel(options.program);
  };

  // A second optional argument can be given to further configure
  // the parser process. These options are recognized:

  var defaultOptions = exports.defaultOptions = {
    // `ecmaVersion` indicates the ECMAScript version to parse. Must
    // be either 3 or 5. This
    // influences support for strict mode, the set of reserved words, and
    // support for getters and setter.
    ecmaVersion: 5,
    // Turn on `strictSemicolons` to prevent the parser from doing
    // automatic semicolon insertion.
    strictSemicolons: false,
    // When `allowTrailingCommas` is false, the parser will not allow
    // trailing commas in array and object literals.
    allowTrailingCommas: true,
    // By default, reserved words are not enforced. Enable
    // `forbidReserved` to enforce them.
    forbidReserved: false,
    // When `locations` is on, `loc` properties holding objects with
    // `start` and `end` properties in `{line, column}` form (with
    // line being 1-based and column 0-based) will be attached to the
    // nodes.
    locations: false,
    // A function can be passed as `onComment` option, which will
    // cause Acorn to call that function with `(block, text, start,
    // end)` parameters whenever a comment is skipped. `block` is a
    // boolean indicating whether this is a block (`/* */`) comment,
    // `text` is the content of the comment, and `start` and `end` are
    // character offsets that denote the start and end of the comment.
    // When the `locations` option is on, two more parameters are
    // passed, the full `{line, column}` locations of the start and
    // end of the comments.
    onComment: null,
    // Nodes have their start and end characters offsets recorded in
    // `start` and `end` properties (directly on the node, rather than
    // the `loc` object, which holds line/column data. To also add a
    // [semi-standardized][range] `range` property holding a `[start,
    // end]` array with the same numbers, set the `ranges` option to
    // `true`.
    //
    // [range]: https://bugzilla.mozilla.org/show_bug.cgi?id=745678
    ranges: false,
    // It is possible to parse multiple files into a single AST by
    // passing the tree produced by parsing the first file as
    // `program` option in subsequent parses. This will add the
    // toplevel forms of the parsed file to the `Program` (top) node
    // of an existing parse tree.
    program: null,
    // When `location` is on, you can pass this to record the source
    // file in every node's `loc` object.
    sourceFile: null
  };

  function setOptions(opts) {
    options = opts || {};
    for (var opt in defaultOptions) if (!Object.prototype.hasOwnProperty.call(options, opt))
      options[opt] = defaultOptions[opt];
    sourceFile = options.sourceFile || null;
  }

  // The `getLineInfo` function is mostly useful when the
  // `locations` option is off (for performance reasons) and you
  // want to find the line/column position for a given character
  // offset. `input` should be the code string that the offset refers
  // into.

  var getLineInfo = exports.getLineInfo = function(input, offset) {
    for (var line = 1, cur = 0;;) {
      lineBreak.lastIndex = cur;
      var match = lineBreak.exec(input);
      if (match && match.index < offset) {
        ++line;
        cur = match.index + match[0].length;
      } else break;
    }
    return {line: line, column: offset - cur};
  };

  // Acorn is organized as a tokenizer and a recursive-descent parser.
  // The `tokenize` export provides an interface to the tokenizer.
  // Because the tokenizer is optimized for being efficiently used by
  // the Acorn parser itself, this interface is somewhat crude and not
  // very modular. Performing another parse or call to `tokenize` will
  // reset the internal state, and invalidate existing tokenizers.

  exports.tokenize = function(inpt, opts) {
    input = String(inpt); inputLen = input.length;
    setOptions(opts);
    initTokenState();

    var t = {};
    function getToken(forceRegexp) {
      readToken(forceRegexp);
      t.start = tokStart; t.end = tokEnd;
      t.startLoc = tokStartLoc; t.endLoc = tokEndLoc;
      t.type = tokType; t.value = tokVal;
      return t;
    }
    getToken.jumpTo = function(pos, reAllowed) {
      tokPos = pos;
      if (options.locations) {
        tokCurLine = tokLineStart = lineBreak.lastIndex = 0;
        var match;
        while ((match = lineBreak.exec(input)) && match.index < pos) {
          ++tokCurLine;
          tokLineStart = match.index + match[0].length;
        }
      }
      var ch = input.charAt(pos - 1);
      tokRegexpAllowed = reAllowed;
      skipSpace();
    };
    return getToken;
  };

  // State is kept in (closure-)global variables. We already saw the
  // `options`, `input`, and `inputLen` variables above.

  // The current position of the tokenizer in the input.

  var tokPos;

  // The start and end offsets of the current token.

  var tokStart, tokEnd;

  // When `options.locations` is true, these hold objects
  // containing the tokens start and end line/column pairs.

  var tokStartLoc, tokEndLoc;

  // The type and value of the current token. Token types are objects,
  // named by variables against which they can be compared, and
  // holding properties that describe them (indicating, for example,
  // the precedence of an infix operator, and the original name of a
  // keyword token). The kind of value that's held in `tokVal` depends
  // on the type of the token. For literals, it is the literal value,
  // for operators, the operator name, and so on.

  var tokType, tokVal;

  // Interal state for the tokenizer. To distinguish between division
  // operators and regular expressions, it remembers whether the last
  // token was one that is allowed to be followed by an expression.
  // (If it is, a slash is probably a regexp, if it isn't it's a
  // division operator. See the `parseStatement` function for a
  // caveat.)

  var tokRegexpAllowed;

  // When `options.locations` is true, these are used to keep
  // track of the current line, and know when a new line has been
  // entered.

  var tokCurLine, tokLineStart;

  // These store the position of the previous token, which is useful
  // when finishing a node and assigning its `end` position.

  var lastStart, lastEnd, lastEndLoc;

  // This is the parser's state. `inFunction` is used to reject
  // `return` statements outside of functions, `labels` to verify that
  // `break` and `continue` have somewhere to jump to, and `strict`
  // indicates whether strict mode is on.

  var inFunction, labels, strict;

  // This function is used to raise exceptions on parse errors. It
  // takes an offset integer (into the current `input`) to indicate
  // the location of the error, attaches the position to the end
  // of the error message, and then raises a `SyntaxError` with that
  // message.

  function raise(pos, message) {
    var loc = getLineInfo(input, pos);
    message += " (" + loc.line + ":" + loc.column + ")";
    var err = new SyntaxError(message);
    err.pos = pos; err.loc = loc; err.raisedAt = tokPos;
    throw err;
  }

  // ## Token types

  // The assignment of fine-grained, information-carrying type objects
  // allows the tokenizer to store the information it has about a
  // token in a way that is very cheap for the parser to look up.

  // All token type variables start with an underscore, to make them
  // easy to recognize.

  // These are the general types. The `type` property is only used to
  // make them recognizeable when debugging.

  var _num = {type: "num"}, _regexp = {type: "regexp"}, _string = {type: "string"};
  var _name = {type: "name"}, _eof = {type: "eof"};

  // Keyword tokens. The `keyword` property (also used in keyword-like
  // operators) indicates that the token originated from an
  // identifier-like word, which is used when parsing property names.
  //
  // The `beforeExpr` property is used to disambiguate between regular
  // expressions and divisions. It is set on all token types that can
  // be followed by an expression (thus, a slash after them would be a
  // regular expression).
  //
  // `isLoop` marks a keyword as starting a loop, which is important
  // to know when parsing a label, in order to allow or disallow
  // continue jumps to that label.

  var _break = {keyword: "break"}, _case = {keyword: "case", beforeExpr: true}, _catch = {keyword: "catch"};
  var _continue = {keyword: "continue"}, _debugger = {keyword: "debugger"}, _default = {keyword: "default"};
  var _do = {keyword: "do", isLoop: true}, _else = {keyword: "else", beforeExpr: true};
  var _finally = {keyword: "finally"}, _for = {keyword: "for", isLoop: true}, _function = {keyword: "function"};
  var _if = {keyword: "if"}, _return = {keyword: "return", beforeExpr: true}, _switch = {keyword: "switch"};
  var _throw = {keyword: "throw", beforeExpr: true}, _try = {keyword: "try"}, _var = {keyword: "var"};
  var _while = {keyword: "while", isLoop: true}, _with = {keyword: "with"}, _new = {keyword: "new", beforeExpr: true};
  var _this = {keyword: "this"};

  // The keywords that denote values.

  var _null = {keyword: "null", atomValue: null}, _true = {keyword: "true", atomValue: true};
  var _false = {keyword: "false", atomValue: false};

  // Some keywords are treated as regular operators. `in` sometimes
  // (when parsing `for`) needs to be tested against specifically, so
  // we assign a variable name to it for quick comparing.

  var _in = {keyword: "in", binop: 7, beforeExpr: true};

  // Map keyword names to token types.

  var keywordTypes = {"break": _break, "case": _case, "catch": _catch,
                      "continue": _continue, "debugger": _debugger, "default": _default,
                      "do": _do, "else": _else, "finally": _finally, "for": _for,
                      "function": _function, "if": _if, "return": _return, "switch": _switch,
                      "throw": _throw, "try": _try, "var": _var, "while": _while, "with": _with,
                      "null": _null, "true": _true, "false": _false, "new": _new, "in": _in,
                      "instanceof": {keyword: "instanceof", binop: 7, beforeExpr: true}, "this": _this,
                      "typeof": {keyword: "typeof", prefix: true, beforeExpr: true},
                      "void": {keyword: "void", prefix: true, beforeExpr: true},
                      "delete": {keyword: "delete", prefix: true, beforeExpr: true}};

  // Punctuation token types. Again, the `type` property is purely for debugging.

  var _bracketL = {type: "[", beforeExpr: true}, _bracketR = {type: "]"}, _braceL = {type: "{", beforeExpr: true};
  var _braceR = {type: "}"}, _parenL = {type: "(", beforeExpr: true}, _parenR = {type: ")"};
  var _comma = {type: ",", beforeExpr: true}, _semi = {type: ";", beforeExpr: true};
  var _colon = {type: ":", beforeExpr: true}, _dot = {type: "."}, _question = {type: "?", beforeExpr: true};

  // Operators. These carry several kinds of properties to help the
  // parser use them properly (the presence of these properties is
  // what categorizes them as operators).
  //
  // `binop`, when present, specifies that this operator is a binary
  // operator, and will refer to its precedence.
  //
  // `prefix` and `postfix` mark the operator as a prefix or postfix
  // unary operator. `isUpdate` specifies that the node produced by
  // the operator should be of type UpdateExpression rather than
  // simply UnaryExpression (`++` and `--`).
  //
  // `isAssign` marks all of `=`, `+=`, `-=` etcetera, which act as
  // binary operators with a very low precedence, that should result
  // in AssignmentExpression nodes.

  var _slash = {binop: 10, beforeExpr: true}, _eq = {isAssign: true, beforeExpr: true};
  var _assign = {isAssign: true, beforeExpr: true}, _plusmin = {binop: 9, prefix: true, beforeExpr: true};
  var _incdec = {postfix: true, prefix: true, isUpdate: true}, _prefix = {prefix: true, beforeExpr: true};
  var _bin1 = {binop: 1, beforeExpr: true}, _bin2 = {binop: 2, beforeExpr: true};
  var _bin3 = {binop: 3, beforeExpr: true}, _bin4 = {binop: 4, beforeExpr: true};
  var _bin5 = {binop: 5, beforeExpr: true}, _bin6 = {binop: 6, beforeExpr: true};
  var _bin7 = {binop: 7, beforeExpr: true}, _bin8 = {binop: 8, beforeExpr: true};
  var _bin10 = {binop: 10, beforeExpr: true};

  // Provide access to the token types for external users of the
  // tokenizer.

  exports.tokTypes = {bracketL: _bracketL, bracketR: _bracketR, braceL: _braceL, braceR: _braceR,
                      parenL: _parenL, parenR: _parenR, comma: _comma, semi: _semi, colon: _colon,
                      dot: _dot, question: _question, slash: _slash, eq: _eq, name: _name, eof: _eof,
                      num: _num, regexp: _regexp, string: _string};
  for (var kw in keywordTypes) exports.tokTypes["_" + kw] = keywordTypes[kw];

  // This is a trick taken from Esprima. It turns out that, on
  // non-Chrome browsers, to check whether a string is in a set, a
  // predicate containing a big ugly `switch` statement is faster than
  // a regular expression, and on Chrome the two are about on par.
  // This function uses `eval` (non-lexical) to produce such a
  // predicate from a space-separated string of words.
  //
  // It starts by sorting the words by length.

  function makePredicate(words) {
    words = words.split(" ");
    var f = "", cats = [];
    out: for (var i = 0; i < words.length; ++i) {
      for (var j = 0; j < cats.length; ++j)
        if (cats[j][0].length == words[i].length) {
          cats[j].push(words[i]);
          continue out;
        }
      cats.push([words[i]]);
    }
    function compareTo(arr) {
      if (arr.length == 1) return f += "return str === " + JSON.stringify(arr[0]) + ";";
      f += "switch(str){";
      for (var i = 0; i < arr.length; ++i) f += "case " + JSON.stringify(arr[i]) + ":";
      f += "return true}return false;";
    }

    // When there are more than three length categories, an outer
    // switch first dispatches on the lengths, to save on comparisons.

    if (cats.length > 3) {
      cats.sort(function(a, b) {return b.length - a.length;});
      f += "switch(str.length){";
      for (var i = 0; i < cats.length; ++i) {
        var cat = cats[i];
        f += "case " + cat[0].length + ":";
        compareTo(cat);
      }
      f += "}";

    // Otherwise, simply generate a flat `switch` statement.

    } else {
      compareTo(words);
    }
    return new Function("str", f);
  }

  // The ECMAScript 3 reserved word list.

  var isReservedWord3 = makePredicate("abstract boolean byte char class double enum export extends final float goto implements import int interface long native package private protected public short static super synchronized throws transient volatile");

  // ECMAScript 5 reserved words.

  var isReservedWord5 = makePredicate("class enum extends super const export import");

  // The additional reserved words in strict mode.

  var isStrictReservedWord = makePredicate("implements interface let package private protected public static yield");

  // The forbidden variable names in strict mode.

  var isStrictBadIdWord = makePredicate("eval arguments");

  // And the keywords.

  var isKeyword = makePredicate("break case catch continue debugger default do else finally for function if return switch throw try var while with null true false instanceof typeof void delete new in this");

  // ## Character categories

  // Big ugly regular expressions that match characters in the
  // whitespace, identifier, and identifier-start categories. These
  // are only applied when a character is found to actually have a
  // code point above 128.

  var nonASCIIwhitespace = /[\u1680\u180e\u2000-\u200a\u2028\u2029\u202f\u205f\u3000\ufeff]/;
  var nonASCIIidentifierStartChars = "\xaa\xb5\xba\xc0-\xd6\xd8-\xf6\xf8-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0370-\u0374\u0376\u0377\u037a-\u037d\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0481\u048a-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05d0-\u05ea\u05f0-\u05f2\u0620-\u064a\u066e\u066f\u0671-\u06d3\u06d5\u06e5\u06e6\u06ee\u06ef\u06fa-\u06fc\u06ff\u0710\u0712-\u072f\u074d-\u07a5\u07b1\u07ca-\u07ea\u07f4\u07f5\u07fa\u0800-\u0815\u081a\u0824\u0828\u0840-\u0858\u08a0\u08a2-\u08ac\u0904-\u0939\u093d\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097f\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bd\u09ce\u09dc\u09dd\u09df-\u09e1\u09f0\u09f1\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a59-\u0a5c\u0a5e\u0a72-\u0a74\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abd\u0ad0\u0ae0\u0ae1\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3d\u0b5c\u0b5d\u0b5f-\u0b61\u0b71\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bd0\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c33\u0c35-\u0c39\u0c3d\u0c58\u0c59\u0c60\u0c61\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbd\u0cde\u0ce0\u0ce1\u0cf1\u0cf2\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d\u0d4e\u0d60\u0d61\u0d7a-\u0d7f\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0e01-\u0e30\u0e32\u0e33\u0e40-\u0e46\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb0\u0eb2\u0eb3\u0ebd\u0ec0-\u0ec4\u0ec6\u0edc-\u0edf\u0f00\u0f40-\u0f47\u0f49-\u0f6c\u0f88-\u0f8c\u1000-\u102a\u103f\u1050-\u1055\u105a-\u105d\u1061\u1065\u1066\u106e-\u1070\u1075-\u1081\u108e\u10a0-\u10c5\u10c7\u10cd\u10d0-\u10fa\u10fc-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u1380-\u138f\u13a0-\u13f4\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u16ee-\u16f0\u1700-\u170c\u170e-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176c\u176e-\u1770\u1780-\u17b3\u17d7\u17dc\u1820-\u1877\u1880-\u18a8\u18aa\u18b0-\u18f5\u1900-\u191c\u1950-\u196d\u1970-\u1974\u1980-\u19ab\u19c1-\u19c7\u1a00-\u1a16\u1a20-\u1a54\u1aa7\u1b05-\u1b33\u1b45-\u1b4b\u1b83-\u1ba0\u1bae\u1baf\u1bba-\u1be5\u1c00-\u1c23\u1c4d-\u1c4f\u1c5a-\u1c7d\u1ce9-\u1cec\u1cee-\u1cf1\u1cf5\u1cf6\u1d00-\u1dbf\u1e00-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u2071\u207f\u2090-\u209c\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u2160-\u2188\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cee\u2cf2\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d80-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u2e2f\u3005-\u3007\u3021-\u3029\u3031-\u3035\u3038-\u303c\u3041-\u3096\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fcc\ua000-\ua48c\ua4d0-\ua4fd\ua500-\ua60c\ua610-\ua61f\ua62a\ua62b\ua640-\ua66e\ua67f-\ua697\ua6a0-\ua6ef\ua717-\ua71f\ua722-\ua788\ua78b-\ua78e\ua790-\ua793\ua7a0-\ua7aa\ua7f8-\ua801\ua803-\ua805\ua807-\ua80a\ua80c-\ua822\ua840-\ua873\ua882-\ua8b3\ua8f2-\ua8f7\ua8fb\ua90a-\ua925\ua930-\ua946\ua960-\ua97c\ua984-\ua9b2\ua9cf\uaa00-\uaa28\uaa40-\uaa42\uaa44-\uaa4b\uaa60-\uaa76\uaa7a\uaa80-\uaaaf\uaab1\uaab5\uaab6\uaab9-\uaabd\uaac0\uaac2\uaadb-\uaadd\uaae0-\uaaea\uaaf2-\uaaf4\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uabc0-\uabe2\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf900-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\ufb1d\ufb1f-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe70-\ufe74\ufe76-\ufefc\uff21-\uff3a\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc";
  var nonASCIIidentifierChars = "\u0300-\u036f\u0483-\u0487\u0591-\u05bd\u05bf\u05c1\u05c2\u05c4\u05c5\u05c7\u0610-\u061a\u0620-\u0649\u0672-\u06d3\u06e7-\u06e8\u06fb-\u06fc\u0730-\u074a\u0800-\u0814\u081b-\u0823\u0825-\u0827\u0829-\u082d\u0840-\u0857\u08e4-\u08fe\u0900-\u0903\u093a-\u093c\u093e-\u094f\u0951-\u0957\u0962-\u0963\u0966-\u096f\u0981-\u0983\u09bc\u09be-\u09c4\u09c7\u09c8\u09d7\u09df-\u09e0\u0a01-\u0a03\u0a3c\u0a3e-\u0a42\u0a47\u0a48\u0a4b-\u0a4d\u0a51\u0a66-\u0a71\u0a75\u0a81-\u0a83\u0abc\u0abe-\u0ac5\u0ac7-\u0ac9\u0acb-\u0acd\u0ae2-\u0ae3\u0ae6-\u0aef\u0b01-\u0b03\u0b3c\u0b3e-\u0b44\u0b47\u0b48\u0b4b-\u0b4d\u0b56\u0b57\u0b5f-\u0b60\u0b66-\u0b6f\u0b82\u0bbe-\u0bc2\u0bc6-\u0bc8\u0bca-\u0bcd\u0bd7\u0be6-\u0bef\u0c01-\u0c03\u0c46-\u0c48\u0c4a-\u0c4d\u0c55\u0c56\u0c62-\u0c63\u0c66-\u0c6f\u0c82\u0c83\u0cbc\u0cbe-\u0cc4\u0cc6-\u0cc8\u0cca-\u0ccd\u0cd5\u0cd6\u0ce2-\u0ce3\u0ce6-\u0cef\u0d02\u0d03\u0d46-\u0d48\u0d57\u0d62-\u0d63\u0d66-\u0d6f\u0d82\u0d83\u0dca\u0dcf-\u0dd4\u0dd6\u0dd8-\u0ddf\u0df2\u0df3\u0e34-\u0e3a\u0e40-\u0e45\u0e50-\u0e59\u0eb4-\u0eb9\u0ec8-\u0ecd\u0ed0-\u0ed9\u0f18\u0f19\u0f20-\u0f29\u0f35\u0f37\u0f39\u0f41-\u0f47\u0f71-\u0f84\u0f86-\u0f87\u0f8d-\u0f97\u0f99-\u0fbc\u0fc6\u1000-\u1029\u1040-\u1049\u1067-\u106d\u1071-\u1074\u1082-\u108d\u108f-\u109d\u135d-\u135f\u170e-\u1710\u1720-\u1730\u1740-\u1750\u1772\u1773\u1780-\u17b2\u17dd\u17e0-\u17e9\u180b-\u180d\u1810-\u1819\u1920-\u192b\u1930-\u193b\u1951-\u196d\u19b0-\u19c0\u19c8-\u19c9\u19d0-\u19d9\u1a00-\u1a15\u1a20-\u1a53\u1a60-\u1a7c\u1a7f-\u1a89\u1a90-\u1a99\u1b46-\u1b4b\u1b50-\u1b59\u1b6b-\u1b73\u1bb0-\u1bb9\u1be6-\u1bf3\u1c00-\u1c22\u1c40-\u1c49\u1c5b-\u1c7d\u1cd0-\u1cd2\u1d00-\u1dbe\u1e01-\u1f15\u200c\u200d\u203f\u2040\u2054\u20d0-\u20dc\u20e1\u20e5-\u20f0\u2d81-\u2d96\u2de0-\u2dff\u3021-\u3028\u3099\u309a\ua640-\ua66d\ua674-\ua67d\ua69f\ua6f0-\ua6f1\ua7f8-\ua800\ua806\ua80b\ua823-\ua827\ua880-\ua881\ua8b4-\ua8c4\ua8d0-\ua8d9\ua8f3-\ua8f7\ua900-\ua909\ua926-\ua92d\ua930-\ua945\ua980-\ua983\ua9b3-\ua9c0\uaa00-\uaa27\uaa40-\uaa41\uaa4c-\uaa4d\uaa50-\uaa59\uaa7b\uaae0-\uaae9\uaaf2-\uaaf3\uabc0-\uabe1\uabec\uabed\uabf0-\uabf9\ufb20-\ufb28\ufe00-\ufe0f\ufe20-\ufe26\ufe33\ufe34\ufe4d-\ufe4f\uff10-\uff19\uff3f";
  var nonASCIIidentifierStart = new RegExp("[" + nonASCIIidentifierStartChars + "]");
  var nonASCIIidentifier = new RegExp("[" + nonASCIIidentifierStartChars + nonASCIIidentifierChars + "]");

  // Whether a single character denotes a newline.

  var newline = /[\n\r\u2028\u2029]/;

  // Matches a whole line break (where CRLF is considered a single
  // line break). Used to count lines.

  var lineBreak = /\r\n|[\n\r\u2028\u2029]/g;

  // Test whether a given character code starts an identifier.

  var isIdentifierStart = exports.isIdentifierStart = function(code) {
    if (code < 65) return code === 36;
    if (code < 91) return true;
    if (code < 97) return code === 95;
    if (code < 123)return true;
    return code >= 0xaa && nonASCIIidentifierStart.test(String.fromCharCode(code));
  };

  // Test whether a given character is part of an identifier.

  var isIdentifierChar = exports.isIdentifierChar = function(code) {
    if (code < 48) return code === 36;
    if (code < 58) return true;
    if (code < 65) return false;
    if (code < 91) return true;
    if (code < 97) return code === 95;
    if (code < 123)return true;
    return code >= 0xaa && nonASCIIidentifier.test(String.fromCharCode(code));
  };

  // ## Tokenizer

  // These are used when `options.locations` is on, for the
  // `tokStartLoc` and `tokEndLoc` properties.

  function line_loc_t() {
    this.line = tokCurLine;
    this.column = tokPos - tokLineStart;
  }

  // Reset the token state. Used at the start of a parse.

  function initTokenState() {
    tokCurLine = 1;
    tokPos = tokLineStart = 0;
    tokRegexpAllowed = true;
    skipSpace();
  }

  // Called at the end of every token. Sets `tokEnd`, `tokVal`, and
  // `tokRegexpAllowed`, and skips the space after the token, so that
  // the next one's `tokStart` will point at the right position.

  function finishToken(type, val) {
    tokEnd = tokPos;
    if (options.locations) tokEndLoc = new line_loc_t;
    tokType = type;
    skipSpace();
    tokVal = val;
    tokRegexpAllowed = type.beforeExpr;
  }

  function skipBlockComment() {
    var startLoc = options.onComment && options.locations && new line_loc_t;
    var start = tokPos, end = input.indexOf("*/", tokPos += 2);
    if (end === -1) raise(tokPos - 2, "Unterminated comment");
    tokPos = end + 2;
    if (options.locations) {
      lineBreak.lastIndex = start;
      var match;
      while ((match = lineBreak.exec(input)) && match.index < tokPos) {
        ++tokCurLine;
        tokLineStart = match.index + match[0].length;
      }
    }
    if (options.onComment)
      options.onComment(true, input.slice(start + 2, end), start, tokPos,
                        startLoc, options.locations && new line_loc_t);
  }

  function skipLineComment() {
    var start = tokPos;
    var startLoc = options.onComment && options.locations && new line_loc_t;
    var ch = input.charCodeAt(tokPos+=2);
    while (tokPos < inputLen && ch !== 10 && ch !== 13 && ch !== 8232 && ch !== 8329) {
      ++tokPos;
      ch = input.charCodeAt(tokPos);
    }
    if (options.onComment)
      options.onComment(false, input.slice(start + 2, tokPos), start, tokPos,
                        startLoc, options.locations && new line_loc_t);
  }

  // Called at the start of the parse and after every token. Skips
  // whitespace and comments, and.

  function skipSpace() {
    while (tokPos < inputLen) {
      var ch = input.charCodeAt(tokPos);
      if (ch === 32) { // ' '
        ++tokPos;
      } else if(ch === 13) {
        ++tokPos;
        var next = input.charCodeAt(tokPos);
        if(next === 10) {
          ++tokPos;
        }
        if(options.locations) {
          ++tokCurLine;
          tokLineStart = tokPos;
        }
      } else if (ch === 10) {
        ++tokPos;
        ++tokCurLine;
        tokLineStart = tokPos;
      } else if(ch < 14 && ch > 8) {
        ++tokPos;
      } else if (ch === 47) { // '/'
        var next = input.charCodeAt(tokPos+1);
        if (next === 42) { // '*'
          skipBlockComment();
        } else if (next === 47) { // '/'
          skipLineComment();
        } else break;
      } else if ((ch < 14 && ch > 8) || ch === 32 || ch === 160) { // ' ', '\xa0'
        ++tokPos;
      } else if (ch >= 5760 && nonASCIIwhitespace.test(String.fromCharCode(ch))) {
        ++tokPos;
      } else {
        break;
      }
    }
  }

  // ### Token reading

  // This is the function that is called to fetch the next token. It
  // is somewhat obscure, because it works in character codes rather
  // than characters, and because operator parsing has been inlined
  // into it.
  //
  // All in the name of speed.
  //
  // The `forceRegexp` parameter is used in the one case where the
  // `tokRegexpAllowed` trick does not work. See `parseStatement`.

  function readToken_dot() {
    var next = input.charCodeAt(tokPos+1);
    if (next >= 48 && next <= 57) return readNumber(true);
    ++tokPos;
    return finishToken(_dot);
  }

  function readToken_slash() { // '/'
    var next = input.charCodeAt(tokPos+1);
    if (tokRegexpAllowed) {++tokPos; return readRegexp();}
    if (next === 61) return finishOp(_assign, 2);
    return finishOp(_slash, 1);
  }

  function readToken_mult_modulo() { // '%*'
    var next = input.charCodeAt(tokPos+1);
    if (next === 61) return finishOp(_assign, 2);
    return finishOp(_bin10, 1);
  }

  function readToken_pipe_amp(code) { // '|&'
    var next = input.charCodeAt(tokPos+1);
    if (next === code) return finishOp(code === 124 ? _bin1 : _bin2, 2);
    if (next === 61) return finishOp(_assign, 2);
    return finishOp(code === 124 ? _bin3 : _bin5, 1);
  }

  function readToken_caret() { // '^'
    var next = input.charCodeAt(tokPos+1);
    if (next === 61) return finishOp(_assign, 2);
    return finishOp(_bin4, 1);
  }

  function readToken_plus_min(code) { // '+-'
    var next = input.charCodeAt(tokPos+1);
    if (next === code) return finishOp(_incdec, 2);
    if (next === 61) return finishOp(_assign, 2);
    return finishOp(_plusmin, 1);
  }

  function readToken_lt_gt(code) { // '<>'
    var next = input.charCodeAt(tokPos+1);
    var size = 1;
    if (next === code) {
      size = code === 62 && input.charCodeAt(tokPos+2) === 62 ? 3 : 2;
      if (input.charCodeAt(tokPos + size) === 61) return finishOp(_assign, size + 1);
      return finishOp(_bin8, size);
    }
    if (next === 61)
      size = input.charCodeAt(tokPos+2) === 61 ? 3 : 2;
    return finishOp(_bin7, size);
  }

  function readToken_eq_excl(code) { // '=!'
    var next = input.charCodeAt(tokPos+1);
    if (next === 61) return finishOp(_bin6, input.charCodeAt(tokPos+2) === 61 ? 3 : 2);
    return finishOp(code === 61 ? _eq : _prefix, 1);
  }

  function getTokenFromCode(code) {
    switch(code) {
      // The interpretation of a dot depends on whether it is followed
      // by a digit.
    case 46: // '.'
      return readToken_dot();

      // Punctuation tokens.
    case 40: ++tokPos; return finishToken(_parenL);
    case 41: ++tokPos; return finishToken(_parenR);
    case 59: ++tokPos; return finishToken(_semi);
    case 44: ++tokPos; return finishToken(_comma);
    case 91: ++tokPos; return finishToken(_bracketL);
    case 93: ++tokPos; return finishToken(_bracketR);
    case 123: ++tokPos; return finishToken(_braceL);
    case 125: ++tokPos; return finishToken(_braceR);
    case 58: ++tokPos; return finishToken(_colon);
    case 63: ++tokPos; return finishToken(_question);

      // '0x' is a hexadecimal number.
    case 48: // '0'
      var next = input.charCodeAt(tokPos+1);
      if (next === 120 || next === 88) return readHexNumber();
      // Anything else beginning with a digit is an integer, octal
      // number, or float.
    case 49: case 50: case 51: case 52: case 53: case 54: case 55: case 56: case 57: // 1-9
      return readNumber(false);

      // Quotes produce strings.
    case 34: case 39: // '"', "'"
      return readString(code);

    // Operators are parsed inline in tiny state machines. '=' (61) is
    // often referred to. `finishOp` simply skips the amount of
    // characters it is given as second argument, and returns a token
    // of the type given by its first argument.

    case 47: // '/'
      return readToken_slash(code);

    case 37: case 42: // '%*'
      return readToken_mult_modulo();

    case 124: case 38: // '|&'
      return readToken_pipe_amp(code);

    case 94: // '^'
      return readToken_caret();

    case 43: case 45: // '+-'
      return readToken_plus_min(code);

    case 60: case 62: // '<>'
      return readToken_lt_gt(code);

    case 61: case 33: // '=!'
      return readToken_eq_excl(code);

    case 126: // '~'
      return finishOp(_prefix, 1);
    }

    return false;
  }

  function readToken(forceRegexp) {
    if (!forceRegexp) tokStart = tokPos;
    else tokPos = tokStart + 1;
    if (options.locations) tokStartLoc = new line_loc_t;
    if (forceRegexp) return readRegexp();
    if (tokPos >= inputLen) return finishToken(_eof);

    var code = input.charCodeAt(tokPos);
    // Identifier or keyword. '\uXXXX' sequences are allowed in
    // identifiers, so '\' also dispatches to that.
    if (isIdentifierStart(code) || code === 92 /* '\' */) return readWord();

    var tok = getTokenFromCode(code);

    if (tok === false) {
      // If we are here, we either found a non-ASCII identifier
      // character, or something that's entirely disallowed.
      var ch = String.fromCharCode(code);
      if (ch === "\\" || nonASCIIidentifierStart.test(ch)) return readWord();
      raise(tokPos, "Unexpected character '" + ch + "'");
    }
    return tok;
  }

  function finishOp(type, size) {
    var str = input.slice(tokPos, tokPos + size);
    tokPos += size;
    finishToken(type, str);
  }

  // Parse a regular expression. Some context-awareness is necessary,
  // since a '/' inside a '[]' set does not end the expression.

  function readRegexp() {
    var content = "", escaped, inClass, start = tokPos;
    for (;;) {
      if (tokPos >= inputLen) raise(start, "Unterminated regular expression");
      var ch = input.charAt(tokPos);
      if (newline.test(ch)) raise(start, "Unterminated regular expression");
      if (!escaped) {
        if (ch === "[") inClass = true;
        else if (ch === "]" && inClass) inClass = false;
        else if (ch === "/" && !inClass) break;
        escaped = ch === "\\";
      } else escaped = false;
      ++tokPos;
    }
    var content = input.slice(start, tokPos);
    ++tokPos;
    // Need to use `readWord1` because '\uXXXX' sequences are allowed
    // here (don't ask).
    var mods = readWord1();
    if (mods && !/^[gmsiy]*$/.test(mods)) raise(start, "Invalid regexp flag");
    return finishToken(_regexp, new RegExp(content, mods));
  }

  // Read an integer in the given radix. Return null if zero digits
  // were read, the integer value otherwise. When `len` is given, this
  // will return `null` unless the integer has exactly `len` digits.

  function readInt(radix, len) {
    var start = tokPos, total = 0;
    for (var i = 0, e = len == null ? Infinity : len; i < e; ++i) {
      var code = input.charCodeAt(tokPos), val;
      if (code >= 97) val = code - 97 + 10; // a
      else if (code >= 65) val = code - 65 + 10; // A
      else if (code >= 48 && code <= 57) val = code - 48; // 0-9
      else val = Infinity;
      if (val >= radix) break;
      ++tokPos;
      total = total * radix + val;
    }
    if (tokPos === start || len != null && tokPos - start !== len) return null;

    return total;
  }

  function readHexNumber() {
    tokPos += 2; // 0x
    var val = readInt(16);
    if (val == null) raise(tokStart + 2, "Expected hexadecimal number");
    if (isIdentifierStart(input.charCodeAt(tokPos))) raise(tokPos, "Identifier directly after number");
    return finishToken(_num, val);
  }

  // Read an integer, octal integer, or floating-point number.

  function readNumber(startsWithDot) {
    var start = tokPos, isFloat = false, octal = input.charCodeAt(tokPos) === 48;
    if (!startsWithDot && readInt(10) === null) raise(start, "Invalid number");
    if (input.charCodeAt(tokPos) === 46) {
      ++tokPos;
      readInt(10);
      isFloat = true;
    }
    var next = input.charCodeAt(tokPos);
    if (next === 69 || next === 101) { // 'eE'
      next = input.charCodeAt(++tokPos);
      if (next === 43 || next === 45) ++tokPos; // '+-'
      if (readInt(10) === null) raise(start, "Invalid number");
      isFloat = true;
    }
    if (isIdentifierStart(input.charCodeAt(tokPos))) raise(tokPos, "Identifier directly after number");

    var str = input.slice(start, tokPos), val;
    if (isFloat) val = parseFloat(str);
    else if (!octal || str.length === 1) val = parseInt(str, 10);
    else if (/[89]/.test(str) || strict) raise(start, "Invalid number");
    else val = parseInt(str, 8);
    return finishToken(_num, val);
  }

  // Read a string value, interpreting backslash-escapes.

  function readString(quote) {
    tokPos++;
    var out = "";
    for (;;) {
      if (tokPos >= inputLen) raise(tokStart, "Unterminated string constant");
      var ch = input.charCodeAt(tokPos);
      if (ch === quote) {
        ++tokPos;
        return finishToken(_string, out);
      }
      if (ch === 92) { // '\'
        ch = input.charCodeAt(++tokPos);
        var octal = /^[0-7]+/.exec(input.slice(tokPos, tokPos + 3));
        if (octal) octal = octal[0];
        while (octal && parseInt(octal, 8) > 255) octal = octal.slice(0, octal.length - 1);
        if (octal === "0") octal = null;
        ++tokPos;
        if (octal) {
          if (strict) raise(tokPos - 2, "Octal literal in strict mode");
          out += String.fromCharCode(parseInt(octal, 8));
          tokPos += octal.length - 1;
        } else {
          switch (ch) {
          case 110: out += "\n"; break; // 'n' -> '\n'
          case 114: out += "\r"; break; // 'r' -> '\r'
          case 120: out += String.fromCharCode(readHexChar(2)); break; // 'x'
          case 117: out += String.fromCharCode(readHexChar(4)); break; // 'u'
          case 85: out += String.fromCharCode(readHexChar(8)); break; // 'U'
          case 116: out += "\t"; break; // 't' -> '\t'
          case 98: out += "\b"; break; // 'b' -> '\b'
          case 118: out += "\u000b"; break; // 'v' -> '\u000b'
          case 102: out += "\f"; break; // 'f' -> '\f'
          case 48: out += "\0"; break; // 0 -> '\0'
          case 13: if (input.charCodeAt(tokPos) === 10) ++tokPos; // '\r\n'
          case 10: // ' \n'
            if (options.locations) { tokLineStart = tokPos; ++tokCurLine; }
            break;
          default: out += String.fromCharCode(ch); break;
          }
        }
      } else {
        if (ch === 13 || ch === 10 || ch === 8232 || ch === 8329) raise(tokStart, "Unterminated string constant");
        out += String.fromCharCode(ch); // '\'
        ++tokPos;
      }
    }
  }

  // Used to read character escape sequences ('\x', '\u', '\U').

  function readHexChar(len) {
    var n = readInt(16, len);
    if (n === null) raise(tokStart, "Bad character escape sequence");
    return n;
  }

  // Used to signal to callers of `readWord1` whether the word
  // contained any escape sequences. This is needed because words with
  // escape sequences must not be interpreted as keywords.

  var containsEsc;

  // Read an identifier, and return it as a string. Sets `containsEsc`
  // to whether the word contained a '\u' escape.
  //
  // Only builds up the word character-by-character when it actually
  // containeds an escape, as a micro-optimization.

  function readWord1() {
    containsEsc = false;
    var word, first = true, start = tokPos;
    for (;;) {
      var ch = input.charCodeAt(tokPos);
      if (isIdentifierChar(ch)) {
        if (containsEsc) word += input.charAt(tokPos);
        ++tokPos;
      } else if (ch === 92) { // "\"
        if (!containsEsc) word = input.slice(start, tokPos);
        containsEsc = true;
        if (input.charCodeAt(++tokPos) != 117) // "u"
          raise(tokPos, "Expecting Unicode escape sequence \\uXXXX");
        ++tokPos;
        var esc = readHexChar(4);
        var escStr = String.fromCharCode(esc);
        if (!escStr) raise(tokPos - 1, "Invalid Unicode escape");
        if (!(first ? isIdentifierStart(esc) : isIdentifierChar(esc)))
          raise(tokPos - 4, "Invalid Unicode escape");
        word += escStr;
      } else {
        break;
      }
      first = false;
    }
    return containsEsc ? word : input.slice(start, tokPos);
  }

  // Read an identifier or keyword token. Will check for reserved
  // words when necessary.

  function readWord() {
    var word = readWord1();
    var type = _name;
    if (!containsEsc) {
      if (isKeyword(word)) type = keywordTypes[word];
      else if (options.forbidReserved &&
               (options.ecmaVersion === 3 ? isReservedWord3 : isReservedWord5)(word) ||
               strict && isStrictReservedWord(word))
        raise(tokStart, "The keyword '" + word + "' is reserved");
    }
    return finishToken(type, word);
  }

  // ## Parser

  // A recursive descent parser operates by defining functions for all
  // syntactic elements, and recursively calling those, each function
  // advancing the input stream and returning an AST node. Precedence
  // of constructs (for example, the fact that `!x[1]` means `!(x[1])`
  // instead of `(!x)[1]` is handled by the fact that the parser
  // function that parses unary prefix operators is called first, and
  // in turn calls the function that parses `[]` subscripts — that
  // way, it'll receive the node for `x[1]` already parsed, and wraps
  // *that* in the unary operator node.
  //
  // Acorn uses an [operator precedence parser][opp] to handle binary
  // operator precedence, because it is much more compact than using
  // the technique outlined above, which uses different, nesting
  // functions to specify precedence, for all of the ten binary
  // precedence levels that JavaScript defines.
  //
  // [opp]: http://en.wikipedia.org/wiki/Operator-precedence_parser

  // ### Parser utilities

  // Continue to the next token.

  function next() {
    lastStart = tokStart;
    lastEnd = tokEnd;
    lastEndLoc = tokEndLoc;
    readToken();
  }

  // Enter strict mode. Re-reads the next token to please pedantic
  // tests ("use strict"; 010; -- should fail).

  function setStrict(strct) {
    strict = strct;
    tokPos = lastEnd;
    while (tokPos < tokLineStart) {
      tokLineStart = input.lastIndexOf("\n", tokLineStart - 2) + 1;
      --tokCurLine;
    }
    skipSpace();
    readToken();
  }

  // Start an AST node, attaching a start offset.

  function node_t() {
    this.type = null;
    this.start = tokStart;
    this.end = null;
  }

  function node_loc_t() {
    this.start = tokStartLoc;
    this.end = null;
    if (sourceFile !== null) this.source = sourceFile;
  }

  function startNode() {
    var node = new node_t();
    if (options.locations)
      node.loc = new node_loc_t();
    if (options.ranges)
      node.range = [tokStart, 0];
    return node;
  }

  // Start a node whose start offset information should be based on
  // the start of another node. For example, a binary operator node is
  // only started after its left-hand side has already been parsed.

  function startNodeFrom(other) {
    var node = new node_t();
    node.start = other.start;
    if (options.locations) {
      node.loc = new node_loc_t();
      node.loc.start = other.loc.start;
    }
    if (options.ranges)
      node.range = [other.range[0], 0];

    return node;
  }

  // Finish an AST node, adding `type` and `end` properties.

  function finishNode(node, type) {
    node.type = type;
    node.end = lastEnd;
    if (options.locations)
      node.loc.end = lastEndLoc;
    if (options.ranges)
      node.range[1] = lastEnd;
    return node;
  }

  // Test whether a statement node is the string literal `"use strict"`.

  function isUseStrict(stmt) {
    return options.ecmaVersion >= 5 && stmt.type === "ExpressionStatement" &&
      stmt.expression.type === "Literal" && stmt.expression.value === "use strict";
  }

  // Predicate that tests whether the next token is of the given
  // type, and if yes, consumes it as a side effect.

  function eat(type) {
    if (tokType === type) {
      next();
      return true;
    }
  }

  // Test whether a semicolon can be inserted at the current position.

  function canInsertSemicolon() {
    return !options.strictSemicolons &&
      (tokType === _eof || tokType === _braceR || newline.test(input.slice(lastEnd, tokStart)));
  }

  // Consume a semicolon, or, failing that, see if we are allowed to
  // pretend that there is a semicolon at this position.

  function semicolon() {
    if (!eat(_semi) && !canInsertSemicolon()) unexpected();
  }

  // Expect a token of a given type. If found, consume it, otherwise,
  // raise an unexpected token error.

  function expect(type) {
    if (tokType === type) next();
    else unexpected();
  }

  // Raise an unexpected token error.

  function unexpected() {
    raise(tokStart, "Unexpected token");
  }

  // Verify that a node is an lval — something that can be assigned
  // to.

  function checkLVal(expr) {
    if (expr.type !== "Identifier" && expr.type !== "MemberExpression")
      raise(expr.start, "Assigning to rvalue");
    if (strict && expr.type === "Identifier" && isStrictBadIdWord(expr.name))
      raise(expr.start, "Assigning to " + expr.name + " in strict mode");
  }

  // ### Statement parsing

  // Parse a program. Initializes the parser, reads any number of
  // statements, and wraps them in a Program node.  Optionally takes a
  // `program` argument.  If present, the statements will be appended
  // to its body instead of creating a new node.

  function parseTopLevel(program) {
    lastStart = lastEnd = tokPos;
    if (options.locations) lastEndLoc = new line_loc_t;
    inFunction = strict = null;
    labels = [];
    readToken();

    var node = program || startNode(), first = true;
    if (!program) node.body = [];
    while (tokType !== _eof) {
      var stmt = parseStatement();
      node.body.push(stmt);
      if (first && isUseStrict(stmt)) setStrict(true);
      first = false;
    }
    return finishNode(node, "Program");
  }

  var loopLabel = {kind: "loop"}, switchLabel = {kind: "switch"};

  // Parse a single statement.
  //
  // If expecting a statement and finding a slash operator, parse a
  // regular expression literal. This is to handle cases like
  // `if (foo) /blah/.exec(foo);`, where looking at the previous token
  // does not help.

  function parseStatement() {
    if (tokType === _slash)
      readToken(true);

    var starttype = tokType, node = startNode();

    // Most types of statements are recognized by the keyword they
    // start with. Many are trivial to parse, some require a bit of
    // complexity.

    switch (starttype) {
    case _break: case _continue:
      next();
      var isBreak = starttype === _break;
      if (eat(_semi) || canInsertSemicolon()) node.label = null;
      else if (tokType !== _name) unexpected();
      else {
        node.label = parseIdent();
        semicolon();
      }

      // Verify that there is an actual destination to break or
      // continue to.
      for (var i = 0; i < labels.length; ++i) {
        var lab = labels[i];
        if (node.label == null || lab.name === node.label.name) {
          if (lab.kind != null && (isBreak || lab.kind === "loop")) break;
          if (node.label && isBreak) break;
        }
      }
      if (i === labels.length) raise(node.start, "Unsyntactic " + starttype.keyword);
      return finishNode(node, isBreak ? "BreakStatement" : "ContinueStatement");

    case _debugger:
      next();
      semicolon();
      return finishNode(node, "DebuggerStatement");

    case _do:
      next();
      labels.push(loopLabel);
      node.body = parseStatement();
      labels.pop();
      expect(_while);
      node.test = parseParenExpression();
      semicolon();
      return finishNode(node, "DoWhileStatement");

      // Disambiguating between a `for` and a `for`/`in` loop is
      // non-trivial. Basically, we have to parse the init `var`
      // statement or expression, disallowing the `in` operator (see
      // the second parameter to `parseExpression`), and then check
      // whether the next token is `in`. When there is no init part
      // (semicolon immediately after the opening parenthesis), it is
      // a regular `for` loop.

    case _for:
      next();
      labels.push(loopLabel);
      expect(_parenL);
      if (tokType === _semi) return parseFor(node, null);
      if (tokType === _var) {
        var init = startNode();
        next();
        parseVar(init, true);
        if (init.declarations.length === 1 && eat(_in))
          return parseForIn(node, init);
        return parseFor(node, init);
      }
      var init = parseExpression(false, true);
      if (eat(_in)) {checkLVal(init); return parseForIn(node, init);}
      return parseFor(node, init);

    case _function:
      next();
      return parseFunction(node, true);

    case _if:
      next();
      node.test = parseParenExpression();
      node.consequent = parseStatement();
      node.alternate = eat(_else) ? parseStatement() : null;
      return finishNode(node, "IfStatement");

    case _return:
      if (!inFunction) raise(tokStart, "'return' outside of function");
      next();

      // In `return` (and `break`/`continue`), the keywords with
      // optional arguments, we eagerly look for a semicolon or the
      // possibility to insert one.

      if (eat(_semi) || canInsertSemicolon()) node.argument = null;
      else { node.argument = parseExpression(); semicolon(); }
      return finishNode(node, "ReturnStatement");

    case _switch:
      next();
      node.discriminant = parseParenExpression();
      node.cases = [];
      expect(_braceL);
      labels.push(switchLabel);

      // Statements under must be grouped (by label) in SwitchCase
      // nodes. `cur` is used to keep the node that we are currently
      // adding statements to.

      for (var cur, sawDefault; tokType != _braceR;) {
        if (tokType === _case || tokType === _default) {
          var isCase = tokType === _case;
          if (cur) finishNode(cur, "SwitchCase");
          node.cases.push(cur = startNode());
          cur.consequent = [];
          next();
          if (isCase) cur.test = parseExpression();
          else {
            if (sawDefault) raise(lastStart, "Multiple default clauses"); sawDefault = true;
            cur.test = null;
          }
          expect(_colon);
        } else {
          if (!cur) unexpected();
          cur.consequent.push(parseStatement());
        }
      }
      if (cur) finishNode(cur, "SwitchCase");
      next(); // Closing brace
      labels.pop();
      return finishNode(node, "SwitchStatement");

    case _throw:
      next();
      if (newline.test(input.slice(lastEnd, tokStart)))
        raise(lastEnd, "Illegal newline after throw");
      node.argument = parseExpression();
      semicolon();
      return finishNode(node, "ThrowStatement");

    case _try:
      next();
      node.block = parseBlock();
      node.handler = null;
      if (tokType === _catch) {
        var clause = startNode();
        next();
        expect(_parenL);
        clause.param = parseIdent();
        if (strict && isStrictBadIdWord(clause.param.name))
          raise(clause.param.start, "Binding " + clause.param.name + " in strict mode");
        expect(_parenR);
        clause.guard = null;
        clause.body = parseBlock();
        node.handler = finishNode(clause, "CatchClause");
      }
      node.finalizer = eat(_finally) ? parseBlock() : null;
      if (!node.handler && !node.finalizer)
        raise(node.start, "Missing catch or finally clause");
      return finishNode(node, "TryStatement");

    case _var:
      next();
      node = parseVar(node);
      semicolon();
      return node;

    case _while:
      next();
      node.test = parseParenExpression();
      labels.push(loopLabel);
      node.body = parseStatement();
      labels.pop();
      return finishNode(node, "WhileStatement");

    case _with:
      if (strict) raise(tokStart, "'with' in strict mode");
      next();
      node.object = parseParenExpression();
      node.body = parseStatement();
      return finishNode(node, "WithStatement");

    case _braceL:
      return parseBlock();

    case _semi:
      next();
      return finishNode(node, "EmptyStatement");

      // If the statement does not start with a statement keyword or a
      // brace, it's an ExpressionStatement or LabeledStatement. We
      // simply start parsing an expression, and afterwards, if the
      // next token is a colon and the expression was a simple
      // Identifier node, we switch to interpreting it as a label.

    default:
      var maybeName = tokVal, expr = parseExpression();
      if (starttype === _name && expr.type === "Identifier" && eat(_colon)) {
        for (var i = 0; i < labels.length; ++i)
          if (labels[i].name === maybeName) raise(expr.start, "Label '" + maybeName + "' is already declared");
        var kind = tokType.isLoop ? "loop" : tokType === _switch ? "switch" : null;
        labels.push({name: maybeName, kind: kind});
        node.body = parseStatement();
        labels.pop();
        node.label = expr;
        return finishNode(node, "LabeledStatement");
      } else {
        node.expression = expr;
        semicolon();
        return finishNode(node, "ExpressionStatement");
      }
    }
  }

  // Used for constructs like `switch` and `if` that insist on
  // parentheses around their expression.

  function parseParenExpression() {
    expect(_parenL);
    var val = parseExpression();
    expect(_parenR);
    return val;
  }

  // Parse a semicolon-enclosed block of statements, handling `"use
  // strict"` declarations when `allowStrict` is true (used for
  // function bodies).

  function parseBlock(allowStrict) {
    var node = startNode(), first = true, strict = false, oldStrict;
    node.body = [];
    expect(_braceL);
    while (!eat(_braceR)) {
      var stmt = parseStatement();
      node.body.push(stmt);
      if (first && isUseStrict(stmt)) {
        oldStrict = strict;
        setStrict(strict = true);
      }
      first = false
    }
    if (strict && !oldStrict) setStrict(false);
    return finishNode(node, "BlockStatement");
  }

  // Parse a regular `for` loop. The disambiguation code in
  // `parseStatement` will already have parsed the init statement or
  // expression.

  function parseFor(node, init) {
    node.init = init;
    expect(_semi);
    node.test = tokType === _semi ? null : parseExpression();
    expect(_semi);
    node.update = tokType === _parenR ? null : parseExpression();
    expect(_parenR);
    node.body = parseStatement();
    labels.pop();
    return finishNode(node, "ForStatement");
  }

  // Parse a `for`/`in` loop.

  function parseForIn(node, init) {
    node.left = init;
    node.right = parseExpression();
    expect(_parenR);
    node.body = parseStatement();
    labels.pop();
    return finishNode(node, "ForInStatement");
  }

  // Parse a list of variable declarations.

  function parseVar(node, noIn) {
    node.declarations = [];
    node.kind = "var";
    for (;;) {
      var decl = startNode();
      decl.id = parseIdent();
      if (strict && isStrictBadIdWord(decl.id.name))
        raise(decl.id.start, "Binding " + decl.id.name + " in strict mode");
      decl.init = eat(_eq) ? parseExpression(true, noIn) : null;
      node.declarations.push(finishNode(decl, "VariableDeclarator"));
      if (!eat(_comma)) break;
    }
    return finishNode(node, "VariableDeclaration");
  }

  // ### Expression parsing

  // These nest, from the most general expression type at the top to
  // 'atomic', nondivisible expression types at the bottom. Most of
  // the functions will simply let the function(s) below them parse,
  // and, *if* the syntactic construct they handle is present, wrap
  // the AST node that the inner parser gave them in another node.

  // Parse a full expression. The arguments are used to forbid comma
  // sequences (in argument lists, array literals, or object literals)
  // or the `in` operator (in for loops initalization expressions).

  function parseExpression(noComma, noIn) {
    var expr = parseMaybeAssign(noIn);
    if (!noComma && tokType === _comma) {
      var node = startNodeFrom(expr);
      node.expressions = [expr];
      while (eat(_comma)) node.expressions.push(parseMaybeAssign(noIn));
      return finishNode(node, "SequenceExpression");
    }
    return expr;
  }

  // Parse an assignment expression. This includes applications of
  // operators like `+=`.

  function parseMaybeAssign(noIn) {
    var left = parseMaybeConditional(noIn);
    if (tokType.isAssign) {
      var node = startNodeFrom(left);
      node.operator = tokVal;
      node.left = left;
      next();
      node.right = parseMaybeAssign(noIn);
      checkLVal(left);
      return finishNode(node, "AssignmentExpression");
    }
    return left;
  }

  // Parse a ternary conditional (`?:`) operator.

  function parseMaybeConditional(noIn) {
    var expr = parseExprOps(noIn);
    if (eat(_question)) {
      var node = startNodeFrom(expr);
      node.test = expr;
      node.consequent = parseExpression(true);
      expect(_colon);
      node.alternate = parseExpression(true, noIn);
      return finishNode(node, "ConditionalExpression");
    }
    return expr;
  }

  // Start the precedence parser.

  function parseExprOps(noIn) {
    return parseExprOp(parseMaybeUnary(noIn), -1, noIn);
  }

  // Parse binary operators with the operator precedence parsing
  // algorithm. `left` is the left-hand side of the operator.
  // `minPrec` provides context that allows the function to stop and
  // defer further parser to one of its callers when it encounters an
  // operator that has a lower precedence than the set it is parsing.

  function parseExprOp(left, minPrec, noIn) {
    var prec = tokType.binop;
    if (prec != null && (!noIn || tokType !== _in)) {
      if (prec > minPrec) {
        var node = startNodeFrom(left);
        node.left = left;
        node.operator = tokVal;
        next();
        node.right = parseExprOp(parseMaybeUnary(noIn), prec, noIn);
        var node = finishNode(node, /&&|\|\|/.test(node.operator) ? "LogicalExpression" : "BinaryExpression");
        return parseExprOp(node, minPrec, noIn);
      }
    }
    return left;
  }

  // Parse unary operators, both prefix and postfix.

  function parseMaybeUnary(noIn) {
    if (tokType.prefix) {
      var node = startNode(), update = tokType.isUpdate;
      node.operator = tokVal;
      node.prefix = true;
      next();
      node.argument = parseMaybeUnary(noIn);
      if (update) checkLVal(node.argument);
      else if (strict && node.operator === "delete" &&
               node.argument.type === "Identifier")
        raise(node.start, "Deleting local variable in strict mode");
      return finishNode(node, update ? "UpdateExpression" : "UnaryExpression");
    }
    var expr = parseExprSubscripts();
    while (tokType.postfix && !canInsertSemicolon()) {
      var node = startNodeFrom(expr);
      node.operator = tokVal;
      node.prefix = false;
      node.argument = expr;
      checkLVal(expr);
      next();
      expr = finishNode(node, "UpdateExpression");
    }
    return expr;
  }

  // Parse call, dot, and `[]`-subscript expressions.

  function parseExprSubscripts() {
    return parseSubscripts(parseExprAtom());
  }

  function parseSubscripts(base, noCalls) {
    if (eat(_dot)) {
      var node = startNodeFrom(base);
      node.object = base;
      node.property = parseIdent(true);
      node.computed = false;
      return parseSubscripts(finishNode(node, "MemberExpression"), noCalls);
    } else if (eat(_bracketL)) {
      var node = startNodeFrom(base);
      node.object = base;
      node.property = parseExpression();
      node.computed = true;
      expect(_bracketR);
      return parseSubscripts(finishNode(node, "MemberExpression"), noCalls);
    } else if (!noCalls && eat(_parenL)) {
      var node = startNodeFrom(base);
      node.callee = base;
      node.arguments = parseExprList(_parenR, false);
      return parseSubscripts(finishNode(node, "CallExpression"), noCalls);
    } else return base;
  }

  // Parse an atomic expression — either a single token that is an
  // expression, an expression started by a keyword like `function` or
  // `new`, or an expression wrapped in punctuation like `()`, `[]`,
  // or `{}`.

  function parseExprAtom() {
    switch (tokType) {
    case _this:
      var node = startNode();
      next();
      return finishNode(node, "ThisExpression");
    case _name:
      return parseIdent();
    case _num: case _string: case _regexp:
      var node = startNode();
      node.value = tokVal;
      node.raw = input.slice(tokStart, tokEnd);
      next();
      return finishNode(node, "Literal");

    case _null: case _true: case _false:
      var node = startNode();
      node.value = tokType.atomValue;
      node.raw = tokType.keyword
      next();
      return finishNode(node, "Literal");

    case _parenL:
      var tokStartLoc1 = tokStartLoc, tokStart1 = tokStart;
      next();
      var val = parseExpression();
      val.start = tokStart1;
      val.end = tokEnd;
      if (options.locations) {
        val.loc.start = tokStartLoc1;
        val.loc.end = tokEndLoc;
      }
      if (options.ranges)
        val.range = [tokStart1, tokEnd];
      expect(_parenR);
      return val;

    case _bracketL:
      var node = startNode();
      next();
      node.elements = parseExprList(_bracketR, true, true);
      return finishNode(node, "ArrayExpression");

    case _braceL:
      return parseObj();

    case _function:
      var node = startNode();
      next();
      return parseFunction(node, false);

    case _new:
      return parseNew();

    default:
      unexpected();
    }
  }

  // New's precedence is slightly tricky. It must allow its argument
  // to be a `[]` or dot subscript expression, but not a call — at
  // least, not without wrapping it in parentheses. Thus, it uses the

  function parseNew() {
    var node = startNode();
    next();
    node.callee = parseSubscripts(parseExprAtom(), true);
    if (eat(_parenL)) node.arguments = parseExprList(_parenR, false);
    else node.arguments = [];
    return finishNode(node, "NewExpression");
  }

  // Parse an object literal.

  function parseObj() {
    var node = startNode(), first = true, sawGetSet = false;
    node.properties = [];
    next();
    while (!eat(_braceR)) {
      if (!first) {
        expect(_comma);
        if (options.allowTrailingCommas && eat(_braceR)) break;
      } else first = false;

      var prop = {key: parsePropertyName()}, isGetSet = false, kind;
      if (eat(_colon)) {
        prop.value = parseExpression(true);
        kind = prop.kind = "init";
      } else if (options.ecmaVersion >= 5 && prop.key.type === "Identifier" &&
                 (prop.key.name === "get" || prop.key.name === "set")) {
        isGetSet = sawGetSet = true;
        kind = prop.kind = prop.key.name;
        prop.key = parsePropertyName();
        if (tokType !== _parenL) unexpected();
        prop.value = parseFunction(startNode(), false);
      } else unexpected();

      // getters and setters are not allowed to clash — either with
      // each other or with an init property — and in strict mode,
      // init properties are also not allowed to be repeated.

      if (prop.key.type === "Identifier" && (strict || sawGetSet)) {
        for (var i = 0; i < node.properties.length; ++i) {
          var other = node.properties[i];
          if (other.key.name === prop.key.name) {
            var conflict = kind == other.kind || isGetSet && other.kind === "init" ||
              kind === "init" && (other.kind === "get" || other.kind === "set");
            if (conflict && !strict && kind === "init" && other.kind === "init") conflict = false;
            if (conflict) raise(prop.key.start, "Redefinition of property");
          }
        }
      }
      node.properties.push(prop);
    }
    return finishNode(node, "ObjectExpression");
  }

  function parsePropertyName() {
    if (tokType === _num || tokType === _string) return parseExprAtom();
    return parseIdent(true);
  }

  // Parse a function declaration or literal (depending on the
  // `isStatement` parameter).

  function parseFunction(node, isStatement) {
    if (tokType === _name) node.id = parseIdent();
    else if (isStatement) unexpected();
    else node.id = null;
    node.params = [];
    var first = true;
    expect(_parenL);
    while (!eat(_parenR)) {
      if (!first) expect(_comma); else first = false;
      node.params.push(parseIdent());
    }

    // Start a new scope with regard to labels and the `inFunction`
    // flag (restore them to their old value afterwards).
    var oldInFunc = inFunction, oldLabels = labels;
    inFunction = true; labels = [];
    node.body = parseBlock(true);
    inFunction = oldInFunc; labels = oldLabels;

    // If this is a strict mode function, verify that argument names
    // are not repeated, and it does not try to bind the words `eval`
    // or `arguments`.
    if (strict || node.body.body.length && isUseStrict(node.body.body[0])) {
      for (var i = node.id ? -1 : 0; i < node.params.length; ++i) {
        var id = i < 0 ? node.id : node.params[i];
        if (isStrictReservedWord(id.name) || isStrictBadIdWord(id.name))
          raise(id.start, "Defining '" + id.name + "' in strict mode");
        if (i >= 0) for (var j = 0; j < i; ++j) if (id.name === node.params[j].name)
          raise(id.start, "Argument name clash in strict mode");
      }
    }

    return finishNode(node, isStatement ? "FunctionDeclaration" : "FunctionExpression");
  }

  // Parses a comma-separated list of expressions, and returns them as
  // an array. `close` is the token type that ends the list, and
  // `allowEmpty` can be turned on to allow subsequent commas with
  // nothing in between them to be parsed as `null` (which is needed
  // for array literals).

  function parseExprList(close, allowTrailingComma, allowEmpty) {
    var elts = [], first = true;
    while (!eat(close)) {
      if (!first) {
        expect(_comma);
        if (allowTrailingComma && options.allowTrailingCommas && eat(close)) break;
      } else first = false;

      if (allowEmpty && tokType === _comma) elts.push(null);
      else elts.push(parseExpression(true));
    }
    return elts;
  }

  // Parse the next token as an identifier. If `liberal` is true (used
  // when parsing properties), it will also convert keywords into
  // identifiers.

  function parseIdent(liberal) {
    var node = startNode();
    node.name = tokType === _name ? tokVal : (liberal && !options.forbidReserved && tokType.keyword) || unexpected();
    next();
    return finishNode(node, "Identifier");
  }

});
;
 ;
// AST walker module for Mozilla Parser API compatible trees

(function(mod) {
  if (typeof exports == "object" && typeof module == "object") return mod(exports); // CommonJS
  if (typeof define == "function" && define.amd) return define(["exports"], mod); // AMD
  mod((this.acorn || (this.acorn = {})).walk = {}); // Plain browser env
})(function(exports) {
  "use strict";

  // A simple walk is one where you simply specify callbacks to be
  // called on specific nodes. The last two arguments are optional. A
  // simple use would be
  //
  //     walk.simple(myTree, {
  //         Expression: function(node) { ... }
  //     });
  //
  // to do something with all expressions. All Parser API node types
  // can be used to identify node types, as well as Expression,
  // Statement, and ScopeBody, which denote categories of nodes.
  //
  // The base argument can be used to pass a custom (recursive)
  // walker, and state can be used to give this walked an initial
  // state.
  exports.simple = function(node, visitors, base, state) {
    if (!base) base = exports.base;
    function c(node, st, override) {
      var type = override || node.type, found = visitors[type];
      base[type](node, st, c);
      if (found) found(node, st);
    }
    c(node, state);
  };

  // A recursive walk is one where your functions override the default
  // walkers. They can modify and replace the state parameter that's
  // threaded through the walk, and can opt how and whether to walk
  // their child nodes (by calling their third argument on these
  // nodes).
  exports.recursive = function(node, state, funcs, base) {
    var visitor = funcs ? exports.make(funcs, base) : base;
    function c(node, st, override) {
      visitor[override || node.type](node, st, c);
    }
    c(node, state);
  };

  function makeTest(test) {
    if (typeof test == "string")
      return function(type) { return type == test; };
    else if (!test)
      return function() { return true; };
    else
      return test;
  }

  function Found(node, state) { this.node = node; this.state = state; }

  // Find a node with a given start, end, and type (all are optional,
  // null can be used as wildcard). Returns a {node, state} object, or
  // undefined when it doesn't find a matching node.
  exports.findNodeAt = function(node, start, end, test, base, state) {
    test = makeTest(test);
    try {
      if (!base) base = exports.base;
      var c = function(node, st, override) {
        var type = override || node.type;
        if ((start == null || node.start <= start) &&
            (end == null || node.end >= end))
          base[type](node, st, c);
        if (test(type, node) &&
            (start == null || node.start == start) &&
            (end == null || node.end == end))
          throw new Found(node, st);
      };
      c(node, state);
    } catch (e) {
      if (e instanceof Found) return e;
      throw e;
    }
  };

  // Find the innermost node of a given type that contains the given
  // position. Interface similar to findNodeAt.
  exports.findNodeAround = function(node, pos, test, base, state) {
    test = makeTest(test);
    try {
      if (!base) base = exports.base;
      var c = function(node, st, override) {
        var type = override || node.type;
        if (node.start > pos || node.end < pos) return;
        base[type](node, st, c);
        if (test(type, node)) throw new Found(node, st);
      };
      c(node, state);
    } catch (e) {
      if (e instanceof Found) return e;
      throw e;
    }
  };

  // Find the outermost matching node after a given position.
  exports.findNodeAfter = function(node, pos, test, base, state) {
    test = makeTest(test);
    try {
      if (!base) base = exports.base;
      var c = function(node, st, override) {
        if (node.end < pos) return;
        var type = override || node.type;
        if (node.start >= pos && test(type, node)) throw new Found(node, st);
        base[type](node, st, c);
      };
      c(node, state);
    } catch (e) {
      if (e instanceof Found) return e;
      throw e;
    }
  };

  // Find the outermost matching node before a given position.
  exports.findNodeBefore = function(node, pos, test, base, state) {
    test = makeTest(test);
    if (!base) base = exports.base;
    var max;
    var c = function(node, st, override) {
      if (node.start > pos) return;
      var type = override || node.type;
      if (node.end <= pos && (!max || max.node.end < node.end) && test(type, node))
        max = new Found(node, st);
      base[type](node, st, c);
    };
    c(node, state);
    return max;
  };

  // Used to create a custom walker. Will fill in all missing node
  // type properties with the defaults.
  exports.make = function(funcs, base) {
    if (!base) base = exports.base;
    var visitor = {};
    for (var type in base) visitor[type] = base[type];
    for (var type in funcs) visitor[type] = funcs[type];
    return visitor;
  };

  function skipThrough(node, st, c) { c(node, st); }
  function ignore(node, st, c) {}

  // Node walkers.

  var base = exports.base = {};
  base.Program = base.BlockStatement = function(node, st, c) {
    for (var i = 0; i < node.body.length; ++i)
      c(node.body[i], st, "Statement");
  };
  base.Statement = skipThrough;
  base.EmptyStatement = ignore;
  base.ExpressionStatement = function(node, st, c) {
    c(node.expression, st, "Expression");
  };
  base.IfStatement = function(node, st, c) {
    c(node.test, st, "Expression");
    c(node.consequent, st, "Statement");
    if (node.alternate) c(node.alternate, st, "Statement");
  };
  base.LabeledStatement = function(node, st, c) {
    c(node.body, st, "Statement");
  };
  base.BreakStatement = base.ContinueStatement = ignore;
  base.WithStatement = function(node, st, c) {
    c(node.object, st, "Expression");
    c(node.body, st, "Statement");
  };
  base.SwitchStatement = function(node, st, c) {
    c(node.discriminant, st, "Expression");
    for (var i = 0; i < node.cases.length; ++i) {
      var cs = node.cases[i];
      if (cs.test) c(cs.test, st, "Expression");
      for (var j = 0; j < cs.consequent.length; ++j)
        c(cs.consequent[j], st, "Statement");
    }
  };
  base.ReturnStatement = function(node, st, c) {
    if (node.argument) c(node.argument, st, "Expression");
  };
  base.ThrowStatement = function(node, st, c) {
    c(node.argument, st, "Expression");
  };
  base.TryStatement = function(node, st, c) {
    c(node.block, st, "Statement");
    if (node.handler) c(node.handler.body, st, "ScopeBody");
    if (node.finalizer) c(node.finalizer, st, "Statement");
  };
  base.WhileStatement = function(node, st, c) {
    c(node.test, st, "Expression");
    c(node.body, st, "Statement");
  };
  base.DoWhileStatement = base.WhileStatement;
  base.ForStatement = function(node, st, c) {
    if (node.init) c(node.init, st, "ForInit");
    if (node.test) c(node.test, st, "Expression");
    if (node.update) c(node.update, st, "Expression");
    c(node.body, st, "Statement");
  };
  base.ForInStatement = function(node, st, c) {
    c(node.left, st, "ForInit");
    c(node.right, st, "Expression");
    c(node.body, st, "Statement");
  };
  base.ForInit = function(node, st, c) {
    if (node.type == "VariableDeclaration") c(node, st);
    else c(node, st, "Expression");
  };
  base.DebuggerStatement = ignore;

  base.FunctionDeclaration = function(node, st, c) {
    c(node, st, "Function");
  };
  base.VariableDeclaration = function(node, st, c) {
    for (var i = 0; i < node.declarations.length; ++i) {
      var decl = node.declarations[i];
      if (decl.init) c(decl.init, st, "Expression");
    }
  };

  base.Function = function(node, st, c) {
    c(node.body, st, "ScopeBody");
  };
  base.ScopeBody = function(node, st, c) {
    c(node, st, "Statement");
  };

  base.Expression = skipThrough;
  base.ThisExpression = ignore;
  base.ArrayExpression = function(node, st, c) {
    for (var i = 0; i < node.elements.length; ++i) {
      var elt = node.elements[i];
      if (elt) c(elt, st, "Expression");
    }
  };
  base.ObjectExpression = function(node, st, c) {
    for (var i = 0; i < node.properties.length; ++i)
      c(node.properties[i].value, st, "Expression");
  };
  base.FunctionExpression = base.FunctionDeclaration;
  base.SequenceExpression = function(node, st, c) {
    for (var i = 0; i < node.expressions.length; ++i)
      c(node.expressions[i], st, "Expression");
  };
  base.UnaryExpression = base.UpdateExpression = function(node, st, c) {
    c(node.argument, st, "Expression");
  };
  base.BinaryExpression = base.AssignmentExpression = base.LogicalExpression = function(node, st, c) {
    c(node.left, st, "Expression");
    c(node.right, st, "Expression");
  };
  base.ConditionalExpression = function(node, st, c) {
    c(node.test, st, "Expression");
    c(node.consequent, st, "Expression");
    c(node.alternate, st, "Expression");
  };
  base.NewExpression = base.CallExpression = function(node, st, c) {
    c(node.callee, st, "Expression");
    if (node.arguments) for (var i = 0; i < node.arguments.length; ++i)
      c(node.arguments[i], st, "Expression");
  };
  base.MemberExpression = function(node, st, c) {
    c(node.object, st, "Expression");
    if (node.computed) c(node.property, st, "Expression");
  };
  base.Identifier = base.Literal = ignore;

  // A custom walker that keeps track of the scope chain and the
  // variables defined in it.
  function makeScope(prev) {
    return {vars: Object.create(null), prev: prev};
  }
  exports.scopeVisitor = exports.make({
    Function: function(node, scope, c) {
      var inner = makeScope(scope);
      for (var i = 0; i < node.params.length; ++i)
        inner.vars[node.params[i].name] = {type: "argument", node: node.params[i]};
      if (node.id) {
        var decl = node.type == "FunctionDeclaration";
        (decl ? scope : inner).vars[node.id.name] =
          {type: decl ? "function" : "function name", node: node.id};
      }
      c(node.body, inner, "ScopeBody");
    },
    TryStatement: function(node, scope, c) {
      c(node.block, scope, "Statement");
      if (node.handler) {
        var inner = makeScope(scope);
        inner.vars[node.handler.param.name] = {type: "catch clause", node: node.handler.param};
        c(node.handler.body, inner, "ScopeBody");
      }
      if (node.finalizer) c(node.finalizer, scope, "Statement");
    },
    VariableDeclaration: function(node, scope, c) {
      for (var i = 0; i < node.declarations.length; ++i) {
        var decl = node.declarations[i];
        scope.vars[decl.id.name] = {type: "var", node: decl.id};
        if (decl.init) c(decl.init, scope, "Expression");
      }
    }
  });

});
;
// Generated by browserify
(function(){var require = function (file, cwd) {
    var resolved = require.resolve(file, cwd || '/');
    var mod = require.modules[resolved];
    if (!mod) throw new Error(
        'Failed to resolve module ' + file + ', tried ' + resolved
    );
    var cached = require.cache[resolved];
    var res = cached? cached.exports : mod();
    return res;
};

require.paths = [];
require.modules = {};
require.cache = {};
require.extensions = [".js",".coffee",".json"];

require._core = {
    'assert': true,
    'events': true,
    'fs': true,
    'path': true,
    'vm': true
};

require.resolve = (function () {
    return function (x, cwd) {
        if (!cwd) cwd = '/';
        
        if (require._core[x]) return x;
        var path = require.modules.path();
        cwd = path.resolve('/', cwd);
        var y = cwd || '/';
        
        if (x.match(/^(?:\.\.?\/|\/)/)) {
            var m = loadAsFileSync(path.resolve(y, x))
                || loadAsDirectorySync(path.resolve(y, x));
            if (m) return m;
        }
        
        var n = loadNodeModulesSync(x, y);
        if (n) return n;
        
        throw new Error("Cannot find module '" + x + "'");
        
        function loadAsFileSync (x) {
            x = path.normalize(x);
            if (require.modules[x]) {
                return x;
            }
            
            for (var i = 0; i < require.extensions.length; i++) {
                var ext = require.extensions[i];
                if (require.modules[x + ext]) return x + ext;
            }
        }
        
        function loadAsDirectorySync (x) {
            x = x.replace(/\/+$/, '');
            var pkgfile = path.normalize(x + '/package.json');
            if (require.modules[pkgfile]) {
                var pkg = require.modules[pkgfile]();
                var b = pkg.browserify;
                if (typeof b === 'object' && b.main) {
                    var m = loadAsFileSync(path.resolve(x, b.main));
                    if (m) return m;
                }
                else if (typeof b === 'string') {
                    var m = loadAsFileSync(path.resolve(x, b));
                    if (m) return m;
                }
                else if (pkg.main) {
                    var m = loadAsFileSync(path.resolve(x, pkg.main));
                    if (m) return m;
                }
            }
            
            return loadAsFileSync(x + '/index');
        }
        
        function loadNodeModulesSync (x, start) {
            var dirs = nodeModulesPathsSync(start);
            for (var i = 0; i < dirs.length; i++) {
                var dir = dirs[i];
                var m = loadAsFileSync(dir + '/' + x);
                if (m) return m;
                var n = loadAsDirectorySync(dir + '/' + x);
                if (n) return n;
            }
            
            var m = loadAsFileSync(x);
            if (m) return m;
        }
        
        function nodeModulesPathsSync (start) {
            var parts;
            if (start === '/') parts = [ '' ];
            else parts = path.normalize(start).split('/');
            
            var dirs = [];
            for (var i = parts.length - 1; i >= 0; i--) {
                if (parts[i] === 'node_modules') continue;
                var dir = parts.slice(0, i + 1).join('/') + '/node_modules';
                dirs.push(dir);
            }
            
            return dirs;
        }
    };
})();

require.alias = function (from, to) {
    var path = require.modules.path();
    var res = null;
    try {
        res = require.resolve(from + '/package.json', '/');
    }
    catch (err) {
        res = require.resolve(from, '/');
    }
    var basedir = path.dirname(res);
    
    var keys = (Object.keys || function (obj) {
        var res = [];
        for (var key in obj) res.push(key);
        return res;
    })(require.modules);
    
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        if (key.slice(0, basedir.length + 1) === basedir + '/') {
            var f = key.slice(basedir.length);
            require.modules[to + f] = require.modules[basedir + f];
        }
        else if (key === basedir) {
            require.modules[to] = require.modules[basedir];
        }
    }
};

(function () {
    var process = {};
    var global = typeof window !== 'undefined' ? window : {};
    var definedProcess = false;
    
    require.define = function (filename, fn) {
        if (!definedProcess && require.modules.__browserify_process) {
            process = require.modules.__browserify_process();
            definedProcess = true;
        }
        
        var dirname = require._core[filename]
            ? ''
            : require.modules.path().dirname(filename)
        ;
        
        var require_ = function (file) {
            var requiredModule = require(file, dirname);
            var cached = require.cache[require.resolve(file, dirname)];

            if (cached && cached.parent === null) {
                cached.parent = module_;
            }

            return requiredModule;
        };
        require_.resolve = function (name) {
            return require.resolve(name, dirname);
        };
        require_.modules = require.modules;
        require_.define = require.define;
        require_.cache = require.cache;
        var module_ = {
            id : filename,
            filename: filename,
            exports : {},
            loaded : false,
            parent: null
        };
        
        require.modules[filename] = function () {
            require.cache[filename] = module_;
            fn.call(
                module_.exports,
                require_,
                module_,
                module_.exports,
                dirname,
                filename,
                process,
                global
            );
            module_.loaded = true;
            return module_.exports;
        };
    };
})();


require.define("path",function(require,module,exports,__dirname,__filename,process,global){function filter (xs, fn) {
    var res = [];
    for (var i = 0; i < xs.length; i++) {
        if (fn(xs[i], i, xs)) res.push(xs[i]);
    }
    return res;
}

// resolves . and .. elements in a path array with directory names there
// must be no slashes, empty elements, or device names (c:\) in the array
// (so also no leading and trailing slashes - it does not distinguish
// relative and absolute paths)
function normalizeArray(parts, allowAboveRoot) {
  // if the path tries to go above the root, `up` ends up > 0
  var up = 0;
  for (var i = parts.length; i >= 0; i--) {
    var last = parts[i];
    if (last == '.') {
      parts.splice(i, 1);
    } else if (last === '..') {
      parts.splice(i, 1);
      up++;
    } else if (up) {
      parts.splice(i, 1);
      up--;
    }
  }

  // if the path is allowed to go above the root, restore leading ..s
  if (allowAboveRoot) {
    for (; up--; up) {
      parts.unshift('..');
    }
  }

  return parts;
}

// Regex to split a filename into [*, dir, basename, ext]
// posix version
var splitPathRe = /^(.+\/(?!$)|\/)?((?:.+?)?(\.[^.]*)?)$/;

// path.resolve([from ...], to)
// posix version
exports.resolve = function() {
var resolvedPath = '',
    resolvedAbsolute = false;

for (var i = arguments.length; i >= -1 && !resolvedAbsolute; i--) {
  var path = (i >= 0)
      ? arguments[i]
      : process.cwd();

  // Skip empty and invalid entries
  if (typeof path !== 'string' || !path) {
    continue;
  }

  resolvedPath = path + '/' + resolvedPath;
  resolvedAbsolute = path.charAt(0) === '/';
}

// At this point the path should be resolved to a full absolute path, but
// handle relative paths to be safe (might happen when process.cwd() fails)

// Normalize the path
resolvedPath = normalizeArray(filter(resolvedPath.split('/'), function(p) {
    return !!p;
  }), !resolvedAbsolute).join('/');

  return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
};

// path.normalize(path)
// posix version
exports.normalize = function(path) {
var isAbsolute = path.charAt(0) === '/',
    trailingSlash = path.slice(-1) === '/';

// Normalize the path
path = normalizeArray(filter(path.split('/'), function(p) {
    return !!p;
  }), !isAbsolute).join('/');

  if (!path && !isAbsolute) {
    path = '.';
  }
  if (path && trailingSlash) {
    path += '/';
  }
  
  return (isAbsolute ? '/' : '') + path;
};


// posix version
exports.join = function() {
  var paths = Array.prototype.slice.call(arguments, 0);
  return exports.normalize(filter(paths, function(p, index) {
    return p && typeof p === 'string';
  }).join('/'));
};


exports.dirname = function(path) {
  var dir = splitPathRe.exec(path)[1] || '';
  var isWindows = false;
  if (!dir) {
    // No dirname
    return '.';
  } else if (dir.length === 1 ||
      (isWindows && dir.length <= 3 && dir.charAt(1) === ':')) {
    // It is just a slash or a drive letter with a slash
    return dir;
  } else {
    // It is a full dirname, strip trailing slash
    return dir.substring(0, dir.length - 1);
  }
};


exports.basename = function(path, ext) {
  var f = splitPathRe.exec(path)[2] || '';
  // TODO: make this comparison case-insensitive on windows?
  if (ext && f.substr(-1 * ext.length) === ext) {
    f = f.substr(0, f.length - ext.length);
  }
  return f;
};


exports.extname = function(path) {
  return splitPathRe.exec(path)[3] || '';
};

exports.relative = function(from, to) {
  from = exports.resolve(from).substr(1);
  to = exports.resolve(to).substr(1);

  function trim(arr) {
    var start = 0;
    for (; start < arr.length; start++) {
      if (arr[start] !== '') break;
    }

    var end = arr.length - 1;
    for (; end >= 0; end--) {
      if (arr[end] !== '') break;
    }

    if (start > end) return [];
    return arr.slice(start, end - start + 1);
  }

  var fromParts = trim(from.split('/'));
  var toParts = trim(to.split('/'));

  var length = Math.min(fromParts.length, toParts.length);
  var samePartsLength = length;
  for (var i = 0; i < length; i++) {
    if (fromParts[i] !== toParts[i]) {
      samePartsLength = i;
      break;
    }
  }

  var outputParts = [];
  for (var i = samePartsLength; i < fromParts.length; i++) {
    outputParts.push('..');
  }

  outputParts = outputParts.concat(toParts.slice(samePartsLength));

  return outputParts.join('/');
};

});

require.define("__browserify_process",function(require,module,exports,__dirname,__filename,process,global){var process = module.exports = {};

process.nextTick = (function () {
    var canSetImmediate = typeof window !== 'undefined'
        && window.setImmediate;
    var canPost = typeof window !== 'undefined'
        && window.postMessage && window.addEventListener
    ;

    if (canSetImmediate) {
        return function (f) { return window.setImmediate(f) };
    }

    if (canPost) {
        var queue = [];
        window.addEventListener('message', function (ev) {
            if (ev.source === window && ev.data === 'browserify-tick') {
                ev.stopPropagation();
                if (queue.length > 0) {
                    var fn = queue.shift();
                    fn();
                }
            }
        }, true);

        return function nextTick(fn) {
            queue.push(fn);
            window.postMessage('browserify-tick', '*');
        };
    }

    return function nextTick(fn) {
        setTimeout(fn, 0);
    };
})();

process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];

process.binding = function (name) {
    if (name === 'evals') return (require)('vm')
    else throw new Error('No such module. (Possibly not yet loaded)')
};

(function () {
    var cwd = '/';
    var path;
    process.cwd = function () { return cwd };
    process.chdir = function (dir) {
        if (!path) path = require('path');
        cwd = path.resolve(dir, cwd);
    };
})();

});

require.define("/package.json",function(require,module,exports,__dirname,__filename,process,global){module.exports = {"main":"escodegen.js"}
});

require.define("/escodegen.js",function(require,module,exports,__dirname,__filename,process,global){/*
  Copyright (C) 2012 Michael Ficarra <escodegen.copyright@michael.ficarra.me>
  Copyright (C) 2012 Robert Gust-Bardon <donate@robert.gust-bardon.org>
  Copyright (C) 2012 John Freeman <jfreeman08@gmail.com>
  Copyright (C) 2011-2012 Ariya Hidayat <ariya.hidayat@gmail.com>
  Copyright (C) 2012 Mathias Bynens <mathias@qiwi.be>
  Copyright (C) 2012 Joost-Wim Boekesteijn <joost-wim@boekesteijn.nl>
  Copyright (C) 2012 Kris Kowal <kris.kowal@cixar.com>
  Copyright (C) 2012 Yusuke Suzuki <utatane.tea@gmail.com>
  Copyright (C) 2012 Arpad Borsos <arpad.borsos@googlemail.com>

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*jslint bitwise:true */
/*global escodegen:true, exports:true, generateStatement:true, generateExpression:true, generateFunctionBody:true, process:true, require:true, define:true, global:true*/
(function () {
    'use strict';

    var Syntax,
        Precedence,
        BinaryPrecedence,
        Regex,
        VisitorKeys,
        VisitorOption,
        SourceNode,
        isArray,
        base,
        indent,
        json,
        renumber,
        hexadecimal,
        quotes,
        escapeless,
        newline,
        space,
        parentheses,
        semicolons,
        safeConcatenation,
        directive,
        extra,
        parse,
        sourceMap,
        traverse;

    traverse = require('estraverse').traverse;

    Syntax = {
        AssignmentExpression: 'AssignmentExpression',
        ArrayExpression: 'ArrayExpression',
        ArrayPattern: 'ArrayPattern',
        BlockStatement: 'BlockStatement',
        BinaryExpression: 'BinaryExpression',
        BreakStatement: 'BreakStatement',
        CallExpression: 'CallExpression',
        CatchClause: 'CatchClause',
        ComprehensionBlock: 'ComprehensionBlock',
        ComprehensionExpression: 'ComprehensionExpression',
        ConditionalExpression: 'ConditionalExpression',
        ContinueStatement: 'ContinueStatement',
        DirectiveStatement: 'DirectiveStatement',
        DoWhileStatement: 'DoWhileStatement',
        DebuggerStatement: 'DebuggerStatement',
        EmptyStatement: 'EmptyStatement',
        ExpressionStatement: 'ExpressionStatement',
        ForStatement: 'ForStatement',
        ForInStatement: 'ForInStatement',
        FunctionDeclaration: 'FunctionDeclaration',
        FunctionExpression: 'FunctionExpression',
        Identifier: 'Identifier',
        IfStatement: 'IfStatement',
        Literal: 'Literal',
        LabeledStatement: 'LabeledStatement',
        LogicalExpression: 'LogicalExpression',
        MemberExpression: 'MemberExpression',
        NewExpression: 'NewExpression',
        ObjectExpression: 'ObjectExpression',
        ObjectPattern: 'ObjectPattern',
        Program: 'Program',
        Property: 'Property',
        ReturnStatement: 'ReturnStatement',
        SequenceExpression: 'SequenceExpression',
        SwitchStatement: 'SwitchStatement',
        SwitchCase: 'SwitchCase',
        ThisExpression: 'ThisExpression',
        ThrowStatement: 'ThrowStatement',
        TryStatement: 'TryStatement',
        UnaryExpression: 'UnaryExpression',
        UpdateExpression: 'UpdateExpression',
        VariableDeclaration: 'VariableDeclaration',
        VariableDeclarator: 'VariableDeclarator',
        WhileStatement: 'WhileStatement',
        WithStatement: 'WithStatement',
        YieldExpression: 'YieldExpression'

    };

    Precedence = {
        Sequence: 0,
        Assignment: 1,
        Conditional: 2,
        LogicalOR: 3,
        LogicalAND: 4,
        BitwiseOR: 5,
        BitwiseXOR: 6,
        BitwiseAND: 7,
        Equality: 8,
        Relational: 9,
        BitwiseSHIFT: 10,
        Additive: 11,
        Multiplicative: 12,
        Unary: 13,
        Postfix: 14,
        Call: 15,
        New: 16,
        Member: 17,
        Primary: 18
    };

    BinaryPrecedence = {
        '||': Precedence.LogicalOR,
        '&&': Precedence.LogicalAND,
        '|': Precedence.BitwiseOR,
        '^': Precedence.BitwiseXOR,
        '&': Precedence.BitwiseAND,
        '==': Precedence.Equality,
        '!=': Precedence.Equality,
        '===': Precedence.Equality,
        '!==': Precedence.Equality,
        'is': Precedence.Equality,
        'isnt': Precedence.Equality,
        '<': Precedence.Relational,
        '>': Precedence.Relational,
        '<=': Precedence.Relational,
        '>=': Precedence.Relational,
        'in': Precedence.Relational,
        'instanceof': Precedence.Relational,
        '<<': Precedence.BitwiseSHIFT,
        '>>': Precedence.BitwiseSHIFT,
        '>>>': Precedence.BitwiseSHIFT,
        '+': Precedence.Additive,
        '-': Precedence.Additive,
        '*': Precedence.Multiplicative,
        '%': Precedence.Multiplicative,
        '/': Precedence.Multiplicative
    };

    Regex = {
        NonAsciiIdentifierPart: new RegExp('[\xaa\xb5\xba\xc0-\xd6\xd8-\xf6\xf8-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0300-\u0374\u0376\u0377\u037a-\u037d\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0481\u0483-\u0487\u048a-\u0527\u0531-\u0556\u0559\u0561-\u0587\u0591-\u05bd\u05bf\u05c1\u05c2\u05c4\u05c5\u05c7\u05d0-\u05ea\u05f0-\u05f2\u0610-\u061a\u0620-\u0669\u066e-\u06d3\u06d5-\u06dc\u06df-\u06e8\u06ea-\u06fc\u06ff\u0710-\u074a\u074d-\u07b1\u07c0-\u07f5\u07fa\u0800-\u082d\u0840-\u085b\u08a0\u08a2-\u08ac\u08e4-\u08fe\u0900-\u0963\u0966-\u096f\u0971-\u0977\u0979-\u097f\u0981-\u0983\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bc-\u09c4\u09c7\u09c8\u09cb-\u09ce\u09d7\u09dc\u09dd\u09df-\u09e3\u09e6-\u09f1\u0a01-\u0a03\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a3c\u0a3e-\u0a42\u0a47\u0a48\u0a4b-\u0a4d\u0a51\u0a59-\u0a5c\u0a5e\u0a66-\u0a75\u0a81-\u0a83\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abc-\u0ac5\u0ac7-\u0ac9\u0acb-\u0acd\u0ad0\u0ae0-\u0ae3\u0ae6-\u0aef\u0b01-\u0b03\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3c-\u0b44\u0b47\u0b48\u0b4b-\u0b4d\u0b56\u0b57\u0b5c\u0b5d\u0b5f-\u0b63\u0b66-\u0b6f\u0b71\u0b82\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bbe-\u0bc2\u0bc6-\u0bc8\u0bca-\u0bcd\u0bd0\u0bd7\u0be6-\u0bef\u0c01-\u0c03\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c33\u0c35-\u0c39\u0c3d-\u0c44\u0c46-\u0c48\u0c4a-\u0c4d\u0c55\u0c56\u0c58\u0c59\u0c60-\u0c63\u0c66-\u0c6f\u0c82\u0c83\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbc-\u0cc4\u0cc6-\u0cc8\u0cca-\u0ccd\u0cd5\u0cd6\u0cde\u0ce0-\u0ce3\u0ce6-\u0cef\u0cf1\u0cf2\u0d02\u0d03\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d-\u0d44\u0d46-\u0d48\u0d4a-\u0d4e\u0d57\u0d60-\u0d63\u0d66-\u0d6f\u0d7a-\u0d7f\u0d82\u0d83\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0dca\u0dcf-\u0dd4\u0dd6\u0dd8-\u0ddf\u0df2\u0df3\u0e01-\u0e3a\u0e40-\u0e4e\u0e50-\u0e59\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb9\u0ebb-\u0ebd\u0ec0-\u0ec4\u0ec6\u0ec8-\u0ecd\u0ed0-\u0ed9\u0edc-\u0edf\u0f00\u0f18\u0f19\u0f20-\u0f29\u0f35\u0f37\u0f39\u0f3e-\u0f47\u0f49-\u0f6c\u0f71-\u0f84\u0f86-\u0f97\u0f99-\u0fbc\u0fc6\u1000-\u1049\u1050-\u109d\u10a0-\u10c5\u10c7\u10cd\u10d0-\u10fa\u10fc-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u135d-\u135f\u1380-\u138f\u13a0-\u13f4\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u16ee-\u16f0\u1700-\u170c\u170e-\u1714\u1720-\u1734\u1740-\u1753\u1760-\u176c\u176e-\u1770\u1772\u1773\u1780-\u17d3\u17d7\u17dc\u17dd\u17e0-\u17e9\u180b-\u180d\u1810-\u1819\u1820-\u1877\u1880-\u18aa\u18b0-\u18f5\u1900-\u191c\u1920-\u192b\u1930-\u193b\u1946-\u196d\u1970-\u1974\u1980-\u19ab\u19b0-\u19c9\u19d0-\u19d9\u1a00-\u1a1b\u1a20-\u1a5e\u1a60-\u1a7c\u1a7f-\u1a89\u1a90-\u1a99\u1aa7\u1b00-\u1b4b\u1b50-\u1b59\u1b6b-\u1b73\u1b80-\u1bf3\u1c00-\u1c37\u1c40-\u1c49\u1c4d-\u1c7d\u1cd0-\u1cd2\u1cd4-\u1cf6\u1d00-\u1de6\u1dfc-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u200c\u200d\u203f\u2040\u2054\u2071\u207f\u2090-\u209c\u20d0-\u20dc\u20e1\u20e5-\u20f0\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u2160-\u2188\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d7f-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u2de0-\u2dff\u2e2f\u3005-\u3007\u3021-\u302f\u3031-\u3035\u3038-\u303c\u3041-\u3096\u3099\u309a\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fcc\ua000-\ua48c\ua4d0-\ua4fd\ua500-\ua60c\ua610-\ua62b\ua640-\ua66f\ua674-\ua67d\ua67f-\ua697\ua69f-\ua6f1\ua717-\ua71f\ua722-\ua788\ua78b-\ua78e\ua790-\ua793\ua7a0-\ua7aa\ua7f8-\ua827\ua840-\ua873\ua880-\ua8c4\ua8d0-\ua8d9\ua8e0-\ua8f7\ua8fb\ua900-\ua92d\ua930-\ua953\ua960-\ua97c\ua980-\ua9c0\ua9cf-\ua9d9\uaa00-\uaa36\uaa40-\uaa4d\uaa50-\uaa59\uaa60-\uaa76\uaa7a\uaa7b\uaa80-\uaac2\uaadb-\uaadd\uaae0-\uaaef\uaaf2-\uaaf6\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uabc0-\uabea\uabec\uabed\uabf0-\uabf9\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf900-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\ufb1d-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe00-\ufe0f\ufe20-\ufe26\ufe33\ufe34\ufe4d-\ufe4f\ufe70-\ufe74\ufe76-\ufefc\uff10-\uff19\uff21-\uff3a\uff3f\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc]')
    };

    function getDefaultOptions() {
        // default options
        return {
            indent: null,
            base: null,
            parse: null,
            comment: false,
            format: {
                indent: {
                    style: '    ',
                    base: 0,
                    adjustMultilineComment: false
                },
                json: false,
                renumber: false,
                hexadecimal: false,
                quotes: 'single',
                escapeless: false,
                compact: false,
                parentheses: true,
                semicolons: true,
                safeConcatenation: false
            },
            moz: {
                starlessGenerator: false,
                parenthesizedComprehensionBlock: false
            },
            sourceMap: null,
            sourceMapRoot: null,
            sourceMapWithCode: false,
            directive: false,
            verbatim: null
        };
    }

    function stringToArray(str) {
        var length = str.length,
            result = [],
            i;
        for (i = 0; i < length; i += 1) {
            result[i] = str.charAt(i);
        }
        return result;
    }

    function stringRepeat(str, num) {
        var result = '';

        for (num |= 0; num > 0; num >>>= 1, str += str) {
            if (num & 1) {
                result += str;
            }
        }

        return result;
    }

    isArray = Array.isArray;
    if (!isArray) {
        isArray = function isArray(array) {
            return Object.prototype.toString.call(array) === '[object Array]';
        };
    }

    // Fallback for the non SourceMap environment
    function SourceNodeMock(line, column, filename, chunk) {
        var result = [];

        function flatten(input) {
            var i, iz;
            if (isArray(input)) {
                for (i = 0, iz = input.length; i < iz; ++i) {
                    flatten(input[i]);
                }
            } else if (input instanceof SourceNodeMock) {
                result.push(input);
            } else if (typeof input === 'string' && input) {
                result.push(input);
            }
        }

        flatten(chunk);
        this.children = result;
    }

    SourceNodeMock.prototype.toString = function toString() {
        var res = '', i, iz, node;
        for (i = 0, iz = this.children.length; i < iz; ++i) {
            node = this.children[i];
            if (node instanceof SourceNodeMock) {
                res += node.toString();
            } else {
                res += node;
            }
        }
        return res;
    };

    SourceNodeMock.prototype.replaceRight = function replaceRight(pattern, replacement) {
        var last = this.children[this.children.length - 1];
        if (last instanceof SourceNodeMock) {
            last.replaceRight(pattern, replacement);
        } else if (typeof last === 'string') {
            this.children[this.children.length - 1] = last.replace(pattern, replacement);
        } else {
            this.children.push(''.replace(pattern, replacement));
        }
        return this;
    };

    SourceNodeMock.prototype.join = function join(sep) {
        var i, iz, result;
        result = [];
        iz = this.children.length;
        if (iz > 0) {
            for (i = 0, iz -= 1; i < iz; ++i) {
                result.push(this.children[i], sep);
            }
            result.push(this.children[iz]);
            this.children = result;
        }
        return this;
    };

    function hasLineTerminator(str) {
        return (/[\r\n]/g).test(str);
    }

    function endsWithLineTerminator(str) {
        var ch = str.charAt(str.length - 1);
        return ch === '\r' || ch === '\n';
    }

    function shallowCopy(obj) {
        var ret = {}, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                ret[key] = obj[key];
            }
        }
        return ret;
    }

    function deepCopy(obj) {
        var ret = {}, key, val;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                val = obj[key];
                if (typeof val === 'object' && val !== null) {
                    ret[key] = deepCopy(val);
                } else {
                    ret[key] = val;
                }
            }
        }
        return ret;
    }

    function updateDeeply(target, override) {
        var key, val;

        function isHashObject(target) {
            return typeof target === 'object' && target instanceof Object && !(target instanceof RegExp);
        }

        for (key in override) {
            if (override.hasOwnProperty(key)) {
                val = override[key];
                if (isHashObject(val)) {
                    if (isHashObject(target[key])) {
                        updateDeeply(target[key], val);
                    } else {
                        target[key] = updateDeeply({}, val);
                    }
                } else {
                    target[key] = val;
                }
            }
        }
        return target;
    }

    function generateNumber(value) {
        var result, point, temp, exponent, pos;

        if (value !== value) {
            throw new Error('Numeric literal whose value is NaN');
        }
        if (value < 0 || (value === 0 && 1 / value < 0)) {
            throw new Error('Numeric literal whose value is negative');
        }

        if (value === 1 / 0) {
            return json ? 'null' : renumber ? '1e400' : '1e+400';
        }

        result = '' + value;
        if (!renumber || result.length < 3) {
            return result;
        }

        point = result.indexOf('.');
        if (!json && result.charAt(0) === '0' && point === 1) {
            point = 0;
            result = result.slice(1);
        }
        temp = result;
        result = result.replace('e+', 'e');
        exponent = 0;
        if ((pos = temp.indexOf('e')) > 0) {
            exponent = +temp.slice(pos + 1);
            temp = temp.slice(0, pos);
        }
        if (point >= 0) {
            exponent -= temp.length - point - 1;
            temp = +(temp.slice(0, point) + temp.slice(point + 1)) + '';
        }
        pos = 0;
        while (temp.charAt(temp.length + pos - 1) === '0') {
            pos -= 1;
        }
        if (pos !== 0) {
            exponent -= pos;
            temp = temp.slice(0, pos);
        }
        if (exponent !== 0) {
            temp += 'e' + exponent;
        }
        if ((temp.length < result.length ||
                    (hexadecimal && value > 1e12 && Math.floor(value) === value && (temp = '0x' + value.toString(16)).length < result.length)) &&
                +temp === value) {
            result = temp;
        }

        return result;
    }

    // Generate valid RegExp expression.
    // This function is based on https://github.com/Constellation/iv Engine

    function escapeRegExpCharacter(ch, previousIsBackslash) {
        // not handling '\' and handling \u2028 or \u2029 to unicode escape sequence
        if ((ch & ~1) === 0x2028) {
            return (previousIsBackslash ? 'u' : '\\u') + ((ch === 0x2028) ? '2028' : '2029');
        } else if (ch === 10 || ch === 13) {  // \n, \r
            return (previousIsBackslash ? '' : '\\') + ((ch === 10) ? 'n' : 'r');
        }
        return String.fromCharCode(ch);
    }

    function generateRegExp(reg) {
        var match, result, flags, i, iz, ch, characterInBrack, previousIsBackslash;

        result = reg.toString();

        if (reg.source) {
            // extract flag from toString result
            match = result.match(/\/([^/]*)$/);
            if (!match) {
                return result;
            }

            flags = match[1];
            result = '';

            characterInBrack = false;
            previousIsBackslash = false;
            for (i = 0, iz = reg.source.length; i < iz; ++i) {
                ch = reg.source.charCodeAt(i);

                if (!previousIsBackslash) {
                    if (characterInBrack) {
                        if (ch === 93) {  // ]
                            characterInBrack = false;
                        }
                    } else {
                        if (ch === 47) {  // /
                            result += '\\';
                        } else if (ch === 91) {  // [
                            characterInBrack = true;
                        }
                    }
                    result += escapeRegExpCharacter(ch, previousIsBackslash);
                    previousIsBackslash = ch === 92;  // \
                } else {
                    // if new RegExp("\\\n') is provided, create /\n/
                    result += escapeRegExpCharacter(ch, previousIsBackslash);
                    // prevent like /\\[/]/
                    previousIsBackslash = false;
                }
            }

            return '/' + result + '/' + flags;
        }

        return result;
    }

    function escapeAllowedCharacter(ch, next) {
        var code = ch.charCodeAt(0), hex = code.toString(16), result = '\\';

        switch (ch) {
        case '\b':
            result += 'b';
            break;
        case '\f':
            result += 'f';
            break;
        case '\t':
            result += 't';
            break;
        default:
            if (json || code > 0xff) {
                result += 'u' + '0000'.slice(hex.length) + hex;
            } else if (ch === '\u0000' && '0123456789'.indexOf(next) < 0) {
                result += '0';
            } else if (ch === '\x0B') { // '\v'
                result += 'x0B';
            } else {
                result += 'x' + '00'.slice(hex.length) + hex;
            }
            break;
        }

        return result;
    }

    function escapeDisallowedCharacter(ch) {
        var result = '\\';
        switch (ch) {
        case '\\':
            result += '\\';
            break;
        case '\n':
            result += 'n';
            break;
        case '\r':
            result += 'r';
            break;
        case '\u2028':
            result += 'u2028';
            break;
        case '\u2029':
            result += 'u2029';
            break;
        default:
            throw new Error('Incorrectly classified character');
        }

        return result;
    }

    function escapeDirective(str) {
        var i, iz, ch, single, buf, quote;

        buf = str;
        if (typeof buf[0] === 'undefined') {
            buf = stringToArray(buf);
        }

        quote = quotes === 'double' ? '"' : '\'';
        for (i = 0, iz = buf.length; i < iz; i += 1) {
            ch = buf[i];
            if (ch === '\'') {
                quote = '"';
                break;
            } else if (ch === '"') {
                quote = '\'';
                break;
            } else if (ch === '\\') {
                i += 1;
            }
        }

        return quote + str + quote;
    }

    function escapeString(str) {
        var result = '', i, len, ch, next, singleQuotes = 0, doubleQuotes = 0, single;

        if (typeof str[0] === 'undefined') {
            str = stringToArray(str);
        }

        for (i = 0, len = str.length; i < len; i += 1) {
            ch = str[i];
            if (ch === '\'') {
                singleQuotes += 1;
            } else if (ch === '"') {
                doubleQuotes += 1;
            } else if (ch === '/' && json) {
                result += '\\';
            } else if ('\\\n\r\u2028\u2029'.indexOf(ch) >= 0) {
                result += escapeDisallowedCharacter(ch);
                continue;
            } else if ((json && ch < ' ') || !(json || escapeless || (ch >= ' ' && ch <= '~'))) {
                result += escapeAllowedCharacter(ch, str[i + 1]);
                continue;
            }
            result += ch;
        }

        single = !(quotes === 'double' || (quotes === 'auto' && doubleQuotes < singleQuotes));
        str = result;
        result = single ? '\'' : '"';

        if (typeof str[0] === 'undefined') {
            str = stringToArray(str);
        }

        for (i = 0, len = str.length; i < len; i += 1) {
            ch = str[i];
            if ((ch === '\'' && single) || (ch === '"' && !single)) {
                result += '\\';
            }
            result += ch;
        }

        return result + (single ? '\'' : '"');
    }

    function isWhiteSpace(ch) {
        // Use `\x0B` instead of `\v` for IE < 9 compatibility
        return '\t\x0B\f \xa0'.indexOf(ch) >= 0 || (ch.charCodeAt(0) >= 0x1680 && '\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000\ufeff'.indexOf(ch) >= 0);
    }

    function isLineTerminator(ch) {
        return '\n\r\u2028\u2029'.indexOf(ch) >= 0;
    }

    function isIdentifierPart(ch) {
        return (ch === '$') || (ch === '_') || (ch === '\\') ||
            (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') ||
            ((ch >= '0') && (ch <= '9')) ||
            ((ch.charCodeAt(0) >= 0x80) && Regex.NonAsciiIdentifierPart.test(ch));
    }

    function toSourceNode(generated, node) {
        if (node == null) {
            if (generated instanceof SourceNode) {
                return generated;
            } else {
                node = {};
            }
        }
        if (node.loc == null) {
            return new SourceNode(null, null, sourceMap, generated);
        }
        return new SourceNode(node.loc.start.line, node.loc.start.column, (sourceMap === true ? node.loc.source || null : sourceMap), generated);
    }

    function join(left, right) {
        var leftSource = toSourceNode(left).toString(),
            rightSource = toSourceNode(right).toString(),
            leftChar = leftSource.charAt(leftSource.length - 1),
            rightChar = rightSource.charAt(0);

        if (((leftChar === '+' || leftChar === '-') && leftChar === rightChar) || (isIdentifierPart(leftChar) && isIdentifierPart(rightChar))) {
            return [left, ' ', right];
        } else if (isWhiteSpace(leftChar) || isLineTerminator(leftChar) || isWhiteSpace(rightChar) || isLineTerminator(rightChar)) {
            return [left, right];
        }
        return [left, space, right];
    }

    function addIndent(stmt) {
        return [base, stmt];
    }

    function withIndent(fn) {
        var previousBase, result;
        previousBase = base;
        base += indent;
        result = fn.call(this, base);
        base = previousBase;
        return result;
    }

    function calculateSpaces(str) {
        var i;
        for (i = str.length - 1; i >= 0; i -= 1) {
            if (isLineTerminator(str.charAt(i))) {
                break;
            }
        }
        return (str.length - 1) - i;
    }

    function adjustMultilineComment(value, specialBase) {
        var array, i, len, line, j, ch, spaces, previousBase;

        array = value.split(/\r\n|[\r\n]/);
        spaces = Number.MAX_VALUE;

        // first line doesn't have indentation
        for (i = 1, len = array.length; i < len; i += 1) {
            line = array[i];
            j = 0;
            while (j < line.length && isWhiteSpace(line[j])) {
                j += 1;
            }
            if (spaces > j) {
                spaces = j;
            }
        }

        if (typeof specialBase !== 'undefined') {
            // pattern like
            // {
            //   var t = 20;  /*
            //                 * this is comment
            //                 */
            // }
            previousBase = base;
            if (array[1][spaces] === '*') {
                specialBase += ' ';
            }
            base = specialBase;
        } else {
            if (spaces & 1) {
                // /*
                //  *
                //  */
                // If spaces are odd number, above pattern is considered.
                // We waste 1 space.
                spaces -= 1;
            }
            previousBase = base;
        }

        for (i = 1, len = array.length; i < len; i += 1) {
            array[i] = toSourceNode(addIndent(array[i].slice(spaces))).join('');
        }

        base = previousBase;

        return array.join('\n');
    }

    function generateComment(comment, specialBase) {
        if (comment.type === 'Line') {
            if (endsWithLineTerminator(comment.value)) {
                return '//' + comment.value;
            } else {
                // Always use LineTerminator
                return '//' + comment.value + '\n';
            }
        }
        if (extra.format.indent.adjustMultilineComment && /[\n\r]/.test(comment.value)) {
            return adjustMultilineComment('/*' + comment.value + '*/', specialBase);
        }
        return '/*' + comment.value + '*/';
    }

    function addCommentsToStatement(stmt, result) {
        var i, len, comment, save, node, tailingToStatement, specialBase, fragment;

        if (stmt.leadingComments && stmt.leadingComments.length > 0) {
            save = result;

            comment = stmt.leadingComments[0];
            result = [];
            if (safeConcatenation && stmt.type === Syntax.Program && stmt.body.length === 0) {
                result.push('\n');
            }
            result.push(generateComment(comment));
            if (!endsWithLineTerminator(toSourceNode(result).toString())) {
                result.push('\n');
            }

            for (i = 1, len = stmt.leadingComments.length; i < len; i += 1) {
                comment = stmt.leadingComments[i];
                fragment = [generateComment(comment)];
                if (!endsWithLineTerminator(toSourceNode(fragment).toString())) {
                    fragment.push('\n');
                }
                result.push(addIndent(fragment));
            }

            result.push(addIndent(save));
        }

        if (stmt.trailingComments) {
            tailingToStatement = !endsWithLineTerminator(toSourceNode(result).toString());
            specialBase = stringRepeat(' ', calculateSpaces(toSourceNode([base, result, indent]).toString()));
            for (i = 0, len = stmt.trailingComments.length; i < len; i += 1) {
                comment = stmt.trailingComments[i];
                if (tailingToStatement) {
                    // We assume target like following script
                    //
                    // var t = 20;  /**
                    //               * This is comment of t
                    //               */
                    if (i === 0) {
                        // first case
                        result = [result, indent];
                    } else {
                        result = [result, specialBase];
                    }
                    result.push(generateComment(comment, specialBase));
                } else {
                    result = [result, addIndent(generateComment(comment))];
                }
                if (i !== len - 1 && !endsWithLineTerminator(toSourceNode(result).toString())) {
                    result = [result, '\n'];
                }
            }
        }

        return result;
    }

    function parenthesize(text, current, should) {
        if (current < should) {
            return ['(', text, ')'];
        }
        return text;
    }

    function maybeBlock(stmt, semicolonOptional, functionBody) {
        var result, noLeadingComment;

        noLeadingComment = !extra.comment || !stmt.leadingComments;

        if (stmt.type === Syntax.BlockStatement && noLeadingComment) {
            return [space, generateStatement(stmt, { functionBody: functionBody })];
        }

        if (stmt.type === Syntax.EmptyStatement && noLeadingComment) {
            return ';';
        }

        withIndent(function () {
            result = [newline, addIndent(generateStatement(stmt, { semicolonOptional: semicolonOptional, functionBody: functionBody }))];
        });

        return result;
    }

    function maybeBlockSuffix(stmt, result) {
        var ends = endsWithLineTerminator(toSourceNode(result).toString());
        if (stmt.type === Syntax.BlockStatement && (!extra.comment || !stmt.leadingComments) && !ends) {
            return [result, space];
        }
        if (ends) {
            return [result, base];
        }
        return [result, newline, base];
    }

    function generateVerbatim(expr, option) {
        var i, result;
        result = expr[extra.verbatim].split(/\r\n|\n/);
        for (i = 1; i < result.length; i++) {
            result[i] = newline + base + result[i];
        }

        result = parenthesize(result, Precedence.Sequence, option.precedence);
        return toSourceNode(result, expr);
    }

    function generateFunctionBody(node) {
        var result, i, len, expr;
        result = ['('];
        for (i = 0, len = node.params.length; i < len; i += 1) {
            result.push(node.params[i].name);
            if (i + 1 < len) {
                result.push(',' + space);
            }
        }
        result.push(')');

        if (node.expression) {
            result.push(space);
            expr = generateExpression(node.body, {
                precedence: Precedence.Assignment,
                allowIn: true,
                allowCall: true
            });
            if (expr.toString().charAt(0) === '{') {
                expr = ['(', expr, ')'];
            }
            result.push(expr);
        } else {
            result.push(maybeBlock(node.body, false, true));
        }
        return result;
    }

    function generateExpression(expr, option) {
        var result, precedence, type, currentPrecedence, i, len, raw, fragment, multiline, leftChar, leftSource, rightChar, rightSource, allowIn, allowCall, allowUnparenthesizedNew, property, key, value;

        precedence = option.precedence;
        allowIn = option.allowIn;
        allowCall = option.allowCall;
        type = expr.type || option.type;

        if (extra.verbatim && expr.hasOwnProperty(extra.verbatim)) {
            return generateVerbatim(expr, option);
        }

        switch (type) {
        case Syntax.SequenceExpression:
            result = [];
            allowIn |= (Precedence.Sequence < precedence);
            for (i = 0, len = expr.expressions.length; i < len; i += 1) {
                result.push(generateExpression(expr.expressions[i], {
                    precedence: Precedence.Assignment,
                    allowIn: allowIn,
                    allowCall: true
                }));
                if (i + 1 < len) {
                    result.push(',' + space);
                }
            }
            result = parenthesize(result, Precedence.Sequence, precedence);
            break;

        case Syntax.AssignmentExpression:
            allowIn |= (Precedence.Assignment < precedence);
            result = parenthesize(
                [
                    generateExpression(expr.left, {
                        precedence: Precedence.Call,
                        allowIn: allowIn,
                        allowCall: true
                    }),
                    space + expr.operator + space,
                    generateExpression(expr.right, {
                        precedence: Precedence.Assignment,
                        allowIn: allowIn,
                        allowCall: true
                    })
                ],
                Precedence.Assignment,
                precedence
            );
            break;

        case Syntax.ConditionalExpression:
            allowIn |= (Precedence.Conditional < precedence);
            result = parenthesize(
                [
                    generateExpression(expr.test, {
                        precedence: Precedence.LogicalOR,
                        allowIn: allowIn,
                        allowCall: true
                    }),
                    space + '?' + space,
                    generateExpression(expr.consequent, {
                        precedence: Precedence.Assignment,
                        allowIn: allowIn,
                        allowCall: true
                    }),
                    space + ':' + space,
                    generateExpression(expr.alternate, {
                        precedence: Precedence.Assignment,
                        allowIn: allowIn,
                        allowCall: true
                    })
                ],
                Precedence.Conditional,
                precedence
            );
            break;

        case Syntax.LogicalExpression:
        case Syntax.BinaryExpression:
            currentPrecedence = BinaryPrecedence[expr.operator];

            allowIn |= (currentPrecedence < precedence);

            fragment = generateExpression(expr.left, {
                precedence: currentPrecedence,
                allowIn: allowIn,
                allowCall: true
            });

            leftSource = fragment.toString();

            if (leftSource.charAt(leftSource.length - 1) === '/' && isIdentifierPart(expr.operator.charAt(0))) {
                result = [fragment, ' ', expr.operator];
            } else {
                result = join(fragment, expr.operator);
            }

            fragment = generateExpression(expr.right, {
                precedence: currentPrecedence + 1,
                allowIn: allowIn,
                allowCall: true
            });

            if (expr.operator === '/' && fragment.toString().charAt(0) === '/') {
                // If '/' concats with '/', it is interpreted as comment start
                result.push(' ', fragment);
            } else {
                result = join(result, fragment);
            }

            if (expr.operator === 'in' && !allowIn) {
                result = ['(', result, ')'];
            } else {
                result = parenthesize(result, currentPrecedence, precedence);
            }

            break;

        case Syntax.CallExpression:
            result = [generateExpression(expr.callee, {
                precedence: Precedence.Call,
                allowIn: true,
                allowCall: true,
                allowUnparenthesizedNew: false
            })];

            result.push('(');
            for (i = 0, len = expr['arguments'].length; i < len; i += 1) {
                result.push(generateExpression(expr['arguments'][i], {
                    precedence: Precedence.Assignment,
                    allowIn: true,
                    allowCall: true
                }));
                if (i + 1 < len) {
                    result.push(',' + space);
                }
            }
            result.push(')');

            if (!allowCall) {
                result = ['(', result, ')'];
            } else {
                result = parenthesize(result, Precedence.Call, precedence);
            }
            break;

        case Syntax.NewExpression:
            len = expr['arguments'].length;
            allowUnparenthesizedNew = option.allowUnparenthesizedNew === undefined || option.allowUnparenthesizedNew;

            result = join(
                'new',
                generateExpression(expr.callee, {
                    precedence: Precedence.New,
                    allowIn: true,
                    allowCall: false,
                    allowUnparenthesizedNew: allowUnparenthesizedNew && !parentheses && len === 0
                })
            );

            if (!allowUnparenthesizedNew || parentheses || len > 0) {
                result.push('(');
                for (i = 0; i < len; i += 1) {
                    result.push(generateExpression(expr['arguments'][i], {
                        precedence: Precedence.Assignment,
                        allowIn: true,
                        allowCall: true
                    }));
                    if (i + 1 < len) {
                        result.push(',' + space);
                    }
                }
                result.push(')');
            }

            result = parenthesize(result, Precedence.New, precedence);
            break;

        case Syntax.MemberExpression:
            result = [generateExpression(expr.object, {
                precedence: Precedence.Call,
                allowIn: true,
                allowCall: allowCall,
                allowUnparenthesizedNew: false
            })];

            if (expr.computed) {
                result.push('[', generateExpression(expr.property, {
                    precedence: Precedence.Sequence,
                    allowIn: true,
                    allowCall: allowCall
                }), ']');
            } else {
                if (expr.object.type === Syntax.Literal && typeof expr.object.value === 'number') {
                    fragment = toSourceNode(result).toString();
                    if (fragment.indexOf('.') < 0) {
                        if (!/[eExX]/.test(fragment) &&
                                !(fragment.length >= 2 && fragment.charCodeAt(0) === 48)) {  // '0'
                            result.push('.');
                        }
                    }
                }
                result.push('.' + expr.property.name);
            }

            result = parenthesize(result, Precedence.Member, precedence);
            break;

        case Syntax.UnaryExpression:
            fragment = generateExpression(expr.argument, {
                precedence: Precedence.Unary,
                allowIn: true,
                allowCall: true
            });

            if (space === '') {
                result = join(expr.operator, fragment);
            } else {
                result = [expr.operator];
                if (expr.operator.length > 2) {
                    // delete, void, typeof
                    // get `typeof []`, not `typeof[]`
                    result = join(result, fragment);
                } else {
                    // Prevent inserting spaces between operator and argument if it is unnecessary
                    // like, `!cond`
                    leftSource = toSourceNode(result).toString();
                    leftChar = leftSource.charAt(leftSource.length - 1);
                    rightChar = fragment.toString().charAt(0);

                    if (((leftChar === '+' || leftChar === '-') && leftChar === rightChar) || (isIdentifierPart(leftChar) && isIdentifierPart(rightChar))) {
                        result.push(' ', fragment);
                    } else {
                        result.push(fragment);
                    }
                }
            }
            result = parenthesize(result, Precedence.Unary, precedence);
            break;

        case Syntax.YieldExpression:
            if (expr.delegate) {
                result = 'yield*';
            } else {
                result = 'yield';
            }
            if (expr.argument) {
                result = join(
                    result,
                    generateExpression(expr.argument, {
                        precedence: Precedence.Assignment,
                        allowIn: true,
                        allowCall: true
                    })
                );
            }
            break;

        case Syntax.UpdateExpression:
            if (expr.prefix) {
                result = parenthesize(
                    [
                        expr.operator,
                        generateExpression(expr.argument, {
                            precedence: Precedence.Unary,
                            allowIn: true,
                            allowCall: true
                        })
                    ],
                    Precedence.Unary,
                    precedence
                );
            } else {
                result = parenthesize(
                    [
                        generateExpression(expr.argument, {
                            precedence: Precedence.Postfix,
                            allowIn: true,
                            allowCall: true
                        }),
                        expr.operator
                    ],
                    Precedence.Postfix,
                    precedence
                );
            }
            break;

        case Syntax.FunctionExpression:
            result = 'function';
            if (expr.id) {
                result += ' ' + expr.id.name;
            } else {
                result += space;
            }

            result = [result, generateFunctionBody(expr)];
            break;

        case Syntax.ArrayPattern:
        case Syntax.ArrayExpression:
            if (!expr.elements.length) {
                result = '[]';
                break;
            }
            multiline = expr.elements.length > 1;
            result = ['[', multiline ? newline : ''];
            withIndent(function (indent) {
                for (i = 0, len = expr.elements.length; i < len; i += 1) {
                    if (!expr.elements[i]) {
                        if (multiline) {
                            result.push(indent);
                        }
                        if (i + 1 === len) {
                            result.push(',');
                        }
                    } else {
                        result.push(multiline ? indent : '', generateExpression(expr.elements[i], {
                            precedence: Precedence.Assignment,
                            allowIn: true,
                            allowCall: true
                        }));
                    }
                    if (i + 1 < len) {
                        result.push(',' + (multiline ? newline : space));
                    }
                }
            });
            if (multiline && !endsWithLineTerminator(toSourceNode(result).toString())) {
                result.push(newline);
            }
            result.push(multiline ? base : '', ']');
            break;

        case Syntax.Property:
            if (expr.kind === 'get' || expr.kind === 'set') {
                result = [
                    expr.kind + ' ',
                    generateExpression(expr.key, {
                        precedence: Precedence.Sequence,
                        allowIn: true,
                        allowCall: true
                    }),
                    generateFunctionBody(expr.value)
                ];
            } else {
                if (expr.shorthand) {
                    result = generateExpression(expr.key, {
                        precedence: Precedence.Sequence,
                        allowIn: true,
                        allowCall: true
                    });
                } else if (expr.method) {
                    result = [];
                    if (expr.value.generator) {
                        result.push('*');
                    }
                    result.push(generateExpression(expr.key, {
                        precedence: Precedence.Sequence,
                        allowIn: true,
                        allowCall: true
                    }), generateFunctionBody(expr.value));
                } else {
                    result = [
                        generateExpression(expr.key, {
                            precedence: Precedence.Sequence,
                            allowIn: true,
                            allowCall: true
                        }),
                        ':' + space,
                        generateExpression(expr.value, {
                            precedence: Precedence.Assignment,
                            allowIn: true,
                            allowCall: true
                        })
                    ];
                }
            }
            break;

        case Syntax.ObjectExpression:
            if (!expr.properties.length) {
                result = '{}';
                break;
            }
            multiline = expr.properties.length > 1;

            withIndent(function (indent) {
                fragment = generateExpression(expr.properties[0], {
                    precedence: Precedence.Sequence,
                    allowIn: true,
                    allowCall: true,
                    type: Syntax.Property
                });
            });

            if (!multiline) {
                // issues 4
                // Do not transform from
                //   dejavu.Class.declare({
                //       method2: function () {}
                //   });
                // to
                //   dejavu.Class.declare({method2: function () {
                //       }});
                if (!hasLineTerminator(toSourceNode(fragment).toString())) {
                    result = [ '{', space, fragment, space, '}' ];
                    break;
                }
            }

            withIndent(function (indent) {
                result = [ '{', newline, indent, fragment ];

                if (multiline) {
                    result.push(',' + newline);
                    for (i = 1, len = expr.properties.length; i < len; i += 1) {
                        result.push(indent, generateExpression(expr.properties[i], {
                            precedence: Precedence.Sequence,
                            allowIn: true,
                            allowCall: true,
                            type: Syntax.Property
                        }));
                        if (i + 1 < len) {
                            result.push(',' + newline);
                        }
                    }
                }
            });

            if (!endsWithLineTerminator(toSourceNode(result).toString())) {
                result.push(newline);
            }
            result.push(base, '}');
            break;

        case Syntax.ObjectPattern:
            if (!expr.properties.length) {
                result = '{}';
                break;
            }

            multiline = false;
            if (expr.properties.length === 1) {
                property = expr.properties[0];
                if (property.value.type !== Syntax.Identifier) {
                    multiline = true;
                }
            } else {
                for (i = 0, len = expr.properties.length; i < len; i += 1) {
                    property = expr.properties[i];
                    if (!property.shorthand) {
                        multiline = true;
                        break;
                    }
                }
            }
            result = ['{', multiline ? newline : '' ];

            withIndent(function (indent) {
                for (i = 0, len = expr.properties.length; i < len; i += 1) {
                    result.push(multiline ? indent : '', generateExpression(expr.properties[i], {
                        precedence: Precedence.Sequence,
                        allowIn: true,
                        allowCall: true
                    }));
                    if (i + 1 < len) {
                        result.push(',' + (multiline ? newline : space));
                    }
                }
            });

            if (multiline && !endsWithLineTerminator(toSourceNode(result).toString())) {
                result.push(newline);
            }
            result.push(multiline ? base : '', '}');
            break;

        case Syntax.ThisExpression:
            result = 'this';
            break;

        case Syntax.Identifier:
            result = expr.name;
            break;

        case Syntax.Literal:
            if (expr.hasOwnProperty('raw') && parse) {
                try {
                    raw = parse(expr.raw).body[0].expression;
                    if (raw.type === Syntax.Literal) {
                        if (raw.value === expr.value) {
                            result = expr.raw;
                            break;
                        }
                    }
                } catch (e) {
                    // not use raw property
                }
            }

            if (expr.value === null) {
                result = 'null';
                break;
            }

            if (typeof expr.value === 'string') {
                result = escapeString(expr.value);
                break;
            }

            if (typeof expr.value === 'number') {
                result = generateNumber(expr.value);
                break;
            }

            if (typeof expr.value === 'boolean') {
                result = expr.value ? 'true' : 'false';
                break;
            }

            result = generateRegExp(expr.value);
            break;

        case Syntax.ComprehensionExpression:
            result = [
                '[',
                generateExpression(expr.body, {
                    precedence: Precedence.Assignment,
                    allowIn: true,
                    allowCall: true
                })
            ];

            if (expr.blocks) {
                for (i = 0, len = expr.blocks.length; i < len; i += 1) {
                    fragment = generateExpression(expr.blocks[i], {
                        precedence: Precedence.Sequence,
                        allowIn: true,
                        allowCall: true
                    });
                    result = join(result, fragment);
                }
            }

            if (expr.filter) {
                result = join(result, 'if' + space);
                fragment = generateExpression(expr.filter, {
                    precedence: Precedence.Sequence,
                    allowIn: true,
                    allowCall: true
                });
                if (extra.moz.parenthesizedComprehensionBlock) {
                    result = join(result, [ '(', fragment, ')' ]);
                } else {
                    result = join(result, fragment);
                }
            }
            result.push(']');
            break;

        case Syntax.ComprehensionBlock:
            if (expr.left.type === Syntax.VariableDeclaration) {
                fragment = [
                    expr.left.kind + ' ',
                    generateStatement(expr.left.declarations[0], {
                        allowIn: false
                    })
                ];
            } else {
                fragment = generateExpression(expr.left, {
                    precedence: Precedence.Call,
                    allowIn: true,
                    allowCall: true
                });
            }

            fragment = join(fragment, expr.of ? 'of' : 'in');
            fragment = join(fragment, generateExpression(expr.right, {
                precedence: Precedence.Sequence,
                allowIn: true,
                allowCall: true
            }));

            if (extra.moz.parenthesizedComprehensionBlock) {
                result = [ 'for' + space + '(', fragment, ')' ];
            } else {
                result = join('for' + space, fragment);
            }
            break;

        default:
            throw new Error('Unknown expression type: ' + expr.type);
        }

        return toSourceNode(result, expr);
    }

    function generateStatement(stmt, option) {
        var i, len, result, node, allowIn, functionBody, directiveContext, fragment, semicolon;

        allowIn = true;
        semicolon = ';';
        functionBody = false;
        directiveContext = false;
        if (option) {
            allowIn = option.allowIn === undefined || option.allowIn;
            if (!semicolons && option.semicolonOptional === true) {
                semicolon = '';
            }
            functionBody = option.functionBody;
            directiveContext = option.directiveContext;
        }

        switch (stmt.type) {
        case Syntax.BlockStatement:
            result = ['{', newline];

            withIndent(function () {
                for (i = 0, len = stmt.body.length; i < len; i += 1) {
                    fragment = addIndent(generateStatement(stmt.body[i], {
                        semicolonOptional: i === len - 1,
                        directiveContext: functionBody
                    }));
                    result.push(fragment);
                    if (!endsWithLineTerminator(toSourceNode(fragment).toString())) {
                        result.push(newline);
                    }
                }
            });

            result.push(addIndent('}'));
            break;

        case Syntax.BreakStatement:
            if (stmt.label) {
                result = 'break ' + stmt.label.name + semicolon;
            } else {
                result = 'break' + semicolon;
            }
            break;

        case Syntax.ContinueStatement:
            if (stmt.label) {
                result = 'continue ' + stmt.label.name + semicolon;
            } else {
                result = 'continue' + semicolon;
            }
            break;

        case Syntax.DirectiveStatement:
            if (stmt.raw) {
                result = stmt.raw + semicolon;
            } else {
                result = escapeDirective(stmt.directive) + semicolon;
            }
            break;

        case Syntax.DoWhileStatement:
            // Because `do 42 while (cond)` is Syntax Error. We need semicolon.
            result = join('do', maybeBlock(stmt.body));
            result = maybeBlockSuffix(stmt.body, result);
            result = join(result, [
                'while' + space + '(',
                generateExpression(stmt.test, {
                    precedence: Precedence.Sequence,
                    allowIn: true,
                    allowCall: true
                }),
                ')' + semicolon
            ]);
            break;

        case Syntax.CatchClause:
            withIndent(function () {
                result = [
                    'catch' + space + '(',
                    generateExpression(stmt.param, {
                        precedence: Precedence.Sequence,
                        allowIn: true,
                        allowCall: true
                    }),
                    ')'
                ];
            });
            result.push(maybeBlock(stmt.body));
            break;

        case Syntax.DebuggerStatement:
            result = 'debugger' + semicolon;
            break;

        case Syntax.EmptyStatement:
            result = ';';
            break;

        case Syntax.ExpressionStatement:
            result = [generateExpression(stmt.expression, {
                precedence: Precedence.Sequence,
                allowIn: true,
                allowCall: true
            })];
            // 12.4 '{', 'function' is not allowed in this position.
            // wrap expression with parentheses
            fragment = toSourceNode(result).toString();
            if (fragment.charAt(0) === '{' || (fragment.slice(0, 8) === 'function' && " (".indexOf(fragment.charAt(8)) >= 0) || (directive && directiveContext && stmt.expression.type === Syntax.Literal && typeof stmt.expression.value === 'string')) {
                result = ['(', result, ')' + semicolon];
            } else {
                result.push(semicolon);
            }
            break;

        case Syntax.VariableDeclarator:
            if (stmt.init) {
                result = [
                    generateExpression(stmt.id, {
                        precedence: Precedence.Assignment,
                        allowIn: allowIn,
                        allowCall: true
                    }) + space + '=' + space,
                    generateExpression(stmt.init, {
                        precedence: Precedence.Assignment,
                        allowIn: allowIn,
                        allowCall: true
                    })
                ];
            } else {
                result = stmt.id.name;
            }
            break;

        case Syntax.VariableDeclaration:
            result = [stmt.kind];
            // special path for
            // var x = function () {
            // };
            if (stmt.declarations.length === 1 && stmt.declarations[0].init &&
                    stmt.declarations[0].init.type === Syntax.FunctionExpression) {
                result.push(' ', generateStatement(stmt.declarations[0], {
                    allowIn: allowIn
                }));
            } else {
                // VariableDeclarator is typed as Statement,
                // but joined with comma (not LineTerminator).
                // So if comment is attached to target node, we should specialize.
                withIndent(function () {
                    node = stmt.declarations[0];
                    if (extra.comment && node.leadingComments) {
                        result.push('\n', addIndent(generateStatement(node, {
                            allowIn: allowIn
                        })));
                    } else {
                        result.push(' ', generateStatement(node, {
                            allowIn: allowIn
                        }));
                    }

                    for (i = 1, len = stmt.declarations.length; i < len; i += 1) {
                        node = stmt.declarations[i];
                        if (extra.comment && node.leadingComments) {
                            result.push(',' + newline, addIndent(generateStatement(node, {
                                allowIn: allowIn
                            })));
                        } else {
                            result.push(',' + space, generateStatement(node, {
                                allowIn: allowIn
                            }));
                        }
                    }
                });
            }
            result.push(semicolon);
            break;

        case Syntax.ThrowStatement:
            result = [join(
                'throw',
                generateExpression(stmt.argument, {
                    precedence: Precedence.Sequence,
                    allowIn: true,
                    allowCall: true
                })
            ), semicolon];
            break;

        case Syntax.TryStatement:
            result = ['try', maybeBlock(stmt.block)];
            result = maybeBlockSuffix(stmt.block, result);
            if (stmt.handlers) {
                // old interface
                for (i = 0, len = stmt.handlers.length; i < len; i += 1) {
                    result = join(result, generateStatement(stmt.handlers[i]));
                    if (stmt.finalizer || i + 1 !== len) {
                        result = maybeBlockSuffix(stmt.handlers[i].body, result);
                    }
                }
            } else {
                // new interface
                if (stmt.handler) {
                    result = join(result, generateStatement(stmt.handler));
                    if (stmt.finalizer || stmt.guardedHandlers.length > 0) {
                        result = maybeBlockSuffix(stmt.handler.body, result);
                    }
                }

                for (i = 0, len = stmt.guardedHandlers.length; i < len; i += 1) {
                    result = join(result, generateStatement(stmt.guardedHandlers[i]));
                    if (stmt.finalizer || i + 1 !== len) {
                        result = maybeBlockSuffix(stmt.guardedHandlers[i].body, result);
                    }
                }
            }
            if (stmt.finalizer) {
                result = join(result, ['finally', maybeBlock(stmt.finalizer)]);
            }
            break;

        case Syntax.SwitchStatement:
            withIndent(function () {
                result = [
                    'switch' + space + '(',
                    generateExpression(stmt.discriminant, {
                        precedence: Precedence.Sequence,
                        allowIn: true,
                        allowCall: true
                    }),
                    ')' + space + '{' + newline
                ];
            });
            if (stmt.cases) {
                for (i = 0, len = stmt.cases.length; i < len; i += 1) {
                    fragment = addIndent(generateStatement(stmt.cases[i], {semicolonOptional: i === len - 1}));
                    result.push(fragment);
                    if (!endsWithLineTerminator(toSourceNode(fragment).toString())) {
                        result.push(newline);
                    }
                }
            }
            result.push(addIndent('}'));
            break;

        case Syntax.SwitchCase:
            withIndent(function () {
                if (stmt.test) {
                    result = [
                        join('case', generateExpression(stmt.test, {
                            precedence: Precedence.Sequence,
                            allowIn: true,
                            allowCall: true
                        })),
                        ':'
                    ];
                } else {
                    result = ['default:'];
                }

                i = 0;
                len = stmt.consequent.length;
                if (len && stmt.consequent[0].type === Syntax.BlockStatement) {
                    fragment = maybeBlock(stmt.consequent[0]);
                    result.push(fragment);
                    i = 1;
                }

                if (i !== len && !endsWithLineTerminator(toSourceNode(result).toString())) {
                    result.push(newline);
                }

                for (; i < len; i += 1) {
                    fragment = addIndent(generateStatement(stmt.consequent[i], {semicolonOptional: i === len - 1 && semicolon === ''}));
                    result.push(fragment);
                    if (i + 1 !== len && !endsWithLineTerminator(toSourceNode(fragment).toString())) {
                        result.push(newline);
                    }
                }
            });
            break;

        case Syntax.IfStatement:
            withIndent(function () {
                result = [
                    'if' + space + '(',
                    generateExpression(stmt.test, {
                        precedence: Precedence.Sequence,
                        allowIn: true,
                        allowCall: true
                    }),
                    ')'
                ];
            });
            if (stmt.alternate) {
                result.push(maybeBlock(stmt.consequent));
                result = maybeBlockSuffix(stmt.consequent, result);
                if (stmt.alternate.type === Syntax.IfStatement) {
                    result = join(result, ['else ', generateStatement(stmt.alternate, {semicolonOptional: semicolon === ''})]);
                } else {
                    result = join(result, join('else', maybeBlock(stmt.alternate, semicolon === '')));
                }
            } else {
                result.push(maybeBlock(stmt.consequent, semicolon === ''));
            }
            break;

        case Syntax.ForStatement:
            withIndent(function () {
                result = ['for' + space + '('];
                if (stmt.init) {
                    if (stmt.init.type === Syntax.VariableDeclaration) {
                        result.push(generateStatement(stmt.init, {allowIn: false}));
                    } else {
                        result.push(generateExpression(stmt.init, {
                            precedence: Precedence.Sequence,
                            allowIn: false,
                            allowCall: true
                        }), ';');
                    }
                } else {
                    result.push(';');
                }

                if (stmt.test) {
                    result.push(space, generateExpression(stmt.test, {
                        precedence: Precedence.Sequence,
                        allowIn: true,
                        allowCall: true
                    }), ';');
                } else {
                    result.push(';');
                }

                if (stmt.update) {
                    result.push(space, generateExpression(stmt.update, {
                        precedence: Precedence.Sequence,
                        allowIn: true,
                        allowCall: true
                    }), ')');
                } else {
                    result.push(')');
                }
            });

            result.push(maybeBlock(stmt.body, semicolon === ''));
            break;

        case Syntax.ForInStatement:
            result = ['for' + space + '('];
            withIndent(function () {
                if (stmt.left.type === Syntax.VariableDeclaration) {
                    withIndent(function () {
                        result.push(stmt.left.kind + ' ', generateStatement(stmt.left.declarations[0], {
                            allowIn: false
                        }));
                    });
                } else {
                    result.push(generateExpression(stmt.left, {
                        precedence: Precedence.Call,
                        allowIn: true,
                        allowCall: true
                    }));
                }

                result = join(result, 'in');
                result = [join(
                    result,
                    generateExpression(stmt.right, {
                        precedence: Precedence.Sequence,
                        allowIn: true,
                        allowCall: true
                    })
                ), ')'];
            });
            result.push(maybeBlock(stmt.body, semicolon === ''));
            break;

        case Syntax.LabeledStatement:
            result = [stmt.label.name + ':', maybeBlock(stmt.body, semicolon === '')];
            break;

        case Syntax.Program:
            len = stmt.body.length;
            result = [safeConcatenation && len > 0 ? '\n' : ''];
            for (i = 0; i < len; i += 1) {
                fragment = addIndent(
                    generateStatement(stmt.body[i], {
                        semicolonOptional: !safeConcatenation && i === len - 1,
                        directiveContext: true
                    })
                );
                result.push(fragment);
                if (i + 1 < len && !endsWithLineTerminator(toSourceNode(fragment).toString())) {
                    result.push(newline);
                }
            }
            break;

        case Syntax.FunctionDeclaration:
            result = [(stmt.generator && !extra.moz.starlessGenerator ? 'function* ' : 'function ') + stmt.id.name, generateFunctionBody(stmt)];
            break;

        case Syntax.ReturnStatement:
            if (stmt.argument) {
                result = [join(
                    'return',
                    generateExpression(stmt.argument, {
                        precedence: Precedence.Sequence,
                        allowIn: true,
                        allowCall: true
                    })
                ), semicolon];
            } else {
                result = ['return' + semicolon];
            }
            break;

        case Syntax.WhileStatement:
            withIndent(function () {
                result = [
                    'while' + space + '(',
                    generateExpression(stmt.test, {
                        precedence: Precedence.Sequence,
                        allowIn: true,
                        allowCall: true
                    }),
                    ')'
                ];
            });
            result.push(maybeBlock(stmt.body, semicolon === ''));
            break;

        case Syntax.WithStatement:
            withIndent(function () {
                result = [
                    'with' + space + '(',
                    generateExpression(stmt.object, {
                        precedence: Precedence.Sequence,
                        allowIn: true,
                        allowCall: true
                    }),
                    ')'
                ];
            });
            result.push(maybeBlock(stmt.body, semicolon === ''));
            break;

        default:
            throw new Error('Unknown statement type: ' + stmt.type);
        }

        // Attach comments

        if (extra.comment) {
            result = addCommentsToStatement(stmt, result);
        }

        fragment = toSourceNode(result).toString();
        if (stmt.type === Syntax.Program && !safeConcatenation && newline === '' &&  fragment.charAt(fragment.length - 1) === '\n') {
            result = toSourceNode(result).replaceRight(/\s+$/, '');
        }

        return toSourceNode(result, stmt);
    }

    function generate(node, options) {
        var defaultOptions = getDefaultOptions(), result, pair;

        if (options != null) {
            // Obsolete options
            //
            //   `options.indent`
            //   `options.base`
            //
            // Instead of them, we can use `option.format.indent`.
            if (typeof options.indent === 'string') {
                defaultOptions.format.indent.style = options.indent;
            }
            if (typeof options.base === 'number') {
                defaultOptions.format.indent.base = options.base;
            }
            options = updateDeeply(defaultOptions, options);
            indent = options.format.indent.style;
            if (typeof options.base === 'string') {
                base = options.base;
            } else {
                base = stringRepeat(indent, options.format.indent.base);
            }
        } else {
            options = defaultOptions;
            indent = options.format.indent.style;
            base = stringRepeat(indent, options.format.indent.base);
        }
        json = options.format.json;
        renumber = options.format.renumber;
        hexadecimal = json ? false : options.format.hexadecimal;
        quotes = json ? 'double' : options.format.quotes;
        escapeless = options.format.escapeless;
        if (options.format.compact) {
            newline = space = indent = base = '';
        } else {
            newline = '\n';
            space = ' ';
        }
        parentheses = options.format.parentheses;
        semicolons = options.format.semicolons;
        safeConcatenation = options.format.safeConcatenation;
        directive = options.directive;
        parse = json ? null : options.parse;
        sourceMap = options.sourceMap;
        extra = options;

        if (sourceMap) {
            if (!exports.browser) {
                // We assume environment is node.js
                // And prevent from including source-map by browserify
                SourceNode = require('source-map').SourceNode;
            } else {
                SourceNode = global.sourceMap.SourceNode;
            }
        } else {
            SourceNode = SourceNodeMock;
        }

        switch (node.type) {
        case Syntax.BlockStatement:
        case Syntax.BreakStatement:
        case Syntax.CatchClause:
        case Syntax.ContinueStatement:
        case Syntax.DirectiveStatement:
        case Syntax.DoWhileStatement:
        case Syntax.DebuggerStatement:
        case Syntax.EmptyStatement:
        case Syntax.ExpressionStatement:
        case Syntax.ForStatement:
        case Syntax.ForInStatement:
        case Syntax.FunctionDeclaration:
        case Syntax.IfStatement:
        case Syntax.LabeledStatement:
        case Syntax.Program:
        case Syntax.ReturnStatement:
        case Syntax.SwitchStatement:
        case Syntax.SwitchCase:
        case Syntax.ThrowStatement:
        case Syntax.TryStatement:
        case Syntax.VariableDeclaration:
        case Syntax.VariableDeclarator:
        case Syntax.WhileStatement:
        case Syntax.WithStatement:
            result = generateStatement(node);
            break;

        case Syntax.AssignmentExpression:
        case Syntax.ArrayExpression:
        case Syntax.ArrayPattern:
        case Syntax.BinaryExpression:
        case Syntax.CallExpression:
        case Syntax.ConditionalExpression:
        case Syntax.FunctionExpression:
        case Syntax.Identifier:
        case Syntax.Literal:
        case Syntax.LogicalExpression:
        case Syntax.MemberExpression:
        case Syntax.NewExpression:
        case Syntax.ObjectExpression:
        case Syntax.ObjectPattern:
        case Syntax.Property:
        case Syntax.SequenceExpression:
        case Syntax.ThisExpression:
        case Syntax.UnaryExpression:
        case Syntax.UpdateExpression:
        case Syntax.YieldExpression:

            result = generateExpression(node, {
                precedence: Precedence.Sequence,
                allowIn: true,
                allowCall: true
            });
            break;

        default:
            throw new Error('Unknown node type: ' + node.type);
        }

        if (!sourceMap) {
            return result.toString();
        }

        pair = result.toStringWithSourceMap({
            file: options.sourceMap,
            sourceRoot: options.sourceMapRoot
        });

        if (options.sourceMapWithCode) {
            return pair;
        }
        return pair.map.toString();
    }

    // simple visitor implementation

    VisitorKeys = {
        AssignmentExpression: ['left', 'right'],
        ArrayExpression: ['elements'],
        ArrayPattern: ['elements'],
        BlockStatement: ['body'],
        BinaryExpression: ['left', 'right'],
        BreakStatement: ['label'],
        CallExpression: ['callee', 'arguments'],
        CatchClause: ['param', 'body'],
        ConditionalExpression: ['test', 'consequent', 'alternate'],
        ContinueStatement: ['label'],
        DirectiveStatement: [],
        DoWhileStatement: ['body', 'test'],
        DebuggerStatement: [],
        EmptyStatement: [],
        ExpressionStatement: ['expression'],
        ForStatement: ['init', 'test', 'update', 'body'],
        ForInStatement: ['left', 'right', 'body'],
        FunctionDeclaration: ['id', 'params', 'body'],
        FunctionExpression: ['id', 'params', 'body'],
        Identifier: [],
        IfStatement: ['test', 'consequent', 'alternate'],
        Literal: [],
        LabeledStatement: ['label', 'body'],
        LogicalExpression: ['left', 'right'],
        MemberExpression: ['object', 'property'],
        NewExpression: ['callee', 'arguments'],
        ObjectExpression: ['properties'],
        ObjectPattern: ['properties'],
        Program: ['body'],
        Property: ['key', 'value'],
        ReturnStatement: ['argument'],
        SequenceExpression: ['expressions'],
        SwitchStatement: ['discriminant', 'cases'],
        SwitchCase: ['test', 'consequent'],
        ThisExpression: [],
        ThrowStatement: ['argument'],
        TryStatement: ['block', 'handlers', 'finalizer'],
        UnaryExpression: ['argument'],
        UpdateExpression: ['argument'],
        VariableDeclaration: ['declarations'],
        VariableDeclarator: ['id', 'init'],
        WhileStatement: ['test', 'body'],
        WithStatement: ['object', 'body'],
        YieldExpression: ['argument']
    };

    VisitorOption = {
        Break: 1,
        Skip: 2
    };

    // based on LLVM libc++ upper_bound / lower_bound
    // MIT License

    function upperBound(array, func) {
        var diff, len, i, current;

        len = array.length;
        i = 0;

        while (len) {
            diff = len >>> 1;
            current = i + diff;
            if (func(array[current])) {
                len = diff;
            } else {
                i = current + 1;
                len -= diff + 1;
            }
        }
        return i;
    }

    function lowerBound(array, func) {
        var diff, len, i, current;

        len = array.length;
        i = 0;

        while (len) {
            diff = len >>> 1;
            current = i + diff;
            if (func(array[current])) {
                i = current + 1;
                len -= diff + 1;
            } else {
                len = diff;
            }
        }
        return i;
    }

    function extendCommentRange(comment, tokens) {
        var target, token;

        target = upperBound(tokens, function search(token) {
            return token.range[0] > comment.range[0];
        });

        comment.extendedRange = [comment.range[0], comment.range[1]];

        if (target !== tokens.length) {
            comment.extendedRange[1] = tokens[target].range[0];
        }

        target -= 1;
        if (target >= 0) {
            if (target < tokens.length) {
                comment.extendedRange[0] = tokens[target].range[1];
            } else if (token.length) {
                comment.extendedRange[1] = tokens[tokens.length - 1].range[0];
            }
        }

        return comment;
    }

    function attachComments(tree, providedComments, tokens) {
        // At first, we should calculate extended comment ranges.
        var comments = [], comment, len, i;

        if (!tree.range) {
            throw new Error('attachComments needs range information');
        }

        // tokens array is empty, we attach comments to tree as 'leadingComments'
        if (!tokens.length) {
            if (providedComments.length) {
                for (i = 0, len = providedComments.length; i < len; i += 1) {
                    comment = deepCopy(providedComments[i]);
                    comment.extendedRange = [0, tree.range[0]];
                    comments.push(comment);
                }
                tree.leadingComments = comments;
            }
            return tree;
        }

        for (i = 0, len = providedComments.length; i < len; i += 1) {
            comments.push(extendCommentRange(deepCopy(providedComments[i]), tokens));
        }

        // This is based on John Freeman's implementation.
        traverse(tree, {
            cursor: 0,
            enter: function (node) {
                var comment;

                while (this.cursor < comments.length) {
                    comment = comments[this.cursor];
                    if (comment.extendedRange[1] > node.range[0]) {
                        break;
                    }

                    if (comment.extendedRange[1] === node.range[0]) {
                        if (!node.leadingComments) {
                            node.leadingComments = [];
                        }
                        node.leadingComments.push(comment);
                        comments.splice(this.cursor, 1);
                    } else {
                        this.cursor += 1;
                    }
                }

                // already out of owned node
                if (this.cursor === comments.length) {
                    return VisitorOption.Break;
                }

                if (comments[this.cursor].extendedRange[0] > node.range[1]) {
                    return VisitorOption.Skip;
                }
            }
        });

        traverse(tree, {
            cursor: 0,
            leave: function (node) {
                var comment;

                while (this.cursor < comments.length) {
                    comment = comments[this.cursor];
                    if (node.range[1] < comment.extendedRange[0]) {
                        break;
                    }

                    if (node.range[1] === comment.extendedRange[0]) {
                        if (!node.trailingComments) {
                            node.trailingComments = [];
                        }
                        node.trailingComments.push(comment);
                        comments.splice(this.cursor, 1);
                    } else {
                        this.cursor += 1;
                    }
                }

                // already out of owned node
                if (this.cursor === comments.length) {
                    return VisitorOption.Break;
                }

                if (comments[this.cursor].extendedRange[0] > node.range[1]) {
                    return VisitorOption.Skip;
                }
            }
        });

        return tree;
    }

    exports.version = require('./package.json').version;
    exports.generate = generate;
    exports.attachComments = attachComments;
    exports.browser = false;
}());
/* vim: set sw=4 ts=4 et tw=80 : */

});

require.define("/node_modules/estraverse/package.json",function(require,module,exports,__dirname,__filename,process,global){module.exports = {"main":"estraverse.js"}
});

require.define("/node_modules/estraverse/estraverse.js",function(require,module,exports,__dirname,__filename,process,global){/*
  Copyright (C) 2012 Yusuke Suzuki <utatane.tea@gmail.com>
  Copyright (C) 2012 Ariya Hidayat <ariya.hidayat@gmail.com>

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*jslint bitwise:true */
/*global exports:true, define:true, window:true */
(function (factory) {
    'use strict';

    // Universal Module Definition (UMD) to support AMD, CommonJS/Node.js,
    // and plain browser loading,
    if (typeof define === 'function' && define.amd) {
        define(['exports'], factory);
    } else if (typeof exports !== 'undefined') {
        factory(exports);
    } else {
        factory((window.estraverse = {}));
    }
}(function (exports) {
    'use strict';

    var Syntax,
        isArray,
        VisitorOption,
        VisitorKeys,
        wrappers;

    Syntax = {
        AssignmentExpression: 'AssignmentExpression',
        ArrayExpression: 'ArrayExpression',
        BlockStatement: 'BlockStatement',
        BinaryExpression: 'BinaryExpression',
        BreakStatement: 'BreakStatement',
        CallExpression: 'CallExpression',
        CatchClause: 'CatchClause',
        ConditionalExpression: 'ConditionalExpression',
        ContinueStatement: 'ContinueStatement',
        DebuggerStatement: 'DebuggerStatement',
        DirectiveStatement: 'DirectiveStatement',
        DoWhileStatement: 'DoWhileStatement',
        EmptyStatement: 'EmptyStatement',
        ExpressionStatement: 'ExpressionStatement',
        ForStatement: 'ForStatement',
        ForInStatement: 'ForInStatement',
        FunctionDeclaration: 'FunctionDeclaration',
        FunctionExpression: 'FunctionExpression',
        Identifier: 'Identifier',
        IfStatement: 'IfStatement',
        Literal: 'Literal',
        LabeledStatement: 'LabeledStatement',
        LogicalExpression: 'LogicalExpression',
        MemberExpression: 'MemberExpression',
        NewExpression: 'NewExpression',
        ObjectExpression: 'ObjectExpression',
        Program: 'Program',
        Property: 'Property',
        ReturnStatement: 'ReturnStatement',
        SequenceExpression: 'SequenceExpression',
        SwitchStatement: 'SwitchStatement',
        SwitchCase: 'SwitchCase',
        ThisExpression: 'ThisExpression',
        ThrowStatement: 'ThrowStatement',
        TryStatement: 'TryStatement',
        UnaryExpression: 'UnaryExpression',
        UpdateExpression: 'UpdateExpression',
        VariableDeclaration: 'VariableDeclaration',
        VariableDeclarator: 'VariableDeclarator',
        WhileStatement: 'WhileStatement',
        WithStatement: 'WithStatement'
    };

    isArray = Array.isArray;
    if (!isArray) {
        isArray = function isArray(array) {
            return Object.prototype.toString.call(array) === '[object Array]';
        };
    }

    VisitorKeys = {
        AssignmentExpression: ['left', 'right'],
        ArrayExpression: ['elements'],
        BlockStatement: ['body'],
        BinaryExpression: ['left', 'right'],
        BreakStatement: ['label'],
        CallExpression: ['callee', 'arguments'],
        CatchClause: ['param', 'body'],
        ConditionalExpression: ['test', 'consequent', 'alternate'],
        ContinueStatement: ['label'],
        DebuggerStatement: [],
        DirectiveStatement: [],
        DoWhileStatement: ['body', 'test'],
        EmptyStatement: [],
        ExpressionStatement: ['expression'],
        ForStatement: ['init', 'test', 'update', 'body'],
        ForInStatement: ['left', 'right', 'body'],
        FunctionDeclaration: ['id', 'params', 'body'],
        FunctionExpression: ['id', 'params', 'body'],
        Identifier: [],
        IfStatement: ['test', 'consequent', 'alternate'],
        Literal: [],
        LabeledStatement: ['label', 'body'],
        LogicalExpression: ['left', 'right'],
        MemberExpression: ['object', 'property'],
        NewExpression: ['callee', 'arguments'],
        ObjectExpression: ['properties'],
        Program: ['body'],
        Property: ['key', 'value'],
        ReturnStatement: ['argument'],
        SequenceExpression: ['expressions'],
        SwitchStatement: ['discriminant', 'cases'],
        SwitchCase: ['test', 'consequent'],
        ThisExpression: [],
        ThrowStatement: ['argument'],
        TryStatement: ['block', 'handlers', 'finalizer'],
        UnaryExpression: ['argument'],
        UpdateExpression: ['argument'],
        VariableDeclaration: ['declarations'],
        VariableDeclarator: ['id', 'init'],
        WhileStatement: ['test', 'body'],
        WithStatement: ['object', 'body']
    };

    VisitorOption = {
        Break: 1,
        Skip: 2
    };

    wrappers = {
        PropertyWrapper: 'Property'
    };

    function traverse(top, visitor) {
        var worklist, leavelist, node, nodeType, ret, current, current2, candidates, candidate, marker = {};

        worklist = [ top ];
        leavelist = [ null ];

        while (worklist.length) {
            node = worklist.pop();
            nodeType = node.type;

            if (node === marker) {
                node = leavelist.pop();
                if (visitor.leave) {
                    ret = visitor.leave(node, leavelist[leavelist.length - 1]);
                } else {
                    ret = undefined;
                }
                if (ret === VisitorOption.Break) {
                    return;
                }
            } else if (node) {
                if (wrappers.hasOwnProperty(nodeType)) {
                    node = node.node;
                    nodeType = wrappers[nodeType];
                }

                if (visitor.enter) {
                    ret = visitor.enter(node, leavelist[leavelist.length - 1]);
                } else {
                    ret = undefined;
                }

                if (ret === VisitorOption.Break) {
                    return;
                }

                worklist.push(marker);
                leavelist.push(node);

                if (ret !== VisitorOption.Skip) {
                    candidates = VisitorKeys[nodeType];
                    current = candidates.length;
                    while ((current -= 1) >= 0) {
                        candidate = node[candidates[current]];
                        if (candidate) {
                            if (isArray(candidate)) {
                                current2 = candidate.length;
                                while ((current2 -= 1) >= 0) {
                                    if (candidate[current2]) {
                                        if(nodeType === Syntax.ObjectExpression && 'properties' === candidates[current] && null == candidates[current].type) {
                                            worklist.push({type: 'PropertyWrapper', node: candidate[current2]});
                                        } else {
                                            worklist.push(candidate[current2]);
                                        }
                                    }
                                }
                            } else {
                                worklist.push(candidate);
                            }
                        }
                    }
                }
            }
        }
    }

    function replace(top, visitor) {
        var worklist, leavelist, node, nodeType, target, tuple, ret, current, current2, candidates, candidate, marker = {}, result;

        result = {
            top: top
        };

        tuple = [ top, result, 'top' ];
        worklist = [ tuple ];
        leavelist = [ tuple ];

        function notify(v) {
            ret = v;
        }

        while (worklist.length) {
            tuple = worklist.pop();

            if (tuple === marker) {
                tuple = leavelist.pop();
                ret = undefined;
                if (visitor.leave) {
                    node = tuple[0];
                    target = visitor.leave(tuple[0], leavelist[leavelist.length - 1][0], notify);
                    if (target !== undefined) {
                        node = target;
                    }
                    tuple[1][tuple[2]] = node;
                }
                if (ret === VisitorOption.Break) {
                    return result.top;
                }
            } else if (tuple[0]) {
                ret = undefined;
                node = tuple[0];

                nodeType = node.type;
                if (wrappers.hasOwnProperty(nodeType)) {
                    tuple[0] = node = node.node;
                    nodeType = wrappers[nodeType];
                }

                if (visitor.enter) {
                    target = visitor.enter(tuple[0], leavelist[leavelist.length - 1][0], notify);
                    if (target !== undefined) {
                        node = target;
                    }
                    tuple[1][tuple[2]] = node;
                    tuple[0] = node;
                }

                if (ret === VisitorOption.Break) {
                    return result.top;
                }

                if (tuple[0]) {
                    worklist.push(marker);
                    leavelist.push(tuple);

                    if (ret !== VisitorOption.Skip) {
                        candidates = VisitorKeys[nodeType];
                        current = candidates.length;
                        while ((current -= 1) >= 0) {
                            candidate = node[candidates[current]];
                            if (candidate) {
                                if (isArray(candidate)) {
                                    current2 = candidate.length;
                                    while ((current2 -= 1) >= 0) {
                                        if (candidate[current2]) {
                                            if(nodeType === Syntax.ObjectExpression && 'properties' === candidates[current] && null == candidates[current].type) {
                                                worklist.push([{type: 'PropertyWrapper', node: candidate[current2]}, candidate, current2]);
                                            } else {
                                                worklist.push([candidate[current2], candidate, current2]);
                                            }
                                        }
                                    }
                                } else {
                                    worklist.push([candidate, node, candidates[current]]);
                                }
                            }
                        }
                    }
                }
            }
        }

        return result.top;
    }

    exports.version = '0.0.4';
    exports.Syntax = Syntax;
    exports.traverse = traverse;
    exports.replace = replace;
    exports.VisitorKeys = VisitorKeys;
    exports.VisitorOption = VisitorOption;
}));
/* vim: set sw=4 ts=4 et tw=80 : */

});

require.define("/tools/entry-point.js",function(require,module,exports,__dirname,__filename,process,global){/*
  Copyright (C) 2012 Yusuke Suzuki <utatane.tea@gmail.com>

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

(function () {
    'use strict';
    var escodegen;
    escodegen = global.escodegen = require('../escodegen');
    escodegen.browser = true;
}());

});
require("/tools/entry-point.js");
})();

;
/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

/*global esprima, escodegen, window */
(function (isNode) {
    "use strict";
    var SYNTAX,
        nodeType,
        ESP = isNode ? require('esprima') : esprima,
        ESPGEN = isNode ? require('escodegen') : escodegen,  //TODO - package as dependency
        crypto = isNode ? require('crypto') : null,
        LEADER_WRAP = '(function () { ',
        TRAILER_WRAP = '\n}());',
        astgen,
        preconditions,
        cond,
        isArray = Array.isArray;

    if (!isArray) {
        isArray = function (thing) { return thing &&  Object.prototype.toString.call(thing) === '[object Array]'; };
    }

    if (!isNode) {
        preconditions = {
            'Could not find esprima': ESP,
            'Could not find escodegen': ESPGEN,
            'JSON object not in scope': JSON,
            'Array does not implement push': [].push,
            'Array does not implement unshift': [].unshift
        };
        for (cond in preconditions) {
            if (preconditions.hasOwnProperty(cond)) {
                if (!preconditions[cond]) { throw new Error(cond); }
            }
        }
    }

    function generateTrackerVar(filename, omitSuffix) {
        var hash, suffix;
        if (crypto !== null) {
            hash = crypto.createHash('md5');
            hash.update(filename);
            suffix = hash.digest('base64');
            //trim trailing equal signs, turn identifier unsafe chars to safe ones + => _ and / => $
            suffix = suffix.replace(new RegExp('=', 'g'), '')
                .replace(new RegExp('\\+', 'g'), '_')
                .replace(new RegExp('/', 'g'), '$');
        } else {
            window.__cov_seq = window.__cov_seq || 0;
            window.__cov_seq += 1;
            suffix = window.__cov_seq;
        }
        return '__cov_' + (omitSuffix ? '' : suffix);
    }

    function pushAll(ary, thing) {
        if (!isArray(thing)) {
            thing = [ thing ];
        }
        Array.prototype.push.apply(ary, thing);
    }

    SYNTAX = {
        ArrayExpression: [ 'elements' ],
        AssignmentExpression: ['left', 'right'],
        BinaryExpression: ['left', 'right' ],
        BlockStatement: [ 'body' ],
        BreakStatement: [ 'label' ],
        CallExpression: [ 'callee', 'arguments'],
        CatchClause: ['param', 'body'],
        ConditionalExpression: [ 'test', 'consequent', 'alternate' ],
        ContinueStatement: [ 'label' ],
        DebuggerStatement: [ ],
        DoWhileStatement: [ 'test', 'body' ],
        EmptyStatement: [],
        ExpressionStatement: [ 'expression'],
        ForInStatement: [ 'left', 'right', 'body' ],
        ForStatement: ['init', 'test', 'update', 'body' ],
        FunctionDeclaration: ['id', 'params', 'body'],
        FunctionExpression: ['id', 'params', 'defaults', 'body'],
        Identifier: [],
        IfStatement: ['test', 'consequent', 'alternate'],
        LabeledStatement: ['label', 'body'],
        Literal: [],
        LogicalExpression: [ 'left', 'right' ],
        MemberExpression: ['object', 'property'],
        NewExpression: ['callee', 'arguments'],
        ObjectExpression: [ 'properties' ],
        Program: [ 'body' ],
        Property: [ 'key', 'value'],
        ReturnStatement: ['argument'],
        SequenceExpression: ['expressions'],
        SwitchCase: [ 'test', 'consequent' ],
        SwitchStatement: ['discriminant', 'cases' ],
        ThisExpression: [],
        ThrowStatement: ['argument'],
        TryStatement: [ 'block', 'handlers', 'finalizer' ],
        UnaryExpression: ['argument'],
        UpdateExpression: [ 'argument' ],
        VariableDeclaration: [ 'declarations' ],
        VariableDeclarator: [ 'id', 'init' ],
        WhileStatement: [ 'test', 'body' ],
        WithStatement: [ 'object', 'body' ]

    };

    for (nodeType in SYNTAX) {
        if (SYNTAX.hasOwnProperty(nodeType)) {
            SYNTAX[nodeType] = { name: nodeType, children: SYNTAX[nodeType] };
        }
    }

    astgen = {
        variable: function (name) { return { type: SYNTAX.Identifier.name, name: name }; },
        stringLiteral: function (str) { return { type: SYNTAX.Literal.name, value: String(str) }; },
        numericLiteral: function (num) { return { type: SYNTAX.Literal.name, value: Number(num) }; },
        statement: function (contents) { return { type: SYNTAX.ExpressionStatement.name, expression: contents }; },
        dot: function (obj, field) { return { type: SYNTAX.MemberExpression.name, computed: false, object: obj, property: field }; },
        subscript: function (obj, sub) { return { type: SYNTAX.MemberExpression.name, computed: true, object: obj, property: sub }; },
        postIncrement: function (obj) { return { type: SYNTAX.UpdateExpression.name, operator: '++', prefix: false, argument: obj }; },
        sequence: function (one, two) { return { type: SYNTAX.SequenceExpression.name, expressions: [one, two] }; }
    };

    function Walker(walkMap, scope, debug) {
        this.walkMap = walkMap;
        this.scope = scope;
        this.debug = debug;
        if (this.debug) {
            this.level = 0;
            this.seq = true;
        }
    }

    function defaultWalker(node, walker) {

        var type = node.type,
            children = SYNTAX[type].children,
            // don't run generated nodes thru custom walks otherwise we will attempt to instrument the instrumentation code :)
            applyCustomWalker = !!node.loc || node.type === SYNTAX.Program.name,
            walkerFn = applyCustomWalker ? walker.walkMap[type] : null,
            i,
            j,
            walkFnIndex,
            childType,
            childNode,
            ret = node,
            childArray,
            childElement,
            pathElement,
            assignNode,
            isLast;

        if (node.walking) { throw new Error('Infinite regress: Custom walkers may NOT call walker.walk(node)'); }
        node.walking = true;
        if (isArray(walkerFn)) {
            for (walkFnIndex = 0; walkFnIndex < walkerFn.length; walkFnIndex += 1) {
                isLast = walkFnIndex === walkerFn.length - 1;
                ret = walker.walk(ret, walkerFn[walkFnIndex]) || ret;
                if (ret.type !== type && !isLast) {
                    throw new Error('Only the last walker is allowed to change the node type: [type was: ' + type + ' ]');
                }
            }
        } else {
            if (walkerFn) {
                ret = walker.walk(node, walkerFn) || ret;
            }
        }

        for (i = 0; i < children.length; i += 1) {
            childType = children[i];
            childNode = node[childType];
            if (childNode && !childNode.skipWalk) {
                pathElement = { node: node, property: childType };
                if (isArray(childNode)) {
                    childArray = [];
                    for (j = 0; j < childNode.length; j += 1) {
                        childElement = childNode[j];
                        pathElement.index = j;
                        if (childElement) {
                          assignNode = walker.walk(childElement, null, pathElement) || childElement;
                          if (isArray(assignNode.prepend)) {
                              pushAll(childArray, assignNode.prepend);
                              delete assignNode.prepend;
                          }
                        }
                        pushAll(childArray, assignNode);
                    }
                    node[childType] = childArray;
                } else {
                    assignNode = walker.walk(childNode, null, pathElement) || childNode;
                    if (isArray(assignNode.prepend)) {
                        throw new Error('Internal error: attempt to prepend statements in disallowed (non-array) context');
                        /* if this should be allowed, this is how to solve it
                        tmpNode = { type: 'BlockStatement', body: [] };
                        pushAll(tmpNode.body, assignNode.prepend);
                        pushAll(tmpNode.body, assignNode);
                        node[childType] = tmpNode;
                        delete assignNode.prepend;
                        */
                    } else {
                        node[childType] = assignNode;
                    }
                }
            }
        }

        delete node.walking;
        return ret;
    }

    Walker.prototype = {
        startWalk: function (node) {
            this.path = [];
            this.walk(node);
        },

        walk: function (node, walkFn, pathElement) {
            var ret, i, seq, prefix;

            walkFn = walkFn || defaultWalker;
            if (this.debug) {
                this.seq += 1;
                this.level += 1;
                seq = this.seq;
                prefix = '';
                for (i = 0; i < this.level; i += 1) { prefix += '    '; }
                console.log(prefix + 'Enter (' + seq + '):' + node.type);
            }
            if (pathElement) { this.path.push(pathElement); }
            ret = walkFn.call(this.scope, node, this);
            if (pathElement) { this.path.pop(); }
            if (this.debug) {
                this.level -= 1;
                console.log(prefix + 'Return (' + seq + '):' + node.type);
            }
            return ret;
        },

        startLineForNode: function (node) {
            return node && node.loc && node.loc.start ? node.loc.start.line : null;
        },

        ancestor: function (n) {
            return this.path.length > n - 1 ? this.path[this.path.length - n] : null;
        },

        parent: function () {
            return this.ancestor(1);
        },

        isLabeled: function () {
            var el = this.parent();
            return el && el.node.type === SYNTAX.LabeledStatement.name;
        }
    };

    /**
     * mechanism to instrument code for coverage. It uses the `esprima` and
     * `escodegen` libraries for JS parsing and code generation respectively.
     *
     * Works on `node` as well as the browser.
     *
     * Usage on nodejs
     * ---------------
     *
     *      var instrumenter = new require('istanbul').Instrumenter(),
     *          changed = instrumenter.instrumentSync('function meaningOfLife() { return 42; }', 'filename.js');
     *
     * Usage in a browser
     * ------------------
     *
     * Load `esprima.js`, `escodegen.js` and `instrumenter.js` (this file) using `script` tags or other means.
     *
     * Create an instrumenter object as:
     *
     *      var instrumenter = new Instrumenter(),
     *          changed = instrumenter.instrumentSync('function meaningOfLife() { return 42; }', 'filename.js');
     *
     * Aside from demonstration purposes, it is unclear why you would want to instrument code in a browser.
     *
     * @class Instrumenter
     * @constructor
     * @param {Object} options Optional. Configuration options.
     * @param {String} [options.coverageVariable] the global variable name to use for
     *      tracking coverage. Defaults to `__coverage__`
     * @param {Boolean} [options.embedSource] whether to embed the source code of every
     *      file as an array in the file coverage object for that file. Defaults to `false`
     * @param {Boolean} [options.noCompact] emit readable code when set. Defaults to `false`
     * @param {Boolean} [options.noAutoWrap] do not automatically wrap the source in
     *      an anonymous function before covering it. By default, code is wrapped in
     *      an anonymous function before it is parsed. This is done because
     *      some nodejs libraries have `return` statements outside of
     *      a function which is technically invalid Javascript and causes the parser to fail.
     *      This construct, however, works correctly in node since module loading
     *      is done in the context of an anonymous function.
     *
     * Note that the semantics of the code *returned* by the instrumenter does not change in any way.
     * The function wrapper is "unwrapped" before the instrumented code is generated.
     * @param {Object} [options.codeGenerationOptions] an object that is directly passed to the `escodegen`
     *      library as configuration for code generation. The `noCompact` setting is not honored when this
     *      option is specified
     * @param {Boolean} [options.debug] assist in debugging. Currently, the only effect of
     *      setting this option is a pretty-print of the coverage variable. Defaults to `false`
     * @param {Boolean} [options.walkDebug] assist in debugging of the AST walker used by this class.
     *
     */
    function Instrumenter(options) {
        this.opts = options || {
            debug: false,
            walkDebug: false,
            coverageVariable: '__coverage__',
            codeGenerationOptions: undefined,
            noAutoWrap: false,
            noCompact: false,
            embedSource: false
        };

        this.walker = new Walker({
            ExpressionStatement: this.coverStatement,
            BreakStatement: this.coverStatement,
            ContinueStatement: this.coverStatement,
            DebuggerStatement: this.coverStatement,
            ReturnStatement: this.coverStatement,
            ThrowStatement: this.coverStatement,
            TryStatement: this.coverStatement,
            VariableDeclaration: this.coverStatement,
            IfStatement: [ this.ifBlockConverter, this.ifBranchInjector, this.coverStatement ],
            ForStatement: [ this.skipInit, this.loopBlockConverter, this.coverStatement ],
            ForInStatement: [ this.skipLeft, this.loopBlockConverter, this.coverStatement ],
            WhileStatement: [ this.loopBlockConverter, this.coverStatement ],
            DoWhileStatement: [ this.loopBlockConverter, this.coverStatement ],
            SwitchStatement: [ this.switchBranchInjector, this.coverStatement ],
            WithStatement: this.coverStatement,
            FunctionDeclaration: [ this.coverFunction, this.coverStatement ],
            FunctionExpression: this.coverFunction,
            LabeledStatement: this.coverStatement,
            ConditionalExpression: this.conditionalBranchInjector,
            LogicalExpression: this.logicalExpressionBranchInjector
        }, this, this.opts.walkDebug);

        //unit testing purposes only
        if (this.opts.backdoor && this.opts.backdoor.omitTrackerSuffix) {
            this.omitTrackerSuffix = true;
        }
    }

    Instrumenter.prototype = {
        /**
         * synchronous instrumentation method. Throws when illegal code is passed to it
         * @method instrumentSync
         * @param {String} code the code to be instrumented as a String
         * @param {String} filename Optional. The name of the file from which
         *  the code was read. A temporary filename is generated when not specified.
         *  Not specifying a filename is only useful for unit tests and demonstrations
         *  of this library.
         */
        instrumentSync: function (code, filename) {
            var program;

            //protect from users accidentally passing in a Buffer object instead
            if (typeof code !== 'string') { throw new Error('Code must be string'); }
            if (code.charAt(0) === '#') { //shebang, 'comment' it out, won't affect syntax tree locations for things we care about
                code = '//' + code;
            }
            if (!this.opts.noAutoWrap) {
                code = LEADER_WRAP + code + TRAILER_WRAP;
            }
            program = ESP.parse(code, { loc: true });
            if (!this.opts.noAutoWrap) {
                program = { type: SYNTAX.Program.name, body: program.body[0].expression.callee.body.body };
            }
            return this.instrumentASTSync(program, filename);
        },
        /**
         * synchronous instrumentation method that instruments an AST instead.
         * @method instrumentASTSync
         * @param {String} program the AST to be instrumented
         * @param {String} filename Optional. The name of the file from which
         *  the code was read. A temporary filename is generated when not specified.
         *  Not specifying a filename is only useful for unit tests and demonstrations
         *  of this library.
         */
        instrumentASTSync: function (program, filename) {
            filename = filename || String(new Date().getTime()) + '.js';
            this.coverState = {
                path: filename,
                s: {},
                b: {},
                f: {},
                fnMap: {},
                statementMap: {},
                branchMap: {}
            };
            this.currentState = {
                trackerVar: generateTrackerVar(filename, this.omitTrackerSuffix),
                func: 0,
                branch: 0,
                variable: 0,
                statement: 0
            };
            this.walker.startWalk(program);
            var codegenOptions = this.opts.codeGenerationOptions || { format: { compact: !this.opts.noCompact }};
            //console.log(JSON.stringify(program, undefined, 2));
            return this.getPreamble("") + '\n' + ESPGEN.generate(program, codegenOptions) + '\n';
        },
        /**
         * Callback based instrumentation. Note that this still executes synchronously in the same process tick
         * and calls back immediately. It only provides the options for callback style error handling as
         * opposed to a `try-catch` style and nothing more. Implemented as a wrapper over `instrumentSync`
         *
         * @method instrument
         * @param {String} code the code to be instrumented as a String
         * @param {String} filename Optional. The name of the file from which
         *  the code was read. A temporary filename is generated when not specified.
         *  Not specifying a filename is only useful for unit tests and demonstrations
         *  of this library.
         * @param {Function(err, instrumentedCode)} callback - the callback function
         */
        instrument: function (code, filename, callback) {

            if (!callback && typeof filename === 'function') {
                callback = filename;
                filename = null;
            }
            try {
                callback(null, this.instrumentSync(code, filename));
            } catch (ex) {
                callback(ex);
            }
        },
        /**
         * returns the file coverage object for the code that was instrumented
         * just before calling this method. Note that this represents a
         * "zero-coverage" object which is not even representative of the code
         * being loaded in node or a browser (which would increase the statement
         * counts for mainline code).
         * @return {Object} a "zero-coverage" file coverage object for the code last instrumented
         * by this instrumenter
         */
        lastFileCoverage: function () {
            return this.coverState;
        },
        fixColumnPositions: function (coverState) {
            var offset = LEADER_WRAP.length,
                fixer = function (loc) {
                    if (loc.start.line === 1) {
                        loc.start.column -= offset;
                    }
                    if (loc.end.line === 1) {
                        loc.end.column -= offset;
                    }
                },
                k,
                obj,
                i,
                locations;

            obj = coverState.statementMap;
            for (k in obj) {
                if (obj.hasOwnProperty(k)) { fixer(obj[k]); }
            }
            obj = coverState.fnMap;
            for (k in obj) {
                if (obj.hasOwnProperty(k)) { fixer(obj[k].loc); }
            }
            obj = coverState.branchMap;
            for (k in obj) {
                if (obj.hasOwnProperty(k)) {
                    locations = obj[k].locations;
                    for (i = 0; i < locations.length; i += 1) {
                        fixer(locations[i]);
                    }
                }
            }
        },

        getPreamble: function (sourceCode) {
            var varName = this.opts.coverageVariable || '__coverage__',
                file = this.coverState.path.replace(/\\/g, '\\\\'),
                tracker = this.currentState.trackerVar,
                coverState,
                // return replacements using the function to ensure that the replacement is
                // treated like a dumb string and not as a string with RE replacement patterns
                replacer = function (s) {
                    return function () { return s; };
                },
                code;
            if (!this.opts.noAutoWrap) {
                this.fixColumnPositions(this.coverState);
            }
            if (this.opts.embedSource) {
                this.coverState.code = sourceCode.split(/\n/);
            }
            coverState = this.opts.debug ? JSON.stringify(this.coverState, undefined, 4) : JSON.stringify(this.coverState);
            code = [
                "if (typeof %GLOBAL% === 'undefined') { %GLOBAL% = {}; }",
                "if (!%GLOBAL%['%FILE%']) {",
                "   %GLOBAL%['%FILE%'] = %OBJECT%;",
                "}",
                "var %VAR% = %GLOBAL%['%FILE%'];"
            ].join("\n")
                .replace(/%VAR%/g, replacer(tracker))
                .replace(/%GLOBAL%/g, replacer(varName))
                .replace(/%FILE%/g, replacer(file))
                .replace(/%OBJECT%/g, replacer(coverState));
            return code;
        },

        convertToBlock: function (node) {
            if (!node) {
                return { type: 'BlockStatement', body: [] };
            } else if (node.type === 'BlockStatement') {
                return node;
            } else {
                return { type: 'BlockStatement', body: [ node ] };
            }
        },

        ifBlockConverter: function (node) {
            node.consequent = this.convertToBlock(node.consequent);
            node.alternate = this.convertToBlock(node.alternate);
        },

        loopBlockConverter: function (node) {
            node.body = this.convertToBlock(node.body);
        },

        statementName: function (location, initValue) {
            initValue = initValue || 0;
            var sName;
            this.currentState.statement += 1;
            sName = this.currentState.statement;
            this.coverState.statementMap[sName] = location;
            this.coverState.s[sName] = initValue;
            return sName;
        },

        skipInit: function (node /*, walker */) {
            if (node.init) {
                node.init.skipWalk = true;
            }
        },

        skipLeft: function (node /*, walker */) {
            node.left.skipWalk = true;
        },

        isUseStrictExpression: function (node) {
            return node && node.type === SYNTAX.ExpressionStatement.name &&
                node.expression  && node.expression.type === SYNTAX.Literal.name &&
                node.expression.value === 'use strict';
        },
        coverStatement: function (node, walker) {
            var sName,
                incrStatementCount,
                grandParent;

            if (this.isUseStrictExpression(node)) {
                grandParent = walker.ancestor(2);
                if (grandParent) {
                    if ((grandParent.node.type === SYNTAX.FunctionExpression.name ||
                        grandParent.node.type === SYNTAX.FunctionDeclaration.name)  &&
                        walker.parent().node.body[0] === node) {
                        return;
                    }
                }
            }
            if (node.type === SYNTAX.FunctionDeclaration.name) {
                sName = this.statementName(node.loc, 1);
            } else {
                sName = this.statementName(node.loc);
                incrStatementCount = astgen.statement(
                    astgen.postIncrement(
                        astgen.subscript(
                            astgen.dot(astgen.variable(this.currentState.trackerVar), astgen.variable('s')),
                            astgen.stringLiteral(sName)
                        )
                    )
                );
                this.splice(incrStatementCount, node, walker);
            }
        },

        splice: function (statements, node, walker) {
            var targetNode = walker.isLabeled() ? walker.parent().node : node;
            targetNode.prepend = targetNode.prepend || [];
            pushAll(targetNode.prepend, statements);
        },

        functionName: function (node, line, location) {
            this.currentState.func += 1;
            var id = this.currentState.func,
                name = node.id ? node.id.name : '(anonymous_' + id + ')';
            this.coverState.fnMap[id] = { name: name, line: line, loc: location };
            this.coverState.f[id] = 0;
            return id;
        },

        coverFunction: function (node, walker) {
            var id = this.functionName(node, walker.startLineForNode(node), {
                    start: node.loc.start,
                    end: { line: node.body.loc.start.line, column: node.body.loc.start.column }
                }),
                body = node.body,
                blockBody = body.body,
                popped;

            if (blockBody.length > 0 && this.isUseStrictExpression(blockBody[0])) {
                popped = blockBody.shift();
            }
            blockBody.unshift(
                astgen.statement(
                    astgen.postIncrement(
                        astgen.subscript(
                            astgen.dot(astgen.variable(this.currentState.trackerVar), astgen.variable('f')),
                            astgen.stringLiteral(id)
                        )
                    )
                )
            );
            if (popped) {
                blockBody.unshift(popped);
            }
        },

        branchName: function (type, startLine, pathLocations) {
            var bName,
                paths = [],
                locations = [],
                i;
            this.currentState.branch += 1;
            bName = this.currentState.branch;
            for (i = 0; i < pathLocations.length; i += 1) {
                paths.push(0);
                locations.push(pathLocations[i]);
            }
            this.coverState.b[bName] = paths;
            this.coverState.branchMap[bName] = { line: startLine, type: type, locations: locations };
            return bName;
        },

        branchIncrementExprAst: function (varName, branchIndex, down) {
            var ret = astgen.postIncrement(
                astgen.subscript(
                    astgen.subscript(
                        astgen.dot(astgen.variable(this.currentState.trackerVar), astgen.variable('b')),
                        astgen.stringLiteral(varName)
                    ),
                    astgen.numericLiteral(branchIndex)
                ),
                down
            );
            return ret;
        },

        locationsForNodes: function (nodes) {
            var ret = [],
                i;
            for (i = 0; i < nodes.length; i += 1) {
                ret.push(nodes[i].loc);
            }
            return ret;
        },

        ifBranchInjector: function (node, walker) {
            var line = node.loc.start.line,
                col = node.loc.start.column,
                start = { line: line, column: col },
                end = { line: line, column: col },
                bName = this.branchName('if', walker.startLineForNode(node), [
                    { start: start, end: end },
                    { start: start, end: end }
                ]),
                thenBody = node.consequent.body,
                elseBody = node.alternate.body;
            thenBody.unshift(astgen.statement(this.branchIncrementExprAst(bName, 0)));
            elseBody.unshift(astgen.statement(this.branchIncrementExprAst(bName, 1)));
        },

        switchBranchInjector: function (node, walker) {
            var cases = node.cases,
                bName,
                i;

            if (!cases) {
                return;
            }
            bName = this.branchName('switch', walker.startLineForNode(node), this.locationsForNodes(cases));
            for (i = 0; i < cases.length; i += 1) {
                cases[i].consequent.unshift(astgen.statement(this.branchIncrementExprAst(bName, i)));
            }
        },

        conditionalBranchInjector: function (node, walker) {
            var bName = this.branchName('cond-expr', walker.startLineForNode(node), this.locationsForNodes([ node.consequent, node.alternate ])),
                ast1 = this.branchIncrementExprAst(bName, 0),
                ast2 = this.branchIncrementExprAst(bName, 1);

            node.consequent = astgen.sequence(ast1, node.consequent);
            node.alternate = astgen.sequence(ast2, node.alternate);
        },

        logicalExpressionBranchInjector: function (node, walker) {
            var parent = walker.parent(),
                leaves = [],
                bName,
                tuple,
                i;

            if (parent && parent.node.type === SYNTAX.LogicalExpression.name) {
                //already covered
                return;
            }

            this.findLeaves(node, leaves);
            bName = this.branchName('binary-expr',
                walker.startLineForNode(node),
                this.locationsForNodes(leaves.map(function (item) { return item.node; }))
            );
            for (i = 0; i < leaves.length; i += 1) {
                tuple = leaves[i];
                tuple.parent[tuple.property] = astgen.sequence(this.branchIncrementExprAst(bName, i), tuple.node);
            }
        },

        findLeaves: function (node, accumulator, parent, property) {
            if (node.type === SYNTAX.LogicalExpression.name) {
                this.findLeaves(node.left, accumulator, node, 'left');
                this.findLeaves(node.right, accumulator, node, 'right');
            } else {
                accumulator.push({ node: node, parent: parent, property: property });
            }
        }
    };

    if (isNode) {
        module.exports = Instrumenter;
    } else {
        window.Instrumenter = Instrumenter;
    }

}(typeof module !== 'undefined' && typeof module.exports !== 'undefined' && typeof exports !== 'undefined'));

;
(function(e){if("function"==typeof bootstrap)bootstrap("istanbulcollector",e);else if("object"==typeof exports)module.exports=e();else if("function"==typeof define&&define.amd)define(e);else if("undefined"!=typeof ses){if(!ses.ok())return;ses.makeIstanbulCollector=e}else"undefined"!=typeof window?window.IstanbulCollector=e():global.IstanbulCollector=e()})(function(){var define,ses,bootstrap,module,exports;
return (function(e,t,n){function i(n,s){if(!t[n]){if(!e[n]){var o=typeof require=="function"&&require;if(!s&&o)return o(n,!0);if(r)return r(n,!0);throw new Error("Cannot find module '"+n+"'")}var u=t[n]={exports:{}};e[n][0].call(u.exports,function(t){var r=e[n][1][t];return i(r?r:t)},u,u.exports)}return t[n].exports}var r=typeof require=="function"&&require;for(var s=0;s<n.length;s++)i(n[s]);return i})({1:[function(require,module,exports){
/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

var MemoryStore = require('./store/memory'),
    utils = require('./object-utils');

/**
 * a mechanism to merge multiple coverage objects into one. Handles the use case
 * of overlapping coverage information for the same files in multiple coverage
 * objects and does not double-count in this situation. For example, if
 * you pass the same coverage object multiple times, the final merged object will be
 * no different that any of the objects passed in (except for execution counts).
 *
 * The `Collector` is built for scale to handle thousands of coverage objects.
 * By default, all processing is done in memory since the common use-case is of
 * one or a few coverage objects. You can work around memory
 * issues by passing in a `Store` implementation that stores temporary computations
 * on disk (the `tmp` store, for example).
 *
 * The `getFinalCoverage` method returns an object with merged coverage information
 * and is provided as a convenience for implementors working with coverage information
 * that can fit into memory. Reporters, in the interest of generality, should *not* use this method for
 * creating reports.
 *
 * Usage
 * -----
 *
 *      var collector = new require('istanbul').Collector();
 *
 *      files.forEach(function (f) {
 *          //each coverage object can have overlapping information about multiple files
 *          collector.add(JSON.parse(fs.readFileSync(f, 'utf8')));
 *      });
 *
 *      collector.files().forEach(function(file) {
 *          var fileCoverage = collector.fileCoverageFor(file);
 *          console.log('Coverage for ' + file + ' is:' + JSON.stringify(fileCoverage));
 *      });
 *
 *      // convenience method: do not use this when dealing with a large number of files
 *      var finalCoverage = collector.getFinalCoverage();
 *
 * @class Collector
 * @constructor
 * @param {Object} options Optional. Configuration options.
 * @param {Store} options.store - an implementation of `Store` to use for temporary
 *      calculations.
 */
function Collector(options) {
    options = options || {};
    this.store = options.store || new MemoryStore();
}

Collector.prototype = {
    /**
     * adds a coverage object to the collector.
     *
     * @method add
     * @param {Object} coverage the coverage object.
     * @param {String} testName Optional. The name of the test used to produce the object.
     *      This is currently not used.
     */
    add: function (coverage /*, testName */) {
        var store = this.store;
        Object.keys(coverage).forEach(function (key) {
            var fileCoverage = coverage[key];
            if (store.hasKey(key)) {
                store.setObject(key, utils.mergeFileCoverage(fileCoverage, store.getObject(key)));
            } else {
                store.setObject(key, fileCoverage);
            }
        });
    },
    /**
     * returns a list of unique file paths for which coverage information has been added.
     * @method files
     * @return {Array} an array of file paths for which coverage information is present.
     */
    files: function () {
        return this.store.keys();
    },
    /**
     * return file coverage information for a single file
     * @method fileCoverageFor
     * @param {String} fileName the path for the file for which coverage information is
     *      required. Must be one of the values returned in the `files()` method.
     * @return {Object} the coverage information for the specified file.
     */
    fileCoverageFor: function (fileName) {
        var ret = this.store.getObject(fileName);
        utils.addDerivedInfoForFile(ret);
        return ret;
    },
    /**
     * returns file coverage information for all files. This has the same format as
     * any of the objects passed in to the `add` method. The number of keys in this
     * object will be a superset of all keys found in the objects passed to `add()`
     * @method getFinalCoverage
     * @return {Object} the merged coverage information
     */
    getFinalCoverage: function () {
        var ret = {},
            that = this;
        this.files().forEach(function (file) {
            ret[file] = that.fileCoverageFor(file);
        });
        return ret;
    },
    /**
     * disposes this collector and reclaims temporary resources used in the
     * computation. Calls `dispose()` on the underlying store.
     * @method dispose
     */
    dispose: function () {
        this.store.dispose();
    }
};

module.exports = Collector;
},{"./store/memory":2,"./object-utils":3}],3:[function(require,module,exports){
(function(){/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

/**
 * utility methods to process coverage objects. A coverage object has the following
 * format.
 *
 *      {
 *          "/path/to/file1.js": { file1 coverage },
 *          "/path/to/file2.js": { file2 coverage }
 *      }
 *
 *  The internals of the file coverage object are intentionally not documented since
 *  it is not a public interface.
 *
 *  *Note:* When a method of this module has the word `File` in it, it will accept
 *  one of the sub-objects of the main coverage object as an argument. Other
 *  methods accept the higher level coverage object with multiple keys.
 *
 * Works on `node` as well as the browser.
 *
 * Usage on nodejs
 * ---------------
 *
 *      var objectUtils = require('istanbul').utils;
 *
 * Usage in a browser
 * ------------------
 *
 * Load this file using a `script` tag or other means. This will set `window.coverageUtils`
 * to this module's exports.
 *
 * @class ObjectUtils
 * @static
 */
(function (isNode) {
    /**
     * adds line coverage information to a file coverage object, reverse-engineering
     * it from statement coverage. The object passed in is updated in place.
     *
     * Note that if line coverage information is already present in the object,
     * it is not recomputed.
     *
     * @method addDerivedInfoForFile
     * @static
     * @param {Object} fileCoverage the coverage object for a single file
     */
    function addDerivedInfoForFile(fileCoverage) {
        var statementMap = fileCoverage.statementMap,
            statements = fileCoverage.s,
            lineMap;

        if (!fileCoverage.l) {
            fileCoverage.l = lineMap = {};
            Object.keys(statements).forEach(function (st) {
                var line = statementMap[st].start.line,
                    count = statements[st],
                    prevVal = lineMap[line];
                if (typeof prevVal === 'undefined' || prevVal < count) {
                    lineMap[line] = count;
                }
            });
        }
    }
    /**
     * adds line coverage information to all file coverage objects.
     *
     * @method addDerivedInfo
     * @static
     * @param {Object} coverage the coverage object
     */
    function addDerivedInfo(coverage) {
        Object.keys(coverage).forEach(function (k) {
            addDerivedInfoForFile(coverage[k]);
        });
    }
    /**
     * removes line coverage information from all file coverage objects
     * @method removeDerivedInfo
     * @static
     * @param {Object} coverage the coverage object
     */
    function removeDerivedInfo(coverage) {
        Object.keys(coverage).forEach(function (k) {
            delete coverage[k].l;
        });
    }

    function percent(covered, total) {
        var tmp;
        if (total > 0) {
            tmp = 1000 * 100 * covered / total + 5;
            return Math.floor(tmp / 10) / 100;
        } else {
            return 100.00;
        }
    }

    function computeSimpleTotals(fileCoverage, property) {
        var stats = fileCoverage[property],
            ret = { total: 0, covered: 0 };

        Object.keys(stats).forEach(function (key) {
            ret.total += 1;
            if (stats[key]) {
                ret.covered += 1;
            }
        });
        ret.pct = percent(ret.covered, ret.total);
        return ret;
    }

    function computeBranchTotals(fileCoverage) {
        var stats = fileCoverage.b,
            ret = { total: 0, covered: 0 };

        Object.keys(stats).forEach(function (key) {
            var branches = stats[key],
                covered = branches.filter(function (num) { return num > 0; });
            ret.total += branches.length;
            ret.covered += covered.length;
        });
        ret.pct = percent(ret.covered, ret.total);
        return ret;
    }
    /**
     * returns a blank summary metrics object. A metrics object has the following
     * format.
     *
     *      {
     *          lines: lineMetrics,
     *          statements: statementMetrics,
     *          functions: functionMetrics,
     *          branches: branchMetrics
     *      }
     *
     *  Each individual metric object looks as follows:
     *
     *      {
     *          total: n,
     *          covered: m,
     *          pct: percent
     *      }
     *
     * @method blankSummary
     * @static
     * @return {Object} a blank metrics object
     */
    function blankSummary() {
        return {
            lines: {
                total: 0,
                covered: 0,
                pct: 'Unknown'
            },
            statements: {
                total: 0,
                covered: 0,
                pct: 'Unknown'
            },
            functions: {
                total: 0,
                covered: 0,
                pct: 'Unknown'
            },
            branches: {
                total: 0,
                covered: 0,
                pct: 'Unknown'
            }
        };
    }
    /**
     * returns the summary metrics given the coverage object for a single file. See `blankSummary()`
     * to understand the format of the returned object.
     *
     * @method summarizeFileCoverage
     * @static
     * @param {Object} fileCoverage the coverage object for a single file.
     * @return {Object} the summary metrics for the file
     */
    function summarizeFileCoverage(fileCoverage) {
        var ret = blankSummary();
        addDerivedInfoForFile(fileCoverage);
        ret.lines = computeSimpleTotals(fileCoverage, 'l');
        ret.functions = computeSimpleTotals(fileCoverage, 'f');
        ret.statements = computeSimpleTotals(fileCoverage, 's');
        ret.branches = computeBranchTotals(fileCoverage);
        return ret;
    }
    /**
     * merges two instances of file coverage objects *for the same file*
     * such that the execution counts are correct.
     *
     * @method mergeFileCoverage
     * @static
     * @param {Object} first the first file coverage object for a given file
     * @param {Object} second the second file coverage object for the same file
     * @return {Object} an object that is a result of merging the two. Note that
     *      the input objects are not changed in any way.
     */
    function mergeFileCoverage(first, second) {
        var ret = JSON.parse(JSON.stringify(first)),
            i;

        delete ret.l; //remove derived info

        Object.keys(second.s).forEach(function (k) {
            ret.s[k] += second.s[k];
        });
        Object.keys(second.f).forEach(function (k) {
            ret.f[k] += second.f[k];
        });
        Object.keys(second.b).forEach(function (k) {
            var retArray = ret.b[k],
                secondArray = second.b[k];
            for (i = 0; i < retArray.length; i += 1) {
                retArray[i] += secondArray[i];
            }
        });

        return ret;
    }
    /**
     * merges multiple summary metrics objects by summing up the `totals` and
     * `covered` fields and recomputing the percentages. This function is generic
     * and can accept any number of arguments.
     *
     * @method mergeSummaryObjects
     * @static
     * @param {Object} summary... multiple summary metrics objects
     * @return {Object} the merged summary metrics
     */
    function mergeSummaryObjects() {
        var ret = blankSummary(),
            args = Array.prototype.slice.call(arguments),
            keys = ['lines', 'statements', 'branches', 'functions'],
            increment = function (obj) {
                if (obj) {
                    keys.forEach(function (key) {
                        ret[key].total += obj[key].total;
                        ret[key].covered += obj[key].covered;
                    });
                }
            };
        args.forEach(function (arg) {
            increment(arg);
        });
        keys.forEach(function (key) {
            ret[key].pct = percent(ret[key].covered, ret[key].total);
        });

        return ret;
    }
    /**
     * returns the coverage summary for a single coverage object. This is
     * wrapper over `summarizeFileCoverage` and `mergeSummaryObjects` for
     * the common case of a single coverage object
     * @method summarizeCoverage
     * @static
     * @param {Object} coverage  the coverage object
     * @return {Object} summary coverage metrics across all files in the coverage object
     */
    function summarizeCoverage(coverage) {
        var fileSummary = [];
        Object.keys(coverage).forEach(function (key) {
            fileSummary.push(summarizeFileCoverage(coverage[key]));
        });
        return mergeSummaryObjects.apply(null, fileSummary);
    }

    /**
     * makes the coverage object generated by this library yuitest_coverage compatible.
     * Note that this transformation is lossy since the returned object will not have
     * statement and branch coverage.
     *
     * @method toYUICoverage
     * @static
     * @param {Object} coverage The `istanbul` coverage object
     * @return {Object} a coverage object in `yuitest_coverage` format.
     */
    function toYUICoverage(coverage) {
        var ret = {};

        addDerivedInfo(coverage);

        Object.keys(coverage).forEach(function (k) {
            var fileCoverage = coverage[k],
                lines = fileCoverage.l,
                functions = fileCoverage.f,
                fnMap = fileCoverage.fnMap,
                o;

            o = ret[k] = {
                lines: {},
                calledLines: 0,
                coveredLines: 0,
                functions: {},
                calledFunctions: 0,
                coveredFunctions: 0
            };
            Object.keys(lines).forEach(function (k) {
                o.lines[k] = lines[k];
                o.coveredLines += 1;
                if (lines[k] > 0) {
                    o.calledLines += 1;
                }
            });
            Object.keys(functions).forEach(function (k) {
                var name = fnMap[k].name + ':' + fnMap[k].line;
                o.functions[name] = functions[k];
                o.coveredFunctions += 1;
                if (functions[k] > 0) {
                    o.calledFunctions += 1;
                }
            });
        });
        return ret;
    }

    var exportables = {
        addDerivedInfo: addDerivedInfo,
        addDerivedInfoForFile: addDerivedInfoForFile,
        removeDerivedInfo: removeDerivedInfo,
        blankSummary: blankSummary,
        summarizeFileCoverage: summarizeFileCoverage,
        summarizeCoverage: summarizeCoverage,
        mergeFileCoverage: mergeFileCoverage,
        mergeSummaryObjects: mergeSummaryObjects,
        toYUICoverage: toYUICoverage
    };

    if (isNode) {
        module.exports = exportables;
    } else {
        window.coverageUtils = exportables;
    }
}(typeof module !== 'undefined' && typeof module.exports !== 'undefined' && typeof exports !== 'undefined'));


})()
},{}],2:[function(require,module,exports){
/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

var util = require('util'),
    Store = require('./index');

/**
 * a `Store` implementation using an in-memory object.
 *
 * Usage
 * -----
 *
 *      var store = require('istanbul').Store.create('memory');
 *
 *
 * @class MemoryStore
 * @extends Store
 * @constructor
 */
function MemoryStore() {
    Store.call(this);
    this.map = {};
}

MemoryStore.TYPE = 'memory';
util.inherits(MemoryStore, Store);

Store.mix(MemoryStore, {
    set: function (key, contents) {
        this.map[key] = contents;
    },

    get: function (key) {
        if (!this.hasKey(key)) {
            throw new Error('Unable to find entry for [' + key + ']');
        }
        return this.map[key];
    },

    hasKey: function (key) {
        return this.map.hasOwnProperty(key);
    },

    keys: function () {
        return Object.keys(this.map);
    },

    dispose: function () {
        this.map = {};
    }
});

module.exports = MemoryStore;

},{"util":4,"./index":5}],4:[function(require,module,exports){
var events = require('events');

exports.isArray = isArray;
exports.isDate = function(obj){return Object.prototype.toString.call(obj) === '[object Date]'};
exports.isRegExp = function(obj){return Object.prototype.toString.call(obj) === '[object RegExp]'};


exports.print = function () {};
exports.puts = function () {};
exports.debug = function() {};

exports.inspect = function(obj, showHidden, depth, colors) {
  var seen = [];

  var stylize = function(str, styleType) {
    // http://en.wikipedia.org/wiki/ANSI_escape_code#graphics
    var styles =
        { 'bold' : [1, 22],
          'italic' : [3, 23],
          'underline' : [4, 24],
          'inverse' : [7, 27],
          'white' : [37, 39],
          'grey' : [90, 39],
          'black' : [30, 39],
          'blue' : [34, 39],
          'cyan' : [36, 39],
          'green' : [32, 39],
          'magenta' : [35, 39],
          'red' : [31, 39],
          'yellow' : [33, 39] };

    var style =
        { 'special': 'cyan',
          'number': 'blue',
          'boolean': 'yellow',
          'undefined': 'grey',
          'null': 'bold',
          'string': 'green',
          'date': 'magenta',
          // "name": intentionally not styling
          'regexp': 'red' }[styleType];

    if (style) {
      return '\033[' + styles[style][0] + 'm' + str +
             '\033[' + styles[style][1] + 'm';
    } else {
      return str;
    }
  };
  if (! colors) {
    stylize = function(str, styleType) { return str; };
  }

  function format(value, recurseTimes) {
    // Provide a hook for user-specified inspect functions.
    // Check that value is an object with an inspect function on it
    if (value && typeof value.inspect === 'function' &&
        // Filter out the util module, it's inspect function is special
        value !== exports &&
        // Also filter out any prototype objects using the circular check.
        !(value.constructor && value.constructor.prototype === value)) {
      return value.inspect(recurseTimes);
    }

    // Primitive types cannot have properties
    switch (typeof value) {
      case 'undefined':
        return stylize('undefined', 'undefined');

      case 'string':
        var simple = '\'' + JSON.stringify(value).replace(/^"|"$/g, '')
                                                 .replace(/'/g, "\\'")
                                                 .replace(/\\"/g, '"') + '\'';
        return stylize(simple, 'string');

      case 'number':
        return stylize('' + value, 'number');

      case 'boolean':
        return stylize('' + value, 'boolean');
    }
    // For some reason typeof null is "object", so special case here.
    if (value === null) {
      return stylize('null', 'null');
    }

    // Look up the keys of the object.
    var visible_keys = Object_keys(value);
    var keys = showHidden ? Object_getOwnPropertyNames(value) : visible_keys;

    // Functions without properties can be shortcutted.
    if (typeof value === 'function' && keys.length === 0) {
      if (isRegExp(value)) {
        return stylize('' + value, 'regexp');
      } else {
        var name = value.name ? ': ' + value.name : '';
        return stylize('[Function' + name + ']', 'special');
      }
    }

    // Dates without properties can be shortcutted
    if (isDate(value) && keys.length === 0) {
      return stylize(value.toUTCString(), 'date');
    }

    var base, type, braces;
    // Determine the object type
    if (isArray(value)) {
      type = 'Array';
      braces = ['[', ']'];
    } else {
      type = 'Object';
      braces = ['{', '}'];
    }

    // Make functions say that they are functions
    if (typeof value === 'function') {
      var n = value.name ? ': ' + value.name : '';
      base = (isRegExp(value)) ? ' ' + value : ' [Function' + n + ']';
    } else {
      base = '';
    }

    // Make dates with properties first say the date
    if (isDate(value)) {
      base = ' ' + value.toUTCString();
    }

    if (keys.length === 0) {
      return braces[0] + base + braces[1];
    }

    if (recurseTimes < 0) {
      if (isRegExp(value)) {
        return stylize('' + value, 'regexp');
      } else {
        return stylize('[Object]', 'special');
      }
    }

    seen.push(value);

    var output = keys.map(function(key) {
      var name, str;
      if (value.__lookupGetter__) {
        if (value.__lookupGetter__(key)) {
          if (value.__lookupSetter__(key)) {
            str = stylize('[Getter/Setter]', 'special');
          } else {
            str = stylize('[Getter]', 'special');
          }
        } else {
          if (value.__lookupSetter__(key)) {
            str = stylize('[Setter]', 'special');
          }
        }
      }
      if (visible_keys.indexOf(key) < 0) {
        name = '[' + key + ']';
      }
      if (!str) {
        if (seen.indexOf(value[key]) < 0) {
          if (recurseTimes === null) {
            str = format(value[key]);
          } else {
            str = format(value[key], recurseTimes - 1);
          }
          if (str.indexOf('\n') > -1) {
            if (isArray(value)) {
              str = str.split('\n').map(function(line) {
                return '  ' + line;
              }).join('\n').substr(2);
            } else {
              str = '\n' + str.split('\n').map(function(line) {
                return '   ' + line;
              }).join('\n');
            }
          }
        } else {
          str = stylize('[Circular]', 'special');
        }
      }
      if (typeof name === 'undefined') {
        if (type === 'Array' && key.match(/^\d+$/)) {
          return str;
        }
        name = JSON.stringify('' + key);
        if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
          name = name.substr(1, name.length - 2);
          name = stylize(name, 'name');
        } else {
          name = name.replace(/'/g, "\\'")
                     .replace(/\\"/g, '"')
                     .replace(/(^"|"$)/g, "'");
          name = stylize(name, 'string');
        }
      }

      return name + ': ' + str;
    });

    seen.pop();

    var numLinesEst = 0;
    var length = output.reduce(function(prev, cur) {
      numLinesEst++;
      if (cur.indexOf('\n') >= 0) numLinesEst++;
      return prev + cur.length + 1;
    }, 0);

    if (length > 50) {
      output = braces[0] +
               (base === '' ? '' : base + '\n ') +
               ' ' +
               output.join(',\n  ') +
               ' ' +
               braces[1];

    } else {
      output = braces[0] + base + ' ' + output.join(', ') + ' ' + braces[1];
    }

    return output;
  }
  return format(obj, (typeof depth === 'undefined' ? 2 : depth));
};


function isArray(ar) {
  return ar instanceof Array ||
         Array.isArray(ar) ||
         (ar && ar !== Object.prototype && isArray(ar.__proto__));
}


function isRegExp(re) {
  return re instanceof RegExp ||
    (typeof re === 'object' && Object.prototype.toString.call(re) === '[object RegExp]');
}


function isDate(d) {
  if (d instanceof Date) return true;
  if (typeof d !== 'object') return false;
  var properties = Date.prototype && Object_getOwnPropertyNames(Date.prototype);
  var proto = d.__proto__ && Object_getOwnPropertyNames(d.__proto__);
  return JSON.stringify(proto) === JSON.stringify(properties);
}

function pad(n) {
  return n < 10 ? '0' + n.toString(10) : n.toString(10);
}

var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
              'Oct', 'Nov', 'Dec'];

// 26 Feb 16:19:34
function timestamp() {
  var d = new Date();
  var time = [pad(d.getHours()),
              pad(d.getMinutes()),
              pad(d.getSeconds())].join(':');
  return [d.getDate(), months[d.getMonth()], time].join(' ');
}

exports.log = function (msg) {};

exports.pump = null;

var Object_keys = Object.keys || function (obj) {
    var res = [];
    for (var key in obj) res.push(key);
    return res;
};

var Object_getOwnPropertyNames = Object.getOwnPropertyNames || function (obj) {
    var res = [];
    for (var key in obj) {
        if (Object.hasOwnProperty.call(obj, key)) res.push(key);
    }
    return res;
};

var Object_create = Object.create || function (prototype, properties) {
    // from es5-shim
    var object;
    if (prototype === null) {
        object = { '__proto__' : null };
    }
    else {
        if (typeof prototype !== 'object') {
            throw new TypeError(
                'typeof prototype[' + (typeof prototype) + '] != \'object\''
            );
        }
        var Type = function () {};
        Type.prototype = prototype;
        object = new Type();
        object.__proto__ = prototype;
    }
    if (typeof properties !== 'undefined' && Object.defineProperties) {
        Object.defineProperties(object, properties);
    }
    return object;
};

exports.inherits = function(ctor, superCtor) {
  ctor.super_ = superCtor;
  ctor.prototype = Object_create(superCtor.prototype, {
    constructor: {
      value: ctor,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
};

var formatRegExp = /%[sdj%]/g;
exports.format = function(f) {
  if (typeof f !== 'string') {
    var objects = [];
    for (var i = 0; i < arguments.length; i++) {
      objects.push(exports.inspect(arguments[i]));
    }
    return objects.join(' ');
  }

  var i = 1;
  var args = arguments;
  var len = args.length;
  var str = String(f).replace(formatRegExp, function(x) {
    if (x === '%%') return '%';
    if (i >= len) return x;
    switch (x) {
      case '%s': return String(args[i++]);
      case '%d': return Number(args[i++]);
      case '%j': return JSON.stringify(args[i++]);
      default:
        return x;
    }
  });
  for(var x = args[i]; i < len; x = args[++i]){
    if (x === null || typeof x !== 'object') {
      str += ' ' + x;
    } else {
      str += ' ' + exports.inspect(x);
    }
  }
  return str;
};

},{"events":6}],7:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};

process.nextTick = (function () {
    var canSetImmediate = typeof window !== 'undefined'
    && window.setImmediate;
    var canPost = typeof window !== 'undefined'
    && window.postMessage && window.addEventListener
    ;

    if (canSetImmediate) {
        return function (f) { return window.setImmediate(f) };
    }

    if (canPost) {
        var queue = [];
        window.addEventListener('message', function (ev) {
            if (ev.source === window && ev.data === 'process-tick') {
                ev.stopPropagation();
                if (queue.length > 0) {
                    var fn = queue.shift();
                    fn();
                }
            }
        }, true);

        return function nextTick(fn) {
            queue.push(fn);
            window.postMessage('process-tick', '*');
        };
    }

    return function nextTick(fn) {
        setTimeout(fn, 0);
    };
})();

process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];

process.binding = function (name) {
    throw new Error('process.binding is not supported');
}

// TODO(shtylman)
process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};

},{}],6:[function(require,module,exports){
(function(process){if (!process.EventEmitter) process.EventEmitter = function () {};

var EventEmitter = exports.EventEmitter = process.EventEmitter;
var isArray = typeof Array.isArray === 'function'
    ? Array.isArray
    : function (xs) {
        return Object.prototype.toString.call(xs) === '[object Array]'
    }
;
function indexOf (xs, x) {
    if (xs.indexOf) return xs.indexOf(x);
    for (var i = 0; i < xs.length; i++) {
        if (x === xs[i]) return i;
    }
    return -1;
}

// By default EventEmitters will print a warning if more than
// 10 listeners are added to it. This is a useful default which
// helps finding memory leaks.
//
// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
var defaultMaxListeners = 10;
EventEmitter.prototype.setMaxListeners = function(n) {
  if (!this._events) this._events = {};
  this._events.maxListeners = n;
};


EventEmitter.prototype.emit = function(type) {
  // If there is no 'error' event listener then throw.
  if (type === 'error') {
    if (!this._events || !this._events.error ||
        (isArray(this._events.error) && !this._events.error.length))
    {
      if (arguments[1] instanceof Error) {
        throw arguments[1]; // Unhandled 'error' event
      } else {
        throw new Error("Uncaught, unspecified 'error' event.");
      }
      return false;
    }
  }

  if (!this._events) return false;
  var handler = this._events[type];
  if (!handler) return false;

  if (typeof handler == 'function') {
    switch (arguments.length) {
      // fast cases
      case 1:
        handler.call(this);
        break;
      case 2:
        handler.call(this, arguments[1]);
        break;
      case 3:
        handler.call(this, arguments[1], arguments[2]);
        break;
      // slower
      default:
        var args = Array.prototype.slice.call(arguments, 1);
        handler.apply(this, args);
    }
    return true;

  } else if (isArray(handler)) {
    var args = Array.prototype.slice.call(arguments, 1);

    var listeners = handler.slice();
    for (var i = 0, l = listeners.length; i < l; i++) {
      listeners[i].apply(this, args);
    }
    return true;

  } else {
    return false;
  }
};

// EventEmitter is defined in src/node_events.cc
// EventEmitter.prototype.emit() is also defined there.
EventEmitter.prototype.addListener = function(type, listener) {
  if ('function' !== typeof listener) {
    throw new Error('addListener only takes instances of Function');
  }

  if (!this._events) this._events = {};

  // To avoid recursion in the case that type == "newListeners"! Before
  // adding it to the listeners, first emit "newListeners".
  this.emit('newListener', type, listener);

  if (!this._events[type]) {
    // Optimize the case of one listener. Don't need the extra array object.
    this._events[type] = listener;
  } else if (isArray(this._events[type])) {

    // Check for listener leak
    if (!this._events[type].warned) {
      var m;
      if (this._events.maxListeners !== undefined) {
        m = this._events.maxListeners;
      } else {
        m = defaultMaxListeners;
      }

      if (m && m > 0 && this._events[type].length > m) {
        this._events[type].warned = true;
        console.error('(node) warning: possible EventEmitter memory ' +
                      'leak detected. %d listeners added. ' +
                      'Use emitter.setMaxListeners() to increase limit.',
                      this._events[type].length);
        console.trace();
      }
    }

    // If we've already got an array, just append.
    this._events[type].push(listener);
  } else {
    // Adding the second element, need to change to array.
    this._events[type] = [this._events[type], listener];
  }

  return this;
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.once = function(type, listener) {
  var self = this;
  self.on(type, function g() {
    self.removeListener(type, g);
    listener.apply(this, arguments);
  });

  return this;
};

EventEmitter.prototype.removeListener = function(type, listener) {
  if ('function' !== typeof listener) {
    throw new Error('removeListener only takes instances of Function');
  }

  // does not use listeners(), so no side effect of creating _events[type]
  if (!this._events || !this._events[type]) return this;

  var list = this._events[type];

  if (isArray(list)) {
    var i = indexOf(list, listener);
    if (i < 0) return this;
    list.splice(i, 1);
    if (list.length == 0)
      delete this._events[type];
  } else if (this._events[type] === listener) {
    delete this._events[type];
  }

  return this;
};

EventEmitter.prototype.removeAllListeners = function(type) {
  if (arguments.length === 0) {
    this._events = {};
    return this;
  }

  // does not use listeners(), so no side effect of creating _events[type]
  if (type && this._events && this._events[type]) this._events[type] = null;
  return this;
};

EventEmitter.prototype.listeners = function(type) {
  if (!this._events) this._events = {};
  if (!this._events[type]) this._events[type] = [];
  if (!isArray(this._events[type])) {
    this._events[type] = [this._events[type]];
  }
  return this._events[type];
};

})(require("__browserify_process"))
},{"__browserify_process":7}],5:[function(require,module,exports){
(function(__dirname){/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

var Factory = require('../util/factory'),
    factory = new Factory('store', __dirname, false);
/**
 * An abstraction for keeping track of content against some keys (e.g.
 * original source, instrumented source, coverage objects against file names).
 * This class is both the base class as well as a factory for `Store` implementations.
 *
 * Usage
 * -----
 *
 *      var Store = require('istanbul').Store,
 *          store = Store.create('memory');
 *
 *      //basic use
 *      store.set('foo', 'foo-content');
 *      var content = store.get('foo');
 *
 *      //keys and values
 *      store.keys().forEach(function (key) {
 *          console.log(key + ':\n' + store.get(key);
 *      });
 *      if (store.hasKey('bar') { console.log(store.get('bar'); }
 *
 *
 *      //syntactic sugar
 *      store.setObject('foo', { foo: true });
 *      console.log(store.getObject('foo').foo);
 *
 *      store.dispose();
 *
 * @class Store
 * @constructor
 * @protected
 * @param {Object} options Optional. The options supported by a specific store implementation.
 */
function Store(/* options */) {}

//add register, create, mix, loadAll, getStoreList as class methods
factory.bindClassMethods(Store);

/**
 * registers a new store implementation.
 * @method register
 * @static
 * @param {Function} constructor the constructor function for the store. This function must have a
 *  `TYPE` property of type String, that will be used in `Store.create()`
 */
/**
 * returns a store implementation of the specified type.
 * @method create
 * @static
 * @param {String} type the type of store to create
 * @param {Object} opts Optional. Options specific to the store implementation
 * @return {Store} a new store of the specified type
 */

Store.prototype = {
    /**
     * sets some content associated with a specific key. The manner in which
     * duplicate keys are handled for multiple `set()` calls with the same
     * key is implementation-specific.
     *
     * @method set
     * @param {String} key the key for the content
     * @param {String} contents the contents for the key
     */
    set: function (/* key, contents */) { throw new Error("set: must be overridden"); },
    /**
     * returns the content associated to a specific key or throws if the key
     * was not `set`
     * @method get
     * @param {String} key the key for which to get the content
     * @return {String} the content for the specified key
     */
    get: function (/* key */) { throw new Error("get: must be overridden"); },
    /**
     * returns a list of all known keys
     * @method keys
     * @return {Array} an array of seen keys
     */
    keys: function () { throw new Error("keys: must be overridden"); },
    /**
     * returns true if the key is one for which a `get()` call would work.
     * @method hasKey
     * @param {String} key
     * @return true if the key is valid for this store, false otherwise
     */
    hasKey: function (/* key */) { throw new Error("hasKey: must be overridden"); },
    /**
     * lifecycle method to dispose temporary resources associated with the store
     * @method dispose
     */
    dispose: function () {},
    /**
     * sugar method to return an object associated with a specific key. Throws
     * if the content set against the key was not a valid JSON string.
     * @method getObject
     * @param {String} key the key for which to return the associated object
     * @return {Object} the object corresponding to the key
     */
    getObject: function (key) {
        return JSON.parse(this.get(key));
    },
    /**
     * sugar method to set an object against a specific key.
     * @method setObject
     * @param {String} key the key for the object
     * @param {Object} object the object to be stored
     */
    setObject: function (key, object) {
        return this.set(key, JSON.stringify(object));
    }
};

module.exports = Store;



})("/store")
},{"../util/factory":8}],9:[function(require,module,exports){
(function(process){function filter (xs, fn) {
    var res = [];
    for (var i = 0; i < xs.length; i++) {
        if (fn(xs[i], i, xs)) res.push(xs[i]);
    }
    return res;
}

// resolves . and .. elements in a path array with directory names there
// must be no slashes, empty elements, or device names (c:\) in the array
// (so also no leading and trailing slashes - it does not distinguish
// relative and absolute paths)
function normalizeArray(parts, allowAboveRoot) {
  // if the path tries to go above the root, `up` ends up > 0
  var up = 0;
  for (var i = parts.length; i >= 0; i--) {
    var last = parts[i];
    if (last == '.') {
      parts.splice(i, 1);
    } else if (last === '..') {
      parts.splice(i, 1);
      up++;
    } else if (up) {
      parts.splice(i, 1);
      up--;
    }
  }

  // if the path is allowed to go above the root, restore leading ..s
  if (allowAboveRoot) {
    for (; up--; up) {
      parts.unshift('..');
    }
  }

  return parts;
}

// Regex to split a filename into [*, dir, basename, ext]
// posix version
var splitPathRe = /^(.+\/(?!$)|\/)?((?:.+?)?(\.[^.]*)?)$/;

// path.resolve([from ...], to)
// posix version
exports.resolve = function() {
var resolvedPath = '',
    resolvedAbsolute = false;

for (var i = arguments.length; i >= -1 && !resolvedAbsolute; i--) {
  var path = (i >= 0)
      ? arguments[i]
      : process.cwd();

  // Skip empty and invalid entries
  if (typeof path !== 'string' || !path) {
    continue;
  }

  resolvedPath = path + '/' + resolvedPath;
  resolvedAbsolute = path.charAt(0) === '/';
}

// At this point the path should be resolved to a full absolute path, but
// handle relative paths to be safe (might happen when process.cwd() fails)

// Normalize the path
resolvedPath = normalizeArray(filter(resolvedPath.split('/'), function(p) {
    return !!p;
  }), !resolvedAbsolute).join('/');

  return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
};

// path.normalize(path)
// posix version
exports.normalize = function(path) {
var isAbsolute = path.charAt(0) === '/',
    trailingSlash = path.slice(-1) === '/';

// Normalize the path
path = normalizeArray(filter(path.split('/'), function(p) {
    return !!p;
  }), !isAbsolute).join('/');

  if (!path && !isAbsolute) {
    path = '.';
  }
  if (path && trailingSlash) {
    path += '/';
  }
  
  return (isAbsolute ? '/' : '') + path;
};


// posix version
exports.join = function() {
  var paths = Array.prototype.slice.call(arguments, 0);
  return exports.normalize(filter(paths, function(p, index) {
    return p && typeof p === 'string';
  }).join('/'));
};


exports.dirname = function(path) {
  var dir = splitPathRe.exec(path)[1] || '';
  var isWindows = false;
  if (!dir) {
    // No dirname
    return '.';
  } else if (dir.length === 1 ||
      (isWindows && dir.length <= 3 && dir.charAt(1) === ':')) {
    // It is just a slash or a drive letter with a slash
    return dir;
  } else {
    // It is a full dirname, strip trailing slash
    return dir.substring(0, dir.length - 1);
  }
};


exports.basename = function(path, ext) {
  var f = splitPathRe.exec(path)[2] || '';
  // TODO: make this comparison case-insensitive on windows?
  if (ext && f.substr(-1 * ext.length) === ext) {
    f = f.substr(0, f.length - ext.length);
  }
  return f;
};


exports.extname = function(path) {
  return splitPathRe.exec(path)[3] || '';
};

exports.relative = function(from, to) {
  from = exports.resolve(from).substr(1);
  to = exports.resolve(to).substr(1);

  function trim(arr) {
    var start = 0;
    for (; start < arr.length; start++) {
      if (arr[start] !== '') break;
    }

    var end = arr.length - 1;
    for (; end >= 0; end--) {
      if (arr[end] !== '') break;
    }

    if (start > end) return [];
    return arr.slice(start, end - start + 1);
  }

  var fromParts = trim(from.split('/'));
  var toParts = trim(to.split('/'));

  var length = Math.min(fromParts.length, toParts.length);
  var samePartsLength = length;
  for (var i = 0; i < length; i++) {
    if (fromParts[i] !== toParts[i]) {
      samePartsLength = i;
      break;
    }
  }

  var outputParts = [];
  for (var i = samePartsLength; i < fromParts.length; i++) {
    outputParts.push('..');
  }

  outputParts = outputParts.concat(toParts.slice(samePartsLength));

  return outputParts.join('/');
};

})(require("__browserify_process"))
},{"__browserify_process":7}],10:[function(require,module,exports){
// nothing to see here... no file methods for the browser

},{}],8:[function(require,module,exports){
/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

var util = require('util'),
    path = require('path'),
    fs = require('fs'),
    abbrev = require('abbrev');

function Factory(kind, dir, allowAbbreviations) {
    this.kind = kind;
    this.dir = dir;
    this.allowAbbreviations = allowAbbreviations;
    this.classMap = {};
    this.abbreviations = null;
}

Factory.prototype = {

    knownTypes: function () {
        var keys = Object.keys(this.classMap);
        keys.sort();
        return keys;
    },

    resolve: function (abbreviatedType) {
        if (!this.abbreviations) {
            this.abbreviations = abbrev(this.knownTypes());
        }
        return this.abbreviations[abbreviatedType];
    },

    register: function (constructor) {
        var type = constructor.TYPE;
        if (!type) { throw new Error('Could not register ' + this.kind + ' constructor [no TYPE property]: ' + util.inspect(constructor)); }
        this.classMap[type] = constructor;
        this.abbreviations = null;
    },

    create: function (type, opts) {
        var allowAbbrev = this.allowAbbreviations,
            realType = allowAbbrev ? this.resolve(type) : type,
            Cons;

        Cons = realType ? this.classMap[realType] : null;
        if (!Cons) { throw new Error('Invalid ' + this.kind + ' [' + type + '], allowed values are ' + this.knownTypes().join(', ')); }
        return new Cons(opts);
    },

    loadStandard: function (dir) {
        var that = this;
        fs.readdirSync(dir).forEach(function (file) {
            if (file !== 'index.js' && file.indexOf('.js') === file.length - 3) {
                try {
                    that.register(require(path.resolve(dir, file)));
                } catch (ex) {
                    console.error(ex.message);
                    console.error(ex.stack);
                    throw new Error('Could not register ' + that.kind + ' from file ' + file);
                }
            }
        });
    },

    bindClassMethods: function (Cons) {
        var tmpKind = this.kind.charAt(0).toUpperCase() + this.kind.substring(1), //ucfirst
            allowAbbrev = this.allowAbbreviations;

        Cons.mix = Factory.mix;
        Cons.register = this.register.bind(this);
        Cons.create = this.create.bind(this);
        Cons.loadAll = this.loadStandard.bind(this, this.dir);
        Cons['get' + tmpKind + 'List'] = this.knownTypes.bind(this);
        if (allowAbbrev) {
            Cons['resolve' + tmpKind + 'Name'] = this.resolve.bind(this);
        }
    }
};

Factory.mix = function (cons, proto) {
    Object.keys(proto).forEach(function (key) {
        cons.prototype[key] = proto[key];
    });
};

module.exports = Factory;


},{"util":4,"path":9,"fs":10,"abbrev":11}],11:[function(require,module,exports){

module.exports = exports = abbrev.abbrev = abbrev

abbrev.monkeyPatch = monkeyPatch

function monkeyPatch () {
  Object.defineProperty(Array.prototype, 'abbrev', {
    value: function () { return abbrev(this) },
    enumerable: false, configurable: true, writable: true
  })

  Object.defineProperty(Object.prototype, 'abbrev', {
    value: function () { return abbrev(Object.keys(this)) },
    enumerable: false, configurable: true, writable: true
  })
}

function abbrev (list) {
  if (arguments.length !== 1 || !Array.isArray(list)) {
    list = Array.prototype.slice.call(arguments, 0)
  }
  for (var i = 0, l = list.length, args = [] ; i < l ; i ++) {
    args[i] = typeof list[i] === "string" ? list[i] : String(list[i])
  }

  // sort them lexicographically, so that they're next to their nearest kin
  args = args.sort(lexSort)

  // walk through each, seeing how much it has in common with the next and previous
  var abbrevs = {}
    , prev = ""
  for (var i = 0, l = args.length ; i < l ; i ++) {
    var current = args[i]
      , next = args[i + 1] || ""
      , nextMatches = true
      , prevMatches = true
    if (current === next) continue
    for (var j = 0, cl = current.length ; j < cl ; j ++) {
      var curChar = current.charAt(j)
      nextMatches = nextMatches && curChar === next.charAt(j)
      prevMatches = prevMatches && curChar === prev.charAt(j)
      if (!nextMatches && !prevMatches) {
        j ++
        break
      }
    }
    prev = current
    if (j === cl) {
      abbrevs[current] = current
      continue
    }
    for (var a = current.substr(0, j) ; j <= cl ; j ++) {
      abbrevs[a] = current
      a += current.charAt(j)
    }
  }
  return abbrevs
}

function lexSort (a, b) {
  return a === b ? 0 : a > b ? 1 : -1
}


// tests
if (module === require.main) {

var assert = require("assert")
var util = require("util")

console.log("running tests")
function test (list, expect) {
  var actual = abbrev(list)
  assert.deepEqual(actual, expect,
    "abbrev("+util.inspect(list)+") === " + util.inspect(expect) + "\n"+
    "actual: "+util.inspect(actual))
  actual = abbrev.apply(exports, list)
  assert.deepEqual(abbrev.apply(exports, list), expect,
    "abbrev("+list.map(JSON.stringify).join(",")+") === " + util.inspect(expect) + "\n"+
    "actual: "+util.inspect(actual))
}

test([ "ruby", "ruby", "rules", "rules", "rules" ],
{ rub: 'ruby'
, ruby: 'ruby'
, rul: 'rules'
, rule: 'rules'
, rules: 'rules'
})
test(["fool", "foom", "pool", "pope"],
{ fool: 'fool'
, foom: 'foom'
, poo: 'pool'
, pool: 'pool'
, pop: 'pope'
, pope: 'pope'
})
test(["a", "ab", "abc", "abcd", "abcde", "acde"],
{ a: 'a'
, ab: 'ab'
, abc: 'abc'
, abcd: 'abcd'
, abcde: 'abcde'
, ac: 'acde'
, acd: 'acde'
, acde: 'acde'
})

console.log("pass")

}

},{"assert":12,"util":4}],12:[function(require,module,exports){
(function(){// UTILITY
var util = require('util');
var Buffer = require("buffer").Buffer;
var pSlice = Array.prototype.slice;

function objectKeys(object) {
  if (Object.keys) return Object.keys(object);
  var result = [];
  for (var name in object) {
    if (Object.prototype.hasOwnProperty.call(object, name)) {
      result.push(name);
    }
  }
  return result;
}

// 1. The assert module provides functions that throw
// AssertionError's when particular conditions are not met. The
// assert module must conform to the following interface.

var assert = module.exports = ok;

// 2. The AssertionError is defined in assert.
// new assert.AssertionError({ message: message,
//                             actual: actual,
//                             expected: expected })

assert.AssertionError = function AssertionError(options) {
  this.name = 'AssertionError';
  this.message = options.message;
  this.actual = options.actual;
  this.expected = options.expected;
  this.operator = options.operator;
  var stackStartFunction = options.stackStartFunction || fail;

  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, stackStartFunction);
  }
};
util.inherits(assert.AssertionError, Error);

function replacer(key, value) {
  if (value === undefined) {
    return '' + value;
  }
  if (typeof value === 'number' && (isNaN(value) || !isFinite(value))) {
    return value.toString();
  }
  if (typeof value === 'function' || value instanceof RegExp) {
    return value.toString();
  }
  return value;
}

function truncate(s, n) {
  if (typeof s == 'string') {
    return s.length < n ? s : s.slice(0, n);
  } else {
    return s;
  }
}

assert.AssertionError.prototype.toString = function() {
  if (this.message) {
    return [this.name + ':', this.message].join(' ');
  } else {
    return [
      this.name + ':',
      truncate(JSON.stringify(this.actual, replacer), 128),
      this.operator,
      truncate(JSON.stringify(this.expected, replacer), 128)
    ].join(' ');
  }
};

// assert.AssertionError instanceof Error

assert.AssertionError.__proto__ = Error.prototype;

// At present only the three keys mentioned above are used and
// understood by the spec. Implementations or sub modules can pass
// other keys to the AssertionError's constructor - they will be
// ignored.

// 3. All of the following functions must throw an AssertionError
// when a corresponding condition is not met, with a message that
// may be undefined if not provided.  All assertion methods provide
// both the actual and expected values to the assertion error for
// display purposes.

function fail(actual, expected, message, operator, stackStartFunction) {
  throw new assert.AssertionError({
    message: message,
    actual: actual,
    expected: expected,
    operator: operator,
    stackStartFunction: stackStartFunction
  });
}

// EXTENSION! allows for well behaved errors defined elsewhere.
assert.fail = fail;

// 4. Pure assertion tests whether a value is truthy, as determined
// by !!guard.
// assert.ok(guard, message_opt);
// This statement is equivalent to assert.equal(true, guard,
// message_opt);. To test strictly for the value true, use
// assert.strictEqual(true, guard, message_opt);.

function ok(value, message) {
  if (!!!value) fail(value, true, message, '==', assert.ok);
}
assert.ok = ok;

// 5. The equality assertion tests shallow, coercive equality with
// ==.
// assert.equal(actual, expected, message_opt);

assert.equal = function equal(actual, expected, message) {
  if (actual != expected) fail(actual, expected, message, '==', assert.equal);
};

// 6. The non-equality assertion tests for whether two objects are not equal
// with != assert.notEqual(actual, expected, message_opt);

assert.notEqual = function notEqual(actual, expected, message) {
  if (actual == expected) {
    fail(actual, expected, message, '!=', assert.notEqual);
  }
};

// 7. The equivalence assertion tests a deep equality relation.
// assert.deepEqual(actual, expected, message_opt);

assert.deepEqual = function deepEqual(actual, expected, message) {
  if (!_deepEqual(actual, expected)) {
    fail(actual, expected, message, 'deepEqual', assert.deepEqual);
  }
};

function _deepEqual(actual, expected) {
  // 7.1. All identical values are equivalent, as determined by ===.
  if (actual === expected) {
    return true;

  } else if (Buffer.isBuffer(actual) && Buffer.isBuffer(expected)) {
    if (actual.length != expected.length) return false;

    for (var i = 0; i < actual.length; i++) {
      if (actual[i] !== expected[i]) return false;
    }

    return true;

  // 7.2. If the expected value is a Date object, the actual value is
  // equivalent if it is also a Date object that refers to the same time.
  } else if (actual instanceof Date && expected instanceof Date) {
    return actual.getTime() === expected.getTime();

  // 7.3. Other pairs that do not both pass typeof value == 'object',
  // equivalence is determined by ==.
  } else if (typeof actual != 'object' && typeof expected != 'object') {
    return actual == expected;

  // 7.4. For all other Object pairs, including Array objects, equivalence is
  // determined by having the same number of owned properties (as verified
  // with Object.prototype.hasOwnProperty.call), the same set of keys
  // (although not necessarily the same order), equivalent values for every
  // corresponding key, and an identical 'prototype' property. Note: this
  // accounts for both named and indexed properties on Arrays.
  } else {
    return objEquiv(actual, expected);
  }
}

function isUndefinedOrNull(value) {
  return value === null || value === undefined;
}

function isArguments(object) {
  return Object.prototype.toString.call(object) == '[object Arguments]';
}

function objEquiv(a, b) {
  if (isUndefinedOrNull(a) || isUndefinedOrNull(b))
    return false;
  // an identical 'prototype' property.
  if (a.prototype !== b.prototype) return false;
  //~~~I've managed to break Object.keys through screwy arguments passing.
  //   Converting to array solves the problem.
  if (isArguments(a)) {
    if (!isArguments(b)) {
      return false;
    }
    a = pSlice.call(a);
    b = pSlice.call(b);
    return _deepEqual(a, b);
  }
  try {
    var ka = objectKeys(a),
        kb = objectKeys(b),
        key, i;
  } catch (e) {//happens when one is a string literal and the other isn't
    return false;
  }
  // having the same number of owned properties (keys incorporates
  // hasOwnProperty)
  if (ka.length != kb.length)
    return false;
  //the same set of keys (although not necessarily the same order),
  ka.sort();
  kb.sort();
  //~~~cheap key test
  for (i = ka.length - 1; i >= 0; i--) {
    if (ka[i] != kb[i])
      return false;
  }
  //equivalent values for every corresponding key, and
  //~~~possibly expensive deep test
  for (i = ka.length - 1; i >= 0; i--) {
    key = ka[i];
    if (!_deepEqual(a[key], b[key])) return false;
  }
  return true;
}

// 8. The non-equivalence assertion tests for any deep inequality.
// assert.notDeepEqual(actual, expected, message_opt);

assert.notDeepEqual = function notDeepEqual(actual, expected, message) {
  if (_deepEqual(actual, expected)) {
    fail(actual, expected, message, 'notDeepEqual', assert.notDeepEqual);
  }
};

// 9. The strict equality assertion tests strict equality, as determined by ===.
// assert.strictEqual(actual, expected, message_opt);

assert.strictEqual = function strictEqual(actual, expected, message) {
  if (actual !== expected) {
    fail(actual, expected, message, '===', assert.strictEqual);
  }
};

// 10. The strict non-equality assertion tests for strict inequality, as
// determined by !==.  assert.notStrictEqual(actual, expected, message_opt);

assert.notStrictEqual = function notStrictEqual(actual, expected, message) {
  if (actual === expected) {
    fail(actual, expected, message, '!==', assert.notStrictEqual);
  }
};

function expectedException(actual, expected) {
  if (!actual || !expected) {
    return false;
  }

  if (expected instanceof RegExp) {
    return expected.test(actual);
  } else if (actual instanceof expected) {
    return true;
  } else if (expected.call({}, actual) === true) {
    return true;
  }

  return false;
}

function _throws(shouldThrow, block, expected, message) {
  var actual;

  if (typeof expected === 'string') {
    message = expected;
    expected = null;
  }

  try {
    block();
  } catch (e) {
    actual = e;
  }

  message = (expected && expected.name ? ' (' + expected.name + ').' : '.') +
            (message ? ' ' + message : '.');

  if (shouldThrow && !actual) {
    fail('Missing expected exception' + message);
  }

  if (!shouldThrow && expectedException(actual, expected)) {
    fail('Got unwanted exception' + message);
  }

  if ((shouldThrow && actual && expected &&
      !expectedException(actual, expected)) || (!shouldThrow && actual)) {
    throw actual;
  }
}

// 11. Expected to throw an error:
// assert.throws(block, Error_opt, message_opt);

assert.throws = function(block, /*optional*/error, /*optional*/message) {
  _throws.apply(this, [true].concat(pSlice.call(arguments)));
};

// EXTENSION! This is annoying to write outside this module.
assert.doesNotThrow = function(block, /*optional*/error, /*optional*/message) {
  _throws.apply(this, [false].concat(pSlice.call(arguments)));
};

assert.ifError = function(err) { if (err) {throw err;}};

})()
},{"util":4,"buffer":13}],14:[function(require,module,exports){
exports.readIEEE754 = function(buffer, offset, isBE, mLen, nBytes) {
  var e, m,
      eLen = nBytes * 8 - mLen - 1,
      eMax = (1 << eLen) - 1,
      eBias = eMax >> 1,
      nBits = -7,
      i = isBE ? 0 : (nBytes - 1),
      d = isBE ? 1 : -1,
      s = buffer[offset + i];

  i += d;

  e = s & ((1 << (-nBits)) - 1);
  s >>= (-nBits);
  nBits += eLen;
  for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8);

  m = e & ((1 << (-nBits)) - 1);
  e >>= (-nBits);
  nBits += mLen;
  for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8);

  if (e === 0) {
    e = 1 - eBias;
  } else if (e === eMax) {
    return m ? NaN : ((s ? -1 : 1) * Infinity);
  } else {
    m = m + Math.pow(2, mLen);
    e = e - eBias;
  }
  return (s ? -1 : 1) * m * Math.pow(2, e - mLen);
};

exports.writeIEEE754 = function(buffer, value, offset, isBE, mLen, nBytes) {
  var e, m, c,
      eLen = nBytes * 8 - mLen - 1,
      eMax = (1 << eLen) - 1,
      eBias = eMax >> 1,
      rt = (mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0),
      i = isBE ? (nBytes - 1) : 0,
      d = isBE ? -1 : 1,
      s = value < 0 || (value === 0 && 1 / value < 0) ? 1 : 0;

  value = Math.abs(value);

  if (isNaN(value) || value === Infinity) {
    m = isNaN(value) ? 1 : 0;
    e = eMax;
  } else {
    e = Math.floor(Math.log(value) / Math.LN2);
    if (value * (c = Math.pow(2, -e)) < 1) {
      e--;
      c *= 2;
    }
    if (e + eBias >= 1) {
      value += rt / c;
    } else {
      value += rt * Math.pow(2, 1 - eBias);
    }
    if (value * c >= 2) {
      e++;
      c /= 2;
    }

    if (e + eBias >= eMax) {
      m = 0;
      e = eMax;
    } else if (e + eBias >= 1) {
      m = (value * c - 1) * Math.pow(2, mLen);
      e = e + eBias;
    } else {
      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen);
      e = 0;
    }
  }

  for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8);

  e = (e << mLen) | m;
  eLen += mLen;
  for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8);

  buffer[offset + i - d] |= s * 128;
};

},{}],13:[function(require,module,exports){
(function(){var assert = require('assert');
exports.Buffer = Buffer;
exports.SlowBuffer = Buffer;
Buffer.poolSize = 8192;
exports.INSPECT_MAX_BYTES = 50;

function Buffer(subject, encoding, offset) {
  if (!(this instanceof Buffer)) {
    return new Buffer(subject, encoding, offset);
  }
  this.parent = this;
  this.offset = 0;

  var type;

  // Are we slicing?
  if (typeof offset === 'number') {
    this.length = coerce(encoding);
    this.offset = offset;
  } else {
    // Find the length
    switch (type = typeof subject) {
      case 'number':
        this.length = coerce(subject);
        break;

      case 'string':
        this.length = Buffer.byteLength(subject, encoding);
        break;

      case 'object': // Assume object is an array
        this.length = coerce(subject.length);
        break;

      default:
        throw new Error('First argument needs to be a number, ' +
                        'array or string.');
    }

    // Treat array-ish objects as a byte array.
    if (isArrayIsh(subject)) {
      for (var i = 0; i < this.length; i++) {
        if (subject instanceof Buffer) {
          this[i] = subject.readUInt8(i);
        }
        else {
          this[i] = subject[i];
        }
      }
    } else if (type == 'string') {
      // We are a string
      this.length = this.write(subject, 0, encoding);
    } else if (type === 'number') {
      for (var i = 0; i < this.length; i++) {
        this[i] = 0;
      }
    }
  }
}

Buffer.prototype.get = function get(i) {
  if (i < 0 || i >= this.length) throw new Error('oob');
  return this[i];
};

Buffer.prototype.set = function set(i, v) {
  if (i < 0 || i >= this.length) throw new Error('oob');
  return this[i] = v;
};

Buffer.byteLength = function (str, encoding) {
  switch (encoding || "utf8") {
    case 'hex':
      return str.length / 2;

    case 'utf8':
    case 'utf-8':
      return utf8ToBytes(str).length;

    case 'ascii':
    case 'binary':
      return str.length;

    case 'base64':
      return base64ToBytes(str).length;

    default:
      throw new Error('Unknown encoding');
  }
};

Buffer.prototype.utf8Write = function (string, offset, length) {
  var bytes, pos;
  return Buffer._charsWritten =  blitBuffer(utf8ToBytes(string), this, offset, length);
};

Buffer.prototype.asciiWrite = function (string, offset, length) {
  var bytes, pos;
  return Buffer._charsWritten =  blitBuffer(asciiToBytes(string), this, offset, length);
};

Buffer.prototype.binaryWrite = Buffer.prototype.asciiWrite;

Buffer.prototype.base64Write = function (string, offset, length) {
  var bytes, pos;
  return Buffer._charsWritten = blitBuffer(base64ToBytes(string), this, offset, length);
};

Buffer.prototype.base64Slice = function (start, end) {
  var bytes = Array.prototype.slice.apply(this, arguments)
  return require("base64-js").fromByteArray(bytes);
};

Buffer.prototype.utf8Slice = function () {
  var bytes = Array.prototype.slice.apply(this, arguments);
  var res = "";
  var tmp = "";
  var i = 0;
  while (i < bytes.length) {
    if (bytes[i] <= 0x7F) {
      res += decodeUtf8Char(tmp) + String.fromCharCode(bytes[i]);
      tmp = "";
    } else
      tmp += "%" + bytes[i].toString(16);

    i++;
  }

  return res + decodeUtf8Char(tmp);
}

Buffer.prototype.asciiSlice = function () {
  var bytes = Array.prototype.slice.apply(this, arguments);
  var ret = "";
  for (var i = 0; i < bytes.length; i++)
    ret += String.fromCharCode(bytes[i]);
  return ret;
}

Buffer.prototype.binarySlice = Buffer.prototype.asciiSlice;

Buffer.prototype.inspect = function() {
  var out = [],
      len = this.length;
  for (var i = 0; i < len; i++) {
    out[i] = toHex(this[i]);
    if (i == exports.INSPECT_MAX_BYTES) {
      out[i + 1] = '...';
      break;
    }
  }
  return '<Buffer ' + out.join(' ') + '>';
};


Buffer.prototype.hexSlice = function(start, end) {
  var len = this.length;

  if (!start || start < 0) start = 0;
  if (!end || end < 0 || end > len) end = len;

  var out = '';
  for (var i = start; i < end; i++) {
    out += toHex(this[i]);
  }
  return out;
};


Buffer.prototype.toString = function(encoding, start, end) {
  encoding = String(encoding || 'utf8').toLowerCase();
  start = +start || 0;
  if (typeof end == 'undefined') end = this.length;

  // Fastpath empty strings
  if (+end == start) {
    return '';
  }

  switch (encoding) {
    case 'hex':
      return this.hexSlice(start, end);

    case 'utf8':
    case 'utf-8':
      return this.utf8Slice(start, end);

    case 'ascii':
      return this.asciiSlice(start, end);

    case 'binary':
      return this.binarySlice(start, end);

    case 'base64':
      return this.base64Slice(start, end);

    case 'ucs2':
    case 'ucs-2':
      return this.ucs2Slice(start, end);

    default:
      throw new Error('Unknown encoding');
  }
};


Buffer.prototype.hexWrite = function(string, offset, length) {
  offset = +offset || 0;
  var remaining = this.length - offset;
  if (!length) {
    length = remaining;
  } else {
    length = +length;
    if (length > remaining) {
      length = remaining;
    }
  }

  // must be an even number of digits
  var strLen = string.length;
  if (strLen % 2) {
    throw new Error('Invalid hex string');
  }
  if (length > strLen / 2) {
    length = strLen / 2;
  }
  for (var i = 0; i < length; i++) {
    var byte = parseInt(string.substr(i * 2, 2), 16);
    if (isNaN(byte)) throw new Error('Invalid hex string');
    this[offset + i] = byte;
  }
  Buffer._charsWritten = i * 2;
  return i;
};


Buffer.prototype.write = function(string, offset, length, encoding) {
  // Support both (string, offset, length, encoding)
  // and the legacy (string, encoding, offset, length)
  if (isFinite(offset)) {
    if (!isFinite(length)) {
      encoding = length;
      length = undefined;
    }
  } else {  // legacy
    var swap = encoding;
    encoding = offset;
    offset = length;
    length = swap;
  }

  offset = +offset || 0;
  var remaining = this.length - offset;
  if (!length) {
    length = remaining;
  } else {
    length = +length;
    if (length > remaining) {
      length = remaining;
    }
  }
  encoding = String(encoding || 'utf8').toLowerCase();

  switch (encoding) {
    case 'hex':
      return this.hexWrite(string, offset, length);

    case 'utf8':
    case 'utf-8':
      return this.utf8Write(string, offset, length);

    case 'ascii':
      return this.asciiWrite(string, offset, length);

    case 'binary':
      return this.binaryWrite(string, offset, length);

    case 'base64':
      return this.base64Write(string, offset, length);

    case 'ucs2':
    case 'ucs-2':
      return this.ucs2Write(string, offset, length);

    default:
      throw new Error('Unknown encoding');
  }
};


// slice(start, end)
Buffer.prototype.slice = function(start, end) {
  if (end === undefined) end = this.length;

  if (end > this.length) {
    throw new Error('oob');
  }
  if (start > end) {
    throw new Error('oob');
  }

  return new Buffer(this, end - start, +start);
};

// copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)
Buffer.prototype.copy = function(target, target_start, start, end) {
  var source = this;
  start || (start = 0);
  if (end === undefined || isNaN(end)) {
    end = this.length;
  }
  target_start || (target_start = 0);

  if (end < start) throw new Error('sourceEnd < sourceStart');

  // Copy 0 bytes; we're done
  if (end === start) return 0;
  if (target.length == 0 || source.length == 0) return 0;

  if (target_start < 0 || target_start >= target.length) {
    throw new Error('targetStart out of bounds');
  }

  if (start < 0 || start >= source.length) {
    throw new Error('sourceStart out of bounds');
  }

  if (end < 0 || end > source.length) {
    throw new Error('sourceEnd out of bounds');
  }

  // Are we oob?
  if (end > this.length) {
    end = this.length;
  }

  if (target.length - target_start < end - start) {
    end = target.length - target_start + start;
  }

  var temp = [];
  for (var i=start; i<end; i++) {
    assert.ok(typeof this[i] !== 'undefined', "copying undefined buffer bytes!");
    temp.push(this[i]);
  }

  for (var i=target_start; i<target_start+temp.length; i++) {
    target[i] = temp[i-target_start];
  }
};

// fill(value, start=0, end=buffer.length)
Buffer.prototype.fill = function fill(value, start, end) {
  value || (value = 0);
  start || (start = 0);
  end || (end = this.length);

  if (typeof value === 'string') {
    value = value.charCodeAt(0);
  }
  if (!(typeof value === 'number') || isNaN(value)) {
    throw new Error('value is not a number');
  }

  if (end < start) throw new Error('end < start');

  // Fill 0 bytes; we're done
  if (end === start) return 0;
  if (this.length == 0) return 0;

  if (start < 0 || start >= this.length) {
    throw new Error('start out of bounds');
  }

  if (end < 0 || end > this.length) {
    throw new Error('end out of bounds');
  }

  for (var i = start; i < end; i++) {
    this[i] = value;
  }
}

// Static methods
Buffer.isBuffer = function isBuffer(b) {
  return b instanceof Buffer || b instanceof Buffer;
};

Buffer.concat = function (list, totalLength) {
  if (!isArray(list)) {
    throw new Error("Usage: Buffer.concat(list, [totalLength])\n \
      list should be an Array.");
  }

  if (list.length === 0) {
    return new Buffer(0);
  } else if (list.length === 1) {
    return list[0];
  }

  if (typeof totalLength !== 'number') {
    totalLength = 0;
    for (var i = 0; i < list.length; i++) {
      var buf = list[i];
      totalLength += buf.length;
    }
  }

  var buffer = new Buffer(totalLength);
  var pos = 0;
  for (var i = 0; i < list.length; i++) {
    var buf = list[i];
    buf.copy(buffer, pos);
    pos += buf.length;
  }
  return buffer;
};

// helpers

function coerce(length) {
  // Coerce length to a number (possibly NaN), round up
  // in case it's fractional (e.g. 123.456) then do a
  // double negate to coerce a NaN to 0. Easy, right?
  length = ~~Math.ceil(+length);
  return length < 0 ? 0 : length;
}

function isArray(subject) {
  return (Array.isArray ||
    function(subject){
      return {}.toString.apply(subject) == '[object Array]'
    })
    (subject)
}

function isArrayIsh(subject) {
  return isArray(subject) || Buffer.isBuffer(subject) ||
         subject && typeof subject === 'object' &&
         typeof subject.length === 'number';
}

function toHex(n) {
  if (n < 16) return '0' + n.toString(16);
  return n.toString(16);
}

function utf8ToBytes(str) {
  var byteArray = [];
  for (var i = 0; i < str.length; i++)
    if (str.charCodeAt(i) <= 0x7F)
      byteArray.push(str.charCodeAt(i));
    else {
      var h = encodeURIComponent(str.charAt(i)).substr(1).split('%');
      for (var j = 0; j < h.length; j++)
        byteArray.push(parseInt(h[j], 16));
    }

  return byteArray;
}

function asciiToBytes(str) {
  var byteArray = []
  for (var i = 0; i < str.length; i++ )
    // Node's code seems to be doing this and not & 0x7F..
    byteArray.push( str.charCodeAt(i) & 0xFF );

  return byteArray;
}

function base64ToBytes(str) {
  return require("base64-js").toByteArray(str);
}

function blitBuffer(src, dst, offset, length) {
  var pos, i = 0;
  while (i < length) {
    if ((i+offset >= dst.length) || (i >= src.length))
      break;

    dst[i + offset] = src[i];
    i++;
  }
  return i;
}

function decodeUtf8Char(str) {
  try {
    return decodeURIComponent(str);
  } catch (err) {
    return String.fromCharCode(0xFFFD); // UTF 8 invalid char
  }
}

// read/write bit-twiddling

Buffer.prototype.readUInt8 = function(offset, noAssert) {
  var buffer = this;

  if (!noAssert) {
    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset < buffer.length,
        'Trying to read beyond buffer length');
  }

  if (offset >= buffer.length) return;

  return buffer[offset];
};

function readUInt16(buffer, offset, isBigEndian, noAssert) {
  var val = 0;


  if (!noAssert) {
    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 1 < buffer.length,
        'Trying to read beyond buffer length');
  }

  if (offset >= buffer.length) return 0;

  if (isBigEndian) {
    val = buffer[offset] << 8;
    if (offset + 1 < buffer.length) {
      val |= buffer[offset + 1];
    }
  } else {
    val = buffer[offset];
    if (offset + 1 < buffer.length) {
      val |= buffer[offset + 1] << 8;
    }
  }

  return val;
}

Buffer.prototype.readUInt16LE = function(offset, noAssert) {
  return readUInt16(this, offset, false, noAssert);
};

Buffer.prototype.readUInt16BE = function(offset, noAssert) {
  return readUInt16(this, offset, true, noAssert);
};

function readUInt32(buffer, offset, isBigEndian, noAssert) {
  var val = 0;

  if (!noAssert) {
    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 3 < buffer.length,
        'Trying to read beyond buffer length');
  }

  if (offset >= buffer.length) return 0;

  if (isBigEndian) {
    if (offset + 1 < buffer.length)
      val = buffer[offset + 1] << 16;
    if (offset + 2 < buffer.length)
      val |= buffer[offset + 2] << 8;
    if (offset + 3 < buffer.length)
      val |= buffer[offset + 3];
    val = val + (buffer[offset] << 24 >>> 0);
  } else {
    if (offset + 2 < buffer.length)
      val = buffer[offset + 2] << 16;
    if (offset + 1 < buffer.length)
      val |= buffer[offset + 1] << 8;
    val |= buffer[offset];
    if (offset + 3 < buffer.length)
      val = val + (buffer[offset + 3] << 24 >>> 0);
  }

  return val;
}

Buffer.prototype.readUInt32LE = function(offset, noAssert) {
  return readUInt32(this, offset, false, noAssert);
};

Buffer.prototype.readUInt32BE = function(offset, noAssert) {
  return readUInt32(this, offset, true, noAssert);
};


/*
 * Signed integer types, yay team! A reminder on how two's complement actually
 * works. The first bit is the signed bit, i.e. tells us whether or not the
 * number should be positive or negative. If the two's complement value is
 * positive, then we're done, as it's equivalent to the unsigned representation.
 *
 * Now if the number is positive, you're pretty much done, you can just leverage
 * the unsigned translations and return those. Unfortunately, negative numbers
 * aren't quite that straightforward.
 *
 * At first glance, one might be inclined to use the traditional formula to
 * translate binary numbers between the positive and negative values in two's
 * complement. (Though it doesn't quite work for the most negative value)
 * Mainly:
 *  - invert all the bits
 *  - add one to the result
 *
 * Of course, this doesn't quite work in Javascript. Take for example the value
 * of -128. This could be represented in 16 bits (big-endian) as 0xff80. But of
 * course, Javascript will do the following:
 *
 * > ~0xff80
 * -65409
 *
 * Whoh there, Javascript, that's not quite right. But wait, according to
 * Javascript that's perfectly correct. When Javascript ends up seeing the
 * constant 0xff80, it has no notion that it is actually a signed number. It
 * assumes that we've input the unsigned value 0xff80. Thus, when it does the
 * binary negation, it casts it into a signed value, (positive 0xff80). Then
 * when you perform binary negation on that, it turns it into a negative number.
 *
 * Instead, we're going to have to use the following general formula, that works
 * in a rather Javascript friendly way. I'm glad we don't support this kind of
 * weird numbering scheme in the kernel.
 *
 * (BIT-MAX - (unsigned)val + 1) * -1
 *
 * The astute observer, may think that this doesn't make sense for 8-bit numbers
 * (really it isn't necessary for them). However, when you get 16-bit numbers,
 * you do. Let's go back to our prior example and see how this will look:
 *
 * (0xffff - 0xff80 + 1) * -1
 * (0x007f + 1) * -1
 * (0x0080) * -1
 */
Buffer.prototype.readInt8 = function(offset, noAssert) {
  var buffer = this;
  var neg;

  if (!noAssert) {
    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset < buffer.length,
        'Trying to read beyond buffer length');
  }

  if (offset >= buffer.length) return;

  neg = buffer[offset] & 0x80;
  if (!neg) {
    return (buffer[offset]);
  }

  return ((0xff - buffer[offset] + 1) * -1);
};

function readInt16(buffer, offset, isBigEndian, noAssert) {
  var neg, val;

  if (!noAssert) {
    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 1 < buffer.length,
        'Trying to read beyond buffer length');
  }

  val = readUInt16(buffer, offset, isBigEndian, noAssert);
  neg = val & 0x8000;
  if (!neg) {
    return val;
  }

  return (0xffff - val + 1) * -1;
}

Buffer.prototype.readInt16LE = function(offset, noAssert) {
  return readInt16(this, offset, false, noAssert);
};

Buffer.prototype.readInt16BE = function(offset, noAssert) {
  return readInt16(this, offset, true, noAssert);
};

function readInt32(buffer, offset, isBigEndian, noAssert) {
  var neg, val;

  if (!noAssert) {
    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 3 < buffer.length,
        'Trying to read beyond buffer length');
  }

  val = readUInt32(buffer, offset, isBigEndian, noAssert);
  neg = val & 0x80000000;
  if (!neg) {
    return (val);
  }

  return (0xffffffff - val + 1) * -1;
}

Buffer.prototype.readInt32LE = function(offset, noAssert) {
  return readInt32(this, offset, false, noAssert);
};

Buffer.prototype.readInt32BE = function(offset, noAssert) {
  return readInt32(this, offset, true, noAssert);
};

function readFloat(buffer, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset + 3 < buffer.length,
        'Trying to read beyond buffer length');
  }

  return require('./buffer_ieee754').readIEEE754(buffer, offset, isBigEndian,
      23, 4);
}

Buffer.prototype.readFloatLE = function(offset, noAssert) {
  return readFloat(this, offset, false, noAssert);
};

Buffer.prototype.readFloatBE = function(offset, noAssert) {
  return readFloat(this, offset, true, noAssert);
};

function readDouble(buffer, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset + 7 < buffer.length,
        'Trying to read beyond buffer length');
  }

  return require('./buffer_ieee754').readIEEE754(buffer, offset, isBigEndian,
      52, 8);
}

Buffer.prototype.readDoubleLE = function(offset, noAssert) {
  return readDouble(this, offset, false, noAssert);
};

Buffer.prototype.readDoubleBE = function(offset, noAssert) {
  return readDouble(this, offset, true, noAssert);
};


/*
 * We have to make sure that the value is a valid integer. This means that it is
 * non-negative. It has no fractional component and that it does not exceed the
 * maximum allowed value.
 *
 *      value           The number to check for validity
 *
 *      max             The maximum value
 */
function verifuint(value, max) {
  assert.ok(typeof (value) == 'number',
      'cannot write a non-number as a number');

  assert.ok(value >= 0,
      'specified a negative value for writing an unsigned value');

  assert.ok(value <= max, 'value is larger than maximum value for type');

  assert.ok(Math.floor(value) === value, 'value has a fractional component');
}

Buffer.prototype.writeUInt8 = function(value, offset, noAssert) {
  var buffer = this;

  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset < buffer.length,
        'trying to write beyond buffer length');

    verifuint(value, 0xff);
  }

  if (offset < buffer.length) {
    buffer[offset] = value;
  }
};

function writeUInt16(buffer, value, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 1 < buffer.length,
        'trying to write beyond buffer length');

    verifuint(value, 0xffff);
  }

  for (var i = 0; i < Math.min(buffer.length - offset, 2); i++) {
    buffer[offset + i] =
        (value & (0xff << (8 * (isBigEndian ? 1 - i : i)))) >>>
            (isBigEndian ? 1 - i : i) * 8;
  }

}

Buffer.prototype.writeUInt16LE = function(value, offset, noAssert) {
  writeUInt16(this, value, offset, false, noAssert);
};

Buffer.prototype.writeUInt16BE = function(value, offset, noAssert) {
  writeUInt16(this, value, offset, true, noAssert);
};

function writeUInt32(buffer, value, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 3 < buffer.length,
        'trying to write beyond buffer length');

    verifuint(value, 0xffffffff);
  }

  for (var i = 0; i < Math.min(buffer.length - offset, 4); i++) {
    buffer[offset + i] =
        (value >>> (isBigEndian ? 3 - i : i) * 8) & 0xff;
  }
}

Buffer.prototype.writeUInt32LE = function(value, offset, noAssert) {
  writeUInt32(this, value, offset, false, noAssert);
};

Buffer.prototype.writeUInt32BE = function(value, offset, noAssert) {
  writeUInt32(this, value, offset, true, noAssert);
};


/*
 * We now move onto our friends in the signed number category. Unlike unsigned
 * numbers, we're going to have to worry a bit more about how we put values into
 * arrays. Since we are only worrying about signed 32-bit values, we're in
 * slightly better shape. Unfortunately, we really can't do our favorite binary
 * & in this system. It really seems to do the wrong thing. For example:
 *
 * > -32 & 0xff
 * 224
 *
 * What's happening above is really: 0xe0 & 0xff = 0xe0. However, the results of
 * this aren't treated as a signed number. Ultimately a bad thing.
 *
 * What we're going to want to do is basically create the unsigned equivalent of
 * our representation and pass that off to the wuint* functions. To do that
 * we're going to do the following:
 *
 *  - if the value is positive
 *      we can pass it directly off to the equivalent wuint
 *  - if the value is negative
 *      we do the following computation:
 *         mb + val + 1, where
 *         mb   is the maximum unsigned value in that byte size
 *         val  is the Javascript negative integer
 *
 *
 * As a concrete value, take -128. In signed 16 bits this would be 0xff80. If
 * you do out the computations:
 *
 * 0xffff - 128 + 1
 * 0xffff - 127
 * 0xff80
 *
 * You can then encode this value as the signed version. This is really rather
 * hacky, but it should work and get the job done which is our goal here.
 */

/*
 * A series of checks to make sure we actually have a signed 32-bit number
 */
function verifsint(value, max, min) {
  assert.ok(typeof (value) == 'number',
      'cannot write a non-number as a number');

  assert.ok(value <= max, 'value larger than maximum allowed value');

  assert.ok(value >= min, 'value smaller than minimum allowed value');

  assert.ok(Math.floor(value) === value, 'value has a fractional component');
}

function verifIEEE754(value, max, min) {
  assert.ok(typeof (value) == 'number',
      'cannot write a non-number as a number');

  assert.ok(value <= max, 'value larger than maximum allowed value');

  assert.ok(value >= min, 'value smaller than minimum allowed value');
}

Buffer.prototype.writeInt8 = function(value, offset, noAssert) {
  var buffer = this;

  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset < buffer.length,
        'Trying to write beyond buffer length');

    verifsint(value, 0x7f, -0x80);
  }

  if (value >= 0) {
    buffer.writeUInt8(value, offset, noAssert);
  } else {
    buffer.writeUInt8(0xff + value + 1, offset, noAssert);
  }
};

function writeInt16(buffer, value, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 1 < buffer.length,
        'Trying to write beyond buffer length');

    verifsint(value, 0x7fff, -0x8000);
  }

  if (value >= 0) {
    writeUInt16(buffer, value, offset, isBigEndian, noAssert);
  } else {
    writeUInt16(buffer, 0xffff + value + 1, offset, isBigEndian, noAssert);
  }
}

Buffer.prototype.writeInt16LE = function(value, offset, noAssert) {
  writeInt16(this, value, offset, false, noAssert);
};

Buffer.prototype.writeInt16BE = function(value, offset, noAssert) {
  writeInt16(this, value, offset, true, noAssert);
};

function writeInt32(buffer, value, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 3 < buffer.length,
        'Trying to write beyond buffer length');

    verifsint(value, 0x7fffffff, -0x80000000);
  }

  if (value >= 0) {
    writeUInt32(buffer, value, offset, isBigEndian, noAssert);
  } else {
    writeUInt32(buffer, 0xffffffff + value + 1, offset, isBigEndian, noAssert);
  }
}

Buffer.prototype.writeInt32LE = function(value, offset, noAssert) {
  writeInt32(this, value, offset, false, noAssert);
};

Buffer.prototype.writeInt32BE = function(value, offset, noAssert) {
  writeInt32(this, value, offset, true, noAssert);
};

function writeFloat(buffer, value, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 3 < buffer.length,
        'Trying to write beyond buffer length');

    verifIEEE754(value, 3.4028234663852886e+38, -3.4028234663852886e+38);
  }

  require('./buffer_ieee754').writeIEEE754(buffer, value, offset, isBigEndian,
      23, 4);
}

Buffer.prototype.writeFloatLE = function(value, offset, noAssert) {
  writeFloat(this, value, offset, false, noAssert);
};

Buffer.prototype.writeFloatBE = function(value, offset, noAssert) {
  writeFloat(this, value, offset, true, noAssert);
};

function writeDouble(buffer, value, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 7 < buffer.length,
        'Trying to write beyond buffer length');

    verifIEEE754(value, 1.7976931348623157E+308, -1.7976931348623157E+308);
  }

  require('./buffer_ieee754').writeIEEE754(buffer, value, offset, isBigEndian,
      52, 8);
}

Buffer.prototype.writeDoubleLE = function(value, offset, noAssert) {
  writeDouble(this, value, offset, false, noAssert);
};

Buffer.prototype.writeDoubleBE = function(value, offset, noAssert) {
  writeDouble(this, value, offset, true, noAssert);
};

})()
},{"assert":12,"./buffer_ieee754":14,"base64-js":15}],15:[function(require,module,exports){
(function (exports) {
	'use strict';

	var lookup = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

	function b64ToByteArray(b64) {
		var i, j, l, tmp, placeHolders, arr;
	
		if (b64.length % 4 > 0) {
			throw 'Invalid string. Length must be a multiple of 4';
		}

		// the number of equal signs (place holders)
		// if there are two placeholders, than the two characters before it
		// represent one byte
		// if there is only one, then the three characters before it represent 2 bytes
		// this is just a cheap hack to not do indexOf twice
		placeHolders = b64.indexOf('=');
		placeHolders = placeHolders > 0 ? b64.length - placeHolders : 0;

		// base64 is 4/3 + up to two characters of the original data
		arr = [];//new Uint8Array(b64.length * 3 / 4 - placeHolders);

		// if there are placeholders, only get up to the last complete 4 chars
		l = placeHolders > 0 ? b64.length - 4 : b64.length;

		for (i = 0, j = 0; i < l; i += 4, j += 3) {
			tmp = (lookup.indexOf(b64[i]) << 18) | (lookup.indexOf(b64[i + 1]) << 12) | (lookup.indexOf(b64[i + 2]) << 6) | lookup.indexOf(b64[i + 3]);
			arr.push((tmp & 0xFF0000) >> 16);
			arr.push((tmp & 0xFF00) >> 8);
			arr.push(tmp & 0xFF);
		}

		if (placeHolders === 2) {
			tmp = (lookup.indexOf(b64[i]) << 2) | (lookup.indexOf(b64[i + 1]) >> 4);
			arr.push(tmp & 0xFF);
		} else if (placeHolders === 1) {
			tmp = (lookup.indexOf(b64[i]) << 10) | (lookup.indexOf(b64[i + 1]) << 4) | (lookup.indexOf(b64[i + 2]) >> 2);
			arr.push((tmp >> 8) & 0xFF);
			arr.push(tmp & 0xFF);
		}

		return arr;
	}

	function uint8ToBase64(uint8) {
		var i,
			extraBytes = uint8.length % 3, // if we have 1 byte left, pad 2 bytes
			output = "",
			temp, length;

		function tripletToBase64 (num) {
			return lookup[num >> 18 & 0x3F] + lookup[num >> 12 & 0x3F] + lookup[num >> 6 & 0x3F] + lookup[num & 0x3F];
		};

		// go through the array every three bytes, we'll deal with trailing stuff later
		for (i = 0, length = uint8.length - extraBytes; i < length; i += 3) {
			temp = (uint8[i] << 16) + (uint8[i + 1] << 8) + (uint8[i + 2]);
			output += tripletToBase64(temp);
		}

		// pad the end with zeros, but make sure to not forget the extra bytes
		switch (extraBytes) {
			case 1:
				temp = uint8[uint8.length - 1];
				output += lookup[temp >> 2];
				output += lookup[(temp << 4) & 0x3F];
				output += '==';
				break;
			case 2:
				temp = (uint8[uint8.length - 2] << 8) + (uint8[uint8.length - 1]);
				output += lookup[temp >> 10];
				output += lookup[(temp >> 4) & 0x3F];
				output += lookup[(temp << 2) & 0x3F];
				output += '=';
				break;
		}

		return output;
	}

	module.exports.toByteArray = b64ToByteArray;
	module.exports.fromByteArray = uint8ToBase64;
}());

},{}]},{},[1])(1)
});
;;
(function(e){if("function"==typeof bootstrap)bootstrap("istanbulsiestahtmlreport",e);else if("object"==typeof exports)module.exports=e();else if("function"==typeof define&&define.amd)define(e);else if("undefined"!=typeof ses){if(!ses.ok())return;ses.makeIstanbulSiestaHtmlReport=e}else"undefined"!=typeof window?window.IstanbulSiestaHtmlReport=e():global.IstanbulSiestaHtmlReport=e()})(function(){var define,ses,bootstrap,module,exports;
return (function(e,t,n){function i(n,s){if(!t[n]){if(!e[n]){var o=typeof require=="function"&&require;if(!s&&o)return o(n,!0);if(r)return r(n,!0);throw new Error("Cannot find module '"+n+"'")}var u=t[n]={exports:{}};e[n][0].call(u.exports,function(t){var r=e[n][1][t];return i(r?r:t)},u,u.exports)}return t[n].exports}var r=typeof require=="function"&&require;for(var s=0;s<n.length;s++)i(n[s]);return i})({1:[function(require,module,exports){
/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

var path = require('path'),
    SEP = path.sep || '/',
    utils = require('../object-utils');

function commonArrayPrefix(first, second) {
    var len = first.length < second.length ? first.length : second.length,
        i,
        ret = [];
    for (i = 0; i < len; i += 1) {
        if (first[i] === second[i]) {
            ret.push(first[i]);
        } else {
            break;
        }
    }
    return ret;
}

function findCommonArrayPrefix(args) {
    if (args.length === 0) {
        return [];
    }

    var separated = args.map(function (arg) { return arg.split(SEP); }),
        ret = separated.pop();

    if (separated.length === 0) {
        return ret.slice(0, ret.length - 1);
    } else {
        return separated.reduce(commonArrayPrefix, ret);
    }
}

function Node(fullName, kind, metrics) {
    this.name = fullName;
    this.fullName = fullName;
    this.kind = kind;
    this.metrics = metrics || null;
    this.parent = null;
    this.children = [];
}

Node.prototype = {
    displayShortName: function () {
        return this.relativeName;
    },
    fullPath: function () {
        return this.fullName;
    },
    addChild: function (child) {
        this.children.push(child);
        child.parent = this;
    },
    toJSON: function () {
        return {
            name: this.name,
            relativeName: this.relativeName,
            fullName: this.fullName,
            kind: this.kind,
            metrics: this.metrics,
            parent: this.parent === null ? null : this.parent.name,
            children: this.children.map(function (node) { return node.toJSON(); }),
            
            html        : this.html
        };
    }
};

function TreeSummary(summaryMap, commonPrefix, mode) {
    this.mode       = mode
    this.prefix     = commonPrefix;
    
    this.convertToTree(summaryMap, commonPrefix);
}

TreeSummary.prototype = {
    getNode: function (shortName) {
        return this.map[shortName];
    },

    convertToTree: function (summaryMap, arrayPrefix) {
        var rootPath        = arrayPrefix.join(SEP) || SEP
        var root            = new Node(rootPath, 'dir')
        var seen            = {}
        
        seen[ rootPath ]    = root;
        
        var specs           = Object.keys(summaryMap).map(function (path) {
            return {
                fullPath    : path, 
                parts       : path.split(SEP)
            }
        })
        
        specs.sort(function (a, b) {
            return a.parts.length - b.parts.length
        })
        
        specs.forEach(function (spec) {
            var key         = spec.fullPath
            var metrics     = summaryMap[ key ]
            
            var node        = new Node(key, 'file', metrics);
            
            seen[ key ]     = node
            
            var partsLen    = spec.parts.length
            
            for (var i = 0; i < partsLen; i++) {
                spec.parts.pop()  
                
                var parentPath  = spec.parts.join(SEP) || SEP
                var parent      = seen[ parentPath ]
                
                if (!parent) {
                    parent  = seen[ parentPath ] = new Node(parentPath, 'dir')
                    
                    parent.addChild(node)
                    
                    node    = parent
                } else {
                    parent.addChild(node)
                    break
                }
            }
        })
        
        if (root.children.length == 1 && root.children[ 0 ].kind == 'dir') {
            root            = root.children[ 0 ]
            root.parent     = null
        }
        
        this.fixupNodes(root, root.name);
        this.calculateMetrics(root);
        this.root = root;
        this.map = {};
        this.indexAndSortTree(root, this.map);
    },

    fixupNodes: function (node, prefix, parent) {
        var me  = this;
        
        if (parent) {
            node.relativeName   = node.name.split(SEP).pop()
            
//            if (node.name.indexOf(prefix) === 0) {
//                node.name = node.name.substring(prefix.length);
//            }
//            if (node.name.charAt(0) === SEP) {
//                node.name = node.name.substring(1);
//            }
//            if (parent) {
//                if (parent.name !== '__root__/') {
//                    node.relativeName = node.name.substring(parent.name.length);
//                } else {
//                    node.relativeName = node.name;
//                }
//            } else {
//                node.relativeName = node.name.substring(prefix.length);
//            }
        }
        
        node.children.forEach(function (child) {
            me.fixupNodes(child, prefix, node);
        });
    },
    calculateMetrics: function (entry) {
        var that = this,
            fileChildren;
        if (entry.kind !== 'dir') {return; }
        entry.children.forEach(function (child) {
            that.calculateMetrics(child);
        });
        entry.metrics = utils.mergeSummaryObjects.apply(
            null,
            entry.children.map(function (child) { return child.metrics; })
        );
        // calclulate "java-style" package metrics where there is no hierarchy
        // across packages
        fileChildren = entry.children.filter(function (n) { return n.kind !== 'dir'; });
        if (fileChildren.length > 0) {
            entry.packageMetrics = utils.mergeSummaryObjects.apply(
                null,
                fileChildren.map(function (child) { return child.metrics; })
            );
        } else {
            entry.packageMetrics = null;
        }
    },
    indexAndSortTree: function (node, map) {
        var that = this;
        map[node.name] = node;
        node.children.sort(function (a, b) {
            if (a.kind == 'dir' && b.kind == 'file') return 1
            if (a.kind == 'file' && b.kind == 'dir') return -1
            
            a = a.relativeName;
            b = b.relativeName;
            return a < b ? -1 : a > b ? 1 : 0;
        });
        node.children.forEach(function (child) {
            that.indexAndSortTree(child, map);
        });
    },
    toJSON: function () {
        return {
            prefix: this.prefix,
            root: this.root.toJSON()
        };
    }
};

function TreeSummarizer(opts) {
    this.summaryMap = {};
    
    this.opts       = opts
}

TreeSummarizer.prototype = {
    addFileCoverageSummary: function (filePath, metrics) {
        this.summaryMap[filePath] = metrics;
    },
    getTreeSummary: function () {
        var commonArrayPrefix = findCommonArrayPrefix(Object.keys(this.summaryMap));
        return new TreeSummary(this.summaryMap, this.opts.mode == 'trueTree' ? [] : commonArrayPrefix, this.opts.mode);
    }
};

module.exports = TreeSummarizer;

},{"path":2,"../object-utils":3}],4:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};

process.nextTick = (function () {
    var canSetImmediate = typeof window !== 'undefined'
    && window.setImmediate;
    var canPost = typeof window !== 'undefined'
    && window.postMessage && window.addEventListener
    ;

    if (canSetImmediate) {
        return function (f) { return window.setImmediate(f) };
    }

    if (canPost) {
        var queue = [];
        window.addEventListener('message', function (ev) {
            if (ev.source === window && ev.data === 'process-tick') {
                ev.stopPropagation();
                if (queue.length > 0) {
                    var fn = queue.shift();
                    fn();
                }
            }
        }, true);

        return function nextTick(fn) {
            queue.push(fn);
            window.postMessage('process-tick', '*');
        };
    }

    return function nextTick(fn) {
        setTimeout(fn, 0);
    };
})();

process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];

process.binding = function (name) {
    throw new Error('process.binding is not supported');
}

// TODO(shtylman)
process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};

},{}],2:[function(require,module,exports){
(function(process){function filter (xs, fn) {
    var res = [];
    for (var i = 0; i < xs.length; i++) {
        if (fn(xs[i], i, xs)) res.push(xs[i]);
    }
    return res;
}

// resolves . and .. elements in a path array with directory names there
// must be no slashes, empty elements, or device names (c:\) in the array
// (so also no leading and trailing slashes - it does not distinguish
// relative and absolute paths)
function normalizeArray(parts, allowAboveRoot) {
  // if the path tries to go above the root, `up` ends up > 0
  var up = 0;
  for (var i = parts.length; i >= 0; i--) {
    var last = parts[i];
    if (last == '.') {
      parts.splice(i, 1);
    } else if (last === '..') {
      parts.splice(i, 1);
      up++;
    } else if (up) {
      parts.splice(i, 1);
      up--;
    }
  }

  // if the path is allowed to go above the root, restore leading ..s
  if (allowAboveRoot) {
    for (; up--; up) {
      parts.unshift('..');
    }
  }

  return parts;
}

// Regex to split a filename into [*, dir, basename, ext]
// posix version
var splitPathRe = /^(.+\/(?!$)|\/)?((?:.+?)?(\.[^.]*)?)$/;

// path.resolve([from ...], to)
// posix version
exports.resolve = function() {
var resolvedPath = '',
    resolvedAbsolute = false;

for (var i = arguments.length; i >= -1 && !resolvedAbsolute; i--) {
  var path = (i >= 0)
      ? arguments[i]
      : process.cwd();

  // Skip empty and invalid entries
  if (typeof path !== 'string' || !path) {
    continue;
  }

  resolvedPath = path + '/' + resolvedPath;
  resolvedAbsolute = path.charAt(0) === '/';
}

// At this point the path should be resolved to a full absolute path, but
// handle relative paths to be safe (might happen when process.cwd() fails)

// Normalize the path
resolvedPath = normalizeArray(filter(resolvedPath.split('/'), function(p) {
    return !!p;
  }), !resolvedAbsolute).join('/');

  return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
};

// path.normalize(path)
// posix version
exports.normalize = function(path) {
var isAbsolute = path.charAt(0) === '/',
    trailingSlash = path.slice(-1) === '/';

// Normalize the path
path = normalizeArray(filter(path.split('/'), function(p) {
    return !!p;
  }), !isAbsolute).join('/');

  if (!path && !isAbsolute) {
    path = '.';
  }
  if (path && trailingSlash) {
    path += '/';
  }
  
  return (isAbsolute ? '/' : '') + path;
};


// posix version
exports.join = function() {
  var paths = Array.prototype.slice.call(arguments, 0);
  return exports.normalize(filter(paths, function(p, index) {
    return p && typeof p === 'string';
  }).join('/'));
};


exports.dirname = function(path) {
  var dir = splitPathRe.exec(path)[1] || '';
  var isWindows = false;
  if (!dir) {
    // No dirname
    return '.';
  } else if (dir.length === 1 ||
      (isWindows && dir.length <= 3 && dir.charAt(1) === ':')) {
    // It is just a slash or a drive letter with a slash
    return dir;
  } else {
    // It is a full dirname, strip trailing slash
    return dir.substring(0, dir.length - 1);
  }
};


exports.basename = function(path, ext) {
  var f = splitPathRe.exec(path)[2] || '';
  // TODO: make this comparison case-insensitive on windows?
  if (ext && f.substr(-1 * ext.length) === ext) {
    f = f.substr(0, f.length - ext.length);
  }
  return f;
};


exports.extname = function(path) {
  return splitPathRe.exec(path)[3] || '';
};

exports.relative = function(from, to) {
  from = exports.resolve(from).substr(1);
  to = exports.resolve(to).substr(1);

  function trim(arr) {
    var start = 0;
    for (; start < arr.length; start++) {
      if (arr[start] !== '') break;
    }

    var end = arr.length - 1;
    for (; end >= 0; end--) {
      if (arr[end] !== '') break;
    }

    if (start > end) return [];
    return arr.slice(start, end - start + 1);
  }

  var fromParts = trim(from.split('/'));
  var toParts = trim(to.split('/'));

  var length = Math.min(fromParts.length, toParts.length);
  var samePartsLength = length;
  for (var i = 0; i < length; i++) {
    if (fromParts[i] !== toParts[i]) {
      samePartsLength = i;
      break;
    }
  }

  var outputParts = [];
  for (var i = samePartsLength; i < fromParts.length; i++) {
    outputParts.push('..');
  }

  outputParts = outputParts.concat(toParts.slice(samePartsLength));

  return outputParts.join('/');
};

})(require("__browserify_process"))
},{"__browserify_process":4}],5:[function(require,module,exports){
/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

function InsertionText(text, consumeBlanks) {
    this.text = text;
    this.origLength = text.length;
    this.offsets = [];
    this.consumeBlanks = consumeBlanks;
    this.startPos = this.findFirstNonBlank();
    this.endPos = this.findLastNonBlank();
}

var WHITE_RE = /[ \f\n\r\t\v\u00A0\u2028\u2029]/;

InsertionText.prototype = {

    findFirstNonBlank: function () {
        var pos = -1,
            text = this.text,
            len = text.length,
            i;
        for (i = 0; i < len; i += 1) {
            if (!text.charAt(i).match(WHITE_RE)) {
                pos = i;
                break;
            }
        }
        return pos;
    },
    findLastNonBlank: function () {
        var text = this.text,
            len = text.length,
            pos = text.length + 1,
            i;
        for (i = len - 1; i >= 0; i -= 1) {
            if (!text.charAt(i).match(WHITE_RE)) {
                pos = i;
                break;
            }
        }
        return pos;
    },
    originalLength: function () {
        return this.origLength;
    },

    insertAt: function (col, str, insertBefore, consumeBlanks) {
        consumeBlanks = typeof consumeBlanks === 'undefined' ? this.consumeBlanks : consumeBlanks;
        col = col > this.originalLength() ? this.originalLength() : col;
        col = col < 0 ? 0 : col;

        if (consumeBlanks) {
            if (col <= this.startPos) {
                col = 0;
            }
            if (col > this.endPos) {
                col = this.origLength;
            }
        }

        var len = str.length,
            offset = this.findOffset(col, len, insertBefore),
            realPos = col + offset,
            text = this.text;
        this.text = text.substring(0, realPos) + str + text.substring(realPos);
        return this;
    },

    findOffset: function (pos, len, insertBefore) {
        var offsets = this.offsets,
            offsetObj,
            cumulativeOffset = 0,
            i;

        for (i = 0; i < offsets.length; i += 1) {
            offsetObj = offsets[i];
            if (offsetObj.pos < pos || (offsetObj.pos === pos && !insertBefore)) {
                cumulativeOffset += offsetObj.len;
            }
            if (offsetObj.pos >= pos) {
                break;
            }
        }
        if (offsetObj && offsetObj.pos === pos) {
            offsetObj.len += len;
        } else {
            offsets.splice(i, 0, { pos: pos, len: len });
        }
        return cumulativeOffset;
    },

    wrap: function (startPos, startText, endPos, endText, consumeBlanks) {
        this.insertAt(startPos, startText, true, consumeBlanks);
        this.insertAt(endPos, endText, false, consumeBlanks);
        return this;
    },

    wrapLine: function (startText, endText) {
        this.wrap(0, startText, this.originalLength(), endText);
    },

    toString: function () {
        return this.text;
    }
};

module.exports = InsertionText;
},{}],3:[function(require,module,exports){
(function(){/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

/**
 * utility methods to process coverage objects. A coverage object has the following
 * format.
 *
 *      {
 *          "/path/to/file1.js": { file1 coverage },
 *          "/path/to/file2.js": { file2 coverage }
 *      }
 *
 *  The internals of the file coverage object are intentionally not documented since
 *  it is not a public interface.
 *
 *  *Note:* When a method of this module has the word `File` in it, it will accept
 *  one of the sub-objects of the main coverage object as an argument. Other
 *  methods accept the higher level coverage object with multiple keys.
 *
 * Works on `node` as well as the browser.
 *
 * Usage on nodejs
 * ---------------
 *
 *      var objectUtils = require('istanbul').utils;
 *
 * Usage in a browser
 * ------------------
 *
 * Load this file using a `script` tag or other means. This will set `window.coverageUtils`
 * to this module's exports.
 *
 * @class ObjectUtils
 * @static
 */
(function (isNode) {
    /**
     * adds line coverage information to a file coverage object, reverse-engineering
     * it from statement coverage. The object passed in is updated in place.
     *
     * Note that if line coverage information is already present in the object,
     * it is not recomputed.
     *
     * @method addDerivedInfoForFile
     * @static
     * @param {Object} fileCoverage the coverage object for a single file
     */
    function addDerivedInfoForFile(fileCoverage) {
        var statementMap = fileCoverage.statementMap,
            statements = fileCoverage.s,
            lineMap;

        if (!fileCoverage.l) {
            fileCoverage.l = lineMap = {};
            Object.keys(statements).forEach(function (st) {
                var line = statementMap[st].start.line,
                    count = statements[st],
                    prevVal = lineMap[line];
                if (typeof prevVal === 'undefined' || prevVal < count) {
                    lineMap[line] = count;
                }
            });
        }
    }
    /**
     * adds line coverage information to all file coverage objects.
     *
     * @method addDerivedInfo
     * @static
     * @param {Object} coverage the coverage object
     */
    function addDerivedInfo(coverage) {
        Object.keys(coverage).forEach(function (k) {
            addDerivedInfoForFile(coverage[k]);
        });
    }
    /**
     * removes line coverage information from all file coverage objects
     * @method removeDerivedInfo
     * @static
     * @param {Object} coverage the coverage object
     */
    function removeDerivedInfo(coverage) {
        Object.keys(coverage).forEach(function (k) {
            delete coverage[k].l;
        });
    }

    function percent(covered, total) {
        var tmp;
        if (total > 0) {
            tmp = 1000 * 100 * covered / total + 5;
            return Math.floor(tmp / 10) / 100;
        } else {
            return 100.00;
        }
    }

    function computeSimpleTotals(fileCoverage, property) {
        var stats = fileCoverage[property],
            ret = { total: 0, covered: 0 };

        Object.keys(stats).forEach(function (key) {
            ret.total += 1;
            if (stats[key]) {
                ret.covered += 1;
            }
        });
        ret.pct = percent(ret.covered, ret.total);
        return ret;
    }

    function computeBranchTotals(fileCoverage) {
        var stats = fileCoverage.b,
            ret = { total: 0, covered: 0 };

        Object.keys(stats).forEach(function (key) {
            var branches = stats[key],
                covered = branches.filter(function (num) { return num > 0; });
            ret.total += branches.length;
            ret.covered += covered.length;
        });
        ret.pct = percent(ret.covered, ret.total);
        return ret;
    }
    /**
     * returns a blank summary metrics object. A metrics object has the following
     * format.
     *
     *      {
     *          lines: lineMetrics,
     *          statements: statementMetrics,
     *          functions: functionMetrics,
     *          branches: branchMetrics
     *      }
     *
     *  Each individual metric object looks as follows:
     *
     *      {
     *          total: n,
     *          covered: m,
     *          pct: percent
     *      }
     *
     * @method blankSummary
     * @static
     * @return {Object} a blank metrics object
     */
    function blankSummary() {
        return {
            lines: {
                total: 0,
                covered: 0,
                pct: 'Unknown'
            },
            statements: {
                total: 0,
                covered: 0,
                pct: 'Unknown'
            },
            functions: {
                total: 0,
                covered: 0,
                pct: 'Unknown'
            },
            branches: {
                total: 0,
                covered: 0,
                pct: 'Unknown'
            }
        };
    }
    /**
     * returns the summary metrics given the coverage object for a single file. See `blankSummary()`
     * to understand the format of the returned object.
     *
     * @method summarizeFileCoverage
     * @static
     * @param {Object} fileCoverage the coverage object for a single file.
     * @return {Object} the summary metrics for the file
     */
    function summarizeFileCoverage(fileCoverage) {
        var ret = blankSummary();
        addDerivedInfoForFile(fileCoverage);
        ret.lines = computeSimpleTotals(fileCoverage, 'l');
        ret.functions = computeSimpleTotals(fileCoverage, 'f');
        ret.statements = computeSimpleTotals(fileCoverage, 's');
        ret.branches = computeBranchTotals(fileCoverage);
        return ret;
    }
    /**
     * merges two instances of file coverage objects *for the same file*
     * such that the execution counts are correct.
     *
     * @method mergeFileCoverage
     * @static
     * @param {Object} first the first file coverage object for a given file
     * @param {Object} second the second file coverage object for the same file
     * @return {Object} an object that is a result of merging the two. Note that
     *      the input objects are not changed in any way.
     */
    function mergeFileCoverage(first, second) {
        var ret = JSON.parse(JSON.stringify(first)),
            i;

        delete ret.l; //remove derived info

        Object.keys(second.s).forEach(function (k) {
            ret.s[k] += second.s[k];
        });
        Object.keys(second.f).forEach(function (k) {
            ret.f[k] += second.f[k];
        });
        Object.keys(second.b).forEach(function (k) {
            var retArray = ret.b[k],
                secondArray = second.b[k];
            for (i = 0; i < retArray.length; i += 1) {
                retArray[i] += secondArray[i];
            }
        });

        return ret;
    }
    /**
     * merges multiple summary metrics objects by summing up the `totals` and
     * `covered` fields and recomputing the percentages. This function is generic
     * and can accept any number of arguments.
     *
     * @method mergeSummaryObjects
     * @static
     * @param {Object} summary... multiple summary metrics objects
     * @return {Object} the merged summary metrics
     */
    function mergeSummaryObjects() {
        var ret = blankSummary(),
            args = Array.prototype.slice.call(arguments),
            keys = ['lines', 'statements', 'branches', 'functions'],
            increment = function (obj) {
                if (obj) {
                    keys.forEach(function (key) {
                        ret[key].total += obj[key].total;
                        ret[key].covered += obj[key].covered;
                    });
                }
            };
        args.forEach(function (arg) {
            increment(arg);
        });
        keys.forEach(function (key) {
            ret[key].pct = percent(ret[key].covered, ret[key].total);
        });

        return ret;
    }
    /**
     * returns the coverage summary for a single coverage object. This is
     * wrapper over `summarizeFileCoverage` and `mergeSummaryObjects` for
     * the common case of a single coverage object
     * @method summarizeCoverage
     * @static
     * @param {Object} coverage  the coverage object
     * @return {Object} summary coverage metrics across all files in the coverage object
     */
    function summarizeCoverage(coverage) {
        var fileSummary = [];
        Object.keys(coverage).forEach(function (key) {
            fileSummary.push(summarizeFileCoverage(coverage[key]));
        });
        return mergeSummaryObjects.apply(null, fileSummary);
    }

    /**
     * makes the coverage object generated by this library yuitest_coverage compatible.
     * Note that this transformation is lossy since the returned object will not have
     * statement and branch coverage.
     *
     * @method toYUICoverage
     * @static
     * @param {Object} coverage The `istanbul` coverage object
     * @return {Object} a coverage object in `yuitest_coverage` format.
     */
    function toYUICoverage(coverage) {
        var ret = {};

        addDerivedInfo(coverage);

        Object.keys(coverage).forEach(function (k) {
            var fileCoverage = coverage[k],
                lines = fileCoverage.l,
                functions = fileCoverage.f,
                fnMap = fileCoverage.fnMap,
                o;

            o = ret[k] = {
                lines: {},
                calledLines: 0,
                coveredLines: 0,
                functions: {},
                calledFunctions: 0,
                coveredFunctions: 0
            };
            Object.keys(lines).forEach(function (k) {
                o.lines[k] = lines[k];
                o.coveredLines += 1;
                if (lines[k] > 0) {
                    o.calledLines += 1;
                }
            });
            Object.keys(functions).forEach(function (k) {
                var name = fnMap[k].name + ':' + fnMap[k].line;
                o.functions[name] = functions[k];
                o.coveredFunctions += 1;
                if (functions[k] > 0) {
                    o.calledFunctions += 1;
                }
            });
        });
        return ret;
    }

    var exportables = {
        addDerivedInfo: addDerivedInfo,
        addDerivedInfoForFile: addDerivedInfoForFile,
        removeDerivedInfo: removeDerivedInfo,
        blankSummary: blankSummary,
        summarizeFileCoverage: summarizeFileCoverage,
        summarizeCoverage: summarizeCoverage,
        mergeFileCoverage: mergeFileCoverage,
        mergeSummaryObjects: mergeSummaryObjects,
        toYUICoverage: toYUICoverage
    };

    if (isNode) {
        module.exports = exportables;
    } else {
        window.coverageUtils = exportables;
    }
}(typeof module !== 'undefined' && typeof module.exports !== 'undefined' && typeof exports !== 'undefined'));


})()
},{}],6:[function(require,module,exports){
(function(){/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

/*jshint maxlen: 300 */
var handlebars          = require('handlebars'),
//    path = require('path'),
//    SEP = path.sep || '/',
//    fs = require('fs'),
//    util = require('util'),
//    FileWriter = require('../util/file-writer'),
//    Report = require('./index'),
//    Store = require('../store'),
    InsertionText       = require('../util/insertion-text'),
    TreeSummarizer      = require('../util/istanbul_siesta_tree_summarizer.js'),
    utils               = require('../object-utils'),
//    templateFor = function (name) { return handlebars.compile(fs.readFileSync(path.resolve(__dirname, 'templates', name + '.txt'), 'utf8')); },
//    headerTemplate = templateFor('head'),
//    footerTemplate = templateFor('foot'),
//    pathTemplate = handlebars.compile('<div class="path">{{{html}}}</div>'),
    detailTemplate      = handlebars.compile([
        '<pre class="coverage"><table class="coverage">',
            '<tr>',
                '<td class="line-count">{{#show_lines}}{{maxLines}}{{/show_lines}}</td>',
                '<td class="line-coverage">{{#show_line_execution_counts fileCoverage}}{{maxLines}}{{/show_line_execution_counts}}</td>',
                '<td class="text"><pre class="prettyprint lang-js">{{#show_code structured}}{{/show_code}}</pre></td>',
            '</tr>',
        '</table></pre>\n'
    ].join('')),
//    summaryTableHeader = [
//        '<div class="coverage-summary">',
//        '<table>',
//        '<thead>',
//        '<tr>',
//        '   <th data-col="file" data-fmt="html" data-html="true" class="file">File</th>',
//        '   <th data-col="pic" data-type="number" data-fmt="html" data-html="true" class="pic"></th>',
//        '   <th data-col="statements" data-type="number" data-fmt="pct" class="pct">Statements</th>',
//        '   <th data-col="statements_raw" data-type="number" data-fmt="html" class="abs"></th>',
//        '   <th data-col="branches" data-type="number" data-fmt="pct" class="pct">Branches</th>',
//        '   <th data-col="branches_raw" data-type="number" data-fmt="html" class="abs"></th>',
//        '   <th data-col="functions" data-type="number" data-fmt="pct" class="pct">Functions</th>',
//        '   <th data-col="functions_raw" data-type="number" data-fmt="html" class="abs"></th>',
//        '   <th data-col="lines" data-type="number" data-fmt="pct" class="pct">Lines</th>',
//        '   <th data-col="lines_raw" data-type="number" data-fmt="html" class="abs"></th>',
//        '</tr>',
//        '</thead>',
//        '<tbody>'
//    ].join('\n'),
//    summaryLineTemplate = handlebars.compile([
//        '<tr>',
//        '<td class="file {{reportClasses.statements}}" data-value="{{file}}"><a href="{{output}}">{{file}}</a></td>',
//        '<td data-value="{{metrics.statements.pct}}" class="pic {{reportClasses.statements}}">{{#show_picture}}{{metrics.statements.pct}}{{/show_picture}}</td>',
//        '<td data-value="{{metrics.statements.pct}}" class="pct {{reportClasses.statements}}">{{metrics.statements.pct}}%</td>',
//        '<td data-value="{{metrics.statements.total}}" class="abs {{reportClasses.statements}}">({{metrics.statements.covered}}&nbsp;/&nbsp;{{metrics.statements.total}})</td>',
//        '<td data-value="{{metrics.branches.pct}}" class="pct {{reportClasses.branches}}">{{metrics.branches.pct}}%</td>',
//        '<td data-value="{{metrics.branches.total}}" class="abs {{reportClasses.branches}}">({{metrics.branches.covered}}&nbsp;/&nbsp;{{metrics.branches.total}})</td>',
//        '<td data-value="{{metrics.functions.pct}}" class="pct {{reportClasses.functions}}">{{metrics.functions.pct}}%</td>',
//        '<td data-value="{{metrics.functions.total}}" class="abs {{reportClasses.functions}}">({{metrics.functions.covered}}&nbsp;/&nbsp;{{metrics.functions.total}})</td>',
//        '<td data-value="{{metrics.lines.pct}}" class="pct {{reportClasses.lines}}">{{metrics.lines.pct}}%</td>',
//        '<td data-value="{{metrics.lines.total}}" class="abs {{reportClasses.lines}}">({{metrics.lines.covered}}&nbsp;/&nbsp;{{metrics.lines.total}})</td>',
//        '</tr>\n'
//    ].join('\n\t')),
//    summaryTableFooter = [
//        '</tbody>',
//        '</table>',
//        '</div>'
//    ].join('\n'),
    lt          = '\u0001',
    gt          = '\u0002',
    RE_LT       = /</g,
    RE_GT       = />/g,
    RE_AMP      = /&/g,
    RE_lt       = /\u0001/g,
    RE_gt       = /\u0002/g;

//handlebars.registerHelper('show_picture', function (opts) {
//    var num = Number(opts.fn(this)),
//        rest,
//        cls = '';
//    if (isFinite(num)) {
//        if (num === 100) {
//            cls = ' cover-full';
//        }
//        num = Math.floor(num);
//        rest = 100 - num;
//        return '<span class="cover-fill' + cls + '" style="width: ' + num + 'px;"></span>' +
//            '<span class="cover-empty" style="width:' + rest + 'px;"></span>';
//    } else {
//        return '';
//    }
//});

handlebars.registerHelper('show_lines', function (opts) {
    var maxLines = Number(opts.fn(this)),
        i,
        array = [];

    for (i = 0; i < maxLines; i += 1) {
        array[i] = i + 1;
    }
    return array.join('\n');
});

handlebars.registerHelper('show_line_execution_counts', function (context, opts) {
    var lines = context.l,
        maxLines = Number(opts.fn(this)),
        i,
        lineNumber,
        array = [],
        covered,
        value = '';

    for (i = 0; i < maxLines; i += 1) {
        lineNumber = i + 1;
        value = '&nbsp;';
        covered = 'neutral';
        if (lines.hasOwnProperty(lineNumber)) {
            if (lines[lineNumber] > 0) {
                covered = 'yes';
                value = lines[lineNumber];
            } else {
                covered = 'no';
            }
        }
        array.push('<span class="cline-any cline-' + covered + '">' + value + '</span>');
    }
    return array.join('\n');
});

function customEscape(text) {
    text = text.toString();
    return text.replace(RE_AMP, '&amp;')
        .replace(RE_LT, '&lt;')
        .replace(RE_GT, '&gt;')
        .replace(RE_lt, '<')
        .replace(RE_gt, '>');
}

handlebars.registerHelper('show_code', function (context /*, opts */) {
    var array = [];

    context.forEach(function (item) {
        array.push(customEscape(item.text) || '&nbsp;');
    });
    return array.join('\n');
});

function title(str) {
    return ' title="' + str + '" ';
}

function annotateLines(fileCoverage, structuredText) {
    var lineStats = fileCoverage.l;
    if (!lineStats) { return; }
    Object.keys(lineStats).forEach(function (lineNumber) {
        var count = lineStats[lineNumber];
        structuredText[lineNumber].covered = count > 0 ? 'yes' : 'no';
    });
    structuredText.forEach(function (item) {
        if (item.covered === null) {
            item.covered = 'neutral';
        }
    });
}

function annotateStatements(fileCoverage, structuredText) {
    var statementStats = fileCoverage.s,
        statementMeta = fileCoverage.statementMap;
    Object.keys(statementStats).forEach(function (stName) {
        var count = statementStats[stName],
            meta = statementMeta[stName],
            type = count > 0 ? 'yes' : 'no',
            startCol = meta.start.column,
            endCol = meta.end.column + 1,
            startLine = meta.start.line,
            endLine = meta.end.line,
            openSpan = lt + 'span class="cstat-no"' + title('statement not covered') + gt,
            closeSpan = lt + '/span' + gt,
            text;

        if (type === 'no') {
            if (endLine !== startLine) {
                endLine = startLine;
                endCol = structuredText[startLine].text.originalLength();
            }
            text = structuredText[startLine].text;
            text.wrap(startCol,
                openSpan,
                startLine === endLine ? endCol : text.originalLength(),
                closeSpan);
        }
    });
}

function annotateFunctions(fileCoverage, structuredText) {

    var fnStats = fileCoverage.f,
        fnMeta = fileCoverage.fnMap;
    if (!fnStats) { return; }
    Object.keys(fnStats).forEach(function (fName) {
        var count = fnStats[fName],
            meta = fnMeta[fName],
            type = count > 0 ? 'yes' : 'no',
            startCol = meta.loc.start.column,
            endCol = meta.loc.end.column + 1,
            startLine = meta.loc.start.line,
            endLine = meta.loc.end.line,
            openSpan = lt + 'span class="fstat-no"' + title('function not covered') + gt,
            closeSpan = lt + '/span' + gt,
            text;

        if (type === 'no') {
            if (endLine !== startLine) {
                endLine = startLine;
                endCol = structuredText[startLine].text.originalLength();
            }
            text = structuredText[startLine].text;
            text.wrap(startCol,
                openSpan,
                startLine === endLine ? endCol : text.originalLength(),
                closeSpan);
        }
    });
}

function annotateBranches(fileCoverage, structuredText) {
    var branchStats = fileCoverage.b,
        branchMeta = fileCoverage.branchMap;
    if (!branchStats) { return; }

    Object.keys(branchStats).forEach(function (branchName) {
        var branchArray = branchStats[branchName],
            sumCount = branchArray.reduce(function (p, n) { return p + n; }, 0),
            metaArray = branchMeta[branchName].locations,
            i,
            count,
            meta,
            type,
            startCol,
            endCol,
            startLine,
            endLine,
            openSpan,
            closeSpan,
            text;

        if (sumCount > 0) { //only highlight if partial branches are missing
            for (i = 0; i < branchArray.length; i += 1) {
                count = branchArray[i];
                meta = metaArray[i];
                type = count > 0 ? 'yes' : 'no';
                startCol = meta.start.column;
                endCol = meta.end.column + 1;
                startLine = meta.start.line;
                endLine = meta.end.line;
                openSpan = lt + 'span class="branch-' + i + ' cbranch-no"' + title('branch not covered') + gt;
                closeSpan = lt + '/span' + gt;

                if (count === 0) { //skip branches taken
                    if (endLine !== startLine) {
                        endLine = startLine;
                        endCol = structuredText[startLine].text.originalLength();
                    }
                    text = structuredText[startLine].text;
                    if (branchMeta[branchName].type === 'if') { // and 'if' is a special case since the else branch might not be visible, being non-existent
                        text.insertAt(startCol, lt + 'span class="missing-if-branch"' +
                            title((i === 0 ? 'if' : 'else') + ' path not taken"') + gt +
                            (i === 0 ? 'I' : 'E')  + lt + '/span' + gt, true, false);
                    } else {
                        text.wrap(startCol,
                            openSpan,
                            startLine === endLine ? endCol : text.originalLength(),
                            closeSpan);
                    }
                }
            }
        }
    });
}

function getReportClass(stats) {
    var coveragePct = stats.pct,
        identity  = 1;
    if (coveragePct * identity === coveragePct) {
        return coveragePct >= 80 ? 'high' : coveragePct >= 50 ? 'medium' : 'low';
    } else {
        return '';
    }
}

/**
 * a `Report` implementation that produces HTML coverage reports.
 *
 * Usage
 * -----
 *
 *      var report = require('istanbul').Report.create('html');
 *
 *
 * @class SiestaReport
 * @extends Report
 * @constructor
 * @param {Object} opts optional
 * @param {String} [opts.dir] the directory in which to generate reports. Defaults to `./html-report`
 */
function SiestaReport(opts) {
//    Report.call(this);
    this.opts = opts || {};
//    this.opts.dir = this.opts.dir || path.resolve(process.cwd(), 'html-report');
//    this.opts.sourceStore = this.opts.sourceStore || Store.create('fslookup');
//    this.opts.linkMapper = this.opts.linkMapper || this.standardLinkMapper();
//    this.opts.writer = this.opts.writer || null;
//    this.opts.templateData = { datetime: Date() };
}

//SiestaReport.TYPE = 'html';
//util.inherits(SiestaReport, Report);

SiestaReport.prototype = {
    
    coverageNoSource        : false,

//    getPathHtml: function (node, linkMapper) {
//        var parent = node.parent,
//            nodePath = [],
//            linkPath = [],
//            i;
//
//        while (parent) {
//            nodePath.push(parent);
//            parent = parent.parent;
//        }
//
//        for (i = 0; i < nodePath.length; i += 1) {
//            linkPath.push('<a href="' + linkMapper.ancestor(node, i + 1) + '">' +
//                (nodePath[i].relativeName || 'All files') + '</a>');
//        }
//        linkPath.reverse();
//        return linkPath.length > 0 ? linkPath.join(' &#187; ') + ' &#187; ' +
//            node.displayShortName() : '';
//    },

//    fillTemplate: function (node, templateData) {
//        var opts = this.opts,
//            linkMapper = opts.linkMapper;
//
//        templateData.entity = node.name || 'All files';
//        templateData.metrics = node.metrics;
//        templateData.reportClass = getReportClass(node.metrics.statements);
//        templateData.pathHtml = pathTemplate({ html: this.getPathHtml(node, linkMapper) });
//        templateData.prettify = {
//            js: linkMapper.asset(node, 'prettify.js'),
//            css: linkMapper.asset(node, 'prettify.css')
//        };
//    },
    
    visitFileNode: function (node, fileCoverage, contentManager) {
        var opts            = this.opts,
//            sourceStore     = opts.sourceStore,
//            templateData    = opts.templateData,
//            sourceText      = fileCoverage.code && Array.isArray(fileCoverage.code) ? fileCoverage.code.join('\n') + '\n' : sourceStore.get(fileCoverage.path),
            sourceText      = contentManager.getLogicalUnitContent(fileCoverage.path),
            code            = (sourceText || '').split(/\r?\n/),
            count           = 0,
            structured      = code.map(function (str) { count += 1; return { line: count, covered: null, text: new InsertionText(str, true) }; })
//            ,
//            context;

        structured.unshift({ line: 0, covered: null, text: new InsertionText("") });

//        this.fillTemplate(node, templateData);
//        writer.write(headerTemplate(templateData));
//        writer.write('<pre><table class="coverage">\n');

        annotateLines(fileCoverage, structured);
        //note: order is important, since statements typically result in spanning the whole line and doing branches late
        //causes mismatched tags
        annotateBranches(fileCoverage, structured);
        annotateFunctions(fileCoverage, structured);
        annotateStatements(fileCoverage, structured);

        structured.shift();
        
        if (!this.coverageNoSource) {
            node.coverageInfo   = structured
            node.fileCoverage   = fileCoverage
            
            node.html           = detailTemplate({
                structured      : structured,
                maxLines        : structured.length,
                fileCoverage    : fileCoverage
            })
        }
//        writer.write(detailTemplate(context));
//        writer.write('</table></pre>\n');
//        writer.write(footerTemplate(templateData));
    },

    visitDirectoryNode: function (node) {
//        var linkMapper = this.opts.linkMapper,
//            templateData = this.opts.templateData,
            
        var children    = node.children.slice();

        children.sort(function (a, b) {
            return a.name < b.name ? -1 : 1;
        });

//        this.fillTemplate(node, templateData);
//        writer.write(headerTemplate(templateData));
//        writer.write(summaryTableHeader);
        var info        = []

        children.forEach(function (child) {
            var metrics             = child.metrics,
                reportClasses       = {
                    statements      : getReportClass(metrics.statements),
                    lines           : getReportClass(metrics.lines),
                    functions       : getReportClass(metrics.functions),
                    branches        : getReportClass(metrics.branches)
                },
                data                = {
                    metrics         : metrics,
                    reportClasses   : reportClasses,
                    file            : child.displayShortName()
//                    ,
//                    output: linkMapper.fromParent(child)
                };
                
            info.push(data)
//            writer.write(summaryLineTemplate(data) + '\n');
        });
//        writer.write(summaryTableFooter);
//        writer.write(footerTemplate(templateData));

        node.coverageInfo   = info
    },

    walkFiles: function (node, collector, contentManager) {
        var me      = this
//            indexFile = path.resolve(dir, 'index.html'),
//            childFile;
//        if (this.opts.verbose) { console.error('Writing ' + indexFile); }
//        writer.writeFile(indexFile, function (contentWriter) {
        me.visitDirectoryNode(node);
//        });
        node.children.forEach(function (child) {
            if (child.kind === 'dir') {
                me.walkFiles(child, collector, contentManager);
            } else {
//                childFile = path.resolve(dir, child.relativeName + '.html');
//                if (me.opts.verbose) { console.error('Writing ' + childFile); }
//                writer.writeFile(childFile, function (contentWriter) {
                me.visitFileNode(child, collector.fileCoverageFor(child.fullPath()), contentManager);
//                });
            }
        });
    },

//    standardLinkMapper: function () {
//        return {
//            fromParent: function (node) {
//                var i = 0,
//                    relativeName = node.relativeName,
//                    ch;
//                if (SEP !== '/') {
//                    relativeName = '';
//                    for (i = 0; i < node.relativeName.length; i += 1) {
//                        ch = node.relativeName.charAt(i);
//                        if (ch === SEP) {
//                            relativeName += '/';
//                        } else {
//                            relativeName += ch;
//                        }
//                    }
//                }
//                return node.kind === 'dir' ? relativeName + 'index.html' : relativeName + '.html';
//            },
//            ancestorHref: function (node, num) {
//                var href = '',
//                    separated,
//                    levels,
//                    i,
//                    j;
//                for (i = 0; i < num; i += 1) {
//                    separated = node.relativeName.split(SEP);
//                    levels = separated.length - 1;
//                    for (j = 0; j < levels; j += 1) {
//                        href += '../';
//                    }
//                    node = node.parent;
//                }
//                return href;
//            },
//            ancestor: function (node, num) {
//                return this.ancestorHref(node, num) + 'index.html';
//            },
//            asset: function (node, name) {
//                var i = 0,
//                    parent = node.parent;
//                while (parent) { i += 1; parent = parent.parent; }
//                return this.ancestorHref(node, i) + name;
//            }
//        };
//    },

    getTreeReport: function (collector, contentManager, coverageNoSource) {
//        var opts        = this.opts,
//            dir         = opts.dir,
        
        // a bit ugly to use shared state, but will work
        this.coverageNoSource   = coverageNoSource;
        
        var summarizer  = new TreeSummarizer({ mode : this.opts.mode })
//        ,
//            writer = opts.writer || new FileWriter(sync),
//            tree;
        
//        console.time('adding')

        collector.files().forEach(function (key) {
            summarizer.addFileCoverageSummary(key, utils.summarizeFileCoverage(collector.fileCoverageFor(key)));
        });
        
//        console.timeEnd('adding')
        
//        console.time('getTreeSummary')
        
        var tree        = summarizer.getTreeSummary();
//        fs.readdirSync(path.resolve(__dirname, '..', 'vendor')).forEach(function (f) {
//            var resolvedSource = path.resolve(__dirname, '..', 'vendor', f),
//                resolvedDestination = path.resolve(dir, f),
//                stat = fs.statSync(resolvedSource);
//
//            if (stat.isFile()) {
//                if (opts.verbose) {
//                    console.log('Write asset: ' + resolvedDestination);
//                }
//                writer.copyFile(resolvedSource, resolvedDestination);
//            }
//        });
        //console.log(JSON.stringify(tree.root, undefined, 4));
        
//        console.timeEnd('getTreeSummary')
        
//        console.time('walkFiles')
        
        this.walkFiles(tree.root, collector, contentManager);
        
//        console.timeEnd('walkFiles')
        
        return tree
    }
};

module.exports = SiestaReport;


})()
},{"../util/istanbul_siesta_tree_summarizer.js":1,"../util/insertion-text":5,"../object-utils":3,"handlebars":7}],8:[function(require,module,exports){
// nothing to see here... no file methods for the browser

},{}],7:[function(require,module,exports){
var handlebars = require("./handlebars/base"),

// Each of these augment the Handlebars object. No need to setup here.
// (This is done to easily share code between commonjs and browse envs)
  utils = require("./handlebars/utils"),
  compiler = require("./handlebars/compiler"),
  runtime = require("./handlebars/runtime");

var create = function() {
  var hb = handlebars.create();

  utils.attach(hb);
  compiler.attach(hb);
  runtime.attach(hb);

  return hb;
};

var Handlebars = create();
Handlebars.create = create;

module.exports = Handlebars; // instantiate an instance

// Publish a Node.js require() handler for .handlebars and .hbs files
if (require.extensions) {
  var extension = function(module, filename) {
    var fs = require("fs");
    var templateString = fs.readFileSync(filename, "utf8");
    module.exports = Handlebars.compile(templateString);
  };
  require.extensions[".handlebars"] = extension;
  require.extensions[".hbs"] = extension;
}

// BEGIN(BROWSER)

// END(BROWSER)

// USAGE:
// var handlebars = require('handlebars');

// var singleton = handlebars.Handlebars,
//  local = handlebars.create();

},{"fs":8,"./handlebars/base":9,"./handlebars/utils":10,"./handlebars/runtime":11,"./handlebars/compiler":12}],11:[function(require,module,exports){
exports.attach = function(Handlebars) {

// BEGIN(BROWSER)

Handlebars.VM = {
  template: function(templateSpec) {
    // Just add water
    var container = {
      escapeExpression: Handlebars.Utils.escapeExpression,
      invokePartial: Handlebars.VM.invokePartial,
      programs: [],
      program: function(i, fn, data) {
        var programWrapper = this.programs[i];
        if(data) {
          programWrapper = Handlebars.VM.program(i, fn, data);
        } else if (!programWrapper) {
          programWrapper = this.programs[i] = Handlebars.VM.program(i, fn);
        }
        return programWrapper;
      },
      merge: function(param, common) {
        var ret = param || common;

        if (param && common) {
          ret = {};
          Handlebars.Utils.extend(ret, common);
          Handlebars.Utils.extend(ret, param);
        }
        return ret;
      },
      programWithDepth: Handlebars.VM.programWithDepth,
      noop: Handlebars.VM.noop,
      compilerInfo: null
    };

    return function(context, options) {
      options = options || {};
      var result = templateSpec.call(container, Handlebars, context, options.helpers, options.partials, options.data);

      var compilerInfo = container.compilerInfo || [],
          compilerRevision = compilerInfo[0] || 1,
          currentRevision = Handlebars.COMPILER_REVISION;

      if (compilerRevision !== currentRevision) {
        if (compilerRevision < currentRevision) {
          var runtimeVersions = Handlebars.REVISION_CHANGES[currentRevision],
              compilerVersions = Handlebars.REVISION_CHANGES[compilerRevision];
          throw "Template was precompiled with an older version of Handlebars than the current runtime. "+
                "Please update your precompiler to a newer version ("+runtimeVersions+") or downgrade your runtime to an older version ("+compilerVersions+").";
        } else {
          // Use the embedded version info since the runtime doesn't know about this revision yet
          throw "Template was precompiled with a newer version of Handlebars than the current runtime. "+
                "Please update your runtime to a newer version ("+compilerInfo[1]+").";
        }
      }

      return result;
    };
  },

  programWithDepth: function(i, fn, data /*, $depth */) {
    var args = Array.prototype.slice.call(arguments, 3);

    var program = function(context, options) {
      options = options || {};

      return fn.apply(this, [context, options.data || data].concat(args));
    };
    program.program = i;
    program.depth = args.length;
    return program;
  },
  program: function(i, fn, data) {
    var program = function(context, options) {
      options = options || {};

      return fn(context, options.data || data);
    };
    program.program = i;
    program.depth = 0;
    return program;
  },
  noop: function() { return ""; },
  invokePartial: function(partial, name, context, helpers, partials, data) {
    var options = { helpers: helpers, partials: partials, data: data };

    if(partial === undefined) {
      throw new Handlebars.Exception("The partial " + name + " could not be found");
    } else if(partial instanceof Function) {
      return partial(context, options);
    } else if (!Handlebars.compile) {
      throw new Handlebars.Exception("The partial " + name + " could not be compiled when running in runtime-only mode");
    } else {
      partials[name] = Handlebars.compile(partial, {data: data !== undefined});
      return partials[name](context, options);
    }
  }
};

Handlebars.template = Handlebars.VM.template;

// END(BROWSER)

return Handlebars;

};

},{}],10:[function(require,module,exports){
exports.attach = function(Handlebars) {

var toString = Object.prototype.toString;

// BEGIN(BROWSER)

var errorProps = ['description', 'fileName', 'lineNumber', 'message', 'name', 'number', 'stack'];

Handlebars.Exception = function(message) {
  var tmp = Error.prototype.constructor.apply(this, arguments);

  // Unfortunately errors are not enumerable in Chrome (at least), so `for prop in tmp` doesn't work.
  for (var idx = 0; idx < errorProps.length; idx++) {
    this[errorProps[idx]] = tmp[errorProps[idx]];
  }
};
Handlebars.Exception.prototype = new Error();

// Build out our basic SafeString type
Handlebars.SafeString = function(string) {
  this.string = string;
};
Handlebars.SafeString.prototype.toString = function() {
  return this.string.toString();
};

var escape = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  '"': "&quot;",
  "'": "&#x27;",
  "`": "&#x60;"
};

var badChars = /[&<>"'`]/g;
var possible = /[&<>"'`]/;

var escapeChar = function(chr) {
  return escape[chr] || "&amp;";
};

Handlebars.Utils = {
  extend: function(obj, value) {
    for(var key in value) {
      if(value.hasOwnProperty(key)) {
        obj[key] = value[key];
      }
    }
  },

  escapeExpression: function(string) {
    // don't escape SafeStrings, since they're already safe
    if (string instanceof Handlebars.SafeString) {
      return string.toString();
    } else if (string == null || string === false) {
      return "";
    }

    // Force a string conversion as this will be done by the append regardless and
    // the regex test will do this transparently behind the scenes, causing issues if
    // an object's to string has escaped characters in it.
    string = string.toString();

    if(!possible.test(string)) { return string; }
    return string.replace(badChars, escapeChar);
  },

  isEmpty: function(value) {
    if (!value && value !== 0) {
      return true;
    } else if(toString.call(value) === "[object Array]" && value.length === 0) {
      return true;
    } else {
      return false;
    }
  }
};

// END(BROWSER)

return Handlebars;
};

},{}],9:[function(require,module,exports){
/*jshint eqnull: true */

module.exports.create = function() {

var Handlebars = {};

// BEGIN(BROWSER)

Handlebars.VERSION = "1.0.0";
Handlebars.COMPILER_REVISION = 4;

Handlebars.REVISION_CHANGES = {
  1: '<= 1.0.rc.2', // 1.0.rc.2 is actually rev2 but doesn't report it
  2: '== 1.0.0-rc.3',
  3: '== 1.0.0-rc.4',
  4: '>= 1.0.0'
};

Handlebars.helpers  = {};
Handlebars.partials = {};

var toString = Object.prototype.toString,
    functionType = '[object Function]',
    objectType = '[object Object]';

Handlebars.registerHelper = function(name, fn, inverse) {
  if (toString.call(name) === objectType) {
    if (inverse || fn) { throw new Handlebars.Exception('Arg not supported with multiple helpers'); }
    Handlebars.Utils.extend(this.helpers, name);
  } else {
    if (inverse) { fn.not = inverse; }
    this.helpers[name] = fn;
  }
};

Handlebars.registerPartial = function(name, str) {
  if (toString.call(name) === objectType) {
    Handlebars.Utils.extend(this.partials,  name);
  } else {
    this.partials[name] = str;
  }
};

Handlebars.registerHelper('helperMissing', function(arg) {
  if(arguments.length === 2) {
    return undefined;
  } else {
    throw new Error("Missing helper: '" + arg + "'");
  }
});

Handlebars.registerHelper('blockHelperMissing', function(context, options) {
  var inverse = options.inverse || function() {}, fn = options.fn;

  var type = toString.call(context);

  if(type === functionType) { context = context.call(this); }

  if(context === true) {
    return fn(this);
  } else if(context === false || context == null) {
    return inverse(this);
  } else if(type === "[object Array]") {
    if(context.length > 0) {
      return Handlebars.helpers.each(context, options);
    } else {
      return inverse(this);
    }
  } else {
    return fn(context);
  }
});

Handlebars.K = function() {};

Handlebars.createFrame = Object.create || function(object) {
  Handlebars.K.prototype = object;
  var obj = new Handlebars.K();
  Handlebars.K.prototype = null;
  return obj;
};

Handlebars.logger = {
  DEBUG: 0, INFO: 1, WARN: 2, ERROR: 3, level: 3,

  methodMap: {0: 'debug', 1: 'info', 2: 'warn', 3: 'error'},

  // can be overridden in the host environment
  log: function(level, obj) {
    if (Handlebars.logger.level <= level) {
      var method = Handlebars.logger.methodMap[level];
      if (typeof console !== 'undefined' && console[method]) {
        console[method].call(console, obj);
      }
    }
  }
};

Handlebars.log = function(level, obj) { Handlebars.logger.log(level, obj); };

Handlebars.registerHelper('each', function(context, options) {
  var fn = options.fn, inverse = options.inverse;
  var i = 0, ret = "", data;

  var type = toString.call(context);
  if(type === functionType) { context = context.call(this); }

  if (options.data) {
    data = Handlebars.createFrame(options.data);
  }

  if(context && typeof context === 'object') {
    if(context instanceof Array){
      for(var j = context.length; i<j; i++) {
        if (data) { data.index = i; }
        ret = ret + fn(context[i], { data: data });
      }
    } else {
      for(var key in context) {
        if(context.hasOwnProperty(key)) {
          if(data) { data.key = key; }
          ret = ret + fn(context[key], {data: data});
          i++;
        }
      }
    }
  }

  if(i === 0){
    ret = inverse(this);
  }

  return ret;
});

Handlebars.registerHelper('if', function(conditional, options) {
  var type = toString.call(conditional);
  if(type === functionType) { conditional = conditional.call(this); }

  if(!conditional || Handlebars.Utils.isEmpty(conditional)) {
    return options.inverse(this);
  } else {
    return options.fn(this);
  }
});

Handlebars.registerHelper('unless', function(conditional, options) {
  return Handlebars.helpers['if'].call(this, conditional, {fn: options.inverse, inverse: options.fn});
});

Handlebars.registerHelper('with', function(context, options) {
  var type = toString.call(context);
  if(type === functionType) { context = context.call(this); }

  if (!Handlebars.Utils.isEmpty(context)) return options.fn(context);
});

Handlebars.registerHelper('log', function(context, options) {
  var level = options.data && options.data.level != null ? parseInt(options.data.level, 10) : 1;
  Handlebars.log(level, context);
});

// END(BROWSER)

return Handlebars;
};

},{}],12:[function(require,module,exports){
// Each of these module will augment the Handlebars object as it loads. No need to perform addition operations
module.exports.attach = function(Handlebars) {

var visitor = require("./visitor"),
    printer = require("./printer"),
    ast = require("./ast"),
    compiler = require("./compiler");

visitor.attach(Handlebars);
printer.attach(Handlebars);
ast.attach(Handlebars);
compiler.attach(Handlebars);

return Handlebars;

};

},{"./visitor":13,"./printer":14,"./ast":15,"./compiler":16}],15:[function(require,module,exports){
exports.attach = function(Handlebars) {

// BEGIN(BROWSER)
Handlebars.AST = {};

Handlebars.AST.ProgramNode = function(statements, inverse) {
  this.type = "program";
  this.statements = statements;
  if(inverse) { this.inverse = new Handlebars.AST.ProgramNode(inverse); }
};

Handlebars.AST.MustacheNode = function(rawParams, hash, unescaped) {
  this.type = "mustache";
  this.escaped = !unescaped;
  this.hash = hash;

  var id = this.id = rawParams[0];
  var params = this.params = rawParams.slice(1);

  // a mustache is an eligible helper if:
  // * its id is simple (a single part, not `this` or `..`)
  var eligibleHelper = this.eligibleHelper = id.isSimple;

  // a mustache is definitely a helper if:
  // * it is an eligible helper, and
  // * it has at least one parameter or hash segment
  this.isHelper = eligibleHelper && (params.length || hash);

  // if a mustache is an eligible helper but not a definite
  // helper, it is ambiguous, and will be resolved in a later
  // pass or at runtime.
};

Handlebars.AST.PartialNode = function(partialName, context) {
  this.type         = "partial";
  this.partialName  = partialName;
  this.context      = context;
};

Handlebars.AST.BlockNode = function(mustache, program, inverse, close) {
  var verifyMatch = function(open, close) {
    if(open.original !== close.original) {
      throw new Handlebars.Exception(open.original + " doesn't match " + close.original);
    }
  };

  verifyMatch(mustache.id, close);
  this.type = "block";
  this.mustache = mustache;
  this.program  = program;
  this.inverse  = inverse;

  if (this.inverse && !this.program) {
    this.isInverse = true;
  }
};

Handlebars.AST.ContentNode = function(string) {
  this.type = "content";
  this.string = string;
};

Handlebars.AST.HashNode = function(pairs) {
  this.type = "hash";
  this.pairs = pairs;
};

Handlebars.AST.IdNode = function(parts) {
  this.type = "ID";

  var original = "",
      dig = [],
      depth = 0;

  for(var i=0,l=parts.length; i<l; i++) {
    var part = parts[i].part;
    original += (parts[i].separator || '') + part;

    if (part === ".." || part === "." || part === "this") {
      if (dig.length > 0) { throw new Handlebars.Exception("Invalid path: " + original); }
      else if (part === "..") { depth++; }
      else { this.isScoped = true; }
    }
    else { dig.push(part); }
  }

  this.original = original;
  this.parts    = dig;
  this.string   = dig.join('.');
  this.depth    = depth;

  // an ID is simple if it only has one part, and that part is not
  // `..` or `this`.
  this.isSimple = parts.length === 1 && !this.isScoped && depth === 0;

  this.stringModeValue = this.string;
};

Handlebars.AST.PartialNameNode = function(name) {
  this.type = "PARTIAL_NAME";
  this.name = name.original;
};

Handlebars.AST.DataNode = function(id) {
  this.type = "DATA";
  this.id = id;
};

Handlebars.AST.StringNode = function(string) {
  this.type = "STRING";
  this.original =
    this.string =
    this.stringModeValue = string;
};

Handlebars.AST.IntegerNode = function(integer) {
  this.type = "INTEGER";
  this.original =
    this.integer = integer;
  this.stringModeValue = Number(integer);
};

Handlebars.AST.BooleanNode = function(bool) {
  this.type = "BOOLEAN";
  this.bool = bool;
  this.stringModeValue = bool === "true";
};

Handlebars.AST.CommentNode = function(comment) {
  this.type = "comment";
  this.comment = comment;
};

// END(BROWSER)

return Handlebars;
};


},{}],14:[function(require,module,exports){
exports.attach = function(Handlebars) {

// BEGIN(BROWSER)

Handlebars.print = function(ast) {
  return new Handlebars.PrintVisitor().accept(ast);
};

Handlebars.PrintVisitor = function() { this.padding = 0; };
Handlebars.PrintVisitor.prototype = new Handlebars.Visitor();

Handlebars.PrintVisitor.prototype.pad = function(string, newline) {
  var out = "";

  for(var i=0,l=this.padding; i<l; i++) {
    out = out + "  ";
  }

  out = out + string;

  if(newline !== false) { out = out + "\n"; }
  return out;
};

Handlebars.PrintVisitor.prototype.program = function(program) {
  var out = "",
      statements = program.statements,
      inverse = program.inverse,
      i, l;

  for(i=0, l=statements.length; i<l; i++) {
    out = out + this.accept(statements[i]);
  }

  this.padding--;

  return out;
};

Handlebars.PrintVisitor.prototype.block = function(block) {
  var out = "";

  out = out + this.pad("BLOCK:");
  this.padding++;
  out = out + this.accept(block.mustache);
  if (block.program) {
    out = out + this.pad("PROGRAM:");
    this.padding++;
    out = out + this.accept(block.program);
    this.padding--;
  }
  if (block.inverse) {
    if (block.program) { this.padding++; }
    out = out + this.pad("{{^}}");
    this.padding++;
    out = out + this.accept(block.inverse);
    this.padding--;
    if (block.program) { this.padding--; }
  }
  this.padding--;

  return out;
};

Handlebars.PrintVisitor.prototype.mustache = function(mustache) {
  var params = mustache.params, paramStrings = [], hash;

  for(var i=0, l=params.length; i<l; i++) {
    paramStrings.push(this.accept(params[i]));
  }

  params = "[" + paramStrings.join(", ") + "]";

  hash = mustache.hash ? " " + this.accept(mustache.hash) : "";

  return this.pad("{{ " + this.accept(mustache.id) + " " + params + hash + " }}");
};

Handlebars.PrintVisitor.prototype.partial = function(partial) {
  var content = this.accept(partial.partialName);
  if(partial.context) { content = content + " " + this.accept(partial.context); }
  return this.pad("{{> " + content + " }}");
};

Handlebars.PrintVisitor.prototype.hash = function(hash) {
  var pairs = hash.pairs;
  var joinedPairs = [], left, right;

  for(var i=0, l=pairs.length; i<l; i++) {
    left = pairs[i][0];
    right = this.accept(pairs[i][1]);
    joinedPairs.push( left + "=" + right );
  }

  return "HASH{" + joinedPairs.join(", ") + "}";
};

Handlebars.PrintVisitor.prototype.STRING = function(string) {
  return '"' + string.string + '"';
};

Handlebars.PrintVisitor.prototype.INTEGER = function(integer) {
  return "INTEGER{" + integer.integer + "}";
};

Handlebars.PrintVisitor.prototype.BOOLEAN = function(bool) {
  return "BOOLEAN{" + bool.bool + "}";
};

Handlebars.PrintVisitor.prototype.ID = function(id) {
  var path = id.parts.join("/");
  if(id.parts.length > 1) {
    return "PATH:" + path;
  } else {
    return "ID:" + path;
  }
};

Handlebars.PrintVisitor.prototype.PARTIAL_NAME = function(partialName) {
    return "PARTIAL:" + partialName.name;
};

Handlebars.PrintVisitor.prototype.DATA = function(data) {
  return "@" + this.accept(data.id);
};

Handlebars.PrintVisitor.prototype.content = function(content) {
  return this.pad("CONTENT[ '" + content.string + "' ]");
};

Handlebars.PrintVisitor.prototype.comment = function(comment) {
  return this.pad("{{! '" + comment.comment + "' }}");
};
// END(BROWSER)

return Handlebars;
};


},{}],13:[function(require,module,exports){
exports.attach = function(Handlebars) {

// BEGIN(BROWSER)

Handlebars.Visitor = function() {};

Handlebars.Visitor.prototype = {
  accept: function(object) {
    return this[object.type](object);
  }
};

// END(BROWSER)

return Handlebars;
};



},{}],16:[function(require,module,exports){
var compilerbase = require("./base");

exports.attach = function(Handlebars) {

compilerbase.attach(Handlebars);

// BEGIN(BROWSER)

/*jshint eqnull:true*/
var Compiler = Handlebars.Compiler = function() {};
var JavaScriptCompiler = Handlebars.JavaScriptCompiler = function() {};

// the foundHelper register will disambiguate helper lookup from finding a
// function in a context. This is necessary for mustache compatibility, which
// requires that context functions in blocks are evaluated by blockHelperMissing,
// and then proceed as if the resulting value was provided to blockHelperMissing.

Compiler.prototype = {
  compiler: Compiler,

  disassemble: function() {
    var opcodes = this.opcodes, opcode, out = [], params, param;

    for (var i=0, l=opcodes.length; i<l; i++) {
      opcode = opcodes[i];

      if (opcode.opcode === 'DECLARE') {
        out.push("DECLARE " + opcode.name + "=" + opcode.value);
      } else {
        params = [];
        for (var j=0; j<opcode.args.length; j++) {
          param = opcode.args[j];
          if (typeof param === "string") {
            param = "\"" + param.replace("\n", "\\n") + "\"";
          }
          params.push(param);
        }
        out.push(opcode.opcode + " " + params.join(" "));
      }
    }

    return out.join("\n");
  },
  equals: function(other) {
    var len = this.opcodes.length;
    if (other.opcodes.length !== len) {
      return false;
    }

    for (var i = 0; i < len; i++) {
      var opcode = this.opcodes[i],
          otherOpcode = other.opcodes[i];
      if (opcode.opcode !== otherOpcode.opcode || opcode.args.length !== otherOpcode.args.length) {
        return false;
      }
      for (var j = 0; j < opcode.args.length; j++) {
        if (opcode.args[j] !== otherOpcode.args[j]) {
          return false;
        }
      }
    }

    len = this.children.length;
    if (other.children.length !== len) {
      return false;
    }
    for (i = 0; i < len; i++) {
      if (!this.children[i].equals(other.children[i])) {
        return false;
      }
    }

    return true;
  },

  guid: 0,

  compile: function(program, options) {
    this.children = [];
    this.depths = {list: []};
    this.options = options;

    // These changes will propagate to the other compiler components
    var knownHelpers = this.options.knownHelpers;
    this.options.knownHelpers = {
      'helperMissing': true,
      'blockHelperMissing': true,
      'each': true,
      'if': true,
      'unless': true,
      'with': true,
      'log': true
    };
    if (knownHelpers) {
      for (var name in knownHelpers) {
        this.options.knownHelpers[name] = knownHelpers[name];
      }
    }

    return this.program(program);
  },

  accept: function(node) {
    return this[node.type](node);
  },

  program: function(program) {
    var statements = program.statements, statement;
    this.opcodes = [];

    for(var i=0, l=statements.length; i<l; i++) {
      statement = statements[i];
      this[statement.type](statement);
    }
    this.isSimple = l === 1;

    this.depths.list = this.depths.list.sort(function(a, b) {
      return a - b;
    });

    return this;
  },

  compileProgram: function(program) {
    var result = new this.compiler().compile(program, this.options);
    var guid = this.guid++, depth;

    this.usePartial = this.usePartial || result.usePartial;

    this.children[guid] = result;

    for(var i=0, l=result.depths.list.length; i<l; i++) {
      depth = result.depths.list[i];

      if(depth < 2) { continue; }
      else { this.addDepth(depth - 1); }
    }

    return guid;
  },

  block: function(block) {
    var mustache = block.mustache,
        program = block.program,
        inverse = block.inverse;

    if (program) {
      program = this.compileProgram(program);
    }

    if (inverse) {
      inverse = this.compileProgram(inverse);
    }

    var type = this.classifyMustache(mustache);

    if (type === "helper") {
      this.helperMustache(mustache, program, inverse);
    } else if (type === "simple") {
      this.simpleMustache(mustache);

      // now that the simple mustache is resolved, we need to
      // evaluate it by executing `blockHelperMissing`
      this.opcode('pushProgram', program);
      this.opcode('pushProgram', inverse);
      this.opcode('emptyHash');
      this.opcode('blockValue');
    } else {
      this.ambiguousMustache(mustache, program, inverse);

      // now that the simple mustache is resolved, we need to
      // evaluate it by executing `blockHelperMissing`
      this.opcode('pushProgram', program);
      this.opcode('pushProgram', inverse);
      this.opcode('emptyHash');
      this.opcode('ambiguousBlockValue');
    }

    this.opcode('append');
  },

  hash: function(hash) {
    var pairs = hash.pairs, pair, val;

    this.opcode('pushHash');

    for(var i=0, l=pairs.length; i<l; i++) {
      pair = pairs[i];
      val  = pair[1];

      if (this.options.stringParams) {
        if(val.depth) {
          this.addDepth(val.depth);
        }
        this.opcode('getContext', val.depth || 0);
        this.opcode('pushStringParam', val.stringModeValue, val.type);
      } else {
        this.accept(val);
      }

      this.opcode('assignToHash', pair[0]);
    }
    this.opcode('popHash');
  },

  partial: function(partial) {
    var partialName = partial.partialName;
    this.usePartial = true;

    if(partial.context) {
      this.ID(partial.context);
    } else {
      this.opcode('push', 'depth0');
    }

    this.opcode('invokePartial', partialName.name);
    this.opcode('append');
  },

  content: function(content) {
    this.opcode('appendContent', content.string);
  },

  mustache: function(mustache) {
    var options = this.options;
    var type = this.classifyMustache(mustache);

    if (type === "simple") {
      this.simpleMustache(mustache);
    } else if (type === "helper") {
      this.helperMustache(mustache);
    } else {
      this.ambiguousMustache(mustache);
    }

    if(mustache.escaped && !options.noEscape) {
      this.opcode('appendEscaped');
    } else {
      this.opcode('append');
    }
  },

  ambiguousMustache: function(mustache, program, inverse) {
    var id = mustache.id,
        name = id.parts[0],
        isBlock = program != null || inverse != null;

    this.opcode('getContext', id.depth);

    this.opcode('pushProgram', program);
    this.opcode('pushProgram', inverse);

    this.opcode('invokeAmbiguous', name, isBlock);
  },

  simpleMustache: function(mustache) {
    var id = mustache.id;

    if (id.type === 'DATA') {
      this.DATA(id);
    } else if (id.parts.length) {
      this.ID(id);
    } else {
      // Simplified ID for `this`
      this.addDepth(id.depth);
      this.opcode('getContext', id.depth);
      this.opcode('pushContext');
    }

    this.opcode('resolvePossibleLambda');
  },

  helperMustache: function(mustache, program, inverse) {
    var params = this.setupFullMustacheParams(mustache, program, inverse),
        name = mustache.id.parts[0];

    if (this.options.knownHelpers[name]) {
      this.opcode('invokeKnownHelper', params.length, name);
    } else if (this.options.knownHelpersOnly) {
      throw new Error("You specified knownHelpersOnly, but used the unknown helper " + name);
    } else {
      this.opcode('invokeHelper', params.length, name);
    }
  },

  ID: function(id) {
    this.addDepth(id.depth);
    this.opcode('getContext', id.depth);

    var name = id.parts[0];
    if (!name) {
      this.opcode('pushContext');
    } else {
      this.opcode('lookupOnContext', id.parts[0]);
    }

    for(var i=1, l=id.parts.length; i<l; i++) {
      this.opcode('lookup', id.parts[i]);
    }
  },

  DATA: function(data) {
    this.options.data = true;
    if (data.id.isScoped || data.id.depth) {
      throw new Handlebars.Exception('Scoped data references are not supported: ' + data.original);
    }

    this.opcode('lookupData');
    var parts = data.id.parts;
    for(var i=0, l=parts.length; i<l; i++) {
      this.opcode('lookup', parts[i]);
    }
  },

  STRING: function(string) {
    this.opcode('pushString', string.string);
  },

  INTEGER: function(integer) {
    this.opcode('pushLiteral', integer.integer);
  },

  BOOLEAN: function(bool) {
    this.opcode('pushLiteral', bool.bool);
  },

  comment: function() {},

  // HELPERS
  opcode: function(name) {
    this.opcodes.push({ opcode: name, args: [].slice.call(arguments, 1) });
  },

  declare: function(name, value) {
    this.opcodes.push({ opcode: 'DECLARE', name: name, value: value });
  },

  addDepth: function(depth) {
    if(isNaN(depth)) { throw new Error("EWOT"); }
    if(depth === 0) { return; }

    if(!this.depths[depth]) {
      this.depths[depth] = true;
      this.depths.list.push(depth);
    }
  },

  classifyMustache: function(mustache) {
    var isHelper   = mustache.isHelper;
    var isEligible = mustache.eligibleHelper;
    var options    = this.options;

    // if ambiguous, we can possibly resolve the ambiguity now
    if (isEligible && !isHelper) {
      var name = mustache.id.parts[0];

      if (options.knownHelpers[name]) {
        isHelper = true;
      } else if (options.knownHelpersOnly) {
        isEligible = false;
      }
    }

    if (isHelper) { return "helper"; }
    else if (isEligible) { return "ambiguous"; }
    else { return "simple"; }
  },

  pushParams: function(params) {
    var i = params.length, param;

    while(i--) {
      param = params[i];

      if(this.options.stringParams) {
        if(param.depth) {
          this.addDepth(param.depth);
        }

        this.opcode('getContext', param.depth || 0);
        this.opcode('pushStringParam', param.stringModeValue, param.type);
      } else {
        this[param.type](param);
      }
    }
  },

  setupMustacheParams: function(mustache) {
    var params = mustache.params;
    this.pushParams(params);

    if(mustache.hash) {
      this.hash(mustache.hash);
    } else {
      this.opcode('emptyHash');
    }

    return params;
  },

  // this will replace setupMustacheParams when we're done
  setupFullMustacheParams: function(mustache, program, inverse) {
    var params = mustache.params;
    this.pushParams(params);

    this.opcode('pushProgram', program);
    this.opcode('pushProgram', inverse);

    if(mustache.hash) {
      this.hash(mustache.hash);
    } else {
      this.opcode('emptyHash');
    }

    return params;
  }
};

var Literal = function(value) {
  this.value = value;
};

JavaScriptCompiler.prototype = {
  // PUBLIC API: You can override these methods in a subclass to provide
  // alternative compiled forms for name lookup and buffering semantics
  nameLookup: function(parent, name /* , type*/) {
    if (/^[0-9]+$/.test(name)) {
      return parent + "[" + name + "]";
    } else if (JavaScriptCompiler.isValidJavaScriptVariableName(name)) {
      return parent + "." + name;
    }
    else {
      return parent + "['" + name + "']";
    }
  },

  appendToBuffer: function(string) {
    if (this.environment.isSimple) {
      return "return " + string + ";";
    } else {
      return {
        appendToBuffer: true,
        content: string,
        toString: function() { return "buffer += " + string + ";"; }
      };
    }
  },

  initializeBuffer: function() {
    return this.quotedString("");
  },

  namespace: "Handlebars",
  // END PUBLIC API

  compile: function(environment, options, context, asObject) {
    this.environment = environment;
    this.options = options || {};

    Handlebars.log(Handlebars.logger.DEBUG, this.environment.disassemble() + "\n\n");

    this.name = this.environment.name;
    this.isChild = !!context;
    this.context = context || {
      programs: [],
      environments: [],
      aliases: { }
    };

    this.preamble();

    this.stackSlot = 0;
    this.stackVars = [];
    this.registers = { list: [] };
    this.compileStack = [];
    this.inlineStack = [];

    this.compileChildren(environment, options);

    var opcodes = environment.opcodes, opcode;

    this.i = 0;

    for(l=opcodes.length; this.i<l; this.i++) {
      opcode = opcodes[this.i];

      if(opcode.opcode === 'DECLARE') {
        this[opcode.name] = opcode.value;
      } else {
        this[opcode.opcode].apply(this, opcode.args);
      }
    }

    return this.createFunctionContext(asObject);
  },

  nextOpcode: function() {
    var opcodes = this.environment.opcodes;
    return opcodes[this.i + 1];
  },

  eat: function() {
    this.i = this.i + 1;
  },

  preamble: function() {
    var out = [];

    if (!this.isChild) {
      var namespace = this.namespace;

      var copies = "helpers = this.merge(helpers, " + namespace + ".helpers);";
      if (this.environment.usePartial) { copies = copies + " partials = this.merge(partials, " + namespace + ".partials);"; }
      if (this.options.data) { copies = copies + " data = data || {};"; }
      out.push(copies);
    } else {
      out.push('');
    }

    if (!this.environment.isSimple) {
      out.push(", buffer = " + this.initializeBuffer());
    } else {
      out.push("");
    }

    // track the last context pushed into place to allow skipping the
    // getContext opcode when it would be a noop
    this.lastContext = 0;
    this.source = out;
  },

  createFunctionContext: function(asObject) {
    var locals = this.stackVars.concat(this.registers.list);

    if(locals.length > 0) {
      this.source[1] = this.source[1] + ", " + locals.join(", ");
    }

    // Generate minimizer alias mappings
    if (!this.isChild) {
      for (var alias in this.context.aliases) {
        if (this.context.aliases.hasOwnProperty(alias)) {
          this.source[1] = this.source[1] + ', ' + alias + '=' + this.context.aliases[alias];
        }
      }
    }

    if (this.source[1]) {
      this.source[1] = "var " + this.source[1].substring(2) + ";";
    }

    // Merge children
    if (!this.isChild) {
      this.source[1] += '\n' + this.context.programs.join('\n') + '\n';
    }

    if (!this.environment.isSimple) {
      this.source.push("return buffer;");
    }

    var params = this.isChild ? ["depth0", "data"] : ["Handlebars", "depth0", "helpers", "partials", "data"];

    for(var i=0, l=this.environment.depths.list.length; i<l; i++) {
      params.push("depth" + this.environment.depths.list[i]);
    }

    // Perform a second pass over the output to merge content when possible
    var source = this.mergeSource();

    if (!this.isChild) {
      var revision = Handlebars.COMPILER_REVISION,
          versions = Handlebars.REVISION_CHANGES[revision];
      source = "this.compilerInfo = ["+revision+",'"+versions+"'];\n"+source;
    }

    if (asObject) {
      params.push(source);

      return Function.apply(this, params);
    } else {
      var functionSource = 'function ' + (this.name || '') + '(' + params.join(',') + ') {\n  ' + source + '}';
      Handlebars.log(Handlebars.logger.DEBUG, functionSource + "\n\n");
      return functionSource;
    }
  },
  mergeSource: function() {
    // WARN: We are not handling the case where buffer is still populated as the source should
    // not have buffer append operations as their final action.
    var source = '',
        buffer;
    for (var i = 0, len = this.source.length; i < len; i++) {
      var line = this.source[i];
      if (line.appendToBuffer) {
        if (buffer) {
          buffer = buffer + '\n    + ' + line.content;
        } else {
          buffer = line.content;
        }
      } else {
        if (buffer) {
          source += 'buffer += ' + buffer + ';\n  ';
          buffer = undefined;
        }
        source += line + '\n  ';
      }
    }
    return source;
  },

  // [blockValue]
  //
  // On stack, before: hash, inverse, program, value
  // On stack, after: return value of blockHelperMissing
  //
  // The purpose of this opcode is to take a block of the form
  // `{{#foo}}...{{/foo}}`, resolve the value of `foo`, and
  // replace it on the stack with the result of properly
  // invoking blockHelperMissing.
  blockValue: function() {
    this.context.aliases.blockHelperMissing = 'helpers.blockHelperMissing';

    var params = ["depth0"];
    this.setupParams(0, params);

    this.replaceStack(function(current) {
      params.splice(1, 0, current);
      return "blockHelperMissing.call(" + params.join(", ") + ")";
    });
  },

  // [ambiguousBlockValue]
  //
  // On stack, before: hash, inverse, program, value
  // Compiler value, before: lastHelper=value of last found helper, if any
  // On stack, after, if no lastHelper: same as [blockValue]
  // On stack, after, if lastHelper: value
  ambiguousBlockValue: function() {
    this.context.aliases.blockHelperMissing = 'helpers.blockHelperMissing';

    var params = ["depth0"];
    this.setupParams(0, params);

    var current = this.topStack();
    params.splice(1, 0, current);

    // Use the options value generated from the invocation
    params[params.length-1] = 'options';

    this.source.push("if (!" + this.lastHelper + ") { " + current + " = blockHelperMissing.call(" + params.join(", ") + "); }");
  },

  // [appendContent]
  //
  // On stack, before: ...
  // On stack, after: ...
  //
  // Appends the string value of `content` to the current buffer
  appendContent: function(content) {
    this.source.push(this.appendToBuffer(this.quotedString(content)));
  },

  // [append]
  //
  // On stack, before: value, ...
  // On stack, after: ...
  //
  // Coerces `value` to a String and appends it to the current buffer.
  //
  // If `value` is truthy, or 0, it is coerced into a string and appended
  // Otherwise, the empty string is appended
  append: function() {
    // Force anything that is inlined onto the stack so we don't have duplication
    // when we examine local
    this.flushInline();
    var local = this.popStack();
    this.source.push("if(" + local + " || " + local + " === 0) { " + this.appendToBuffer(local) + " }");
    if (this.environment.isSimple) {
      this.source.push("else { " + this.appendToBuffer("''") + " }");
    }
  },

  // [appendEscaped]
  //
  // On stack, before: value, ...
  // On stack, after: ...
  //
  // Escape `value` and append it to the buffer
  appendEscaped: function() {
    this.context.aliases.escapeExpression = 'this.escapeExpression';

    this.source.push(this.appendToBuffer("escapeExpression(" + this.popStack() + ")"));
  },

  // [getContext]
  //
  // On stack, before: ...
  // On stack, after: ...
  // Compiler value, after: lastContext=depth
  //
  // Set the value of the `lastContext` compiler value to the depth
  getContext: function(depth) {
    if(this.lastContext !== depth) {
      this.lastContext = depth;
    }
  },

  // [lookupOnContext]
  //
  // On stack, before: ...
  // On stack, after: currentContext[name], ...
  //
  // Looks up the value of `name` on the current context and pushes
  // it onto the stack.
  lookupOnContext: function(name) {
    this.push(this.nameLookup('depth' + this.lastContext, name, 'context'));
  },

  // [pushContext]
  //
  // On stack, before: ...
  // On stack, after: currentContext, ...
  //
  // Pushes the value of the current context onto the stack.
  pushContext: function() {
    this.pushStackLiteral('depth' + this.lastContext);
  },

  // [resolvePossibleLambda]
  //
  // On stack, before: value, ...
  // On stack, after: resolved value, ...
  //
  // If the `value` is a lambda, replace it on the stack by
  // the return value of the lambda
  resolvePossibleLambda: function() {
    this.context.aliases.functionType = '"function"';

    this.replaceStack(function(current) {
      return "typeof " + current + " === functionType ? " + current + ".apply(depth0) : " + current;
    });
  },

  // [lookup]
  //
  // On stack, before: value, ...
  // On stack, after: value[name], ...
  //
  // Replace the value on the stack with the result of looking
  // up `name` on `value`
  lookup: function(name) {
    this.replaceStack(function(current) {
      return current + " == null || " + current + " === false ? " + current + " : " + this.nameLookup(current, name, 'context');
    });
  },

  // [lookupData]
  //
  // On stack, before: ...
  // On stack, after: data[id], ...
  //
  // Push the result of looking up `id` on the current data
  lookupData: function(id) {
    this.push('data');
  },

  // [pushStringParam]
  //
  // On stack, before: ...
  // On stack, after: string, currentContext, ...
  //
  // This opcode is designed for use in string mode, which
  // provides the string value of a parameter along with its
  // depth rather than resolving it immediately.
  pushStringParam: function(string, type) {
    this.pushStackLiteral('depth' + this.lastContext);

    this.pushString(type);

    if (typeof string === 'string') {
      this.pushString(string);
    } else {
      this.pushStackLiteral(string);
    }
  },

  emptyHash: function() {
    this.pushStackLiteral('{}');

    if (this.options.stringParams) {
      this.register('hashTypes', '{}');
      this.register('hashContexts', '{}');
    }
  },
  pushHash: function() {
    this.hash = {values: [], types: [], contexts: []};
  },
  popHash: function() {
    var hash = this.hash;
    this.hash = undefined;

    if (this.options.stringParams) {
      this.register('hashContexts', '{' + hash.contexts.join(',') + '}');
      this.register('hashTypes', '{' + hash.types.join(',') + '}');
    }
    this.push('{\n    ' + hash.values.join(',\n    ') + '\n  }');
  },

  // [pushString]
  //
  // On stack, before: ...
  // On stack, after: quotedString(string), ...
  //
  // Push a quoted version of `string` onto the stack
  pushString: function(string) {
    this.pushStackLiteral(this.quotedString(string));
  },

  // [push]
  //
  // On stack, before: ...
  // On stack, after: expr, ...
  //
  // Push an expression onto the stack
  push: function(expr) {
    this.inlineStack.push(expr);
    return expr;
  },

  // [pushLiteral]
  //
  // On stack, before: ...
  // On stack, after: value, ...
  //
  // Pushes a value onto the stack. This operation prevents
  // the compiler from creating a temporary variable to hold
  // it.
  pushLiteral: function(value) {
    this.pushStackLiteral(value);
  },

  // [pushProgram]
  //
  // On stack, before: ...
  // On stack, after: program(guid), ...
  //
  // Push a program expression onto the stack. This takes
  // a compile-time guid and converts it into a runtime-accessible
  // expression.
  pushProgram: function(guid) {
    if (guid != null) {
      this.pushStackLiteral(this.programExpression(guid));
    } else {
      this.pushStackLiteral(null);
    }
  },

  // [invokeHelper]
  //
  // On stack, before: hash, inverse, program, params..., ...
  // On stack, after: result of helper invocation
  //
  // Pops off the helper's parameters, invokes the helper,
  // and pushes the helper's return value onto the stack.
  //
  // If the helper is not found, `helperMissing` is called.
  invokeHelper: function(paramSize, name) {
    this.context.aliases.helperMissing = 'helpers.helperMissing';

    var helper = this.lastHelper = this.setupHelper(paramSize, name, true);
    var nonHelper = this.nameLookup('depth' + this.lastContext, name, 'context');

    this.push(helper.name + ' || ' + nonHelper);
    this.replaceStack(function(name) {
      return name + ' ? ' + name + '.call(' +
          helper.callParams + ") " + ": helperMissing.call(" +
          helper.helperMissingParams + ")";
    });
  },

  // [invokeKnownHelper]
  //
  // On stack, before: hash, inverse, program, params..., ...
  // On stack, after: result of helper invocation
  //
  // This operation is used when the helper is known to exist,
  // so a `helperMissing` fallback is not required.
  invokeKnownHelper: function(paramSize, name) {
    var helper = this.setupHelper(paramSize, name);
    this.push(helper.name + ".call(" + helper.callParams + ")");
  },

  // [invokeAmbiguous]
  //
  // On stack, before: hash, inverse, program, params..., ...
  // On stack, after: result of disambiguation
  //
  // This operation is used when an expression like `{{foo}}`
  // is provided, but we don't know at compile-time whether it
  // is a helper or a path.
  //
  // This operation emits more code than the other options,
  // and can be avoided by passing the `knownHelpers` and
  // `knownHelpersOnly` flags at compile-time.
  invokeAmbiguous: function(name, helperCall) {
    this.context.aliases.functionType = '"function"';

    this.pushStackLiteral('{}');    // Hash value
    var helper = this.setupHelper(0, name, helperCall);

    var helperName = this.lastHelper = this.nameLookup('helpers', name, 'helper');

    var nonHelper = this.nameLookup('depth' + this.lastContext, name, 'context');
    var nextStack = this.nextStack();

    this.source.push('if (' + nextStack + ' = ' + helperName + ') { ' + nextStack + ' = ' + nextStack + '.call(' + helper.callParams + '); }');
    this.source.push('else { ' + nextStack + ' = ' + nonHelper + '; ' + nextStack + ' = typeof ' + nextStack + ' === functionType ? ' + nextStack + '.apply(depth0) : ' + nextStack + '; }');
  },

  // [invokePartial]
  //
  // On stack, before: context, ...
  // On stack after: result of partial invocation
  //
  // This operation pops off a context, invokes a partial with that context,
  // and pushes the result of the invocation back.
  invokePartial: function(name) {
    var params = [this.nameLookup('partials', name, 'partial'), "'" + name + "'", this.popStack(), "helpers", "partials"];

    if (this.options.data) {
      params.push("data");
    }

    this.context.aliases.self = "this";
    this.push("self.invokePartial(" + params.join(", ") + ")");
  },

  // [assignToHash]
  //
  // On stack, before: value, hash, ...
  // On stack, after: hash, ...
  //
  // Pops a value and hash off the stack, assigns `hash[key] = value`
  // and pushes the hash back onto the stack.
  assignToHash: function(key) {
    var value = this.popStack(),
        context,
        type;

    if (this.options.stringParams) {
      type = this.popStack();
      context = this.popStack();
    }

    var hash = this.hash;
    if (context) {
      hash.contexts.push("'" + key + "': " + context);
    }
    if (type) {
      hash.types.push("'" + key + "': " + type);
    }
    hash.values.push("'" + key + "': (" + value + ")");
  },

  // HELPERS

  compiler: JavaScriptCompiler,

  compileChildren: function(environment, options) {
    var children = environment.children, child, compiler;

    for(var i=0, l=children.length; i<l; i++) {
      child = children[i];
      compiler = new this.compiler();

      var index = this.matchExistingProgram(child);

      if (index == null) {
        this.context.programs.push('');     // Placeholder to prevent name conflicts for nested children
        index = this.context.programs.length;
        child.index = index;
        child.name = 'program' + index;
        this.context.programs[index] = compiler.compile(child, options, this.context);
        this.context.environments[index] = child;
      } else {
        child.index = index;
        child.name = 'program' + index;
      }
    }
  },
  matchExistingProgram: function(child) {
    for (var i = 0, len = this.context.environments.length; i < len; i++) {
      var environment = this.context.environments[i];
      if (environment && environment.equals(child)) {
        return i;
      }
    }
  },

  programExpression: function(guid) {
    this.context.aliases.self = "this";

    if(guid == null) {
      return "self.noop";
    }

    var child = this.environment.children[guid],
        depths = child.depths.list, depth;

    var programParams = [child.index, child.name, "data"];

    for(var i=0, l = depths.length; i<l; i++) {
      depth = depths[i];

      if(depth === 1) { programParams.push("depth0"); }
      else { programParams.push("depth" + (depth - 1)); }
    }

    return (depths.length === 0 ? "self.program(" : "self.programWithDepth(") + programParams.join(", ") + ")";
  },

  register: function(name, val) {
    this.useRegister(name);
    this.source.push(name + " = " + val + ";");
  },

  useRegister: function(name) {
    if(!this.registers[name]) {
      this.registers[name] = true;
      this.registers.list.push(name);
    }
  },

  pushStackLiteral: function(item) {
    return this.push(new Literal(item));
  },

  pushStack: function(item) {
    this.flushInline();

    var stack = this.incrStack();
    if (item) {
      this.source.push(stack + " = " + item + ";");
    }
    this.compileStack.push(stack);
    return stack;
  },

  replaceStack: function(callback) {
    var prefix = '',
        inline = this.isInline(),
        stack;

    // If we are currently inline then we want to merge the inline statement into the
    // replacement statement via ','
    if (inline) {
      var top = this.popStack(true);

      if (top instanceof Literal) {
        // Literals do not need to be inlined
        stack = top.value;
      } else {
        // Get or create the current stack name for use by the inline
        var name = this.stackSlot ? this.topStackName() : this.incrStack();

        prefix = '(' + this.push(name) + ' = ' + top + '),';
        stack = this.topStack();
      }
    } else {
      stack = this.topStack();
    }

    var item = callback.call(this, stack);

    if (inline) {
      if (this.inlineStack.length || this.compileStack.length) {
        this.popStack();
      }
      this.push('(' + prefix + item + ')');
    } else {
      // Prevent modification of the context depth variable. Through replaceStack
      if (!/^stack/.test(stack)) {
        stack = this.nextStack();
      }

      this.source.push(stack + " = (" + prefix + item + ");");
    }
    return stack;
  },

  nextStack: function() {
    return this.pushStack();
  },

  incrStack: function() {
    this.stackSlot++;
    if(this.stackSlot > this.stackVars.length) { this.stackVars.push("stack" + this.stackSlot); }
    return this.topStackName();
  },
  topStackName: function() {
    return "stack" + this.stackSlot;
  },
  flushInline: function() {
    var inlineStack = this.inlineStack;
    if (inlineStack.length) {
      this.inlineStack = [];
      for (var i = 0, len = inlineStack.length; i < len; i++) {
        var entry = inlineStack[i];
        if (entry instanceof Literal) {
          this.compileStack.push(entry);
        } else {
          this.pushStack(entry);
        }
      }
    }
  },
  isInline: function() {
    return this.inlineStack.length;
  },

  popStack: function(wrapped) {
    var inline = this.isInline(),
        item = (inline ? this.inlineStack : this.compileStack).pop();

    if (!wrapped && (item instanceof Literal)) {
      return item.value;
    } else {
      if (!inline) {
        this.stackSlot--;
      }
      return item;
    }
  },

  topStack: function(wrapped) {
    var stack = (this.isInline() ? this.inlineStack : this.compileStack),
        item = stack[stack.length - 1];

    if (!wrapped && (item instanceof Literal)) {
      return item.value;
    } else {
      return item;
    }
  },

  quotedString: function(str) {
    return '"' + str
      .replace(/\\/g, '\\\\')
      .replace(/"/g, '\\"')
      .replace(/\n/g, '\\n')
      .replace(/\r/g, '\\r')
      .replace(/\u2028/g, '\\u2028')   // Per Ecma-262 7.3 + 7.8.4
      .replace(/\u2029/g, '\\u2029') + '"';
  },

  setupHelper: function(paramSize, name, missingParams) {
    var params = [];
    this.setupParams(paramSize, params, missingParams);
    var foundHelper = this.nameLookup('helpers', name, 'helper');

    return {
      params: params,
      name: foundHelper,
      callParams: ["depth0"].concat(params).join(", "),
      helperMissingParams: missingParams && ["depth0", this.quotedString(name)].concat(params).join(", ")
    };
  },

  // the params and contexts arguments are passed in arrays
  // to fill in
  setupParams: function(paramSize, params, useRegister) {
    var options = [], contexts = [], types = [], param, inverse, program;

    options.push("hash:" + this.popStack());

    inverse = this.popStack();
    program = this.popStack();

    // Avoid setting fn and inverse if neither are set. This allows
    // helpers to do a check for `if (options.fn)`
    if (program || inverse) {
      if (!program) {
        this.context.aliases.self = "this";
        program = "self.noop";
      }

      if (!inverse) {
       this.context.aliases.self = "this";
        inverse = "self.noop";
      }

      options.push("inverse:" + inverse);
      options.push("fn:" + program);
    }

    for(var i=0; i<paramSize; i++) {
      param = this.popStack();
      params.push(param);

      if(this.options.stringParams) {
        types.push(this.popStack());
        contexts.push(this.popStack());
      }
    }

    if (this.options.stringParams) {
      options.push("contexts:[" + contexts.join(",") + "]");
      options.push("types:[" + types.join(",") + "]");
      options.push("hashContexts:hashContexts");
      options.push("hashTypes:hashTypes");
    }

    if(this.options.data) {
      options.push("data:data");
    }

    options = "{" + options.join(",") + "}";
    if (useRegister) {
      this.register('options', options);
      params.push('options');
    } else {
      params.push(options);
    }
    return params.join(", ");
  }
};

var reservedWords = (
  "break else new var" +
  " case finally return void" +
  " catch for switch while" +
  " continue function this with" +
  " default if throw" +
  " delete in try" +
  " do instanceof typeof" +
  " abstract enum int short" +
  " boolean export interface static" +
  " byte extends long super" +
  " char final native synchronized" +
  " class float package throws" +
  " const goto private transient" +
  " debugger implements protected volatile" +
  " double import public let yield"
).split(" ");

var compilerWords = JavaScriptCompiler.RESERVED_WORDS = {};

for(var i=0, l=reservedWords.length; i<l; i++) {
  compilerWords[reservedWords[i]] = true;
}

JavaScriptCompiler.isValidJavaScriptVariableName = function(name) {
  if(!JavaScriptCompiler.RESERVED_WORDS[name] && /^[a-zA-Z_$][0-9a-zA-Z_$]+$/.test(name)) {
    return true;
  }
  return false;
};

Handlebars.precompile = function(input, options) {
  if (input == null || (typeof input !== 'string' && input.constructor !== Handlebars.AST.ProgramNode)) {
    throw new Handlebars.Exception("You must pass a string or Handlebars AST to Handlebars.precompile. You passed " + input);
  }

  options = options || {};
  if (!('data' in options)) {
    options.data = true;
  }
  var ast = Handlebars.parse(input);
  var environment = new Compiler().compile(ast, options);
  return new JavaScriptCompiler().compile(environment, options);
};

Handlebars.compile = function(input, options) {
  if (input == null || (typeof input !== 'string' && input.constructor !== Handlebars.AST.ProgramNode)) {
    throw new Handlebars.Exception("You must pass a string or Handlebars AST to Handlebars.compile. You passed " + input);
  }

  options = options || {};
  if (!('data' in options)) {
    options.data = true;
  }
  var compiled;
  function compile() {
    var ast = Handlebars.parse(input);
    var environment = new Compiler().compile(ast, options);
    var templateSpec = new JavaScriptCompiler().compile(environment, options, undefined, true);
    return Handlebars.template(templateSpec);
  }

  // Template is only compiled on first use and cached after that point.
  return function(context, options) {
    if (!compiled) {
      compiled = compile();
    }
    return compiled.call(this, context, options);
  };
};


// END(BROWSER)

return Handlebars;

};



},{"./base":17}],17:[function(require,module,exports){
var handlebars = require("./parser");

exports.attach = function(Handlebars) {

// BEGIN(BROWSER)

Handlebars.Parser = handlebars;

Handlebars.parse = function(input) {

  // Just return if an already-compile AST was passed in.
  if(input.constructor === Handlebars.AST.ProgramNode) { return input; }

  Handlebars.Parser.yy = Handlebars.AST;
  return Handlebars.Parser.parse(input);
};

// END(BROWSER)

return Handlebars;
};

},{"./parser":18}],18:[function(require,module,exports){
// BEGIN(BROWSER)
/* Jison generated parser */
var handlebars = (function(){
var parser = {trace: function trace() { },
yy: {},
symbols_: {"error":2,"root":3,"program":4,"EOF":5,"simpleInverse":6,"statements":7,"statement":8,"openInverse":9,"closeBlock":10,"openBlock":11,"mustache":12,"partial":13,"CONTENT":14,"COMMENT":15,"OPEN_BLOCK":16,"inMustache":17,"CLOSE":18,"OPEN_INVERSE":19,"OPEN_ENDBLOCK":20,"path":21,"OPEN":22,"OPEN_UNESCAPED":23,"CLOSE_UNESCAPED":24,"OPEN_PARTIAL":25,"partialName":26,"params":27,"hash":28,"dataName":29,"param":30,"STRING":31,"INTEGER":32,"BOOLEAN":33,"hashSegments":34,"hashSegment":35,"ID":36,"EQUALS":37,"DATA":38,"pathSegments":39,"SEP":40,"$accept":0,"$end":1},
terminals_: {2:"error",5:"EOF",14:"CONTENT",15:"COMMENT",16:"OPEN_BLOCK",18:"CLOSE",19:"OPEN_INVERSE",20:"OPEN_ENDBLOCK",22:"OPEN",23:"OPEN_UNESCAPED",24:"CLOSE_UNESCAPED",25:"OPEN_PARTIAL",31:"STRING",32:"INTEGER",33:"BOOLEAN",36:"ID",37:"EQUALS",38:"DATA",40:"SEP"},
productions_: [0,[3,2],[4,2],[4,3],[4,2],[4,1],[4,1],[4,0],[7,1],[7,2],[8,3],[8,3],[8,1],[8,1],[8,1],[8,1],[11,3],[9,3],[10,3],[12,3],[12,3],[13,3],[13,4],[6,2],[17,3],[17,2],[17,2],[17,1],[17,1],[27,2],[27,1],[30,1],[30,1],[30,1],[30,1],[30,1],[28,1],[34,2],[34,1],[35,3],[35,3],[35,3],[35,3],[35,3],[26,1],[26,1],[26,1],[29,2],[21,1],[39,3],[39,1]],
performAction: function anonymous(yytext,yyleng,yylineno,yy,yystate,$$,_$) {

var $0 = $$.length - 1;
switch (yystate) {
case 1: return $$[$0-1]; 
break;
case 2: this.$ = new yy.ProgramNode([], $$[$0]); 
break;
case 3: this.$ = new yy.ProgramNode($$[$0-2], $$[$0]); 
break;
case 4: this.$ = new yy.ProgramNode($$[$0-1], []); 
break;
case 5: this.$ = new yy.ProgramNode($$[$0]); 
break;
case 6: this.$ = new yy.ProgramNode([], []); 
break;
case 7: this.$ = new yy.ProgramNode([]); 
break;
case 8: this.$ = [$$[$0]]; 
break;
case 9: $$[$0-1].push($$[$0]); this.$ = $$[$0-1]; 
break;
case 10: this.$ = new yy.BlockNode($$[$0-2], $$[$0-1].inverse, $$[$0-1], $$[$0]); 
break;
case 11: this.$ = new yy.BlockNode($$[$0-2], $$[$0-1], $$[$0-1].inverse, $$[$0]); 
break;
case 12: this.$ = $$[$0]; 
break;
case 13: this.$ = $$[$0]; 
break;
case 14: this.$ = new yy.ContentNode($$[$0]); 
break;
case 15: this.$ = new yy.CommentNode($$[$0]); 
break;
case 16: this.$ = new yy.MustacheNode($$[$0-1][0], $$[$0-1][1]); 
break;
case 17: this.$ = new yy.MustacheNode($$[$0-1][0], $$[$0-1][1]); 
break;
case 18: this.$ = $$[$0-1]; 
break;
case 19:
    // Parsing out the '&' escape token at this level saves ~500 bytes after min due to the removal of one parser node.
    this.$ = new yy.MustacheNode($$[$0-1][0], $$[$0-1][1], $$[$0-2][2] === '&');
  
break;
case 20: this.$ = new yy.MustacheNode($$[$0-1][0], $$[$0-1][1], true); 
break;
case 21: this.$ = new yy.PartialNode($$[$0-1]); 
break;
case 22: this.$ = new yy.PartialNode($$[$0-2], $$[$0-1]); 
break;
case 23: 
break;
case 24: this.$ = [[$$[$0-2]].concat($$[$0-1]), $$[$0]]; 
break;
case 25: this.$ = [[$$[$0-1]].concat($$[$0]), null]; 
break;
case 26: this.$ = [[$$[$0-1]], $$[$0]]; 
break;
case 27: this.$ = [[$$[$0]], null]; 
break;
case 28: this.$ = [[$$[$0]], null]; 
break;
case 29: $$[$0-1].push($$[$0]); this.$ = $$[$0-1]; 
break;
case 30: this.$ = [$$[$0]]; 
break;
case 31: this.$ = $$[$0]; 
break;
case 32: this.$ = new yy.StringNode($$[$0]); 
break;
case 33: this.$ = new yy.IntegerNode($$[$0]); 
break;
case 34: this.$ = new yy.BooleanNode($$[$0]); 
break;
case 35: this.$ = $$[$0]; 
break;
case 36: this.$ = new yy.HashNode($$[$0]); 
break;
case 37: $$[$0-1].push($$[$0]); this.$ = $$[$0-1]; 
break;
case 38: this.$ = [$$[$0]]; 
break;
case 39: this.$ = [$$[$0-2], $$[$0]]; 
break;
case 40: this.$ = [$$[$0-2], new yy.StringNode($$[$0])]; 
break;
case 41: this.$ = [$$[$0-2], new yy.IntegerNode($$[$0])]; 
break;
case 42: this.$ = [$$[$0-2], new yy.BooleanNode($$[$0])]; 
break;
case 43: this.$ = [$$[$0-2], $$[$0]]; 
break;
case 44: this.$ = new yy.PartialNameNode($$[$0]); 
break;
case 45: this.$ = new yy.PartialNameNode(new yy.StringNode($$[$0])); 
break;
case 46: this.$ = new yy.PartialNameNode(new yy.IntegerNode($$[$0])); 
break;
case 47: this.$ = new yy.DataNode($$[$0]); 
break;
case 48: this.$ = new yy.IdNode($$[$0]); 
break;
case 49: $$[$0-2].push({part: $$[$0], separator: $$[$0-1]}); this.$ = $$[$0-2]; 
break;
case 50: this.$ = [{part: $$[$0]}]; 
break;
}
},
table: [{3:1,4:2,5:[2,7],6:3,7:4,8:6,9:7,11:8,12:9,13:10,14:[1,11],15:[1,12],16:[1,13],19:[1,5],22:[1,14],23:[1,15],25:[1,16]},{1:[3]},{5:[1,17]},{5:[2,6],7:18,8:6,9:7,11:8,12:9,13:10,14:[1,11],15:[1,12],16:[1,13],19:[1,19],20:[2,6],22:[1,14],23:[1,15],25:[1,16]},{5:[2,5],6:20,8:21,9:7,11:8,12:9,13:10,14:[1,11],15:[1,12],16:[1,13],19:[1,5],20:[2,5],22:[1,14],23:[1,15],25:[1,16]},{17:23,18:[1,22],21:24,29:25,36:[1,28],38:[1,27],39:26},{5:[2,8],14:[2,8],15:[2,8],16:[2,8],19:[2,8],20:[2,8],22:[2,8],23:[2,8],25:[2,8]},{4:29,6:3,7:4,8:6,9:7,11:8,12:9,13:10,14:[1,11],15:[1,12],16:[1,13],19:[1,5],20:[2,7],22:[1,14],23:[1,15],25:[1,16]},{4:30,6:3,7:4,8:6,9:7,11:8,12:9,13:10,14:[1,11],15:[1,12],16:[1,13],19:[1,5],20:[2,7],22:[1,14],23:[1,15],25:[1,16]},{5:[2,12],14:[2,12],15:[2,12],16:[2,12],19:[2,12],20:[2,12],22:[2,12],23:[2,12],25:[2,12]},{5:[2,13],14:[2,13],15:[2,13],16:[2,13],19:[2,13],20:[2,13],22:[2,13],23:[2,13],25:[2,13]},{5:[2,14],14:[2,14],15:[2,14],16:[2,14],19:[2,14],20:[2,14],22:[2,14],23:[2,14],25:[2,14]},{5:[2,15],14:[2,15],15:[2,15],16:[2,15],19:[2,15],20:[2,15],22:[2,15],23:[2,15],25:[2,15]},{17:31,21:24,29:25,36:[1,28],38:[1,27],39:26},{17:32,21:24,29:25,36:[1,28],38:[1,27],39:26},{17:33,21:24,29:25,36:[1,28],38:[1,27],39:26},{21:35,26:34,31:[1,36],32:[1,37],36:[1,28],39:26},{1:[2,1]},{5:[2,2],8:21,9:7,11:8,12:9,13:10,14:[1,11],15:[1,12],16:[1,13],19:[1,19],20:[2,2],22:[1,14],23:[1,15],25:[1,16]},{17:23,21:24,29:25,36:[1,28],38:[1,27],39:26},{5:[2,4],7:38,8:6,9:7,11:8,12:9,13:10,14:[1,11],15:[1,12],16:[1,13],19:[1,19],20:[2,4],22:[1,14],23:[1,15],25:[1,16]},{5:[2,9],14:[2,9],15:[2,9],16:[2,9],19:[2,9],20:[2,9],22:[2,9],23:[2,9],25:[2,9]},{5:[2,23],14:[2,23],15:[2,23],16:[2,23],19:[2,23],20:[2,23],22:[2,23],23:[2,23],25:[2,23]},{18:[1,39]},{18:[2,27],21:44,24:[2,27],27:40,28:41,29:48,30:42,31:[1,45],32:[1,46],33:[1,47],34:43,35:49,36:[1,50],38:[1,27],39:26},{18:[2,28],24:[2,28]},{18:[2,48],24:[2,48],31:[2,48],32:[2,48],33:[2,48],36:[2,48],38:[2,48],40:[1,51]},{21:52,36:[1,28],39:26},{18:[2,50],24:[2,50],31:[2,50],32:[2,50],33:[2,50],36:[2,50],38:[2,50],40:[2,50]},{10:53,20:[1,54]},{10:55,20:[1,54]},{18:[1,56]},{18:[1,57]},{24:[1,58]},{18:[1,59],21:60,36:[1,28],39:26},{18:[2,44],36:[2,44]},{18:[2,45],36:[2,45]},{18:[2,46],36:[2,46]},{5:[2,3],8:21,9:7,11:8,12:9,13:10,14:[1,11],15:[1,12],16:[1,13],19:[1,19],20:[2,3],22:[1,14],23:[1,15],25:[1,16]},{14:[2,17],15:[2,17],16:[2,17],19:[2,17],20:[2,17],22:[2,17],23:[2,17],25:[2,17]},{18:[2,25],21:44,24:[2,25],28:61,29:48,30:62,31:[1,45],32:[1,46],33:[1,47],34:43,35:49,36:[1,50],38:[1,27],39:26},{18:[2,26],24:[2,26]},{18:[2,30],24:[2,30],31:[2,30],32:[2,30],33:[2,30],36:[2,30],38:[2,30]},{18:[2,36],24:[2,36],35:63,36:[1,64]},{18:[2,31],24:[2,31],31:[2,31],32:[2,31],33:[2,31],36:[2,31],38:[2,31]},{18:[2,32],24:[2,32],31:[2,32],32:[2,32],33:[2,32],36:[2,32],38:[2,32]},{18:[2,33],24:[2,33],31:[2,33],32:[2,33],33:[2,33],36:[2,33],38:[2,33]},{18:[2,34],24:[2,34],31:[2,34],32:[2,34],33:[2,34],36:[2,34],38:[2,34]},{18:[2,35],24:[2,35],31:[2,35],32:[2,35],33:[2,35],36:[2,35],38:[2,35]},{18:[2,38],24:[2,38],36:[2,38]},{18:[2,50],24:[2,50],31:[2,50],32:[2,50],33:[2,50],36:[2,50],37:[1,65],38:[2,50],40:[2,50]},{36:[1,66]},{18:[2,47],24:[2,47],31:[2,47],32:[2,47],33:[2,47],36:[2,47],38:[2,47]},{5:[2,10],14:[2,10],15:[2,10],16:[2,10],19:[2,10],20:[2,10],22:[2,10],23:[2,10],25:[2,10]},{21:67,36:[1,28],39:26},{5:[2,11],14:[2,11],15:[2,11],16:[2,11],19:[2,11],20:[2,11],22:[2,11],23:[2,11],25:[2,11]},{14:[2,16],15:[2,16],16:[2,16],19:[2,16],20:[2,16],22:[2,16],23:[2,16],25:[2,16]},{5:[2,19],14:[2,19],15:[2,19],16:[2,19],19:[2,19],20:[2,19],22:[2,19],23:[2,19],25:[2,19]},{5:[2,20],14:[2,20],15:[2,20],16:[2,20],19:[2,20],20:[2,20],22:[2,20],23:[2,20],25:[2,20]},{5:[2,21],14:[2,21],15:[2,21],16:[2,21],19:[2,21],20:[2,21],22:[2,21],23:[2,21],25:[2,21]},{18:[1,68]},{18:[2,24],24:[2,24]},{18:[2,29],24:[2,29],31:[2,29],32:[2,29],33:[2,29],36:[2,29],38:[2,29]},{18:[2,37],24:[2,37],36:[2,37]},{37:[1,65]},{21:69,29:73,31:[1,70],32:[1,71],33:[1,72],36:[1,28],38:[1,27],39:26},{18:[2,49],24:[2,49],31:[2,49],32:[2,49],33:[2,49],36:[2,49],38:[2,49],40:[2,49]},{18:[1,74]},{5:[2,22],14:[2,22],15:[2,22],16:[2,22],19:[2,22],20:[2,22],22:[2,22],23:[2,22],25:[2,22]},{18:[2,39],24:[2,39],36:[2,39]},{18:[2,40],24:[2,40],36:[2,40]},{18:[2,41],24:[2,41],36:[2,41]},{18:[2,42],24:[2,42],36:[2,42]},{18:[2,43],24:[2,43],36:[2,43]},{5:[2,18],14:[2,18],15:[2,18],16:[2,18],19:[2,18],20:[2,18],22:[2,18],23:[2,18],25:[2,18]}],
defaultActions: {17:[2,1]},
parseError: function parseError(str, hash) {
    throw new Error(str);
},
parse: function parse(input) {
    var self = this, stack = [0], vstack = [null], lstack = [], table = this.table, yytext = "", yylineno = 0, yyleng = 0, recovering = 0, TERROR = 2, EOF = 1;
    this.lexer.setInput(input);
    this.lexer.yy = this.yy;
    this.yy.lexer = this.lexer;
    this.yy.parser = this;
    if (typeof this.lexer.yylloc == "undefined")
        this.lexer.yylloc = {};
    var yyloc = this.lexer.yylloc;
    lstack.push(yyloc);
    var ranges = this.lexer.options && this.lexer.options.ranges;
    if (typeof this.yy.parseError === "function")
        this.parseError = this.yy.parseError;
    function popStack(n) {
        stack.length = stack.length - 2 * n;
        vstack.length = vstack.length - n;
        lstack.length = lstack.length - n;
    }
    function lex() {
        var token;
        token = self.lexer.lex() || 1;
        if (typeof token !== "number") {
            token = self.symbols_[token] || token;
        }
        return token;
    }
    var symbol, preErrorSymbol, state, action, a, r, yyval = {}, p, len, newState, expected;
    while (true) {
        state = stack[stack.length - 1];
        if (this.defaultActions[state]) {
            action = this.defaultActions[state];
        } else {
            if (symbol === null || typeof symbol == "undefined") {
                symbol = lex();
            }
            action = table[state] && table[state][symbol];
        }
        if (typeof action === "undefined" || !action.length || !action[0]) {
            var errStr = "";
            if (!recovering) {
                expected = [];
                for (p in table[state])
                    if (this.terminals_[p] && p > 2) {
                        expected.push("'" + this.terminals_[p] + "'");
                    }
                if (this.lexer.showPosition) {
                    errStr = "Parse error on line " + (yylineno + 1) + ":\n" + this.lexer.showPosition() + "\nExpecting " + expected.join(", ") + ", got '" + (this.terminals_[symbol] || symbol) + "'";
                } else {
                    errStr = "Parse error on line " + (yylineno + 1) + ": Unexpected " + (symbol == 1?"end of input":"'" + (this.terminals_[symbol] || symbol) + "'");
                }
                this.parseError(errStr, {text: this.lexer.match, token: this.terminals_[symbol] || symbol, line: this.lexer.yylineno, loc: yyloc, expected: expected});
            }
        }
        if (action[0] instanceof Array && action.length > 1) {
            throw new Error("Parse Error: multiple actions possible at state: " + state + ", token: " + symbol);
        }
        switch (action[0]) {
        case 1:
            stack.push(symbol);
            vstack.push(this.lexer.yytext);
            lstack.push(this.lexer.yylloc);
            stack.push(action[1]);
            symbol = null;
            if (!preErrorSymbol) {
                yyleng = this.lexer.yyleng;
                yytext = this.lexer.yytext;
                yylineno = this.lexer.yylineno;
                yyloc = this.lexer.yylloc;
                if (recovering > 0)
                    recovering--;
            } else {
                symbol = preErrorSymbol;
                preErrorSymbol = null;
            }
            break;
        case 2:
            len = this.productions_[action[1]][1];
            yyval.$ = vstack[vstack.length - len];
            yyval._$ = {first_line: lstack[lstack.length - (len || 1)].first_line, last_line: lstack[lstack.length - 1].last_line, first_column: lstack[lstack.length - (len || 1)].first_column, last_column: lstack[lstack.length - 1].last_column};
            if (ranges) {
                yyval._$.range = [lstack[lstack.length - (len || 1)].range[0], lstack[lstack.length - 1].range[1]];
            }
            r = this.performAction.call(yyval, yytext, yyleng, yylineno, this.yy, action[1], vstack, lstack);
            if (typeof r !== "undefined") {
                return r;
            }
            if (len) {
                stack = stack.slice(0, -1 * len * 2);
                vstack = vstack.slice(0, -1 * len);
                lstack = lstack.slice(0, -1 * len);
            }
            stack.push(this.productions_[action[1]][0]);
            vstack.push(yyval.$);
            lstack.push(yyval._$);
            newState = table[stack[stack.length - 2]][stack[stack.length - 1]];
            stack.push(newState);
            break;
        case 3:
            return true;
        }
    }
    return true;
}
};
/* Jison generated lexer */
var lexer = (function(){
var lexer = ({EOF:1,
parseError:function parseError(str, hash) {
        if (this.yy.parser) {
            this.yy.parser.parseError(str, hash);
        } else {
            throw new Error(str);
        }
    },
setInput:function (input) {
        this._input = input;
        this._more = this._less = this.done = false;
        this.yylineno = this.yyleng = 0;
        this.yytext = this.matched = this.match = '';
        this.conditionStack = ['INITIAL'];
        this.yylloc = {first_line:1,first_column:0,last_line:1,last_column:0};
        if (this.options.ranges) this.yylloc.range = [0,0];
        this.offset = 0;
        return this;
    },
input:function () {
        var ch = this._input[0];
        this.yytext += ch;
        this.yyleng++;
        this.offset++;
        this.match += ch;
        this.matched += ch;
        var lines = ch.match(/(?:\r\n?|\n).*/g);
        if (lines) {
            this.yylineno++;
            this.yylloc.last_line++;
        } else {
            this.yylloc.last_column++;
        }
        if (this.options.ranges) this.yylloc.range[1]++;

        this._input = this._input.slice(1);
        return ch;
    },
unput:function (ch) {
        var len = ch.length;
        var lines = ch.split(/(?:\r\n?|\n)/g);

        this._input = ch + this._input;
        this.yytext = this.yytext.substr(0, this.yytext.length-len-1);
        //this.yyleng -= len;
        this.offset -= len;
        var oldLines = this.match.split(/(?:\r\n?|\n)/g);
        this.match = this.match.substr(0, this.match.length-1);
        this.matched = this.matched.substr(0, this.matched.length-1);

        if (lines.length-1) this.yylineno -= lines.length-1;
        var r = this.yylloc.range;

        this.yylloc = {first_line: this.yylloc.first_line,
          last_line: this.yylineno+1,
          first_column: this.yylloc.first_column,
          last_column: lines ?
              (lines.length === oldLines.length ? this.yylloc.first_column : 0) + oldLines[oldLines.length - lines.length].length - lines[0].length:
              this.yylloc.first_column - len
          };

        if (this.options.ranges) {
            this.yylloc.range = [r[0], r[0] + this.yyleng - len];
        }
        return this;
    },
more:function () {
        this._more = true;
        return this;
    },
less:function (n) {
        this.unput(this.match.slice(n));
    },
pastInput:function () {
        var past = this.matched.substr(0, this.matched.length - this.match.length);
        return (past.length > 20 ? '...':'') + past.substr(-20).replace(/\n/g, "");
    },
upcomingInput:function () {
        var next = this.match;
        if (next.length < 20) {
            next += this._input.substr(0, 20-next.length);
        }
        return (next.substr(0,20)+(next.length > 20 ? '...':'')).replace(/\n/g, "");
    },
showPosition:function () {
        var pre = this.pastInput();
        var c = new Array(pre.length + 1).join("-");
        return pre + this.upcomingInput() + "\n" + c+"^";
    },
next:function () {
        if (this.done) {
            return this.EOF;
        }
        if (!this._input) this.done = true;

        var token,
            match,
            tempMatch,
            index,
            col,
            lines;
        if (!this._more) {
            this.yytext = '';
            this.match = '';
        }
        var rules = this._currentRules();
        for (var i=0;i < rules.length; i++) {
            tempMatch = this._input.match(this.rules[rules[i]]);
            if (tempMatch && (!match || tempMatch[0].length > match[0].length)) {
                match = tempMatch;
                index = i;
                if (!this.options.flex) break;
            }
        }
        if (match) {
            lines = match[0].match(/(?:\r\n?|\n).*/g);
            if (lines) this.yylineno += lines.length;
            this.yylloc = {first_line: this.yylloc.last_line,
                           last_line: this.yylineno+1,
                           first_column: this.yylloc.last_column,
                           last_column: lines ? lines[lines.length-1].length-lines[lines.length-1].match(/\r?\n?/)[0].length : this.yylloc.last_column + match[0].length};
            this.yytext += match[0];
            this.match += match[0];
            this.matches = match;
            this.yyleng = this.yytext.length;
            if (this.options.ranges) {
                this.yylloc.range = [this.offset, this.offset += this.yyleng];
            }
            this._more = false;
            this._input = this._input.slice(match[0].length);
            this.matched += match[0];
            token = this.performAction.call(this, this.yy, this, rules[index],this.conditionStack[this.conditionStack.length-1]);
            if (this.done && this._input) this.done = false;
            if (token) return token;
            else return;
        }
        if (this._input === "") {
            return this.EOF;
        } else {
            return this.parseError('Lexical error on line '+(this.yylineno+1)+'. Unrecognized text.\n'+this.showPosition(),
                    {text: "", token: null, line: this.yylineno});
        }
    },
lex:function lex() {
        var r = this.next();
        if (typeof r !== 'undefined') {
            return r;
        } else {
            return this.lex();
        }
    },
begin:function begin(condition) {
        this.conditionStack.push(condition);
    },
popState:function popState() {
        return this.conditionStack.pop();
    },
_currentRules:function _currentRules() {
        return this.conditions[this.conditionStack[this.conditionStack.length-1]].rules;
    },
topState:function () {
        return this.conditionStack[this.conditionStack.length-2];
    },
pushState:function begin(condition) {
        this.begin(condition);
    }});
lexer.options = {};
lexer.performAction = function anonymous(yy,yy_,$avoiding_name_collisions,YY_START) {

var YYSTATE=YY_START
switch($avoiding_name_collisions) {
case 0: yy_.yytext = "\\"; return 14; 
break;
case 1:
                                   if(yy_.yytext.slice(-1) !== "\\") this.begin("mu");
                                   if(yy_.yytext.slice(-1) === "\\") yy_.yytext = yy_.yytext.substr(0,yy_.yyleng-1), this.begin("emu");
                                   if(yy_.yytext) return 14;
                                 
break;
case 2: return 14; 
break;
case 3:
                                   if(yy_.yytext.slice(-1) !== "\\") this.popState();
                                   if(yy_.yytext.slice(-1) === "\\") yy_.yytext = yy_.yytext.substr(0,yy_.yyleng-1);
                                   return 14;
                                 
break;
case 4: yy_.yytext = yy_.yytext.substr(0, yy_.yyleng-4); this.popState(); return 15; 
break;
case 5: return 25; 
break;
case 6: return 16; 
break;
case 7: return 20; 
break;
case 8: return 19; 
break;
case 9: return 19; 
break;
case 10: return 23; 
break;
case 11: return 22; 
break;
case 12: this.popState(); this.begin('com'); 
break;
case 13: yy_.yytext = yy_.yytext.substr(3,yy_.yyleng-5); this.popState(); return 15; 
break;
case 14: return 22; 
break;
case 15: return 37; 
break;
case 16: return 36; 
break;
case 17: return 36; 
break;
case 18: return 40; 
break;
case 19: /*ignore whitespace*/ 
break;
case 20: this.popState(); return 24; 
break;
case 21: this.popState(); return 18; 
break;
case 22: yy_.yytext = yy_.yytext.substr(1,yy_.yyleng-2).replace(/\\"/g,'"'); return 31; 
break;
case 23: yy_.yytext = yy_.yytext.substr(1,yy_.yyleng-2).replace(/\\'/g,"'"); return 31; 
break;
case 24: return 38; 
break;
case 25: return 33; 
break;
case 26: return 33; 
break;
case 27: return 32; 
break;
case 28: return 36; 
break;
case 29: yy_.yytext = yy_.yytext.substr(1, yy_.yyleng-2); return 36; 
break;
case 30: return 'INVALID'; 
break;
case 31: return 5; 
break;
}
};
lexer.rules = [/^(?:\\\\(?=(\{\{)))/,/^(?:[^\x00]*?(?=(\{\{)))/,/^(?:[^\x00]+)/,/^(?:[^\x00]{2,}?(?=(\{\{|$)))/,/^(?:[\s\S]*?--\}\})/,/^(?:\{\{>)/,/^(?:\{\{#)/,/^(?:\{\{\/)/,/^(?:\{\{\^)/,/^(?:\{\{\s*else\b)/,/^(?:\{\{\{)/,/^(?:\{\{&)/,/^(?:\{\{!--)/,/^(?:\{\{![\s\S]*?\}\})/,/^(?:\{\{)/,/^(?:=)/,/^(?:\.(?=[}\/ ]))/,/^(?:\.\.)/,/^(?:[\/.])/,/^(?:\s+)/,/^(?:\}\}\})/,/^(?:\}\})/,/^(?:"(\\["]|[^"])*")/,/^(?:'(\\[']|[^'])*')/,/^(?:@)/,/^(?:true(?=[}\s]))/,/^(?:false(?=[}\s]))/,/^(?:-?[0-9]+(?=[}\s]))/,/^(?:[^\s!"#%-,\.\/;->@\[-\^`\{-~]+(?=[=}\s\/.]))/,/^(?:\[[^\]]*\])/,/^(?:.)/,/^(?:$)/];
lexer.conditions = {"mu":{"rules":[5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],"inclusive":false},"emu":{"rules":[3],"inclusive":false},"com":{"rules":[4],"inclusive":false},"INITIAL":{"rules":[0,1,2,31],"inclusive":true}};
return lexer;})()
parser.lexer = lexer;
function Parser () { this.yy = {}; }Parser.prototype = parser;parser.Parser = Parser;
return new Parser;
})();
// END(BROWSER)

module.exports = handlebars;

},{}]},{},[6])(6)
});
;;
(function(e){if("function"==typeof bootstrap)bootstrap("istanbulsiestalcovreport",e);else if("object"==typeof exports)module.exports=e();else if("function"==typeof define&&define.amd)define(e);else if("undefined"!=typeof ses){if(!ses.ok())return;ses.makeIstanbulSiestaLcovReport=e}else"undefined"!=typeof window?window.IstanbulSiestaLcovReport=e():global.IstanbulSiestaLcovReport=e()})(function(){var define,ses,bootstrap,module,exports;
return (function(e,t,n){function i(n,s){if(!t[n]){if(!e[n]){var o=typeof require=="function"&&require;if(!s&&o)return o(n,!0);if(r)return r(n,!0);throw new Error("Cannot find module '"+n+"'")}var u=t[n]={exports:{}};e[n][0].call(u.exports,function(t){var r=e[n][1][t];return i(r?r:t)},u,u.exports)}return t[n].exports}var r=typeof require=="function"&&require;for(var s=0;s<n.length;s++)i(n[s]);return i})({1:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};

process.nextTick = (function () {
    var canSetImmediate = typeof window !== 'undefined'
    && window.setImmediate;
    var canPost = typeof window !== 'undefined'
    && window.postMessage && window.addEventListener
    ;

    if (canSetImmediate) {
        return function (f) { return window.setImmediate(f) };
    }

    if (canPost) {
        var queue = [];
        window.addEventListener('message', function (ev) {
            if (ev.source === window && ev.data === 'process-tick') {
                ev.stopPropagation();
                if (queue.length > 0) {
                    var fn = queue.shift();
                    fn();
                }
            }
        }, true);

        return function nextTick(fn) {
            queue.push(fn);
            window.postMessage('process-tick', '*');
        };
    }

    return function nextTick(fn) {
        setTimeout(fn, 0);
    };
})();

process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];

process.binding = function (name) {
    throw new Error('process.binding is not supported');
}

// TODO(shtylman)
process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};

},{}],2:[function(require,module,exports){
(function(process){/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

var path = require('path'),
    Writer = require('../util/file-writer'),
    util = require('util'),
    Report = require('./index'),
    utils = require('../object-utils');
/**
 * a `Report` implementation that produces an LCOV coverage file from coverage objects.
 *
 * Usage
 * -----
 *
 *      var report = require('istanbul').Report.create('lcovonly');
 *
 *
 * @class LcovOnlyReport
 * @extends Report
 * @constructor
 * @param {Object} opts optional
 * @param {String} [opts.dir] the directory in which to the `lcov.info` file. Defaults to `process.cwd()`
 */
function LcovOnlyReport(opts) {
    this.opts = opts || {};
    this.opts.dir = this.opts.dir || process.cwd();
    this.opts.writer = this.opts.writer || null;
}
LcovOnlyReport.TYPE = 'lcovonly';
util.inherits(LcovOnlyReport, Report);

Report.mix(LcovOnlyReport, {
    writeFileCoverage: function (writer, fc) {
        var functions = fc.f,
            functionMap = fc.fnMap,
            lines = fc.l,
            branches = fc.b,
            branchMap = fc.branchMap,
            summary = utils.summarizeFileCoverage(fc);

        writer.println('TN:'); //no test name
        writer.println('SF:' + fc.path);

        Object.keys(functions).forEach(function (key) {
            var meta = functionMap[key];
            writer.println('FN:' + [ meta.line, meta.name ].join(','));
        });
        writer.println('FNF:' + summary.functions.total);
        writer.println('FNH:' + summary.functions.covered);

        Object.keys(functions).forEach(function (key) {
            var stats = functions[key],
                meta = functionMap[key];
            writer.println('FNDA:' + [ stats, meta.name ].join(','));
        });

        Object.keys(lines).forEach(function (key) {
            var stat = lines[key];
            writer.println('DA:' + [ key, stat ].join(','));
        });
        writer.println('LF:' + summary.lines.total);
        writer.println('LH:' + summary.lines.covered);

        Object.keys(branches).forEach(function (key) {
            var branchArray = branches[key],
                meta = branchMap[key],
                line = meta.line,
                i = 0;
            branchArray.forEach(function (b) {
                writer.println('BRDA:' + [line, key, i, b].join(','));
                i += 1;
            });
        });
        writer.println('BRF:' + summary.branches.total);
        writer.println('BRH:' + summary.branches.covered);
        writer.println('end_of_record');
    },

    writeReport: function (collector, sync) {
        var outputFile = path.resolve(this.opts.dir, 'lcov.info'),
            writer = this.opts.writer || new Writer(sync),
            that = this;
        writer.writeFile(outputFile, function (contentWriter) {
            collector.files().forEach(function (key) {
                that.writeFileCoverage(contentWriter, collector.fileCoverageFor(key));
            });
        });
    }
});

module.exports = LcovOnlyReport;
})(require("__browserify_process"))
},{"path":3,"util":4,"../util/file-writer":5,"./index":6,"../object-utils":7,"__browserify_process":1}],3:[function(require,module,exports){
(function(process){function filter (xs, fn) {
    var res = [];
    for (var i = 0; i < xs.length; i++) {
        if (fn(xs[i], i, xs)) res.push(xs[i]);
    }
    return res;
}

// resolves . and .. elements in a path array with directory names there
// must be no slashes, empty elements, or device names (c:\) in the array
// (so also no leading and trailing slashes - it does not distinguish
// relative and absolute paths)
function normalizeArray(parts, allowAboveRoot) {
  // if the path tries to go above the root, `up` ends up > 0
  var up = 0;
  for (var i = parts.length; i >= 0; i--) {
    var last = parts[i];
    if (last == '.') {
      parts.splice(i, 1);
    } else if (last === '..') {
      parts.splice(i, 1);
      up++;
    } else if (up) {
      parts.splice(i, 1);
      up--;
    }
  }

  // if the path is allowed to go above the root, restore leading ..s
  if (allowAboveRoot) {
    for (; up--; up) {
      parts.unshift('..');
    }
  }

  return parts;
}

// Regex to split a filename into [*, dir, basename, ext]
// posix version
var splitPathRe = /^(.+\/(?!$)|\/)?((?:.+?)?(\.[^.]*)?)$/;

// path.resolve([from ...], to)
// posix version
exports.resolve = function() {
var resolvedPath = '',
    resolvedAbsolute = false;

for (var i = arguments.length; i >= -1 && !resolvedAbsolute; i--) {
  var path = (i >= 0)
      ? arguments[i]
      : process.cwd();

  // Skip empty and invalid entries
  if (typeof path !== 'string' || !path) {
    continue;
  }

  resolvedPath = path + '/' + resolvedPath;
  resolvedAbsolute = path.charAt(0) === '/';
}

// At this point the path should be resolved to a full absolute path, but
// handle relative paths to be safe (might happen when process.cwd() fails)

// Normalize the path
resolvedPath = normalizeArray(filter(resolvedPath.split('/'), function(p) {
    return !!p;
  }), !resolvedAbsolute).join('/');

  return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
};

// path.normalize(path)
// posix version
exports.normalize = function(path) {
var isAbsolute = path.charAt(0) === '/',
    trailingSlash = path.slice(-1) === '/';

// Normalize the path
path = normalizeArray(filter(path.split('/'), function(p) {
    return !!p;
  }), !isAbsolute).join('/');

  if (!path && !isAbsolute) {
    path = '.';
  }
  if (path && trailingSlash) {
    path += '/';
  }
  
  return (isAbsolute ? '/' : '') + path;
};


// posix version
exports.join = function() {
  var paths = Array.prototype.slice.call(arguments, 0);
  return exports.normalize(filter(paths, function(p, index) {
    return p && typeof p === 'string';
  }).join('/'));
};


exports.dirname = function(path) {
  var dir = splitPathRe.exec(path)[1] || '';
  var isWindows = false;
  if (!dir) {
    // No dirname
    return '.';
  } else if (dir.length === 1 ||
      (isWindows && dir.length <= 3 && dir.charAt(1) === ':')) {
    // It is just a slash or a drive letter with a slash
    return dir;
  } else {
    // It is a full dirname, strip trailing slash
    return dir.substring(0, dir.length - 1);
  }
};


exports.basename = function(path, ext) {
  var f = splitPathRe.exec(path)[2] || '';
  // TODO: make this comparison case-insensitive on windows?
  if (ext && f.substr(-1 * ext.length) === ext) {
    f = f.substr(0, f.length - ext.length);
  }
  return f;
};


exports.extname = function(path) {
  return splitPathRe.exec(path)[3] || '';
};

exports.relative = function(from, to) {
  from = exports.resolve(from).substr(1);
  to = exports.resolve(to).substr(1);

  function trim(arr) {
    var start = 0;
    for (; start < arr.length; start++) {
      if (arr[start] !== '') break;
    }

    var end = arr.length - 1;
    for (; end >= 0; end--) {
      if (arr[end] !== '') break;
    }

    if (start > end) return [];
    return arr.slice(start, end - start + 1);
  }

  var fromParts = trim(from.split('/'));
  var toParts = trim(to.split('/'));

  var length = Math.min(fromParts.length, toParts.length);
  var samePartsLength = length;
  for (var i = 0; i < length; i++) {
    if (fromParts[i] !== toParts[i]) {
      samePartsLength = i;
      break;
    }
  }

  var outputParts = [];
  for (var i = samePartsLength; i < fromParts.length; i++) {
    outputParts.push('..');
  }

  outputParts = outputParts.concat(toParts.slice(samePartsLength));

  return outputParts.join('/');
};

})(require("__browserify_process"))
},{"__browserify_process":1}],4:[function(require,module,exports){
var events = require('events');

exports.isArray = isArray;
exports.isDate = function(obj){return Object.prototype.toString.call(obj) === '[object Date]'};
exports.isRegExp = function(obj){return Object.prototype.toString.call(obj) === '[object RegExp]'};


exports.print = function () {};
exports.puts = function () {};
exports.debug = function() {};

exports.inspect = function(obj, showHidden, depth, colors) {
  var seen = [];

  var stylize = function(str, styleType) {
    // http://en.wikipedia.org/wiki/ANSI_escape_code#graphics
    var styles =
        { 'bold' : [1, 22],
          'italic' : [3, 23],
          'underline' : [4, 24],
          'inverse' : [7, 27],
          'white' : [37, 39],
          'grey' : [90, 39],
          'black' : [30, 39],
          'blue' : [34, 39],
          'cyan' : [36, 39],
          'green' : [32, 39],
          'magenta' : [35, 39],
          'red' : [31, 39],
          'yellow' : [33, 39] };

    var style =
        { 'special': 'cyan',
          'number': 'blue',
          'boolean': 'yellow',
          'undefined': 'grey',
          'null': 'bold',
          'string': 'green',
          'date': 'magenta',
          // "name": intentionally not styling
          'regexp': 'red' }[styleType];

    if (style) {
      return '\033[' + styles[style][0] + 'm' + str +
             '\033[' + styles[style][1] + 'm';
    } else {
      return str;
    }
  };
  if (! colors) {
    stylize = function(str, styleType) { return str; };
  }

  function format(value, recurseTimes) {
    // Provide a hook for user-specified inspect functions.
    // Check that value is an object with an inspect function on it
    if (value && typeof value.inspect === 'function' &&
        // Filter out the util module, it's inspect function is special
        value !== exports &&
        // Also filter out any prototype objects using the circular check.
        !(value.constructor && value.constructor.prototype === value)) {
      return value.inspect(recurseTimes);
    }

    // Primitive types cannot have properties
    switch (typeof value) {
      case 'undefined':
        return stylize('undefined', 'undefined');

      case 'string':
        var simple = '\'' + JSON.stringify(value).replace(/^"|"$/g, '')
                                                 .replace(/'/g, "\\'")
                                                 .replace(/\\"/g, '"') + '\'';
        return stylize(simple, 'string');

      case 'number':
        return stylize('' + value, 'number');

      case 'boolean':
        return stylize('' + value, 'boolean');
    }
    // For some reason typeof null is "object", so special case here.
    if (value === null) {
      return stylize('null', 'null');
    }

    // Look up the keys of the object.
    var visible_keys = Object_keys(value);
    var keys = showHidden ? Object_getOwnPropertyNames(value) : visible_keys;

    // Functions without properties can be shortcutted.
    if (typeof value === 'function' && keys.length === 0) {
      if (isRegExp(value)) {
        return stylize('' + value, 'regexp');
      } else {
        var name = value.name ? ': ' + value.name : '';
        return stylize('[Function' + name + ']', 'special');
      }
    }

    // Dates without properties can be shortcutted
    if (isDate(value) && keys.length === 0) {
      return stylize(value.toUTCString(), 'date');
    }

    var base, type, braces;
    // Determine the object type
    if (isArray(value)) {
      type = 'Array';
      braces = ['[', ']'];
    } else {
      type = 'Object';
      braces = ['{', '}'];
    }

    // Make functions say that they are functions
    if (typeof value === 'function') {
      var n = value.name ? ': ' + value.name : '';
      base = (isRegExp(value)) ? ' ' + value : ' [Function' + n + ']';
    } else {
      base = '';
    }

    // Make dates with properties first say the date
    if (isDate(value)) {
      base = ' ' + value.toUTCString();
    }

    if (keys.length === 0) {
      return braces[0] + base + braces[1];
    }

    if (recurseTimes < 0) {
      if (isRegExp(value)) {
        return stylize('' + value, 'regexp');
      } else {
        return stylize('[Object]', 'special');
      }
    }

    seen.push(value);

    var output = keys.map(function(key) {
      var name, str;
      if (value.__lookupGetter__) {
        if (value.__lookupGetter__(key)) {
          if (value.__lookupSetter__(key)) {
            str = stylize('[Getter/Setter]', 'special');
          } else {
            str = stylize('[Getter]', 'special');
          }
        } else {
          if (value.__lookupSetter__(key)) {
            str = stylize('[Setter]', 'special');
          }
        }
      }
      if (visible_keys.indexOf(key) < 0) {
        name = '[' + key + ']';
      }
      if (!str) {
        if (seen.indexOf(value[key]) < 0) {
          if (recurseTimes === null) {
            str = format(value[key]);
          } else {
            str = format(value[key], recurseTimes - 1);
          }
          if (str.indexOf('\n') > -1) {
            if (isArray(value)) {
              str = str.split('\n').map(function(line) {
                return '  ' + line;
              }).join('\n').substr(2);
            } else {
              str = '\n' + str.split('\n').map(function(line) {
                return '   ' + line;
              }).join('\n');
            }
          }
        } else {
          str = stylize('[Circular]', 'special');
        }
      }
      if (typeof name === 'undefined') {
        if (type === 'Array' && key.match(/^\d+$/)) {
          return str;
        }
        name = JSON.stringify('' + key);
        if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
          name = name.substr(1, name.length - 2);
          name = stylize(name, 'name');
        } else {
          name = name.replace(/'/g, "\\'")
                     .replace(/\\"/g, '"')
                     .replace(/(^"|"$)/g, "'");
          name = stylize(name, 'string');
        }
      }

      return name + ': ' + str;
    });

    seen.pop();

    var numLinesEst = 0;
    var length = output.reduce(function(prev, cur) {
      numLinesEst++;
      if (cur.indexOf('\n') >= 0) numLinesEst++;
      return prev + cur.length + 1;
    }, 0);

    if (length > 50) {
      output = braces[0] +
               (base === '' ? '' : base + '\n ') +
               ' ' +
               output.join(',\n  ') +
               ' ' +
               braces[1];

    } else {
      output = braces[0] + base + ' ' + output.join(', ') + ' ' + braces[1];
    }

    return output;
  }
  return format(obj, (typeof depth === 'undefined' ? 2 : depth));
};


function isArray(ar) {
  return ar instanceof Array ||
         Array.isArray(ar) ||
         (ar && ar !== Object.prototype && isArray(ar.__proto__));
}


function isRegExp(re) {
  return re instanceof RegExp ||
    (typeof re === 'object' && Object.prototype.toString.call(re) === '[object RegExp]');
}


function isDate(d) {
  if (d instanceof Date) return true;
  if (typeof d !== 'object') return false;
  var properties = Date.prototype && Object_getOwnPropertyNames(Date.prototype);
  var proto = d.__proto__ && Object_getOwnPropertyNames(d.__proto__);
  return JSON.stringify(proto) === JSON.stringify(properties);
}

function pad(n) {
  return n < 10 ? '0' + n.toString(10) : n.toString(10);
}

var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
              'Oct', 'Nov', 'Dec'];

// 26 Feb 16:19:34
function timestamp() {
  var d = new Date();
  var time = [pad(d.getHours()),
              pad(d.getMinutes()),
              pad(d.getSeconds())].join(':');
  return [d.getDate(), months[d.getMonth()], time].join(' ');
}

exports.log = function (msg) {};

exports.pump = null;

var Object_keys = Object.keys || function (obj) {
    var res = [];
    for (var key in obj) res.push(key);
    return res;
};

var Object_getOwnPropertyNames = Object.getOwnPropertyNames || function (obj) {
    var res = [];
    for (var key in obj) {
        if (Object.hasOwnProperty.call(obj, key)) res.push(key);
    }
    return res;
};

var Object_create = Object.create || function (prototype, properties) {
    // from es5-shim
    var object;
    if (prototype === null) {
        object = { '__proto__' : null };
    }
    else {
        if (typeof prototype !== 'object') {
            throw new TypeError(
                'typeof prototype[' + (typeof prototype) + '] != \'object\''
            );
        }
        var Type = function () {};
        Type.prototype = prototype;
        object = new Type();
        object.__proto__ = prototype;
    }
    if (typeof properties !== 'undefined' && Object.defineProperties) {
        Object.defineProperties(object, properties);
    }
    return object;
};

exports.inherits = function(ctor, superCtor) {
  ctor.super_ = superCtor;
  ctor.prototype = Object_create(superCtor.prototype, {
    constructor: {
      value: ctor,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
};

var formatRegExp = /%[sdj%]/g;
exports.format = function(f) {
  if (typeof f !== 'string') {
    var objects = [];
    for (var i = 0; i < arguments.length; i++) {
      objects.push(exports.inspect(arguments[i]));
    }
    return objects.join(' ');
  }

  var i = 1;
  var args = arguments;
  var len = args.length;
  var str = String(f).replace(formatRegExp, function(x) {
    if (x === '%%') return '%';
    if (i >= len) return x;
    switch (x) {
      case '%s': return String(args[i++]);
      case '%d': return Number(args[i++]);
      case '%j': return JSON.stringify(args[i++]);
      default:
        return x;
    }
  });
  for(var x = args[i]; i < len; x = args[++i]){
    if (x === null || typeof x !== 'object') {
      str += ' ' + x;
    } else {
      str += ' ' + exports.inspect(x);
    }
  }
  return str;
};

},{"events":8}],7:[function(require,module,exports){
(function(){/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

/**
 * utility methods to process coverage objects. A coverage object has the following
 * format.
 *
 *      {
 *          "/path/to/file1.js": { file1 coverage },
 *          "/path/to/file2.js": { file2 coverage }
 *      }
 *
 *  The internals of the file coverage object are intentionally not documented since
 *  it is not a public interface.
 *
 *  *Note:* When a method of this module has the word `File` in it, it will accept
 *  one of the sub-objects of the main coverage object as an argument. Other
 *  methods accept the higher level coverage object with multiple keys.
 *
 * Works on `node` as well as the browser.
 *
 * Usage on nodejs
 * ---------------
 *
 *      var objectUtils = require('istanbul').utils;
 *
 * Usage in a browser
 * ------------------
 *
 * Load this file using a `script` tag or other means. This will set `window.coverageUtils`
 * to this module's exports.
 *
 * @class ObjectUtils
 * @static
 */
(function (isNode) {
    /**
     * adds line coverage information to a file coverage object, reverse-engineering
     * it from statement coverage. The object passed in is updated in place.
     *
     * Note that if line coverage information is already present in the object,
     * it is not recomputed.
     *
     * @method addDerivedInfoForFile
     * @static
     * @param {Object} fileCoverage the coverage object for a single file
     */
    function addDerivedInfoForFile(fileCoverage) {
        var statementMap = fileCoverage.statementMap,
            statements = fileCoverage.s,
            lineMap;

        if (!fileCoverage.l) {
            fileCoverage.l = lineMap = {};
            Object.keys(statements).forEach(function (st) {
                var line = statementMap[st].start.line,
                    count = statements[st],
                    prevVal = lineMap[line];
                if (typeof prevVal === 'undefined' || prevVal < count) {
                    lineMap[line] = count;
                }
            });
        }
    }
    /**
     * adds line coverage information to all file coverage objects.
     *
     * @method addDerivedInfo
     * @static
     * @param {Object} coverage the coverage object
     */
    function addDerivedInfo(coverage) {
        Object.keys(coverage).forEach(function (k) {
            addDerivedInfoForFile(coverage[k]);
        });
    }
    /**
     * removes line coverage information from all file coverage objects
     * @method removeDerivedInfo
     * @static
     * @param {Object} coverage the coverage object
     */
    function removeDerivedInfo(coverage) {
        Object.keys(coverage).forEach(function (k) {
            delete coverage[k].l;
        });
    }

    function percent(covered, total) {
        var tmp;
        if (total > 0) {
            tmp = 1000 * 100 * covered / total + 5;
            return Math.floor(tmp / 10) / 100;
        } else {
            return 100.00;
        }
    }

    function computeSimpleTotals(fileCoverage, property) {
        var stats = fileCoverage[property],
            ret = { total: 0, covered: 0 };

        Object.keys(stats).forEach(function (key) {
            ret.total += 1;
            if (stats[key]) {
                ret.covered += 1;
            }
        });
        ret.pct = percent(ret.covered, ret.total);
        return ret;
    }

    function computeBranchTotals(fileCoverage) {
        var stats = fileCoverage.b,
            ret = { total: 0, covered: 0 };

        Object.keys(stats).forEach(function (key) {
            var branches = stats[key],
                covered = branches.filter(function (num) { return num > 0; });
            ret.total += branches.length;
            ret.covered += covered.length;
        });
        ret.pct = percent(ret.covered, ret.total);
        return ret;
    }
    /**
     * returns a blank summary metrics object. A metrics object has the following
     * format.
     *
     *      {
     *          lines: lineMetrics,
     *          statements: statementMetrics,
     *          functions: functionMetrics,
     *          branches: branchMetrics
     *      }
     *
     *  Each individual metric object looks as follows:
     *
     *      {
     *          total: n,
     *          covered: m,
     *          pct: percent
     *      }
     *
     * @method blankSummary
     * @static
     * @return {Object} a blank metrics object
     */
    function blankSummary() {
        return {
            lines: {
                total: 0,
                covered: 0,
                pct: 'Unknown'
            },
            statements: {
                total: 0,
                covered: 0,
                pct: 'Unknown'
            },
            functions: {
                total: 0,
                covered: 0,
                pct: 'Unknown'
            },
            branches: {
                total: 0,
                covered: 0,
                pct: 'Unknown'
            }
        };
    }
    /**
     * returns the summary metrics given the coverage object for a single file. See `blankSummary()`
     * to understand the format of the returned object.
     *
     * @method summarizeFileCoverage
     * @static
     * @param {Object} fileCoverage the coverage object for a single file.
     * @return {Object} the summary metrics for the file
     */
    function summarizeFileCoverage(fileCoverage) {
        var ret = blankSummary();
        addDerivedInfoForFile(fileCoverage);
        ret.lines = computeSimpleTotals(fileCoverage, 'l');
        ret.functions = computeSimpleTotals(fileCoverage, 'f');
        ret.statements = computeSimpleTotals(fileCoverage, 's');
        ret.branches = computeBranchTotals(fileCoverage);
        return ret;
    }
    /**
     * merges two instances of file coverage objects *for the same file*
     * such that the execution counts are correct.
     *
     * @method mergeFileCoverage
     * @static
     * @param {Object} first the first file coverage object for a given file
     * @param {Object} second the second file coverage object for the same file
     * @return {Object} an object that is a result of merging the two. Note that
     *      the input objects are not changed in any way.
     */
    function mergeFileCoverage(first, second) {
        var ret = JSON.parse(JSON.stringify(first)),
            i;

        delete ret.l; //remove derived info

        Object.keys(second.s).forEach(function (k) {
            ret.s[k] += second.s[k];
        });
        Object.keys(second.f).forEach(function (k) {
            ret.f[k] += second.f[k];
        });
        Object.keys(second.b).forEach(function (k) {
            var retArray = ret.b[k],
                secondArray = second.b[k];
            for (i = 0; i < retArray.length; i += 1) {
                retArray[i] += secondArray[i];
            }
        });

        return ret;
    }
    /**
     * merges multiple summary metrics objects by summing up the `totals` and
     * `covered` fields and recomputing the percentages. This function is generic
     * and can accept any number of arguments.
     *
     * @method mergeSummaryObjects
     * @static
     * @param {Object} summary... multiple summary metrics objects
     * @return {Object} the merged summary metrics
     */
    function mergeSummaryObjects() {
        var ret = blankSummary(),
            args = Array.prototype.slice.call(arguments),
            keys = ['lines', 'statements', 'branches', 'functions'],
            increment = function (obj) {
                if (obj) {
                    keys.forEach(function (key) {
                        ret[key].total += obj[key].total;
                        ret[key].covered += obj[key].covered;
                    });
                }
            };
        args.forEach(function (arg) {
            increment(arg);
        });
        keys.forEach(function (key) {
            ret[key].pct = percent(ret[key].covered, ret[key].total);
        });

        return ret;
    }
    /**
     * returns the coverage summary for a single coverage object. This is
     * wrapper over `summarizeFileCoverage` and `mergeSummaryObjects` for
     * the common case of a single coverage object
     * @method summarizeCoverage
     * @static
     * @param {Object} coverage  the coverage object
     * @return {Object} summary coverage metrics across all files in the coverage object
     */
    function summarizeCoverage(coverage) {
        var fileSummary = [];
        Object.keys(coverage).forEach(function (key) {
            fileSummary.push(summarizeFileCoverage(coverage[key]));
        });
        return mergeSummaryObjects.apply(null, fileSummary);
    }

    /**
     * makes the coverage object generated by this library yuitest_coverage compatible.
     * Note that this transformation is lossy since the returned object will not have
     * statement and branch coverage.
     *
     * @method toYUICoverage
     * @static
     * @param {Object} coverage The `istanbul` coverage object
     * @return {Object} a coverage object in `yuitest_coverage` format.
     */
    function toYUICoverage(coverage) {
        var ret = {};

        addDerivedInfo(coverage);

        Object.keys(coverage).forEach(function (k) {
            var fileCoverage = coverage[k],
                lines = fileCoverage.l,
                functions = fileCoverage.f,
                fnMap = fileCoverage.fnMap,
                o;

            o = ret[k] = {
                lines: {},
                calledLines: 0,
                coveredLines: 0,
                functions: {},
                calledFunctions: 0,
                coveredFunctions: 0
            };
            Object.keys(lines).forEach(function (k) {
                o.lines[k] = lines[k];
                o.coveredLines += 1;
                if (lines[k] > 0) {
                    o.calledLines += 1;
                }
            });
            Object.keys(functions).forEach(function (k) {
                var name = fnMap[k].name + ':' + fnMap[k].line;
                o.functions[name] = functions[k];
                o.coveredFunctions += 1;
                if (functions[k] > 0) {
                    o.calledFunctions += 1;
                }
            });
        });
        return ret;
    }

    var exportables = {
        addDerivedInfo: addDerivedInfo,
        addDerivedInfoForFile: addDerivedInfoForFile,
        removeDerivedInfo: removeDerivedInfo,
        blankSummary: blankSummary,
        summarizeFileCoverage: summarizeFileCoverage,
        summarizeCoverage: summarizeCoverage,
        mergeFileCoverage: mergeFileCoverage,
        mergeSummaryObjects: mergeSummaryObjects,
        toYUICoverage: toYUICoverage
    };

    if (isNode) {
        module.exports = exportables;
    } else {
        window.coverageUtils = exportables;
    }
}(typeof module !== 'undefined' && typeof module.exports !== 'undefined' && typeof exports !== 'undefined'));


})()
},{}],8:[function(require,module,exports){
(function(process){if (!process.EventEmitter) process.EventEmitter = function () {};

var EventEmitter = exports.EventEmitter = process.EventEmitter;
var isArray = typeof Array.isArray === 'function'
    ? Array.isArray
    : function (xs) {
        return Object.prototype.toString.call(xs) === '[object Array]'
    }
;
function indexOf (xs, x) {
    if (xs.indexOf) return xs.indexOf(x);
    for (var i = 0; i < xs.length; i++) {
        if (x === xs[i]) return i;
    }
    return -1;
}

// By default EventEmitters will print a warning if more than
// 10 listeners are added to it. This is a useful default which
// helps finding memory leaks.
//
// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
var defaultMaxListeners = 10;
EventEmitter.prototype.setMaxListeners = function(n) {
  if (!this._events) this._events = {};
  this._events.maxListeners = n;
};


EventEmitter.prototype.emit = function(type) {
  // If there is no 'error' event listener then throw.
  if (type === 'error') {
    if (!this._events || !this._events.error ||
        (isArray(this._events.error) && !this._events.error.length))
    {
      if (arguments[1] instanceof Error) {
        throw arguments[1]; // Unhandled 'error' event
      } else {
        throw new Error("Uncaught, unspecified 'error' event.");
      }
      return false;
    }
  }

  if (!this._events) return false;
  var handler = this._events[type];
  if (!handler) return false;

  if (typeof handler == 'function') {
    switch (arguments.length) {
      // fast cases
      case 1:
        handler.call(this);
        break;
      case 2:
        handler.call(this, arguments[1]);
        break;
      case 3:
        handler.call(this, arguments[1], arguments[2]);
        break;
      // slower
      default:
        var args = Array.prototype.slice.call(arguments, 1);
        handler.apply(this, args);
    }
    return true;

  } else if (isArray(handler)) {
    var args = Array.prototype.slice.call(arguments, 1);

    var listeners = handler.slice();
    for (var i = 0, l = listeners.length; i < l; i++) {
      listeners[i].apply(this, args);
    }
    return true;

  } else {
    return false;
  }
};

// EventEmitter is defined in src/node_events.cc
// EventEmitter.prototype.emit() is also defined there.
EventEmitter.prototype.addListener = function(type, listener) {
  if ('function' !== typeof listener) {
    throw new Error('addListener only takes instances of Function');
  }

  if (!this._events) this._events = {};

  // To avoid recursion in the case that type == "newListeners"! Before
  // adding it to the listeners, first emit "newListeners".
  this.emit('newListener', type, listener);

  if (!this._events[type]) {
    // Optimize the case of one listener. Don't need the extra array object.
    this._events[type] = listener;
  } else if (isArray(this._events[type])) {

    // Check for listener leak
    if (!this._events[type].warned) {
      var m;
      if (this._events.maxListeners !== undefined) {
        m = this._events.maxListeners;
      } else {
        m = defaultMaxListeners;
      }

      if (m && m > 0 && this._events[type].length > m) {
        this._events[type].warned = true;
        console.error('(node) warning: possible EventEmitter memory ' +
                      'leak detected. %d listeners added. ' +
                      'Use emitter.setMaxListeners() to increase limit.',
                      this._events[type].length);
        console.trace();
      }
    }

    // If we've already got an array, just append.
    this._events[type].push(listener);
  } else {
    // Adding the second element, need to change to array.
    this._events[type] = [this._events[type], listener];
  }

  return this;
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.once = function(type, listener) {
  var self = this;
  self.on(type, function g() {
    self.removeListener(type, g);
    listener.apply(this, arguments);
  });

  return this;
};

EventEmitter.prototype.removeListener = function(type, listener) {
  if ('function' !== typeof listener) {
    throw new Error('removeListener only takes instances of Function');
  }

  // does not use listeners(), so no side effect of creating _events[type]
  if (!this._events || !this._events[type]) return this;

  var list = this._events[type];

  if (isArray(list)) {
    var i = indexOf(list, listener);
    if (i < 0) return this;
    list.splice(i, 1);
    if (list.length == 0)
      delete this._events[type];
  } else if (this._events[type] === listener) {
    delete this._events[type];
  }

  return this;
};

EventEmitter.prototype.removeAllListeners = function(type) {
  if (arguments.length === 0) {
    this._events = {};
    return this;
  }

  // does not use listeners(), so no side effect of creating _events[type]
  if (type && this._events && this._events[type]) this._events[type] = null;
  return this;
};

EventEmitter.prototype.listeners = function(type) {
  if (!this._events) this._events = {};
  if (!this._events[type]) this._events[type] = [];
  if (!isArray(this._events[type])) {
    this._events[type] = [this._events[type]];
  }
  return this._events[type];
};

})(require("__browserify_process"))
},{"__browserify_process":1}],6:[function(require,module,exports){
(function(__dirname){/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

var Factory = require('../util/factory'),
    factory = new Factory('report', __dirname, false);
/**
 * abstract report class for producing coverage reports.
 *
 * Usage
 * -----
 *
 *      var Report = require('istanbul').Report,
 *          report = Report.create('html'),
 *          collector = new require('istanbul').Collector;
 *
 *      collector.add(coverageObject);
 *      report.writeReport(collector);
 *
 * @class Report
 * @constructor
 * @protected
 * @param {Object} options Optional. The options supported by a specific store implementation.
 */
function Report(/* options */) {}
//add register, create, mix, loadAll, getStoreList as class methods
factory.bindClassMethods(Report);

/**
 * registers a new report implementation.
 * @method register
 * @static
 * @param {Function} constructor the constructor function for the report. This function must have a
 *  `TYPE` property of type String, that will be used in `Report.create()`
 */
/**
 * returns a report implementation of the specified type.
 * @method create
 * @static
 * @param {String} type the type of report to create
 * @param {Object} opts Optional. Options specific to the report implementation
 * @return {Report} a new store of the specified type
 */

Report.prototype = {
    /**
     * writes the report for a set of coverage objects added to a collector.
     * @method writeReport
     * @param {Collector} collector the collector for getting the set of files and coverage
     * @param {Boolean} sync true if reports must be written synchronously, false if they can be written using asynchronous means (e.g. stream.write)
     */
    writeReport: function (/* collector, sync */) {
        throw new Error('writeReport: must be overridden');
    }
};

module.exports = Report;



})("/")
},{"../util/factory":9}],10:[function(require,module,exports){
// nothing to see here... no file methods for the browser

},{}],5:[function(require,module,exports){
/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

var path = require('path'),
    util = require('util'),
    fs = require('fs'),
    async = require('async'),
    mkdirp = require('mkdirp'),
    writer = require('./writer'),
    Writer = writer.Writer,
    ContentWriter = writer.ContentWriter;

function extend(cons, proto) {
    Object.keys(proto).forEach(function (k) {
        cons.prototype[k] = proto[k];
    });
}

function BufferedContentWriter() {
    ContentWriter.call(this);
    this.content = '';
}
util.inherits(BufferedContentWriter, ContentWriter);

extend(BufferedContentWriter, {
    write: function (str) {
        this.content += str;
    },
    getContent: function () {
        return this.content;
    }
});

function StreamContentWriter(stream) {
    ContentWriter.call(this);
    this.stream = stream;
}
util.inherits(StreamContentWriter, ContentWriter);

extend(StreamContentWriter, {
    write: function (str) {
        this.stream.write(str);
    }
});

function SyncFileWriter() {
    Writer.call(this);
}
util.inherits(SyncFileWriter, Writer);

extend(SyncFileWriter, {
    writeFile: function (file, callback) {
        mkdirp.sync(path.dirname(file));
        var cw = new BufferedContentWriter();
        callback(cw);
        fs.writeFileSync(file, cw.getContent(), 'utf8');
    },
    done: function () {
        this.emit('done'); //everything already done
    }
});

function AsyncFileWriter() {
    this.queue = async.queue(this.processFile.bind(this), 20);
    this.openFileMap = {};
}

util.inherits(AsyncFileWriter, Writer);

extend(AsyncFileWriter, {
    writeFile: function (file, callback) {
        this.openFileMap[file] = true;
        this.queue.push({ file: file, callback: callback });
    },
    processFile: function (task, cb) {
        var file = task.file,
            userCallback = task.callback,
            that = this,
            stream,
            contentWriter;

        mkdirp.sync(path.dirname(file));
        stream = fs.createWriteStream(file);
        stream.on('close', function () {
            delete that.openFileMap[file];
            cb();
            that.checkDone();
        });
        stream.on('error', function (err) { that.emit('error', err); });
        contentWriter = new StreamContentWriter(stream);
        userCallback(contentWriter);
        stream.end();
    },
    done: function () {
        this.doneCalled = true;
        this.checkDone();
    },
    checkDone: function () {
        if (!this.doneCalled) { return; }
        if (Object.keys(this.openFileMap).length === 0) {
            this.emit('done');
        }
    }
});

function FileWriter(sync) {
    Writer.call(this);
    var that = this;
    this.delegate = sync ? new SyncFileWriter() : new AsyncFileWriter();
    this.delegate.on('error', function (err) { that.emit('error', err); });
    this.delegate.on('done', function () { that.emit('done'); });
}

util.inherits(FileWriter, Writer);

extend(FileWriter, {
    copyFile: function (source, dest) {
        mkdirp.sync(path.dirname(dest));
        fs.writeFileSync(dest, fs.readFileSync(source));
    },
    writeFile: function (file, callback) {
        this.delegate.writeFile(file, callback);
    },
    done: function () {
        this.delegate.done();
    }
});

module.exports = FileWriter;
},{"path":3,"util":4,"fs":10,"./writer":11,"async":12,"mkdirp":13}],13:[function(require,module,exports){
(function(process){var path = require('path');
var fs = require('fs');

module.exports = mkdirP.mkdirp = mkdirP.mkdirP = mkdirP;

function mkdirP (p, mode, f, made) {
    if (typeof mode === 'function' || mode === undefined) {
        f = mode;
        mode = 0777 & (~process.umask());
    }
    if (!made) made = null;

    var cb = f || function () {};
    if (typeof mode === 'string') mode = parseInt(mode, 8);
    p = path.resolve(p);

    fs.mkdir(p, mode, function (er) {
        if (!er) {
            made = made || p;
            return cb(null, made);
        }
        switch (er.code) {
            case 'ENOENT':
                mkdirP(path.dirname(p), mode, function (er, made) {
                    if (er) cb(er, made);
                    else mkdirP(p, mode, cb, made);
                });
                break;

            // In the case of any other error, just see if there's a dir
            // there already.  If so, then hooray!  If not, then something
            // is borked.
            default:
                fs.stat(p, function (er2, stat) {
                    // if the stat fails, then that's super weird.
                    // let the original error be the failure reason.
                    if (er2 || !stat.isDirectory()) cb(er, made)
                    else cb(null, made);
                });
                break;
        }
    });
}

mkdirP.sync = function sync (p, mode, made) {
    if (mode === undefined) {
        mode = 0777 & (~process.umask());
    }
    if (!made) made = null;

    if (typeof mode === 'string') mode = parseInt(mode, 8);
    p = path.resolve(p);

    try {
        fs.mkdirSync(p, mode);
        made = made || p;
    }
    catch (err0) {
        switch (err0.code) {
            case 'ENOENT' :
                made = sync(path.dirname(p), mode, made);
                sync(p, mode, made);
                break;

            // In the case of any other error, just see if there's a dir
            // there already.  If so, then hooray!  If not, then something
            // is borked.
            default:
                var stat;
                try {
                    stat = fs.statSync(p);
                }
                catch (err1) {
                    throw err0;
                }
                if (!stat.isDirectory()) throw err0;
                break;
        }
    }

    return made;
};

})(require("__browserify_process"))
},{"path":3,"fs":10,"__browserify_process":1}],11:[function(require,module,exports){
/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

var util = require('util'),
    EventEmitter = require('events').EventEmitter;

function extend(cons, proto) {
    Object.keys(proto).forEach(function (k) {
        cons.prototype[k] = proto[k];
    });
}

//abstract interface for writing content
function ContentWriter() {
}

ContentWriter.prototype = {
    write: function (/* str */) { throw new Error('write: must be overridden'); },
    println: function (str) { this.write(str); this.write('\n'); }
};

//abstract interface for writing files and assets
function Writer() {
    EventEmitter.call(this);
}

util.inherits(Writer, EventEmitter);

extend(Writer, {
    /**
     * allows writing content to a file using a callback that is passed a content writer
     * @param file the name of the file to write
     * @param callback the callback that is called as `callback(contentWriter)`
     */
    writeFile: function (/* file, callback */) { throw new Error('writeFile: must be overridden'); },
    /**
     * copies a file from source to destination
     * @param source the file to copy, found on the file system
     * @param dest the destination path
     */
    copyFile: function (/* source, dest */) { throw new Error('copyFile: must be overridden'); },
    /**
     * marker method to indicate that the caller is done with this writer object
     * The writer is expected to emit a `done` event only after this method is called
     * and it is truly done.
     */
    done: function () { throw new Error('done: must be overridden'); }
});

module.exports = {
    Writer: Writer,
    ContentWriter: ContentWriter
};


},{"util":4,"events":8}],12:[function(require,module,exports){
(function(process){/*global setImmediate: false, setTimeout: false, console: false */
(function () {

    var async = {};

    // global on the server, window in the browser
    var root, previous_async;

    root = this;
    if (root != null) {
      previous_async = root.async;
    }

    async.noConflict = function () {
        root.async = previous_async;
        return async;
    };

    function only_once(fn) {
        var called = false;
        return function() {
            if (called) throw new Error("Callback was already called.");
            called = true;
            fn.apply(root, arguments);
        }
    }

    //// cross-browser compatiblity functions ////

    var _each = function (arr, iterator) {
        if (arr.forEach) {
            return arr.forEach(iterator);
        }
        for (var i = 0; i < arr.length; i += 1) {
            iterator(arr[i], i, arr);
        }
    };

    var _map = function (arr, iterator) {
        if (arr.map) {
            return arr.map(iterator);
        }
        var results = [];
        _each(arr, function (x, i, a) {
            results.push(iterator(x, i, a));
        });
        return results;
    };

    var _reduce = function (arr, iterator, memo) {
        if (arr.reduce) {
            return arr.reduce(iterator, memo);
        }
        _each(arr, function (x, i, a) {
            memo = iterator(memo, x, i, a);
        });
        return memo;
    };

    var _keys = function (obj) {
        if (Object.keys) {
            return Object.keys(obj);
        }
        var keys = [];
        for (var k in obj) {
            if (obj.hasOwnProperty(k)) {
                keys.push(k);
            }
        }
        return keys;
    };

    //// exported async module functions ////

    //// nextTick implementation with browser-compatible fallback ////
    if (typeof process === 'undefined' || !(process.nextTick)) {
        if (typeof setImmediate === 'function') {
            async.nextTick = function (fn) {
                // not a direct alias for IE10 compatibility
                setImmediate(fn);
            };
            async.setImmediate = async.nextTick;
        }
        else {
            async.nextTick = function (fn) {
                setTimeout(fn, 0);
            };
            async.setImmediate = async.nextTick;
        }
    }
    else {
        async.nextTick = process.nextTick;
        if (typeof setImmediate !== 'undefined') {
            async.setImmediate = setImmediate;
        }
        else {
            async.setImmediate = async.nextTick;
        }
    }

    async.each = function (arr, iterator, callback) {
        callback = callback || function () {};
        if (!arr.length) {
            return callback();
        }
        var completed = 0;
        _each(arr, function (x) {
            iterator(x, only_once(function (err) {
                if (err) {
                    callback(err);
                    callback = function () {};
                }
                else {
                    completed += 1;
                    if (completed >= arr.length) {
                        callback(null);
                    }
                }
            }));
        });
    };
    async.forEach = async.each;

    async.eachSeries = function (arr, iterator, callback) {
        callback = callback || function () {};
        if (!arr.length) {
            return callback();
        }
        var completed = 0;
        var iterate = function () {
            iterator(arr[completed], function (err) {
                if (err) {
                    callback(err);
                    callback = function () {};
                }
                else {
                    completed += 1;
                    if (completed >= arr.length) {
                        callback(null);
                    }
                    else {
                        iterate();
                    }
                }
            });
        };
        iterate();
    };
    async.forEachSeries = async.eachSeries;

    async.eachLimit = function (arr, limit, iterator, callback) {
        var fn = _eachLimit(limit);
        fn.apply(null, [arr, iterator, callback]);
    };
    async.forEachLimit = async.eachLimit;

    var _eachLimit = function (limit) {

        return function (arr, iterator, callback) {
            callback = callback || function () {};
            if (!arr.length || limit <= 0) {
                return callback();
            }
            var completed = 0;
            var started = 0;
            var running = 0;

            (function replenish () {
                if (completed >= arr.length) {
                    return callback();
                }

                while (running < limit && started < arr.length) {
                    started += 1;
                    running += 1;
                    iterator(arr[started - 1], function (err) {
                        if (err) {
                            callback(err);
                            callback = function () {};
                        }
                        else {
                            completed += 1;
                            running -= 1;
                            if (completed >= arr.length) {
                                callback();
                            }
                            else {
                                replenish();
                            }
                        }
                    });
                }
            })();
        };
    };


    var doParallel = function (fn) {
        return function () {
            var args = Array.prototype.slice.call(arguments);
            return fn.apply(null, [async.each].concat(args));
        };
    };
    var doParallelLimit = function(limit, fn) {
        return function () {
            var args = Array.prototype.slice.call(arguments);
            return fn.apply(null, [_eachLimit(limit)].concat(args));
        };
    };
    var doSeries = function (fn) {
        return function () {
            var args = Array.prototype.slice.call(arguments);
            return fn.apply(null, [async.eachSeries].concat(args));
        };
    };


    var _asyncMap = function (eachfn, arr, iterator, callback) {
        var results = [];
        arr = _map(arr, function (x, i) {
            return {index: i, value: x};
        });
        eachfn(arr, function (x, callback) {
            iterator(x.value, function (err, v) {
                results[x.index] = v;
                callback(err);
            });
        }, function (err) {
            callback(err, results);
        });
    };
    async.map = doParallel(_asyncMap);
    async.mapSeries = doSeries(_asyncMap);
    async.mapLimit = function (arr, limit, iterator, callback) {
        return _mapLimit(limit)(arr, iterator, callback);
    };

    var _mapLimit = function(limit) {
        return doParallelLimit(limit, _asyncMap);
    };

    // reduce only has a series version, as doing reduce in parallel won't
    // work in many situations.
    async.reduce = function (arr, memo, iterator, callback) {
        async.eachSeries(arr, function (x, callback) {
            iterator(memo, x, function (err, v) {
                memo = v;
                callback(err);
            });
        }, function (err) {
            callback(err, memo);
        });
    };
    // inject alias
    async.inject = async.reduce;
    // foldl alias
    async.foldl = async.reduce;

    async.reduceRight = function (arr, memo, iterator, callback) {
        var reversed = _map(arr, function (x) {
            return x;
        }).reverse();
        async.reduce(reversed, memo, iterator, callback);
    };
    // foldr alias
    async.foldr = async.reduceRight;

    var _filter = function (eachfn, arr, iterator, callback) {
        var results = [];
        arr = _map(arr, function (x, i) {
            return {index: i, value: x};
        });
        eachfn(arr, function (x, callback) {
            iterator(x.value, function (v) {
                if (v) {
                    results.push(x);
                }
                callback();
            });
        }, function (err) {
            callback(_map(results.sort(function (a, b) {
                return a.index - b.index;
            }), function (x) {
                return x.value;
            }));
        });
    };
    async.filter = doParallel(_filter);
    async.filterSeries = doSeries(_filter);
    // select alias
    async.select = async.filter;
    async.selectSeries = async.filterSeries;

    var _reject = function (eachfn, arr, iterator, callback) {
        var results = [];
        arr = _map(arr, function (x, i) {
            return {index: i, value: x};
        });
        eachfn(arr, function (x, callback) {
            iterator(x.value, function (v) {
                if (!v) {
                    results.push(x);
                }
                callback();
            });
        }, function (err) {
            callback(_map(results.sort(function (a, b) {
                return a.index - b.index;
            }), function (x) {
                return x.value;
            }));
        });
    };
    async.reject = doParallel(_reject);
    async.rejectSeries = doSeries(_reject);

    var _detect = function (eachfn, arr, iterator, main_callback) {
        eachfn(arr, function (x, callback) {
            iterator(x, function (result) {
                if (result) {
                    main_callback(x);
                    main_callback = function () {};
                }
                else {
                    callback();
                }
            });
        }, function (err) {
            main_callback();
        });
    };
    async.detect = doParallel(_detect);
    async.detectSeries = doSeries(_detect);

    async.some = function (arr, iterator, main_callback) {
        async.each(arr, function (x, callback) {
            iterator(x, function (v) {
                if (v) {
                    main_callback(true);
                    main_callback = function () {};
                }
                callback();
            });
        }, function (err) {
            main_callback(false);
        });
    };
    // any alias
    async.any = async.some;

    async.every = function (arr, iterator, main_callback) {
        async.each(arr, function (x, callback) {
            iterator(x, function (v) {
                if (!v) {
                    main_callback(false);
                    main_callback = function () {};
                }
                callback();
            });
        }, function (err) {
            main_callback(true);
        });
    };
    // all alias
    async.all = async.every;

    async.sortBy = function (arr, iterator, callback) {
        async.map(arr, function (x, callback) {
            iterator(x, function (err, criteria) {
                if (err) {
                    callback(err);
                }
                else {
                    callback(null, {value: x, criteria: criteria});
                }
            });
        }, function (err, results) {
            if (err) {
                return callback(err);
            }
            else {
                var fn = function (left, right) {
                    var a = left.criteria, b = right.criteria;
                    return a < b ? -1 : a > b ? 1 : 0;
                };
                callback(null, _map(results.sort(fn), function (x) {
                    return x.value;
                }));
            }
        });
    };

    async.auto = function (tasks, callback) {
        callback = callback || function () {};
        var keys = _keys(tasks);
        if (!keys.length) {
            return callback(null);
        }

        var results = {};

        var listeners = [];
        var addListener = function (fn) {
            listeners.unshift(fn);
        };
        var removeListener = function (fn) {
            for (var i = 0; i < listeners.length; i += 1) {
                if (listeners[i] === fn) {
                    listeners.splice(i, 1);
                    return;
                }
            }
        };
        var taskComplete = function () {
            _each(listeners.slice(0), function (fn) {
                fn();
            });
        };

        addListener(function () {
            if (_keys(results).length === keys.length) {
                callback(null, results);
                callback = function () {};
            }
        });

        _each(keys, function (k) {
            var task = (tasks[k] instanceof Function) ? [tasks[k]]: tasks[k];
            var taskCallback = function (err) {
                var args = Array.prototype.slice.call(arguments, 1);
                if (args.length <= 1) {
                    args = args[0];
                }
                if (err) {
                    var safeResults = {};
                    _each(_keys(results), function(rkey) {
                        safeResults[rkey] = results[rkey];
                    });
                    safeResults[k] = args;
                    callback(err, safeResults);
                    // stop subsequent errors hitting callback multiple times
                    callback = function () {};
                }
                else {
                    results[k] = args;
                    async.setImmediate(taskComplete);
                }
            };
            var requires = task.slice(0, Math.abs(task.length - 1)) || [];
            var ready = function () {
                return _reduce(requires, function (a, x) {
                    return (a && results.hasOwnProperty(x));
                }, true) && !results.hasOwnProperty(k);
            };
            if (ready()) {
                task[task.length - 1](taskCallback, results);
            }
            else {
                var listener = function () {
                    if (ready()) {
                        removeListener(listener);
                        task[task.length - 1](taskCallback, results);
                    }
                };
                addListener(listener);
            }
        });
    };

    async.waterfall = function (tasks, callback) {
        callback = callback || function () {};
        if (tasks.constructor !== Array) {
          var err = new Error('First argument to waterfall must be an array of functions');
          return callback(err);
        }
        if (!tasks.length) {
            return callback();
        }
        var wrapIterator = function (iterator) {
            return function (err) {
                if (err) {
                    callback.apply(null, arguments);
                    callback = function () {};
                }
                else {
                    var args = Array.prototype.slice.call(arguments, 1);
                    var next = iterator.next();
                    if (next) {
                        args.push(wrapIterator(next));
                    }
                    else {
                        args.push(callback);
                    }
                    async.setImmediate(function () {
                        iterator.apply(null, args);
                    });
                }
            };
        };
        wrapIterator(async.iterator(tasks))();
    };

    var _parallel = function(eachfn, tasks, callback) {
        callback = callback || function () {};
        if (tasks.constructor === Array) {
            eachfn.map(tasks, function (fn, callback) {
                if (fn) {
                    fn(function (err) {
                        var args = Array.prototype.slice.call(arguments, 1);
                        if (args.length <= 1) {
                            args = args[0];
                        }
                        callback.call(null, err, args);
                    });
                }
            }, callback);
        }
        else {
            var results = {};
            eachfn.each(_keys(tasks), function (k, callback) {
                tasks[k](function (err) {
                    var args = Array.prototype.slice.call(arguments, 1);
                    if (args.length <= 1) {
                        args = args[0];
                    }
                    results[k] = args;
                    callback(err);
                });
            }, function (err) {
                callback(err, results);
            });
        }
    };

    async.parallel = function (tasks, callback) {
        _parallel({ map: async.map, each: async.each }, tasks, callback);
    };

    async.parallelLimit = function(tasks, limit, callback) {
        _parallel({ map: _mapLimit(limit), each: _eachLimit(limit) }, tasks, callback);
    };

    async.series = function (tasks, callback) {
        callback = callback || function () {};
        if (tasks.constructor === Array) {
            async.mapSeries(tasks, function (fn, callback) {
                if (fn) {
                    fn(function (err) {
                        var args = Array.prototype.slice.call(arguments, 1);
                        if (args.length <= 1) {
                            args = args[0];
                        }
                        callback.call(null, err, args);
                    });
                }
            }, callback);
        }
        else {
            var results = {};
            async.eachSeries(_keys(tasks), function (k, callback) {
                tasks[k](function (err) {
                    var args = Array.prototype.slice.call(arguments, 1);
                    if (args.length <= 1) {
                        args = args[0];
                    }
                    results[k] = args;
                    callback(err);
                });
            }, function (err) {
                callback(err, results);
            });
        }
    };

    async.iterator = function (tasks) {
        var makeCallback = function (index) {
            var fn = function () {
                if (tasks.length) {
                    tasks[index].apply(null, arguments);
                }
                return fn.next();
            };
            fn.next = function () {
                return (index < tasks.length - 1) ? makeCallback(index + 1): null;
            };
            return fn;
        };
        return makeCallback(0);
    };

    async.apply = function (fn) {
        var args = Array.prototype.slice.call(arguments, 1);
        return function () {
            return fn.apply(
                null, args.concat(Array.prototype.slice.call(arguments))
            );
        };
    };

    var _concat = function (eachfn, arr, fn, callback) {
        var r = [];
        eachfn(arr, function (x, cb) {
            fn(x, function (err, y) {
                r = r.concat(y || []);
                cb(err);
            });
        }, function (err) {
            callback(err, r);
        });
    };
    async.concat = doParallel(_concat);
    async.concatSeries = doSeries(_concat);

    async.whilst = function (test, iterator, callback) {
        if (test()) {
            iterator(function (err) {
                if (err) {
                    return callback(err);
                }
                async.whilst(test, iterator, callback);
            });
        }
        else {
            callback();
        }
    };

    async.doWhilst = function (iterator, test, callback) {
        iterator(function (err) {
            if (err) {
                return callback(err);
            }
            if (test()) {
                async.doWhilst(iterator, test, callback);
            }
            else {
                callback();
            }
        });
    };

    async.until = function (test, iterator, callback) {
        if (!test()) {
            iterator(function (err) {
                if (err) {
                    return callback(err);
                }
                async.until(test, iterator, callback);
            });
        }
        else {
            callback();
        }
    };

    async.doUntil = function (iterator, test, callback) {
        iterator(function (err) {
            if (err) {
                return callback(err);
            }
            if (!test()) {
                async.doUntil(iterator, test, callback);
            }
            else {
                callback();
            }
        });
    };

    async.queue = function (worker, concurrency) {
        if (concurrency === undefined) {
            concurrency = 1;
        }
        function _insert(q, data, pos, callback) {
          if(data.constructor !== Array) {
              data = [data];
          }
          _each(data, function(task) {
              var item = {
                  data: task,
                  callback: typeof callback === 'function' ? callback : null
              };

              if (pos) {
                q.tasks.unshift(item);
              } else {
                q.tasks.push(item);
              }

              if (q.saturated && q.tasks.length === concurrency) {
                  q.saturated();
              }
              async.setImmediate(q.process);
          });
        }

        var workers = 0;
        var q = {
            tasks: [],
            concurrency: concurrency,
            saturated: null,
            empty: null,
            drain: null,
            push: function (data, callback) {
              _insert(q, data, false, callback);
            },
            unshift: function (data, callback) {
              _insert(q, data, true, callback);
            },
            process: function () {
                if (workers < q.concurrency && q.tasks.length) {
                    var task = q.tasks.shift();
                    if (q.empty && q.tasks.length === 0) {
                        q.empty();
                    }
                    workers += 1;
                    var next = function () {
                        workers -= 1;
                        if (task.callback) {
                            task.callback.apply(task, arguments);
                        }
                        if (q.drain && q.tasks.length + workers === 0) {
                            q.drain();
                        }
                        q.process();
                    };
                    var cb = only_once(next);
                    worker(task.data, cb);
                }
            },
            length: function () {
                return q.tasks.length;
            },
            running: function () {
                return workers;
            }
        };
        return q;
    };

    async.cargo = function (worker, payload) {
        var working     = false,
            tasks       = [];

        var cargo = {
            tasks: tasks,
            payload: payload,
            saturated: null,
            empty: null,
            drain: null,
            push: function (data, callback) {
                if(data.constructor !== Array) {
                    data = [data];
                }
                _each(data, function(task) {
                    tasks.push({
                        data: task,
                        callback: typeof callback === 'function' ? callback : null
                    });
                    if (cargo.saturated && tasks.length === payload) {
                        cargo.saturated();
                    }
                });
                async.setImmediate(cargo.process);
            },
            process: function process() {
                if (working) return;
                if (tasks.length === 0) {
                    if(cargo.drain) cargo.drain();
                    return;
                }

                var ts = typeof payload === 'number'
                            ? tasks.splice(0, payload)
                            : tasks.splice(0);

                var ds = _map(ts, function (task) {
                    return task.data;
                });

                if(cargo.empty) cargo.empty();
                working = true;
                worker(ds, function () {
                    working = false;

                    var args = arguments;
                    _each(ts, function (data) {
                        if (data.callback) {
                            data.callback.apply(null, args);
                        }
                    });

                    process();
                });
            },
            length: function () {
                return tasks.length;
            },
            running: function () {
                return working;
            }
        };
        return cargo;
    };

    var _console_fn = function (name) {
        return function (fn) {
            var args = Array.prototype.slice.call(arguments, 1);
            fn.apply(null, args.concat([function (err) {
                var args = Array.prototype.slice.call(arguments, 1);
                if (typeof console !== 'undefined') {
                    if (err) {
                        if (console.error) {
                            console.error(err);
                        }
                    }
                    else if (console[name]) {
                        _each(args, function (x) {
                            console[name](x);
                        });
                    }
                }
            }]));
        };
    };
    async.log = _console_fn('log');
    async.dir = _console_fn('dir');
    /*async.info = _console_fn('info');
    async.warn = _console_fn('warn');
    async.error = _console_fn('error');*/

    async.memoize = function (fn, hasher) {
        var memo = {};
        var queues = {};
        hasher = hasher || function (x) {
            return x;
        };
        var memoized = function () {
            var args = Array.prototype.slice.call(arguments);
            var callback = args.pop();
            var key = hasher.apply(null, args);
            if (key in memo) {
                callback.apply(null, memo[key]);
            }
            else if (key in queues) {
                queues[key].push(callback);
            }
            else {
                queues[key] = [callback];
                fn.apply(null, args.concat([function () {
                    memo[key] = arguments;
                    var q = queues[key];
                    delete queues[key];
                    for (var i = 0, l = q.length; i < l; i++) {
                      q[i].apply(null, arguments);
                    }
                }]));
            }
        };
        memoized.memo = memo;
        memoized.unmemoized = fn;
        return memoized;
    };

    async.unmemoize = function (fn) {
      return function () {
        return (fn.unmemoized || fn).apply(null, arguments);
      };
    };

    async.times = function (count, iterator, callback) {
        var counter = [];
        for (var i = 0; i < count; i++) {
            counter.push(i);
        }
        return async.map(counter, iterator, callback);
    };

    async.timesSeries = function (count, iterator, callback) {
        var counter = [];
        for (var i = 0; i < count; i++) {
            counter.push(i);
        }
        return async.mapSeries(counter, iterator, callback);
    };

    async.compose = function (/* functions... */) {
        var fns = Array.prototype.reverse.call(arguments);
        return function () {
            var that = this;
            var args = Array.prototype.slice.call(arguments);
            var callback = args.pop();
            async.reduce(fns, args, function (newargs, fn, cb) {
                fn.apply(that, newargs.concat([function () {
                    var err = arguments[0];
                    var nextargs = Array.prototype.slice.call(arguments, 1);
                    cb(err, nextargs);
                }]))
            },
            function (err, results) {
                callback.apply(that, [err].concat(results));
            });
        };
    };

    var _applyEach = function (eachfn, fns /*args...*/) {
        var go = function () {
            var that = this;
            var args = Array.prototype.slice.call(arguments);
            var callback = args.pop();
            return eachfn(fns, function (fn, cb) {
                fn.apply(that, args.concat([cb]));
            },
            callback);
        };
        if (arguments.length > 2) {
            var args = Array.prototype.slice.call(arguments, 2);
            return go.apply(this, args);
        }
        else {
            return go;
        }
    };
    async.applyEach = doParallel(_applyEach);
    async.applyEachSeries = doSeries(_applyEach);

    async.forever = function (fn, callback) {
        function next(err) {
            if (err) {
                if (callback) {
                    return callback(err);
                }
                throw err;
            }
            fn(next);
        }
        next();
    };

    // AMD / RequireJS
    if (typeof define !== 'undefined' && define.amd) {
        define([], function () {
            return async;
        });
    }
    // Node.js
    else if (typeof module !== 'undefined' && module.exports) {
        module.exports = async;
    }
    // included directly via <script> tag
    else {
        root.async = async;
    }

}());

})(require("__browserify_process"))
},{"__browserify_process":1}],9:[function(require,module,exports){
/*
 Copyright (c) 2012, Yahoo! Inc.  All rights reserved.
 Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

var util = require('util'),
    path = require('path'),
    fs = require('fs'),
    abbrev = require('abbrev');

function Factory(kind, dir, allowAbbreviations) {
    this.kind = kind;
    this.dir = dir;
    this.allowAbbreviations = allowAbbreviations;
    this.classMap = {};
    this.abbreviations = null;
}

Factory.prototype = {

    knownTypes: function () {
        var keys = Object.keys(this.classMap);
        keys.sort();
        return keys;
    },

    resolve: function (abbreviatedType) {
        if (!this.abbreviations) {
            this.abbreviations = abbrev(this.knownTypes());
        }
        return this.abbreviations[abbreviatedType];
    },

    register: function (constructor) {
        var type = constructor.TYPE;
        if (!type) { throw new Error('Could not register ' + this.kind + ' constructor [no TYPE property]: ' + util.inspect(constructor)); }
        this.classMap[type] = constructor;
        this.abbreviations = null;
    },

    create: function (type, opts) {
        var allowAbbrev = this.allowAbbreviations,
            realType = allowAbbrev ? this.resolve(type) : type,
            Cons;

        Cons = realType ? this.classMap[realType] : null;
        if (!Cons) { throw new Error('Invalid ' + this.kind + ' [' + type + '], allowed values are ' + this.knownTypes().join(', ')); }
        return new Cons(opts);
    },

    loadStandard: function (dir) {
        var that = this;
        fs.readdirSync(dir).forEach(function (file) {
            if (file !== 'index.js' && file.indexOf('.js') === file.length - 3) {
                try {
                    that.register(require(path.resolve(dir, file)));
                } catch (ex) {
                    console.error(ex.message);
                    console.error(ex.stack);
                    throw new Error('Could not register ' + that.kind + ' from file ' + file);
                }
            }
        });
    },

    bindClassMethods: function (Cons) {
        var tmpKind = this.kind.charAt(0).toUpperCase() + this.kind.substring(1), //ucfirst
            allowAbbrev = this.allowAbbreviations;

        Cons.mix = Factory.mix;
        Cons.register = this.register.bind(this);
        Cons.create = this.create.bind(this);
        Cons.loadAll = this.loadStandard.bind(this, this.dir);
        Cons['get' + tmpKind + 'List'] = this.knownTypes.bind(this);
        if (allowAbbrev) {
            Cons['resolve' + tmpKind + 'Name'] = this.resolve.bind(this);
        }
    }
};

Factory.mix = function (cons, proto) {
    Object.keys(proto).forEach(function (key) {
        cons.prototype[key] = proto[key];
    });
};

module.exports = Factory;


},{"util":4,"path":3,"fs":10,"abbrev":14}],14:[function(require,module,exports){

module.exports = exports = abbrev.abbrev = abbrev

abbrev.monkeyPatch = monkeyPatch

function monkeyPatch () {
  Object.defineProperty(Array.prototype, 'abbrev', {
    value: function () { return abbrev(this) },
    enumerable: false, configurable: true, writable: true
  })

  Object.defineProperty(Object.prototype, 'abbrev', {
    value: function () { return abbrev(Object.keys(this)) },
    enumerable: false, configurable: true, writable: true
  })
}

function abbrev (list) {
  if (arguments.length !== 1 || !Array.isArray(list)) {
    list = Array.prototype.slice.call(arguments, 0)
  }
  for (var i = 0, l = list.length, args = [] ; i < l ; i ++) {
    args[i] = typeof list[i] === "string" ? list[i] : String(list[i])
  }

  // sort them lexicographically, so that they're next to their nearest kin
  args = args.sort(lexSort)

  // walk through each, seeing how much it has in common with the next and previous
  var abbrevs = {}
    , prev = ""
  for (var i = 0, l = args.length ; i < l ; i ++) {
    var current = args[i]
      , next = args[i + 1] || ""
      , nextMatches = true
      , prevMatches = true
    if (current === next) continue
    for (var j = 0, cl = current.length ; j < cl ; j ++) {
      var curChar = current.charAt(j)
      nextMatches = nextMatches && curChar === next.charAt(j)
      prevMatches = prevMatches && curChar === prev.charAt(j)
      if (!nextMatches && !prevMatches) {
        j ++
        break
      }
    }
    prev = current
    if (j === cl) {
      abbrevs[current] = current
      continue
    }
    for (var a = current.substr(0, j) ; j <= cl ; j ++) {
      abbrevs[a] = current
      a += current.charAt(j)
    }
  }
  return abbrevs
}

function lexSort (a, b) {
  return a === b ? 0 : a > b ? 1 : -1
}


// tests
if (module === require.main) {

var assert = require("assert")
var util = require("util")

console.log("running tests")
function test (list, expect) {
  var actual = abbrev(list)
  assert.deepEqual(actual, expect,
    "abbrev("+util.inspect(list)+") === " + util.inspect(expect) + "\n"+
    "actual: "+util.inspect(actual))
  actual = abbrev.apply(exports, list)
  assert.deepEqual(abbrev.apply(exports, list), expect,
    "abbrev("+list.map(JSON.stringify).join(",")+") === " + util.inspect(expect) + "\n"+
    "actual: "+util.inspect(actual))
}

test([ "ruby", "ruby", "rules", "rules", "rules" ],
{ rub: 'ruby'
, ruby: 'ruby'
, rul: 'rules'
, rule: 'rules'
, rules: 'rules'
})
test(["fool", "foom", "pool", "pope"],
{ fool: 'fool'
, foom: 'foom'
, poo: 'pool'
, pool: 'pool'
, pop: 'pope'
, pope: 'pope'
})
test(["a", "ab", "abc", "abcd", "abcde", "acde"],
{ a: 'a'
, ab: 'ab'
, abc: 'abc'
, abcd: 'abcd'
, abcde: 'abcde'
, ac: 'acde'
, acd: 'acde'
, acde: 'acde'
})

console.log("pass")

}

},{"assert":15,"util":4}],15:[function(require,module,exports){
(function(){// UTILITY
var util = require('util');
var Buffer = require("buffer").Buffer;
var pSlice = Array.prototype.slice;

function objectKeys(object) {
  if (Object.keys) return Object.keys(object);
  var result = [];
  for (var name in object) {
    if (Object.prototype.hasOwnProperty.call(object, name)) {
      result.push(name);
    }
  }
  return result;
}

// 1. The assert module provides functions that throw
// AssertionError's when particular conditions are not met. The
// assert module must conform to the following interface.

var assert = module.exports = ok;

// 2. The AssertionError is defined in assert.
// new assert.AssertionError({ message: message,
//                             actual: actual,
//                             expected: expected })

assert.AssertionError = function AssertionError(options) {
  this.name = 'AssertionError';
  this.message = options.message;
  this.actual = options.actual;
  this.expected = options.expected;
  this.operator = options.operator;
  var stackStartFunction = options.stackStartFunction || fail;

  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, stackStartFunction);
  }
};
util.inherits(assert.AssertionError, Error);

function replacer(key, value) {
  if (value === undefined) {
    return '' + value;
  }
  if (typeof value === 'number' && (isNaN(value) || !isFinite(value))) {
    return value.toString();
  }
  if (typeof value === 'function' || value instanceof RegExp) {
    return value.toString();
  }
  return value;
}

function truncate(s, n) {
  if (typeof s == 'string') {
    return s.length < n ? s : s.slice(0, n);
  } else {
    return s;
  }
}

assert.AssertionError.prototype.toString = function() {
  if (this.message) {
    return [this.name + ':', this.message].join(' ');
  } else {
    return [
      this.name + ':',
      truncate(JSON.stringify(this.actual, replacer), 128),
      this.operator,
      truncate(JSON.stringify(this.expected, replacer), 128)
    ].join(' ');
  }
};

// assert.AssertionError instanceof Error

assert.AssertionError.__proto__ = Error.prototype;

// At present only the three keys mentioned above are used and
// understood by the spec. Implementations or sub modules can pass
// other keys to the AssertionError's constructor - they will be
// ignored.

// 3. All of the following functions must throw an AssertionError
// when a corresponding condition is not met, with a message that
// may be undefined if not provided.  All assertion methods provide
// both the actual and expected values to the assertion error for
// display purposes.

function fail(actual, expected, message, operator, stackStartFunction) {
  throw new assert.AssertionError({
    message: message,
    actual: actual,
    expected: expected,
    operator: operator,
    stackStartFunction: stackStartFunction
  });
}

// EXTENSION! allows for well behaved errors defined elsewhere.
assert.fail = fail;

// 4. Pure assertion tests whether a value is truthy, as determined
// by !!guard.
// assert.ok(guard, message_opt);
// This statement is equivalent to assert.equal(true, guard,
// message_opt);. To test strictly for the value true, use
// assert.strictEqual(true, guard, message_opt);.

function ok(value, message) {
  if (!!!value) fail(value, true, message, '==', assert.ok);
}
assert.ok = ok;

// 5. The equality assertion tests shallow, coercive equality with
// ==.
// assert.equal(actual, expected, message_opt);

assert.equal = function equal(actual, expected, message) {
  if (actual != expected) fail(actual, expected, message, '==', assert.equal);
};

// 6. The non-equality assertion tests for whether two objects are not equal
// with != assert.notEqual(actual, expected, message_opt);

assert.notEqual = function notEqual(actual, expected, message) {
  if (actual == expected) {
    fail(actual, expected, message, '!=', assert.notEqual);
  }
};

// 7. The equivalence assertion tests a deep equality relation.
// assert.deepEqual(actual, expected, message_opt);

assert.deepEqual = function deepEqual(actual, expected, message) {
  if (!_deepEqual(actual, expected)) {
    fail(actual, expected, message, 'deepEqual', assert.deepEqual);
  }
};

function _deepEqual(actual, expected) {
  // 7.1. All identical values are equivalent, as determined by ===.
  if (actual === expected) {
    return true;

  } else if (Buffer.isBuffer(actual) && Buffer.isBuffer(expected)) {
    if (actual.length != expected.length) return false;

    for (var i = 0; i < actual.length; i++) {
      if (actual[i] !== expected[i]) return false;
    }

    return true;

  // 7.2. If the expected value is a Date object, the actual value is
  // equivalent if it is also a Date object that refers to the same time.
  } else if (actual instanceof Date && expected instanceof Date) {
    return actual.getTime() === expected.getTime();

  // 7.3. Other pairs that do not both pass typeof value == 'object',
  // equivalence is determined by ==.
  } else if (typeof actual != 'object' && typeof expected != 'object') {
    return actual == expected;

  // 7.4. For all other Object pairs, including Array objects, equivalence is
  // determined by having the same number of owned properties (as verified
  // with Object.prototype.hasOwnProperty.call), the same set of keys
  // (although not necessarily the same order), equivalent values for every
  // corresponding key, and an identical 'prototype' property. Note: this
  // accounts for both named and indexed properties on Arrays.
  } else {
    return objEquiv(actual, expected);
  }
}

function isUndefinedOrNull(value) {
  return value === null || value === undefined;
}

function isArguments(object) {
  return Object.prototype.toString.call(object) == '[object Arguments]';
}

function objEquiv(a, b) {
  if (isUndefinedOrNull(a) || isUndefinedOrNull(b))
    return false;
  // an identical 'prototype' property.
  if (a.prototype !== b.prototype) return false;
  //~~~I've managed to break Object.keys through screwy arguments passing.
  //   Converting to array solves the problem.
  if (isArguments(a)) {
    if (!isArguments(b)) {
      return false;
    }
    a = pSlice.call(a);
    b = pSlice.call(b);
    return _deepEqual(a, b);
  }
  try {
    var ka = objectKeys(a),
        kb = objectKeys(b),
        key, i;
  } catch (e) {//happens when one is a string literal and the other isn't
    return false;
  }
  // having the same number of owned properties (keys incorporates
  // hasOwnProperty)
  if (ka.length != kb.length)
    return false;
  //the same set of keys (although not necessarily the same order),
  ka.sort();
  kb.sort();
  //~~~cheap key test
  for (i = ka.length - 1; i >= 0; i--) {
    if (ka[i] != kb[i])
      return false;
  }
  //equivalent values for every corresponding key, and
  //~~~possibly expensive deep test
  for (i = ka.length - 1; i >= 0; i--) {
    key = ka[i];
    if (!_deepEqual(a[key], b[key])) return false;
  }
  return true;
}

// 8. The non-equivalence assertion tests for any deep inequality.
// assert.notDeepEqual(actual, expected, message_opt);

assert.notDeepEqual = function notDeepEqual(actual, expected, message) {
  if (_deepEqual(actual, expected)) {
    fail(actual, expected, message, 'notDeepEqual', assert.notDeepEqual);
  }
};

// 9. The strict equality assertion tests strict equality, as determined by ===.
// assert.strictEqual(actual, expected, message_opt);

assert.strictEqual = function strictEqual(actual, expected, message) {
  if (actual !== expected) {
    fail(actual, expected, message, '===', assert.strictEqual);
  }
};

// 10. The strict non-equality assertion tests for strict inequality, as
// determined by !==.  assert.notStrictEqual(actual, expected, message_opt);

assert.notStrictEqual = function notStrictEqual(actual, expected, message) {
  if (actual === expected) {
    fail(actual, expected, message, '!==', assert.notStrictEqual);
  }
};

function expectedException(actual, expected) {
  if (!actual || !expected) {
    return false;
  }

  if (expected instanceof RegExp) {
    return expected.test(actual);
  } else if (actual instanceof expected) {
    return true;
  } else if (expected.call({}, actual) === true) {
    return true;
  }

  return false;
}

function _throws(shouldThrow, block, expected, message) {
  var actual;

  if (typeof expected === 'string') {
    message = expected;
    expected = null;
  }

  try {
    block();
  } catch (e) {
    actual = e;
  }

  message = (expected && expected.name ? ' (' + expected.name + ').' : '.') +
            (message ? ' ' + message : '.');

  if (shouldThrow && !actual) {
    fail('Missing expected exception' + message);
  }

  if (!shouldThrow && expectedException(actual, expected)) {
    fail('Got unwanted exception' + message);
  }

  if ((shouldThrow && actual && expected &&
      !expectedException(actual, expected)) || (!shouldThrow && actual)) {
    throw actual;
  }
}

// 11. Expected to throw an error:
// assert.throws(block, Error_opt, message_opt);

assert.throws = function(block, /*optional*/error, /*optional*/message) {
  _throws.apply(this, [true].concat(pSlice.call(arguments)));
};

// EXTENSION! This is annoying to write outside this module.
assert.doesNotThrow = function(block, /*optional*/error, /*optional*/message) {
  _throws.apply(this, [false].concat(pSlice.call(arguments)));
};

assert.ifError = function(err) { if (err) {throw err;}};

})()
},{"util":4,"buffer":16}],17:[function(require,module,exports){
exports.readIEEE754 = function(buffer, offset, isBE, mLen, nBytes) {
  var e, m,
      eLen = nBytes * 8 - mLen - 1,
      eMax = (1 << eLen) - 1,
      eBias = eMax >> 1,
      nBits = -7,
      i = isBE ? 0 : (nBytes - 1),
      d = isBE ? 1 : -1,
      s = buffer[offset + i];

  i += d;

  e = s & ((1 << (-nBits)) - 1);
  s >>= (-nBits);
  nBits += eLen;
  for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8);

  m = e & ((1 << (-nBits)) - 1);
  e >>= (-nBits);
  nBits += mLen;
  for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8);

  if (e === 0) {
    e = 1 - eBias;
  } else if (e === eMax) {
    return m ? NaN : ((s ? -1 : 1) * Infinity);
  } else {
    m = m + Math.pow(2, mLen);
    e = e - eBias;
  }
  return (s ? -1 : 1) * m * Math.pow(2, e - mLen);
};

exports.writeIEEE754 = function(buffer, value, offset, isBE, mLen, nBytes) {
  var e, m, c,
      eLen = nBytes * 8 - mLen - 1,
      eMax = (1 << eLen) - 1,
      eBias = eMax >> 1,
      rt = (mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0),
      i = isBE ? (nBytes - 1) : 0,
      d = isBE ? -1 : 1,
      s = value < 0 || (value === 0 && 1 / value < 0) ? 1 : 0;

  value = Math.abs(value);

  if (isNaN(value) || value === Infinity) {
    m = isNaN(value) ? 1 : 0;
    e = eMax;
  } else {
    e = Math.floor(Math.log(value) / Math.LN2);
    if (value * (c = Math.pow(2, -e)) < 1) {
      e--;
      c *= 2;
    }
    if (e + eBias >= 1) {
      value += rt / c;
    } else {
      value += rt * Math.pow(2, 1 - eBias);
    }
    if (value * c >= 2) {
      e++;
      c /= 2;
    }

    if (e + eBias >= eMax) {
      m = 0;
      e = eMax;
    } else if (e + eBias >= 1) {
      m = (value * c - 1) * Math.pow(2, mLen);
      e = e + eBias;
    } else {
      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen);
      e = 0;
    }
  }

  for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8);

  e = (e << mLen) | m;
  eLen += mLen;
  for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8);

  buffer[offset + i - d] |= s * 128;
};

},{}],16:[function(require,module,exports){
(function(){var assert = require('assert');
exports.Buffer = Buffer;
exports.SlowBuffer = Buffer;
Buffer.poolSize = 8192;
exports.INSPECT_MAX_BYTES = 50;

function Buffer(subject, encoding, offset) {
  if (!(this instanceof Buffer)) {
    return new Buffer(subject, encoding, offset);
  }
  this.parent = this;
  this.offset = 0;

  var type;

  // Are we slicing?
  if (typeof offset === 'number') {
    this.length = coerce(encoding);
    this.offset = offset;
  } else {
    // Find the length
    switch (type = typeof subject) {
      case 'number':
        this.length = coerce(subject);
        break;

      case 'string':
        this.length = Buffer.byteLength(subject, encoding);
        break;

      case 'object': // Assume object is an array
        this.length = coerce(subject.length);
        break;

      default:
        throw new Error('First argument needs to be a number, ' +
                        'array or string.');
    }

    // Treat array-ish objects as a byte array.
    if (isArrayIsh(subject)) {
      for (var i = 0; i < this.length; i++) {
        if (subject instanceof Buffer) {
          this[i] = subject.readUInt8(i);
        }
        else {
          this[i] = subject[i];
        }
      }
    } else if (type == 'string') {
      // We are a string
      this.length = this.write(subject, 0, encoding);
    } else if (type === 'number') {
      for (var i = 0; i < this.length; i++) {
        this[i] = 0;
      }
    }
  }
}

Buffer.prototype.get = function get(i) {
  if (i < 0 || i >= this.length) throw new Error('oob');
  return this[i];
};

Buffer.prototype.set = function set(i, v) {
  if (i < 0 || i >= this.length) throw new Error('oob');
  return this[i] = v;
};

Buffer.byteLength = function (str, encoding) {
  switch (encoding || "utf8") {
    case 'hex':
      return str.length / 2;

    case 'utf8':
    case 'utf-8':
      return utf8ToBytes(str).length;

    case 'ascii':
    case 'binary':
      return str.length;

    case 'base64':
      return base64ToBytes(str).length;

    default:
      throw new Error('Unknown encoding');
  }
};

Buffer.prototype.utf8Write = function (string, offset, length) {
  var bytes, pos;
  return Buffer._charsWritten =  blitBuffer(utf8ToBytes(string), this, offset, length);
};

Buffer.prototype.asciiWrite = function (string, offset, length) {
  var bytes, pos;
  return Buffer._charsWritten =  blitBuffer(asciiToBytes(string), this, offset, length);
};

Buffer.prototype.binaryWrite = Buffer.prototype.asciiWrite;

Buffer.prototype.base64Write = function (string, offset, length) {
  var bytes, pos;
  return Buffer._charsWritten = blitBuffer(base64ToBytes(string), this, offset, length);
};

Buffer.prototype.base64Slice = function (start, end) {
  var bytes = Array.prototype.slice.apply(this, arguments)
  return require("base64-js").fromByteArray(bytes);
};

Buffer.prototype.utf8Slice = function () {
  var bytes = Array.prototype.slice.apply(this, arguments);
  var res = "";
  var tmp = "";
  var i = 0;
  while (i < bytes.length) {
    if (bytes[i] <= 0x7F) {
      res += decodeUtf8Char(tmp) + String.fromCharCode(bytes[i]);
      tmp = "";
    } else
      tmp += "%" + bytes[i].toString(16);

    i++;
  }

  return res + decodeUtf8Char(tmp);
}

Buffer.prototype.asciiSlice = function () {
  var bytes = Array.prototype.slice.apply(this, arguments);
  var ret = "";
  for (var i = 0; i < bytes.length; i++)
    ret += String.fromCharCode(bytes[i]);
  return ret;
}

Buffer.prototype.binarySlice = Buffer.prototype.asciiSlice;

Buffer.prototype.inspect = function() {
  var out = [],
      len = this.length;
  for (var i = 0; i < len; i++) {
    out[i] = toHex(this[i]);
    if (i == exports.INSPECT_MAX_BYTES) {
      out[i + 1] = '...';
      break;
    }
  }
  return '<Buffer ' + out.join(' ') + '>';
};


Buffer.prototype.hexSlice = function(start, end) {
  var len = this.length;

  if (!start || start < 0) start = 0;
  if (!end || end < 0 || end > len) end = len;

  var out = '';
  for (var i = start; i < end; i++) {
    out += toHex(this[i]);
  }
  return out;
};


Buffer.prototype.toString = function(encoding, start, end) {
  encoding = String(encoding || 'utf8').toLowerCase();
  start = +start || 0;
  if (typeof end == 'undefined') end = this.length;

  // Fastpath empty strings
  if (+end == start) {
    return '';
  }

  switch (encoding) {
    case 'hex':
      return this.hexSlice(start, end);

    case 'utf8':
    case 'utf-8':
      return this.utf8Slice(start, end);

    case 'ascii':
      return this.asciiSlice(start, end);

    case 'binary':
      return this.binarySlice(start, end);

    case 'base64':
      return this.base64Slice(start, end);

    case 'ucs2':
    case 'ucs-2':
      return this.ucs2Slice(start, end);

    default:
      throw new Error('Unknown encoding');
  }
};


Buffer.prototype.hexWrite = function(string, offset, length) {
  offset = +offset || 0;
  var remaining = this.length - offset;
  if (!length) {
    length = remaining;
  } else {
    length = +length;
    if (length > remaining) {
      length = remaining;
    }
  }

  // must be an even number of digits
  var strLen = string.length;
  if (strLen % 2) {
    throw new Error('Invalid hex string');
  }
  if (length > strLen / 2) {
    length = strLen / 2;
  }
  for (var i = 0; i < length; i++) {
    var byte = parseInt(string.substr(i * 2, 2), 16);
    if (isNaN(byte)) throw new Error('Invalid hex string');
    this[offset + i] = byte;
  }
  Buffer._charsWritten = i * 2;
  return i;
};


Buffer.prototype.write = function(string, offset, length, encoding) {
  // Support both (string, offset, length, encoding)
  // and the legacy (string, encoding, offset, length)
  if (isFinite(offset)) {
    if (!isFinite(length)) {
      encoding = length;
      length = undefined;
    }
  } else {  // legacy
    var swap = encoding;
    encoding = offset;
    offset = length;
    length = swap;
  }

  offset = +offset || 0;
  var remaining = this.length - offset;
  if (!length) {
    length = remaining;
  } else {
    length = +length;
    if (length > remaining) {
      length = remaining;
    }
  }
  encoding = String(encoding || 'utf8').toLowerCase();

  switch (encoding) {
    case 'hex':
      return this.hexWrite(string, offset, length);

    case 'utf8':
    case 'utf-8':
      return this.utf8Write(string, offset, length);

    case 'ascii':
      return this.asciiWrite(string, offset, length);

    case 'binary':
      return this.binaryWrite(string, offset, length);

    case 'base64':
      return this.base64Write(string, offset, length);

    case 'ucs2':
    case 'ucs-2':
      return this.ucs2Write(string, offset, length);

    default:
      throw new Error('Unknown encoding');
  }
};


// slice(start, end)
Buffer.prototype.slice = function(start, end) {
  if (end === undefined) end = this.length;

  if (end > this.length) {
    throw new Error('oob');
  }
  if (start > end) {
    throw new Error('oob');
  }

  return new Buffer(this, end - start, +start);
};

// copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)
Buffer.prototype.copy = function(target, target_start, start, end) {
  var source = this;
  start || (start = 0);
  if (end === undefined || isNaN(end)) {
    end = this.length;
  }
  target_start || (target_start = 0);

  if (end < start) throw new Error('sourceEnd < sourceStart');

  // Copy 0 bytes; we're done
  if (end === start) return 0;
  if (target.length == 0 || source.length == 0) return 0;

  if (target_start < 0 || target_start >= target.length) {
    throw new Error('targetStart out of bounds');
  }

  if (start < 0 || start >= source.length) {
    throw new Error('sourceStart out of bounds');
  }

  if (end < 0 || end > source.length) {
    throw new Error('sourceEnd out of bounds');
  }

  // Are we oob?
  if (end > this.length) {
    end = this.length;
  }

  if (target.length - target_start < end - start) {
    end = target.length - target_start + start;
  }

  var temp = [];
  for (var i=start; i<end; i++) {
    assert.ok(typeof this[i] !== 'undefined', "copying undefined buffer bytes!");
    temp.push(this[i]);
  }

  for (var i=target_start; i<target_start+temp.length; i++) {
    target[i] = temp[i-target_start];
  }
};

// fill(value, start=0, end=buffer.length)
Buffer.prototype.fill = function fill(value, start, end) {
  value || (value = 0);
  start || (start = 0);
  end || (end = this.length);

  if (typeof value === 'string') {
    value = value.charCodeAt(0);
  }
  if (!(typeof value === 'number') || isNaN(value)) {
    throw new Error('value is not a number');
  }

  if (end < start) throw new Error('end < start');

  // Fill 0 bytes; we're done
  if (end === start) return 0;
  if (this.length == 0) return 0;

  if (start < 0 || start >= this.length) {
    throw new Error('start out of bounds');
  }

  if (end < 0 || end > this.length) {
    throw new Error('end out of bounds');
  }

  for (var i = start; i < end; i++) {
    this[i] = value;
  }
}

// Static methods
Buffer.isBuffer = function isBuffer(b) {
  return b instanceof Buffer || b instanceof Buffer;
};

Buffer.concat = function (list, totalLength) {
  if (!isArray(list)) {
    throw new Error("Usage: Buffer.concat(list, [totalLength])\n \
      list should be an Array.");
  }

  if (list.length === 0) {
    return new Buffer(0);
  } else if (list.length === 1) {
    return list[0];
  }

  if (typeof totalLength !== 'number') {
    totalLength = 0;
    for (var i = 0; i < list.length; i++) {
      var buf = list[i];
      totalLength += buf.length;
    }
  }

  var buffer = new Buffer(totalLength);
  var pos = 0;
  for (var i = 0; i < list.length; i++) {
    var buf = list[i];
    buf.copy(buffer, pos);
    pos += buf.length;
  }
  return buffer;
};

// helpers

function coerce(length) {
  // Coerce length to a number (possibly NaN), round up
  // in case it's fractional (e.g. 123.456) then do a
  // double negate to coerce a NaN to 0. Easy, right?
  length = ~~Math.ceil(+length);
  return length < 0 ? 0 : length;
}

function isArray(subject) {
  return (Array.isArray ||
    function(subject){
      return {}.toString.apply(subject) == '[object Array]'
    })
    (subject)
}

function isArrayIsh(subject) {
  return isArray(subject) || Buffer.isBuffer(subject) ||
         subject && typeof subject === 'object' &&
         typeof subject.length === 'number';
}

function toHex(n) {
  if (n < 16) return '0' + n.toString(16);
  return n.toString(16);
}

function utf8ToBytes(str) {
  var byteArray = [];
  for (var i = 0; i < str.length; i++)
    if (str.charCodeAt(i) <= 0x7F)
      byteArray.push(str.charCodeAt(i));
    else {
      var h = encodeURIComponent(str.charAt(i)).substr(1).split('%');
      for (var j = 0; j < h.length; j++)
        byteArray.push(parseInt(h[j], 16));
    }

  return byteArray;
}

function asciiToBytes(str) {
  var byteArray = []
  for (var i = 0; i < str.length; i++ )
    // Node's code seems to be doing this and not & 0x7F..
    byteArray.push( str.charCodeAt(i) & 0xFF );

  return byteArray;
}

function base64ToBytes(str) {
  return require("base64-js").toByteArray(str);
}

function blitBuffer(src, dst, offset, length) {
  var pos, i = 0;
  while (i < length) {
    if ((i+offset >= dst.length) || (i >= src.length))
      break;

    dst[i + offset] = src[i];
    i++;
  }
  return i;
}

function decodeUtf8Char(str) {
  try {
    return decodeURIComponent(str);
  } catch (err) {
    return String.fromCharCode(0xFFFD); // UTF 8 invalid char
  }
}

// read/write bit-twiddling

Buffer.prototype.readUInt8 = function(offset, noAssert) {
  var buffer = this;

  if (!noAssert) {
    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset < buffer.length,
        'Trying to read beyond buffer length');
  }

  if (offset >= buffer.length) return;

  return buffer[offset];
};

function readUInt16(buffer, offset, isBigEndian, noAssert) {
  var val = 0;


  if (!noAssert) {
    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 1 < buffer.length,
        'Trying to read beyond buffer length');
  }

  if (offset >= buffer.length) return 0;

  if (isBigEndian) {
    val = buffer[offset] << 8;
    if (offset + 1 < buffer.length) {
      val |= buffer[offset + 1];
    }
  } else {
    val = buffer[offset];
    if (offset + 1 < buffer.length) {
      val |= buffer[offset + 1] << 8;
    }
  }

  return val;
}

Buffer.prototype.readUInt16LE = function(offset, noAssert) {
  return readUInt16(this, offset, false, noAssert);
};

Buffer.prototype.readUInt16BE = function(offset, noAssert) {
  return readUInt16(this, offset, true, noAssert);
};

function readUInt32(buffer, offset, isBigEndian, noAssert) {
  var val = 0;

  if (!noAssert) {
    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 3 < buffer.length,
        'Trying to read beyond buffer length');
  }

  if (offset >= buffer.length) return 0;

  if (isBigEndian) {
    if (offset + 1 < buffer.length)
      val = buffer[offset + 1] << 16;
    if (offset + 2 < buffer.length)
      val |= buffer[offset + 2] << 8;
    if (offset + 3 < buffer.length)
      val |= buffer[offset + 3];
    val = val + (buffer[offset] << 24 >>> 0);
  } else {
    if (offset + 2 < buffer.length)
      val = buffer[offset + 2] << 16;
    if (offset + 1 < buffer.length)
      val |= buffer[offset + 1] << 8;
    val |= buffer[offset];
    if (offset + 3 < buffer.length)
      val = val + (buffer[offset + 3] << 24 >>> 0);
  }

  return val;
}

Buffer.prototype.readUInt32LE = function(offset, noAssert) {
  return readUInt32(this, offset, false, noAssert);
};

Buffer.prototype.readUInt32BE = function(offset, noAssert) {
  return readUInt32(this, offset, true, noAssert);
};


/*
 * Signed integer types, yay team! A reminder on how two's complement actually
 * works. The first bit is the signed bit, i.e. tells us whether or not the
 * number should be positive or negative. If the two's complement value is
 * positive, then we're done, as it's equivalent to the unsigned representation.
 *
 * Now if the number is positive, you're pretty much done, you can just leverage
 * the unsigned translations and return those. Unfortunately, negative numbers
 * aren't quite that straightforward.
 *
 * At first glance, one might be inclined to use the traditional formula to
 * translate binary numbers between the positive and negative values in two's
 * complement. (Though it doesn't quite work for the most negative value)
 * Mainly:
 *  - invert all the bits
 *  - add one to the result
 *
 * Of course, this doesn't quite work in Javascript. Take for example the value
 * of -128. This could be represented in 16 bits (big-endian) as 0xff80. But of
 * course, Javascript will do the following:
 *
 * > ~0xff80
 * -65409
 *
 * Whoh there, Javascript, that's not quite right. But wait, according to
 * Javascript that's perfectly correct. When Javascript ends up seeing the
 * constant 0xff80, it has no notion that it is actually a signed number. It
 * assumes that we've input the unsigned value 0xff80. Thus, when it does the
 * binary negation, it casts it into a signed value, (positive 0xff80). Then
 * when you perform binary negation on that, it turns it into a negative number.
 *
 * Instead, we're going to have to use the following general formula, that works
 * in a rather Javascript friendly way. I'm glad we don't support this kind of
 * weird numbering scheme in the kernel.
 *
 * (BIT-MAX - (unsigned)val + 1) * -1
 *
 * The astute observer, may think that this doesn't make sense for 8-bit numbers
 * (really it isn't necessary for them). However, when you get 16-bit numbers,
 * you do. Let's go back to our prior example and see how this will look:
 *
 * (0xffff - 0xff80 + 1) * -1
 * (0x007f + 1) * -1
 * (0x0080) * -1
 */
Buffer.prototype.readInt8 = function(offset, noAssert) {
  var buffer = this;
  var neg;

  if (!noAssert) {
    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset < buffer.length,
        'Trying to read beyond buffer length');
  }

  if (offset >= buffer.length) return;

  neg = buffer[offset] & 0x80;
  if (!neg) {
    return (buffer[offset]);
  }

  return ((0xff - buffer[offset] + 1) * -1);
};

function readInt16(buffer, offset, isBigEndian, noAssert) {
  var neg, val;

  if (!noAssert) {
    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 1 < buffer.length,
        'Trying to read beyond buffer length');
  }

  val = readUInt16(buffer, offset, isBigEndian, noAssert);
  neg = val & 0x8000;
  if (!neg) {
    return val;
  }

  return (0xffff - val + 1) * -1;
}

Buffer.prototype.readInt16LE = function(offset, noAssert) {
  return readInt16(this, offset, false, noAssert);
};

Buffer.prototype.readInt16BE = function(offset, noAssert) {
  return readInt16(this, offset, true, noAssert);
};

function readInt32(buffer, offset, isBigEndian, noAssert) {
  var neg, val;

  if (!noAssert) {
    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 3 < buffer.length,
        'Trying to read beyond buffer length');
  }

  val = readUInt32(buffer, offset, isBigEndian, noAssert);
  neg = val & 0x80000000;
  if (!neg) {
    return (val);
  }

  return (0xffffffff - val + 1) * -1;
}

Buffer.prototype.readInt32LE = function(offset, noAssert) {
  return readInt32(this, offset, false, noAssert);
};

Buffer.prototype.readInt32BE = function(offset, noAssert) {
  return readInt32(this, offset, true, noAssert);
};

function readFloat(buffer, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset + 3 < buffer.length,
        'Trying to read beyond buffer length');
  }

  return require('./buffer_ieee754').readIEEE754(buffer, offset, isBigEndian,
      23, 4);
}

Buffer.prototype.readFloatLE = function(offset, noAssert) {
  return readFloat(this, offset, false, noAssert);
};

Buffer.prototype.readFloatBE = function(offset, noAssert) {
  return readFloat(this, offset, true, noAssert);
};

function readDouble(buffer, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset + 7 < buffer.length,
        'Trying to read beyond buffer length');
  }

  return require('./buffer_ieee754').readIEEE754(buffer, offset, isBigEndian,
      52, 8);
}

Buffer.prototype.readDoubleLE = function(offset, noAssert) {
  return readDouble(this, offset, false, noAssert);
};

Buffer.prototype.readDoubleBE = function(offset, noAssert) {
  return readDouble(this, offset, true, noAssert);
};


/*
 * We have to make sure that the value is a valid integer. This means that it is
 * non-negative. It has no fractional component and that it does not exceed the
 * maximum allowed value.
 *
 *      value           The number to check for validity
 *
 *      max             The maximum value
 */
function verifuint(value, max) {
  assert.ok(typeof (value) == 'number',
      'cannot write a non-number as a number');

  assert.ok(value >= 0,
      'specified a negative value for writing an unsigned value');

  assert.ok(value <= max, 'value is larger than maximum value for type');

  assert.ok(Math.floor(value) === value, 'value has a fractional component');
}

Buffer.prototype.writeUInt8 = function(value, offset, noAssert) {
  var buffer = this;

  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset < buffer.length,
        'trying to write beyond buffer length');

    verifuint(value, 0xff);
  }

  if (offset < buffer.length) {
    buffer[offset] = value;
  }
};

function writeUInt16(buffer, value, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 1 < buffer.length,
        'trying to write beyond buffer length');

    verifuint(value, 0xffff);
  }

  for (var i = 0; i < Math.min(buffer.length - offset, 2); i++) {
    buffer[offset + i] =
        (value & (0xff << (8 * (isBigEndian ? 1 - i : i)))) >>>
            (isBigEndian ? 1 - i : i) * 8;
  }

}

Buffer.prototype.writeUInt16LE = function(value, offset, noAssert) {
  writeUInt16(this, value, offset, false, noAssert);
};

Buffer.prototype.writeUInt16BE = function(value, offset, noAssert) {
  writeUInt16(this, value, offset, true, noAssert);
};

function writeUInt32(buffer, value, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 3 < buffer.length,
        'trying to write beyond buffer length');

    verifuint(value, 0xffffffff);
  }

  for (var i = 0; i < Math.min(buffer.length - offset, 4); i++) {
    buffer[offset + i] =
        (value >>> (isBigEndian ? 3 - i : i) * 8) & 0xff;
  }
}

Buffer.prototype.writeUInt32LE = function(value, offset, noAssert) {
  writeUInt32(this, value, offset, false, noAssert);
};

Buffer.prototype.writeUInt32BE = function(value, offset, noAssert) {
  writeUInt32(this, value, offset, true, noAssert);
};


/*
 * We now move onto our friends in the signed number category. Unlike unsigned
 * numbers, we're going to have to worry a bit more about how we put values into
 * arrays. Since we are only worrying about signed 32-bit values, we're in
 * slightly better shape. Unfortunately, we really can't do our favorite binary
 * & in this system. It really seems to do the wrong thing. For example:
 *
 * > -32 & 0xff
 * 224
 *
 * What's happening above is really: 0xe0 & 0xff = 0xe0. However, the results of
 * this aren't treated as a signed number. Ultimately a bad thing.
 *
 * What we're going to want to do is basically create the unsigned equivalent of
 * our representation and pass that off to the wuint* functions. To do that
 * we're going to do the following:
 *
 *  - if the value is positive
 *      we can pass it directly off to the equivalent wuint
 *  - if the value is negative
 *      we do the following computation:
 *         mb + val + 1, where
 *         mb   is the maximum unsigned value in that byte size
 *         val  is the Javascript negative integer
 *
 *
 * As a concrete value, take -128. In signed 16 bits this would be 0xff80. If
 * you do out the computations:
 *
 * 0xffff - 128 + 1
 * 0xffff - 127
 * 0xff80
 *
 * You can then encode this value as the signed version. This is really rather
 * hacky, but it should work and get the job done which is our goal here.
 */

/*
 * A series of checks to make sure we actually have a signed 32-bit number
 */
function verifsint(value, max, min) {
  assert.ok(typeof (value) == 'number',
      'cannot write a non-number as a number');

  assert.ok(value <= max, 'value larger than maximum allowed value');

  assert.ok(value >= min, 'value smaller than minimum allowed value');

  assert.ok(Math.floor(value) === value, 'value has a fractional component');
}

function verifIEEE754(value, max, min) {
  assert.ok(typeof (value) == 'number',
      'cannot write a non-number as a number');

  assert.ok(value <= max, 'value larger than maximum allowed value');

  assert.ok(value >= min, 'value smaller than minimum allowed value');
}

Buffer.prototype.writeInt8 = function(value, offset, noAssert) {
  var buffer = this;

  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset < buffer.length,
        'Trying to write beyond buffer length');

    verifsint(value, 0x7f, -0x80);
  }

  if (value >= 0) {
    buffer.writeUInt8(value, offset, noAssert);
  } else {
    buffer.writeUInt8(0xff + value + 1, offset, noAssert);
  }
};

function writeInt16(buffer, value, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 1 < buffer.length,
        'Trying to write beyond buffer length');

    verifsint(value, 0x7fff, -0x8000);
  }

  if (value >= 0) {
    writeUInt16(buffer, value, offset, isBigEndian, noAssert);
  } else {
    writeUInt16(buffer, 0xffff + value + 1, offset, isBigEndian, noAssert);
  }
}

Buffer.prototype.writeInt16LE = function(value, offset, noAssert) {
  writeInt16(this, value, offset, false, noAssert);
};

Buffer.prototype.writeInt16BE = function(value, offset, noAssert) {
  writeInt16(this, value, offset, true, noAssert);
};

function writeInt32(buffer, value, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 3 < buffer.length,
        'Trying to write beyond buffer length');

    verifsint(value, 0x7fffffff, -0x80000000);
  }

  if (value >= 0) {
    writeUInt32(buffer, value, offset, isBigEndian, noAssert);
  } else {
    writeUInt32(buffer, 0xffffffff + value + 1, offset, isBigEndian, noAssert);
  }
}

Buffer.prototype.writeInt32LE = function(value, offset, noAssert) {
  writeInt32(this, value, offset, false, noAssert);
};

Buffer.prototype.writeInt32BE = function(value, offset, noAssert) {
  writeInt32(this, value, offset, true, noAssert);
};

function writeFloat(buffer, value, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 3 < buffer.length,
        'Trying to write beyond buffer length');

    verifIEEE754(value, 3.4028234663852886e+38, -3.4028234663852886e+38);
  }

  require('./buffer_ieee754').writeIEEE754(buffer, value, offset, isBigEndian,
      23, 4);
}

Buffer.prototype.writeFloatLE = function(value, offset, noAssert) {
  writeFloat(this, value, offset, false, noAssert);
};

Buffer.prototype.writeFloatBE = function(value, offset, noAssert) {
  writeFloat(this, value, offset, true, noAssert);
};

function writeDouble(buffer, value, offset, isBigEndian, noAssert) {
  if (!noAssert) {
    assert.ok(value !== undefined && value !== null,
        'missing value');

    assert.ok(typeof (isBigEndian) === 'boolean',
        'missing or invalid endian');

    assert.ok(offset !== undefined && offset !== null,
        'missing offset');

    assert.ok(offset + 7 < buffer.length,
        'Trying to write beyond buffer length');

    verifIEEE754(value, 1.7976931348623157E+308, -1.7976931348623157E+308);
  }

  require('./buffer_ieee754').writeIEEE754(buffer, value, offset, isBigEndian,
      52, 8);
}

Buffer.prototype.writeDoubleLE = function(value, offset, noAssert) {
  writeDouble(this, value, offset, false, noAssert);
};

Buffer.prototype.writeDoubleBE = function(value, offset, noAssert) {
  writeDouble(this, value, offset, true, noAssert);
};

})()
},{"assert":15,"./buffer_ieee754":17,"base64-js":18}],18:[function(require,module,exports){
(function (exports) {
	'use strict';

	var lookup = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

	function b64ToByteArray(b64) {
		var i, j, l, tmp, placeHolders, arr;
	
		if (b64.length % 4 > 0) {
			throw 'Invalid string. Length must be a multiple of 4';
		}

		// the number of equal signs (place holders)
		// if there are two placeholders, than the two characters before it
		// represent one byte
		// if there is only one, then the three characters before it represent 2 bytes
		// this is just a cheap hack to not do indexOf twice
		placeHolders = b64.indexOf('=');
		placeHolders = placeHolders > 0 ? b64.length - placeHolders : 0;

		// base64 is 4/3 + up to two characters of the original data
		arr = [];//new Uint8Array(b64.length * 3 / 4 - placeHolders);

		// if there are placeholders, only get up to the last complete 4 chars
		l = placeHolders > 0 ? b64.length - 4 : b64.length;

		for (i = 0, j = 0; i < l; i += 4, j += 3) {
			tmp = (lookup.indexOf(b64[i]) << 18) | (lookup.indexOf(b64[i + 1]) << 12) | (lookup.indexOf(b64[i + 2]) << 6) | lookup.indexOf(b64[i + 3]);
			arr.push((tmp & 0xFF0000) >> 16);
			arr.push((tmp & 0xFF00) >> 8);
			arr.push(tmp & 0xFF);
		}

		if (placeHolders === 2) {
			tmp = (lookup.indexOf(b64[i]) << 2) | (lookup.indexOf(b64[i + 1]) >> 4);
			arr.push(tmp & 0xFF);
		} else if (placeHolders === 1) {
			tmp = (lookup.indexOf(b64[i]) << 10) | (lookup.indexOf(b64[i + 1]) << 4) | (lookup.indexOf(b64[i + 2]) >> 2);
			arr.push((tmp >> 8) & 0xFF);
			arr.push(tmp & 0xFF);
		}

		return arr;
	}

	function uint8ToBase64(uint8) {
		var i,
			extraBytes = uint8.length % 3, // if we have 1 byte left, pad 2 bytes
			output = "",
			temp, length;

		function tripletToBase64 (num) {
			return lookup[num >> 18 & 0x3F] + lookup[num >> 12 & 0x3F] + lookup[num >> 6 & 0x3F] + lookup[num & 0x3F];
		};

		// go through the array every three bytes, we'll deal with trailing stuff later
		for (i = 0, length = uint8.length - extraBytes; i < length; i += 3) {
			temp = (uint8[i] << 16) + (uint8[i + 1] << 8) + (uint8[i + 2]);
			output += tripletToBase64(temp);
		}

		// pad the end with zeros, but make sure to not forget the extra bytes
		switch (extraBytes) {
			case 1:
				temp = uint8[uint8.length - 1];
				output += lookup[temp >> 2];
				output += lookup[(temp << 4) & 0x3F];
				output += '==';
				break;
			case 2:
				temp = (uint8[uint8.length - 2] << 8) + (uint8[uint8.length - 1]);
				output += lookup[temp >> 10];
				output += lookup[(temp >> 4) & 0x3F];
				output += lookup[(temp << 2) & 0x3F];
				output += '=';
				break;
		}

		return output;
	}

	module.exports.toByteArray = b64ToByteArray;
	module.exports.fromByteArray = uint8ToBase64;
}());

},{}]},{},[2])(2)
});
;;
