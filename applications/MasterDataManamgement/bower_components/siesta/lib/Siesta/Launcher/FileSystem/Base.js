/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Launcher.FileSystem.Base', {
    
    methods : {
        
        fileExists : function (fileName) {
            throw new Error("Abstract method call: `fileExists`")
        },
        
        
        readFile : function (fileName) {
            throw new Error("Abstract method call: `readFile`")
        },
        

        saveFile : function (fileName, content) {
            throw new Error("Abstract method call: `saveFile`")
        },
        
        
        copyFile : function (source, dest) {
            throw new Error("Abstract method call: `copyFile`")
        },
        
        
        copyTree : function (source, dest) {
            throw new Error("Abstract method call: `copyTree`")
        },
        
        
        ensurePathExists : function (path) {
            throw new Error("Abstract method call: `ensurePathExists`")
        },
        
        
        normalizePath : function (path) {
            return path
        }
    }
})
