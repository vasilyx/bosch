/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Harness.Browser.Automation.PhantomJS', {
    
    requires    : [ 'getQueryParam' ],
    
    has : {
        isPhantomJS             : false
    },
    
    
    override : {
        
        initialize : function () {
            if (this.getQueryParam('phantom') != null) {
                this.isAutomated        = true
                this.isPhantomJS        = true
            }
            
            this.SUPERARG(arguments)
        }
    }
})