/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Siesta.Launcher.WebDriverServerRhino', {
    
    does        : [
        Siesta.Launcher.CommandLineTool.RhinoTool,
        Siesta.Launcher.CommandLineTool.WebDriverRhinoTool
    ],
    
    has : {
    },
    
    
    methods : {
        
        start : function () {
            // remove the binDir which comes 1st on command line
            this.binDir     = this.args.shift().replace(/\\\/?$/, '')
            
            this.setChromeDriverServerPath()
            this.setIEDriverServerPath()
            
            var threadsBefore   = java.lang.Thread.activeCount()
            
            // bypass all other command line options unchanged
            Packages.org.openqa.grid.selenium.GridLauncher.main(this.args)
            
            if (java.lang.Thread.activeCount() <= threadsBefore) quit()
        },

        
        printVersion : function () {
        }
    }
    // eof methods
})
