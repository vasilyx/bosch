Ext.define('MAM.view.meterPoint.InitialController', {
    extend: 'Ext.app.ViewController',
    requires: [
        'MAM.model.MeterPointDetail',
        'MAM.model.Relation',
        'MAM.model.CustomAttribute'
    ],
    alias: 'controller.meterPointInitialController',
    control: {
        'meterPointInitial meterPointList': {
            itemdblclick: 'onRecordSelect'
        },
        'meterPointInitial': {
            show: 'onShowInitial'
        },
        '#meterPointEditForm': {
            dirtychange: 'onChangeEditForm'
        }
    },
    listen: {
        store: {
            '#meterPoints': {
                load: 'onMeterPointsLoad'
            }
        },
        controller: {
            'actionInitialController': {
                'meterPointDelete': 'onDelete',
                'meterPointEdit': 'onEdit',
                'meterPointSave': 'onSave'
            },
            'relationsInitialController': {
                'meterPointsRelationAdded': 'setDirtyEntity',
                'meterPointsRelationRemoved': 'setDirtyEntity'
            },
            'customAttributesInitialController': {
                'editedCustomAttributes': 'setDirtyEntity'
            },
            '*': {
                'openMeterPointEdit': 'openEdit'
            }
        }
    },
    routes: {
        '/meterPoint/itemId/:id': 'loadDetails'
    },

    onShowInitial: function () {
        this.fireEvent('openEntity', MAM.utils.Constants.METERPOINT, this.getViewModel().get('meterPoint'));
    },

    /**
     * Loads the necessary data for given entity
     * @param rowModel
     * @param record
     */
    onRecordSelect: function (rowModel, record) {
        var me = this;

        if (this.getViewModel().get('editMode')) {
            Ext.create('Energy.common.ui.DialogBox').showQuestion(_tl.get("mam.dialogbox.meterPoint.editDataLost"),
                [
                    {
                        text: _tl.get("mam.dialogbox.meterPoint.button.yes"),
                        itemId: 'buttonPositive',
                        handler: function () {
                            this.up().up().close();
                            me.loadDetails(record.getId());
                            return true;
                        }
                    },
                    {
                        text: _tl.get("mam.dialogbox.meterPoint.button.no"),
                        itemId: 'buttonNegative',
                        handler: function () {
                            this.up().up().close();
                            return false;
                        }
                    }
                ]


            );
        } else {
            this.loadDetails(record.getId());

        }
    },

    openEdit: function (recordId) {
        var me = this;

        this.loadMeterPointDetails(recordId)
            .then(this.loadMeterPointRelation)
            .then(function(response){
                return new Ext.Promise(function (resolve, reject) {
                    me.onDataMeterPointRelationsLoadSuccess(response);
                    resolve(response.getId());
                });
                },
                function(response){
                    return new Ext.Promise(function (resolve, reject) {
                        me.onDataMeterPointRelationsLoadSuccess(response);
                        reject(response);
                    })
                })
            .then(this.loadMeterPointCustomAttribute)
            .then(function(response){
                return new Ext.Promise(function (resolve, reject) {
                    me.onDataMeterPointCustomAttributeLoadSuccess(response);
                    resolve(response.getId());
                })},
                function(response){
                    return new Ext.Promise(function (resolve, reject) {
                        me.onDataMeterPointCustomAttributesLoadError();
                        reject(response);
                    })
            })
            .then(function(response){
                return new Ext.Promise(function (resolve, reject) {
                    me.onEdit();
                    resolve();
                });
            });

        var tabConfig = MAM.utils.Configuration.getTabConfig(MAM.utils.Constants.METERPOINT);
        //check which tabs are configured
        Ext.each(tabConfig, function (config) {
            if (config.type === MAM.utils.Constants.RELATIONSDETAIL) {

            }
            if (config.type === MAM.utils.Constants.CUSTOMATTRIBUTESDETAIL) {

            }
        });
    },

    /**
     * Load entity details,
     * @param id int
     * return void
     */
    loadDetails: function (id) {
        this.fireEvent('toggleLoadingMask', true, _tl.get('mam.label.ui.loading.meterPointDetail'));
        this.getViewModel().set('editMode', false);

        // commented for turn on view mode after edit
        //       if(!this.getViewModel().get('meterPoint')){
        this.getView().createMeterPointDetailContainer();
        //      }

        //TODO: New MAM-xxx ticket
        //we need to get meter point via versions API
        //GET v2/meterPoints/{id}/versions?expand=all&activeAt={YYYY-MM-dd'T'HH:mm:ss.SSZ}
        MAM.model.MeterPointDetail.load(id, {
            success: this.onDataMeterPointLoadSuccess,
            failure: this.onDataMeterPointLoadError,
            scope: this
        });
    },

    /**
     * Get the total on meter points load
     * @param store
     * @param records
     */
    onMeterPointsLoad: function (store, records) {
        this.getViewModel().set('total', store.getTotalCount());
    },

    loadMeterPointDetails: function (id) {
        var me = this;
        return new Ext.Promise(function (resolve, reject) {
            MAM.model.MeterPointDetail.load(id,
                {
                    success: function (recordDetail) {
                        var viewModel = me.getViewModel();
                        viewModel.set('meterPoint', recordDetail);
                        me.fireEvent('detailsLoaded', MAM.utils.Constants.METERPOINT);
                        me.fireEvent('openEntity', MAM.utils.Constants.METERPOINT, recordDetail);

                        resolve(recordDetail);
                    },
                    failure: function (response) {
                        me.onDataMeterPointLoadError();
                        reject(response);
                    }
                })
        });
    },

    loadMeterPointRelation: function (record) {
        var id = record.getId();
        MAM.model.Relation.getProxy().setEntity(MAM.utils.Constants.METERPOINT);

        return new Ext.Promise(function (resolve, reject) {
            MAM.model.Relation.load(id,
                {
                    success: function (response) {
                        resolve(response);
                    },
                    failure: function (response) {
                        reject(response);
                    }
                });
        })
    },
    loadMeterPointCustomAttribute: function (id) {
        MAM.model.CustomAttribute.getProxy().setEntity(MAM.utils.Constants.METERPOINTS);

        return new Ext.Promise(function (resolve, reject) {
            MAM.model.CustomAttribute.load(id,
                {
                    success: function (response) {
                        resolve(response);
                    }
                    ,
                    failure: function (response) {
                     //   me.onDataMeterPointCustomAttributesLoadError();
                        reject(response);
                    }
                });
        });
    },

    /**
     * Loads relations and custom attributes after sucessfull
     * meter point load
     * @param recordDetail
     */
    onDataMeterPointLoadSuccess: function (recordDetail) {
        var me = this;
        var viewModel = this.getViewModel();
        viewModel.set('meterPoint', recordDetail);

        this.fireEvent('detailsLoaded', MAM.utils.Constants.METERPOINT);
        this.fireEvent('openEntity', MAM.utils.Constants.METERPOINT, recordDetail);

        var tabConfig = MAM.utils.Configuration.getTabConfig(MAM.utils.Constants.METERPOINT);
        //check which tabs are configured
        Ext.each(tabConfig, function (config) {
            if (config.type === MAM.utils.Constants.RELATIONSDETAIL) {
                //load meter point relations
                MAM.model.Relation.getProxy().setEntity(MAM.utils.Constants.METERPOINT);
                MAM.model.Relation.getProxy().on('exception', me.onDataMeterPointRelationsLoadError, me);

                MAM.model.Relation.load(recordDetail.getId(), {
                    success: me.onDataMeterPointRelationsLoadSuccess,
                    failure: me.onDataMeterPointRelationsLoadError,
                    scope: me
                });
            }
            if (config.type === MAM.utils.Constants.CUSTOMATTRIBUTESDETAIL) {

                //load meter point customAttributes
                MAM.model.CustomAttribute.getProxy().setEntity(MAM.utils.Constants.METERPOINTS);
                MAM.model.CustomAttribute.getProxy().on('exception', me.onDataMeterPointCustomAttributesLoadError, me);
                MAM.model.CustomAttribute.load(recordDetail.getId(), {
                    success: me.onDataMeterPointCustomAttributeLoadSuccess,
                    failure: me.onDataMeterPointCustomAttributesLoadError,
                    scope: me
                });
            }
        });
    }
    ,
    /**
     * Creates a realationschange event when meter point relations
     * are sucessull loeaded
     * @param recordDetail
     */
    onDataMeterPointRelationsLoadSuccess: function (recordDetail) {
        var viewModel = this.getViewModel();
        viewModel.set('relations', recordDetail);
        viewModel.get('meterPoint').set('relations', recordDetail);
        this.fireEvent('relationschange', recordDetail, MAM.utils.Constants.METERPOINTS);
        this.fireEvent('toggleLoadingMask', false);
    }
    ,
    /**
     * Creates a customattributechange event when meter point
     * custom attributes are successful loaded
     * @param recordDetail
     */
    onDataMeterPointCustomAttributeLoadSuccess: function (recordDetail) {
        var viewModel = this.getViewModel();
        viewModel.get('meterPoint').set('customAttributes', recordDetail);
        this.fireEvent('customattibuteschange', recordDetail, MAM.utils.Constants.METERPOINTS);

    }
    ,
    /**
     * Meter point cannot be loaded
     * so process no relations and custom attributes error
     * to display empty data
     */
    onDataMeterPointLoadError: function () {
        this.onDataMeterPointRelationsLoadError();
        this.onDataMeterPointCustomAttributesLoadError();
        this.fireEvent('toggleLoadingMask', false);
    }
    ,
    /**
     * relations of meter point can not be loaded
     * fire event relationschange with an empty relation to
     * display empty data
     */
    onDataMeterPointRelationsLoadError: function () {
        var emptyRecord = Ext.create('MAM.model.Relation');
        this.fireEvent('relationschange', emptyRecord, MAM.utils.Constants.METERPOINTS);
        this.fireEvent('toggleLoadingMask', false);
    }
    ,
    /**
     * custom attributes of meter point can not be loaded
     * fire event customattibuteschange with an efmpty relation to
     * display empty data
     */
    onDataMeterPointCustomAttributesLoadError: function () {
        var emptyRecord = Ext.create('MAM.model.CustomAttribute');
        this.fireEvent('customattibuteschange', emptyRecord, MAM.utils.Constants.METERPOINTS);
        this.fireEvent('toggleLoadingMask', false);
    }
    ,
    onDelete: function (record) {
        var me = this;
        var win = Ext.create('Energy.common.ui.DialogBox', {});

        win.showQuestion(Ext.String.format(_tl.get('mam.dialogbox.meterPoint.delete.question'), record.get('customId')), [
            {
                text: _tl.get('mam.dialogbox.meterPoint.button.no'),
                handler: function () {
                    win.close();
                }
            },
            {
                text: _tl.get('mam.dialogbox.meterPoint.button.yes'),
                handler: function () {
                    win.close();

                    record.erase({
                        success: function (request, response) {
                            Ext.getStore('meterPoints').reload();
                            Ext.create('Energy.common.ui.DialogBox').showSuccess(Ext.String.format(_tl.get('mam.dialogbox.meterPoint.delete.response200'), record.get('customId')));
                        },
                        failure: function (request, response) {

                            switch (response.getError().status) {
                                case 404:
                                    Ext.create('Energy.common.ui.DialogBox').showWarning(Ext.String.format(_tl.get('mam.dialogbox.meterPoint.delete.response404'), record.get('customId')));
                                    break;
                                case 409:
                                    Ext.create('Energy.common.ui.DialogBox').showWarning(Ext.String.format(_tl.get('mam.dialogbox.meterPoint.delete.response409'), record.get('customId')));
                                    break;
                            }
                        }
                    });
                }
            }
        ]);
    }
    ,


    onEdit: function (status) {
        this.getViewModel().set('editMode', true);

        if (status === false) {
            var record = this.getViewModel().get('meterPoint');
            this.loadDetails(record.getId());
            return;
        }

        this.getView().createMeterPointEditDetailContainer();
        this.getView().down('#meterPointEditForm').loadRecord(this.getViewModel().get('meterPoint')); // dirtychange doesnt work without it
        this.fireEvent('relationschangeedit', this.getViewModel().get('relations'), MAM.utils.Constants.METERPOINTS, this.getViewModel().get('meterPoint'));
        this.fireEvent('customattibuteschange', this.getViewModel().get('meterPoint').get('customAttributes'), MAM.utils.Constants.METERPOINTS);
    }
    ,

    onSave: function () {
        var me = this;
        var record = this.getViewModel().get('meterPoint');

        record.set(this.getView().down('#meterPointEditForm').getValues());

        var customAttributes = record.get('customAttributes');
        var relations = record.get('relations');

        delete record.data.relations;
        delete record.data.customAttributes;

        var promisesArray = [this._saveEntity(record)];

        customAttributes.customAttributes().getData().each(function (row) {

            var attr = Ext.create('MAM.model.CustomAttribute', {});

            attr.set({
                'customAttributes': [{
                    key: row.get('key'),
                    value: row.get('value')
                }],
                'entity': MAM.utils.Constants.METERPOINTS,
                'id': record.getId()
            });
            attr.phantom = row.phantom;


            if (row.phantom === true) {
                attr.mamRestCreate = true;
            }

            promisesArray.push(me._saveCustomAttributes(attr));
        });

        customAttributes.customAttributes().removed.forEach(function (row) {
            var attr = Ext.create('MAM.model.CustomAttribute', {});
            attr.set({
                key: row.get('key'),
                'entity': MAM.utils.Constants.METERPOINTS,
                'id': record.getId()
            });

            attr.phantom = row.phantom;

            promisesArray.push(me._deleteCustomAttribute(attr));
        });

        var addedRelatitonList = me._getAddedRelationList(relations);

        if (addedRelatitonList) {
            for (key in addedRelatitonList) {

                addedRelatitonList[key].each(function (k) {
                    var relation = Ext.create('MAM.model.Relation', {
                        associatedCompType: 'meterPoint',
                        "relationType": "parent",
                        associatedCompId: record.getId(),
                        "compId": k.getId(),
                        "compType": key.substr(0, key.length - 1) // remove 's
                    });

                    promisesArray.push(me._saveRelation(relation));
                });
            }
        }

        var deletedRelatitonList = me._getDeleteRelationList(relations);

        if (deletedRelatitonList) {
            for (key in deletedRelatitonList) {

                deletedRelatitonList[key].forEach(function (k, v) {
                    var relation = Ext.create('MAM.model.Relation', {});

                    relation.setId(k.id);
                    MAM.model.Relation.getProxy().setEntity(key.substr(0, key.length - 1));
                    relation.phantom = false;

                    promisesArray.push(me._deleteRelation(relation));
                });
            }
        }

       var resultingPromise = Ext.Promise.all(promisesArray);

        resultingPromise
            .then(function () {
                Ext.create('Energy.common.ui.DialogBox').showSuccess(Ext.String.format(_tl.get('mam.dialogbox.meterPoint.update.response200'), record.get('customId')));
                me.fireEvent('setIconEditStatus', MAM.utils.Constants.METERPOINT, true);
                me.fireEvent('setIconSaveStatus', MAM.utils.Constants.METERPOINT, false);
                me.getViewModel().set('editMode', false);
            })
            ['catch'](function (response) {
            switch (response.getError().status) {
                case 400:
                    Ext.create('Energy.common.ui.DialogBox').showWarning(Ext.String.format(_tl.get('mam.dialogbox.meterPoint.update.response404'), record.get('customId')));
                    break;
                case 404:
                    Ext.create('Energy.common.ui.DialogBox').showWarning(Ext.String.format(_tl.get('mam.dialogbox.meterPoint.update.response404'), record.get('customId')));
                    break;
                case 409:
                    Ext.create('Energy.common.ui.DialogBox').showWarning(Ext.String.format(_tl.get('mam.dialogbox.meterPoint.update.response409'), record.get('customId')));
                    break;
            }
            me.fireEvent('setIconEditStatus', MAM.utils.Constants.METERPOINT, true);
            me.fireEvent('setIconSaveStatus', MAM.utils.Constants.METERPOINT, false);
            me.getViewModel().set('editMode', false);
        })/* Dont know how to use _always_ here
        ['always'](function () {
            me.fireEvent('setIconEditStatus', MAM.utils.Constants.METERPOINT, true);
            me.fireEvent('setIconSaveStatus', MAM.utils.Constants.METERPOINT, false);
            this.getViewModel().set('editMode', false);
        });*/
    }
    ,
    /**
     * sends the dirty data
     * @param dirty
     * @param data
     */
    setDirtyEntity: function (dirty, data) {
        if (typeof(dirty) !== "undefined") {
            this.fireEvent('setIconSaveStatus', MAM.utils.Constants.METERPOINT, dirty);
            if(dirty === MAM.utils.Constants.CUSTOMATTRIBUTESDETAIL){
                var mp = this.getViewModel().get('meterPoint');
                if(mp && data.length){
                    mp.get('customAttributes').customAttributes().loadData(data)
                }
            }
        } else {
            var record = this.getViewModel().get('meterPoint');
            var relations = record.get('relations');

            var form = this.getView().down('#meterPointEditForm');

            var addedRelatitonList = this._getAddedRelationList(relations);
            var deletedRelatitonList = this._getDeleteRelationList(relations);

            if ((addedRelatitonList || deletedRelatitonList || form.isDirty()) && form.isValid()) {
                dirty = true;
            } else {
                dirty = false;
            }

            this.fireEvent('setIconSaveStatus', MAM.utils.Constants.METERPOINT, dirty);
        }
    }
    ,

    onChangeEditForm: function () {
        this.setDirtyEntity();
    }
    ,

    _saveEntity: function (record) {
        return new Ext.Promise(function (resolve, reject) {

            record.save({
                success: function (request, response) {
                    resolve();
                },
                failure: function (request, response) {
                    reject(response);
                }
            });
        });
    }
    ,

    _saveCustomAttributes: function (record) {
        return new Ext.Promise(function (resolve, reject) {

            record.save({
                success: function (request, response) {
                    resolve();
                },
                failure: function (request, response) {
                    reject(response);
                }
            });
        });
    }
    ,

    _saveRelation: function (record) {
        return new Ext.Promise(function (resolve, reject) {

            record.save({
                success: function (request, response) {
                    resolve();
                },
                failure: function (request, response) {
                    reject(response);
                }
            });
        });
    }
    ,


    _deleteRelation: function (record) {
        return new Ext.Promise(function (resolve, reject) {
            record.erase({
                success: function (request, response) {
                    resolve();
                },
                failure: function (request, response) {
                    reject(response);
                }
            });
        });
    }
    ,

    _deleteCustomAttribute: function (record) {
        return new Ext.Promise(function (resolve, reject) {
            record.erase({
                success: function (request, response) {
                    resolve();
                },
                failure: function (request, response) {
                    reject(response);
                }
            });
        });
    }
    ,

    _getDeleteRelationList: function (relations) {

        if (!relations) return [];

        var result = {};

        for (key in relations.get('relations')) {

            if (this.getView().query('#relationAccordion grid#' + key).length === 0)  continue;

            var grid = this.getView().query('#relationAccordion grid#' + key)[0];

            relations.get('relations')[key].forEach(function (k, val) {
                if (grid.getStore().find('id', k.id) === -1) {

                    if (!result.hasOwnProperty(key)) result[key] = [];
                    result[key].push(k);
                }
            });
        }

        return result;
    }
    ,

    _getAddedRelationList: function (relations) {

        var relationGridList = this.getView().query('#relationAccordion grid');
        var result = {};

        relationGridList.forEach(function (grid, val) {

            var key = grid.getItemId();

            if (relations && relations.get('relations').hasOwnProperty(key)) {

                var data = grid.getStore().getRange();

                data.forEach(function (dataRow, val) {

                    var setNew = true;

                    relations.get('relations')[key].forEach(function (relRow, val) {
                        if (relRow.id === dataRow.getId()) {
                            setNew = false;
                        }
                    });

                    if (setNew) {
                        if (!result.hasOwnProperty(key)) result[key] = [];
                        result[key].push(dataRow);
                    }
                });

            } else if (grid.getStore().getCount()) {
                if (!result.hasOwnProperty(key)) result[key] = [];
                result[key] = grid.getStore().getData();
            }
        });

        return result;
    }
});