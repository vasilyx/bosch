StartTest(function (t) {

    referenceData = {
        "meterPointId": 1491119,
        "customId": "DE0700081632100000000000070238000"
    };
    var referenceDataDetail = {
        "associatedCompType": "test_undefined",
        "associatedCompId": 1,
        "customId": 607,
        "startTS": "2016-11-15T14:39:29Z",
        "endTS": "9999-12-31T23:59:00Z"
    };


    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/detail_DE0700081632100000000000070238000.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/customAttributes_DE0700081632100000000000070238000.json', referenceData.meterPointId);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/relations_undefined.json', referenceData.meterPointId);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/r/details_1494726.json'); // for related
    MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/r/details_1480156.json'); // for related
    MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/r/details_1035879.json'); // for related

    MOCK_DATA.getMockedDataDetail('tests/data/detail/simCards/1.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/transformers/1.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterAddonDevices/1.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/plants/1.json');

    t.it('#1 Undefined Relation in list', function (t) {
        t.chain(
            {waitForCQ: 'app-main tabbar'},
            {click: 'tabbar=> span.icon-meterPoint'},
            {waitForCQ: 'meterPointList'},
            {waitForRowsVisible: 'meterPointList'},
            function (next) {
                t.doubleClick(t.getFirstRow(t.cq1('meterPointInitial gridpanel')));
                next();
            },
            {waitForCQ: 'meterPointDetail'},
            {waitForComponentVisible: 'meterPointDetail'},
            {
                action: 'wait',
                delay: 1500
            },
            function (next) {
                [
                    MAM.utils.Constants.METERS,
                    MAM.utils.Constants.PERSONS,
                    MAM.utils.Constants.PLANTS,
                    MAM.utils.Constants.METERADDONDEVICES,
                    MAM.utils.Constants.SIMCARDS,
                    MAM.utils.Constants.TRANSFORMERS,
                    MAM.utils.Constants.UNDEFINEDRELATIONS,
                    MAM.utils.Constants.CONNECTIONBUILDINGS
                ].forEach(function (v) {
                    t.elementIsVisible(t.cq1('relationsInitial grid#' + v), 'Tab ' + v + ' is visible');
                });
            }
        )
    });

    t.it('#2 Undefined Relation - view', function (t) {
        t.chain(
            function (next) {
                t.expect(t.cq1('relationsInitial grid#' + MAM.utils.Constants.UNDEFINEDRELATIONS).getStore().getCount()).toBe(2);

                for (var code in referenceDataDetail) {
                    t.contentLike(t.cq1('relationsInitial undefinedDetail displayfield[name="' + code + '"]'), referenceDataDetail[code], 'Found right  #' + code + ' ' + referenceDataDetail[code]);
                }
            }
        )
    });
});