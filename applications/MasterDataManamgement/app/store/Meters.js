Ext.define('MAM.store.Meters', {
    extend: 'Ext.data.Store',
    alias: 'store.meters',
    storeId: 'meters',
    model: 'MAM.model.Meter',
    autoLoad: false,
    pageSize: MAM.Config.List.limit,
    proxy: {
        type: 'ajax',
        url: '/MAM/' + MAM.Config.Backend.Versions + '/meters',
        startParam: 'skip',
        limitParam: 'top',
        extraParams: {
            expand: 'properties',
            totalNumber: true
        },
        headers: {
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty: 'meters',
            totalProperty: 'totalNumber'
        }
    }
});