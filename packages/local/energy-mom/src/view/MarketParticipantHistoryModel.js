Ext.define('Energy.mom.view.MarketParticipantHistoryModel', {
    extend: 'Ext.app.ViewModel',
    requires: ['Energy.mom.store.MarketParticipants'],
    alias: 'viewmodel.marketParticipantHistoryModel',
    data: {
        record: null
    },
    stores: {
        momMarketParticipants: {
            type: 'momMarketParticipants'
        }
    }
});