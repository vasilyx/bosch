/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Siesta.Launcher.Runner.WebDriverNodeJS.BrowserStack', {
    
    isa     : Siesta.Launcher.Runner.WebDriverNodeJS,
    
    does    : [
        Siesta.Launcher.Runner.WebDriverNodeJS.WithTunnel
    ],
    
    has : {
        // increased page poll interval to reduce trafic
        pagePollInterval        : 5000,
        
        tunnelName              : 'BrowserStack'
    },
    
    
    methods : {
        
        onWebDriverInstatiationException : function (e) {
            var str     = String(e)
            
            if (str.indexOf('sessions are currently being used. Please upgrade to add more parallel sessions') != -1) {
                this.warn("Max number of parallel sessions exceeded, reducing --max-workers config by 1")
                
                this.maxWorkers   = Math.max(1, this.maxWorkers - 1)
                
                return false
            }
            
            if (str.indexOf('Browser/Browser_Version not supported') != -1) {
                this.printError("This combination of browser and browser version is not supported")
                
                return false
            }
            
            if (str.indexOf('OS/Browser combination invalid') != -1) {
                this.printError("This combination of OS and browser is not supported")
                
                return false
            }
            
            var match
            
            if (match = /Platform can be one of (.*)/.exec(str)) {
                this.printError("The `--cap platform=value` should be one of " + match[ 1 ])
                
                return false
            }
            
            return this.SUPER(e)
        },
            
            
        buildCapabilities : function () {
            var caps        = this.SUPERARG(arguments)
            var options     = this.dispatcher.options
            
            caps.set('browserstack.debug', options[ 'browserstack-enable-screenshots' ] ? 'true' : 'false')
            
            var tunnelId    = this.tunnelId || options[ 'browserstack-tunnel-identifier' ]
            
            if (tunnelId) {
                caps.set('browserstack.localIdentifier', tunnelId)
            }
            
            return caps
        },
        
        
        // promised method
        setup : function () {
            var me          = this
            
            return this.SUPER().then(function () {
                var launcher    = me.launcher
                var options     = launcher.options
                var bs          = options.bs
                
                if (!bs) throw new Error("This runner should only be used when browserstack is enabled")
                
                if (!bs.shortCut || bs.noTunnel) return me
                
                me.initTunnelId()
                
                var args            = []
                
                args.push('-force') // me switch will close other instances of tunnel
                args.push(bs.key)
                args.push('-localIdentifier'); args.push(me.tunnelId);
                
                if (options[ 'proxy-host' ]) {
                    args.push('-proxyHost'); args.push(options[ 'proxy-host' ])
                    
                    if (options[ 'proxy-port' ]) {
                        args.push('-proxyPort'); args.push(options[ 'proxy-port' ])
                    }
                    if (options[ 'proxy-user' ]) {
                        args.push('-proxyUser'); args.push(options[ 'proxy-user' ])
                    }
                    if (options[ 'proxy-password' ]) {
                        args.push('-proxyPass'); args.push(options[ 'proxy-password' ])
                    }
                }
                
                me.tunnelInfo   = bs.userName
                
                return me.launchTunnel(
                    launcher.binDir + '/binary/browserstack/' + launcher.getPlatformId() + '/BrowserStackLocal' + (launcher.isWindows ? '.exe' : ''),
                    args,
                    undefined,
                    function (line) {
                        if (/Updated/.test(line)) {
                            me.debug(me.tunnelName + " has been updated, restarting")
                            
                            return 'restart'
                        }
                        
                        return /You can now access your local server\(s\) in our remote browser/.test(line) 
                    }
                )
            })
        }
    }
})
