/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Siesta.Launcher.Runner.WebDriverNodeJS.SauceLabs', {
    
    isa     : Siesta.Launcher.Runner.WebDriverNodeJS,
    
    does    : [
        Siesta.Launcher.Runner.WebDriverNodeJS.WithTunnel
    ],
    
    has : {
        // increased page poll interval to reduce trafic
        pagePollInterval        : 5000,
        
        resolutionCapName       : 'screenResolution',
        
        tunnelName              : 'SauceLabs'
    },
    
    
    methods : {
        
        onWebDriverInstatiationException : function (e, builder, capabilities) {
            var str     = String(e)
            var match
            
            if (match = /(Resolution you specified is not available .*)/.exec(str)) {
                var options = this.dispatcher.options
                
                var width   = options.width || Siesta.Launcher.Page.WebDriverNodeJS.prototype.width
                var height  = options.height || Siesta.Launcher.Page.WebDriverNodeJS.prototype.height
                
                this.warn("Requested resolution " + width + 'x' + height + " is not supported for this OS/browser/version/device combo. Retrying w/o requesting any specific resolution.")
                
                capabilities.set(this.resolutionCapName, undefined)
                
                builder.withCapabilities(capabilities)
                
                return true
            }
            
            if (match = /Unsupported OS\/browser\/version\/device combo/.exec(str)) {
                return false
            }
            
            return this.SUPER(e)
        },
        
        
        buildCapabilities : function () {
            var caps        = this.SUPERARG(arguments)
            var options     = this.dispatcher.options
            
            caps.set('recordVideo', options[ 'saucelabs-enable-video' ] ? 'true' : 'false')
            caps.set('recordScreenshots', options[ 'saucelabs-enable-screenshots' ] ? 'true' : 'false')
            
            var tunnelId    = this.tunnelId || options[ 'saucelabs-tunnel-identifier' ]
            
            if (tunnelId) {
                caps.set('tunnelIdentifier', tunnelId)
            }
            
            return caps
        },
            
        
        // promised method
        setup : function () {
            var me          = this
            
            return this.SUPER().then(function () {
                var launcher    = me.launcher
                var options     = launcher.options
                var sl          = options.sl
                
                if (!sl) throw new Error("This runner should only be used when saucelabs is enabled")
            
                if (!sl.shortCut || sl.noTunnel) return me
                
                me.initTunnelId()
                
                var args            = []
                
                args.push('-u'); args.push(sl.userName);
                args.push('-k'); args.push(sl.key);
                args.push('-t'); args.push(sl.tunneledDomains || 'localhost');
                args.push('--tunnel-identifier'); args.push(me.tunnelId);
                // this option is possibly not required, added as an experiment
                args.push('--no-proxy-caching')
                
                if (options[ 'proxy-host' ]) {
                    args.push('-proxy'); args.push(options.WRAPPER.getProxyHostPort())
                    
                    if (options[ 'proxy-user' ] && options[ 'proxy-password' ]) {
                        args.push('--proxy-userpwd'); args.push(options[ 'proxy-user' ] + ':' + options[ 'proxy-password' ])
                    }
                }
                
                me.tunnelInfo   = sl.userName
                
                return me.launchTunnel(
                    launcher.binDir + '/binary/saucelabs/' + launcher.getPlatformId() + '/bin/sc' + (launcher.isWindows ? '.exe' : ''),
                    args,
                    undefined,
                    // the . in regexp is required! it indicates the 2nd entry of `you may start your tests` phrase
                    // (it is encountered 1st as: "Please wait for 'you may start your tests' to start your tests.")
                    /you may start your tests\./i
                )
            })
        }
    }
    // eof methods
})
