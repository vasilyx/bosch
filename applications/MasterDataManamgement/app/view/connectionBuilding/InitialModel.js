Ext.define('MAM.view.connectionBuilding.InitialModel', {
    extend: 'Ext.app.ViewModel',
    requires: ['MAM.model.ConnectionBuilding'],

    alias: 'viewmodel.connectionBuildingInitialModel',

    data: {
        connectionBuilding: null,
        relations: null,
        total: 0
    },

    formulas: {
        totalText: {
            get: function(get) {
                var total = get('total');
                if(total === 1){
                    return Ext.util.Format.number(total, '0,000') + ' ' + _tl.get('mam.label.connectionBuilding');
                }
                else{
                    return Ext.util.Format.number(total, '0,000') + ' ' + _tl.get('mam.label.connectionBuildings')
                }
            }
        }
    }

});