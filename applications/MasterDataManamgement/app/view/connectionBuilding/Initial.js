Ext.define('MAM.view.connectionBuilding.Initial', {
    extend: 'Ext.container.Container',

    requires: [
        'MAM.view.connectionBuilding.List',
        'MAM.view.connectionBuilding.InitialModel',
        'MAM.view.connectionBuilding.InitialController',
        'MAM.view.connectionBuilding.Detail',
        'MAM.view.connectionBuilding.SubDetail',
        'MAM.view.relations.Initial',
        'MAM.view.customAttributes.Initial',
        'Energy.common.ui.Header',
        'MAM.view.action.Initial'
    ],

    alias: 'widget.connectionBuildingInitial',

    viewModel: 'connectionBuildingInitialModel',
    controller: 'connectionBuildingInitialController',

    layout: {
       type: 'hbox',
        align: 'stretch',
        resizable: true
    },
    padding: '0 0 0 10',

    initComponent: function () {

       this.callParent(arguments);

        this.createConnectionBuildingsList();
        this.addSelectMessage();
    },

    addSelectMessage: function(){
        this.lookupReference('detailContainer').removeAll();
        this.lookupReference('detailContainer').add({
            xtype: 'commonUiDisplayMessage',
            message: _tl.get('mam.label.ui.selectEntity')

        });
    },

    createConnectionBuildingsList: function(){
        this.add(
            {
                xtype: 'container',
                width: 300,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype:'commonUiHeader',
                        header: _tl.get('mam.label.connectionBuildings'),
                        bind: {
                            header:  '{totalText}'
                        }

                    },
                    {
                        xtype: 'connectionBuildingList',

                        flex: 1
                    }
                ]
            },
            {
                xtype: 'container',
                margin: '0 0 0 10',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                flex: 1,
                items: [
                    {
                        xtype:'commonUiHeader',
                        header: _tl.get('mam.label.connectionBuilding'),
                        colorClass: 'green',
                        showArrow: true,
                        bind: {
                            header: _tl.get('mam.label.connectionBuilding') + ' {connectionBuilding.customId}',
                            subheader: _tl.get('mam.label.mode.view')
                        }

                    },
                    {
                        xtype: 'container',
                        cls: 'border-lrb',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        flex: 1,
                        reference: 'detailContainer'
                    }

                ]
            }, {
                xtype: 'actionInitial',
                viewModel: {
                    data: {
                        entity: MAM.utils.Constants.CONNECTIONBUILDING
                    }
                }
            }
        );
    },

    createConnectionBuildingDetailContainer: function(){
        var cbDetail = this.lookupReference('detailContainer');
        cbDetail.removeAll();
        cbDetail.add(
            {
                xtype: 'connectionBuildingDetail',
                flex: 1
            },
            {
                xtype: 'connectionBuildingSubDetail',
                flex: 1
            }
        );
        
    }

});