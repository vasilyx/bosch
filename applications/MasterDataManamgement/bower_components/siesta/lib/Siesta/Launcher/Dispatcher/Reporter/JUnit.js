/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Launcher.Dispatcher.Reporter.JUnit', {
    
    requires    : [ 
        'forEachAssertion', 'getAllResults'
    ],
    
    
    methods : {
        
        convertDateToISO8601 : function (date) {
            var prependZeroIfNeeded = function (number, howMany) {
                if (!howMany || howMany == 1) return number < 10 ? '0' + number : number 
                
                return number < 10 ? '00' + number : number < 100 ? '0' + number : number
            }
            
            return date.getFullYear() + '-' + prependZeroIfNeeded(date.getMonth() + 1) + '-' + prependZeroIfNeeded(date.getDate()) + 
                'T' + prependZeroIfNeeded(date.getHours()) + ':' + prependZeroIfNeeded(date.getMinutes()) + ':' + prependZeroIfNeeded(date.getSeconds()) +
                '.' + prependZeroIfNeeded(date.getMilliseconds(), 2)
                
        },
        
        
        generateJunitReport : function (options) {
            options                 = options || {}
            var me                  = this
            
            var testSuiteNode       = new Siesta.Util.XMLNode({
                tag         : 'testsuite',
                
                attributes  : {
                    name        : this.testSuiteName || this.options.build || 'No title',
                    timestamp   : this.convertDateToISO8601(new Date(this.startDate)),
                    time        : ((this.endDate || new Date()) - this.startDate) / 1000,
                    hostname    : this.hostName || ''
                }
            })
            
            var properties          = options.properties
            
            if (properties) {
                var propertiesNode  = testSuiteNode.appendChild({
                    tag         : 'properties'
                })
                
                Joose.O.each(properties, function (value, name) {
                    propertiesNode.appendChild({
                        tag         : 'property',
                        
                        attributes  : {
                            name        : name,
                            value       : value
                        }
                    })
                })
            }
            
            var totalTests      = 0
            var totalErrors     = 0
            var totalFailures   = 0
            
            Joose.A.each(this.getAllResults(), function (testInfo) {
                totalTests++
                
                var testCaseNode    = new Siesta.Util.XMLNode({
                    tag             : 'testcase',
                    
                    attributes      : {
                        name        : testInfo.url,
                        classname   : testInfo.jUnitClass || ''
                    }
                })
                
                if (testInfo.ERROR) {
                    totalErrors++
                    
                    testCaseNode.appendChild({
                        tag             : 'error',
                        textContent     : testInfo.ERROR
                    })
                    
                } else {
                    if (testInfo.sessionId != null) testCaseNode.setAttribute('sessionId', testInfo.sessionId)
                    
                    testCaseNode.setAttribute('time', (testInfo.endDate - testInfo.startDate) / 1000)
                        
                    var hasException    = false
                    
                    me.forEachAssertion(testInfo, function (assertion) {
                        if (!assertion.passed)
                            if (assertion.isException) {
                                totalErrors++
                                
                                hasException    = true
                                
                                testCaseNode.appendChild({
                                    tag             : 'error',
                                    attributes      : {
                                        type        : assertion.exceptionType
                                    },
                                    textContent     : assertion.annotation
                                })
                            } else 
                                testCaseNode.appendChild({
                                    tag         : 'failure',
                                    
                                    attributes  : {
                                        message     : assertion.description || '',
                                        type        : assertion.name || 'FAIL'
                                    },
                                    
                                    textContent : assertion.annotation || ''
                                })
                    }, me, { ignoreTodoAssertions : true })
                    
                    // test has failed, but w/o exception - some other reason
                    if (!hasException && !testInfo.passed) totalFailures++
                }
                
                testSuiteNode.appendChild(testCaseNode)
            })
            
            testSuiteNode.setAttribute('tests', totalTests)
            testSuiteNode.setAttribute('errors', totalErrors)
            testSuiteNode.setAttribute('failures', totalFailures)
            
            return testSuiteNode + ''
        }
    }
})
