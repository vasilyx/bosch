/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Siesta.Launcher.Page.SlimerJS', {
    
    isa         : Siesta.Launcher.Page.PhantomJS,
    
    methods : {
        
        buildPage : function () {
            var me      = this
            
            var page            = require("webpage").create()
            
            page.viewportSize   = {
                width       : this.width,
                height      : this.height
            }
            
            page.onConsoleMessage = function (msg) {
                console.log(msg)
            }
            
            return page
        }
    }
})
