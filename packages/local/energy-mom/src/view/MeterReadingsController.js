Ext.define('Energy.mom.view.MeterReadingsController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.actionMeterReadingsController',

    control: {
        '#closeButton': {
            click:'close'
        },
        '#meterReadingsGrid': {
            afterrender: 'loadMeterReadings'
        }
    },

    /**
     * close the view, if the view is used or open
     * in a window
     */
    close: function() {
        //close if the underlaying component is a window
        if(this.getView().up().getXType() === 'actionWindow'){
            this.getView().up().close();
        }
    },

    loadMeterReadings: function(){
        var store = this.getViewModel().getStore('meterReadings');
        store.getProxy().setUrl(
            Ext.String.format( store.getProxy().getUrl(), this.getViewModel().get('record').get('customId'))
        );
        store.load();
    }
});