/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('MAM.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',
    //hier kann nur alias verwendet werden, da xtype nur bei view componenten
    alias: 'viewmodel.main',

    data: {
        
    }

    //TODO - add data, formulas and/or methods to support your view
});
