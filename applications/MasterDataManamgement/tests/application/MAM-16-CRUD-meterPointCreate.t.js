StartTest(function (t) {

    var referenceData = {
        id: 1491119,
        customId: 'DE0700081632100000000000070238000'
    };

    t.it('#1 Click on create - window open', function (t) {
        t.chain(
            {waitForCQ: 'app-main tabbar'},
            {click: 'tabbar=> span.icon-meterPoint'},

            function (next) {
                t.click(t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-create]'));
                t.elementIsVisible(t.cq1('meterPointCreateDetail'), 'Window is visible');
                t.click(t.cq1('meterPointCreateDetail button#closeButton'));
                t.elementIsNotVisible(t.cq1('meterPointCreateDetail'), 'Window is invisible');

                t.click(t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-create]'));
                t.elementIsVisible(t.cq1('meterPointCreateDetail'), 'Window is visible again');

            }
        )
    });

    t.it('#2 Checking mandatory fields', function (t) {
        t.chain(
            function (next) {

                t.expect(t.cq1('meterPointCreateDetail  button#saveViewButton').isDisabled()).toBe(true);
                t.expect(t.cq1('meterPointCreateDetail  button#saveEditButton').isDisabled()).toBe(true);

                t.type(t.cq1('meterPointCreateDetail  textfield[name=customId]'), 'DE0700081632100000000000070238000');
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.type(t.cq1('meterPointCreateDetail  combo[name=medium]'), 'asd1');

                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.type(t.cq1('meterPointCreateDetail  combo[name=measuringType]'), 'asd2');
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.expect(t.cq1('meterPointCreateDetail button#saveViewButton').isDisabled()).toBe(false);
                t.expect(t.cq1('meterPointCreateDetail button#saveEditButton').isDisabled()).toBe(false);
            })}
    );

    t.it('#3 Check 409 response - attempt to create entity with the same Id', function (t) {
        t.chain(function (next) {
                MOCK_DATA.setMockedResponse('meterPoints/', 'post', 409);
                t.click(t.cq1('meterPointCreateDetail button#saveViewButton'));
                t.elementIsVisible(t.compositeQuery('commonUiDialogBox => div.icon.warning'), 'Warning is visible');
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            })}
    );

    t.it('#4 After success created. window shows this entity in edit mode.', function (t) {
        t.chain(function (next) {
                t.click(t.cq1('commonUiDialogBox button'));
                MOCK_DATA.setMockedResponse('meterPoints/', 'post', 201, Ext.JSON.encode({id:referenceData.id}));
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/detail_' + referenceData.customId + '.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/l/customAttributes_dummy.json', referenceData.id);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/l/relations_dummy.json', referenceData.id);
                t.click(t.cq1('meterPointCreateDetail button#saveEditButton'));

                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.cqExists('meterPointEditDetail', 'meterPointEditDetail view exist');
                t.contentLike(t.cq1('meterPointEditDetail displayfield[name="customId"]'), referenceData.customId, 'Found right customId');
                t.cqExists('customAttributesEdit', 'customAttributesEdit view exist');
                t.cqExists('relationsEdit', 'relationsEdit view exist');
            })}
    );

    t.it('#5 After success created. window shows this entity in view mode.', function (t) {
        t.chain(
            function (next) {
                t.click(t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-create]'));
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.type(t.cq1('meterPointCreateDetail  textfield[name=customId]'), 'DE0700081632100000000000070238000');
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.type(t.cq1('meterPointCreateDetail  combo[name=medium]'), 'asd1');

                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.type(t.cq1('meterPointCreateDetail  combo[name=measuringType]'), 'asd2');
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.click(t.cq1('meterPointCreateDetail button#saveViewButton'));
                next();
            },
            {
                action      : 'wait',
                delay       : 1500
            },
            function (next) {
                t.cqExists('meterPointDetail', 'meterPointDetail view exist');
                t.contentLike(t.cq1('meterPointDetail displayfield[name="customId"]'), referenceData.customId, 'Found right customId');
                t.cqExists('customAttributesInitial', 'customAttributes view exist');
                t.cqExists('relationsInitial', 'Relations view exist');
            })}
    );
});
