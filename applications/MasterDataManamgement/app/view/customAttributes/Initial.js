Ext.define('MAM.view.customAttributes.Initial',{
    extend: 'Ext.grid.Panel',

    requires: [
        'MAM.view.customAttributes.InitialController',
        'MAM.view.customAttributes.InitialModel'
    ],

    alias: 'widget.customAttributesInitial',
    reference: 'customAttributesInitial',
    viewModel: 'customAttributesInitialModel',
    controller: 'customAttributesInitialController',
    border: false,
    viewConfig: {
        loadMask: false
    },
    bind: {
        store: '{customAttributes}'
    },
    initComponent: function () {
        
        if(Ext.isEmpty(this.title)){
            this.title = _tl.get("mam.label.tab.customAttributes");
        }

        this.emptyText = _tl.get('mam.label.ui.noData');
        
        var columns= [
            {
                text: _tl.get("mam.label.customAttributes.key"),
                dataIndex: 'label',
                flex: 1
            },
            {
                text: _tl.get("mam.label.customAttributes.value"),
                dataIndex: 'value',
                flex: 1
            }

        ];


        Ext.apply(this, {columns: columns});
        this.callParent(arguments);
    }

});