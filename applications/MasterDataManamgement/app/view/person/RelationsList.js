Ext.define('MAM.view.person.RelationsList', {
    extend: 'MAM.view.relations.EntityList',
    requires: [
        'MAM.view.relations.EntityList'
    ],
    alias: 'widget.personsRelationsList',

    getToolbar: function () {
        return [{
            xtype: 'textfield',
            name: 'customId',
            hideLabel: true,
            width: 200
        }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'name',
                hideLabel: true,
                width: 120
            }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'nameAffix',
                hideLabel: true,
                width: 120
            }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'title',
                hideLabel: true,
                flex: 1
            }, this.getButtonClear()
        ];
    },

    getColumns: function () {
        return [
            {text: _tl.get("mam.label.person.customId"), dataIndex: 'customId', width: 250},
            {text: _tl.get("mam.label.person.name"), dataIndex: 'name', width: 170},
            {text: _tl.get("mam.label.person.nameAffix"), dataIndex: 'nameAffix', width: 170},
            {text: _tl.get("mam.label.person.postalCode"), dataIndex: 'postalCode', flex: 1}
        ];
    },

    initComponent: function () {

        this.callParent(arguments);
    }
});