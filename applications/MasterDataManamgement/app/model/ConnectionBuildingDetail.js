Ext.define('MAM.model.ConnectionBuildingDetail', {
    extend: 'MAM.model.ConnectionBuilding',
    proxy: {
        type: 'rest',
        api: {
            create: undefined,
            read: '/MAM/' + MAM.Config.Backend.Versions + '/connectionBuildings',
            update: undefined,
            destroy: undefined
        },
        headers: {
            'Accept': 'application/json'
        },
        extraParams: {
            expand: 'properties'
        }
    }
});