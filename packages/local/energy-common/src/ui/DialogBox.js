Ext.define("Energy.common.ui.DialogBox", {
    extend: 'Ext.window.Window',
    requires: [
        'Energy.common.language.DialogBox'
    ],
    alias: 'widget.commonUiDialogBox',
    shadow: false,
    minHeight: 120,
    maxHeight: 740,
    width: 400,
    header: false,
    closeable: true,
    modal: true,
    buttonAlign: 'center',
    cls: 'energy-dialogbox',

    // in config some data predifined
    config: {
        msgTitle: 'Information',//LangDialogBox.get('Information'), //TODO: review 30.01.2017: language settings not possible this way, because LangDialogBox is undefined
        msgContent: null, // string or [{}, {}]
        msgType: 'information', // string
        showButton: true,   // boolean
        defaultButtonsOne: [{
            text: 'OK',//LangDialogBox.get('OK'), //TODO: review 30.01.2017: language settings not possible this way, because LangDialogBox is undefined
            itemId: 'closeButton',
            handler: function () {
                this.up().up().close()
            }
        }]
    },

    constructor: function (config) {

        this.callParent(arguments);
   },

    initComponent: function (config) {
        Ext.create('LangDialogBox');



        // second way to create component
        if (this.getMsgContent() !== null) {
            this._createContent();
        }

        this.initConfig(config);
        this.callParent();
    },

    /**
     * @returns void
     * @private
     */
    _createContent: function () {
        this.items = this._getItems();

        if (this.getShowButton()) {
            this.buttons = this.getDefaultButtonsOne();
        }
    },

    /**
     * @returns {{xtype: string, style: {padding: string}, defaultType: string, items: *[]}}
     * @private
     */
    _getItems: function () {
        var typeMapping = ['information', 'warning', 'error', 'success', 'question'];
        var iconStyle = (typeMapping.indexOf(this.getMsgType()) === -1) ? 'success' : this.getMsgType();

        var items = [{
            cls: 'icon ' + iconStyle
        },
            {
                cls: 'title',
                html: this.getMsgTitle()
            }
        ];

        if (typeof(this.getMsgContent()) === "string") {
            items.push({
                cls: 'content',
                html: this.getMsgContent()
            });
        } else {
            items.push({
                cls: 'content',
                items: this.getMsgContent()
            });
        }

        return {
            xtype: 'container',
            style: {
                padding: '15px'
            },
            defaultType: 'container',
            items: items
        };
    },

    /**
     * @param msg
     */
    showWarning: function (msg) {
        this.setMsgContent(msg);
        this.setMsgType('warning');
        this.setMsgTitle(LangDialogBox.get('Warning'));

        this._createContent();
        this.initComponent();
        this.show();
    },

    /**
     * @param msg
     */
    showError: function (msg) {
        this.setMsgContent(msg);
        this.setMsgType('error');
        this.setMsgTitle(LangDialogBox.get('Error'));

        this._createContent();
        this.initComponent();
        this.show();
    },

    /**
     * @param msg string || object
     * @param showButton boolean
     */
    showSuccess: function (msg, showButton) {
        if (showButton !== undefined) {
            this.setShowButton(showButton);
        }

        this.setMsgContent(msg);
        this.setMsgType('success');
        this.setMsgTitle(LangDialogBox.get('Success'));

        this._createContent();
        this.initComponent();
        this.show();
    },

    /**
     *
     * @param msg
     * @param showButton array
     */
    showInformation: function (msg, showButton) {

        if (showButton !== undefined) {
            this.setShowButton(showButton);
        }

        this.setMsgContent(msg);
        this.setMsgType('information');
        this.setMsgTitle(LangDialogBox.get('Information'));

        this._createContent();
        this.initComponent();
        this.show();
    },

    /**
     * @param msg string
     * @param buttons []
     */
    showQuestion: function (msg, buttons) {
        this.setMsgContent(msg);
        this.setMsgType('question');
        this.setMsgTitle(LangDialogBox.get('Question'));

        if (buttons === undefined) {
            this.setDefaultButtonsOne([
                {
                    text: LangDialogBox.get('Yes'),
                    itemId: 'buttonPositive'
                },
                {
                    text: LangDialogBox.get('No'),
                    itemId: 'buttonNegative'
                }
            ]);
        } else if (typeof(buttons) === 'object') {
            this.setDefaultButtonsOne(buttons);
        }

        this._createContent();
        this.initComponent();
        this.show();
    },

    listeners: {
        show: function (w) {
            if (this.getShowButton() != true) {
                var self = this;
                setTimeout(function () {
                    w.getEl().fadeOut();
                    w.destroy();
                }, 3000);
            }
        }
    }

});