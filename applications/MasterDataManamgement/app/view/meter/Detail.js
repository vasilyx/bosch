Ext.define('MAM.view.meter.Detail', {
    extend: 'Ext.Panel',

    alias: 'widget.meterDetail',

    bind: {
        data: '{meter}'
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    padding: '0 0 10 0',
    scrollable: true,
    
    initComponent: function () {

        var defaults = {
            xtype: 'displayfield',
            labelWidth: 150
        };
        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if(this.responsiveConfig){
            responsiveConfig = this.responsiveConfig;
        }

        var items = [
            {
                xtype: 'container',
                cls: 'bg-grey',
                margin: 10,
                padding: '5 10 0 10',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                items: this.getNorthItems()

            },
            {
                xtype: 'container',
                padding: '0 20 0 20',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getWestItems()
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getEastItems()
                    }
                ]
            },
            {
                xtype: 'tabpanel',
                padding: '0 20 0 20',
                plain: true,
                items: this.getSouthItems()

            }
        ];

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    },
    /*
     * Defines the top area items for connection building properties
     */
    getNorthItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.meter.customId'),
                name: 'customId',
                flex: 1,
                bind: {
                    value: '{meter.customId}'
                }
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                flex: 1,
                items: [
                    {
                        fieldLabel: _tl.get('mam.label.meter.createdTimeStamp'),
                        flex: 1,
                        name: 'createdTimeStamp',
                        bind: {
                            value: '{meter.createdTimeStamp}'
                        }
                    },
                    {
                        fieldLabel: _tl.get('mam.label.meter.id'),
                        name: 'meterId',
                        bind: {
                            value: '{meter.id}'
                        }
                    }
                ]
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the left side of panel
     */
    getWestItems: function() {
        var items = [,
            {
                fieldLabel: _tl.get('mam.label.meter.buildInDate'),
                name: 'buildInDate',
                bind: {
                    value: '{meter.buildInDate}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.buildOutDate'),
                name: 'buildOutDate',
                bind: {
                    value: '{meter.buildOutDate}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.medium'),
                name: 'medium',
                bind: {
                    value: '{medium}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.measuringPrinciple'),
                name: 'measuringPrinciple',
                bind: {
                    value: '{measuringPrinciple}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.status'),
                name: 'status',
                bind: {
                    value: '{status}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.deviceType'),
                name: 'deviceType',
                bind: {
                    value: '{meter.deviceType}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.deviceClass'),
                name: 'deviceClass',
                bind: {
                    value: '{deviceClass}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.deviceKind'),
                name: 'deviceKind',
                bind: {
                    value: '{meter.deviceKind}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.manufacturer'),
                name: 'manufacturer',
                bind: {
                    value: '{meter.manufacturer}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.factoryNumber'),
                name: 'factoryNumber',
                bind: {
                    value: '{meter.factoryNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.deliveryNoteNumber'),
                name: 'deliveryNoteNumber',
                bind: {
                    value: '{meter.deliveryNoteNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.energyFlowDirection'),
                name: 'energyFlowDirection',
                bind: {
                    value: '{meter.energyFlowDirection}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.mountingType'),
                name: 'mountingType',
                bind: {
                    value: '{meter.mountingType}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.meterReadingType'),
                name: 'meterReadingType',
                bind: {
                    value: '{meter.meterReadingType}'
                }
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the right side of panel
     */
    getEastItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.meter.lotNumber'),
                name: 'lotNumber',
                bind: {
                    value: '{meter.lotNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.examinationNumber'),
                name: 'examinationNumber',
                bind: {
                    value: '{meter.examinationNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.charge'),
                name: 'charge',
                bind: {
                    value: '{meter.charge}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.interface'),
                name: 'interface',
                bind: {
                    value: '{meter.interface}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.comAddress'),
                name: 'comAddress',
                bind: {
                    value: '{meter.comAddress}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.serverId'),
                name: 'serverId',
                bind: {
                    value: '{meter.serverId}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.publicKey'),
                name: 'publicKey',
                bind: {
                    value: '{meter.publicKey}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.keyM'),
                name: 'keyM',
                bind: {
                    value: '{meter.keyM}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.calibrationYear'),
                name: 'calibrationYear',
                bind: {
                    value: '{meter.calibrationYear}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.calibrationEndYear'),
                name: 'calibrationEndYear',
                bind: {
                    value: '{meter.calibrationEndYear}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.constructionYear'),
                name: 'constructionYear',
                bind: {
                    value: '{meter.constructionYear}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.meterSize'),
                name: 'meterSize',
                bind: {
                    value: '{meter.meterSize}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.tariffCount'),
                name: 'tariffCount',
                bind: {
                    value: '{meter.tariffCount}'
                }
            }];
        return items;
    },
    /**
     * Defines the area items for meter properties
     * on the bottom side of panel
     * mostyl parameters collected in a grid
     */
    getSouthItems: function(){
        var items = [
            {
                xtype: 'grid',
                border: true,
                title: _tl.get('mam.label.meter.registers'),
                reference: 'meterRegisters',
                emptyText: _tl.get('mam.label.ui.noData'),
                bind: {
                    store: '{registers}'
                },
                viewConfig: {
                    loadMask: false
                },
                columns: [{
                    text: _tl.get("mam.label.meter.register.name"),
                    dataIndex: 'name',
                    flex: 1
                },
                    {
                        text: _tl.get("mam.label.meter.register.billingRelevant"),
                        dataIndex: 'billingRelevant',
                        flex: 1,
                        renderer: function(value){
                            return _tl.get("mam.label.ui." + value.toString());
                        }
                    }]
            }
        ];
        return items;
    }
});