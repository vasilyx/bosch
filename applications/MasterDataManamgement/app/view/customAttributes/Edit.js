Ext.define('MAM.view.customAttributes.Edit',{
    extend: 'Ext.grid.Panel',

    requires: [
        'MAM.view.customAttributes.InitialController',
        'MAM.view.customAttributes.InitialModel',
        'MAM.utils.Configuration'
    ],

    alias: 'widget.customAttributesEdit',
    reference: 'customAttributesEdit',
    viewModel: 'customAttributesInitialModel',
    controller: 'customAttributesInitialController',
    border: false,
    viewConfig: {
        loadMask: false
    },
    bind: {
        store: '{customAttributes}'
    },
    plugins: [
        {
            clicksToMoveEditor: 1,
            autoCancel: false,
            ptype: 'rowediting',
            pluginId: 'rowediting'
        }
    ],
    initComponent: function () {
        var me = this;

        if(Ext.isEmpty(this.title)){
            this.title = _tl.get("mam.label.tab.customAttributes");
        }

        this.emptyText = _tl.get('mam.label.ui.noData');

        var columns= [
            {
                text: _tl.get("mam.label.customAttributes.key"),
                dataIndex: 'key',
                editor: {
                    xtype: 'combo',
                    store: Ext.create('Ext.data.Store', {
                        fields: [ 'key', 'label'],
                        data :  MAM.utils.Configuration.getCustomAttributesList(me.getViewModel().get('entity'))
                    }),
                    queryMode: 'local',
                    displayField: 'label',
                    valueField: 'key',
                    allowBlank: false
                },
                flex: 1
            },
            {
                text: _tl.get("mam.label.customAttributes.value"),
                dataIndex: 'value',
                editor: {
                    xtype: 'textfield',
                    allowBlank: false
                },
                flex: 1
            },
            {
                icon: 'resources/images/edit/delete16.png',
                xtype: 'actioncolumn',
                width: 20,
                tooltip: _tl.get("mam.edit.customAttributes.removeAttribute")
            }
        ];


        Ext.apply(this, {
            columns: columns,
            tbar: [{
                text: _tl.get("mam.edit.customAttributes.addAttribute"),
                itemId: 'addAttribute'
            }]
        });
        this.callParent(arguments);
    }

});