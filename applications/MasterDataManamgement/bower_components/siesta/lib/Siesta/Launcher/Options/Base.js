/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Siesta.Launcher.Options.Base', {
    
    does        : [
        Siesta.Launcher.Role.CanPrintWithLauncher
    ],
    
    
    has         : {
        launcher        : { required : true },
        options         : { required : true, init : Joose.I.Object }
    },
    
    
    methods : {
        
        initialize : function () {
            this.options.WRAPPER    = this
        },
        
        
        readFile : function () {
            return this.launcher.readFile.apply(this.launcher, arguments)
        },
        
        
        printHelp : function () {
            return this.launcher.printHelp.apply(this.launcher, arguments)
        },
        
        
        validate : function () {
            var options     = this.options
            var argv        = this.launcher.argv
            
            if (argv.length && !options[ 'harness-url' ]) options[ 'harness-url' ] = argv.shift()
            
            if (!options[ 'harness-url' ]) {
                this.printError("No harness url is provided\n")
                
                this.printHelp()
                
                return false
            }
            
            var maxWorkers  = options[ 'max-workers' ] || 1
            
            if (maxWorkers) {
                maxWorkers  = Number(maxWorkers)
                
                if (isNaN(maxWorkers) || maxWorkers <= 0) {
                    this.printError("The `--max-workers` should be a valid positive number\n")
                    
                    return false
                }
                
                options[ 'max-workers' ]    = maxWorkers
            }
            
            if (options[ 'verbose' ] && maxWorkers == 1) {
                if (options[ 'rerun-failed' ]) this.warn("The --rerun-failed option is disabled for --verbose and --max-workers=1 combination")
                
                options[ 'rerun-failed' ] = false
            }
            
            return this.validateReportOptions(options) && this.validateCoverageOptions(options)
        },
        
        
        validateReportOptions : function (options) {
            var reportFormat        = (options[ 'report-format' ] || '').toLowerCase()
            var reportFile          = options[ 'report-file' ]
            
            // `report-file-prefix` is deprecated
            if (!reportFile && options[ 'report-file-prefix' ]) {
                reportFile          = options[ 'report-file-prefix' ]
                
                if (!reportFile.match(/\{browser\}/)) {
                    var match           = /(.*?)\.([^.]*)$/.exec(reportFile)
                    
                    if (match) 
                        reportFile  = match[ 1 ] + '{browser}.' + match[ 2 ]
                    else
                        reportFile  = reportFile + '{browser}'
                }
            }
            
            if (reportFormat && reportFormat != 'html' && reportFormat != 'json' && reportFormat != 'jsons' && reportFormat != 'junit') {
                this.printError([
                    'Unrecognized report format: ' + reportFormat
                ]);
                
                return false
            }
            
            if (reportFormat && !reportFile) {
                this.printError([
                    '`report-file` option is required, when `report-format` option is specified'
                ]);
                
                return false
            }
            
            if (reportFile && !reportFormat) options[ 'report-format' ] = 'json'
            
            if (reportFile) options[ 'report-file' ] = this.prepareReportFileName(reportFile)
            // save the lowercased version
            if (reportFormat) options[ 'report-format' ] = reportFormat

            return true
        },
        
        
        validateCoverageOptions : function (options) {
            var me                      = this
            
            var coverageReportFormats   = options[ 'coverage-report-format' ] || ''
            if (!(coverageReportFormats instanceof Array)) coverageReportFormats = [ coverageReportFormats ]
            
            Joose.A.each(coverageReportFormats, function (format, index, arr) {
                var formats     = format.split(/\+|,/)
                
                if (formats.length == 1) return
                
                coverageReportFormats[ index ]    = formats
            })
            
            // flatten
            coverageReportFormats       = Array.prototype.concat.apply([], coverageReportFormats)
            
            // map to lowercase
            coverageReportFormats       = Joose.A.map(coverageReportFormats, function (format) { return format.toLowerCase() })
            
            // filter out empty entries
            coverageReportFormats       = Joose.A.grep(coverageReportFormats, function (format) { return Boolean(format) })
            
            if (
                Joose.A.each(coverageReportFormats, function (format) {
                    if (format != 'html' && format != 'raw' && format != 'lcov') {
                        me.printError([
                            'Unrecognized coverage report format: ' + format
                        ]);
                        
                        return false
                    }
                }) === false
            ) return false
            
            var enableCodeCoverage      = coverageReportFormats.length > 0
            
            if (enableCodeCoverage) {
                var coverageReportDir       = options[ 'coverage-report-dir' ] || './coverage'
                
                // remove "/" at the end if presented
                coverageReportDir           = coverageReportDir.replace(/\/?$/, '')
                
                var coverageUnit            = options[ 'coverage-unit' ]
                
                if (coverageUnit && coverageUnit != 'file' && coverageUnit != 'extjs_class') {
                    this.printError([
                        'Unrecognized coverage unit: ' + coverageUnit
                    ]);
                    
                    return false
                }
                
                var previousCoverageReport  = options[ 'previous-coverage-report' ]
                
                if (previousCoverageReport) {
                    var report
                    
                    try {
                        // first trying as `previousCoverageReport` contains a full file name
                        report              = this.readFile(previousCoverageReport)
                    } catch (e) {
                        try {
                            // then as it contains only a dir
                            report          = this.readFile(previousCoverageReport.replace(/(\\|\/)?$/, '/') + 'raw_coverage_data.json')
                        } catch (e) {
                            this.printError([
                                "Can't read the content of the previous raw coverage report: " + e
                            ]);
                            
                            return false
                        }
                    }
                    
                    try {
                        report              = JSON.parse(report)
                        
                        if (!report.coverageUnit || !report.rawReport) throw ""
                    } catch (e) {
                        this.printError([
                            "Previous raw coverage report is not a valid JSON or has wrong format"
                        ]);
                        
                        return false
                    }
                    
                    previousCoverageReport  = report
                }
            }
            
            options.coverage    = enableCodeCoverage ? {
                coverageUnit                : coverageUnit,
                previousCoverageReport      : previousCoverageReport,
                coverageReportFormats       : coverageReportFormats,
                coverageReportDir           : coverageReportDir
            } : null
            
            return true
        },
        
        
        getReportFileNameTemplateReplacer : function () {
            var options         = this.options
            
            return function (m, match) {
                return options[ match ]
            }
        },
        
        
        prepareReportFileName : function (template) {
            return template.replace(/\{(\w+)\}/, this.getReportFileNameTemplateReplacer())
        }
    }
})
