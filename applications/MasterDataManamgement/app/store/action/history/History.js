Ext.define('MAM.store.action.history.History', {
    extend: 'Ext.data.Store',
    requires: [
        'Ext.data.proxy.SessionStorage'
    ],
    alias: 'store.history',
    storeId: 'history',
    model: 'MAM.model.action.history.History',
    autoLoad: true,
    proxy: {
        type: 'sessionstorage',
        id : 'history'
    },
    data: [
    ]
});

