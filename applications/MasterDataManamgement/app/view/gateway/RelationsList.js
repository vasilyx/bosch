Ext.define('MAM.view.gateway.RelationsList', {
    extend: 'MAM.view.relations.EntityList',
    requires: [
        'MAM.view.relations.EntityList'
    ],
    alias: 'widget.gatewaysRelationsList',

    getToolbar: function () {
        return [{
            xtype: 'textfield',
            name: 'customId',
            hideLabel: true,
            width: 200
        }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'manufacturer',
                hideLabel: true,
                width: 120
            }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'status',
                hideLabel: true,
                width: 120
            }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'lotNumber',
                hideLabel: true,
                flex: 1
            }, this.getButtonClear()
        ];
    },

    getColumns: function () {
        return [
            {text: _tl.get("mam.label.gateway.customId"), dataIndex: 'customId', width: 250},
            {text: _tl.get("mam.label.gateway.manufacturer"), dataIndex: 'manufacturer', width: 170},
            {text: _tl.get("mam.label.gateway.status"), dataIndex: 'status', width: 170},
            {text: _tl.get("mam.label.gateway.lotNumber"), dataIndex: 'lotNumber', flex: 1}
        ];
    },

    initComponent: function () {

        this.callParent(arguments);
    }
});