Ext.define('MAM.view.relations.Edit', {
    extend: 'Ext.container.Container',

    requires: [
        'MAM.view.relations.InitialController',
        'MAM.view.relations.InitialModel',
        'Energy.common.ui.DisplayMessage'
    ],

    alias: 'widget.relationsEdit',

    viewModel: 'relationsInitialModel',
    controller: 'relationsInitialController',
    border: false,
    flex: 1,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    initComponent: function () {

        this.callParent(arguments);
        if (Ext.isEmpty(this.title)) {
            this.title = _tl.get("mam.label.tab.relations");
        }

        this.createEmptyContainer();
    },

    createEmptyContainer: function () {
        this.add({
            xtype: 'panel',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            flex: 1,
            margin: '0 10 0 0',
            reference: 'relationEditDetail'
        });
    },

    createRelationContainer: function () {
        this.add(
            {
                xtype: 'panel',
                itemId: 'relationAccordion',
                reference: 'relationEditAccordion',
                width: 300,
                cls: 'relationAccordion',
                layout: {
                    type: 'accordion',
                    animate: false
                }
            },
            {
                xtype: 'panel',
                flex: 1,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                reference: 'relationDetail'
            }
        );

    },
    createRelationPanel: function (entity, entities, relationCount) {

        this.lookupReference('relationEditAccordion').add({
            xtype: 'grid',
            title: _tl.get('mam.label.' + entities) + ' (' + relationCount + ')',
            bind: {
                store: '{' + entities + '}'
            },
            itemId: entities,
            viewConfig: {
                loadMask: false
            },
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                padding: 1,
                items: [
                    {
                        xtype: 'button',
                        icon: 'resources/images/edit/link16.png',
                        tooltip: Ext.String.format(_tl.get('mam.edit.entityList.addRelation'), Ext.String.capitalize(_tl.get('mam.label.' + entities)))
                    }
                ]
            }],
            flex: 1,
            columns: [{
                text: _tl.get('mam.label.' + entity + '.customId'),
                dataIndex: 'customId',
                flex: 1
            },{
                icon: 'resources/images/edit/unlink16.png',
                xtype: 'actioncolumn',
                width: 30,
                tooltip: _tl.get("mam.edit.relation.unlink")
            }]
        })
    },
    showEmptyMessage: function () {
        this.lookupReference('relationEditDetail').removeAll();
        this.lookupReference('relationEditDetail').add({
            xtype: 'commonUiDisplayMessage',
            message: _tl.get('mam.label.ui.noData')

        });
    }
});