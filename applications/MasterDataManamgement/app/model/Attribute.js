Ext.define('MAM.model.Attribute', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'key', validators: 'presence'},
        { name: 'label', mapping: 'key', convert: MAM.utils.model.Converts.convertCustomAttribute },
        { name: 'value' , validators: 'presence'}
    ]
});