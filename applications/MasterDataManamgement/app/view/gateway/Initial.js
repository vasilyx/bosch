Ext.define('MAM.view.gateway.Initial', {
    extend: 'Ext.container.Container',

    requires: [
        'MAM.view.gateway.List',
        'MAM.view.gateway.InitialModel',
        'MAM.view.gateway.InitialController',
        'MAM.view.gateway.Detail',
        'MAM.view.gateway.SubDetail',
        'Energy.common.ui.Header'
    ],

    alias: 'widget.gatewayInitial',

    viewModel: {
        type: 'gatewayInitialModel'
    },
    controller: 'gatewayInitialController',

    layout: {
        type: 'hbox',
        align: 'stretch',
        resizable: true
    },
    padding: '0 0 0 10',
    initComponent: function () {
        this.callParent(arguments);

        this.createGatewayList();
        this.addSelectMessage();
    },
    addSelectMessage: function(){
        this.lookupReference('detailContainer').removeAll();
        this.lookupReference('detailContainer').add({
            xtype: 'commonUiDisplayMessage',
            message: _tl.get('mam.label.ui.selectEntity')

        });
    },
    createGatewayList: function(){
        this.add(
            {
                xtype: 'container',
                width: 300,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype:'commonUiHeader',
                        header: _tl.get('mam.label.gateways'),
                        bind: {
                            header:  '{totalText}'
                        }

                    },
                    {
                        xtype: 'gatewayList',

                        flex: 1
                    }
                ]
            },
            {
                xtype: 'container',
                margin: '0 0 0 10',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                flex: 1,
                items: [
                    {
                        xtype:'commonUiHeader',
                        header: _tl.get('mam.label.gateway'),
                        colorClass: 'green',
                        showArrow: true,
                        bind: {
                            header: _tl.get('mam.label.gateway') + ' {gateway.customId}',
                            subheader: _tl.get('mam.label.mode.view')
                        }
                    },
                    {
                        xtype: 'container',
                        cls: 'border-lrb',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        reference: 'detailContainer',
                        flex: 1
                    }
                ]
            },
            {
                xtype: 'actionInitial',
                viewModel: {
                    data: {
                        entity: MAM.utils.Constants.GATEWAY
                    }
                }
            }
        );
    },


    createGatewayDetailContainer: function(){
        var detailContainer = this.lookupReference('detailContainer');
        detailContainer.removeAll();
        detailContainer.add(
            {
                xtype: 'gatewayDetail',
                flex: 1
            },
            {
                xtype: 'gatewaySubDetail',
                flex: 1
            }
        );
        //detailContainer.setLoading(_tl.get('mam.label.ui.loadingData'));
    }
});