Ext.define('Energy.common.ui.plugin.CountableTextField', {
    extend: 'Ext.util.Observable',
    alias: 'plugin.commonPluginCountableTextField',

    init: function (field) {
        // update counter whenever field changes
        field.on('change', this.updateCounter, field);

        if (!field.rendered) {
            field.on('afterrender', this.onAfterRender, field);
        } else {
            this.onAfterRender(field);
        }
    },

    onAfterRender: function (field) {
        field.counterEl = field.triggerWrap.createChild({
            tag: 'span',
            style: 'top: 4px; position: absolute; right: 5px; color: grey; cursor: default;',
            html: field.maxLength
        });

        new Ext.util.DelayedTask(function(){
            field.inputEl.setStyle({display:'inline',width:'95%'});
        }).delay(200);
    },

    /**
     * Updates the counter (remaining chars) according to the length of current value and maxLength
     * @param textField
     */
    updateCounter: function (textField) {
        textField.counterEl.update('' + Math.max(0, (textField.maxLength - textField.getValue().length)));
    }
});
