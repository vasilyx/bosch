/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Ariadne.DomQueryFinder.Role.DomHelper', {

    has        : {
    },

    methods : {
        
        ignoreCssClass : function (cls, dom) {
            return false
        },
        
        
        getCssClasses : function (dom) {
            var classes             = dom.className.trim()
            var significantClasses  = []
            var index               = {}

            classes                 = classes && classes.split(/\s+/) || [];

            for (var i = 0; i < classes.length; i++) {
                var cssClass            = classes[ i ]
                
                if (!index[ cssClass ]) {
                    if (!this.ignoreCssClass(cssClass, dom)) significantClasses.push(cssClass)
                    
                    index[ cssClass ]   = true
                }
            }
            
            return significantClasses
        },
        
        
        escapeCssClass : function (cls) {
            cls     = this.escapeDomSelector(cls)
            
            if (/^\d/.test(cls)) cls    = '\\' + cls
            
            return cls
        },
        
        
        escapeDomSelector : function (selector) {
             return (selector || '').replace(/([\~\!\@\$\%\^\&\*\(\)\+\=\,\.\/\'\;\:\"\?\>\<\[\]\\\{\}\|\`\#])/g, '\\$1')
        },
        
        
        // should be copied to /deps/jquery-1.6.2.js and /deps/sizzle.min.js - to the ":textEquals" pseudo 
        unEscapeDomSelector : function (selector) {
            return (selector || '').replace(/\\([\ \~\!\@\$\%\^\&\*\(\)\+\=\,\.\/\'\;\:\"\?\>\<\[\]\\\{\}\|\`\#])/g, '$1')
        }
    }
});
