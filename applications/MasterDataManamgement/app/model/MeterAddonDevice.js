Ext.define('MAM.model.MeterAddonDevice', {
    extend: 'Ext.data.Model',
    requires: [
        'MAM.model.Relation'
    ],
    idProperty: 'id',
    fields: [
        { name: 'id', type: 'int', mapping: 'meterAddonDeviceId'},
        { name: 'customId', type: 'string'},
        { name: 'key', type: 'string'},
        { name: 'lotNumber', type: 'string'},
        { name: 'manufacturer', type: 'string'},

        { name: 'deviceClass', type: 'string'},
        { name: 'deviceType', type: 'string'},
        { name: 'examinationNumber', type: 'string'},

        { name: 'calibrationEndYear', type: 'int'},
        { name: 'calibrationYear', type: 'int'},
        { name: 'constructionYear', type: 'int'},
        { name: 'charge', type: 'string'},

        { name: 'status', type: 'string'},

        { name: 'buildInDate', convert: MAM.utils.model.Converts.convertDate},
        { name: 'buildOutDate', convert: MAM.utils.model.Converts.convertDate}
    ]
});