Ext.define('MAM.view.meterAddonDevice.Detail', {
    extend: 'Ext.Panel',

    alias: 'widget.meterAddonDeviceDetail',


    bind: {
        data: '{meterAddonDevice}'
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    scrollable: true,

    initComponent: function () {

        var defaults = {
            xtype: 'displayfield',
            labelWidth: 150
        };

        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if(this.responsiveConfig){
            responsiveConfig = this.responsiveConfig;
        }

        var items = [
            {
                xtype: 'container',
                cls: 'bg-grey',
                margin: 10,
                padding: '5 10 0 10',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                defaults: defaults,
                items: this.getNorthItems()

            },
            {
                xtype: 'container',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                padding: '0 20 0 20',
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getWestItems()
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getEastItems()
                    }
                ]
            }
        ];

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    },
    /*
     * Defines the top area items for connection building properties
     */
    getNorthItems: function(){

        var items = [
            {
                fieldLabel: _tl.get('mam.label.meterAddonDevice.customId'),
                name: 'customId',
                flex: 1,
                bind: {
                    value: '{meterAddonDevice.customId}'
                }
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                flex: 1,
                items: [
                    {
                        fieldLabel: _tl.get('mam.label.meterAddonDevice.id'),
                        name: 'meterAddonDeviceId',
                        bind: {
                            value: '{meterAddonDevice.id}'
                        }
                    }
                ]
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the left side of panel
     */
    getWestItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.meterAddonDevice.deviceType'),
                name: 'deviceType',
                bind: {
                    value: '{meterAddonDevice.deviceType}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterAddonDevice.deviceClass'),
                name: 'deviceClass',
                bind: {
                    value: '{meterAddonDevice.deviceClass}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterAddonDevice.charge'),
                name: 'medium',
                bind: {
                    value: '{meterAddonDevice.charge}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterAddonDevice.manufacturer'),
                name: 'manufacturer',
                bind: {
                    value: '{meterAddonDevice.manufacturer}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterAddonDevice.calibrationYear'),
                name: 'calibrationYear',
                bind: {
                    value: '{meterAddonDevice.calibrationYear}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterAddonDevice.calibrationEndYear'),
                name: 'calibrationEndYear',
                bind: {
                    value: '{meterAddonDevice.constructionYear}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterAddonDevice.constructionYear'),
                name: 'constructionYear',
                bind: {
                    value: '{meterAddonDevice.constructionYear}'
                }
            }

        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the right side of panel
     */
    getEastItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.meterAddonDevice.buildInDate'),
                name: 'buildInDate',
                bind: {
                    value: '{meterAddonDevice.buildInDate}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterAddonDevice.buildOutDate'),
                name: 'buildOutDate',
                bind: {
                    value: '{meterAddonDevice.buildOutDate}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterAddonDevice.lotNumber'),
                name: 'lotNumber',
                bind: {
                    value: '{meterAddonDevice.lotNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterAddonDevice.examinationNumber'),
                name: 'examinationNumber',
                bind: {
                    value: '{meterAddonDevice.examinationNumber}'
                }
            }
        ];
        return items;
    }
});