Ext.define('MAM.view.meter.InitialModel', {
    extend: 'Ext.app.ViewModel',
    requires: [
        'MAM.model.Gateway',
        'MAM.utils.model.Converts'
    ],

    alias: 'viewmodel.meterInitialModel',

    data: {
        meter: null,
        editMode: false
    },

    stores: {
        registers: {
            model: 'MAM.model.Register',
            autoLoad: false,
            proxy: {
                type: 'memory'
            }
        }
    },
    formulas: {
        deviceClass: function(get) {
            if (get('meter')) {
                return  MAM.utils.model.Converts.convertDeviceClass( get('meter.deviceClass'));
            } else {
                return;
            }
        },
        status: function(get) {
            if (get('meter')) {
                return  MAM.utils.model.Converts.convertMeterStatus( get('meter.status'));
            } else {
                return;
            }
        },
        medium: function(get) {
            if (get('meter')) {
                return  MAM.utils.model.Converts.convertMedium( get('meter.medium') );
            } else {
                return;
            }
        },
        measuringPrinciple: function(get) {
            if (get('meter')) {
                return  MAM.utils.model.Converts.convertMeasuringPrinciple( get('meter.measuringPrinciple') );
            } else {
                return;
            }
        },
        totalText: {
            get: function(get) {
                var total = get('total');
                if(total === 1) {
                    return Ext.util.Format.number(total, '0,000') + ' ' + _tl.get('mam.label.meter');
                }
                else {
                    return Ext.util.Format.number(total, '0,000') + ' ' + _tl.get('mam.label.meters')
                }
            }
        },
        editModeText: function (get) {
            var param = get('editMode');
            return param ? _tl.get('mam.label.mode.edit') : _tl.get('mam.label.mode.view');
        }
    },

    cleanData: function() {
        this.set('meter', null);
    }
});