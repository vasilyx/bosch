Ext.define('MAM.view.connectionBuilding.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.connectionBuildingList',
    requires: [
        'MAM.view.connectionBuilding.ListModel',
        'MAM.view.connectionBuilding.ListController'
    ],
    viewModel: 'connectionBuildingListModel',
    controller: 'connectionBuildingListController',
    border: true,
    bind: {
        store: '{connectionBuildings}'
    },
    viewConfig: {
        loadMask: false
    },
    dockedItems: [{
        xtype: 'pagingtoolbar',
        reference: 'pagingToolbar',
        bind: {
            store: '{connectionBuildings}'
        },
        overflowHandler: 'menu',
        dock: 'bottom',
        displayInfo: true
    }],
    initComponent: function () {
        
        var columns = [
            {
                text: _tl.get('mam.label.connectionBuilding.customId'),
                dataIndex: 'customId',
                flex: 1,
                items: [{
                    xtype: 'textfield',
                    margin: 3,
                    flex: 1,
                    width: 270, //theme error
                    enableKeyEvents: true,
                    listeners: {
                        keyup: 'onCustomIdSearch',
                        buffer: 1000
                    }
                }]
            }
        ];
        Ext.apply(this, {columns: columns});
        this.callParent(arguments);
    }

});