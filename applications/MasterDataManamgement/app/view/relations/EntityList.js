Ext.define('MAM.view.relations.EntityList', {
    extend: 'Ext.panel.Panel',

    requires: [
        'MAM.view.relations.EntityListController',
        'MAM.view.relations.InitialModel'
    ],
    alias: 'widget.relationsEntityList',
    controller: 'relationsEntityListController',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    getButtonClear: function() {
        return {xtype: 'button', icon: 'resources/images/edit/clear16.png', tooltip: _tl.get("mam.edit.entityList.clear")};
    },

    getTopContainer: function () {
        return {
            xtype: 'container',
            bind: {
                html: '{topHeader}'
            },
            cls: 'entityListHeader',
            margin: 10
        }
    },

    getToolbar: function () {
        return [{
            xtype: 'textfield',
            name: 'customId',
            hideLabel: true,
            width: 200
            }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'manufacturer',
                hideLabel: true,
                width: 120
            }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'calibrationEndYear',
                hideLabel: true,
                width: 120
            }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'lotNumber',
                hideLabel: true,
                flex: 1
            }, this.getButtonClear()
        ];
    },

    getColumns: function () {
        return [
            {text: _tl.get("mam.label.meter.customId"), dataIndex: 'customId', width: 250},
            {text: _tl.get("mam.label.meter.manufacturer"), dataIndex: 'manufacturer', width: 170},
            {text: _tl.get("mam.label.meter.calibrationEndYear"), dataIndex: 'calibrationEndYear', width: 170},
            {text: _tl.get("mam.label.meter.lotNumber"), dataIndex: 'lotNumber', flex: 1}
        ];
    },

    initComponent: function () {

        var me = this;

        var items = [
            this.getTopContainer(),
            {
                xtype: 'grid',
                tbar: me.getToolbar(),
                itemId: 'entityGrid',
                bind: {
                    store: '{grid}'
                },
                flex: 1,
                emptyText: _tl.get("mam.edit.entityList.emptyText"),
                columns: me.getColumns()
            }
        ];

        var buttons = [{
            bind: {
                text: '{buttonAddRecordText}'
            },
            itemId: 'addRelation'
        }, {
            text: _tl.get("mam.edit.entityList.cancel"),
            itemId: 'closeButton'
        }];

        Ext.apply(this, {items: items, buttons: buttons});
        this.callParent(arguments);
    }
});