/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Launcher.Role.CanPrintWithLauncher', {
    
    does    : [
        Siesta.Util.Role.CanStyleOutput
    ],
    
    has : {
        launcher                : { required : true }
    },
    
    methods : {
        
        print : function (text, level) {
            return this.launcher.print(text, level)
        },
        
        
        printErr : function (text, level) {
            return this.launcher.printErr(text, level)
        },
        
        
        printError : function (text, level) {
            return this.launcher.printError(text, level)
        },
        
        
        warn : function (text, level) {
            return this.launcher.warn(text, level)
        },
        
        
        debug : function (text) {
            return this.launcher.debug(text)
        }
    }
})
