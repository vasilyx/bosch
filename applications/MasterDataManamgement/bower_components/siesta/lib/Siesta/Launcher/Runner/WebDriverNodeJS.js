/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Class('Siesta.Launcher.Runner.WebDriverNodeJS', {
    
    isa     : Siesta.Launcher.Runner.BaseRunner,
    
    has : {
        pagePollInterval    : 1000,
        
        resolutionCapName   : 'resolution',
        
        maxWorkers          : 1,
        
        capabilities        : {
            lazy        : 'this.buildCapabilities'
        }
    },
    
    
    methods : {

        buildWebDriverBuilder : function () {
            var launcher        = this.launcher
            var builder         = new selenium.Builder().withCapabilities(this.getCapabilities())
            var options         = this.dispatcher.options
            
            var ControlFlow     = require('selenium-webdriver/lib/promise').ControlFlow
            
            builder.setControlFlow(new ControlFlow())
            
            if (options[ 'host' ]) {
                // prepend potentially missing "http://" prefix in "constructURL"
                var url             = this.launcher.constructUrl(options[ 'host' ])
                
                if (!/\/wd\/hub\s*$/i.test(url)) url += ':' + (options[ 'port' ] || 4444) + '/wd/hub'
                
                builder.usingServer(url)
            }
            
            var browserArgs     = options[ 'browser-arg' ]
            
            if (options.browser == 'chrome') {
                var chrome          = require('selenium-webdriver/chrome')
                var chromeOptions   = new chrome.Options()
                
                if (options.xvfb && launcher.isLinux) {
                    chromeOptions.setChromeBinaryPath(launcher.binDir + 'xvfb/chrome')
                }
                
                if (options[ 'touch-events' ]) chromeOptions.addArguments('touch-events')
                
                if (this.launcher.isWindows) chromeOptions.addArguments('disable-gpu')
                
                for (var i = 0; i < browserArgs.length; i++) {
                    chromeOptions.addArguments(browserArgs[ i ])
                }
                
                builder.setChromeOptions(chromeOptions)
            }
            
            if (options.browser == 'firefox') {
                // passing command line options is not supported for remote
                if (!options.host && (browserArgs.length || options.xvfb)) {
                    var firefox         = require('selenium-webdriver/firefox')
                    var firefoxOptions  = new firefox.Options()
                    
                    var binary          = new firefox.Binary(
                        options.xvfb && launcher.isLinux ? launcher.binDir + 'xvfb/firefox' : undefined
                    )
                    
                    for (var i = 0; i < browserArgs.length; i++) {
                        binary.addArguments(browserArgs[ i ])
                    }
                    
                    firefoxOptions.setBinary(binary)
                    
                    if (options[ 'firefox-profile' ]) {
                        var profile = new firefox.Profile(options[ 'firefox-profile' ])
                        firefoxOptions.setProfile(profile)
                    }
                    
                    builder.setFirefoxOptions(firefoxOptions)
                }
            }
            
            if (options.browser == 'internet explorer') {
                // the call to "setIeOptions" changes the "platform" capability to "WINDOWS" 
                // (as implemented in the "selenium-webdriver")
                // this breaks the SL testing (which requires "platform" to be set to "WINDOWS 7")
                // only supporting for local testing for now, until someone complains
                if (!options.host && browserArgs.length) {
                    var ie              = require('selenium-webdriver/ie')
                    var ieOptions       = new ie.Options()
                    
                    if (browserArgs.length) ieOptions.forceCreateProcessApi(true)
                    
                    for (var i = 0; i < browserArgs.length; i++) {
                        ieOptions.addArguments(browserArgs[ i ])
                    }
                    
                    // this call changes the "platform" capability to "WINDOWS" (as implemented in the "selenium-webdriver")
                    builder.setIeOptions(ieOptions)
                }
            }
            
            return builder
        },
        
        
        buildCapabilities : function () {
            var options         = this.dispatcher.options
            
            var capabilities    = new selenium.Capabilities()
            
            var width           = options.width || Siesta.Launcher.Page.WebDriverNodeJS.prototype.width
            var height          = options.height || Siesta.Launcher.Page.WebDriverNodeJS.prototype.height
            
            capabilities.set(this.resolutionCapName, width + 'x' + height)
            
            var caps            = options.cap
            
            for (var i = 0; i < caps.length; i++) {
                var match       = /(.*?)=(.*)/.exec(caps[ i ])
                match && capabilities.set(match[ 1 ] || '', match[ 2 ] || '')
            }
            
            // intentially overwrite any user-provided "browserName" which has been normalized
            capabilities.set('browserName', options.browser)
            
            if (this.proxyHost) {
                var proxyStr    = this.getProxyHostPort()
                
                capabilities.setProxy({
                    proxyType       : 'manual',
                    ftpProxy        : proxyStr,
                    httpProxy       : proxyStr,
                    sslProxy        : proxyStr
                });
            }
            
            return capabilities
        },
        

        // promised method
        createDriverInstance : function (builder, capabilities, level) {
            level           = level || 0
            var options     = this.dispatcher.options
            var me          = this
            
            this.debug("Trying to create a WebDriver instance for browser: " + options.browser)
            
            return Promise.resolve(builder.buildAsync()).then(function (driver) {
                me.debug("WebDriver instantiated successfully")
                
                var config      = {
                    runner          : me,
                    launcher        : me.launcher,
                    dispatcher      : me.dispatcher,
                    
                    pollInterval    : me.pagePollInterval,
                    
                    driver          : driver
                }
                
                if (options.width) config.width     = options.width
                if (options.height) config.height   = options.height
                
                return new Siesta.Launcher.Page.WebDriverNodeJS(config)
                
            }, function (e) {
                me.debug("WebDriver instantiation failed: " + options.browser + ", level : " + level)
                me.debug("Failed with: " + e)
                
                var canTryToRepeat  = me.onWebDriverInstatiationException(e, builder, capabilities)
                
                // try to instantiate the driver 3 times, then throw exception
                if (level < 2 && canTryToRepeat) {
                    me.debug("Trying again")
                    
                    return me.createDriverInstance(builder, capabilities, level + 1)
                } else
                    throw e
            })
        },
        
        
        onWebDriverInstatiationException : function (e, builder, capabilities) {
            return true
        },
        
        
        createPage : function (callback) {
            var capabilities    = this.getCapabilities()
            var builder         = this.buildWebDriverBuilder()
            
            this.createDriverInstance(builder, capabilities).then(function (page) {
                callback(null, page)
            }, function (e) {
                callback(e, null)
            })
        }
    }
})
