/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Util.Role.Dom', {

    has : {
        doesNotIncludeMarginInBodyOffset : false
    },
    
    methods : {
        
        isCrossOriginWindow : function (win) {
            try {
                var doc     = win.document;
            } catch (e) { 
                return true
            }
            
            // Safari doesn't throw exception when trying to access x-domain frames
            return !doc
        },
        

        closest : function (elem, selector, maxLevels) {
            maxLevels = maxLevels || Number.MAX_VALUE;

            var docEl = elem.ownerDocument.documentElement;

            // Get closest match
            for (var i = 0; i < maxLevels && elem && elem !== docEl; elem = elem.parentNode ){
                if (Sizzle.matchesSelector(elem, selector)) {
                    return elem;
                }

                i++;
            }

            return false;
        },

        
        contains : function (container, node) {
            if (!container) return false
            
            return container.contains(node)
        },

        
        is : function (node, selector) {
            return Sizzle.matchesSelector(node, selector);
        },

        
        offset : function (elem) {
            var box;

            if (!elem || !elem.ownerDocument) {
                return null;
            }

            if (elem === elem.ownerDocument.body) {
                return this.bodyOffset(elem);
            }

            try {
                box = elem.getBoundingClientRect();
            } catch (e) {
            }

            var doc     = elem.ownerDocument,
                docElem = doc.documentElement;

            // Make sure we're not dealing with a disconnected DOM node
            if (!box || !this.contains(docElem, elem)) {
                return box ? { top : box.top, left : box.left } : { top : 0, left : 0 };
            }

            var body       = doc.body,
                win        = doc.defaultView || doc.parentWindow,
                clientTop  = docElem.clientTop || body.clientTop || 0,
                clientLeft = docElem.clientLeft || body.clientLeft || 0,
                scrollTop  = win.pageYOffset || docElem.scrollTop || body.scrollTop,
                scrollLeft = win.pageXOffset || docElem.scrollLeft || body.scrollLeft,
                top        = box.top + scrollTop - clientTop,
                left       = box.left + scrollLeft - clientLeft;

            return { top : top, left : left };
        },

        
        bodyOffset: function (body) {
            var top     = body.offsetTop,
                left    = body.offsetLeft;

            this.initializeOffset();

            if (this.doesNotIncludeMarginInBodyOffset) {
                top     += parseFloat(jQuery.css(body, "marginTop")) || 0;
                left    += parseFloat(jQuery.css(body, "marginLeft")) || 0;
            }

            return { top: top, left: left };
        },

        
        initializeOffset: function () {
            var body        = document.body, 
                container   = document.createElement("div"), 
                bodyMarginTop = parseFloat(jQuery.css(body, "marginTop")) || 0,
                html        = "<div style='position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;'><div></div></div><table style='position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;' cellpadding='0' cellspacing='0'><tr><td></td></tr></table>";

            var styles      = { position: "absolute", top: 0, left: 0, margin: 0, border: 0, width: "1px", height: "1px", visibility: "hidden" };

            for (var o in styles) {
                container.style[ o ] = styles[ o ];
            }

            container.innerHTML     = html;
            
            body.insertBefore(container, body.firstChild);
            
            var innerDiv            = container.firstChild;
            var checkDiv            = innerDiv.firstChild;
            var td                  = innerDiv.nextSibling.firstChild.firstChild;

            checkDiv.style.position = "fixed";
            checkDiv.style.top      = "20px";
            checkDiv.style.position = checkDiv.style.top = "";

            innerDiv.style.overflow = "hidden";
            innerDiv.style.position = "relative";

            this.doesNotIncludeMarginInBodyOffset = (body.offsetTop !== bodyMarginTop);

            body.removeChild(container);
            
            this.initializeOffset   = function () {};
        },

        
        getElementWidth : function (el) {
            return el.getBoundingClientRect().width;
        },


        getElementHeight : function (el) {
            return el.getBoundingClientRect().height;
        },
        
        
        isElementReachableAt : function (el, x, y, allowChild) {
            allowChild      = allowChild !== false
            
            var doc         = el.ownerDocument;
            var foundEl     = doc.elementFromPoint(x, y);

            return foundEl && (foundEl === el || allowChild && this.contains(el, foundEl));
        },

        
        isElementReachableAtCenter : function (el, allowChild) {
            allowChild      = allowChild !== false
            
            var offsets     = this.offset(el);

            return this.isElementReachableAt(
                el, 
                offsets.left + (this.getElementWidth(el) / 2), 
                offsets.top + (this.getElementHeight(el) / 2),
                allowChild
            );
        }
    }
})
