/**
 * Displays a tooltip with an info icon and a text when hovering the icon
 * Usage example:
 * {
 *  xtype: 'Tooltip'
 *  quicktipHeader: 'header of the quicktip',
 *  quicktip: 'Explain why this field is neccesary or what happens here',
 *  type: 'error' //information is default, warning
 *  }
 */

Ext.define("Energy.common.ui.FormTooltip", {
    extend: 'Ext.Container',

    alias: 'widget.commonFormTooltip',

    cls: 'energy-tooltip energy-info-blue-img',
    width: 20,
    type: 'information',
    quicktipHeader: undefined,
    quicktip: 'Explain why this field is neccesary or what happens here',

    initComponent: function () {
        if (!(Ext.isEmpty(this.type)) && this.type !== 'information') {
            this.setType(this.type);
        }

        this.on('afterrender', this.onAfterRender, this);

        this.callParent();
    },

    onAfterRender: function (me) {
        // Register the new tip with an element's ID
        Ext.tip.QuickTipManager.register({
            target: me.getId(), // Target button's ID
            title: me.quicktipHeader,  // QuickTip Header
            text: me.quicktip // Tip content
        });
    },

    /**
     * sets the type (icon) to the quicktip
     * @param type
     */
    setType: function (type) {
        var typeMapping = {
            'information': 'energy-info-blue-img',
            'warning': 'energy-warning-img',
            'error': 'energy-error-img',
            'success': 'energy-success-img',
            'question': 'energy-question-img'
        };

        // Set the current css class to the element
        this.addCls(typeMapping[type]);
        delete typeMapping[type];

        // Remove all other type specific css classes from element
        for (var property in typeMapping) {
            if (typeMapping.hasOwnProperty(property)) {
                this.removeCls(typeMapping[property]);
            }
        }
    }
});
