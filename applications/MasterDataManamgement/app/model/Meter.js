Ext.define('MAM.model.Meter', {
    extend: 'Ext.data.Model',
    requires: [
        'MAM.utils.model.Converts',
        'MAM.model.Register'
    ],
    idProperty: 'id',
    fields: [
        { name: 'id', type: 'int', mapping: 'meterId'},
        { name: 'customId', type: 'string'},
        { name: 'status', type: 'string'},
        { name: 'deviceType', type: 'string'},
        { name: 'deviceClass', type: 'string'},
        { name: 'deviceKind', type: 'string'},

        { name: 'manufacturer', type: 'string'},
        { name: 'factoryNumber', type: 'string'},
        { name: 'deliveryNoteNumber', type: 'string'},
        
        { name: 'calibrationYear', type: 'string'},
        { name: 'calibrationEndYear', type: 'string'},
        { name: 'constructionYear', type: 'string'},

        { name: 'buildInDate',  convert: MAM.utils.model.Converts.convertDateWithoutHour },
        { name: 'buildOutDate', convert: MAM.utils.model.Converts.convertDateWithoutHour },

        { name: 'lotNumber', type: 'string'},
        { name: 'examinationNumber', type: 'string'},
        { name: 'charge', type: 'string'},

        { name: 'paramterVersion', type: 'string'},
        { name: 'firmwareVersion', type: 'string'},
        { name: 'hardwareVersion', type: 'string'},

        { name: 'interface', type: 'string'},
        { name: 'comAddress', type: 'string'},
        { name: 'serverId', type: 'string'},
        { name: 'publicKey', type: 'string'},
        { name: 'keyM', type: 'string'},

        { name: 'medium', type: 'string'},
        { name: 'meterSize', type: 'string'},
        { name: 'tariffCount', type: 'string'},
        { name: 'energyFlowDirection', type: 'string'},
        { name: 'mountingType', type: 'string'},
        { name: 'meterReadingType', type: 'string'},
        { name: 'measuringPrinciple', type: 'string'},
        { name: 'registers'},
       /* { name: 'registers', reference: 'MAM.model.Register'}, //TODO: check why not working, so automapping is defined */

        
        { name: 'customAttributes', reference: 'MAM.model.CustomAttribute' },
        { name: 'relations', reference: 'MAM.model.Relation' }

     //   { name: 'createdTimeStamp', convert: MAM.utils.model.Converts.convertDate } // No on backend side

    ]

 });