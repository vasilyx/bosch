/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Launcher.FileSystem.Rhino', {
    
    does        : Siesta.Launcher.FileSystem.Base,
    
    has : {
    },
    
    
    methods : {
        
        fileExists : function (fileName) {
            return new java.io.File(this.normalizePath(fileName)).exists()
        },
        
        
        readFile : function (fileName) {
            return new java.util.Scanner(new java.io.File(this.normalizePath(fileName)), "UTF-8").useDelimiter("\\A").next()
        },
        
        
        ensurePathExists : function (fileName) {
            fileName        = this.normalizePath(fileName)
            
            var file        = new java.io.File(fileName)
            
            var parentDir   = file.getParentFile()
            
            if (parentDir && !parentDir.exists()) return parentDir.mkdirs()
            
            return true
        },
        
        
        saveFile : function (fileName, content) {
            fileName        = this.normalizePath(fileName)
            
            this.ensurePathExists(fileName)
            
            var out     = new java.io.BufferedWriter(new java.io.FileWriter(fileName))
            out.write(String(content))
            out.close()
        },
        
        
        copyFile : function (source, dest) {
            var destFile        = new java.io.File(this.normalizePath(dest))
            var sourceFile      = new java.io.File(this.normalizePath(source))
            
            this.ensurePathExists(dest)
            
            if (!destFile.exists()) destFile.createNewFile()
        
            var sourceChannel, destChannel
            
            try {
                sourceChannel   = new java.io.FileInputStream(sourceFile).getChannel()
                destChannel     = new java.io.FileOutputStream(destFile).getChannel()
                
                destChannel.transferFrom(sourceChannel, 0, sourceChannel.size())
            } finally {
                if (sourceChannel) sourceChannel.close()
                if (destChannel) destChannel.close()
            }            
        },
        
        
        copyTree : function (source, dest) {
            org.apache.commons.io.FileUtils.copyDirectory(
                new java.io.File(this.normalizePath(source)), 
                new java.io.File(this.normalizePath(dest))
            )
        },
        
        
        normalizePath : function (path) {
            return this.isWindows ? path.replace(/\//g, '\\') : path
        }
    }
})
