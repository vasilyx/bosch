Ext.define('MAM.view.meter.EditSubDetail', {
    extend: 'Ext.tab.Panel',
    requires: [
        'MAM.view.relations.Edit',
        'MAM.view.customAttributes.Edit',
        'MAM.view.customAttributes.InitialController'
    ],
    alias: 'widget.meterEditSubDetail',

    initComponent: function () {
        var items = [];
        var tabConfig = MAM.utils.Configuration.getTabConfig( MAM.utils.Constants.METER );

        Ext.each(tabConfig, function( tab ){
            if (tab.type == 'customAttributes') {
                items.push({
                    xtype: tab.type + 'Edit',
                    title: _tl.get("mam.label." + MAM.utils.Constants.METER + "." + tab.type),
                    itemId: MAM.utils.Constants.METERS + '-tab-' + tab.type,
                    viewModel: {
                        type: 'customAttributesInitialModel',
                        data: {
                            entity: MAM.utils.Constants.METER
                        }
                    }
                })
            } else {
                items.push({
                    xtype: tab.type + 'Edit',
                    title: _tl.get("mam.label." + MAM.utils.Constants.METER + "." + tab.type),
                    itemId: MAM.utils.Constants.METERS + '-tab-' + tab.type
                })
            }
        });

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    }
});