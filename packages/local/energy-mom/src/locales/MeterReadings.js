Ext.define("Energy.mom.locales.MeterReadings", {
    extend: 'Energy.common.language.TranslationPackage',
    alternateClassName: ['LangMeterReadings'],
    statics: {
        en: {
            meterReadings: 'Meter Readings',
            readingDate: 'Reading Date',
            meterId: 'Meternumber',
            reason: 'Meter Reading Type',
            reasonType_a: 'Build-In-Reading',
            reasonType_i: 'Build-Out-Reading',
            reasonType_t: 'Re-Reading',
            reasonType_e: 'Single-Reading',
            reasonType_z: 'Between-Reading',
            reasonType_s: 'Special-Reading',
            value: 'Value',
            unit: 'Unit',
            meter: 'Meter',
            registerName: 'Register',
            noValues: 'No readings available.',
            close: 'Close',
            meterPoint: 'Meter Point'
        },
        de: {
            meterReadings: 'Zeitscheiben aller Zählerstände',
            readingDate: 'Lesedatum',
            meterId: 'Zählernummer',
            reason: 'Zählerstandsart',
            reasonType_a: 'Ausbauzählerstand',
            reasonType_i: 'Einbauzählerstand',
            reasonType_t: 'Turnusablesung',
            reasonType_e: 'Einzelablesung',
            reasonType_z: 'Zwischenablesung',
            reasonType_s: 'Sonderablesung',
            value: 'Zählerstand',
            unit: 'Messeinheit',
            meter: 'Zähler',
            registerName: 'Register',
            noValues: 'Keine Auslesung vorhanden.',
            close: 'Schließen',
            meterPoint: 'Zählpunkt'
        }
    }
});