/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Launcher.Script.CanSplitTextIntoChunks', {
    
    methods : {
        
        splitIntoChunks: function (text, maxChunkSize) {
            var length          = text.length
            
            var pos             = 0
            var chunks          = []
            
            while (pos < length) {
                var chunk       = null
                var chunkSize   = maxChunkSize * 0.8
                
                while (!chunk) {
                    // while stringifying, the size of the chunk can increase, we don't know how much upfront
                    chunk       = JSON.stringify(text.substr(pos, chunkSize))
                    
                    if (chunk.length > maxChunkSize) {
                        chunk           = null
                        chunkSize       = Math.floor(chunkSize * 0.8)
                        
                        if (!chunkSize) throw "Chunk size zero"
                    }
                }
                
                chunks.push(chunk)
                
                pos             += chunkSize
            }
            
            return chunks
        }
    }
    // eof methods
})

