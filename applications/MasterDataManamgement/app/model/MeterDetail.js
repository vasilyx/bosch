Ext.define('MAM.model.MeterDetail', {
    extend: 'MAM.model.Meter',
    proxy: {
        type: 'rest',
        api: {
            create: '/MAM/' + MAM.Config.Backend.Versions + '/meters',
            read: '/MAM/' + MAM.Config.Backend.Versions + '/meters',
            update: '/MAM/' + MAM.Config.Backend.Versions + '/meters',
            destroy: '/MAM/' + MAM.Config.Backend.Versions + '/meters'
        },
        actionMethods: {
            create: 'POST',
            read: 'GET',
            update: 'PUT',
            destroy: 'DELETE'
        },
        writer: {
            writeAllFields: true,
            type: 'json',
            transform: {
                fn: function(data, request) {
                    // for backend
                    data.buildInDate = Ext.Date.format(Ext.Date.parse(data.buildInDate, "d.m.Y"), "Y-m-d");
                    data.buildOutDate = Ext.Date.format(Ext.Date.parse(data.buildOutDate, "d.m.Y"), "Y-m-d");

                    // for backend
                    delete data.validFrom;

                    return data;
                },
                scope: this
            }
        },
        headers: {
            'Accept': 'application/json'
        },
        extraParams: {
            expand: 'properties'
        }
    }
});