Ext.define('MAM.model.SimCard', {
    extend: 'Ext.data.Model',
    requires: [
        'MAM.model.Relation'
    ],
    idProperty: 'id',
    fields: [
        { name: 'id', type: 'int', mapping: 'simCardId'},
        { name: 'customId', type: 'string'},
        { name: 'iccid', type: 'string'},
        { name: 'format', type: 'string'},
        { name: 'provider', type: 'string'},

        { name: 'serviceProfile', type: 'string'},
        { name: 'tariffName', type: 'string'},
        { name: 'phoneNumber', type: 'string'},
        { name: 'apn', type: 'string'},

        { name: 'username', type: 'string'},
        { name: 'password', type: 'string'},
        { name: 'compType', type: 'string'},

        { name: 'ipAddress', type: 'string'},
        { name: 'subnetMask', type: 'string'},
        { name: 'defaultGateway', type: 'string'},


        { name: 'status', type: 'string'},

        { name: 'activationDate', convert: MAM.utils.model.Converts.convertDate}
    ]
});