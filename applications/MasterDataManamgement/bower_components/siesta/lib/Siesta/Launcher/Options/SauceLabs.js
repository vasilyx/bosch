/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Launcher.Options.SauceLabs', {
    
    override : {
        
        validate : function () {
            if (this.SUPER() === false) return false

            var options             = this.options
            
            var slUserName          = options[ 'saucelabs-user' ]
            var slKey               = options[ 'saucelabs-key' ]
            var slTunneledDomains
            
            var slShortCut          = options[ 'saucelabs' ] || options[ 'saucelabs_' ]
            
            if (slShortCut) {
                var parts           = (slShortCut === true ? '' : slShortCut).split(',')
                
                slUserName          = parts.shift() || process.env.SL_USER
                slKey               = parts.shift() || process.env.SL_KEY
                
                slTunneledDomains   = parts.join(',') || process.env.SL_TUNNEL
                
                if (!slUserName) {
                    this.printError("No SauceLabs user name found for --saucelabs shortcut")
                    return false
                }
            }
            
            if (slUserName && !slKey) {
                this.printError("No SauceLabs api key provided, for user name: " + slUserName)
                return false
            }
            
            if (slUserName && slKey) {
                if (options.host) {
                    this.printError("SauceLabs mode is enaled, but host is already set to: " + options.host)
                    return false
                }
                
                options.host        = "http://" + slUserName + ":" + slKey + "@ondemand.saucelabs.com:80/wd/hub"
                options.port        = 4444
                
                this.debug("--host set to " + options.host.replace(slKey, 'SAUCELABS_API_KEY'))
                
                options.sl          = {
                    userName        : slUserName,
                    key             : slKey,
                    shortCut        : slShortCut,
                    tunneledDomains : slTunneledDomains,
                    noTunnel        : Boolean(options[ 'saucelabs_' ])
                }
                
                if (options.build) this.addCapability('build', options.build)
            }
            
            return true
        }
    }
})
