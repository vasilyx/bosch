Ext.define('Energy.mom.model.MarketParticipant', {
    extend: 'Ext.data.Model',
    requires: [
        'Energy.mom.util.Converts'
    ],
    idProperty: 'id',
    fields: [
        { name: 'id', type: 'int'},
        { name: 'role', type: 'string'},
        { name: 'code', type: 'string'},
        { name: 'name', type: 'string'},
        { name: 'from', type: 'date'},
        { name: 'to', type: 'date'},
        { name: 'state', type: 'string', convert: Energy.mom.util.Converts.getMarketParticipantsState},
        { name: 'dateFrom', type: 'string', convert: Energy.mom.util.Converts.convertDateMarketParticipants},
        { name: 'dateTo', type: 'string', convert: Energy.mom.util.Converts.convertDateMarketParticipants},
        { name: 'groupTitle', type: 'string', convert: Energy.mom.util.Converts.getMarketParticipantsGroupTitle}
    ]
});