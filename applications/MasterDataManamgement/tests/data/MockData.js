Ext.define('TestApplication.data.MockData', {
    requires: [
        'Ext.ux.ajax.SimManager',
        'Ext.ux.ajax.DataSimlet',
        'Energy.common.ux.ajax.PlainJsonSimlet',
        'Ext.Ajax'
    ],
    //singleton: true,
    constructor: function () {

        var simMgr = Ext.ux.ajax.SimManager.init({
            delay: 150,
            defaultSimlet: null
        });

        this.simMgr = simMgr;

        this.initializeForTest();

    },
    /**
     * put here your mocked data to intilize the application
     * correct for running tests
     * data changes can be done in the test via set mock
     * data for each mocked url
     */
    initializeForTest: function () {
        this.getMockedData('tests/data/meterPoint.json');
        this.getMockedData('tests/data/gateway.json');
        this.getMockedData('tests/data/meter.json');
        this.getMockedData('tests/data/connectionBuilding.json');
        this.getMockedData('tests/data/person.json');
        this.getMockedData('tests/data/marketParticipant.json');
    },

    /**
     * We can use a fake date for testing time
     * relevant functions and views
     * @param year
     * @param month: month is counted from 0!
     * @param day
     */
    setMockTodayDate: function (year, month, day) {
        var _Date = Date;
        //override Date object to have always the
        //same date for new Date()
        Date = function (Date) {
            FakeDate.prototype = Date.prototype;
            FakeDate.testYear = year;
            FakeDate.testMonth = month;
            FakeDate.testDay = day;
            FakeDate.parse = Date.parse;
            FakeDate.UTC = Date.UTC;
            FakeDate.now = function () {
                return new _Date(this.testYear, this.testMonth, this.testDay).valueOf();
            };
            return FakeDate;

            //Fake date is is set a test case level
            function FakeDate(y, m, d, h, M, s, ms) {
                var date, now = null;

                switch (arguments.length) {
                    case 0:
                        if (now !== null) {
                            date = new _Date(now);
                        } else {
                            date = new _Date(FakeDate.testYear, FakeDate.testMonth, FakeDate.testDay);
                        }
                        break;

                    case 1:
                        date = new _Date(y);
                        break;

                    default:
                        m = m || 0;
                        d = d || 1;
                        h = h || 0;
                        M = M || 0;
                        s = s || 0;
                        ms = ms || 0;
                        date = new _Date(y, m, d, h, M, s, ms);
                        break;
                }

                return date;

            }
        }(Date);
    },

    getMockedData: function (dataLocation) {
        var me = this;
        Ext.Ajax.request({
            url: dataLocation,
            success: function (response) {
                if (dataLocation.startsWith('tests/data/meterPoint')) {
                    me.registerMeterPointsUrl(Ext.JSON.decode(response.responseText));
                }
                else if (dataLocation.startsWith('tests/data/gateway')) {
                    me.registerGatewaysUrl(Ext.JSON.decode(response.responseText));
                }
                else if (dataLocation.startsWith('tests/data/meter')) {
                    me.registerMeterUrl(Ext.JSON.decode(response.responseText));
                }
                else if (dataLocation.startsWith('tests/data/person')) {
                    me.registerPersonUrl(Ext.JSON.decode(response.responseText));
                }
                else if (dataLocation.startsWith('tests/data/connectionBuilding')) {
                    me.registerConnectionBuildingUrl(Ext.JSON.decode(response.responseText));
                }
                else if (dataLocation.startsWith('tests/data/marketParticipant')) {
                    me.registerMarketParticipantUrl(Ext.JSON.decode(response.responseText));
                }
            }
        });
    },

    getMockedDataDetail: function (dataLocation, id) {
        var me = this;
        Ext.Ajax.request({
            url: dataLocation,
            success: function (response) {
                if (dataLocation.startsWith('tests/data/detail/meterPoints')) {
                    me.registerMeterPointsDetailUrl(Ext.JSON.decode(response.responseText), me.getDetailTypeFromPath(dataLocation), id);
                }
                else if (dataLocation.startsWith('tests/data/detail/gateways')) {
                    me.registerGatewaysDetailUrl(Ext.JSON.decode(response.responseText), me.getDetailTypeFromPath(dataLocation), id);
                }
                else if (dataLocation.startsWith('tests/data/detail/connectionBuildings')) {
                    me.registerConnectionBuildingsDetailUrl(Ext.JSON.decode(response.responseText), me.getDetailTypeFromPath(dataLocation), id);
                }
                else if (dataLocation.startsWith('tests/data/detail/persons')) {
                    me.registerPersonDetailUrl(Ext.JSON.decode(response.responseText), me.getDetailTypeFromPath(dataLocation), id);
                }
                else if (dataLocation.startsWith('tests/data/detail/meters')) {
                    me.registerMeterDetailUrl(Ext.JSON.decode(response.responseText), me.getDetailTypeFromPath(dataLocation), id);
                }
                else if (dataLocation.startsWith('tests/data/detail/marketParticipantsHistory')) {
                    me.registermarketParticipantsHistoryDetailUrl(Ext.JSON.decode(response.responseText), me.getDetailTypeFromPath(dataLocation));
                }
                else if (dataLocation.startsWith('tests/data/detail/meterReadingsHistory')) {
                    me.registermeterReadingsHistoryDetailUrl(Ext.JSON.decode(response.responseText), me.getDetailTypeFromPath(dataLocation));
                }
                else if (dataLocation.startsWith('tests/data/detail/marketParticipants')) {
                    me.registerMarketParticipantUrlDetailUrl(Ext.JSON.decode(response.responseText), me.getDetailTypeFromPath(dataLocation), id);
                }
                else if (dataLocation.startsWith('tests/data/detail/attachments')) {
                    me.registerAttachmentDetailUrl(Ext.JSON.decode(response.responseText), me.getDetailTypeFromPath(dataLocation), id);
                }
                else if (dataLocation.startsWith('tests/data/detail/plants')) {
                    me.registerPlantDetailUrl(Ext.JSON.decode(response.responseText), me.getDetailTypeFromPath(dataLocation), id);
                }
                else if (dataLocation.startsWith('tests/data/detail/simCards')) {
                    me.registerSimCardDetailUrl(Ext.JSON.decode(response.responseText), me.getDetailTypeFromPath(dataLocation), id);
                }
                else if (dataLocation.startsWith('tests/data/detail/transformers')) {
                    me.registerTransformerDetailUrl(Ext.JSON.decode(response.responseText), me.getDetailTypeFromPath(dataLocation), id);
                }
                else if (dataLocation.startsWith('tests/data/detail/meterAddonDevices')) {
                    me.registerMeterAddonDeviceDetailUrl(Ext.JSON.decode(response.responseText), me.getDetailTypeFromPath(dataLocation), id);
                }
            }
        });
    },

    getMockedDataDocument: function (dataLocation, id, headers) {
        var me = this;
        Ext.Ajax.request({
            url: dataLocation,
            binary:  true,
            success: function (response) {
                if (dataLocation.startsWith('tests/data/document')) {
                    me.registerDocumentUrl(response, id, headers);
                }
            }
        });
    },

    /**
     *
     * @param dataLocation
     * @param method
     * @param status
     * @param statusText
     */
    setMockedResponse: function (dataLocation, method, status, statusText ) {
        var me = this;

        var path = me.getPrefixPath() + dataLocation,
            status = (status) ? status : 200,
            method = (method) ? method : 'get';

        var simlet = me.simMgr.getSimlet(path);
        // rewrite status/method/statusText
        if (simlet) {
            simlet.status =  status;
            simlet.method =  method;
            simlet.statusText = statusText;
            simlet.responseText = statusText;
        } else {
            var b = {};
            b[path] = {
                stype: 'plainjson',
                method: method,
                status: status,
                statusText: statusText,
                responseText: statusText,

                data: {
                    status: 333
                }
            };
            me.simMgr.register(b);
        }
    },


    getPrefixPath: function()
    {
        return '/MAM/v2/';
    },

    getDetailTypeFromPath: function (url) {
        var types = ['relations', 'customAttributes', 'document'],
            result = '';

        types.some(function (v, i) {
            if (url.search(v) != -1) {
                result = v;
            }
        });

        return result;
    },

    buildDummyDetailsUrls: function(postfix, entity, entityId, id) {
        if (postfix && id) {
            if (postfix == 'relations') {
                var path = this.getPrefixPath() + 'relations';
                //     var path = this.getPrefixPath() + 'relations?compType=' + entity +'&compId=' + id + '&expand=none&id=' + id;
            } else {
                var path = this.getPrefixPath() + entity + 's/' + id + '/' + postfix;
            }
        } else {
            var path = this.getPrefixPath() +  entity + 's/' + entityId;
        }
        return path;
    },

    /**
     * register the simlets to get data for
     * the mocked requests
     * @param path
     * @param responseData
     * return void
     */
    registrationSimlet: function (path, responseData) {
        // check if the url is already
        //registered, if yes: change the data
        //if no: register the url
        var simlet = this.simMgr.getSimlet(path);
        if (simlet) {
            simlet.data = responseData;
        }
        else {
            var b = {};
            b[path] = {
                stype: 'plainjson',
                data: responseData
            };

            this.simMgr.register(b);
        }
    },

    registerMeterPointsUrl: function (responseData) {
        var path = this.getPrefixPath() + 'meterPoints';
        this.registrationSimlet(path, responseData);
    },

    registerMeterPointsDetailUrl: function (responseData, postfix, id) {
        var path = this.buildDummyDetailsUrls(postfix, 'meterPoint', responseData.meterPointId, id);
        this.registrationSimlet(path, responseData);
    },

    registerMeterUrl: function (responseData) {
        var path = this.getPrefixPath() + 'meters';
        this.registrationSimlet(path, responseData);
    },

    registerMeterDetailUrl: function (responseData, postfix, id) {
        var path = this.buildDummyDetailsUrls(postfix, 'meter', responseData.meterId, id);
        this.registrationSimlet(path, responseData);
    },

    registerGatewaysUrl: function (responseData) {
        var path = this.getPrefixPath() + 'gateways';
        this.registrationSimlet(path, responseData);
    },

    registerGatewaysDetailUrl: function (responseData, postfix, id) {
        var path = this.buildDummyDetailsUrls(postfix, 'gateway', responseData.gatewayId, id);

        this.registrationSimlet(path, responseData);
    },

    registerPersonUrl: function (responseData) {
        var path = this.getPrefixPath() + 'persons';
        this.registrationSimlet(path, responseData);

    },

    registerPersonDetailUrl: function (responseData, postfix, id) {
        var path = this.buildDummyDetailsUrls(postfix, 'person', responseData.personId, id);
        this.registrationSimlet(path, responseData);
    },

    registerConnectionBuildingUrl: function (responseData) {
        var path = this.getPrefixPath() + 'connectionBuildings';
        this.registrationSimlet(path, responseData);
    },

    registerConnectionBuildingsDetailUrl: function (responseData, postfix, id) {
        var path = this.buildDummyDetailsUrls(postfix, 'connectionBuilding', responseData.connectionBuildingId, id);
        this.registrationSimlet(path, responseData);
    },

    registerAttachmentDetailUrl: function (responseData, postfix, id) {
        var path = this.buildDummyDetailsUrls(postfix, 'attachment', responseData.id, id);
        this.registrationSimlet(path, responseData);
    },

    registermarketParticipantsHistoryDetailUrl: function (responseData, postfix) {
        var path = '/MOM/v1/meterPoints/' + responseData.body.customId+ '/marketParticipants/history';
        var simlet = this.simMgr.getSimlet(path);
        if (simlet) {
            simlet.data = responseData;
        }
        else {
            var b = {};
            b[path] = {
                stype: 'plainjson',
                data: responseData
            };

            this.simMgr.register(b);
        }
    },

    registermeterReadingsHistoryDetailUrl: function (responseData, postfix) {
        var path = '/MOM/v1/meterPoints/' + responseData.body.customId+ '/meters/meterReadings/history';
        var simlet = this.simMgr.getSimlet(path);
        if (simlet) {
            simlet.data = responseData;
        }
        else {
            var b = {};
            b[path] = {
                stype: 'plainjson',
                data: responseData
            };

            this.simMgr.register(b);
        }
    },

    registerDocumentUrl: function (responseData, id, headers) {
        var path = this.getPrefixPath() + 'attachments/' + id + '/document';
        var simlet = this.simMgr.getSimlet(path);
        if (simlet) {
            simlet.data = responseData;
        }
        else {
            var b = {};
            var responseHeaders = {
                // 'Content-Type' : "image/jpeg",
                'Content-Length' : responseData.responseBytes.length,
                'responseBytes': responseData.responseBytes
            };

            if (headers) {
                Ext.merge(responseHeaders, headers);
            }

            b[path] = {
                binary: true,
                responseHeaders: responseHeaders,
                data: responseData
            };

            this.simMgr.register(b);
        }
    },

    registerMarketParticipantUrl: function (responseData) {
        var path = this.getPrefixPath() + 'marketParticipants';
        this.registrationSimlet(path, responseData);
    },

    registerMarketParticipantUrlDetailUrl: function (responseData, postfix, id) {
        var path = this.buildDummyDetailsUrls(postfix, 'marketParticipant', responseData.id, id);
        this.registrationSimlet(path, responseData);
    },

    registerPlantDetailUrl: function (responseData, postfix, id) {
        var path = this.buildDummyDetailsUrls(postfix, 'plant', responseData.plantId, id);
        this.registrationSimlet(path, responseData);
    },

    registerSimCardDetailUrl: function (responseData, postfix, id) {
        var path = this.buildDummyDetailsUrls(postfix, 'simCard', responseData.simCardId, id);
        this.registrationSimlet(path, responseData);
    },

    registerMeterAddonDeviceDetailUrl: function (responseData, postfix, id) {
        var path = this.buildDummyDetailsUrls(postfix, 'meterAddonDevice', responseData.meterAddonDeviceId, id);
        this.registrationSimlet(path, responseData);
    },

    registerTransformerDetailUrl: function (responseData, postfix, id) {
        var path = this.buildDummyDetailsUrls(postfix, 'transformer', responseData.transformerId, id);
        this.registrationSimlet(path, responseData);
    }
});
