/*jsl:option explicit*/
Ext.define('Energy.common.language.Locale', {
	// Class:
    requires: [
        'Ext.util.Cookies'
    ],
	singleton: true,
	// Attributes:
	
	country: null,
	language: null,
	locale: null,
	loaded: false,

	// Methods:
	
	load: function() {
		var locale = null,
			me = this;

		Ext.log({
			msg: 'Loading locale [check param, liferay, browser attributes, default]'
		});
		
		var params = Ext.urlDecode(location.search.substring(1));
		if(params.lang != undefined){
    		
    		Ext.log({
					msg: 'Take locale "' + params.lang + '" from Param'
			});
    		
    		return me.setLocale(params.lang);
    		    		
    	}

        if(Ext.util.Cookies.get('BOSCHSI_UII_LOCALE')){

            var locales = Ext.util.Cookies.get('BOSCHSI_UII_LOCALE').split('#');

            Ext.log({
                msg: 'Take locale "' + locales[0] + '" from UI-Integrator Cookie'
            });

            //return the locale with the highest priority
            return me.setLocale(locales[0]);
        };
		
		

		if(window.Liferay) {
			locale = window.Liferay.ThemeDisplay.getLanguageId();

			if(locale) {
				Ext.log({
					msg: 'Take locale "' + locale + '" from Liferay'
				});

				return me.setLocale(locale.substring(0,2));
			}
		}

		if(window.navigator) {
			for(var i = 0, attributes = ['language', 'userLanguage', 'browserLanguage']; i < attributes.length; ++i) {
				var attribute = attributes[i];

				if(window.navigator[attribute]) {
					locale = window.navigator[attribute];

					if(locale) {
						Ext.log({
							msg: 'Take locale "' + locale + '" from ' + attribute
						});

						return me.setLocale(locale);
					}
				}
			}
		}

		locale = 'de';
		
		Ext.log({
			msg: 'Take fallback locale "de"'
		});

		return me.setLocale(locale);
	},
	
	getCountry: function() {
		var me = this;
		
		if(!me.loaded) {
			me.load();
		}

		return me.country;
	},
		
	getLanguage: function() {
		var me = this;
		
		if(!me.loaded) {
			me.load();
		}

		return me.language;
	},
		
	getLocale: function() {
		var me = this;
		
		if(!me.loaded) {
			me.load();
		}

		return me.locale;
	},
		
	setLocale: function(locale) {
		var i = locale.indexOf('_'),
			me = this;
		
		/* if(!me.loaded) { // As soon as more than just the locale have to be initialized
			me.load();
		} */

		if(i < 0) {
			me.country = '';
			me.language = locale;
		} else {
			me.country = locale.substring(i + 1);
			me.language = locale.substring(0, i);
		}

		me.locale = locale;

		return me.loaded = true; // Only valid as long as only the locale has to be initialized
	},


	/* ExtJS-Übersetzungdatei für Locale/Language laden.
	   Asynchron, daher Callback - wird bei Erfolg oder Scheitern aufgerufen.
	   ExtJS-Dateien müssen im Verzeichnis 'basePath' (mit abschließendem Slash) liegen.
	*/
    loadExtJSLocale: function(basePath, callback, scope) {
		var callCallback = function() {
			callback.call(scope);
		};
		
		
	    var evalFile = function(fileName, fallback) {
	    	
	    	Ext.Ajax.request({
				url: basePath + fileName,
				scope: this,
				success: function(response) {
					eval(response.responseText);
					callCallback();
				},
				failure: function() {
					fallback.call(this);
				}
			});
	    };

	    evalFile.call(this, 'ext-lang-'+ this.getLocale() + '.js', function() {
			evalFile.call(this, 'ext-lang-'+ this.getLanguage() + '.js', function() {
				evalFile.call(this, 'ext-lang-en.js', callCallback);
			});
		});
	}
});
