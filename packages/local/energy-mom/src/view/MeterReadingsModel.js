Ext.define('Energy.mom.view.MeterReadingsModel', {
    extend: 'Ext.app.ViewModel',
    requires: ['Energy.mom.store.MeterReadings'],
    alias: 'viewmodel.meterReadingsModel',
    data: {
        record: null
    },
    stores: {
        meterReadings: {
            type: 'meterReadings'
        }
    }
});
