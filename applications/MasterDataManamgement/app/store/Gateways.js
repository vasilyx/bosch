Ext.define('MAM.store.Gateways', {
    extend: 'Ext.data.Store',
    alias: 'store.gateways',
    storeId: 'gateways',
    model: 'MAM.model.Gateway',
    autoLoad: false,
    pageSize: MAM.Config.List.limit,
    proxy: {
        type: 'ajax',
        url: '/MAM/' + MAM.Config.Backend.Versions + '/gateways',
        startParam: 'skip',
        limitParam: 'top',
        extraParams: {
            expand: 'properties',
            totalNumber: true
        },
        headers: {
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty: 'gateways',
            totalProperty: 'totalNumber'
        }
    }
});