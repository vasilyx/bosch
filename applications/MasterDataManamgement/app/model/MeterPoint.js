Ext.define('MAM.model.MeterPoint', {
    extend: 'Ext.data.Model',
    requires: [
        'MAM.model.Relation'
    ],
    idProperty: 'id',
    fields: [
        { name: 'id', type: 'int', mapping: 'meterPointId'},
        { name: 'customId', type: 'string'},

        { name: 'status', type: 'string'},


        { name: 'medium', type: 'string'}, /* POWER, GAS, OIL, COLD, COLDWATER, WARMWATER; HEATING */
        { name: 'measuringType', type: 'string'}, /* DIRECT, HALFDIRECT, TRANSFORMER */
        { name: 'measuringProduct', type: 'string'},

        { name: 'consumptionType', type: 'string'},
        { name: 'consumptionUnit', type: 'string'},
        { name: 'consumptionForecast', type: 'int', convert: MAM.utils.model.Converts.convertEmptyInt},
        { name: 'consumptionAverage', type: 'int', convert: MAM.utils.model.Converts.convertEmptyInt},
        { name: 'consumptionYear1', type: 'int', convert: MAM.utils.model.Converts.convertEmptyInt},
        { name: 'consumptionYear2', type: 'int', convert: MAM.utils.model.Converts.convertEmptyInt},
        { name: 'consumptionYear3', type: 'int', convert: MAM.utils.model.Converts.convertEmptyInt},

        { name: 'transformRatio', type: 'string'},
        { name: 'hindrance', type: 'string'},
        { name: 'hindranceReason', type: 'string'},

        { name: 'customAttributes', reference: 'MAM.model.CustomAttribute'},
        { name: 'relations', reference: 'MAM.model.Relation' },

        { name: 'createdTimeStamp', convert: MAM.utils.model.Converts.convertDate }

    ]
});