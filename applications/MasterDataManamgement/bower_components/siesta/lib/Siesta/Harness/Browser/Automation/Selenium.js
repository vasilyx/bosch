/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Harness.Browser.Automation.Selenium', {
    
    requires    : [ 'getQueryParam' ],
    
    has : {
        isSelenium              : false
    },
    
    override : {
        
        initialize : function () {
            if (this.getQueryParam('selenium') != null) {
                this.isAutomated        = true
                this.isSelenium         = true
            }
            
            this.SUPERARG(arguments)
        }
    }
})
//eof Siesta.Harness.Browser.Automation


