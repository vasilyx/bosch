Ext.define('Energy.mom.model.MeterReading', {
    extend: 'Ext.data.Model',
    requires: [
        'Energy.mom.util.Converts'
    ],
    idProperty: 'id',
    fields: [
        { name: 'meterId', type: 'int'},
        { name: 'startTS', type: 'date', format: 'd.m.Y H:i:s'},
        { name: 'endTS',  type: 'date', format: 'd.m.Y H:i:s'},
        { name: 'meterPointTimePeriod', type: 'string', convert: Energy.mom.util.Converts.convertReadingDate},
        { name: 'meterReadingId'},
        { name: 'registerName'},
        { name: 'unit'},
        { name: 'value'},
        { name: 'reason', type: 'string', convert: Energy.mom.util.Converts.strToLower },
        { name: 'readingDate',  type: 'date', format: 'd.m.Y H:i:s'}
    ]
});