Ext.define('MAM.view.relations.EntityListController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.relationsEntityListController',

    control: {
        'grid toolbar textfield': {
            change: 'filterData'
        },
        'grid toolbar button': {
            click: 'clickClearButton'
        },
        '#closeButton': {
            click: 'close'
        },
        '#addRelation': {
            click: 'clickButtonAddRecord'
        },
        'grid': {
            afterrender: 'loadMeters',
            itemdblclick: 'addRecord'
        }
    },

    /**
     * close the view, if the view is used or open
     * in a window
     */
    close: function () {
        if (this.getView().up().getXType() === 'actionWindow') {
            this.getView().up().close();
        }
    },

    loadMeters: function () {
        var store = this.getViewModel().getStore('grid');
        store.load({
            params: {
                top: 9999 // we need all data for local search
            }
        });
    },

    /**
     * Set store's filters by every textfield in toolbar
     */
    filterData: function () {
        var grid = this.getView().down('#entityGrid');
        var store = grid.getStore();
        store.clearFilter();
        Ext.Array.each(this.getView().query('#entityGrid toolbar textfield'), function(input) {

            store.filterBy(function (record) {
                if (record.get(input.getName()).indexOf(input.getValue()) === -1) {
                    return false;
                } else {
                    return true;
                }
            });
        });
    },

    /**
     * Purge value in textfield
     * @param el
     * @param t
     */
    clickClearButton: function (el, t) {
        el.prev('textfield').setValue(''); // clear previous input
    },

    /**
     * Add record to relations (via doubleClick)
     * @param vv
     * @param record
     */
    addRecord: function (vv, record) {
        this.fireEvent('addRelationRecord', record);
        this.getView().up().close();
    },

    /**
     * Add record to relations (via buttonClick)
     */
    clickButtonAddRecord: function () {
        var records = this.getView().down('#entityGrid').getSelectionModel().getSelection();
        if (records.length) {
            this.fireEvent('addRelationRecord', records[0]);
            this.getView().up().close();
        } else {
            Ext.create('Energy.common.ui.DialogBox').showInformation(_tl.get("mam.edit.entityList.selectItem"));
        }
    }

});