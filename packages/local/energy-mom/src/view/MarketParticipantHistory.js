Ext.define('Energy.mom.view.MarketParticipantHistory', {
    extend: 'Ext.panel.Panel',

    requires: [
        'Energy.mom.view.MarketParticipantHistoryController',
        'Energy.mom.view.MarketParticipantHistoryModel',
        'Energy.mom.store.MarketParticipants'
    ],
    alias: 'widget.actionMarketParticipantHistory',
    controller: 'actionMarketParticipantHistoryController',
    viewModel: {
        type: 'marketParticipantHistoryModel'
    },

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    initComponent: function () {

        var groupingFeature = Ext.create('Ext.grid.feature.Grouping',{
            groupHeaderTpl: [
                '{children:this.formatHeader} <div class="stateCheck {[values.rows[0].get(\'state\')]}">&nbsp;</div>',
                {
                    formatHeader: function( children ) {
                        if(children.length > 0){
                            return children[0].get('groupTitle');
                        }
                        else{
                            return '';
                        }
                    }
                }
            ],
            hideGroupedHeader: true
        });

        var items = [
            {
                xtype: 'container',
                html: LangMarketParticipantHistory.get('meterPoint') + ': ' + this.getViewModel().get('record').get('customId'),
                cls: 'marketParticipantHistoryHeader',
                margin: 10
            },
            {
                xtype: 'grid',
                itemId: 'marketParticipantHistoryGrid',
                cls: 'marketParticipantHistoryGrid',
                bind: {
                    store: '{momMarketParticipants}'
                },
                flex: 1,
                emptyText: LangMarketParticipantHistory.get('emptyText'),
                features: groupingFeature,
                columns: [
                    {text: LangMarketParticipantHistory.get('dateFrom'), dataIndex: 'dateFrom', width: 150},
                    {text: LangMarketParticipantHistory.get('dateTo'), dataIndex: 'dateTo', width: 150},
                    {text: LangMarketParticipantHistory.get('role'), dataIndex: 'role', width: 120},
                    {text: LangMarketParticipantHistory.get('name'), dataIndex: 'name', flex: 1},
                    {text: LangMarketParticipantHistory.get('gln'), dataIndex: 'code', flex: 1}
                    /**
                     * this fields are now not filled by the backend and it will be implemented
                     * later by the backend
                     */
                    /*{text: LangMarketParticipantHistory.get('gridArea'), dataIndex: 'gridArea', width: 100},
                    {text: LangMarketParticipantHistory.get('accountingArea'), dataIndex: 'accountingArea', width: 100},
                    {text: LangMarketParticipantHistory.get('comment'), dataIndex: 'comment', width: 100}*/
                ]
            }
        ];

        var buttons =  [{
            text: LangMarketParticipantHistory.get('close'),
            itemId: 'closeButton'

        }];

        Ext.apply(this, {items: items, buttons: buttons});
        this.callParent(arguments);
    }
});