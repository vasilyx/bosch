/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Siesta.Harness.Browser.meta.extend({

    has : {
        
        /**
         * @cfg {Boolean} enablePageRedirect When set to `true`, the test scripts (your *.t.js files) will be executed in separate context.
         * They will not be included on the test page itself. Thus such tests will survive any page redirects or refreshes 
         * (for example after form submit, etc).
         * 
         * Note, when using this option, all "preload" files will still be loaded into test page context, not in the test script context.
         * Setting this option to `true` will disable the `overrideSetTimeout` option for this test.
         * 
         * Please refer to <a href="#!/guide/cross_page_testing">Cross page testing</a> guide for more details.
         * 
         * This option can also (and probably should only) be specified in the test file descriptor.
         * 
         * **This option is available only in Standard package**.
         * 
         * @member Siesta.Harness.Browser
         */
        enablePageRedirect             : false,
        
        // can't just set the attribute to Joose.I.Object, because its a static class 
        // that has already been created, so attribute initializer won't be called
        testScopesByURL             : {
            lazy    : Joose.I.Object
        },
        
        /**
         * @cfg {Boolean/String} enableCodeCoverage When set to `true`, will enable the collection of the code coverage information
         * for currently running test suite. Coverage information is only collected for the JavaScript files in the `preload` config,
         * that are marked with "instrument : true" property:
         * 

    harness.configure({
        enableCodeCoverage  : true,
        preload         : [
            {
                type        : 'js',
                url         : 'some_file.js',
                instrument  : true
            }
        ],
        ...
    })

         * 
         * When set to `true`, coverage information collection will be enabled only if the `siesta-coverage-all.js` file is loaded on the harness page.
         * Otherwise Siesta will warn user about missing files. The warning can be disabled with the special value for this 
         * config option - a string `ifloaded`, which enables coverage only if files are loaded. 
         * 
         * Normally, coverage information is collected on a per-file basis, the Ext JS layer adds another special coverage mode - per class.
         * See {@link #coverageUnit} for details.
         * 
         * See also {@link #includeCoverageUnits}, {@link #excludeCoverageUnits}, {@link #installLoaderInstrumentationHook} configs and
         * <a href="#!/guide/code_coverage">Collecting code coverage information</a> guide.
         * 
         * This option can be **disabled** in the test file descriptor.
         * 
         * **This option is available only in the Siesta Standard package**.
         * 
         * @member Siesta.Harness.Browser
         */
        enableCodeCoverage          : false,
        
        
        /**
         * @cfg {String} coverageUnit A string defining how the instrumented files are processed.
         * 
         * At this level, the only recognized and default value is "file", which means Siesta just instruments the files
         * in the `preload` config that are marked with `instrument : true` property:

    harness.configure({
        preload         : [
            {
                type        : 'js',
                url         : 'some_file.js',
                instrument  : true
            }
        ],
        ...
    })

         * The Ext JS layer adds an additional mode - "extjs_class", please refer to the {@link Siesta.Harness.Browser.ExtJS#coverageUnit documentation}.
         * 
         * **This option is available only in the Siesta Standard package**.
         * 
         * @member Siesta.Harness.Browser
         */
        coverageUnit                : {
            lazy    : function () { return 'file' }
        },
        
        
        /**
         * @cfg {RegExp} includeCoverageUnits Regular expression, defining which coverage units to instrument.
         * By default all found units are included.
         * 
         * See also {@link #excludeCoverageUnits}, {@link #coverageUnit}, {@link #enableCodeCoverage}
         *  
         * **This option is available only in Standard package**.
         * 
         * @member Siesta.Harness.Browser
         */
        includeCoverageUnits        : /.*/,

        
        /**
         * @cfg {RegExp} excludeCoverageUnits A regular expression, defining which coverage units to exclude from the instrumentation.
         * Default value is `null` meaning that nothing should be excluded.
         * 
         * See also {@link #includeCoverageUnits}, {@link #coverageUnit}, {@link #enableCodeCoverage}
         *  
         * **This option is available only in the Siesta Standard package**.
         * 
         * @member Siesta.Harness.Browser
         */
        excludeCoverageUnits        : null,
        
        // if there's a previous report, we need to wait for the content manager state even for the 1st page
        hasPreviousReport           : false,
        
        coverageNoSource            : false,
        
        instrumenter                : null,
        currentContentManager       : null,
        
        htmlCoverageReport          : {
            lazy    : function () { 
                return new IstanbulSiestaHtmlReport({ mode : this.getCoverageUnit() == 'file' ? 'normal' : 'trueTree' }) 
            }
        },
        lcovCoverageReport          : {
            lazy    : 'this.buildLcovReport'
        }
    },

    override : {
        
        initInstrumenter : function () {
            if (!this.enableCodeCoverage || this.instrumeter) return
            
            var needToWarn              = false
            
            if (window.Instrumenter && window.IstanbulCollector && window.IstanbulSiestaHtmlReport && window.IstanbulSiestaLcovReport) {
                this.instrumenter       = new Instrumenter()
            } else {
                needToWarn              = String(this.enableCodeCoverage).toLowerCase() != 'ifloaded'
                this.enableCodeCoverage = false
            }
                
            // do not show the alert with warning in case of automation - it will throw exception in selenium and is handled in the launchers anyway
            if (needToWarn && !this.isAutomated) alert(Siesta.Resource('Siesta.Harness.Browser', "codeCoverageWarningText"));
        },
        
        
        launch : function () {
            this.initInstrumenter()
            
            // need to do it here, because UI can override this setting
            if (this.enableCodeCoverage) this.cachePreload  = true
            
            this.SUPERARG(arguments)
        },
        
        
        onTestSuiteStart : function (descriptors, contentManager, launchState) {
            if (this.enableCodeCoverage) {
                if (this.currentContentManager) this.currentContentManager.disposeCoverageCollector()
                
                this.currentContentManager          = contentManager
                
                // a bit ugly but will work
                contentManager.includeCoverageUnits = this.includeCoverageUnits
                contentManager.excludeCoverageUnits = this.excludeCoverageUnits
                
                this.fireEvent('nocoverageinfo')
            }
            
            this.SUPERARG(arguments)
        },
        
        
        // this method is only used in Phantom now - to be removed
        addPreviousCoverageInfo : function (previousCoverageInfo) {
            this.currentContentManager.addRawCoverageResult(previousCoverageInfo)
        },
        
        
        addPreviousCoverageResults : function (coverageResults) {
            var me      = this
            
            Joose.A.each(coverageResults, function (coverageResult) {
                me.currentContentManager.addRawCoverageResult(coverageResult)
            })
        },
        
        
        buildLcovReport : function () {
            var content     = []
            
            return new IstanbulSiestaLcovReport({
                dir         : 'dir',
                writer      : {
                    println     : function (text) {
                        content.push(text)
                    },
                    
                    writeFile   : function (fileName, callback) {
                        callback(this)
                    },
                    
                    getContent  : function () {
                        return content.join('\n')
                    }
                }
            })
        },
        
        
        generateCoverageRawReport : function (coverageResults) {
            if (coverageResults) this.addPreviousCoverageResults(coverageResults)
            
            return {
                contentManagerState     : this.getContentManagerState(),
                rawReport               : this.currentContentManager.getCollector().getFinalCoverage(),
                coverageUnit            : this.coverageUnit
            }
        },
        
        
        generateCoverageHtmlReport : function (coverageResults) {
            if (coverageResults) this.addPreviousCoverageResults(coverageResults)
            
            var currentContentManager   = this.currentContentManager
            
            return {
                coverageNoSource    : this.coverageNoSource,
                htmlReport          : this.getHtmlCoverageReport().getTreeReport(
                    currentContentManager.getCollector(), 
                    currentContentManager, 
                    this.coverageNoSource
                ),
                coverageUnit        : this.coverageUnit
            }
        },
        
        
        generateCoverageLcovReport : function (coverageResults) {
            if (coverageResults) this.addPreviousCoverageResults(coverageResults)
            
            var report                  = this.getLcovCoverageReport()
            
            report.writeReport(this.currentContentManager.getCollector())
            
            return {
                lcovReport      : report.opts.writer.getContent(),
                coverageUnit    : this.coverageUnit
            }
        },
        
        
        canUseCachedContent : function (resource, desc) {
            // this line still uses "this.enableCodeCoverage" instead of "this.getDescriptorConfig(desc, 'enableCodeCoverage')"
            // this is by intention, to always switch the "?" operator in the 1st branch, when code coverage is enabled
            // and limit the usage of the cached preloads (which have some troubles because of not creating real <script> tag)
            return this.enableCodeCoverage ?
                resource instanceof Siesta.Content.Resource.JavaScript && resource.instrument 
            : 
                this.SUPER(resource, desc)
        },
        
        
        addCachedResourceToPreloads : function (scopeProvider, contentManager, resource, desc) {
            if (this.getDescriptorConfig(desc, 'enableCodeCoverage')) {
                var content     = contentManager.getInstrumentedContentOf(resource, this.instrumenter, this.getCoverageUnit())
                
                // if instrumentation has failed we'll fallback to SUPER implementation
                if (content != null) {
                    scopeProvider.addPreload({
                        type        : 'js',
                        content     : content
                    })
                    
                    return
                }
            }
            
            this.SUPERARG(arguments)
        },        
        
        
        getRawTotalCoverageInfo : function (asString, previousCoverageInfo) {
            var currentContentManager   = this.currentContentManager
            
            // previous coverage info can be missing in case test suite fits on single page
            if (previousCoverageInfo) currentContentManager.addRawCoverageResult(previousCoverageInfo)
            
            var result                  = currentContentManager.getCollector().getFinalCoverage()
        
            return asString ? JSON.stringify(result) : result
        },
        
        
        // to be used in automation mode
        getContentManagerState : function (asString) {
            var result          = this.currentContentManager.getState()
                
            return asString ? JSON.stringify(result) : result
        },
        
         
        // in case of "enablePageRedirect" we don't need to run any seeding script - the test script will ran in different context
        prepareScopeSeeding : function (scopeProvider, desc, contentManager) {
            if (!this.getDescriptorConfig(desc, 'enablePageRedirect')) this.SUPERARG(arguments)
        },
        
        
        launchTest : function (options, callback) {
            var me                          = this
            var desc                        = options.desc
            var url                         = desc.url
            
            // 
            if (!this.getDescriptorConfig(desc, 'enablePageRedirect')) return this.SUPERARG(arguments)
            
            var testScriptScopeProvider     = this.getTestScopesByURL()[ url ] = new Scope.Provider.IFrame({
                seedingCode     : this.getSeedingCode(desc, options.launchState.launchId)
            })
            
            if (desc.testCode || this.cachePreload && options.contentManager.hasContentOf(url))
                testScriptScopeProvider.addPreload({
                    type        : 'js', 
                    content     : desc.testCode || (options.contentManager.getContentOf(url) + '\n//# sourceURL=' + desc.url)
                })
            else
                testScriptScopeProvider.seedingScript = this.resolveURL(url, options.scopeProvider, desc)
                
            var testHolder  = options.testHolder
            
            if (!this.getDescriptorConfig(desc, 'transparentEx')) { 
                testScriptScopeProvider.addOnErrorHandler(this.getOnErrorHandler(testHolder))
            }
                
            var sup     = this.SUPER
            
            testScriptScopeProvider.setup(function () {
                var scope                       = testScriptScopeProvider.scope
                
                options.startTestAnchor         = scope.StartTest
                options.originalSetTimeout      = scope.setTimeout
                options.originalClearTimeout    = scope.clearTimeout
                
                sup.call(me, options, callback)
            })
        },
        
        
        getNewTestConfiguration : function (desc, scopeProvider, contentManager, launchState) {
            var config          = this.SUPERARG(arguments)
            
            if (this.getDescriptorConfig(desc, 'enablePageRedirect')) {
                // disable the overriding of `setTimeout` for scripts on separate page
                config.overrideSetTimeout   = false
            }
            
            return config
        },
        
        
        cleanupScopeForURL : function (url) {
            this.SUPER(url)
            
            var testScriptScopeProvider = this.getTestScopesByURL()[ url ]
            
            if (testScriptScopeProvider) {
                delete this.getTestScopesByURL()[ url ]
                
                testScriptScopeProvider.cleanup()
            }
        }
    }
})