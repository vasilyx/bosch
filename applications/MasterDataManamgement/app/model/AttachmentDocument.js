Ext.define('MAM.model.AttachmentDocument', {
    extend: 'MAM.model.Attachment',
    proxy: {
        type: 'ajax',
        binary: true,
        url: '/MAM/' + MAM.Config.Backend.Versions + '/attachments/{id}/document',
        reader: {
            transform: {
                fn: function(data) {
                    // dirty hack. only for tests
                    if (data.getResponseHeader('responseBytes')) {
                        data.responseBytes =  data.getResponseHeader('responseBytes').split(',');
                    }

                    return data;
                },
                scope: this
            }
        }

    }
});