Ext.define('MAM.view.meter.RelationsList', {
    extend: 'MAM.view.relations.EntityList',
    requires: [
        'MAM.view.relations.EntityList'
    ],
    alias: 'widget.metersRelationsList',

    getToolbar: function () {
        return [{
            xtype: 'textfield',
            name: 'customId',
            hideLabel: true,
            width: 200
        }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'manufacturer',
                hideLabel: true,
                width: 120
            }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'calibrationEndYear',
                hideLabel: true,
                width: 120
            }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'lotNumber',
                hideLabel: true,
                flex: 1
            }, this.getButtonClear()
        ];
    },

    getColumns: function () {
        return [
            {text: _tl.get("mam.label.meter.customId"), dataIndex: 'customId', width: 250},
            {text: _tl.get("mam.label.meter.manufacturer"), dataIndex: 'manufacturer', width: 170},
            {text: _tl.get("mam.label.meter.calibrationEndYear"), dataIndex: 'calibrationEndYear', width: 170},
            {text: _tl.get("mam.label.meter.lotNumber"), dataIndex: 'lotNumber', flex: 1}
        ];
    },

    initComponent: function () {

        this.callParent(arguments);
    }
});