StartTest(function (t) {

   // var list = Ext.create('TestApplication.application.AbstractList', {t: t, name: 'meterPoint'}),

        referenceData = {
            "meterPointId": 1491119,
            "customId": "DE0700081632100000000000070238000",
            "consumptionAverage": 566733.0,
            "measuringType": "DIRECT",
            "medium": {
                en: "Electricity",
                de: "Elektrizität"
            },
            "consumptionForecast": 566733.0,
            "consumptionUnit": "kwh",
            "measuringProduct": "714"
/*        },
        customAttributes = [
            {
                "key": {
                    en: "Consumption Type",
                    de: "Verbrauchstyp"
                },
                "value": "G"
            }
        ],
        referenceDataDetail = {
            "customId": "G22175AAA25185",
            "deviceType": "22175",
            "manufacturer": "GASELAN"
        },
        personRelation = {
            "customId": "70380913",
            "name": "Georg Mustermann"
        },
        buildingRelation = {
            "customId": "81078588",
            "municipalityKey": "12060005"*/
        };

    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/detail_DE0700081632100000000000070238000.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/customAttributes_DE0700081632100000000000070238000.json', referenceData.meterPointId);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/relations_DE0700081632100000000000070238000.json', referenceData.meterPointId);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/r/details_1494726.json'); // for related
    MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/r/details_1480156.json'); // for related
    MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/r/details_1035879.json'); // for related

   //  list.testListViewArea();

    t.it('#1 Enter full entity id into filter filed', function (t) {
        t.chain(

            { waitForCQ : 'app-main tabbar' },
            { click : 'tabbar=> span.icon-meterPoint'},
            { waitForCQ :  'meterPointList'},
            { waitForRowsVisible : 'meterPointList'},
            function (next) {
                t.doubleClick(t.getFirstRow(t.cq1('meterPointInitial gridpanel')));
                next();
            },
            {waitForCQ: 'meterPointDetail'},
            {waitForComponentVisible: 'meterPointDetail'},
            {
                action: 'wait',
                delay: 1500
            },
            function (next) {
                t.cqExists('meterPointDetail', 'meterPointDetail view exist');
/*
                for (var code in referenceData) {
                    var val = referenceData[code].hasOwnProperty(_locale.getLocale()) ? referenceData[code][_locale.getLocale()] : referenceData[code];
                    t.contentLike(t.cq1('meterPointDetail displayfield[name="' + code + '"]'), val, 'Found right  #' + code + ' ' + val);
                }*/

            }
        )
    });
});