Ext.define('MAM.view.gateway.ListModel', {
    extend: 'Ext.app.ViewModel',
    requires: ['MAM.model.Gateway', 'MAM.store.Gateways'],
    
    alias: 'viewmodel.gatewayListModel',
    
    data: {
        header: null
    },
    stores: {
        gateways: {
           type: 'gateways'
        }
    }
});