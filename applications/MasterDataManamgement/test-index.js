var harness = new Siesta.Harness.Browser.ExtJS();

harness.configure({
    title       : 'MAM Test Suite',
    /**
     * need also tests for small resoultion
     * 1024x768
     * 1280x800
     * 1366x768
     * 1680x1050 (default)
     */
    viewportHeight: 1050,
    viewportWidth: 1680,
    loaderPath  : {
        'Ext': '../../ext/classic/classic/src',
        'Energy.common': '../../packages/local/energy-common/src',
        'Ext.ux': '../../ext/packages/ux/src',
        'MAM' : 'app',
        'TestApplication': 'tests'
    },
    installLoaderInstrumentationHook: true,
    autocheckGlobals: true,
    expectedGlobals: [
        'Ext',
        'Energy.common',
        'MAM',
        'MockData',
        'MockConfig',
        'TestApplication'
    ],
    requires: [
        'Energy.common.language.Locale',
        'Energy.common.language.Translations',
        'TestApplication.data.MockData',
        'TestApplication.data.MockConfig',
        'TestApplication.application.AbstractList'
    ],
    disableCaching: true,
    enableCodeCoverage      : true,
    runCore: 'sequential', //run tests sequential, default is parallel
    preload     : [
        // version of ExtJS used by your application 
        // (not needed if you use Sencha Cmd which builds a complete 'all-file' including Ext JS itself)
        '../../build/testing/MasterDataManagement/classic/resources/MAM-all.css',
        {
            text: harness.getLoaderPathHook() //get loaderPath config before executing load mocked data
        },
        {
            // inject the hook right after ExtJS and before application file
            text    : harness.getLoaderInstrumentationHook()
        }

    ]
});


harness.start(
    {
        group               : 'Prepare Test Suite',
        waitForAppReady     : true,
        waitForExtReady     : true,
        loaderPath  : {
            'TestApplication': 'tests'
        },
        pageUrl: 'index.html?siestatest',
        items               : [
            'tests/000_initial.t.js'
        ]
    },
    {
        group               : 'Application',
        autoCheckGlobals    : false,
        waitForAppReady     : true,
        waitForExtReady     : true,
        loaderPath  : {
            'TestApplication': 'tests'
        },
        pageUrl: 'index.html?siestatest',
        items: [
            'tests/application/MAM-1_meterPointList.t.js',
            'tests/application/MAM-2_gatewayList.t.js',
            'tests/application/MAM-3_meterList.t.js',
            'tests/application/MAM-4_connectionBuildingList.t.js',
            'tests/application/MAM-5_personList.t.js',
            'tests/application/MAM-6_meterPointDetail.t.js',
            'tests/application/MAM-7_gatewayDetail.t.js',
            'tests/application/MAM-8_meterDetail.t.js',
            'tests/application/MAM-9_personDetail.t.js',
            'tests/application/MAM-10_connectionBuildingDetail.t.js',
            'tests/application/MAM-11_actionBar.t.js',
            'tests/application/MAM-12_marketParticipantList.t.js',
            'tests/application/MAM-13_marketParticipantDetail.t.js',
            'tests/application/MAM-14-CRUD-meterPointDelete.t.js',
            'tests/application/MAM-15-CRUD-meterPointEdit.t.js',
            'tests/application/MAM-16-CRUD-meterPointCreate.t.js',
            'tests/application/MAM-17-UndefinedRelations.t.js',
            'tests/application/MAM-18-CRUD-meterDelete.t.js',
            'tests/application/MAM-19-CRUD-meterCreate.t.js',
            'tests/application/mom/MAM-1_marketParticipantView.t.js',
            'tests/application/mom/MAM-2_meterReadingsView.t.js'
        ]
    },
    {
        group               : 'Unit',
        autoCheckGlobals    : false,
        waitForExtReady     : true,
        loaderPath  : {
            'Energy.common': '../../packages/local/energy-common/src'
        },
        preload: [
            'bootstrap.css',
            '../../ext/build/ext-all-debug.js',
            {
                text: harness.getLoaderPathHook() //get loaderPath config before executing load mocked data
            }
        ],
        requires: [
            'Energy.common.*'
        ],
        items               : [
            'tests/unit/common/UI-1_dialogBox.t.js'
        ]
    }
);