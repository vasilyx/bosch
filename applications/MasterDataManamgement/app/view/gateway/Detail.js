Ext.define('MAM.view.gateway.Detail', {
    extend: 'Ext.Panel',

    alias: 'widget.gatewayDetail',

    bind: {
        data: '{gateway}'
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    padding: '0 0 10 0',
    scrollable: true,
    
    initComponent: function () {

        var defaults = {
            xtype: 'displayfield',
            labelWidth: 150
        };

        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if(this.responsiveConfig){
            responsiveConfig = this.responsiveConfig;
        }

        var items = [
            {
                xtype: 'container',
                cls: 'bg-grey',
                margin: 10,
                padding: '5 10 0 10',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                defaults: defaults,
                items: this.getNorthItems()

            },
            {
                xtype: 'container',
                padding: '0 20 0 20',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getWestItems()
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getEastItems()
                    }
                ]
            },
            {
                xtype: 'tabpanel',
                padding: '0 20 0 20',
                plain: true,
                items: this.getSouthItems()

            }
        ];

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    },
    /*
     * Defines the top area items for connection building properties
     */
    getNorthItems: function(){
        var items = [
            {
                xtype: 'container',
                xtype: 'container',
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                flex: 1,
                items: [
                    {
                        fieldLabel: _tl.get('mam.label.gateway.customId'),
                        name: 'customId',
                        flex: 1,
                        bind: {
                            value: '{gateway.customId}'
                        }
                    },
                    {
                        fieldLabel: _tl.get('mam.label.gateway.description'),
                        name: 'description',
                        flex: 1,
                        bind: {
                            value: '{gateway.description}'
                        }
                    }

                ]
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                flex: 1,
                items: [
                    {
                        fieldLabel: _tl.get('mam.label.gateway.createdTimeStamp'),
                        flex: 1,
                        name: 'createdTimeStamp',
                        bind: {
                            value: '{gateway.createdTimeStamp}'
                        }
                    },
                    {
                        fieldLabel: _tl.get('mam.label.gateway.id'),
                        name: 'gatewayId',
                        bind: {
                            value: '{gateway.id}'
                        }
                    }
                ]
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the left side of panel
     */
    getWestItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.gateway.propertyNumber'),
                name: 'propertyNumber',
                bind: {
                    value: '{gateway.propertyNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.gateway.manufacturer'),
                name: 'manufacturer',
                bind: {
                    value: '{gateway.manufacturer}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.gateway.firmware'),
                name: 'firmware',
                bind: {
                    value: '{gateway.firmware}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.gateway.constructionYear'),
                name: 'constructionYear',
                bind: {
                    value: '{gateway.constructionYear}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.gateway.calibrationYear'),
                name: 'calibrationYear',
                bind: {
                    value: '{gateway.calibrationYear}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.gateway.calibrationEndYear'),
                name: 'calibrationEndYear',
                bind: {
                    value: '{gateway.calibrationEndYear}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.gateway.buildInDate'),
                name: 'buildInDate',
                bind: {
                    value: '{gateway.buildInDate}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.gateway.buildOutDate'),
                name: 'buildOutDate',
                bind: {
                    value: '{gateway.buildOutDate}'
                }
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the right side of panel
     */
    getEastItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.gateway.lotNumber'),
                name: 'lotNumber',
                bind: {
                    value: '{gateway.lotNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.gateway.examinationNumber'),
                name: 'examinationNumber',
                bind: {
                    value: '{gateway.examinationNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.gateway.charge'),
                name: 'charge',
                bind: {
                    value: '{gateway.charge}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.gateway.status'),
                name: 'status',
                bind: {
                    value: '{status}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.gateway.configStatus'),
                name: 'configStatus',
                bind: {
                    value: '{configStatus}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.gateway.communication'),
                name: 'communication',
                bind: {
                    value: '{gateway.communication}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.gateway.communicationType'),
                name: 'communicationType',
                bind: {
                    value: '{gateway.communicationType}'
                }
            }

        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the bottom side of panel
     * mostyl parameters collected in a grid
     */
    getSouthItems: function(){
        var items = [
            {
                xtype: 'grid',
                border: true,
                title: _tl.get('mam.label.gateway.certificates'),
                emptyText: _tl.get('mam.label.ui.noData'),
                reference: 'gatewayCertificates',
                name: 'certificates',
                viewConfig: {
                    loadMask: false
                },
                bind: {
                    store: '{certificates}'
                },
                columns: [{
                    text: _tl.get("mam.label.gateway.certificates.certificate"),
                    dataIndex: 'certificate',
                    flex: 1
                }]
            }
        ];
        return items;
    }
});