Ext.define('MAM.view.gateway.Edit', {
    extend: 'Ext.form.Panel',
    alias: 'widget.gatewayEdit',
    defaults: {
        allowBlank: false,
        padding: '0 10 0 10'
    },
    items: [
        {
            xtype: 'textfield',
            fieldLabel: 'Gateway Id',
            name: 'gatewayId',
            bind: {
                value: '{gateway.gatewayId}'
            }
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Beschreibung',
            name: 'customId',
            bind: {
                value: '{gateway.customId}'
            }

        },
        {
            xtype: 'textfield',
            fieldLabel: 'Hersteller',
            name: 'manufacturer',
            bind: {
                value: '{gateway.manufacturer}'
            }
        }
    ],
    bbar: [
        '->',
        {
            type: 'button',
            formBind: true,
            text: 'Speichern'
        }
    ]
})