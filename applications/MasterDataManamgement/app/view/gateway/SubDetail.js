Ext.define('MAM.view.gateway.SubDetail', {
    extend: 'Ext.tab.Panel',
    requires: [
        'MAM.view.relations.Initial'
    ],
    alias: 'widget.gatewaySubDetail',
    //plain: true,
    initComponent: function () {
        var items = [];
        var tabConfig = MAM.utils.Configuration.getTabConfig( MAM.utils.Constants.GATEWAY );

        Ext.each(tabConfig, function( tab ){
           items.push({
               xtype: tab.type + 'Initial',
               title: _tl.get("mam.label." + MAM.utils.Constants.GATEWAY + "." + tab.type),
               itemId: MAM.utils.Constants.GATEWAYS + '-tab-' + tab.type
           }) 
        });

       
        Ext.apply(this, {items: items});
        this.callParent(arguments);
    }
});