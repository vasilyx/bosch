Ext.define('MAM.view.meterPoint.Initial', {
    extend: 'Ext.container.Container',

    requires: [
        'MAM.view.meterPoint.List',
        'MAM.view.meterPoint.InitialModel',
        'MAM.view.meterPoint.InitialController',
        'MAM.view.meterPoint.Detail',
        'MAM.view.meterPoint.EditDetail',
        'MAM.view.meterPoint.SubDetail',
        'MAM.view.meterPoint.EditSubDetail',
        'MAM.view.relations.Initial',
        'MAM.view.customAttributes.Initial',
        'Energy.common.ui.Header'
    ],

    alias: 'widget.meterPointInitial',

    viewModel: 'meterPointInitialModel',
    controller: 'meterPointInitialController',

    layout: {
       type: 'hbox',
        align: 'stretch',
        resizable: true
    },
    padding: '0 0 0 10',

    initComponent: function () {

        this.callParent(arguments);

        this.createMeterPointsList();
        this.addSelectMessage();
    },
    addSelectMessage: function(){
        this.lookupReference('detailContainer').removeAll();
        this.lookupReference('detailContainer').add({
            xtype: 'commonUiDisplayMessage',
            message: _tl.get('mam.label.ui.selectEntity')

        });
    },

    createMeterPointsList: function(){
        this.add({
            xtype: 'container',
                width: 300,
            layout: {
            type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype:'commonUiHeader',
                    header: _tl.get('mam.label.meterPoints'),
                    bind: {
                        header:  '{totalText}'
                    }

                },
                {
                    xtype: 'meterPointList',

                    flex: 1
                }
            ]
        },{
            xtype: 'container',
            margin: '0 0 0 10',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            flex: 1,
            items: [{
                xtype:'commonUiHeader',
                header: _tl.get('mam.label.meterPoint'),
                colorClass: 'green',
                showArrow: true,
                bind: {
                    header:  _tl.get('mam.label.meterPoint')  + ' {meterPoint.customId}',
                    subheader: '{editModeText}'
                }

            },
                {
                    xtype: 'container',
                    cls: 'border-lrb',
                    reference: 'detailContainer',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    flex: 1
                }]
        },
            {
                xtype: 'actionInitial',
                viewModel: {
                    data: {
                        entity: MAM.utils.Constants.METERPOINT
                    }
                }
            });
    },

    createMeterPointDetailContainer: function(){
        var detail = this.lookupReference('detailContainer');
        detail.removeAll();

        this.lookupReference('detailContainer').add(
            {
                xtype: 'meterPointDetail',
                flex: 1
            },
            {
                xtype: 'meterPointSubDetail',
                flex: 1
            }
        );
    },

    createMeterPointEditDetailContainer: function(){
        var detail = this.lookupReference('detailContainer');
        detail.removeAll();

        this.lookupReference('detailContainer').add(
            {
                xtype: 'meterPointEditDetail',
                flex: 1
            },
            {
                xtype: 'meterPointEditSubDetail',
                flex: 1
            }
        );
    }

});