Ext.define('MAM.model.CustomAttribute', {
    extend: 'Ext.data.Model',
    requires: [
        'MAM.utils.data.proxy.MamRest',
        'MAM.model.Attribute'/*,
         'MAM.model.PersonDetail',
         'MAM.model.ConnectionBuildingDetail'*/
    ],
    fields: [{
        name: 'hasCustomAttributes', mapping: 'customAttributes', convert: function( value ){
            if(value === undefined){
                return false;
            }else if( MAM.utils.Validation.isObjectEmpty(value)){
                return false;
            }else if(value.length === 0){
                return false;
            }
            else{
                return true;
            }
        }
    }],
    hasMany: [
        { name: 'customAttributes',  associationKey: 'customAttributes', model: 'MAM.model.Attribute' }
    ],
    proxy: {
        type: 'mamrest',
        appendId: false,
        headers: {
            'Accept': 'application/json'
        },
        api: {
            create: '/MAM/' + MAM.Config.Backend.Versions + '/{entity}/{id}/customAttributes',
            read: '/MAM/' + MAM.Config.Backend.Versions + '/{entity}/{id}/customAttributes',
            update: '/MAM/' + MAM.Config.Backend.Versions + '/{entity}/{id}/customAttributes',
            destroy: '/MAM/' + MAM.Config.Backend.Versions + '/{entity}/{id}/customAttributes/{key}'
        },
        actionMethods: {
            create: 'POST',
            read: 'GET',
            update: 'PUT',
            destroy: 'DELETE'
        },
        reader: {
            type: 'json',
            transform: {
                fn: function(data) {
                    var newData = { };
                    // extract only customAttributes from response
                    if(data.customAttributes){
                        newData.customAttributes = data.customAttributes;
                        return newData;
                    }
                    return data;
                },
                scope: this
            }
        }
    }
});