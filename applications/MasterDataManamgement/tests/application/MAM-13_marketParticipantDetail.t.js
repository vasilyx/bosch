StartTest(function (t) {

    var list = Ext.create('TestApplication.application.AbstractList', {t: t, name: 'marketParticipant'});
    list.testListViewArea();

    var referenceData = {
      //  "id":1,
        "code":"1477665228709",
        "customId":"TEST_1477665228709", // codeType
        "role":"Manager",
        "name":"John"
    };

    var referenceDataDetail = {
        customId: 'DE0700081632100000000000070238321',
        medium: "",
        consumptionUnit: 'kwh'
    };
    var referenceDataCustomAttributes = {
            "1110": 'MP Attributes4',
            "1111": 'MP Attributes5',
            "1112": 'MP Attributes6',
            "1114": 'MP Attributes7'
    };

    t.it('#2 Enter full entity id into filter filed', function(t) {
        t.chain(
            function(next) {
                MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipants/details_1.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipants/customAttributes_1.json', 1);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/marketParticipants/relations_1.json', 1);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/r/details_1491237.json'); // for related
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/r/details_1491238.json'); // for related
                next();
            },
            { doubleclick : t.getFirstRow(t.cq1('marketParticipantInitial gridpanel')) },
            { waitForCQ : 'marketParticipantDetail' },
            { waitForComponentVisible : 'marketParticipantDetail'},
            {
                action      : 'wait',
                delay       : 1500      // 1500 ms
            },
            function(next) {

                t.cqExists('marketParticipantDetail', 'MarketParticipant Detail view exist');

                for (var code in referenceData) {
                    var val = referenceData[code].hasOwnProperty(_locale.getLocale()) ? referenceData[code][_locale.getLocale()] : referenceData[code];
                    t.contentLike(t.cq1('marketParticipantDetail displayfield[name="' + code + '"]'), val, 'Found right  #' + code + ' ' + val);
                }
            }
        )
    });
    t.it('#3 Check person Sub Tabs', function (t) {
        t.chain(
            {
                waitForCQ: 'marketParticipantSubDetail',
                waitForCQ: 'customAttributesInitial'
            }, function (next) {
                t.cqExists('marketParticipantSubDetail', 'marketParticipant Detail Tabs view exist');
                t.elementIsVisible(t.cq1('relationsInitial'), 'Tab "Relations" is visible');
                t.cqExists('relationsInitial', 'Relations tab exist');
                t.cqExists('customAttributesInitial', 'Custom Attributes tab exist');
                next();
            }
        );
    });
    //#4 first tab contains relations => Expected Result: 1 relation availalbe (Meter Point)
    t.it('#4 First tab contains relations', function (t) {
        t.chain(
            {
                waitForCQ: 'marketParticipantSubDetail',
                waitForComponentVisible: 'relationDetail'
            },
            {
                click: t.compositeQuery('marketParticipantSubDetail tabbar => .x-tab')[0]
            }, {
                waitForCQ: 'relationsInitial'
            }, function (next) {
                t.elementIsVisible(t.cq1('marketParticipantSubDetail relationsInitial grid'), 'Relations is visible');
                t.expect(t.cq1('marketParticipantSubDetail').items.length).toBe(2);

                var accordionEl = t.cq('relationsInitial #relationAccordion grid');
                t.like(accordionEl[0].getTitle(), Energy.common.language.Translations.get("mam.label.meterPoints"), 'Accordion meterPoints is visible');
                t.expect(accordionEl.length).toBe(1);

            }
        );
    });

    //#5 MarketParticipant point relation view => Expected Result: two Meter Points and detail view is visible
    t.it('#5 MarketParticipant point relation view', function (t) {
        t.chain(
            {
                click: t.compositeQuery('marketParticipantSubDetail relationsInitial grid => .x-grid-cell')[1],
                waitForComponentVisible: 'marketParticipantPointDetail'
            },
            function (next) {
                t.expect( t.cq1('marketParticipantSubDetail relationsInitial grid').getStore().getCount()).toBe(2);
                t.elementIsVisible(t.cq1('relationsInitial meterPointDetail'), 'MeterPointDetail is visible');

                for (var code in referenceDataDetail) {
                    var val = referenceDataDetail[code].hasOwnProperty(_locale.getLocale()) ? referenceDataDetail[code][_locale.getLocale()] : referenceDataDetail[code];
                    t.contentLike(t.cq1('relationsInitial meterPointDetail displayfield[name="' + code + '"]'), val, 'Found right  #' + code + ' ' + val);
                }
            }
        );
    });

    //#6 Custom Attributes of the meter tab => Expected Result: four items with values
    t.it('#6 Custom Attributes of the marketParticipant tab', function (t) {
        t.chain(
            {
                click: t.compositeQuery('marketParticipantSubDetail tabbar => .x-tab')[1]
            },
            function (next) {
                t.expect( t.cq1('customAttributesInitial').getStore().getCount()).toBe(4);

                for (var code in referenceDataCustomAttributes[_locale.getLocale()]) {
                    t.like(t.cq1('customAttributesInitial').getStore().findRecord('key', code).get('value'), referenceDataCustomAttributes[_locale.getLocale()][code]);
                }
            }
        );
    });
});