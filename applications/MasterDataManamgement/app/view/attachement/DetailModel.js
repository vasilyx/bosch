Ext.define('MAM.view.attachment.DetailModel', {
    extend: 'Ext.app.ViewModel',
    requires: ['MAM.model.Attachment'],

    alias: 'viewmodel.attachmentDetailModel',

    data: {
        document: null
    }
});