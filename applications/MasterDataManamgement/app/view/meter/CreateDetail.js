Ext.define('MAM.view.meter.CreateDetail', {
    extend: 'Ext.Panel',

    controller: 'meterCreateDetailController',
    alias: 'widget.meterCreateDetail',
    requires: ['MAM.utils.Configuration'],

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    scrollable: true,

    initComponent: function() {

        var defaults = {
            xtype: 'textfield',
            labelWidth: 150
        };

        var editForm = {
            xtype: 'form',
            trackResetOnLoad: true,
            id: 'meterCreateForm',
            items:[]
        };

        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if(this.responsiveConfig){
            responsiveConfig = this.responsiveConfig;
        }

        editForm.items = [
            {
                xtype: 'container',
                margin: '10 0 0 10',
                padding: '0 25 0 15',
                plugins: 'responsive',
                layout: 'form',
                responsiveConfig: responsiveConfig,
                defaults: defaults,
                items: this.getNorthItems()
            },

            {
                xtype: 'container',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                padding: '0 20 0 20',
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        layout: 'form',
                        padding: '0 40 0 0',
                        defaults: defaults,
                        items: this.getWestItems()
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        layout: 'form',
                        defaults: defaults,
                        items: this.getEastItems()
                    }
                ]
            }
        ];

        var buttons =  [{
            text: _tl.get('mam.label.saveViewButton'),
            disabled: true,
            itemId: 'saveViewButton'
        },{
            text: _tl.get('mam.label.saveEditButton'),
            disabled: true,
            itemId: 'saveEditButton'
        }, {
            text: _tl.get('mam.label.close'),
            itemId: 'closeButton'
        }];

        Ext.apply(this, {items: editForm, buttons: buttons});
        this.callParent(arguments);
    },
    getNorthItems: function() {

        var items = [
            {
                fieldLabel: _tl.get('mam.label.meter.customId'),
                name: 'customId',
                length: 8,
                allowBlank: false,
                minLength: 8,
                maxLength: 8,
                regex: /^[0-9]{8}$/,
                regexText: _tl.get('mam.notice.meter.customId.validate'),
                flex: 1
            },
            {
                xtype: 'commonFormTooltip',
                quicktipHeader: _tl.get('mam.notice.meter.customId.quicktipHeader'),
                quicktip: _tl.get('mam.notice.meter.customId.quicktip')
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the left side of panel
     */
    getWestItems: function () {
        var items = [
            {
                xtype: 'combo',
                name: 'medium',
                allowBlank: false,
                forceSelection: true,
                fieldLabel: _tl.get('mam.label.meter.medium'),
                displayField: 'name',
                valueField: 'code',
                width: 'auto',
                store: new Ext.data.JsonStore({
                    autoLoad: true,
                    proxy: {
                        type: 'ajax',
                        url: MAM.utils.Configuration.getUrlEnumProperty(MAM.utils.Constants.METER, 'medium'),
                        reader: {
                            type: 'json',
                            rootProperty: 'medium',
                            transform: {
                                fn: function (data) {
                                    for (k in data.medium) {
                                        data.medium[k].name = _tl.get('mam.medium.' + data.medium[k].name);
                                    }
                                    return data;
                                },
                                scope: this
                            }
                        }
                    }
                }),
                bind: {
                    value: '{meter.medium}'
                }
            },
            {
                xtype: 'combo',
                name: 'measuringPrinciple',
                allowBlank: false,
                forceSelection: true,
                fieldLabel: _tl.get('mam.label.meter.measuringPrinciple'),
                displayField: 'name',
                valueField: 'code',
                width: 'auto',
                store: new Ext.data.JsonStore({
                    autoLoad: true,
                    proxy: {
                        type: 'ajax',
                        url: MAM.utils.Configuration.getUrlEnumProperty(MAM.utils.Constants.METER, 'measuringPrinciple'),
                        reader: {
                            type: 'json',
                            rootProperty: 'measuringPrinciple',
                            transform: {
                                fn: function (data) {
                                    for (k in data.measuringPrinciple) {
                                        data.measuringPrinciple[k].name = _tl.get('mam.measuringPrinciple.' + data.measuringPrinciple[k].name);
                                    }
                                    return data;
                                },
                                scope: this
                            }
                        }
                    }
                }),
                bind: {
                    value: '{meter.measuringPrinciple}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.buildInDate'),
                name: 'buildInDate',
                xtype: 'datefield',
                format: 'd.m.Y',
                allowBlank: false,
                submitFormat: 'Y-m-d',
                bind: {
                    value: '{meter.buildInDate}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.buildOutDate'),
                name: 'buildOutDate',
                xtype: 'datefield',
                format: 'd.m.Y',
                submitFormat: 'Y-m-d',
                bind: {
                    value: '{meter.buildOutDate}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.status'),
                name: 'status',
                bind: {
                    value: '{status}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.deviceType'),
                name: 'deviceType',
                bind: {
                    value: '{meter.deviceType}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.deviceClass'),
                name: 'deviceClass',
                bind: {
                    value: '{deviceClass}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.deviceKind'),
                name: 'deviceKind',
                bind: {
                    value: '{meter.deviceKind}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.manufacturer'),
                name: 'manufacturer',
                bind: {
                    value: '{meter.manufacturer}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.factoryNumber'),
                name: 'factoryNumber',
                length: 14,
              //  allowBlank: false,
                minLength: 14,
                maxLength: 14,
                regex: /^[0-9]{14}$/,
                bind: {
                    value: '{meter.factoryNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.deliveryNoteNumber'),
                name: 'deliveryNoteNumber',
                bind: {
                    value: '{meter.deliveryNoteNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.energyFlowDirection'),
                name: 'energyFlowDirection',
                bind: {
                    value: '{meter.energyFlowDirection}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.mountingType'),
                name: 'mountingType',
                bind: {
                    value: '{meter.mountingType}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.meterReadingType'),
                name: 'meterReadingType',
                bind: {
                    value: '{meter.meterReadingType}'
                }
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the right side of panel
     */
    getEastItems: function () {
        var items = [
            {
                fieldLabel: _tl.get('mam.label.meter.lotNumber'),
                name: 'lotNumber',
                bind: {
                    value: '{meter.lotNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.examinationNumber'),
                name: 'examinationNumber',
                bind: {
                    value: '{meter.examinationNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.charge'),
                name: 'charge',
                bind: {
                    value: '{meter.charge}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.interface'),
                name: 'interface',
                bind: {
                    value: '{meter.interface}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.comAddress'),
                name: 'comAddress',
                bind: {
                    value: '{meter.comAddress}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.serverId'),
                name: 'serverId',
                bind: {
                    value: '{meter.serverId}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.publicKey'),
                name: 'publicKey',
                bind: {
                    value: '{meter.publicKey}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.keyM'),
                name: 'keyM',
                bind: {
                    value: '{meter.keyM}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.calibrationYear'),
                name: 'calibrationYear',
                bind: {
                    value: '{meter.calibrationYear}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.calibrationEndYear'),
                name: 'calibrationEndYear',
                bind: {
                    value: '{meter.calibrationEndYear}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.constructionYear'),
                name: 'constructionYear',
                bind: {
                    value: '{meter.constructionYear}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.meterSize'),
                name: 'meterSize',
                bind: {
                    value: '{meter.meterSize}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meter.tariffCount'),
                name: 'tariffCount',
                bind: {
                    value: '{meter.tariffCount}'
                }
            }];
        return items;
    }
});