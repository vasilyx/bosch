Ext.define('Energy.mom.store.MarketParticipants', {
    extend: 'Ext.data.Store',
    alias: 'store.momMarketParticipants',
    storeId: 'momMarketParticipants',
    model: 'Energy.mom.model.MarketParticipant',
    autoLoad: false,
    pageSize: 25,
    groupField: 'from',
    groupDir: 'DESC',
    proxy: {
        type: 'ajax',
        appendId: false,
        url: '/MOM/v1/meterPoints/{0}/marketParticipants/history',
        startParam: 'skip',
        limitParam: 'top',
        extraParams: {
            expand: 'properties',
            totalNumber: true
        },
        headers: {
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty: 'timeSlices',
            totalProperty: 'totalNumber',
            transform: {
                fn: function(data) {
                    var newData = [];

                    Ext.each(data.body.timeSlices, function(item, key) {
                        if (data.body.timeSlices[key+1] && data.body.timeSlices[key+1].from == item.to) {
                            var d = new Date(item.to);
                            d.setDate(d.getDate() - 1);
                            item.toUi = d;
                        }
                    });


                    Ext.each(data.body.timeSlices, function(item) {

                        Ext.each(item.marketParticipants, function(marketParticipant) {
                            newData.push({
                                name: marketParticipant.name,
                                role: marketParticipant.role,
                                code: marketParticipant.code,
                                from: item.from, // double dates because used in some different converts and needed "clear" dates
                                to: item.to,
                                toUi: item.toUi,//the changed toUi date is used (if start and end of tow slices are the same, the to date will be set  to start(next ts)-1
                                dateFrom: item.from,
                                dateTo: item.toUi
                            });
                        });

                    });

                    return {'timeSlices': newData};
                },
                scope: this
            }
        }
    }
});