Ext.define('MAM.view.relations.InitialController', {
    extend: 'Ext.app.ViewController',
    requires: [
        'MAM.model.MeterDetail',
        'MAM.view.attachment.Detail',
        'MAM.view.relations.EntityList'
    ],
    alias: 'controller.relationsInitialController',
    control: {
        '#relationAccordion grid': {
            itemdblclick: 'onRecordSelect'
        },
        '#relationAccordion grid actioncolumn': {
            click: 'clickRemoveRecord'
        },
        '#relationAccordion toolbar button': {
            click: 'onClickLinkButton'
        },
        '#relationAccordion panel[collapsed=false]': {
            afterrender: 'onAccordionReady'
        },
        '#relationAccordion panel': {
            expand: 'onPanelExpand'
        }
    },
    listen: {
        controller: {
            '*': {
                relationschange: 'onRelationsChange',
                relationschangeedit: 'onRelationsChangeEdit'
            },
            'relationsEntityListController': {
                addRelationRecord: 'addRelationRecord'
            }
        }
    },

     /**
     * select the first record of first accordion
     * it's a delayed task because we need to wait till
     * all relations area loaded
     * @param component
     */
    onAccordionReady: function( panel ){

         var me = this;
         var task = new Ext.util.DelayedTask(function() {
             if(panel.getStore().getCount() > 0){
                me.onRecordSelect( null, panel.getStore().getAt(0));
             }
         });
         task.delay(500);

    },

    /**
     * select the first record of expanded panel
     * @param panel
     */
    onPanelExpand: function( panel ){

        this.onRecordSelect( null, panel.getStore().getAt(0));

    },
    /**
     * Loads the necessary data for given relation
     * @param rowModel
     * @param record
     */
    onRecordSelect: function(rowModel, record){
        var detailContainer = this.lookupReference('relationDetail');
        detailContainer.removeAll();

        /**
         * TODO: needs to be reafactored soon
         * because of too many relations, this method should
         * be refactored
         */

        if(record instanceof MAM.model.Meter) {
            this.getViewModel().set('meter', record);
            detailContainer.add({
                xtype: 'meterDetail', 
                flex: 1,
                responsiveConfig: MAM.utils.Responsive.subDetailHeaderPanel
            });
            this.getView().lookupReference('meterRegisters').getStore().loadRawData(record.get('registers'))
        }
        else if(record instanceof MAM.model.MeterPoint){
            this.getViewModel().set('meterPoint', record);
            detailContainer.add({ 
                xtype: 'meterPointDetail', 
                flex: 1,
                responsiveConfig: MAM.utils.Responsive.subDetailHeaderPanel
            });
        }
        else if(record instanceof MAM.model.Gateway){
            this.getViewModel().set('gateway', record);
            detailContainer.add({ 
                xtype: 'gatewayDetail', 
                flex: 1,
                responsiveConfig: MAM.utils.Responsive.subDetailHeaderPanel
            });
            this.getView().lookupReference('gatewayCertificates').getStore().loadRawData(record.get('certificates'))
        }
        else if(record instanceof MAM.model.ConnectionBuilding){
            this.getViewModel().set('connectionBuilding', record);
            detailContainer.add({ 
                xtype: 'connectionBuildingDetail', 
                flex: 1,
                responsiveConfig: MAM.utils.Responsive.subDetailHeaderPanel
            });
        }
        else if(record instanceof MAM.model.Person){
            this.getViewModel().set('person', record);
            detailContainer.add({
                xtype: 'personDetail',
                flex: 1,
                responsiveConfig: MAM.utils.Responsive.subDetailHeaderPanel
            });
        }
        else if(record instanceof MAM.model.Attachment){
            this.getViewModel().set('attachment', record);
            var attachmentDetail = Ext.create('MAM.view.attachment.Detail', { 
                flex: 1,
                responsiveConfig: MAM.utils.Responsive.subDetailHeaderPanel
            });
            detailContainer.add( attachmentDetail );
            attachmentDetail.fireEvent('attachmentChange', attachmentDetail, record.getId());

        }
        else if(record instanceof MAM.model.MarketParticipant){
            this.getViewModel().set('marketParticipant', record);
            detailContainer.add({
                xtype: 'marketParticipantDetail',
                flex: 1,
                responsiveConfig: MAM.utils.Responsive.subDetailHeaderPanel
            });
        }
        else if(record instanceof MAM.model.Plant){
            this.getViewModel().set('plant', record);
            detailContainer.add({
                xtype: 'plantDetail',
                flex: 1,
                responsiveConfig: MAM.utils.Responsive.subDetailHeaderPanel
            });
        }
        else if(record instanceof MAM.model.SimCard){
            this.getViewModel().set('simCard', record);
            detailContainer.add({
                xtype: 'simCardDetail',
                flex: 1,
                responsiveConfig: MAM.utils.Responsive.subDetailHeaderPanel
            });
        }
        else if(record instanceof MAM.model.Transformer){
            this.getViewModel().set('transformer', record);
            detailContainer.add({
                xtype: 'transformerDetail',
                flex: 1,
                responsiveConfig: MAM.utils.Responsive.subDetailHeaderPanel
            });
        }
        else if(record instanceof MAM.model.MeterAddonDevice){
            this.getViewModel().set('meterAddonDevice', record);
            detailContainer.add({
                xtype: 'meterAddonDeviceDetail',
                flex: 1,
                responsiveConfig: MAM.utils.Responsive.subDetailHeaderPanel
            });
        }

        if (record && typeof(record.get('associatedCompType')) !== 'undefined'
            && MAM.utils.Configuration.getDefinedRelationList().indexOf(record.get('associatedCompType')) === -1) {
            this.getViewModel().set('undefinedRelation', record);
            detailContainer.add({
                xtype: 'undefinedDetail',
                flex: 1,
                responsiveConfig: MAM.utils.Responsive.subDetailHeaderPanel
            });
        }

    },
    /**
     * Model relations of current entity
     * @param relationsRecord
     * @param ownerEntity
     */
    onRelationsChange: function( relationsRecord , ownerEntity ){
        var currentView = ownerEntity + '-tab-relations';
        /*TODO
        BUG oder FALSCHE VERWENDUNG IM CODE (PRÜFEN)
        Wenn die Relations view wiederverwendet wird und dieser ViewController auf den Event 'relationchange' horcht,
        wird dieser Event von allen Instancen der Relation View ausgefürht und nicht nur von der grade Aktiven
        daher haben die ganzen TabInstanzen eine itemId welche die entitiät beinhaltet
        ist die im view controller verbundene view nicht die, welche den event abgefeurert wird,
        so wird dieser event ignoriert.
        Den event listener nur auf die Controller zu begrenzen macht keinen sinn, denn wenn man einen Tab aufruft, ist die
        view zwar nicht sichtbar, aber der controller horcht trotzdem auf den event
        Auf Event durch die ausgelöste view anstall controller zu lauschen hat nicht geklapp bsp:
            listen: {
                component: {
                    'meterPointInitial': { //die event werfende view
                        relationschange: 'onRelationsChange'
                    }
                }
            }
        Ein Fiddle und Frage im ExtJs Forum wird erstellt
        25.05.2016 aja
         */
        if(this.getView().getItemId() === currentView){
            this.clearAllRelations();
            if(relationsRecord.get('hasRelations')){

                this.getView().createRelationContainer();
                this._conditionsLoadRelations( relationsRecord, ownerEntity );
            }
            else{
                this.getView().createEmptyContainer();
                this.getView().showEmptyMessage();
            }
        }
    },

    onRelationsChangeEdit: function( relationsRecord, ownerEntityType, ownerEntitiy ){
        this.clearAllRelations();
        var me = this;
        var allowedList = MAM.utils.Configuration.getRelationEditList(ownerEntityType.substring(0, ownerEntityType.length -1));

        this.getViewModel().set('ownerEntity', ownerEntityType);
        this.getViewModel().set('ownerId', ownerEntitiy.get('id'));
        this.getViewModel().set('ownerCustomId', ownerEntitiy.get('customId'));

        this.getView().createRelationContainer();
        this._conditionsLoadRelations( relationsRecord, ownerEntityType );

        allowedList.forEach(function(val, key) {
            me.getView().createRelationPanel( val, val + 's', 0 );
        });
    },

    /**
     * @param ownerEntity
     * @private
     */
    _conditionsLoadRelations: function(relationsRecord, ownerEntity) {
        if(ownerEntity !== MAM.utils.Constants.METERS
            && relationsRecord.meters() && relationsRecord.meters().getCount() > 0){
            this.loadRelations( relationsRecord.meters(), MAM.utils.Constants.METERS );
        }

        if(ownerEntity !== MAM.utils.Constants.GATEWAYS
            && relationsRecord.gateways() && relationsRecord.gateways().getCount() > 0){
            this.loadRelations( relationsRecord.gateways(), MAM.utils.Constants.GATEWAYS );
        }

        if(ownerEntity !== MAM.utils.Constants.METERPOINTS
            && relationsRecord.meterPoints() && relationsRecord.meterPoints().getCount() > 0){
            this.loadRelations( relationsRecord.meterPoints(), MAM.utils.Constants.METERPOINTS );
        }

        if(ownerEntity !== MAM.utils.Constants.PERSONS
            && relationsRecord.persons() && relationsRecord.persons().getCount() > 0){
            this.loadRelations( relationsRecord.persons(), MAM.utils.Constants.PERSONS );
        }

        if(ownerEntity !== MAM.utils.Constants.CONNECTIONBUILDINGS
            && relationsRecord.connectionBuildings() && relationsRecord.connectionBuildings().getCount() > 0){
            this.loadRelations( relationsRecord.connectionBuildings(), MAM.utils.Constants.CONNECTIONBUILDINGS );
        }

        if(ownerEntity !== MAM.utils.Constants.ATTACHMENTS
            && relationsRecord.attachments() && relationsRecord.attachments().getCount() > 0){
            this.loadRelations( relationsRecord.attachments(), MAM.utils.Constants.ATTACHMENTS );
        }

        if(ownerEntity !== MAM.utils.Constants.MARKETPARTICIPANTS
            && relationsRecord.marketParticipants() && relationsRecord.marketParticipants().getCount() > 0){
            this.loadRelations( relationsRecord.marketParticipants(), MAM.utils.Constants.MARKETPARTICIPANTS );
        }

        if(ownerEntity !== MAM.utils.Constants.PLANTS
            && relationsRecord.plants() && relationsRecord.plants().getCount() > 0){
            this.loadRelations( relationsRecord.plants(), MAM.utils.Constants.PLANTS );
        }

        if(ownerEntity !== MAM.utils.Constants.SIMCARDS
            && relationsRecord.simCards() && relationsRecord.simCards().getCount() > 0){
            this.loadRelations( relationsRecord.simCards(), MAM.utils.Constants.SIMCARDS );
        }

        if(ownerEntity !== MAM.utils.Constants.TRANSFORMERS
            && relationsRecord.transformers() && relationsRecord.transformers().getCount() > 0){
            this.loadRelations( relationsRecord.transformers(), MAM.utils.Constants.TRANSFORMERS );
        }

        if(ownerEntity !== MAM.utils.Constants.METERADDONDEVICES
            && relationsRecord.meterAddonDevices() && relationsRecord.meterAddonDevices().getCount() > 0){
            this.loadRelations( relationsRecord.meterAddonDevices(), MAM.utils.Constants.METERADDONDEVICES );
        }

        var otherRelations = false;

        for ( key in relationsRecord.get('relations')) {
            if (MAM.utils.Configuration.getDefinedRelationList().indexOf(key) === -1
                && relationsRecord.get('relations')[key].length > 0 ) {

                otherRelations = true;
                this.getViewModel().getStore('undefinedRelations').add(relationsRecord.get('relations')[key]);
            }
        }

        if (otherRelations) {
            this.getView().createRelationPanel(
                MAM.utils.Constants.UNDEFINEDRELATIONS,
                MAM.utils.Constants.UNDEFINEDRELATIONS,
                this.getViewModel().getStore(MAM.utils.Constants.UNDEFINEDRELATIONS).getCount()
            );

        }
    },

    onDataLoadSuccess: function( recordDetail, eObjore, store, entity, entitites, index, relationCount ){
        if(index === 0){
            if (!this.getView().down('grid#' + entitites)) {
                this.getView().createRelationPanel( entity, entitites, relationCount );
            } else {
                this._updateGridTitle(this.getView().down('grid#' + entitites), relationCount);
            }
        }
        store.add( recordDetail );
    },


    onDataLoadFailure: function(){
        console.log("Error loading Data");
    },
    
    loadRelations: function( relationsStore, entityType ) {
        var store = this.getViewModel().getStore( entityType );
        relationsStore.each(function( record, index){
            var id = record.get('id');
            var relationCount = relationsStore.getCount();
            if( entityType === MAM.utils.Constants.METERS ){
                MAM.model.MeterDetail.load( id, {
                    //erweitern von parametern in einer funktion
                    success: Ext.bind(this.onDataLoadSuccess, this, [store, MAM.utils.Constants.METER, MAM.utils.Constants.METERS, index, relationCount], true),
                    failure: this.onDataLoadFailure,
                    scope: this
                });
            }
            else if ( entityType === MAM.utils.Constants.GATEWAYS ){
                MAM.model.GatewayDetail.load( id, {
                    //erweitern von parametern in einer funktion
                    success: Ext.bind(this.onDataLoadSuccess, this, [store, MAM.utils.Constants.GATEWAY, MAM.utils.Constants.GATEWAYS, index, relationCount], true),
                    failure: this.onDataLoadFailure,
                    scope: this
                });
            }
            else if ( entityType === MAM.utils.Constants.METERPOINTS ){
                MAM.model.MeterPointDetail.load( id, {
                    //erweitern von parametern in einer funktion
                    success: Ext.bind(this.onDataLoadSuccess, this, [store, MAM.utils.Constants.METERPOINT, MAM.utils.Constants.METERPOINTS, index, relationCount], true),
                    failure: this.onDataLoadFailure,
                    scope: this
                });
            }
            else if ( entityType === MAM.utils.Constants.PERSONS ){
                MAM.model.PersonDetail.load( id, {
                    //erweitern von parametern in einer funktion
                    success: Ext.bind(this.onDataLoadSuccess, this, [store, MAM.utils.Constants.PERSON, MAM.utils.Constants.PERSONS, index, relationCount], true),
                    failure: this.onDataLoadFailure,
                    scope: this
                });
            }
            else if ( entityType === MAM.utils.Constants.CONNECTIONBUILDINGS ){
                MAM.model.ConnectionBuildingDetail.load( id, {
                    //erweitern von parametern in einer funktion
                    success: Ext.bind(this.onDataLoadSuccess, this, [store, MAM.utils.Constants.CONNECTIONBUILDING, MAM.utils.Constants.CONNECTIONBUILDINGS, index, relationCount], true),
                    failure: this.onDataLoadSuccess,
                    scope: this
                });
            }
            else if ( entityType === MAM.utils.Constants.ATTACHMENTS ){
                
                MAM.model.AttachmentDetail.load( id, {
                    //erweitern von parametern in einer funktion
                    success: Ext.bind(this.onDataLoadSuccess, this, [store, MAM.utils.Constants.ATTACHMENT, MAM.utils.Constants.ATTACHMENTS, index, relationCount], true),
                    failure: this.onDataLoadFailure,
                    scope: this
                });
            }
            else if ( entityType === MAM.utils.Constants.MARKETPARTICIPANTS ){

                MAM.model.MarketParticipantDetail.load( id, {
                    success: Ext.bind(this.onDataLoadSuccess, this, [store, MAM.utils.Constants.MARKETPARTICIPANT, MAM.utils.Constants.MARKETPARTICIPANTS, index, relationCount], true),
                    failure: this.onDataLoadFailure,
                    scope: this
                });
            }
            else if ( entityType === MAM.utils.Constants.PLANTS ){

                MAM.model.PlantDetail.load( id, {
                    success: Ext.bind(this.onDataLoadSuccess, this, [store, MAM.utils.Constants.PLANT, MAM.utils.Constants.PLANTS, index, relationCount], true),
                    failure: this.onDataLoadFailure,
                    scope: this
                });
            }
            else if ( entityType === MAM.utils.Constants.SIMCARDS ){

                MAM.model.SimCardDetail.load( id, {
                    success: Ext.bind(this.onDataLoadSuccess, this, [store, MAM.utils.Constants.SIMCARD, MAM.utils.Constants.SIMCARDS, index, relationCount], true),
                    failure: this.onDataLoadFailure,
                    scope: this
                });
            }
            else if ( entityType === MAM.utils.Constants.TRANSFORMERS ){

                MAM.model.TransformerDetail.load( id, {
                    success: Ext.bind(this.onDataLoadSuccess, this, [store, MAM.utils.Constants.TRANSFORMER, MAM.utils.Constants.TRANSFORMERS, index, relationCount], true),
                    failure: this.onDataLoadFailure,
                    scope: this
                });
            }
            else if ( entityType === MAM.utils.Constants.METERADDONDEVICES ){

                MAM.model.MeterAddonDeviceDetail.load( id, {
                    success: Ext.bind(this.onDataLoadSuccess, this, [store, MAM.utils.Constants.METERADDONDEVICE, MAM.utils.Constants.METERADDONDEVICES, index, relationCount], true),
                    failure: this.onDataLoadFailure,
                    scope: this
                });
            }



        }, this);

    },

    clearAllRelations: function( ){
        this.getView().removeAll();
        this.getViewModel().getStore( MAM.utils.Constants.METERS ).removeAll();
        this.getViewModel().getStore( MAM.utils.Constants.GATEWAYS ).removeAll();
        this.getViewModel().getStore( MAM.utils.Constants.METERPOINTS).removeAll();
        this.getViewModel().getStore( MAM.utils.Constants.PERSONS ).removeAll();
        this.getViewModel().getStore( MAM.utils.Constants.CONNECTIONBUILDINGS ).removeAll();
        this.getViewModel().getStore( MAM.utils.Constants.ATTACHMENTS ).removeAll();
        this.getViewModel().getStore( MAM.utils.Constants.MARKETPARTICIPANTS ).removeAll();
        this.getViewModel().getStore( MAM.utils.Constants.PLANTS ).removeAll();
        this.getViewModel().getStore( MAM.utils.Constants.SIMCARDS ).removeAll();
        this.getViewModel().getStore( MAM.utils.Constants.TRANSFORMERS ).removeAll();
        this.getViewModel().getStore( MAM.utils.Constants.METERADDONDEVICES ).removeAll();
        this.getViewModel().getStore( MAM.utils.Constants.UNDEFINEDRELATIONS ).removeAll();
    },

    /**
     * @param el
     */
    onClickLinkButton: function(el) {
        var title = Ext.String.format(_tl.get('mam.edit.entityList.addRelation'), Ext.String.capitalize(_tl.get('mam.label.' + el.up('grid').getItemId())));
        var window = Ext.create('MAM.view.window.ActionWindow', {
            title: title,
            items:  {
                xtype: el.up('grid').getItemId() + 'RelationsList',
                viewModel: {
                    data: {
                        buttonAddRecordText: title,
                        topHeader: _tl.get("mam.label.relation." + this.getViewModel().get('ownerEntity')) + ': ' + (this.getViewModel().get('ownerCustomId')||this.getViewModel().get('ownerId'))
                    },
                    stores:{
                        grid: {
                            type: el.up('grid').getItemId()
                        }
                    }
                }
            }
        });

        window.show();
    },
    /**
     * @param record
     */
    addRelationRecord: function(record) {
        var grid = this.getView().down('grid#' + record.store.type);
        grid.getStore().add(record);
        grid.getStore().sync();
        this._updateGridTitle(grid);
        this.fireEvent(this.getViewModel().get('ownerEntity') + 'RelationAdded');
        this.onRecordSelect( null, record);
    },

    /**
     * @param grid
     * @param el
     * @param rowIndex
     */
    clickRemoveRecord: function(grid, el, rowIndex) {
        grid.getStore().removeAt(rowIndex);
        grid.getStore().sync();
        this._updateGridTitle(grid.grid);
        this.fireEvent(this.getViewModel().get('ownerEntity') + 'RelationRemoved');
    },

    /**
     * @param grid
     * @param count
     * @private
     */
    _updateGridTitle: function(grid, count) {
        var title = grid.getTitle().substr(0, grid.getTitle().indexOf('(')+1);

        if (typeof(count) !== "undefined") {
            grid.setTitle(title + count +')');
        } else {
            grid.setTitle(title + grid.getStore().getCount() +')');
        }
    }
});