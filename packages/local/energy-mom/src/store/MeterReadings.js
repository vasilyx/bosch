Ext.define('Energy.mom.store.MeterReadings', {
    extend: 'Ext.data.Store',
    requires: ['Energy.mom.model.MeterReading'],
    alias: 'store.meterReadings',
    storeId: 'meterReadings',
    model: 'Energy.mom.model.MeterReading',
    autoLoad: false,
    pageSize: 25,
    groupField: 'meterId',
    proxy: {
        type: 'ajax',
        appendId: false,
        url:  '/MOM/v1/meterPoints/{0}/meters/meterReadings/history',
        //url:  '/MOM/v1/meterPoints/TEST_MOM_CREATE_1482131939513/meters/meterReadings/history',

        startParam: 'skip',
        limitParam: 'top',
        extraParams: {
            expand: 'properties',
            totalNumber: true
        },
        headers: {
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty: 'totalNumber',
            transform: {
                fn: function(data) {
                    var newData = [];

                    Ext.each(data.body.meters, function(item) {
                        var meterId = item.meterId;
                        if(item.meterReadings.length > 0){
                            Ext.each(item.meterReadings, function(meterReading) {
                                Ext.each(meterReading.versions, function(version) {
                                    newData.push({
                                        meterId: meterId,
                                        startTS: item.startTS,
                                        endTS: item.endTS,
                                        meterReadingId: version.meterReading.meterReadingId,
                                        registerName: version.meterReading.registerName,
                                        reason: version.meterReading.reason,
                                        unit: version.meterReading.unit,
                                        value: version.meterReading.value,
                                        readingDate: version.meterReading.readingDate
                                    });
                                });
                            });
                        }
                        else{
                            newData.push({
                                meterId: meterId,
                                startTS: item.startTS,
                                endTS: item.endTS,
                                meterReadingId: undefined,
                                registerName: undefined,
                                reason: undefined,
                                unit: undefined,
                                value: undefined
                            });
                        }

                    });

                    return {'data': newData};
                },
                scope: this
            }
        }
    }
});