Ext.define('MAM.utils.Constants', {

    singleton: true,

    GATEWAYS: 'gateways',
    METERS: 'meters',
    METERPOINTS: 'meterPoints',
    MARKETPARTICIPANTS: 'marketParticipants',
    PERSONS: 'persons',
    CONNECTIONBUILDINGS: 'connectionBuildings',
    ATTACHMENTS: 'attachments',

    GATEWAY: 'gateway',
    METER: 'meter',
    METERPOINT: 'meterPoint',
    MARKETPARTICIPANT: 'marketParticipant',
    PERSON: 'person',
    CONNECTIONBUILDING: 'connectionBuilding',
    ATTACHMENT: 'attachment',
    
    RELATIONSDETAIL: 'relations',
    CUSTOMATTRIBUTESDETAIL: 'customAttributes',

    PLANT: 'plant',
    PLANTS: 'plants',

    SIMCARD: 'simCard',
    SIMCARDS: 'simCards',

    TRANSFORMER: 'transformer',
    TRANSFORMERS: 'transformers',

    METERADDONDEVICE: 'meterAddonDevice',
    METERADDONDEVICES: 'meterAddonDevices',

    UNDEFINEDRELATIONS: 'undefinedRelations'

});
