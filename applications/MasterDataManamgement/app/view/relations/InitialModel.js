Ext.define('MAM.view.relations.InitialModel', {
    extend: 'Ext.app.ViewModel',
    requires: [
        'MAM.model.*'
    ],

    alias: 'viewmodel.relationsInitialModel',

    data: {
        meterPoint: null,
        meter: null,
        gateway: null,
        person: null,
        connectionBuilding: null,
        attachment: null,
        marketParticipant: null,
        simCard: null,
        transformer: null,
        meterAddonDevice: null,
        plant: null,
        undefinedRelation: null,
        record: null,
        ownerEntity: null,
        ownerCustomId: null,
        ownerId: null
    },

    stores: {
        meters: {
            model: 'MAM.model.MeterDetail',
            autoLoad: false,
            proxy: {
                type: 'memory'
            }
        },
        gateways: {
            model: 'MAM.model.GatewayDetail',
            autoLoad: false,
            proxy: {
                type: 'memory'
            }
        },
        meterPoints: {
            model: 'MAM.model.MeterPointDetail',
            autoLoad: false,
            proxy: {
                type: 'memory'
            }
        },
        connectionBuildings: {
            model: 'MAM.model.ConnectionBuildingDetail',
            autoLoad: false,
            proxy: {
                type: 'memory'
            }
        },
        persons: {
            model: 'MAM.model.PersonDetail',
            autoLoad: false,
            proxy: {
                type: 'memory'
            }
        },
        attachments: {
            model: 'MAM.model.AttachmentDetail',
            autoLoad: false,
            proxy: {
                type: 'memory'
            }
        },
        marketParticipants: {
            model: 'MAM.model.MarketParticipantDetail',
            autoLoad: false,
            proxy: {
                type: 'memory'
            }
        },
        plants: {
            model: 'MAM.model.PlantDetail',
            autoLoad: false,
            proxy: {
                type: 'memory'
            }
        },
        simCards: {
            model: 'MAM.model.SimCardDetail',
            autoLoad: false,
            proxy: {
                type: 'memory'
            }
        },
        transformers: {
            model: 'MAM.model.TransformerDetail',
            autoLoad: false,
            proxy: {
                type: 'memory'
            }
        },
        meterAddonDevices: {
            model: 'MAM.model.MeterAddonDeviceDetail',
            autoLoad: false,
            proxy: {
                type: 'memory'
            }
        },
        undefinedRelations: {
        //    model: 'MAM.model.Other',
            autoLoad: false,
            proxy: {
                type: 'memory'
            }
        },
        stores: {
            grid: {
                type: 'grid'
            }
        }
    }
});