Ext.define('MAM.view.person.Initial', {
    extend: 'Ext.container.Container',

    requires: [
        'MAM.view.action.Initial',
        'Energy.common.ui.Header'
    ],

    alias: 'widget.personInitial',

    viewModel: 'personInitialModel',
    controller: 'personInitialController',

    layout: {
       type: 'hbox',
        align: 'stretch',
        resizable: true
    },
    padding: '0 0 0 10',

    initComponent: function () {

        this.callParent(arguments);

        this.createPersonsList();
        this.addSelectMessage();
    },

    addSelectMessage: function(){
        this.lookupReference('detailContainer').removeAll();
        this.lookupReference('detailContainer').add({
            xtype: 'commonUiDisplayMessage',
            message: _tl.get('mam.label.ui.selectEntity')

        });
    },

    createPersonsList: function(){
        this.add(
            {
                xtype: 'container',
                width: 300,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype:'commonUiHeader',
                        header: _tl.get('mam.label.persons'),
                        bind: {
                            header:  '{totalText}'
                        }

                    },
                    {
                        xtype: 'personList',

                        flex: 1
                    }
                ]
            },
            {
                xtype: 'container',
                margin: '0 0 0 10',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                flex: 1,
                items: [
                    {
                        xtype:'commonUiHeader',
                        header: _tl.get('mam.label.person'),
                        colorClass: 'green',
                        margin: 10,
                        showArrow: true,
                        bind: {
                            header: _tl.get('mam.label.person') + ' {person.customId}',
                            subheader: _tl.get('mam.label.mode.view')
                        }

                    },
                    {
                        xtype: 'container',
                        cls: 'border-lrb',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        flex: 1,
                        reference: 'detailContainer'
                    }

                ]
            }, {
                xtype: 'actionInitial',
                viewModel: {
                    data: {
                        entity: MAM.utils.Constants.PERSON
                    }
                }
            }
        );
    },


    createPersonDetailContainer: function(){
        var detail = this.lookupReference('detailContainer');
        detail.removeAll();
        this.lookupReference('detailContainer').add(
            {
                xtype: 'personDetail',
                flex: 1
            },
            {
                xtype: 'personSubDetail',
                flex: 1
            }
        );
        
    }

});