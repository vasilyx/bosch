StartTest(function (t) {

    var storeHistory = Ext.create('MAM.store.action.history.History');
    storeHistory.clearData(); // clear sessions

    // reference data loaded via MockConfig. app.js


    function checkActionBarButtons(t, entity) {

        t.elementIsVisible(t.cq1(entity + 'Initial actionInitial'), ' ActionBar ' + entity + ' is visible');

        MAM.Config.ActionPanel.rules[entity].forEach(function(el){
            t.elementIsVisible(t.cq1(entity + 'Initial actionInitial toolbar button[cls=icon-' + el + ']'), entity + ' button ' + el + ' is visible');
        });
    }

    t.it('#1 check button configuration in the detail view', function (t) {
        t.chain(
            {waitForCQ: 'app-main tabbar'},

            {click: 'tabbar=> span.icon-meterPoint'},
            function (next) {
                checkActionBarButtons(t, 'meterPoint');
                // check if buttons disabled in edit mode
                t.expect( t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-information]').isDisabled()).toBe(true);
                t.expect( t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-action]').isDisabled()).toBe(true);

                var menu = t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-information]').getMenu();
                t.like(menu.items.items[0].text, _tl.get('mam.label.actionPanel.marketParticipantHistory'), 'marketParticipants menu item is available');
                t.like(menu.items.items[1].text, _tl.get('mam.label.actionPanel.meterReadings'), 'meterReadings menu item is available');


                var menuAction = t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-action]').getMenu();
                t.like(menuAction.items.items[0].text, _tl.get('mam.label.actionPanel.onDemandDelivery'), 'onDemandDelivery menu item is available');
                next();
            },
            {click: 'tabbar=> span.icon-gateway'},
            function (next) {
                checkActionBarButtons(t, 'gateway');
                var menu = t.cq1('gatewayInitial actionInitial toolbar button[cls=icon-information]').getMenu();
                t.expect(menu.items.items.length).toBe(0);

                var menuAction = t.cq1('gatewayInitial actionInitial toolbar button[cls=icon-action]').getMenu();
                t.like(menuAction.items.items[0].text, _tl.get('mam.label.actionPanel.getLogData'), 'getLogData menu item is available');

                next();
            },
            {click: 'tabbar=> span.icon-meter'},
            function (next) {
                checkActionBarButtons(t, 'meter');
                next();
            },
            {click: 'tabbar=> span.icon-person'},
            function (next) {
                checkActionBarButtons(t, 'person');

                t.expect( t.cq1('personInitial actionInitial toolbar button[cls=icon-information]').isDisabled()).toBe(true);
                t.expect( t.cq1('personInitial actionInitial toolbar button[cls=icon-action]').isDisabled()).toBe(true);
                next();
            },
            {click: 'tabbar=> span.icon-connectionBuilding'},
            function (next) {
                checkActionBarButtons(t, 'connectionBuilding');
                next();
            }
        )
    });

    t.it('#2 add history items', function (t) {
        t.chain(
            function (next) {
                MOCK_DATA.getMockedData('tests/data/meterPoint3.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/l/details_1.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/customAttributes_DE0700081632100000000000070238000.json', 1);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/relations_DE0700081632100000000000070238000.json', 1); // for related

                MOCK_DATA.getMockedData('tests/data/gateway3.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/gateways/l/details_5085.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/gateways/l/customAttributes_dummy.json', 5085);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/gateways/l/relations_dummy.json', 5085);

                MOCK_DATA.getMockedData('tests/data/meter3.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/details_1494747.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/customAttributes_dummy.json', 1494747);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meters/l/relations_dummy.json', 1494747);

                MOCK_DATA.getMockedData('tests/data/person3.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/l/details_1477904.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/l/customAttributes_dummy.json', 1477904);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/persons/l/relations_dummy.json', 1477904);

                next();
            },
            {waitForCQ: 'app-main tabbar'},

            {click: 'tabbar=> span.icon-gateway'},
            { doubleclick : t.compositeQuery('gatewayInitial gridpanel => .x-grid-cell')[1] },

            {click: 'tabbar=> span.icon-meter'},
            { doubleclick : t.compositeQuery('meterInitial gridpanel => .x-grid-cell')[1] },

            {click: 'tabbar=> span.icon-meterPoint'},
            { doubleclick : t.compositeQuery('meterPointInitial gridpanel => .x-grid-cell')[1] },
            {
                action      : 'wait',
                delay       : 1500      // 1500 ms
            },
            function(next) {
                // check if buttons available in edit mode
                t.expect( t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-information]').isDisabled()).toBe(false);
                t.expect( t.cq1('meterPointInitial actionInitial toolbar button[cls=icon-action]').isDisabled()).toBe(false);
                next();
            },
            {click: 'tabbar=> span.icon-person'},
            { doubleclick : t.compositeQuery('personInitial gridpanel => .x-grid-cell')[1] },

            {
                action      : 'wait',
                delay       : 1500      // 1500 ms
            },
            {click: t.cq1('personInitial historyInitial')},

            function (next) {
                var historyMenu = t.cq1('personInitial historyInitial').getMenu();


                var visibleItems = 0;
                historyMenu.items.each(function( item ) {

                    if (item.isVisible()) {
                        visibleItems++;
                    } else {
                        t.like(item.iconCls, 'person', 'Current entity is invisible');
                    }
                });

                t.expect(historyMenu.items.length).toBe(4); // elements in history
                t.expect(visibleItems).toBe(3); // visible items


                t.like(historyMenu.items.items[0].iconCls, 'person', 'Person entity is available');
                t.like(historyMenu.items.items[1].iconCls, 'meterPoint', 'MeterPoint entity is available');
                t.like(historyMenu.items.items[2].iconCls, 'meter', 'Meter entity is available');
                t.like(historyMenu.items.items[3].iconCls, 'gateway', 'Gateway entity is available');

                t.click(t.cq('personInitial historyInitial menuitem')[2]); // meter entity
                next();
            },
            {
                action      : 'wait',
                delay       : 1500      // 1500 ms
            },
            {click: t.cq1('meterInitial historyInitial')},
            {
                action      : 'wait',
                delay       : 1500      // 1500 ms
            },
            function(next) {
                t.cqExists('meterDetail', 'Found meterEdit details');
                t.expect( t.cq1('meterInitial actionInitial toolbar button[cls=icon-information]').isDisabled()).toBe(true);
                t.expect( t.cq1('meterInitial actionInitial toolbar button[cls=icon-action]').isDisabled()).toBe(true);

                var historyMenu = t.cq1('meterInitial historyInitial').getMenu();

                var visibleItems = 0;
                historyMenu.items.each(function( item ) {

                    if (item.isVisible()) {
                        visibleItems++;
                    } else {
                        t.like(item.iconCls, 'meter', 'Current entity is invisible');
                    }
                });

                t.expect(historyMenu.items.length).toBe(4); // elements in history
                t.expect(visibleItems).toBe(3); // visible items

                t.like(historyMenu.items.items[0].iconCls, 'meter', 'Meter entity is available');
                t.like(historyMenu.items.items[1].iconCls, 'person', 'Person entity is available');
                t.like(historyMenu.items.items[2].iconCls, 'meterPoint', 'MeterPoint entity is available');
                t.like(historyMenu.items.items[3].iconCls, 'gateway', 'Gateway entity is available');
                next();
            }
        );
    });

});