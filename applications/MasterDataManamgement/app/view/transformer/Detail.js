Ext.define('MAM.view.transformer.Detail', {
    extend: 'Ext.Panel',

    alias: 'widget.transformerDetail',


    bind: {
        data: '{transformer}'
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    scrollable: true,
    
    initComponent: function () {

        var defaults = {
            xtype: 'displayfield',
            labelWidth: 150
        };

        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if(this.responsiveConfig){
            responsiveConfig = this.responsiveConfig;
        }

        var items = [
            {
                xtype: 'container',
                cls: 'bg-grey',
                margin: 10,
                padding: '5 10 0 10',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                defaults: defaults,
                items: this.getNorthItems()

            },
            {
                xtype: 'container',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                padding: '0 20 0 20',
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getWestItems()
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getEastItems()
                    }
                ]
            }
        ];

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    },
    /*
     * Defines the top area items for connection building properties
     */
    getNorthItems: function(){

        var items = [
            {
                fieldLabel: _tl.get('mam.label.transformer.customId'),
                name: 'customId',
                flex: 1,
                bind: {
                    value: '{transformer.customId}'
                }
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                flex: 1,
                items: [
                    {
                        fieldLabel: _tl.get('mam.label.transformer.id'),
                        name: 'transformerId',
                        bind: {
                            value: '{transformer.id}'
                        }
                    }
                ]
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the left side of panel
     */
    getWestItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.transformer.deviceType'),
                name: 'deviceType',
                bind: {
                    value: '{transformer.deviceType}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.transformer.deviceClass'),
                name: 'deviceClass',
                bind: {
                    value: '{transformer.deviceClass}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.transformer.medium'),
                name: 'medium',
                bind: {
                    value: '{transformer.medium}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.transformer.manufacturer'),
                name: 'manufacturer',
                bind: {
                    value: '{transformer.manufacturer}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.transformer.calibrationYear'),
                name: 'calibrationYear',
                bind: {
                    value: '{transformer.calibrationYear}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.transformer.constructionYear'),
                name: 'constructionYear',
                bind: {
                    value: '{transformer.constructionYear}'
                }
            }

        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the right side of panel
     */
    getEastItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.transformer.buildInDate'),
                name: 'buildInDate',
                bind: {
                    value: '{transformer.buildInDate}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.transformer.buildOutDate'),
                name: 'buildOutDate',
                bind: {
                    value: '{transformer.buildOutDate}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.transformer.lotNumber'),
                name: 'lotNumber',
                bind: {
                    value: '{transformer.lotNumber}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.transformer.transformerConstant'),
                name: 'transformerConstant',
                bind: {
                    value: '{transformer.transformerConstant}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.transformer.transformerPhase'),
                name: 'transformerPhase',
                bind: {
                    value: '{transformer.transformerPhase}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.transformer.examinationNumber'),
                name: 'examinationNumber',
                bind: {
                    value: '{transformer.examinationNumber}'
                }
            }
        ];
        return items;
    }
});