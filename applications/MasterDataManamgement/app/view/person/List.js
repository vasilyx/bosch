Ext.define('MAM.view.person.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.personList',
    requires: [
        'MAM.view.person.ListModel',
        'MAM.view.person.ListController'
    ],
    viewModel: 'personListModel',
    controller: 'personListController',
    border: true,
    bind: {
        store: '{persons}'
    },
    viewConfig: {
        loadMask: false
    },
    dockedItems: [{
        xtype: 'pagingtoolbar',
        reference: 'pagingToolbar',
        bind: {
            store: '{persons}'
        },
        overflowHandler: 'menu',
        dock: 'bottom',
        displayInfo: true
    }],
    initComponent: function () {
        var columns = [
            {
                text: _tl.get('mam.label.person.customId'),
                dataIndex: 'customId',
                flex: 1,
                items: [{
                    xtype: 'textfield',
                    margin: 3,
                    flex: 1,
                    width: 270, //theme error
                    enableKeyEvents: true,
                    listeners: {
                        keyup: 'onCustomIdSearch',
                        buffer: 1000
                    }
                }]
            }
        ];
        Ext.apply(this, {columns: columns});
        this.callParent(arguments);
    }

});