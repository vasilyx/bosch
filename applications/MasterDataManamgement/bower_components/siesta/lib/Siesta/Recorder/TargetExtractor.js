/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
/**

@class Siesta.Recorder.TargetExtractor

The type of target, possible options:

- 'css'     css selector
- 'xy'      XY coordinates

*/
Class('Siesta.Recorder.TargetExtractor', {

    does       : [
        Siesta.Util.Role.Dom
    ],

    has        : {
        ariadneDomFinder            : null,
        
        uniqueDomNodeProperty       : 'id',

        swallowExceptions           : false,

        // Copy of the first exception that occurred during target extraction
        exception                   : null
    },

    methods : {

        initialize : function () {
            this.ariadneDomFinder   = new Ariadne.DomQueryFinder({
                uniqueDomNodeProperty   : this.uniqueDomNodeProperty
            })
        },
        
        
        findOffset : function (pageX, pageY, relativeTo) {
            var offset    = this.offset(relativeTo)
            
            offset.left   = offset.left
            offset.top    = offset.top

            var relativeOffset = [ pageX - offset.left, pageY - offset.top ]

            return relativeOffset;
        },
        
        
        findDomQueryFor : function (target, root) {
            return this.ariadneDomFinder.findQueries(target, root)[ 0 ]
        },
        
        
        resolveTarget : function (target) {
            if (target.type == 'css') {
                return Sizzle(target.target)[ 0 ]
            }
        },
        
        
        getTargets : function (event, useContainsSelector, saveOffset) {
            // By default we should use :contains selector unless useContainsSelector is strictly false
            useContainsSelector     = useContainsSelector === false ? false : true;

            var result              = []
            var cssQuery
            var target              = event.target;
            
            var hasCoordinates      = event.x != null && event.y != null
            
            if (target != target.ownerDocument.body || !hasCoordinates) {
                if (this.swallowExceptions) {
                    try {
                        cssQuery        = this.findDomQueryFor(target)
                    } catch (e) {
                        this.exception  = this.exception || e;
                    }
                } else {
                    cssQuery            = this.findDomQueryFor(target)
                }
    
                if (cssQuery) result.push({
                    type        : 'css',
                    target      : cssQuery,
                    offset      : hasCoordinates && (saveOffset || !this.isElementReachableAtCenter(target, false)) ? 
                        this.findOffset(event.x, event.y, target) 
                    : 
                        null
                })
            }

            hasCoordinates && result.push({
                type        : 'xy',
                target      : [ event.x, event.y ]
            })
            
            return result
        }
    }
});
