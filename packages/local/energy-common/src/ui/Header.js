/*
 * Possible colorClass: green, grey, darkGrey, red, blue25, blue50, blue75, blue100
 * When setting the hight, it will be adjusted if using arrow or not
 * so usually, if you set the height to x, 8 pixel will be substracted
 * e.g. we have to Header next to each other, one without arrow, the other with arrow
 * so the first Header will be set to 62 and the second also to 62 + 8 px for the arrow
 * 
 */

Ext.define("Energy.common.ui.Header", {
    extend: 'Ext.container.Container',
    alias: 'widget.commonUiHeader',
    cls: 'appHeader',
    height: 70,
    border: false,
    layout: {
        type: 'hbox'
    },
    colorClass: 'darkGrey',
    menu: undefined,

    initComponent : function(config) {

            if(!this.showArrow){
                this.height = this.height - 8;
            }

            var items = [
            {
                xtype: 'container',
                flex: 1,
                header: this.header,
                cls: this.showArrow && this.border ? 'border-lr' : '',
                subheader: this.subheader,
                colorClass: this.colorClass,
                showArrow: this.showArrow,
                height: this.height,
                fillData: function(record) {

                    var h = this.height;
                    var cc = this.colorClass;

                    if(record){

                        if(!record.height && this.showArrow){
                            record.contentHeight = h-8;
                        }
                        if(!record.colorClass){
                            record.colorClass = cc;
                        }
                        if(!record.showArrow){
                            record.showArrow = this.showArrow ? 'arrow' : '';
                        }

                        this.update(record);
                    }
                },
                tpl: [
                    '<tpl for=".">',
                    '  <div class="content bg-{colorClass}" style="height:{contentHeight}px">',
                    '       <span class="header">{header}</span>',
                    '       <br /><span>{subheader}</span>',
                    '  </div>',
                    '  <div class="{showArrow} img-{colorClass}" />',
                    '</tpl>'
                ]
            }
        ];


        /* if a menu is defined, so it is added to the header
         * it could be a button or a menu
         */
        if(this.menu != undefined){

            var colorClass = "bg-" + this.colorClass;
            var height = this.showArrow ? this.height - 8 : this.height;

            //console.log(height);

            var menuItems = {
                xtype: 'container',
                width: 50,
                height: height,
                cls: colorClass,
                style: {
                    height: height + 'px !important',
                    borderBottom: '1px solid #e0e0e1'
                },
                items: [ this.menu ]
            };

            items.push(menuItems);

        }

        Ext.apply(this, {items: items});

        this.callParent(config);

        this.updateHeader();

    },

    fillData: function(record) {

        var me = this;
        me.down('container').fillData(record);
    },

    updateHeader: function( data ){

        var me = this;
        var header = me.down('container');


        if(data != null){
            Ext.apply(header,  data );
        }

        var newData = {
            header: header.header,
            subheader: header.subheader,
            colorClass: header.colorClass,
            contentHeight: header.showArrow ? header.height - 8 : header.height,
            showArrow: header.showArrow ? 'arrow' : ''
        };

        me.down('container').fillData(newData);

    },
    setSubheader: function( value ){
        var subheader = { subheader: value };
        this.updateHeader( subheader );
    },
    setHeader: function( value ){
        var header = { header: value };
        this.updateHeader( header );
    }

}); 
