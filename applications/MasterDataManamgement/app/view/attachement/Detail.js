Ext.define('MAM.view.attachment.Detail', {
    extend: 'Ext.Panel',

    requires: [
        'MAM.view.attachment.DetailModel',
        'MAM.view.attachment.DetailController'
    ],

    alias: 'widget.attachmentDetail',

    viewModel: 'attachmentDetailModel',
    controller: 'attachmentDetailController',

   layout: {
        type: 'vbox',
        align: 'stretch'
    },
    scrollable: true,
    
    initComponent: function () {

        var defaults = {
            xtype: 'displayfield',
            labelWidth: 150
        };

        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if(this.responsiveConfig){
            responsiveConfig = this.responsiveConfig;
        }

        var items = [
            {
                xtype: 'container',
                cls: 'bg-grey',
                margin: 10,
                padding: '0 10 0 10',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                defaults: defaults,
                items: this.getNorthItems()

            },
            {
                xtype: 'container',
                padding: '0 10 0 20',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getWestItems()
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        defaults: defaults,
                        items: this.getEastItems()
                    }
                ]
            }
        ];

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    },
    /*
     * Defines the top area items for connection building properties
     */
    getNorthItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.attachment.id'),
                name: 'customId',
                flex: 1,
                bind: {
                    value: '{attachment.id}'
                }
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                flex: 1,
                items: [
                    {
                        fieldLabel: _tl.get('mam.label.attachment.createdAt'),
                        flex: 1,
                        name: 'createdAt',
                        bind: {
                            value: '{attachment.timestamp}'
                        }
                    }
                ]
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the left side of panel
     */
    getWestItems: function(){
        var items = [
            {
                fieldLabel: _tl.get('mam.label.attachment.description'),
                name: 'description',
                bind: {
                    value: '{attachment.description}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.attachment.documentType'),
                name: 'documentType',
                bind: {
                    value: '{attachment.documentType}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.attachment.mimeType'),
                name: 'mimeType',
                bind: {
                    value: '{attachment.mimeType}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.attachment.filename'),
                name: 'filename',
                bind: {
                    value: '{attachment.filename}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.attachment.updatedLast'),
                name: 'updatedLast',
                bind: {
                    value: '{attachment.fileTimestamp}'
                }
            }
        ];
        return items;
    },
    /**
     * Defines the area items for attachment properties
     * on the right side of panel
     */
    getEastItems: function() {
        var items = [
            {
                xtype: 'container',
                reference: 'imageContainer',
                tpl: '<tpl for="."><img src="{data}" width="{imgWidth}" /></tpl>',
                bind: {
                    data: '{document}'
                },
                hidden: true

            },
            {
                xtype: 'container',
                reference: 'attachmentContainer',
                tpl: '<tpl for="."><a href="{data}"/>Download</a></tpl>',
                bind: {
                    data: '{document}'
                },
                hidden: true
            }
         ];

        return items;
    }
});