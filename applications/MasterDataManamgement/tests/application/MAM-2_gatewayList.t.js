StartTest( function(t) {

    var list = Ext.create('TestApplication.application.AbstractList', {t: t, name: 'gateway'});

    MOCK_DATA.getMockedDataDetail('tests/data/detail/gateways/l/details_5085.json');
    MOCK_DATA.getMockedDataDetail('tests/data/detail/gateways/l/details_5088.json');
    //dummy only to mock data and don't get error from browser
    MOCK_DATA.getMockedDataDetail('tests/data/detail/gateways/l/customAttributes_dummy.json', 5085);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/gateways/l/relations_dummy.json', 5085);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/gateways/l/customAttributes_dummy.json', 5088);
    MOCK_DATA.getMockedDataDetail('tests/data/detail/gateways/l/relations_dummy.json', 5088);

    list.testListViewArea();
    list.testFilledDataInList(26);
    list.testPageCorrect();
    list.testDetailArea();
    list.testFilterById('EPPC0210487402');
    list.testFilterEmpty();
    list.testSelectRow(1); // select second row
    list.testSelectRow(4, 8);
});
