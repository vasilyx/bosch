Ext.define('MAM.view.marketParticipant.InitialModel', {
    extend: 'Ext.app.ViewModel',
    requires: [
        'MAM.model.Gateway',
        'MAM.utils.model.Converts'
    ],

    alias: 'viewmodel.marketParticipantInitialModel',

    data: {
        marketParticipant: null,
        relations: null,
        total: 0
    },

    
    formulas: {
        medium: function(get){
            return  MAM.utils.model.Converts.convertMedium( get('marketParticipant.medium') );
        },
        measuringType: function(get){
            return  MAM.utils.model.Converts.convertMeasuringType( get('marketParticipant.measuringType') );
        },
        totalText: {
            get: function(get) {
                var total = get('total');
                if(total === 1){
                    return Ext.util.Format.number(total, '0,000') + ' ' + _tl.get('mam.label.marketParticipant');
                }
                else{
                    return Ext.util.Format.number(total, '0,000') + ' ' + _tl.get('mam.label.marketParticipants')
                }
            }
        }
    }
});