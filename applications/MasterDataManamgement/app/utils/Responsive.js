Ext.define('MAM.utils.Responsive', {

    singleton: true,

    detailHeaderPanel: {
        'width < 1280': {
            layout: {
                type: 'box',//'vbox',
                align: 'stretch',
                vertical: true
            }
        },
        'width >= 1280': {
            layout: {
                type: 'box',//'hbox',
                align: 'stretch',
                vertical: false
            }
        }
    },
    subDetailHeaderPanel: {
        'width - 730 < 1280': {
            layout: {
                type: 'box',//'vbox',
                align: 'stretch',
                vertical: true
            }
        },
        'width - 730 >= 1280': {
            layout: {
                type: 'box',//'hbox',
                align: 'stretch',
                vertical: false
            }
        }
    }

});