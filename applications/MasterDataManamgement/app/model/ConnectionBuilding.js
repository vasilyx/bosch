Ext.define('MAM.model.ConnectionBuilding', {
    extend: 'Ext.data.Model',
    requires: [
        'MAM.model.Relation'
    ],
    idProperty: 'id',
    fields: [
        { name: 'id', type: 'int', mapping: 'connectionBuildingId'},
        { name: 'customId', type: 'string'},
        { name: 'name', type: 'string'},
        { name: 'note', type: 'string'},

        { name: 'street', type: 'string'},
        { name: 'houseNumber', type: 'string'},
        { name: 'houseNumberAppendix', type: 'string'},

        { name: 'postalCode', type: 'string'},
        { name: 'city', type: 'string'},
        { name: 'country', type: 'string'},

        { name: 'municipalityKey', type: 'string'},

        { name: 'longitude', type: 'string'},
        { name: 'latitude', type: 'string'},

        { name: 'createdTimeStamp', convert: MAM.utils.model.Converts.convertDate },

        { name: 'customAttributes', reference: 'MAM.model.CustomAttribute'},
        { name: 'relations', reference: 'MAM.model.Relation', unique: true }

    ]
});