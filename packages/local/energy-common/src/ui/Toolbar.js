Ext.define("Energy.common.ui.Toolbar", {
    extend: 'Ext.toolbar.Toolbar',
    
    alias: 'widget.commonUiToolbar',

    config : {
        title: 'none',
        buttonsBefore: undefined,
        buttonsAfter: undefined
    },
    initComponent: function(config) {

        this.style = {
            borderBottom: '1px solid #CCCCCC !important',
            borderWidth: '0px',
            paddingBottom: '2px !important'
        };

        var items = [];

        if(this.buttonsBefore != undefined){

            for(i=0; i < this.buttonsBefore.length; i++){
                items.push(this.buttonsBefore[i]);
            }
        }

        items.push({

            xtype: 'container',
            html: '<b>' + this.title + '</b>'

        });
        items.push('->');

        if(this.buttonsAfter != undefined){

            for(i=0; i < this.buttonsAfter.length; i++){
                items.push(this.buttonsAfter[i]);
            }
        }


        this.items = items;

        this.initConfig(config);
        this.callParent(config);
    },

    updateToolbarTitle: function(newTitle){

        this.down('container').update('<b>' + newTitle + '</b>');

    }



}); 