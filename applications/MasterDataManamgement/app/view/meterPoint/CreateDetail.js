Ext.define('MAM.view.meterPoint.CreateDetail', {
    extend: 'Ext.Panel',

    controller: 'meterPointCreateDetailController',
    alias: 'widget.meterPointCreateDetail',
    requires: ['MAM.utils.Configuration'],

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    scrollable: true,

    initComponent: function() {

        var defaults = {
            xtype: 'textfield',
            labelWidth: 150
        };

        var editForm = {
            xtype: 'form',
            trackResetOnLoad: true,
            id: 'meterPointCreateForm',
            items:[]
        };

        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if(this.responsiveConfig){
            responsiveConfig = this.responsiveConfig;
        }

        editForm.items = [
            {
                xtype: 'container',
                margin: '10 0 0 10',
                padding: '0 25 0 15',
                plugins: 'responsive',
                layout: 'form',
                responsiveConfig: responsiveConfig,
                defaults: defaults,
                items: this.getNorthItems()
            },

            {
                xtype: 'container',
                plugins: 'responsive',
                responsiveConfig: responsiveConfig,
                padding: '0 20 0 20',
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        layout: 'form',
                        padding: '0 40 0 0',
                        defaults: defaults,
                        items: this.getWestItems()
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        layout: 'form',
                        defaults: defaults,
                        items: this.getEastItems()
                    }
                ]
            }
        ];

        var buttons =  [{
            text: _tl.get('mam.label.saveViewButton'),
            disabled: true,
            itemId: 'saveViewButton'
        },{
            text: _tl.get('mam.label.saveEditButton'),
            disabled: true,
            itemId: 'saveEditButton'
        }, {
            text: _tl.get('mam.label.close'),
            itemId: 'closeButton'
        }];

        Ext.apply(this, {items: editForm, buttons: buttons});
        this.callParent(arguments);
    },
    getNorthItems: function() {

        var items = [
            {
                fieldLabel: _tl.get('mam.label.meterPoint.customId'),
                name: 'customId',
                length: 33,
                allowBlank: false,
                minLength: 33,
                maxLength: 33,
                regex: /^[A-Z]{2}\w{31}$/,
                regexText: _tl.get('mam.notice.meterPoint.customId.validate'),
                flex: 1
            },
            {
                xtype: 'commonFormTooltip',
                quicktipHeader: _tl.get('mam.notice.meterPoint.customId.quicktipHeader'),
                quicktip: _tl.get('mam.notice.meterPoint.customId.quicktip')
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the left side of panel
     */
    getWestItems: function(){
        var items = [
            {
                xtype: 'combo',
                name: 'medium',
                allowBlank: false,
                fieldLabel: _tl.get('mam.label.meterPoint.medium'),
                displayField: 'name',
                valueField: 'code',
                width: 'auto',
                store: new Ext.data.JsonStore({
                    autoLoad: true,
                    proxy: {
                        type: 'ajax',
                        url: MAM.utils.Configuration.getUrlEnumProperty( MAM.utils.Constants.METERPOINT, 'medium'),
                        reader: {
                            type: 'json',
                            rootProperty: 'medium',
                            transform: {
                                fn: function(data) {
                                    for (k in data.medium) {
                                        data.medium[k].name = _tl.get('mam.medium.' + data.medium[k].name);
                                    }
                                    return data;
                                },
                                scope: this
                            }
                        }
                    }
                })
            },
            {
                xtype: 'combo',
                name: 'measuringType',
                allowBlank: false,
                fieldLabel: _tl.get('mam.label.meterPoint.measuringType'),
                displayField: 'name',
                valueField: 'code',
                store: new Ext.data.JsonStore({
                    autoLoad: true,
                    proxy: {
                        type: 'ajax',
                        url: MAM.utils.Configuration.getUrlEnumProperty( MAM.utils.Constants.METERPOINT, 'measuringType'),
                        reader: {
                            type: 'json',
                            rootProperty: 'measuringType',
                            transform: {
                                fn: function(data) {
                                    for (k in data.measuringType) {
                                        data.measuringType[k].name = _tl.get('mam.measuringType.' + data.measuringType[k].name);
                                    }
                                    return data;
                                },
                                scope: this
                            }
                        }
                    }
                })
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.measuringProduct'),
                name: 'measuringProduct'
            },
            {
                xtype: 'combo',
                fieldLabel: _tl.get('mam.label.meterPoint.transformRatio'),
                name: 'transformRatio',
                displayField: 'name',
                valueField: 'code',
                store: new Ext.data.JsonStore({
                    autoLoad: true,
                    proxy: {
                        type: 'ajax',
                        url: MAM.utils.Configuration.getUrlEnumProperty( MAM.utils.Constants.METERPOINT, 'transformRatio'),
                        reader: {
                            type: 'json',
                            rootProperty: 'transformRatio'
                        }
                    }
                })
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.hindrance'),
                name: 'hindrance',
                bind: {
                    value: '{meterPoint.hindrance}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.hindranceReason'),
                name: 'hindranceReason',
                bind: {
                    value: '{meterPoint.hindranceReason}'
                }
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the right side of panel
     */
    getEastItems: function(){
        var items = [
            {
                xtype: 'combo',
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionUnit'),
                name: 'consumptionUnit',
                displayField: 'name',
                valueField: 'code',
                store: new Ext.data.JsonStore({
                    autoLoad: true,
                    proxy: {
                        type: 'ajax',
                        url: MAM.utils.Configuration.getUrlEnumProperty( MAM.utils.Constants.METERPOINT, 'consumptionUnit'),
                        reader: {
                            type: 'json',
                            rootProperty: 'consumptionUnit'
                        }
                    }
                })
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionForecast'),
                name: 'consumptionForecast'
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionAverage'),
                name: 'consumptionAverage'
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionYear1'),
                name: 'consumptionYear1'
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionYear2'),
                name: 'consumptionYear2'
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionYear3'),
                name: 'consumptionYear3'
            }
        ];
        return items;
    }
});