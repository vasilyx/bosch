Ext.define('Energy.mom.view.MarketParticipantHistoryController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.actionMarketParticipantHistoryController',

    control: {
        '#closeButton': {
            click:'close'
        },
        '#marketParticipantHistoryGrid': {
            afterrender: 'loadMarketParticipantHistory'
        }
    },
    /**
     * close the view, if the view is used or open
     * in a window
     */
    close: function() {
        //close if the underlaying component is a window
        if(this.getView().up().getXType() === 'actionWindow'){
            this.getView().up().close();
        }
    },
    loadMarketParticipantHistory: function() {
        var store = this.getViewModel().getStore('momMarketParticipants');
        store.getProxy().setUrl(
            Ext.String.format( store.getProxy().getUrl(), this.getViewModel().get('record').get('customId'))
        );
        store.load();
    }
});