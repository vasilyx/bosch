Ext.define('MAM.model.Gateway', {
    extend: 'Ext.data.Model',
    requires: [
        'MAM.model.Relation',
        'MAM.model.Certificate'
    ],
    idProperty: 'id',
    fields: [
        { name: 'id', type: 'int', mapping: 'gatewayId'},
        { name: 'customId', type: 'string'},
        { name: 'description', type: 'string'},

        { name: 'manufacturer', type: 'string'},
        { name: 'propertyNumber', type: 'string'},
        { name: 'firmware', type: 'string'},

        { name: 'communication', type: 'string'},
        { name: 'communicationType', type: 'string'}, /* GPRS, DSL */

        { name: 'status', type: 'string'}, /* PLANNED, MOUNTED, DISMOUNTED */
        { name: 'configStatus', type: 'string'}, /* CONFIGURED, UNCONFIGURED */


        { name: 'calibrationYear', type: 'string'},
        { name: 'calibrationEndYear', type: 'string'},
        { name: 'constructionYear', type: 'string'},

        { name: 'buildInDate', convert: MAM.utils.model.Converts.convertDate },
        { name: 'buildOutDate', convert: MAM.utils.model.Converts.convertDate },

        { name: 'lotNumber', type: 'string'},
        { name: 'examinationNumber', type: 'string'},
        { name: 'charge', type: 'string'},

        { name: 'customAttributes', reference: 'MAM.model.CustomAttribute'},
        { name: 'relations', reference: 'MAM.model.Relation'},
        { name: 'certificates'},

        { name: 'createdTimeStamp', convert: MAM.utils.model.Converts.convertDate }

    ]
    /* TODO: Change to reference, but still more time to check how it works */
    /*hasMany: [
        { name: 'certificates', associationKey: 'certificates', model: 'MAM.model.Certificate' }
    ]*/
});