Ext.define('MAM.view.meterPoint.RelationsList', {
    extend: 'MAM.view.relations.EntityList',
    requires: [
        'MAM.view.relations.EntityList'
    ],
    alias: 'widget.meterPointsRelationsList',

    getToolbar: function () {
        return [{
            xtype: 'textfield',
            name: 'customId',
            hideLabel: true,
            width: 200
        }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'description',
                hideLabel: true,
                width: 120
            }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'measuringProduct',
                hideLabel: true,
                width: 120
            }, this.getButtonClear(), '-'
            , {
                xtype: 'textfield',
                name: 'measuringProduct',
                hideLabel: true,
                flex: 1
            }, this.getButtonClear()
        ];
    },

    getColumns: function () {
        return [
            {text: _tl.get("mam.label.meterPoint.customId"), dataIndex: 'customId', width: 250},
            {text: _tl.get("mam.label.meterPoint.description"), dataIndex: 'description', width: 170},
            {text: _tl.get("mam.label.meterPoint.measuringProduct"), dataIndex: 'measuringProduct', width: 170},
            {text: _tl.get("mam.label.meterPoint.measuringType"), dataIndex: 'measuringType', flex: 1}
        ];
    },

    initComponent: function () {

        this.callParent(arguments);
    }
});