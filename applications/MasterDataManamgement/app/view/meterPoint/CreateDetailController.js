Ext.define('MAM.view.meterPoint.CreateDetailController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.meterPointCreateDetailController',
    control: {
        '#meterPointCreateForm': {
            validitychange: 'onChangeCreateForm'
        },
        'meterPointCreateDetail button#saveViewButton': {
            click: 'onSaveView'
        },
        'meterPointCreateDetail button#saveEditButton': {
            click: 'onSaveEdit'
        },
        'meterPointCreateDetail button#closeButton': {
            click: 'onCloseCreateWindow'
        }
    },

    onSaveView: function() {
        this.create(false);
    },

    onSaveEdit: function() {
        this.create(true);
    },

    create: function(edit) {
        var record = Ext.create('MAM.model.MeterPointDetail', {}),
            me = this;

        record.set(this.getView().down('#meterPointCreateForm').getValues());
        record.save({
            success: function (request, response) {
                me.onCloseCreateWindow();
                Ext.getStore('meterPoints').reload();
                var data = Ext.JSON.decode(response.getResponse().responseText);

                if (data.hasOwnProperty('id') && data.id) {
                    if (edit === true) {
                        me.fireEvent('openMeterPointEdit', data.id);
                    } else {
                        me.redirectTo('/'+ MAM.utils.Constants.METERPOINT + '/itemId/' + data.id);
                    }

                } else {
                    Ext.create('Energy.common.ui.DialogBox').showAlert('Server didn\'t return entity id that was created');
                }


            },
            failure: function (request, response) {
                switch (response.getError().status) {
                    case 400:
                        Ext.create('Energy.common.ui.DialogBox').showWarning(_tl.get('mam.dialogbox.meterPoint.create.response400'));
                        break;
                    case 409:
                        Ext.create('Energy.common.ui.DialogBox').showWarning(_tl.get('mam.dialogbox.meterPoint.create.response409'));
                        break;
                    default:
                        Ext.create('Energy.common.ui.DialogBox').showWarning('Unknown server response - ' + response.getError().status);
                }
            }
        });
    },

    onCloseCreateWindow: function() {
        if(this.getView().up().getXType() === 'actionWindow'){
            this.getView().up().close();
        }
    },

    onChangeCreateForm: function(form) {
        this.getView().down('#saveViewButton').setDisabled(!form.isValid());
        this.getView().down('#saveEditButton').setDisabled(!form.isValid());
    }
});