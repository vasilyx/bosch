Ext.define('MAM.view.meterPoint.EditDetail', {
    extend: 'Ext.Panel',

    alias: 'widget.meterPointEditDetail',
    requires: ['MAM.utils.Configuration'],

    bind: {
        data: '{meterPoint}'
    },
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    scrollable: true,

    initComponent: function () {

        var defaults = {
            xtype: 'textfield',
            labelWidth: 150
        };

        var editForm = {
            xtype: 'form',
            trackResetOnLoad: true,
            id: 'meterPointEditForm',
            items:[]
        };

        var responsiveConfig = MAM.utils.Responsive.detailHeaderPanel;
        if(this.responsiveConfig){
            responsiveConfig = this.responsiveConfig;
        }

         editForm.items = [
             {
                            xtype: 'container',
                            cls: 'bg-grey',
                            margin: 10,
                            padding: '5 10 0 10',
                            plugins: 'responsive',
                            responsiveConfig: responsiveConfig,
                            defaults: defaults,
                            items: this.getNorthItems()

                        },
                        {
                            xtype: 'container',
                            plugins: 'responsive',
                            responsiveConfig: responsiveConfig,
                            padding: '0 20 0 20',
                            items: [
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: 'form',
                                    padding: '0 40 0 0',
                                    defaults: defaults,
                                    items: this.getWestItems()
                                },
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: 'form',
                                    defaults: defaults,
                                    items: this.getEastItems()
                                }
                            ]
                        }
        ];

        Ext.apply(this, {items: editForm});
        this.callParent(arguments);
    },
    /*
     * Defines the top area items for connection building properties
     */
    getNorthItems: function(){

        var items = [
            {
                fieldLabel: _tl.get('mam.label.meterPoint.customId'),
                name: 'customId',
                flex: 1,
                xtype: 'displayfield',
                bind: {
                    value: '{meterPoint.customId}'
                }
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'displayfield',
                    labelWidth: 150
                },
                flex: 1,
                items: [
                    {
                        fieldLabel: _tl.get('mam.label.meterPoint.createdTimeStamp'),
                        flex: 1,
                        name: 'createdTimeStamp',
                        bind: {
                            value: '{meterPoint.createdTimeStamp}'
                        }
                    },
                    {
                        fieldLabel: _tl.get('mam.label.meterPoint.id'),
                        name: 'meterPointId',
                        bind: {
                            value: '{meterPoint.id}'
                        }
                    }
                ]
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the left side of panel
     */
    getWestItems: function(){
        var items = [
            {
                xtype: 'combo',
                name: 'medium',
                allowBlank: false,
                forceSelection : true,
                fieldLabel: _tl.get('mam.label.meterPoint.medium'),
                displayField: 'name',
                valueField: 'code',
                width: 'auto',
                store: new Ext.data.JsonStore({
                    autoLoad: true,
                    proxy: {
                        type: 'ajax',
                        url: MAM.utils.Configuration.getUrlEnumProperty( MAM.utils.Constants.METERPOINT, 'medium'),
                        reader: {
                            type: 'json',
                            rootProperty: 'medium',
                            transform: {
                                fn: function(data) {
                                    for (k in data.medium) {
                                        data.medium[k].name = _tl.get('mam.medium.' + data.medium[k].name);
                                    }
                                    return data;
                                },
                                scope: this
                            }
                        }
                    }
                }),
                bind: {
                    value: '{meterPoint.medium}'
                }
            },
            {
                xtype: 'combo',
                name: 'measuringType',
                forceSelection: true,
                allowBlank: false,
                fieldLabel: _tl.get('mam.label.meterPoint.measuringType'),
                displayField: 'name',
                valueField: 'code',
                store: new Ext.data.JsonStore({
                    autoLoad: true,
                    proxy: {
                        type: 'ajax',
                        url: MAM.utils.Configuration.getUrlEnumProperty( MAM.utils.Constants.METERPOINT, 'measuringType'),
                        reader: {
                            type: 'json',
                            rootProperty: 'measuringType',
                            transform: {
                                fn: function(data) {
                                    for (k in data.measuringType) {
                                        data.measuringType[k].name = _tl.get('mam.measuringType.' + data.measuringType[k].name);
                                    }
                                    return data;
                                },
                                scope: this
                            }
                        }
                    }
                }),
                bind: {
                    value: '{meterPoint.measuringType}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.measuringProduct'),
                name: 'measuringProduct',
                bind: {
                    value: '{meterPoint.measuringProduct}'
                }
            },
            {
                xtype: 'combo',
                fieldLabel: _tl.get('mam.label.meterPoint.transformRatio'),
                name: 'transformRatio',
                displayField: 'name',
                forceSelection: true,
                valueField: 'code',
                store: new Ext.data.JsonStore({
                    autoLoad: true,
                    proxy: {
                        type: 'ajax',
                        url: MAM.utils.Configuration.getUrlEnumProperty( MAM.utils.Constants.METERPOINT, 'transformRatio'),
                        reader: {
                            type: 'json',
                            rootProperty: 'transformRatio'
                        }
                    }
                }),
                bind: {
                    value: '{meterPoint.transformRatio}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.hindrance'),
                name: 'hindrance',
                bind: {
                    value: '{meterPoint.hindrance}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.hindranceReason'),
                name: 'hindranceReason',
                bind: {
                    value: '{meterPoint.hindranceReason}'
                }
            }
        ];
        return items;
    },
    /**
     * Defines the area items for gatway properties
     * on the right side of panel
     */
    getEastItems: function(){
        var items = [
            {
                xtype: 'combo',
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionUnit'),
                name: 'consumptionUnit',
                displayField: 'name',
                valueField: 'code',
                forceSelection: true,
                //TODO: move stores to viewModel
                store: new Ext.data.JsonStore({
                    autoLoad: true,
                    proxy: {
                        type: 'ajax',
                        url: MAM.utils.Configuration.getUrlEnumProperty( MAM.utils.Constants.METERPOINT, 'consumptionUnit'),
                        reader: {
                            type: 'json',
                            rootProperty: 'consumptionUnit'
                        }
                    }
                }),
                bind: {
                    value: '{meterPoint.consumptionUnit}'
                    /*store: '{consumptionUnits}'*/
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionForecast'),
                fieldStyle: 'text-align: right;',
                name: 'consumptionForecast',
                bind: {
                    value: '{meterPoint.consumptionForecast}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionAverage'),
                fieldStyle: 'text-align: right;',
                name: 'consumptionAverage',
                bind: {
                    value: '{meterPoint.consumptionAverage}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionYear1'),
                fieldStyle: 'text-align: right;',
                name: 'consumptionYear1',
                bind: {
                    value: '{meterPoint.consumptionYear1}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionYear2'),
                fieldStyle: 'text-align: right;',
                name: 'consumptionYear2',
                bind: {
                    value: '{meterPoint.consumptionYear2}'
                }
            },
            {
                fieldLabel: _tl.get('mam.label.meterPoint.consumptionYear3'),
                fieldStyle: 'text-align: right;',
                name: 'consumptionYear3',
                bind: {
                    value: '{meterPoint.consumptionYear3}'
                }
            }
        ];
        return items;
    }
});