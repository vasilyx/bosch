Ext.define('MAM.view.person.InitialController', {
    extend: 'Ext.app.ViewController',
    requires: [
        'MAM.model.PersonDetail',
        'MAM.model.Relation',
        'MAM.model.CustomAttribute'
    ],
    alias: 'controller.personInitialController',

    control: {
        'personInitial personList': {
            itemdblclick: 'onRecordSelect'
        },
        'personInitial': {
            show: 'onShowInitial'
        }
    },
    listen: {
         store: {
             '#persons': {
                load: 'onPersonsLoad'
             }
         }
     },
    routes : {
        '/person/itemId/:id' : 'loadDetails'
    },

    onShowInitial: function() {
        this.fireEvent('openEntity', MAM.utils.Constants.PERSON, this.getViewModel().get('person'));
    },

    /**
     * Loads the necessary data for given entity
     * @param rowModel
     * @param record
     */
    onRecordSelect: function(rowModel, record){
        this.loadDetails(record.getId());
    },

    /**
     * Load entity details,
     * @param id int
     * return void
     */
    loadDetails: function(id) {
        var me = this;
        this.fireEvent('toggleLoadingMask', true , _tl.get('mam.label.ui.loading.personDetail'));
        if(!me.getViewModel().get('person')){
            me.getView().createPersonDetailContainer();
        }

        MAM.model.PersonDetail.load( id, {
            success: me.onDataPersonLoadSuccess,
            failure: me.onDataPersonLoadError,
            scope: me
        });
    },

    /**
     * get total count while loading persons
     * @param store
     * @param records
     */
    onPersonsLoad: function( store, records ){
        this.getViewModel().set('total', store.getTotalCount());
    },

    /**
     * Loads custom attributes after sucessfull
     * person load
     * @param recordDetail
     */
    onDataPersonLoadSuccess: function( recordDetail ){
        var me = this;
        var viewModel = this.getViewModel();
        viewModel.set('person', recordDetail);

        this.fireEvent('detailsLoaded', MAM.utils.Constants.PERSON);
        this.fireEvent('openEntity', MAM.utils.Constants.PERSON, recordDetail);

        var tabConfig = MAM.utils.Configuration.getTabConfig( MAM.utils.Constants.PERSON );
        //check which tabs are configured
        Ext.each(tabConfig, function( config ){
            if(config.type === MAM.utils.Constants.CUSTOMATTRIBUTESDETAIL){

                //set custom attributes empty
                viewModel.set('customAttributes', null );

                //load meter point customAttributes
                MAM.model.CustomAttribute.getProxy().setEntity( MAM.utils.Constants.PERSONS );
                MAM.model.CustomAttribute.getProxy().on('exception', me.onDataPersonCustomAttributesLoadError , me);

                MAM.model.CustomAttribute.load( recordDetail.getId(), {
                    success: me.onDataPersonCustomAttributeLoadSuccess,
                    failure: me.onDataPersonCustomAttributesLoadError,
                    scope: me
                });
            }
        });
        this.fireEvent('toggleLoadingMask', false );

    },
    /**
     * Creates a customattributechange event when person
     * custom attributes are successful loaded
     * @param recordDetail
     */
    onDataPersonCustomAttributeLoadSuccess: function( recordDetail ){
        var viewModel = this.getViewModel();
        viewModel.get('person').set('customAttributes', recordDetail);
        this.fireEvent('customattibuteschange', recordDetail, MAM.utils.Constants.PERSONS );
    },
    /**
     * Person cannot be loaded
     * so process no relations and custom attributes error
     * to display empty data
     */
    onDataPersonLoadError: function(){
        this.onDataPersonCustomAttributesLoadError();
        this.fireEvent('toggleLoadingMask', false );
    },
    /**
     * custom attributes of person can not be loaded
     * fire event customattibuteschange with an empty relation to
     * display empty data
     * @param request
     */
    onDataPersonCustomAttributesLoadError: function( request ){
        var emptyRecord = Ext.create('MAM.model.CustomAttribute');
        this.fireEvent('customattibuteschange', emptyRecord, MAM.utils.Constants.PERSONS );
        this.fireEvent('toggleLoadingMask', false );
    }


});