/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Launcher.FileSystem.SlimerJS', {
    
    does        : Siesta.Launcher.FileSystem.PhantomJS,
    
    methods : {

        // assumes to always be called with linux-style filename
        ensurePathExists : function (fileName) {
            var match       = /(.*)\/[^/]*$/.exec(fileName)
            
            if (match) {
                fileName    = this.normalizePath(match[ 1 ])
                
                require('fs').makeTree(fileName)
            }
        }
    },
    
    
    override : {
        
        normalizePath : function (path) {
            path        = path.replace(/^\.\//g, '').replace(/\/\.\//g, '/')
            
            path        = this.isWindows ? path.replace(/\//g, '\\') : path
            
            return this.SUPER(path)
        }
    }
})
