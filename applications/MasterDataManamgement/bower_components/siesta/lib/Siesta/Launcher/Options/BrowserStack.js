/*

Siesta 4.2.0
Copyright(c) 2009-2016 Bryntum AB
http://bryntum.com/contact
http://bryntum.com/products/siesta/license

*/
Role('Siesta.Launcher.Options.BrowserStack', {
    
    override : {
        
        getBrowserName : function () {
            var options         = this.options
            
            var res             = this.SUPER()
            
            return res || options.caps.browser
        },
        
        
        validate : function () {
            if (this.SUPER() === false) return false
            
            var options             = this.options
            
            var bsUserName          = options[ 'browserstack-user' ]
            var bsKey               = options[ 'browserstack-key' ]
            
            var bsShortCut          = options[ 'browserstack' ] || options[ 'browserstack_' ]
            
            if (bsShortCut) {
                var parts           = (bsShortCut === true ? '' : bsShortCut).split(',')
                
                bsUserName          = parts.shift() || process.env.BS_USER
                bsKey               = parts.shift() || process.env.BS_KEY
                
                if (!bsUserName) {
                    this.printError("No BrowserStack user name found for --browserstack shortcut")
                    return false
                }
            }
            
            if (bsUserName && !bsKey) {
                this.printError("No BrowserStack key provided, for user name: " + bsUserName)
                return false
            }
            
            if (bsUserName && bsKey) {
                if (options.host) {
                    this.printError("BrowserStack mode is enaled, but host is already set to: " + options.host)
                    return false
                }
                
                options.host        = "http://" + bsUserName + ":" + bsKey + "@hub.browserstack.com/wd/hub"
                options.port        = 4444
                
                this.debug("--host set to " + options.host.replace(bsKey, 'BROWSERSTACK_API_KEY'))
                
                options.bs          = {
                    userName        : bsUserName,
                    key             : bsKey,
                    shortCut        : bsShortCut,
                    noTunnel        : Boolean(options[ 'browserstack_' ])
                }
                
                if (!options.caps[ 'browserstack.local' ]) 
                    this.addCapability('browserstack.local', 'true')
                
                if (options.build) 
                    this.addCapability('build', options.build)
            }
            
            return true
        }
    }
})
