/*
 * DayMonthSelector
 * Selects a day and month of a year
 * author: aja@bosch-si.com
 * createdOn: 2015-06-12
 * 
 */

Ext.define("Energy.common.ui.DayMonthSelector", {
    extend: 'Ext.container.Container',
    alias: 'widget.commonUiDayMonthSelector',
    fieldLabel: '',
    border: false,
    name: 'start',
    value: null,
    padding: '0 0 5 0',
    useYear: false,
    locale: undefined,
    localizedLabels: undefined,

    initComponent : function(config) {

        var me = this;
        var label = {
            xtype: 'displayfield',
            fieldLabel: this.fieldLabel,
            labelWidth: 200

        };

        me.setLocalizedLabel();

        var localizedLabels = new Ext.util.MixedCollection();
        localizedLabels.add('de.day', 'Tag');
        localizedLabels.add('de.month', 'Monat');
        localizedLabels.add('de.maxDays', 'Der Monat hat maximal {0} Tage.');
        localizedLabels.add('de.maxDaysLeapYear', 'Der Monat hat maximal {0}, im Schaltjahr {1}, Tage.');
        localizedLabels.add('de.year', 'Jahr');
        localizedLabels.add('de.everyYear', 'jedes Jahr');
        localizedLabels.add('de.yearNotEmpty','Die Jahresinformation muss hinterlegt werden');
        localizedLabels.add('en.day', 'Day');
        localizedLabels.add('en.month', 'Month');
        localizedLabels.add('en.maxDays', 'Selected month has {0} days at maximum.');
        localizedLabels.add('en.maxDaysLeapYear', 'Selected month has {0}, in leap year {1}, days at maximum.');
        localizedLabels.add('en.year', 'Year');
        localizedLabels.add('en.everyYear', 'every year');
        localizedLabels.add('en.yearNotEmpty','Year information cannot be empty.');

        me.localizedLabels = localizedLabels;

        var items = [
            {
                xtype: 'container',
                itemId: 'dayMonthSelection',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'numberfield',
                        emptyText: me.getLocalizedLabel('day'),
                        labelWidth: 60,
                        flex:1,
                        name: 'day',
                        minValue: '1',
                        maxValue: '31',
                        msgTarget: 'side',
                        margin: '0 10 0 0',
                        listeners: {
                            'change': function (numberfield) {
                                if(numberfield.next('combo').getValue()){
                                    if(me.isValid()){
                                        me.setNewValue();
                                    }
                                }

                            },
                            scope: me
                        }
                    },
                    {
                        xtype: 'combo',
                        emptyText: me.getLocalizedLabel('month'),
                        name: 'month',
                        flex: 1,
                        labelWidth: 60,
                        displayField: 'monthName',
                        valueField: 'month',
                        allowBlank: false,
                        queryMode: 'local',
                        forceSelection: true,
                        triggerAction: 'all',
                        msgTarget: 'side',
                        store: Ext.create('Ext.data.Store', {
                                fields: [
                                    { name: 'month' },
                                    { name: 'monthName', type: 'string', convert: function (v, rec) {
                                        return Ext.Date.monthNames[rec.get('month')];
                                    } },
                                    { name: 'days'}
                                ],
                                data: [
                                    {month: 0, monthName: 'jan', days: '31'},
                                    {month: 1, monthName: 'feb', days: '29'},
                                    {month: 2, monthName: 'mar', days: '31'},
                                    {month: 3, monthName: 'apr', days: '30'},
                                    {month: 4, monthName: 'may', days: '31'},
                                    {month: 5, monthName: 'jun', days: '30'},
                                    {month: 6, monthName: 'jul', days: '31'},
                                    {month: 7, monthName: 'aug', days: '31'},
                                    {month: 8, monthName: 'sep', days: '30'},
                                    {month: 9, monthName: 'oct', days: '31'},
                                    {month: 10, monthName: 'nov', days: '30'},
                                    {month: 11, monthName: 'dec', days: '31'}
                                ]}
                        ),
                        validator: function (v) {
                            var result;
                            var days = this.prev('numberfield');
                            var selectedDay = days.getValue();

                            var month = this.getStore().findRecord('month', this.getValue(), 0, false, false, true);
                            if(month){
                                var maxDays = month.get('days');
                                days.setMaxValue(maxDays);
                                if (selectedDay > maxDays) {
                                    if (maxDays == 29) {
                                        var usual = maxDays - 1;
                                        result = Ext.String.format(me.getLocalizedLabel('maxDaysLeapYear'), usual, maxDays);
                                    }
                                    else {
                                        result = Ext.String.format(me.getLocalizedLabel('maxDays'), maxDays);
                                    }

                                }
                                else {
                                    result = true;
                                }
                            }


                            return result;
                        },
                        listeners: {
                            'select': function (combo, records) {
                                if(combo.prev('numberfield').getValue()){
                                    if(me.isValid()){
                                        me.setNewValue();
                                    }
                                }
                            },
                            scope: me
                        }
                    }
                ]
            }
        ];

        if(this.labelAlign == 'top'){
            items.splice(0,0,label);
        }
        else{
            items.items.splice(0,0,label);
        }

        if(this.useYear){

            var yearItems = {
                xtype: 'container',
                itemId: 'yearSelection',
                padding: '10 0 0 0',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                items: [

                    {
                        xtype: 'numberfield',
                        disabled: true,
                        name: 'year',
                        minValue: new Date().getFullYear(),
                        maxValue: 2100,
                        emptyText: me.getLocalizedLabel('year'),
                        margin: '0 10 0 0',
                        flex: 1,
                        msgTarget: 'side',
                        validator: function (v) {

                            if(!this.next('checkbox').checked && this.getValue() == null){
                                return me.getLocalizedLabel('yearNotEmpty');
                            }
                            else{
                                return true;
                            }

                        },
                        listeners: {
                            'change': function (numberfield) {
                                if(me.isValid()){
                                    me.setNewValue();
                                }

                            },
                            scope: me
                        }

                    },
                    {
                        xtype: 'checkbox',
                        name: 'everyYear',
                        checked: 'checked',
                        boxLabel: me.getLocalizedLabel('everyYear'),
                        flex: 1,
                        inputValue: '*',
                        validator: function (v) {

                            if(!this.checked && this.prev('numberfield').getValue()==null){
                                return me.getLocalizedLabel('yearNotEmpty');
                            }
                            else{
                                return true;
                            }

                        },
                        listeners: {
                            'change': function (checkbox, newValue, oldValue) {
                                if(newValue){ // is checked
                                    checkbox.up('container').down('numberfield').disable();
                                }
                                else{
                                    checkbox.up('container').down('numberfield').enable();

                                }
                                checkbox.up('container').down('numberfield').validate();
                                if(me.isValid()){
                                    me.setNewValue();
                                }
                            },
                            scope: me
                        }

                    }
                ]
            };

            items.push(yearItems);

        }

        Ext.apply(this, {items: items});
        this.callParent(config);

    },

    setNewValue: function(){
        this.value = this.formatSelectedDayMonth();
    },

    getValue: function(){
        this.setNewValue();
        return this.value;
    },

    formatSelectedDayMonth: function(){

        var day = this.down('#dayMonthSelection numberfield').getValue();
        var month = this.down('#dayMonthSelection combo').getValue() + 1;
        var year = '';
        if(this.useYear){
            if(this.down('#yearSelection checkbox[checked=true]')){
                year = this.down('#yearSelection checkbox[checked=true]').getSubmitValue();
            }
            else{
                year = this.down('#yearSelection numberfield').getValue();
            }
        }
        return  day + '.' + month + '.' + year;

    },

    isValid: function(){

        var validYear = true;

        if(this.useYear){

            if(this.down('#yearSelection checkbox[checked=true]')){
                validYear = true;
            }
            else if (this.down('#yearSelection numberfield').getValue() != null){
                validYear = true;
            }
            else{
                this.down('#yearSelection numberfield').validate();
                validYear = false;
            }

        }

        if(this.down('#dayMonthSelection numberfield').isValid()
            && this.down('#dayMonthSelection combo').isValid()
            && validYear){

            return true;
        }
        else{

            return false;
        }

    },
    /**
     * check if common translation is available and take the current locale
     * otherwise use 'de' as locale
     * @param key
     * @returns {string}
     */
    setLocalizedLabel: function(key){

        var locale = 'de';

        if(Energy.common.language.Locale){
            locale = Energy.common.language.Locale.getLocale();
        }

        this.locale = locale;


    },

    /**
     * get translation for localized labels
     * @param key
     * @returns {string}
     */

    getLocalizedLabel: function( key ){

        return this.localizedLabels.get(this.locale + '.' + key);

    }

}); 
