Ext.define('MAM.view.meterPoint.SubDetail', {
    extend: 'Ext.tab.Panel',
    requires: [
        'MAM.view.relations.Initial'
    ],
    alias: 'widget.meterPointSubDetail',
    //plain: true,
    initComponent: function () {
        var items = [];
        var tabConfig = MAM.utils.Configuration.getTabConfig( MAM.utils.Constants.METERPOINT );

        Ext.each(tabConfig, function( tab ){
           items.push({
               xtype: tab.type + 'Initial',
               title: _tl.get("mam.label." + MAM.utils.Constants.METERPOINT + "." + tab.type),
               itemId: MAM.utils.Constants.METERPOINTS + '-tab-' + tab.type
           }) 
        });

        Ext.apply(this, {items: items});
        this.callParent(arguments);
    }
});