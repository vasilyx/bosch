StartTest(function (t) {

    var list = Ext.create('TestApplication.application.AbstractList', {t: t, name: 'connectionBuilding'});
    list.testListViewArea();

    var referenceData = {
        customId: '81096673',
        connectionBuildingId: 1034468,
        streetAndNumber: 'Röwekamp 0007',
        municipalityKey: '03403000',
        latitude: '5815058',
        longitude: '419148.9'
    };

    var referenceDataDetail = {
        customId: 'DE0700082612100000000000070263003',
        medium: '',
        consumptionUnit: 'kWh'
    };

    t.it('#2 Enter full entity id into filter filed', function(t) {
        t.chain(
            function(next) {
                MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/details_81096673.json');
                MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/customAttributes_81096673.json', referenceData.connectionBuildingId);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/connectionBuildings/relations_81096673.json', referenceData.connectionBuildingId);
                MOCK_DATA.getMockedDataDetail('tests/data/detail/meterPoints/r/details_1525555.json'); // for related
                next();
            },
            { doubleclick : t.getFirstRow(t.cq1('connectionBuildingInitial gridpanel')) },
            { waitForCQ : 'connectionBuildingDetail' },
            { waitForComponentVisible : 'connectionBuildingDetail'},
            {
                action      : 'wait',
                delay       : 1500      // 1500 ms
            },
            function(next) {
                t.cqExists('connectionBuildingDetail', 'connectionBuilding Detail view exist');

                for (var code in referenceData) {
                    t.contentLike(t.cq1('connectionBuildingDetail displayfield[name="' + code + '"]'), referenceData[code], 'Found right  #' + code + ' ' + referenceData[code]);
                }
            }
        )
    });

    //#3 two tabs are visible => Expected Result: Relations of Connection Building and Custom Attributes of Connection Building
    t.it('#3 Check Connection Building Sub Tabs', function (t) {
        t.chain(
            {
                waitForCQ: 'connectionBuildingSubDetail',
                waitForCQ: 'customAttributesInitial'

            }, function (next) {
                t.cqExists('connectionBuildingSubDetail', 'ConnetctionBuilding Detail Tabs view exist');
                t.elementIsVisible(t.cq1('relationsInitial'), 'Tab "Relations" is visible');
                t.expect(t.cq1('connectionBuildingSubDetail').items.length).toBe(2);

            }
        );
    });
    //#4 first tab contains relations => Expected Result: 1 relation availalbe (Meter Point)
    t.it('#4 First tab contains relations', function (t) {
        t.chain(
            {
                waitForCQ: 'connectionBuildingSubDetail',
                waitForComponentVisible: 'relationDetail'
            },
            {
                click: t.compositeQuery('connectionBuildingSubDetail tabbar => .x-tab')[0]
            }, {
                waitForCQ: 'relationsInitial',
                waitForComponentVisible: t.cq1('connectionBuildingSubDetail relationsInitial grid')

            }, function (next) {
                t.elementIsVisible(t.cq1('connectionBuildingSubDetail relationsInitial grid'), 'Relations is visible');
                var accordionEl = t.cq('relationsInitial #relationAccordion grid');
                t.expect(accordionEl.length).toBe(1);
                t.like(accordionEl[0].getTitle(), Energy.common.language.Translations.get("mam.label.meterPoint"), 'Accordion meter is visible');
            }
        );
    });

    t.it('#5 Meter point relation view', function (t) {
        t.chain(
            {
                click: t.compositeQuery('connectionBuildingSubDetail relationsInitial grid => .x-grid-cell')[1],
                waitForComponentVisible: 'meterPointDetail'
            },
            function (next) {
                t.expect( t.cq1('connectionBuildingSubDetail relationsInitial grid').getStore().getCount()).toBe(1);
                t.elementIsVisible(t.cq1('relationsInitial meterPointDetail'), 'MeterPointDetail is visible');

                for (var code in referenceDataDetail) {
                    t.contentLike(t.cq1('relationsInitial meterPointDetail displayfield[name="' + code + '"]'), referenceDataDetail[code], 'Found right  #' + code + ' ' + referenceDataDetail[code]);
                }
            }
        );
    });
});