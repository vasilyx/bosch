Ext.define('MAM.view.meterPoint.InitialModel', {
    extend: 'Ext.app.ViewModel',
    requires: [
        'MAM.model.Gateway',
        'MAM.utils.model.Converts',
        'MAM.utils.Configuration'
    ],

    alias: 'viewmodel.meterPointInitialModel',

    data: {
        meterPoint: null,
        relations: null,
        total: 0,
        editMode: false,
        urlEnumProperties: null
    },

    /* TODO: all stores should be defined in view model
    not working now because of the access to the Configuration Object
    stores: {
        consumptionUnits: {
           // type: 'json',
            autoLoad: false,
            proxy: {
                type: 'ajax',
                url: '{urlEnumProperties.consumptionUrl}',//MAM.utils.Configuration.getUrlEnumProperty( MAM.utils.Constants.METERPOINT, 'consumptionUnit'),
                reader: {
                    type: 'json',
                    rootProperty: 'consumptionUnit'
                }
            }

        }
    },*/


    cleanData: function() {
        this.set('meterPoint', null);
        this.set('relations', null);
    },
    
    formulas: {
        medium: function(get){
            return  MAM.utils.model.Converts.convertMedium( get('meterPoint.medium') );
        },
        measuringType: function(get){
            return  MAM.utils.model.Converts.convertMeasuringType( get('meterPoint.measuringType') );
        },
        totalText: {
            get: function(get) {
                var total = get('total');
                if(total === 1){
                    return Ext.util.Format.number(total, '0,000') + ' ' + _tl.get('mam.label.meterPoint');
                }
                else{
                    return Ext.util.Format.number(total, '0,000') + ' ' + _tl.get('mam.label.meterPoints')
                }
            }
        },
        editModeText: function (get) {
            var param = get('editMode');
            return param ? _tl.get('mam.label.mode.edit') : _tl.get('mam.label.mode.view');
        }
    }


});