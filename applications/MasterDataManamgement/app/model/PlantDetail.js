Ext.define('MAM.model.PlantDetail', {
    extend: 'MAM.model.Plant',
    proxy: {
        type: 'rest',
        api: {
            create: undefined,
            read: '/MAM/' + MAM.Config.Backend.Versions + '/plants',
            update: undefined,
            destroy: undefined
        },
        headers: {
            'Accept': 'application/json'
        },
        extraParams: {
            expand: 'properties'
        }
    }
});